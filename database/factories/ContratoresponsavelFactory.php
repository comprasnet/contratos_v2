<?php

namespace Database\Factories;

use App\Models\CodigoItem;
use App\Models\Contrato;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContratoresponsavelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'contrato_id' => Contrato::factory(),
            'user_id' => User::factory(),
            'funcao_id' => CodigoItem::where('descres', 'FSCADM')->first()->id,
            'instalacao_id' => 1,
            'portaria' => 'teste-teste',
            'situacao' => true,
        ];
    }
}
