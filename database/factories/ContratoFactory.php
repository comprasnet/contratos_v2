<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ContratoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'numero' => 'teste/' . $this->faker->year(),
            'fornecedor_id' => 1,
            'unidade_id' => 1,
            'unidadeorigem_id' => 1,
            'objeto' => $this->faker->text(),
            'vigencia_inicio' => $this->faker->date(),
            'valor_global' => $this->faker->randomFloat(),
            'situacao' => true,
        ];
    }
}
