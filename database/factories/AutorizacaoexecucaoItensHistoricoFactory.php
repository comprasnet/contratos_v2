<?php

namespace Database\Factories;

use App\Models\AutorizacaoexecucaoHistorico;
use App\Models\Contratohistorico;
use App\Models\Saldohistoricoitem;
use Illuminate\Database\Eloquent\Factories\Factory;

class AutorizacaoexecucaoItensHistoricoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'autorizacaoexecucao_historico_id' => AutorizacaoexecucaoHistorico::factory(),
            'contratohistorico_id' => Contratohistorico::first()->id,
            'saldohistoricoitens_id' => Saldohistoricoitem::factory(),
            'tipo_historico' => 'antes',
        ];
    }
}
