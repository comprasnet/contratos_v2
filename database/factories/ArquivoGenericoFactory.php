<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArquivoGenericoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
//            'arquivoable_type' => ,
//            'arquivoable_id' => ,
            'tipo_id' => 1,
            'nome' => $this->faker->name,
            'descricao' => '',
//            'url' => ,
            'restrito' => false,
            'user_id' => User::factory(),
        ];
    }
}
