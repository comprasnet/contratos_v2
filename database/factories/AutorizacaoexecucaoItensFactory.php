<?php

namespace Database\Factories;

use App\Models\AutorizacaoExecucao;
use App\Models\Contrato;
use App\Models\Contratohistorico;
use App\Models\Saldohistoricoitem;
use Illuminate\Database\Eloquent\Factories\Factory;

class AutorizacaoexecucaoItensFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'autorizacaoexecucoes_id' => AutorizacaoExecucao::factory(),
            'saldohistoricoitens_id' => Saldohistoricoitem::factory(),
            'contratohistorico_id' => Contratohistorico::first()->id,
        ];
    }
}
