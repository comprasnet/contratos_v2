<?php

namespace Database\Factories;

use App\Models\Contrato;
use Illuminate\Database\Eloquent\Factories\Factory;

class AutorizacaoExecucaoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'contrato_id' => Contrato::factory(),
            'tipo_id' => 1,
            'situacao_id' => 1,
            'rascunho' => true,
            'originado_sistema_externo' => false,
        ];
    }
}
