<?php

namespace Database\Factories;

use App\Models\AutorizacaoExecucao;
use App\Models\CodigoItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class AutorizacaoexecucaoHistoricoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'autorizacaoexecucoes_id' => AutorizacaoExecucao::factory(),
            'tipo_historico_id' => CodigoItem::where('descres', 'aeh_alterar')->first()->id,
            'rascunho' => false,
            'situacao_id' => CodigoItem::where('descres', 'ae_status_1')->first()->id,
        ];
    }
}
