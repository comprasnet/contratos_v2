<?php

namespace Database\Factories;

use App\Models\Contrato;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Provider\pt_BR\Person;

class ContratoprepostoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $this->faker->addProvider(new Person($this->faker));

        return [
            'contrato_id' => Contrato::factory(),
            'cpf' => $this->faker->cpf(),
            'nome' => $this->faker->name(),
            'email' => $this->faker->email(),
            'data_inicio' => now(),
        ];
    }
}
