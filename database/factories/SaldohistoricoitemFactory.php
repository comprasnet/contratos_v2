<?php

namespace Database\Factories;

use App\Models\Contratohistorico;
use App\Models\Contratoitem;
use Illuminate\Database\Eloquent\Factories\Factory;

class SaldohistoricoitemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'saldoable_type' => Contratohistorico::class,
            'saldoable_id' => Contratohistorico::first()->id,
            'contratoitem_id' => Contratoitem::first()->id,
            'tiposaldo_id' => 1,
            'quantidade' => $this->faker->numberBetween(1, 100),
            'valorunitario' => $this->faker->numberBetween(1, 100),
            'valortotal' => $this->faker->numberBetween(1, 100),
            'elaboracao' => false,
        ];
    }
}
