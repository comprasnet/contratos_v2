<?php

namespace Database\Factories;

use App\Models\ArquivoGenerico;
use App\Models\CodigoItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class ModelSignatarioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'arquivo_generico_id' => ArquivoGenerico::first()->id,
            'status_assinatura_id' => CodigoItem::where('descres', 'status_assinatura')->first()->id,
            'posicao_x_assinatura' => $this->faker->numberBetween(0, 100),
            'posicao_y_assinatura' => $this->faker->numberBetween(0, 100),
            'pagina_assinatura' => $this->faker->numberBetween(0, 10),
        ];
    }
}
