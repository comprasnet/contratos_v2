<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnSistemaExternoELocalidadeoToAutorizacaoexecucaoItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_itens', function (Blueprint $table) {
            $table->string('numero_demanda_sistema_externo')->nullable();
            $table->string('localidade_de_execucao')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_itens', function (Blueprint $table) {
            $table->dropColumn('localidade_de_execucao');
            $table->dropColumn('numero_demanda_sistema_externo');
        });
    }
}
