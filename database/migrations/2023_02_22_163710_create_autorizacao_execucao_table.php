<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutorizacaoExecucaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::create([
            'descricao' => 'Tipo Autorizacao Execução',
            'visivel' => true
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Ordem de Serviço',
            'visivel' => true,
            'descres' => 'SERVICO',
        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Autorização de Fornecimento',
            'visivel' => true,
            'descres' => 'AUTOFORN',

        ]);

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Ordem de Serviço / Autorização de Fornecimento',
            'visivel' => true,
            'descres' => 'SERVAUTORIZACAOFORN',
        ]);

        Schema::create('autorizacaoexecucoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contrato_id');
            $table->string('processo')->nullable();
            $table->integer('tipo_id');
            $table->string('numero')->nullable();
            $table->date('data_assinatura')->nullable();
            $table->text('informacoes_complementares')->nullable();
            $table->boolean('rascunho');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('contrato_id')->references('id')->on('contratos')->onDelete('cascade');
            $table->foreign('tipo_id')->references('id')->on('codigoitens')->onDelete('cascade');
        });

        Schema::create('autorizacaoexecucao_empenhos', function (Blueprint $table) {
            $table->integer('autorizacaoexecucoes_id');
            $table->integer('empenhos_id');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('autorizacaoexecucoes_id')->references('id')->on('autorizacaoexecucoes')->onDelete('cascade');
            $table->foreign('empenhos_id')->references('id')->on('empenhos')->onDelete('cascade');
        });

        Schema::create('autorizacaoexecucao_itens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('autorizacaoexecucoes_id');
            $table->integer('contratohistorico_id');
            $table->integer('saldohistoricoitens_id');
            $table->decimal('quantidade', 15,5)->nullable();
            $table->time('horario')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('autorizacaoexecucoes_id')->references('id')->on('autorizacaoexecucoes')->onDelete('cascade');
            $table->foreign('contratohistorico_id')->references('id')->on('contratohistorico')->onDelete('cascade');
            $table->foreign('saldohistoricoitens_id')->references('id')->on('saldohistoricoitens')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autorizacaoexecucao_empenhos');
        Schema::dropIfExists('autorizacaoexecucao_objetos');
        Schema::dropIfExists('autorizacaoexecucoes');

        Codigo::where([
            'descricao' => 'Tipo Autorizacao Execução',
            'visivel' => true
        ])->forceDelete();
    }
}
