<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlteraColunaQuantidadeAutorizacaoexecucaoItens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_itens', function (Blueprint $table) {
            $table->decimal('quantidade', 30, 17)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_itens', function (Blueprint $table) {
            $table->decimal('quantidade', 15, 5)->change();
        });
    }
}
