<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutorizacaoExecucaoEntregaItens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autorizacaoexecucao_entrega_itens', function (Blueprint $table) {
            $table->id();
            $table->integer('autorizacao_execucao_entrega_id');
            $table->integer('autorizacao_execucao_itens_id');
            $table->decimal('quantidade_informada', 19, 4);
            $table->timestamps();
            $table->foreign('autorizacao_execucao_entrega_id')->references('id')
                ->on('autorizacaoexecucao_entrega')->cascadeOnDelete();
            $table->foreign('autorizacao_execucao_itens_id')->references('id')
                ->on('autorizacaoexecucao_itens')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autorizacaoexecucao_entrega_itens');
    }
}
