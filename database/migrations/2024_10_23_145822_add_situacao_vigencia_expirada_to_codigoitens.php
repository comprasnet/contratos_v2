<?php

use App\Models\AutorizacaoExecucao;
use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSituacaoVigenciaExpiradaToCodigoitens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Situação Autorização Execução')->first();

        $situacaoVigenciaExpirada = CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_status_8',
            'descricao' => 'Vigência Expirada',
        ]);

        $situacaoConcluido = CodigoItem::where('descres', 'ae_status_4')->first();
        $autorizacoes = AutorizacaoExecucao::where('situacao_id', $situacaoConcluido->id)->get();

        $autorizacoes->each(function ($autorizacao) use ($situacaoVigenciaExpirada) {
            $qtdTotalSolicitada = $autorizacao->getQuantidadeTotalSemFormatacao();
            $qtdQtdTotalAposTrd = $autorizacao->getQuantidadeTotalQtdAnaliseAposTRD();

            if ($qtdTotalSolicitada != $qtdQtdTotalAposTrd) {
                $autorizacao->update(['situacao_id' => $situacaoVigenciaExpirada->id]);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $situacaoVigenciaExpirada = CodigoItem::where('descres', 'ae_status_8')->first();
        $situacaoConcluido = CodigoItem::where('descres', 'ae_status_4')->first();

        AutorizacaoExecucao::where('situacao_id', $situacaoVigenciaExpirada->id)
            ->update(['situacao_id' => $situacaoConcluido->id]);

        $situacaoVigenciaExpirada->forceDelete();
    }
}
