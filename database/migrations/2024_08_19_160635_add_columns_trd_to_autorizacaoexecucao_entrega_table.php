<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsTrdToAutorizacaoexecucaoEntregaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->text('introducao_trd')->nullable();
            $table->text('declaracao_itens_entrega')->nullable();
            $table->text('nivel_qualidade_entrega_trd')->nullable();
            $table->text('desconto_efetuado_valor_liquidar')->nullable();
        });

        $codigo = Codigo::where('descricao', 'Situação Autorizacao Execução Entrega')->first();

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_entrega_status_11',
            'descricao' => 'TRD em Elaboração',
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_entrega_status_12',
            'descricao' => 'Aguardando Instrumento de Cobrança',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->dropColumn('introducao_trd');
            $table->dropColumn('declaracao_itens_entrega');
            $table->dropColumn('nivel_qualidade_entrega_trd');
            $table->dropColumn('desconto_efetuado_valor_liquidar');
        });

        CodigoItem::where('descres', 'ae_entrega_status_11')->forceDelete();
        CodigoItem::where('descres', 'ae_entrega_status_12')->forceDelete();
    }
}
