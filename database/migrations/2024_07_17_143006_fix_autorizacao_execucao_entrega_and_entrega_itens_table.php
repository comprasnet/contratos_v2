<?php

use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixAutorizacaoExecucaoEntregaAndEntregaItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->date('mes_ano_referencia')->nullable();
        });

        Schema::table('autorizacaoexecucao_entrega_itens', function (Blueprint $table) {
            $table->decimal('quantidade_informada', 30, 17)->change();
            $table->decimal('valor_glosa', 19)->default(0);
        });

        CodigoItem::where('descres', 'ae_entrega_status_1')->update(['descricao' => 'Em Análise']);
        CodigoItem::where('descres', 'ae_entrega_status_9')->update(['descricao' => 'Em Elaboração']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->dropColumn('mes_ano_referencia');
        });

        Schema::table('autorizacaoexecucao_entrega_itens', function (Blueprint $table) {
            $table->decimal('quantidade_informada', 19, 4)->change();
            $table->dropColumn('valor_glosa');
        });

        CodigoItem::where('descres', 'ae_entrega_status_1')->update(['descricao' => 'Avaliação Sumária']);
        CodigoItem::where('descres', 'ae_entrega_status_9')->update(['descricao' => 'Entrega em elaboração']);
    }
}
