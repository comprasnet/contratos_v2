<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeleteOncascadeArpItemHistorico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    /**
     * Essa migration está sendo comentada 
     * o conteúdo refrenete ao cascade migrado para 2023_05_23_153203_create_table_arp_item_historico.php
     * 
     * Durante execução das migrations percebeu-se que essa migration de 14/04/2023 apaga uma foreignkey
     * de uma tabela que só foi criada em 23/05/2023 (2023_05_23_153203_create_table_arp_item_historico.php)
     * Assim sendo, a criação do cascade está sendo comentada e adicionada no arquiovo citado
     *
     */
    public function up()
    {
    //    Schema::table('arp_item_historico', function (Blueprint $table) {
    //        $table->dropForeign('arp_item_historico_arp_item_id_foreign');
    //        $table->foreign('arp_item_id')->references('id')->on('arp_item')->onDelete('cascade');
    //    });

        Schema::table('arp_alteracao', function (Blueprint $table) {
            $table->dropForeign('arp_alteracao_arp_id_foreign');
            $table->foreign('arp_id')->references('id')->on('arp')->onDelete('cascade');
        });

        Schema::table('arp_historico', function (Blueprint $table) {
            $table->dropForeign('arp_historico_arp_alteracao_id_foreign');
            $table->foreign('arp_alteracao_id')->references('id')->on('arp_alteracao')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
