<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnProcessoSistemaOrigemToAutorizacaoexecucoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucoes', function (Blueprint $table) {
            $table->string('numero_sistema_origem')->nullable();
        });

        Schema::table('autorizacaoexecucao_historico', function (Blueprint $table) {
            $table->string('numero_sistema_origem_antes')->nullable();
            $table->string('numero_sistema_origem_depois')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucoes', function (Blueprint $table) {
            $table->dropColumn('numero_sistema_origem');
        });

        Schema::table('autorizacaoexecucao_historico', function (Blueprint $table) {
            $table->dropColumn('numero_sistema_origem_antes');
            $table->dropColumn('numero_sistema_origem_depois');
        });
    }
}
