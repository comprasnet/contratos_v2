<?php

use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixIssue514Entrega extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->text('informacoes_complementares_trp')->nullable();
            $table->text('informacoes_complementares_trd')->nullable();

            $table->dropColumn([
                'recebimento',
                'nivel_qualidade_entrega',
                'declaracao_itens_entrega',
                'nivel_qualidade_entrega_trd',
                'desconto_efetuado_valor_liquidar'
            ]);
        });

        Schema::table('autorizacaoexecucao_entrega_itens', function (Blueprint $table) {
            $table->dropColumn([
               'local',
               'requisitante',
               'ocorrencia',
            ]);
        });

        CodigoItem::where('descres', 'ae_entrega_status_10')->update(['descricao' => 'Aguardando TRD']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->dropColumn([
                'informacoes_complementares_trp',
                'informacoes_complementares_trd',
            ]);

            $table->text('recebimento')->nullable();
            $table->text('nivel_qualidade_entrega')->nullable();
            $table->text('declaracao_itens_entrega')->nullable();
            $table->text('nivel_qualidade_entrega_trd')->nullable();
            $table->text('desconto_efetuado_valor_liquidar')->nullable();
        });

        Schema::table('autorizacaoexecucao_entrega_itens', function (Blueprint $table) {
            $table->string('local')->nullable();
            $table->string('requisitante')->nullable();
            $table->string('ocorrencia')->nullable();
        });

        CodigoItem::where('descres', 'ae_entrega_status_10')->update(['descricao' => 'Elaborar TRD']);
    }
}
