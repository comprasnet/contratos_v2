<?php

use App\Models\ArquivoGenerico;
use App\Models\AutorizacaoExecucao;
use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;

class InsertTipoArquivosAutorizacaoExecucao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::create([
            'descricao' => 'Tipo Arquivos Autorização Execução',
            'visivel' => true,
        ]);

        $codigoItemAssinado = CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_arquivo_assinado',
            'descricao' => 'Arquivo - OS/F assinada',
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_arquivo_anexo',
            'descricao' => 'Anexos - OS/F',
        ]);

        $codigoItem = CodigoItem::where('descres', 'ae_status_2')->first();
        ArquivoGenerico::where('arquivoable_type', AutorizacaoExecucao::class)
            ->where('tipo_id', $codigoItem->id)
            ->update(['tipo_id' => $codigoItemAssinado->id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigoItemAssinado = CodigoItem::where('descres', 'ae_arquivo_assinado')->first();
        $codigoItem = CodigoItem::where('descres', 'ae_status_2')->first();
        ArquivoGenerico::where('arquivoable_type', AutorizacaoExecucao::class)
            ->where('tipo_id', $codigoItemAssinado->id)
            ->update(['tipo_id' => $codigoItem->id]);

        Codigo::where('descricao', 'Tipo Arquivos Autorização Execução')->forceDelete();
    }
}
