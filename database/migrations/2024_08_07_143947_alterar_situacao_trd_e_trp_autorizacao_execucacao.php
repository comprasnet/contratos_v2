<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\CodigoItem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AlterarSituacaoTrdETrpAutorizacaoExecucacao extends Migration
{
    private $situacaoAtual = ['ae_entrega_status_6' => 'Elaborar TRP'];
    
    private $novaSituacao = ['ae_entrega_status_6' => 'Aguardando TRP'];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->alterarSituacao($this->situacaoAtual, $this->novaSituacao);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->alterarSituacao($this->novaSituacao, $this->situacaoAtual);
    }
    
    private function alterarSituacao(array $situacaoAtual, array $novaSituacao)
    {
        try {
            DB::beginTransaction();
            foreach ($situacaoAtual as $descres => $descricao) {
                $codigoItem = CodigoItem::where('descres', $descres)->where('descricao', $descricao)->first();
                
                $codigoItem->descricao = $novaSituacao[$descres];
                $codigoItem->save();
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error('Erro ao executar a migration AlterarSituacaoTrdETrpAutorizacaoExecucacao');
            Log::error($ex->getMessage());
        }
    }
}
