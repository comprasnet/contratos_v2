<?php

use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToAutorizacaoexecucaoEntregaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->date('data_prazo_trp')->nullable();
            $table->date('data_prazo_trd')->nullable();
            $table->integer('user_id_criacao')->nullable();
            $table->text('justificativa_motivo_analise')->nullable();
        });

        CodigoItem::where('descres', 'ae_entrega_status_3')->update([
            'descricao' => 'Recusada'
        ]);

        CodigoItem::where('descres', 'ae_entrega_status_5')->update([
            'descricao' => 'Cancelada'
        ]);

        CodigoItem::where('descres', 'ae_entrega_status_6')->update([
            'descricao' => 'Elaborar TRP'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->dropColumn('data_prazo_trp');
            $table->dropColumn('data_prazo_trd');
            $table->dropColumn('user_id_criacao');
            $table->dropColumn('justificativa_motivo_analise');
        });

        CodigoItem::where('descres', 'ae_entrega_status_3')->update([
            'descricao' => 'Correção Sumária'
        ]);

        CodigoItem::where('descres', 'ae_entrega_status_5')->update([
            'descricao' => 'Avaliando Execução'
        ]);

        CodigoItem::where('descres', 'ae_entrega_status_6')->update([
            'descricao' => 'Correção Execução'
        ]);
    }
}
