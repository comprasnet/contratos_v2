<?php

use App\Models\Arp;
use App\Repositories\Compra\CompraItemFornecedorRepository;
use Illuminate\Database\Migrations\Migration;
use \App\Models\Compras;
use \Illuminate\Support\Facades\DB;
use \App\Models\CompraItemFornecedor;
use \Illuminate\Support\Facades\Log;
use \App\Models\ArpItemHistorico;
use \App\Models\ArpItem;

class FixValorTotalItemFornecedorAlteracaoValorUnitarioCompraSisrp14133 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $itens = Compras::join('codigoitens', function ($join) {
            $join->on('compras.tipo_compra_id', '=', 'codigoitens.id')
                ->where('codigoitens.descres', '=', '02');
        })
            ->join('compra_items', 'compras.id', '=', 'compra_items.compra_id')
            ->join('compra_item_fornecedor', 'compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')
            ->select(
                'quantidade_homologada_vencedor',
                'valor_unitario',
                DB::raw('quantidade_homologada_vencedor * valor_unitario as valor_total'),
                'valor_negociado',
                DB::raw('(quantidade_homologada_vencedor * valor_unitario = valor_negociado) as comparacao'),
                'compra_item_fornecedor.id'
            )
            ->where('compras.lei', '=', 'LEI14133')
            ->whereRaw('(quantidade_homologada_vencedor * valor_unitario) <> valor_negociado')
            ->where('valor_unitario', '>', 0)
            ->get();
        
        try {
            DB::beginTransaction();
            
            $compraItemFornecedorRepository =  new CompraItemFornecedorRepository();
            
            foreach ($itens as $item) {
                $mensagem ="ID: {$item->id} valor_negociado_atual: {$item->valor_negociado}
            valor_negociado_atual: {$item->valor_total}";
                
                $itemFornecedor = CompraItemFornecedor::find($item->id);
                $itemFornecedor->valor_negociado = $item->valor_total;
                $itemFornecedor->save();
                
                # Recuperar as atas que tiveram as alterações de valores
                $atas = ArpItemHistorico::join('arp_item', 'arp_item.id', '=', 'arp_item_historico.arp_item_id')
                    ->where('valor_alterado', true)
                    ->where('arp_item.compra_item_fornecedor_id', '=', $itemFornecedor->id)
                    ->selectRaw('distinct arp_item.arp_id')
                    ->get();
                
                foreach ($atas as $ata) {
                    # Recuperar os itens vinculados as atas
                    $arrayItensAta = ArpItem::where('arp_id', $ata->arp_id)->select('compra_item_fornecedor_id')->get()
                        ->pluck('compra_item_fornecedor_id')-> toArray();
                    
                    $novoValorAta = $compraItemFornecedorRepository->getValorTotalItemFornecedor($arrayItensAta);

                    $arp = Arp::find($ata->arp_id);
                    $valorTotalAntigoAta = $arp->valor_total;
                    $arp->valor_total = $novoValorAta;
                    $arp->save();
                    
                    $mensagemAta = "ID ata: {$arp->id} valor_total_antigo: {$valorTotalAntigoAta}
                        valor_total_novo: {$novoValorAta}";
                    Log::info($mensagemAta);
                }
                Log::info($mensagem);
            }
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            $titulo = 'Erro ao executar a migration FixValorTotalItemFornecedorAlteracaoValorUnitarioCompraSisrp14133';
            Log::error($titulo);
            Log::error($exception);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
