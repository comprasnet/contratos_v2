<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertTipoAdesaoArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where("descricao","Tipo de Ata de Registro de Preços")->first();
        $status = config("arp.arp.status_adesao_inicial");
        foreach ($status as $key => $status) {
            CodigoItem::updateOrInsert(['codigo_id' => $codigo->id, 'descres' => $key, 'descricao' => $status]);
        }
        
        Schema::table('arp_solicitacao_item', function (Blueprint $table) {
            $table->integer('status_id');
            $table->foreign('status_id')->references('id')->on('codigoitens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_solicitacao_item', function (Blueprint $table) {
            $table->dropColumn('status_id');
        });
    }
}
