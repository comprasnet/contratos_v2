<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnviaDadosPncpHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('envia_dados_pncp_historico', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('envia_dados_pncp_id');
            $table->morphs('pncpable');
            $table->string('log_name')->nullable();
            $table->string('status_code')->nullable();
            $table->integer('situacao');
            $table->text('json_enviado_inclusao')->nullable();
            $table->text('json_enviado_alteracao')->nullable();
            $table->text('retorno_pncp')->nullable();
            $table->string('link_pncp')->nullable();
            $table->bigInteger('contrato_id')->nullable();
            $table->string('sequencialPNCP')->nullable();
            $table->string('tipo_contrato')->nullable();
            $table->string('linkArquivoEmpenho')->nullable();
            $table->string('justificativa')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('envia_dados_pncp_id')
                ->references('id')
                ->on('envia_dados_pncp')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('envia_dados_pncp_historico');
    }
}
