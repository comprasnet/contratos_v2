<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InsertCodigoitensRetificarItemExcluir extends Migration
{
    const RETIFICAR_EXCLUIR_ITEM = 'Retificar - Excluir item';
    const RETIFICAR_DADOS_ATA_EXCLUIR_ITEM = 'Retificar - Dados da ata e exclusão de item';

    /**
     * Run the migrations.
     * Insere novo registro na tabela codigoitens para representar o status de item removido por retificação
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {

            # A lógica abaixo é feita para garantir o funcionamento mesmo que executado o rollback várias vezes

            /**************************************************/
            /*** Insere o status 'Retificar - Excluir item' ***/
            /**************************************************/

            // Busca o registro, incluindo registros soft-deleted
            $codigoItem = CodigoItem::withTrashed()
                ->where('descres', 'statushistorico')
                ->where('descricao', self::RETIFICAR_EXCLUIR_ITEM)
                ->first();

            if ($codigoItem) {
                // Se o registro existir, atualiza o deleted_at para null
                $codigoItem->deleted_at = null;
                $codigoItem->save();
            } else {
                # Para buscar o codigo_id da tabela codigos
                $descricaoTipoAta = 'Tipo de Ata de Registro de Preços';
                $tipoAtaCodigo = Codigo::where('descricao', $descricaoTipoAta)->first();

                // Se o registro não existir, cria um novo
                CodigoItem::updateOrCreate([
                    'codigo_id' => $tipoAtaCodigo->id,
                    'descres' => 'statushistorico',
                    'descricao' => self::RETIFICAR_EXCLUIR_ITEM,
                    'visivel' => true,
                    'ordem' => null,
                ]);
            }

            /*********************************************************************/
            /*** Insere o status 'Retificar - Dados da ata e exclusão de item' ***/
            /*********************************************************************/

            define('RETIFICAR_DADOS_ATA_EXCLUIR_ITEM', 'Retificar - Dados da ata e exclusão de item');
            // Busca o registro, incluindo registros soft-deleted
            $codigoItem = CodigoItem::withTrashed()
                ->where('descres', 'statushistorico')
                ->where('descricao', self::RETIFICAR_DADOS_ATA_EXCLUIR_ITEM)
                ->first();

            if ($codigoItem) {
                // Se o registro existir, atualiza o deleted_at para null
                $codigoItem->deleted_at = null;
                $codigoItem->save();
            } else {
                # Para buscar o codigo_id da tabela codigos
                $descricaoTipoAta = 'Tipo de Ata de Registro de Preços';
                $tipoAtaCodigo = Codigo::where('descricao', $descricaoTipoAta)->first();

                // Se o registro não existir, cria um novo
                CodigoItem::updateOrCreate([
                    'codigo_id' => $tipoAtaCodigo->id,
                    'descres' => 'statushistorico',
                    'descricao' => self::RETIFICAR_DADOS_ATA_EXCLUIR_ITEM,
                    'visivel' => true,
                    'ordem' => null,
                ]);
            }
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        CodigoItem::withTrashed()
            ->where(function ($query) {
                $query->where('descres', 'statushistorico')
                    ->where('descricao', self::RETIFICAR_EXCLUIR_ITEM);
            })
            ->orWhere(function ($query) {
                $query->where('descres', 'statushistorico')
                    ->where('descricao', self::RETIFICAR_DADOS_ATA_EXCLUIR_ITEM);
            })
            ->delete();
    }
}
