<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnStatusItemRemanejamento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_remanejamento_itens', function (Blueprint $table) {
            $table->integer('situacao_id')->nullable();
            $table->longText('justificativa_analise')->nullable();

            $table->integer('status_analise_id')->nullable();

            $table->foreign('situacao_id')->references('id')->on('codigoitens');
            $table->foreign('status_analise_id')->references('id')->on('codigoitens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_remanejamento_itens', function (Blueprint $table) {
            $table->dropColumn(['situacao_id', 'justificativa_analise', 'status_analise_id']);
        });
    }
}
