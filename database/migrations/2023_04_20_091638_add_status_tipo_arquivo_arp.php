<?php

use App\Models\ArpArquivoTipo;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusTipoArquivoArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ArpArquivoTipo::updateOrCreate(["nome" => 'Alteração do arquivo']);

        Schema::table('arp_arquivos', function (Blueprint $table) {
            $table->integer('arp_alteracao_id')->nullable();
            $table->foreign('arp_alteracao_id')->references('id')->on('arp_alteracao')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_arquivos', function (Blueprint $table) {
            $table->dropColumn('arp_alteracao_id');
        });

        Schema::table('arp_historico', function (Blueprint $table) {
            $table->text('arquivo_alteracao')->nullable();
            $table->text('descricao_anexo')->nullable();
        });
    }
}
