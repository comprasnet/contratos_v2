<?php

use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;
use \App\Models\Arp;
use Illuminate\Support\Facades\Log;

class FixValorTotalAtaRegistroPreco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $atasValorTotalErrado = Arp::select(
            'arp.id',
            'unidades.codigo',
            'arp.valor_total',
            DB::raw('ROUND(SUM(
            CASE
                WHEN compra_item_fornecedor.percentual_maior_desconto > 0
                    THEN TO_CHAR(
                        compra_item_fornecedor.valor_unitario *
                        ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100),
                        \'FM999999999.0000\'
                    )::float * compra_item_fornecedor.quantidade_homologada_vencedor
                ELSE
                    compra_item_fornecedor.valor_negociado
            END
        )::NUMERIC, 2) as valor_negociado_correto')
        )
            ->join('arp_item', function ($join) {
                $join->on('arp.id', '=', 'arp_item.arp_id')
                    ->whereNull('arp_item.deleted_at')
                    ->where('arp.rascunho', false);
            })
            ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
            ->join('unidades', 'arp.unidade_origem_id', '=', 'unidades.id')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('arp_historico')
                    ->join('arp_item_historico', 'arp_historico.id', '=', 'arp_item_historico.arp_historico_id')
                    ->join('codigoitens AS ciHistorico', 'arp_historico.tipo_id', '=', 'ciHistorico.id')
                    ->whereColumn('arp_historico.arp_id', 'arp.id')
                    ->whereColumn('arp_item_historico.arp_item_id', 'arp_item.id')
                    ->where('arp_item_historico.item_cancelado', true);
            })
            ->whereNull('arp.deleted_at')
            ->groupBy('arp.id', 'unidades.codigo')
            ->havingRaw('arp.valor_total <> ROUND(SUM(
                                        CASE
                                            WHEN compra_item_fornecedor.percentual_maior_desconto > 0
                                                THEN TO_CHAR(
                                                    compra_item_fornecedor.valor_unitario *
                                                    ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100),
                                                    \'FM999999999.0000\'
                                                )::float * compra_item_fornecedor.quantidade_homologada_vencedor
                                            ELSE
                                                compra_item_fornecedor.valor_negociado
                                        END
                                    )::NUMERIC, 2)')
            ->orderBy('arp.updated_at', 'DESC')
            ->get();

        try {
            DB::beginTransaction();

            foreach ($atasValorTotalErrado as $ata) {
                $mensagem =
                    "Ata_ID {$ata->id} com valor alterado de {$ata->valor_total} para $ata->valor_negociado_correto";
                Log::info($mensagem);
                $ata->valor_total = $ata->valor_negociado_correto;
                $ata->unsetEventDispatcher();
                $ata->save();
            }
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            Log::error("Erro na migration FixValorTotalAtaRegistroPreco");
            Log::error($exception);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
