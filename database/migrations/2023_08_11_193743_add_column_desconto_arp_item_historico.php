<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDescontoArpItemHistorico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_item_historico', function (Blueprint $table) {
            $table->decimal('percentual_maior_desconto', 7, 4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_item_historico', function (Blueprint $table) {
            $table->decimal('percentual_maior_desconto', 7, 4)->nullable();
        });
    }
}
