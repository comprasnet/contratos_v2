<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableExecucaoJobUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('job_id')->nullable();
            $table->integer('job_failed_id')->nullable();
            $table->morphs('job_user');
            $table->integer('compra_item_id')->nullable();
            $table->boolean('processando')->default(false);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('job_id')->references('id')->on('jobs')
            ->onDelete('cascade');
            $table->foreign('compra_item_id')->references('id')->on('compra_items');

            $table->foreign('job_failed_id')->references('id')->on('failed_job')
            ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_user');
    }
}
