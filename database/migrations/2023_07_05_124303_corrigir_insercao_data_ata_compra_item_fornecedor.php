<?php

use App\Http\Traits\Arp\ArpItemTrait;
use App\Http\Traits\LogTrait;
use App\Models\Arp;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CorrigirInsercaoDataAtaCompraItemFornecedor extends Migration
{
    use ArpItemTrait;
    use LogTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $arps = Arp::where('rascunho', false)->get();

        try {
            // Iterar sobre os dados da tabela arp
            foreach ($arps as $arp) {
                # Criar o exception com a mensagem sendo a model da ARP
                # para inserir no log customizado
                $exceptionAta = new Exception($arp);

                # Insere a model arp recuperada
                $this->inserirLogCustomizado('gestao_ata', 'info', $exceptionAta);

                $exceptionItemAta = new Exception($arp->item);

                # Insere a model arp recuperada
                $this->inserirLogCustomizado('gestao_ata', 'info', $exceptionItemAta);

                # Atualiza os itens com as datas de vigência
                $this->inserirVigenciaItem($arp);
            }
        } catch (Exception $ex) {
            # Se acontecer algum erro, pegamos o erro para comparar
            $this->inserirLogCustomizado('gestao_ata', 'error', $ex);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
