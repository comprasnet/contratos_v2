<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricoPrepostoIndicacaoFornecedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('historico_preposto_indicacao_fornecedor')) {
            Schema::create('historico_preposto_indicacao_fornecedor', function (Blueprint $table) {
                $table->id();
                $table->integer('user_id')->unsigned()->nullable();
                $table->integer('status')->unsigned()->nullable();
                $table->integer('contratopreposto_id')->unsigned()->nullable();
                $table->timestamps();

                $table->foreign('user_id', 'fk_user_operacao_preposto_fornecedor')
                    ->references('id')->on('users')
                    ->onDelete('set null');
                $table->foreign('status', 'fk_status_operacao_preposto_fornecedor')
                    ->references('id')->on('codigoitens')
                    ->onDelete('set null');
                $table->foreign('contratopreposto_id', 'fk_contratopreposto_id')
                    ->references('id')->on('contratopreposto')
                    ->onDelete('set null');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('historico_preposto_indicacao_fornecedor')) {
            Schema::table('historico_preposto_indicacao_fornecedor', function (Blueprint $table) {
                $table->dropForeign('fk_user_operacao_preposto_fornecedor');
                $table->dropForeign('fk_status_operacao_preposto_fornecedor');
                $table->dropForeign('fk_contratopreposto_id');
            });
        }
        Schema::dropIfExists('historico_preposto_indicacao_fornecedor');
    }
}
