<?php

use App\Models\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;

class InsertPermissionAcessarDashboardUsuarioFornecedor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        $permission = Permission::where('name', 'acessar_dashboard_usuario_fornecedor')->first();

        if (!isset($permission->id)) {
            Permission::create([
                'name' => 'acessar_dashboard_usuario_fornecedor',
                'guard_name' => 'web'
            ]);
        }
        $role = Role::where('name', 'Fornecedor')->first();
        $role->givePermissionTo('acessar_dashboard_usuario_fornecedor');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Fornecedor'])->first();
        $role->revokePermissionTo('acessar_dashboard_usuario_fornecedor');

        Permission::where(['name' => 'acessar_dashboard_usuario_fornecedor'])->forceDelete();
    }
}
