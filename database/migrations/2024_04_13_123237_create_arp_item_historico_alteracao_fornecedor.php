<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArpItemHistoricoAlteracaoFornecedor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arp_item_historico_alteracao_fornecedor', function (Blueprint $table) {
            $table->id();
            $table->integer('arp_historico_id')->nullable();
            $table->integer('compra_item_fornecedor_origem_id')->nullable();
            $table->integer('fornecedor_origem_id')->nullable();
            $table->integer('compra_item_fornecedor_destino_id')->nullable();
            $table->string('novo_cnpj')->nullable();
            $table->string('novo_nome')->nullable();
            
            
            
            $table->foreign('fornecedor_origem_id')->references('id')
                ->on('fornecedores');
            
            $table->foreign('compra_item_fornecedor_origem_id')->references('id')
                ->on('compra_item_fornecedor');
            
            $table->foreign('compra_item_fornecedor_destino_id')->references('id')
                ->on('compra_item_fornecedor')
                ->name('arp_item_historico_alteracao_fornecedor_compra_item_fornecedor_destino_id_foreign');

            
            $table->foreign('arp_historico_id')->references('id')
                ->on('arp_historico')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arp_item_historico_alteracao_fornecedor');
    }
}
