<?php

use App\Http\Traits\BuscaCodigoItens;
use App\Models\Arp;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ArpHotfixEmlaboracao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $success = $this->getCodigoId('SUCESSO', 'Sucesso');
        $elaboracao = $this->getCodigoId('3', 'Em elaboração');
        $ativa = $this->getCodigoId('7', 'Ativa');

        /* @var Arp[] $arps */
        $arps = Arp::join('envia_dados_pncp as e', function ($join) {
                $join->on('e.pncpable_type', '=', DB::raw("'App\\Models\\Arp'"))
                    ->on('e.pncpable_id', '=', 'arp.id');
        })
            ->where('arp.rascunho', false)
            ->whereNotNull('arp.vigencia_final')
            ->whereNotNull('arp.vigencia_inicial')
            ->where('arp.tipo_id', $elaboracao)
            ->where('e.situacao', $success)
            ->get();

        foreach ($arps as $arp) {
            $arp->tipo_id = $ativa;
            $arp->save();
        }
    }

    private function getCodigoId($descres, $description)
    {
        $query = CodigoItem::query();
        $query->where('descres', '=', $descres)
        ->where('descricao', '=', $description);
        return $query->first()->id;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
