<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUnidadeGerenciadoraArpAdesao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->integer('unidade_gerenciadora_id')->nullable();
            $table->foreign('unidade_gerenciadora_id')->references('id')->on('unidades');
        });

        Schema::table('arp', function (Blueprint $table) {
            $table->decimal('valor_total',20,2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
