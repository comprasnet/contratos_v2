<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableAdesaoAddColumnCienteEntreProjetoFederal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->boolean('ciente_analise_entre_projeto_federal')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->dropColumn('ciente_analise_entre_projeto_federal');
        });
    }
}
