<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAlteracaoArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_historico', function (Blueprint $table) {
            // $table->integer('modalidade_id');
            // $table->text('objeto');
            // $table->boolean('compra_centralizada');

            $table->date('data_assinatura_alteracao_vigencia')->nullable();
            $table->date('data_assinatura_alteracao_inicio_vigencia')->nullable();
            $table->date('data_assinatura_alteracao_fim_vigencia')->nullable();
            $table->text('arquivo_alteracao')->nullable();
            // $table->dropColumn('aceita_adesao');

            // $table->foreign('modalidade_id')->references('id')->on('codigoitens');
        });


        // Schema::create('arp_item_historico', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('arp_historico_id');
        //     $table->integer('arp_item_id');
            
        //     $table->decimal('valor', 15,2)->nullable();

        //     $table->date('vigencia_inicial')->nullable();
        //     $table->date('vigencia_final')->nullable();
        //     $table->boolean('item_cancelado')->default(false);
        //     $table->boolean('valor_alterado')->default(false);
        //     $table->timestamps();

        //     $table->foreign('arp_historico_id')->references('id')->on('arp_historico');
        //     $table->foreign('arp_item_id')->references('id')->on('arp_item');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arp_item_historico');

        Schema::table('arp_historico', function (Blueprint $table) {
            // $table->dropColumn('modalidade_id');
            // $table->dropColumn('objeto');

            // $table->dropColumn('compra_centralizada');
            $table->dropColumn('data_assinatura_alteracao_vigencia');
            $table->dropColumn('data_assinatura_alteracao_inicio_vigencia');
            $table->dropColumn('data_assinatura_alteracao_fim_vigencia');
            $table->dropColumn('arquivo_alteracao');
        });
    }
}
