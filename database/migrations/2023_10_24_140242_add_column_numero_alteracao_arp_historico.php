<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnNumeroAlteracaoArpHistorico extends Migration
{
    public function up()
    {
        Schema::table('arp_historico', function (Blueprint $table) {
            $table->string('numero_alteracao', 50)->nullable();
        });
    }

    public function down()
    {
        Schema::table('arp_historico', function (Blueprint $table) {
            $table->dropColumn('numero_alteracao');
        });
    }
}
