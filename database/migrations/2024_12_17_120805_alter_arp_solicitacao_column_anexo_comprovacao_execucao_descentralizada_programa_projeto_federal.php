<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterArpSolicitacaoColumnAnexoComprovacaoExecucaoDescentralizadaProgramaProjetoFederal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->text('anexo_comprovacao_execucao_descentralizada_programa_projeto_federal')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->dropColumn('anexo_comprovacao_execucao_descentralizada_programa_projeto_federal');
        });
    }
}
