<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutorizacaoexecucaoUnidadeRequisitantesHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autorizacaoexecucao_unidade_requisitantes_historico', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('autorizacaoexecucao_historico_id');
            $table->unsignedInteger('unidade_requisitante_id');
            $table->enum('tipo_historico', ['antes', 'depois']);
            $table->timestamps();

            $table->foreign('autorizacaoexecucao_historico_id')
                ->references('id')
                ->on('autorizacaoexecucao_historico')
                ->onDelete('cascade');
            $table->foreign('unidade_requisitante_id')
                ->references('id')
                ->on('unidades')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autorizacaoexecucao_unidade_requisitantes_historico');
    }
}
