<?php

use App\Models\AutorizacaoExecucao;
use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnSituacaoIdToAutorizacaoexecucoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::create([
            'descricao' => 'Situação Autorização Execução',
            'visivel' => true,
        ]);

        $codigoItemElaboracao = CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Em elaboração',
            'visivel' => true,
            'descres' => 'ae_status_1'
        ]);

        $codigoItemAtiva = CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Ativa',
            'visivel' => true,
            'descres' => 'ae_status_2'
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Extinta',
            'visivel' => true,
            'descres' => 'ae_status_3'
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Finalizada',
            'visivel' => true,
            'descres' => 'ae_status_4'
        ]);

        Schema::table('autorizacaoexecucoes', function (Blueprint $table) {
            $table->unsignedInteger('situacao_id')->nullable();

            $table->foreign('situacao_id')
                ->references('id')
                ->on('codigoitens')
                ->onDelete('cascade');
        });


        AutorizacaoExecucao::withTrashed()->where('rascunho', true)->update([
            'situacao_id' => $codigoItemElaboracao->id
        ]);

        AutorizacaoExecucao::withTrashed()->where('rascunho', false)->update([
            'situacao_id' => $codigoItemAtiva->id
        ]);

        Schema::table('autorizacaoexecucoes', function (Blueprint $table) {
            $table->unsignedInteger('situacao_id')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucoes', function (Blueprint $table) {
            $table->dropForeign('autorizacaoexecucoes_situacao_id_foreign');
            $table->dropColumn('situacao_id');
        });

        $codigo = Codigo::where('descricao', 'Situação Autorização Execução')->first();
        $codigo->forceDelete();
    }
}
