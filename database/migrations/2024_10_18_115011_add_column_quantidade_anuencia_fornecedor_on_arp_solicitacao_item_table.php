<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnQuantidadeAnuenciaFornecedorOnArpSolicitacaoItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_solicitacao_item', function (Blueprint $table) {
            $table->decimal('quantidade_anuencia_fornecedor', 15, 5)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_solicitacao_item', function (Blueprint $table) {
            $table->dropColumn('quantidade_anuencia_fornecedor');
        });
    }
}
