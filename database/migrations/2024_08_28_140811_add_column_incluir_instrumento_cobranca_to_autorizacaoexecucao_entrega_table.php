<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIncluirInstrumentoCobrancaToAutorizacaoexecucaoEntregaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->boolean('incluir_instrumento_cobranca')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->dropColumn('incluir_instrumento_cobranca');
        });
    }
}
