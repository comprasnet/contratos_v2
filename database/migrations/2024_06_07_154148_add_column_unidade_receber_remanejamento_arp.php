<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUnidadeReceberRemanejamentoArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_remanejamento_itens', function (Blueprint $table) {
            $table->integer('id_item_unidade_receber')->nullable();
            $table->foreign('id_item_unidade_receber')->references('id')->on('compra_item_unidade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_remanejamento_itens', function (Blueprint $table) {
            $table->dropColumn('id_item_unidade_receber');
        });
    }
}
