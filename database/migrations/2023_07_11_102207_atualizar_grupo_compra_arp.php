<?php

use App\Api\Externa\ApiSiasg;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\LogTrait;
use App\Models\CompraItem;
use App\Models\Compras;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class AtualizarGrupoCompraArp extends Migration
{
    use CompraTrait;
    use LogTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        # Recupera as compras que a coluna grupo compra da tabela compra items não está preenchida
        $itemSemGrupo = CompraItem::join("compras", "compras.id", "compra_items.compra_id")
                            ->join("compra_item_fornecedor", "compra_item_fornecedor.compra_item_id", "compra_items.id")
                            ->join("arp_item", "compra_item_fornecedor.id", "arp_item.compra_item_fornecedor_id")
                            ->whereNull("compra_items.grupo_compra")
                            ->get();

        # Inicia a utlização para consumir a API do SIASG
        $consultaCompra = new ApiSiasg();
        # Retorna a URL principal do COMPRAS
        $urlPrincipal = config('api.siasg.url');
        # Retorna a URI do SISRP complementar o link
        $uri = $consultaCompra->retornaServicoEspecifico('RETORNAITEMSISRP');
        DB::beginTransaction();
        try {
            foreach ($itemSemGrupo as $item) {
                # Concatena a URL + URI
                $urlCompraSisrp = $urlPrincipal.$uri;
                # Monta a QUERY da URL para poder buscar no serviço do Compras
                $queryUrl = $this->montarQueryUrlItem(
                    $item->compra->numero_ano,
                    $item->compra->unidadeOrigem->codigo,
                    $item->compra->unidadeOrigem->codigo,
                    $item->compra->modalidade->descres,
                    $item->numero,
                    'G'
                );

                # Inclui a query na URL principal
                $urlCompraSisrp .= $queryUrl;

                $dadosItemCompra = $consultaCompra->consultaCompraByUrl($urlCompraSisrp);

                # Se não existir o registro obtido no serviço, então pula o item
                if (empty($dadosItemCompra['data'])) {
                    continue;
                }

                # Se não existir o atributo grupo  da compra obtido no serviço, então pula o item
                if (!isset($dadosItemCompra['data']['dadosAta']['grupoCompra'])) {
                    continue;
                }

                # Altera o grupo da compra de nulo para o que veio no serviço
                $itemAtualizado = CompraItem::find($item->compra_item_id);

                # Insere o grupo da compra recuperada no serviço
                $itemAtualizado->grupo_compra = (string) $dadosItemCompra['data']['dadosAta']['grupoCompra'];

                # Salva o registro
                $itemAtualizado->save();
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();

            # Caso aconteça erro, insere no LOG
            $this->inserirLogCustomizado('gestao_ata', 'error', $ex);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
