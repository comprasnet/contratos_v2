<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutorizacaoexecucaoUnidadeRequisitantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autorizacaoexecucao_unidade_requisitantes', function (Blueprint $table) {
            $table->integer('autorizacaoexecucoes_id');
            $table->integer('unidade_requisitante_id');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('autorizacaoexecucoes_id')->references('id')->on('autorizacaoexecucoes')->onDelete('cascade');
            $table->foreign('unidade_requisitante_id')->references('id')->on('unidades')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autorizacaoexecucao_unidade_requisitantes');
    }
}
