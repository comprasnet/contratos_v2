<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixTabelaArpAlteracao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arp_alteracao', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('arp_id');
            $table->boolean('rascunho');
            $table->integer('sequencial');
            $table->timestamps();

            $table->foreign('arp_id')->references('id')->on('arp');
        });

        Schema::table('arp_historico', function (Blueprint $table) {
            $table->integer('arp_alteracao_id')->nullable();

            $table->foreign('arp_alteracao_id')->references('id')->on('arp_alteracao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arp_alteracao');

        Schema::table('arp_historico', function (Blueprint $table) {
            $table->dropColumn('arp_alteracao_id');
        });
    }
}
