<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Arp;
use App\Http\Traits\Arp\ArpItemTrait;
use App\Models\CompraItem;



class UpdateVigenciaCompraItemFornecedor extends Migration
{
    use ArpItemTrait;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Iniciar a transação
        DB::beginTransaction();

        try {
            // Recuperar os dados da tabela arp que não são rascunho
            $arps = Arp::where('rascunho', false)->get();


            // Iterar sobre os dados da tabela arp
            foreach ($arps as $arp) {
                $this->inserirVigenciaItem($arp);

                if(empty($arp->item)) {
                    continue;
                }
                foreach ($arp->item as $item){
                    $item->item_fornecedor_compra->compraItens->ata_vigencia_inicio = null;
                    $item->item_fornecedor_compra->compraItens->ata_vigencia_fim = null;
                    $item->item_fornecedor_compra->compraItens->save();
                }

            }

            // Confirmar a transação se todas as operações forem bem-sucedidas
            DB::commit();
        } catch (\Exception $e) {
            // Caso ocorra uma exceção, desfazer a transação e lançar a exceção novamente
            DB::rollBack();
            Log::error($e->getMessage());
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //

    }
}