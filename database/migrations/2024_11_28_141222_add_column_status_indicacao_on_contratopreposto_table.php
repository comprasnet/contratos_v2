<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnStatusIndicacaoOnContratoprepostoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contratopreposto', function (Blueprint $table) {
            if (!Schema::hasColumn('contratopreposto', 'status_indicacao_id')) {
                $table->integer('status_indicacao_id')->unsigned()->nullable();
                $table->foreign('status_indicacao_id', 'fk_status_indicacao_preposto')
                    ->references('id')->on('codigoitens')
                    ->onDelete('set null');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contratopreposto', function (Blueprint $table) {
            if (Schema::hasColumn('contratopreposto', 'status_indicacao_id')) {
                $table->dropForeign('fk_status_indicacao_preposto');
                $table->dropColumn('status_indicacao_id');
            }
        });
    }
}
