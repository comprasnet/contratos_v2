<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class FixForeignKeyTermoRecebimentoProvisorioItens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('termo_recebimento_provisorio_itens', function (Blueprint $table) {
            $table->foreign('termo_recebimento_provisorio_id')
                ->references('id')
                ->on('termos_recebimento_provisorio')
                ->onDelete('cascade');

            $table->foreign('entrega_item_id')
                ->references('id')
                ->on('entrega_itens')
                ->onDelete('cascade');
        });

        Schema::table('models_signatarios', function (Blueprint $table) {
            if (!DB::table('information_schema.table_constraints')
                ->where('constraint_name', 'models_signatarios_model_termo_recebimento_provisorio_id_foreign')
                ->exists()) {
                $table->foreign('model_termo_recebimento_provisorio_id')
                    ->references('id')
                    ->on('termos_recebimento_provisorio')
                    ->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('termo_recebimento_provisorio_itens', function (Blueprint $table) {
            $table->dropForeign(['termo_recebimento_provisorio_id']);
            $table->dropForeign(['entrega_item_id']);
        });

        Schema::table('models_signatarios', function (Blueprint $table) {
            $table->dropForeign(['model_termo_recebimento_provisorio_id']);
        });
    }
}
