<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutorizacaoexecucaoSignatariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::create([
            'descricao' => 'Status Assinatura',
            'visivel' => true,
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Aguardando',
            'descres' => 'status_assinatura',
            'visivel' => true,
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Assinado',
            'descres' => 'status_assinatura',
            'visivel' => true,
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Recusado',
            'descres' => 'status_assinatura',
            'visivel' => true,
        ]);

        Schema::create('autorizacaoexecucao_signatarios', function (Blueprint $table) {
            $table->id();
            $table->integer('autorizacaoexecucao_id');
            $table->string('signatario_type');
            $table->integer('signatario_id');
            $table->integer('arquivo_generico_id');
            $table->integer('status_assinatura_id');
            $table->float('posicao_x_assinatura');
            $table->float('posicao_y_assinatura');
            $table->integer('pagina_assinatura');
            $table->dateTime('data_operacao')->nullable();
            $table->timestamps();

            $table->unique(['autorizacaoexecucao_id', 'signatario_type', 'signatario_id']);
            $table->foreign('autorizacaoexecucao_id')
                ->references('id')
                ->on('autorizacaoexecucoes')
                ->onDelete('cascade');
            $table->foreign('status_assinatura_id')
                ->references('id')
                ->on('codigoitens')
                ->onDelete('cascade');
            $table->foreign('arquivo_generico_id')
                ->references('id')
                ->on('arquivo_generico')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autorizacaoexecucao_signatarios');

        Codigo::where('descricao', 'Status Assinatura')->forceDelete();
    }
}
