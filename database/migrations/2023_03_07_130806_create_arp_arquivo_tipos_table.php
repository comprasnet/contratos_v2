<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArpArquivoTiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arp_arquivo_tipos', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->timestamps();
        });

        \App\Models\ArpArquivoTipo::create([
            'nome' => 'Ata de Registro de Preços',
        ]);

        \App\Models\ArpArquivoTipo::create([
            'nome' => 'Termo Aditivo',
        ]);

        \App\Models\ArpArquivoTipo::create([
            'nome' => 'Termo de Apostilamento',
        ]);

        \App\Models\ArpArquivoTipo::create([
            'nome' => 'Termo de Extinção',
        ]);

        \App\Models\ArpArquivoTipo::create([
            'nome' => 'Outros',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arp_arquivo_tipos');
    }
}
