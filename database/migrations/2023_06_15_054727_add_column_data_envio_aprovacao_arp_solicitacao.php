<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDataEnvioAprovacaoArpSolicitacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->dateTime('data_envio_analise')->nullable();
            $table->dateTime('data_aprovacao_analise')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->dropColumn(['data_envio_analise', 'data_aprovacao_analise']);
        });
    }
}
