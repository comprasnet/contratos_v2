<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class FixSituacaoAutorizacaoExecucao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // ISSE 340 item 13
        DB::table('codigoitens')->where('descres', 'ae_status_2')->update(['descricao' => 'Em execução']);
        DB::table('codigoitens')->where('descres', 'ae_status_4')->update(['descricao' => 'Concluído']);

        DB::table('codigoitens')->where('descres', 'AUTOFORN')->update(['descricao' => 'Ordem de Fornecimento']);
        DB::table('codigoitens')->where('descres', 'SERVAUTORIZACAOFORN')
            ->update(['descricao' => 'Ordem de Serviço / Ordem de Fornecimento']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('codigoitens')->where('descres', 'ae_status_2')->update(['descricao' => 'Ativa']);
        DB::table('codigoitens')->where('descres', 'ae_status_4')->update(['descricao' => 'Finalizada']);

        DB::table('codigoitens')->where('descres', 'AUTOFORN')->update(['descricao' => 'Autorização de Fornecimento']);
        DB::table('codigoitens')->where('descres', 'SERVAUTORIZACAOFORN')
            ->update(['descricao' => 'Ordem de Serviço / Autorização de Fornecimento']);
    }
}
