<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCancelarAdesao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->string('justificativa_cancelamento')->nullable();
            $table->integer('unidade_id_cancelamento')->nullable();
            $table->integer('usuario_id_cancelamento')->nullable();
            $table->dateTime('data_cancelamento')->nullable();
            
            $table->foreign('unidade_id_cancelamento')->references('id')
                ->on('unidades')->onDelete('cascade');
            
            $table->foreign('usuario_id_cancelamento')->references('id')
                ->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->dropColumn([
                'justificativa_cancelamento',
                'unidade_id_cancelamento',
                'usuario_id_cancelamento',
                'data_cancelamento'
            ]);
        });
    }
}
