<?php

use App\Models\AutorizacaoexecucaoHistorico;
use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToAutorizacaoexecucaoHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Situação Autorização Execução')->first();

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_status_7',
            'descricao' => 'Assinada',
            'visivel' => true
        ]);

        Schema::table('autorizacaoexecucao_historico', function (Blueprint $table) {
            $table->unsignedInteger('situacao_id')->nullable();
            $table->date('data_inicio_alteracao')->nullable();

            $table->foreign('situacao_id')
                ->references('id')
                ->on('codigoitens')
                ->onDelete('cascade');
        });


        $situacaoEmElaboracao = CodigoItem::where('descres', 'ae_status_1')->first();
        $situacaoEmExecucao = CodigoItem::where('descres', 'ae_status_2')->first();

        $tipoHistoricoAlterar = CodigoItem::where('descres', 'aeh_alterar')->first();
        AutorizacaoexecucaoHistorico::where('tipo_historico_id', $tipoHistoricoAlterar->id)
            ->where('rascunho', true)
            ->update(['situacao_id' => $situacaoEmElaboracao->id]);


        AutorizacaoexecucaoHistorico::where('tipo_historico_id', $tipoHistoricoAlterar->id)
            ->where('rascunho', false)
            ->update(['situacao_id' => $situacaoEmExecucao->id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_historico', function (Blueprint $table) {
            $table->dropColumn('data_inicio_alteracao');
            $table->dropColumn('situacao_id');
        });

        CodigoItem::where('descres', 'ae_status_7')->forceDelete();
    }
}
