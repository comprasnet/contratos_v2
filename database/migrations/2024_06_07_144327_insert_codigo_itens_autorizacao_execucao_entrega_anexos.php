<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertCodigoItensAutorizacaoExecucaoEntregaAnexos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::create([
            'descricao' => 'Tipo Arquivos Autorização Execução Entrega',
            'visivel' => true,
        ]);


        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_entrega_arquivo',
            'descricao' => 'Anexos da entrega - OS/F',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigo::where('descricao', 'Tipo Arquivos Autorização Execução Entrega')->forceDelete();
    }
}
