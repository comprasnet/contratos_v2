<?php

use App\Http\Traits\Pncp\ArpPncpTrait;
use App\Services\Pncp\Arp\InserirArquivoArpPncpService;
use App\Services\Pncp\PncpManagerService;
use App\Services\Pncp\PncpService;
use App\Services\Pncp\Responses\PncpPostResponseService;
use Illuminate\Database\Migrations\Migration;
use \App\Models\EnviaDadosPncp;
use \App\Models\Arp;
use Illuminate\Support\Facades\Log;

class FixSequencialPncpAtaDeRegistroDePreco extends Migration
{
    use ArpPncpTrait;
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $atasSemSequencialPncp =
            EnviaDadosPncp::whereHasMorph('pncpable', [Arp::class], function ($query) {
                $query->whereNotNull('link_pncp')
                ->whereNull('sequencialPNCP');
            })->get();
        
        $pncpService = new PncpService();
        
        $urlPncp = config('api.pncp.base_uri');
        $header = ['Content-Type' => 'application/json'];
        $stack = $pncpService->addPnpcAuthenticationMiddleware();

        foreach ($atasSemSequencialPncp as $SemSequencialPncp) {
            $sequencialSalvo = false;
            
            try {
                $linkPncp = explode('/', $SemSequencialPncp->link_pncp);
                
                $parametros = [
                    'cnpj' => $linkPncp[6],
                    'compras' => $linkPncp[8],
                    'sequencialCompra' => $linkPncp[9],
                    'sequencialAta' =>  $linkPncp[11],
                ];
                
                $endpoint = $pncpService->resolveEndpoint('recuperar_ata', $parametros);
                
                $response = $pncpService->makeRequest(
                    'GET',
                    $urlPncp,
                    $endpoint,
                    $stack,
                    $header,
                    null,
                    []
                );
                $dataAPI = json_decode($response->getBody()->getContents(), true);
                $sequencialPncp = $dataAPI['numeroControlePNCP'];
                $sequencialSalvo = true;
            } catch (Exception $e) {
                $sequencialPncp = $e->getMessage();
            }
            
            $SemSequencialPncp->sequencialPNCP = $sequencialPncp;
            $SemSequencialPncp->save();
            
            if ($sequencialSalvo) {
                try {
                    $pncpManagerService = new PncpManagerService();
                    $arp = Arp::find($SemSequencialPncp->pncpable_id);
                    $this->publicarArquivoPNCP(
                        $arp,
                        $pncpManagerService,
                        new InserirArquivoArpPncpService(),
                        new PncpPostResponseService()
                    );
                } catch (Exception $exception) {
                    Log::error("Erro ao enviar o arquivo: ". $exception->getMessage());
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
