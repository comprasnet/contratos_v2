<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterarDescricaoSituacaoArpRemanejamento extends Migration
{
    /**
     * Método responsável em recuperar o status atual para atualizar
     */
    private function recuperarStatus(string $descricao, string $descres = "status_remanejamento")
    {
        # Recuperar o status que vai ser alterado
        return CodigoItem::where("descres", $descres)
                            ->where("descricao", $descricao)
                            ->first();
    }

    /**
     * Método responsável em atualizar a informação no banco de dados
     */
    private function atualizarStatus(CodigoItem $statusAlteracao, string $novoStatus, ?string $novoDescres = null)
    {
        $statusAlteracao->descricao = $novoStatus;

        if (!empty($novoDescres)) {
            $statusAlteracao->descres = $novoDescres;
        }

        $statusAlteracao->save();
    }

    /**
     * Método responsável em inserir os novos status
     */
    private function inserirStatus(int $codigoId, string $descricao, string $descres = 'status_remanejamento')
    {
        CodigoItem::updateOrCreate(
            [
                'codigo_id' => $codigoId,
                'descres' => $descres,
                'descricao' => $descricao
            ]
        );
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Tipo de Ata de Registro de Preços')->first();

        # Inclusão do status Aguardando Aceitação da Unidade de Origem e Gerenciadora
        $this->inserirStatus($codigo->id, 'Aguardando Aceitação da Unidade de Origem e Gerenciadora');

        # Inclusão do status Cancelado Parcialmente
        $this->inserirStatus($codigo->id, 'Cancelado Parcialmente');


        # Inclusão do status Cancelado Parcialmente
        $this->inserirStatus($codigo->id, 'Item Cancelado', 'status_item');

        # Alteração do status Negada
//        $statusAlteracao = $this->recuperarStatus('Aceita Parcialmente');
//        $this->atualizarStatus($statusAlteracao, 'Analisado pela Unidade Gerenciadora da ata');

        # Alteração do status Estornada
        $statusAlteracaoEstornada = $this->recuperarStatus('Estornada', '5');

        $this->atualizarStatus($statusAlteracaoEstornada, 'Item Estornado', 'status_item');
    }

    /**
     * Método responsável em deletar o registro da codigo itens para o status do remanejamento
     */
    private function deletarStatusCodigoItem(int $codigoId, string $descricao, string $descres = 'status_remanejamento')
    {
        CodigoItem::where("codigo_id", $codigoId)
            ->where('descricao', $descricao)
            ->where('descres', $descres)
            ->forceDelete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = Codigo::where('descricao', 'Tipo de Ata de Registro de Preços')->first();

        $this->deletarStatusCodigoItem($codigo->id, 'Aguardando Aceitação da Unidade de Origem e Gerenciadora');

        $this->deletarStatusCodigoItem($codigo->id, 'Cancelado Parcialmente');

        $this->deletarStatusCodigoItem($codigo->id, 'Item Cancelado', 'status_item');

        # Alteração do status Analisado pela Unidade Gerenciadora da ata
        $statusAlteracao = $this->recuperarStatus('Analisado pela Unidade Gerenciadora da ata');
        $this->atualizarStatus($statusAlteracao, 'Aceita Parcialmente');

        # Alteração do status Estornada
        $statusAlteracaoEstornada = $this->recuperarStatus('Item Estornado', 'status_item');
        $this->atualizarStatus($statusAlteracaoEstornada, 'Estornada', '5');
    }
}
