<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutorizacaoexecucaoEmpenhosHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autorizacaoexecucao_empenhos_historico', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('autorizacaoexecucao_historico_id');
            $table->unsignedInteger('empenho_id');
            $table->enum('tipo_historico', ['antes', 'depois']);
            $table->timestamps();

            $table->foreign('autorizacaoexecucao_historico_id')
                ->references('id')
                ->on('autorizacaoexecucao_historico')
                ->onDelete('cascade');
            $table->foreign('empenho_id')
                ->references('id')
                ->on('empenhos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autorizacaoexecucao_empenhos_historico');
    }
}
