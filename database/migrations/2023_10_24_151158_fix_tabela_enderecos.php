<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixTabelaEnderecos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enderecos', function (Blueprint $table) {
            $table->string('bairro')->nullable()->change();
            $table->string('logradouro')->nullable()->change();
            $table->string('cep')->nullable()->change();
            $table->string('codigo_municipio_ndc')->nullable()->comment('Opcional. vem da api do ndc.');
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enderecos', function (Blueprint $table) {
            $table->string('bairro')->change();
            $table->string('logradouro')->change();
            $table->string('cep')->change();
            $table->dropColumn('codigo_municipio_ndc');
        });
    }
}
