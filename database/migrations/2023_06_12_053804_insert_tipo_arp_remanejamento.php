<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertTipoArpRemanejamento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        # Recupera os status
        $todosStatus = config('arp.arp.status_remanejamento');
        $codigo = Codigo::where('descricao', 'Tipo de Ata de Registro de Preços')->first();

        foreach ($todosStatus as $status) {
            CodigoItem::updateOrInsert(
                [
                    'codigo_id' => $codigo->id,
                    'descres' => 'status_remanejamento',
                    'descricao' => $status
                ]
            );
        }

        # Insere o código do item para o tipo de arquivo do remanejamento
        CodigoItem::updateOrInsert(
            [
                'codigo_id' => $codigo->id,
                'descres' => 'arq_remanejamento',
                'descricao' => 'Arquivo Remanejamento'
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
