<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use App\Models\Role;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAtaRegistroPreco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arp', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidade_origem_id');
            $table->integer('compra_id');
            $table->integer('tipo_id');
            $table->integer('modalidade_id');
            $table->string('numero',50);
            $table->integer('ano');
            $table->date('data_assinatura');
            $table->date('vigencia_inicial');
            $table->date('vigencia_final');
            $table->decimal('valor_total',11,2);
            $table->boolean('rascunho');
            $table->timestamps();

            $table->foreign('unidade_origem_id')->references('id')->on('unidades');
            $table->foreign('compra_id')->references('id')->on('compras');
            $table->foreign('tipo_id')->references('id')->on('codigoitens');
            $table->foreign('modalidade_id')->references('id')->on('codigoitens');
        });

        Schema::create('arp_historico', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('arp_id');
            $table->integer('unidade_origem_id');
            $table->integer('compra_id');
            $table->integer('tipo_id');
            $table->string('numero',50);
            $table->integer('ano');
            $table->date('data_assinatura');
            $table->date('vigencia_inicial');
            $table->date('vigencia_final');
            $table->decimal('valor_total',11,2);
            $table->boolean('aceita_adesao');
            $table->timestamps();

            $table->foreign('arp_id')->references('id')->on('arp');
            $table->foreign('unidade_origem_id')->references('id')->on('unidades');
            $table->foreign('compra_id')->references('id')->on('compras');
            $table->foreign('tipo_id')->references('id')->on('codigoitens');
        });

        Schema::create('arp_item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('arp_id');
            $table->integer('compra_item_fornecedor_id');
            $table->decimal('quantidade_max_adesao',15,5);
            $table->boolean('aceita_adesao');
            $table->timestamps();

            $table->foreign('arp_id')->references('id')->on('arp');
            $table->foreign('compra_item_fornecedor_id')->references('id')->on('compra_item_fornecedor');
        });

        Schema::create('arp_unidades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('arp_item_id');
            $table->integer('unidade_id');
            $table->string('tipo_unidade',1);
            $table->decimal('quantidade_autorizada',15,5);
            $table->decimal('quantidade_saldo',15,5);
            $table->decimal('valor_autorizado',15,5);
            $table->timestamps();
            
            $table->foreign('arp_item_id')->references('id')->on('arp_item');
            $table->foreign('unidade_id')->references('id')->on('unidades');
        });

        Schema::create('arp_gestor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('arp_id');
            $table->integer('user_id');
            $table->timestamps();
            
            $table->foreign('arp_id')->references('id')->on('arp');
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('arp_autoridade_signataria', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('arp_id');
            $table->integer('autoridade_signataria_id');
            $table->timestamps();
            
            $table->foreign('arp_id')->references('id')->on('arp');
            $table->foreign('autoridade_signataria_id')->references('id')->on('autoridadesignataria');
        });

        $idCodigo =  Codigo::updateOrCreate(['descricao' => 'Tipo de Ata de Registro de Preços','visivel' => true])->id;

        $tiposArp = config('arp.arp.tipo_inicial');
        foreach($tiposArp as $key => $tipo) {
            CodigoItem::updateOrCreate(['codigo_id'=>$idCodigo, 'descres' => $key, 'descricao' => $tipo, 'visivel' =>true]);
        }

        Role::updateOrCreate(['name' => 'Gestor de ARP' , 'guard_name' => 'web']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arp');
        Schema::dropIfExists('arp_item');
        Schema::dropIfExists('arp_unidades');
        Schema::dropIfExists('arp_historico');
        Schema::dropIfExists('arp_gestor');
        Schema::dropIfExists('arp_autoridade_signataria');
    }
}
