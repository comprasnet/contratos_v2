<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\CompraItemUnidade;
use \Illuminate\Support\Facades\DB;
use \App\Models\ArpRemanejamentoItem;
use Illuminate\Support\Facades\Log;

class IncluirIdItemUnidadeReceberRemanejamento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ItensRemanejamentoAtualizar = CompraItemUnidade::join(
            'arp_remanejamento_itens',
            'compra_item_unidade.id',
            'arp_remanejamento_itens.id_item_unidade'
        )
        ->join('arp_remanejamento', 'arp_remanejamento_itens.remanejamento_id', '=', 'arp_remanejamento.id')
        ->where('compra_item_unidade.situacao', true)
        ->orderBy('arp_remanejamento.id')
        ->select([
            'arp_remanejamento.id as idRemanejamento',
            'arp_remanejamento_itens.id as idRemanejamentoItem',
            DB::raw('(select ciuSolicitante.id
              from compra_item_unidade as ciuSolicitante
              where ciuSolicitante.unidade_id = arp_remanejamento.unidade_solicitante_id
              and ciuSolicitante.compra_item_id = compra_item_unidade.compra_item_id
              and ciuSolicitante.situacao = true
             ) as idCIUOrigemReceber')
        ])
        ->get();
        try {
            DB::beginTransaction();
            foreach ($ItensRemanejamentoAtualizar as $item) {
                $itemRemanejamento = ArpRemanejamentoItem::find($item->idRemanejamentoItem);
                if (empty($item->idciuorigemreceber)) {
                    continue;
                }
                
                $itemRemanejamento->id_item_unidade_receber = $item->idciuorigemreceber;
                $itemRemanejamento->save();
                $mensagem = "IdItemRemanejamento alterado: {$item->idRemanejamentoItem}";
                Log::info($mensagem);
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error('Erro ao executar a migration para atualizar o compra item unidade
             para quem vai receber o remanejamento');
            Log::error($ex);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
