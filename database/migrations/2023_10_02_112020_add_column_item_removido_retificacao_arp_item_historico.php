<?php

use App\Models\ArpItemHistorico;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnItemRemovidoRetificacaoArpItemHistorico extends Migration
{
    /**
     * Run the migrations.
     * Cria coluna item_removido_retificacao para indicar no histórico
     * que o item foi removido um processo de retiricação
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_item_historico', function (Blueprint $table) {
            $table->boolean('item_removido_retificacao')->default(false)->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_item_historico', function (Blueprint $table) {
            $table->dropColumn('item_removido_retificacao');
        });
    }
}
