<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRnovoGestaoAtaCompraItemUnidade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compra_item_fornecedor', function (Blueprint $table) {
            $table->boolean('novo_gestao_ata')->default(false);
        });
        Schema::table('compra_item_unidade', function (Blueprint $table) {
            $table->boolean('novo_gestao_ata')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compra_item_fornecedor', function (Blueprint $table) {
            $table->dropColumn('novo_gestao_ata');
        });
        Schema::table('compra_item_unidade', function (Blueprint $table) {
            $table->dropColumn('novo_gestao_ata');
        });
    }
}
