<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnResponsavelAdesaoArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('arp_solicitacao_item', 'responsavel_id')){
            Schema::table('arp_solicitacao_item', function (Blueprint $table) {
                $table->integer('responsavel_id')->nullable();
                $table->foreign('responsavel_id')->references('id')->on('users');
            });
        }

        if(!Schema::hasColumn('arp_solicitacao', 'responsavel_id')){
            Schema::table('arp_solicitacao', function (Blueprint $table) {
                $table->integer('responsavel_id')->nullable();
                $table->foreign('responsavel_id')->references('id')->on('users');
            });
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_solicitacao_item', function (Blueprint $table) {
            $table->dropColumn('responsavel_id');
        });
        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->dropColumn('responsavel_id');
        });
    }
}
