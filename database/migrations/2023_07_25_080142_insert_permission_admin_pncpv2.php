<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class InsertPermissionAdminPncpv2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        Permission::create(['name' => 'admin_pncpv2_visualizar', 'guard_name' => 'web']);
        Permission::create(['name' => 'admin_pncpv2_reenviar', 'guard_name' => 'web']);
        //dd('foo');

        $role = Role::where(['name' => 'Administrador'])->first();
        $role->givePermissionTo('admin_pncpv2_visualizar');
        $role->givePermissionTo('admin_pncpv2_reenviar');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('admin_pncpv2_visualizar');
        $role->revokePermissionTo('admin_pncpv2_reenviar');


        Permission::where(['name' => 'admin_pncpv2_visualizar'])->delete();
        Permission::where(['name' => 'admin_pncpv2_reenviar'])->delete();
    }
}
