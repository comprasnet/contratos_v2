<?php

use App\Models\UnidadeMedida as UnidadeMedidaModel;
use App\Repositories\UnidadeMedidaRepository;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPdmsInsert extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unidade_medida', function (Blueprint $table){
            $table->integer('codigo_pdm')->nullable();
            $table->integer('codigo_siasg')->nullable()->change();
        });
        $this->addMateriais();
        $this->addServicos();
    }

    public function addMateriais(){
        $file = file_get_contents(database_path('data/lista_pdms_unidade_fornecimento.json'));
        $data = json_decode($file);
        foreach ($data as $pdmItem) {
            $unidadeMedida = new UnidadeMedidaModel();
            $unidadeMedida->sigla = $pdmItem->sigla_unidade_fornecimento;
            $unidadeMedida->nome = $pdmItem->nome_unidade_fornecimento;
            $unidadeMedida->codigo_pdm = $pdmItem->codigo_pdm;
            $unidadeMedida->tipo = UnidadeMedidaRepository::TYPE_MATERIAL;
            $unidadeMedida->save();
        }
    }

    public function addServicos(){
        $file = file_get_contents(database_path('data/lista_servicos_unidade_medida.json'));
        $data = json_decode($file);
        foreach ($data as $pdmItem) {
            $unidadeMedida = new UnidadeMedidaModel();
            $unidadeMedida->sigla = $pdmItem->sigla_unidade_medida;
            $unidadeMedida->nome = $pdmItem->nome_unidade_medida;
            $unidadeMedida->codigo_siasg = $pdmItem->codigo_servico;
            $unidadeMedida->tipo = UnidadeMedidaRepository::TYPE_SERVICO;
            $unidadeMedida->save();
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}