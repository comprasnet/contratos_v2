<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnVigenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucoes', function (Blueprint $table) {
            $table->text('data_vigencia_inicio')->nullable();
            $table->text('data_vigencia_fim')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucoes', function (Blueprint $table) {
            $table->dropColumn('data_vigencia_fim');
            $table->dropColumn('data_vigencia_inicio');
        });
    }
}
