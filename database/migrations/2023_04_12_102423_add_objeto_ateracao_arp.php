<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddObjetoAteracaoArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_historico', function (Blueprint $table) {
            if (!Schema::hasColumn('arp_historico', 'objeto_alteracao')) {
                $table->text('objeto_alteracao')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_historico', function (Blueprint $table) {
            if (Schema::hasColumn('arp_historico', 'objeto_alteracao')) {
                $table->dropColumn('objeto_alteracao');
            }
        });
    }
}
