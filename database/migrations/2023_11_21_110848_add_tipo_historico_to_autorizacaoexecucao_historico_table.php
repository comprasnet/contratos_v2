<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTipoHistoricoToAutorizacaoexecucaoHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::create([
            'descricao' => 'Tipo Autorização Execução Histórico',
            'visivel' => true,
        ]);

        $codigoItem = CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Retificar',
            'visivel' => true,
            'descres' => 'aeh_retificar'
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Alterar',
            'visivel' => true,
            'descres' => 'aeh_alterar'
        ]);

        Schema::table('autorizacaoexecucao_historico', function (Blueprint $table) use ($codigoItem) {
            $table->unsignedInteger('tipo_historico_id')->default($codigoItem->id);
            $table->text('tipo_alteracao')->nullable();
            $table->unsignedInteger('sequencial')->nullable();
            $table->date('data_assinatura_alteracao')->nullable();
            $table->date('data_extincao')->nullable();
            $table->boolean('rascunho')->default(false);

            $table->foreign('tipo_historico_id')
                ->references('id')
                ->on('codigoitens')
                ->cascadeOnDelete();
        });

        Schema::table('autorizacaoexecucao_historico', function (Blueprint $table) {
            $table->unsignedInteger('tipo_historico_id')->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_historico', function (Blueprint $table) {
            $table->dropColumn('tipo_historico_id');
            $table->dropColumn('tipo_alteracao');
            $table->dropColumn('sequencial');
            $table->dropColumn('rascunho');
            $table->dropColumn('data_assinatura_alteracao');
            $table->dropColumn('data_extincao');
        });


        $codigo = Codigo::where('descricao', 'Tipo Autorização Execução Histórico')->first();
        $codigo->forceDelete();
    }
}
