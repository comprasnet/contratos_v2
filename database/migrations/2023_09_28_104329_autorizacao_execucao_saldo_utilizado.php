<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AutorizacaoExecucaoSaldoUtilizado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autorizacaoexecucao_saldo_utilizado', function (Blueprint $table) {
            $table->id();
            $table->integer('contrato_id');
            $table->integer('contratoitens_id');
            $table->integer('quantidade');
            $table->float('valor_unitario')->nullable();
            $table->string('numero_autorizacao')->nullable();
            $table->timestamps();

            $table->foreign('contrato_id')->references('id')->on('contratos')->onDelete('cascade');
            $table->foreign('contratoitens_id')->references('id')->on('contratoitens')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autorizacaoexecucao_saldo_utilizado');
    }
}
