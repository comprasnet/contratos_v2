<?php

use App\Models\BackpackUser;
use App\Models\Role;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;

class AddPermissionNovoDc extends Migration
{
    private $nomePermissao = 'administrador_novo_dc';

    /**
     * Método responsável em retornar a permissão de Administrador do sistema
     * @return mixed
     */
    private function getRoleAdministrator()
    {
        return Role::where(['name' => 'Administrador'])->first();
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        # Informações do usuário da V1 para acessar a API
        $userContratosV1 = BackpackUser::create([
            'name' => 'Contratos V1',
            'cpf' => '000.000.000-00',
            'email' => 'contratosv1@contratosv1.com.br',
            'ugprimaria' => 1,
            'password' => bcrypt(Str::random(40))
        ]);

        Permission::create(['name' => $this->nomePermissao, 'guard_name' => 'web']);

        $role = $this->getRoleAdministrator();

        $role->givePermissionTo($this->nomePermissao);

        $userContratosV1->assignRole($role);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        BackpackUser::where(
            [
                "cpf" => "000.000.000-00",
                "email" => 'contratosv1@contratosv1.com.br'
            ]
        )->delete();

        $role = $this->getRoleAdministrator();

        $role->revokePermissionTo($this->nomePermissao);

        Permission::where("name", $this->nomePermissao)->delete();
    }
}
