<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterColumnAutorizacaoExecucaoItensIdToAutorizacaoexecucaoEntregaItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_entrega_itens', function (Blueprint $table) {
            $table->dropForeign(['autorizacao_execucao_itens_id']);

            $table->foreign('autorizacao_execucao_itens_id')
                ->references('id')
                ->on('autorizacaoexecucao_itens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_entrega_itens', function (Blueprint $table) {
            $table->dropForeign(['autorizacao_execucao_itens_id']);

            $table->foreign('autorizacao_execucao_itens_id')
                ->references('id')
                ->on('autorizacaoexecucao_itens')
                ->onDelete('cascade');
        });
    }
}
