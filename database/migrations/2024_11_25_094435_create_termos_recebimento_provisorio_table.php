<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTermosRecebimentoProvisorioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('termos_recebimento_provisorio', function (Blueprint $table) {
            $table->id();
            $table->integer('contrato_id');
            $table->integer('situacao_id');
            $table->string('numero');
            $table->boolean('rascunho')->default(false);
            $table->decimal('valor_total', 17)->nullable();
            $table->text('introducao')->nullable();
            $table->text('informacoes_complementares')->nullable();
            $table->boolean('originado_sistema_externo')->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('situacao_id')
                ->references('id')
                ->on('codigoitens');

            $table->foreign('contrato_id')
                ->references('id')
                ->on('contratos');
        });

        Schema::create('termo_recebimento_provisorio_entrega', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('termo_recebimento_provisorio_id');
            $table->unsignedInteger('entrega_id');

            $table->foreign('termo_recebimento_provisorio_id')
                ->references('id')
                ->on('termos_recebimento_provisorio')
                ->cascadeOnDelete();

            $table->foreign('entrega_id')
                ->references('id')
                ->on('entregas')
                ->cascadeOnDelete();
        });

        Schema::create('termo_recebimento_provisorio_itens', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('termo_recebimento_provisorio_id');
            $table->unsignedInteger('entrega_item_id');
            $table->unsignedInteger('itemable_id');
            $table->string('itemable_type');
            $table->decimal('quantidade_informada', 30, 17);
            $table->decimal('valor_glosa', 19)->default(0);
            $table->date('mes_ano_competencia')->nullable();
            $table->string('processo_sei')->nullable();
            $table->date('data_inicio')->nullable();
            $table->date('data_fim')->nullable();
            $table->time('horario')->nullable();
            $table->timestamps();
        });

        $codigo = Codigo::create([
            'descricao' => 'Situação Termo de Recebimento Provisório',
            'visivel' => true,
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'trp_status_1',
            'descricao' => 'Em Elaboração',
        ]);
        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'trp_status_2',
            'descricao' => 'Aguardando Assinatura',
        ]);
        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'trp_status_3',
            'descricao' => 'Em Execução',
        ]);
        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'trp_status_4',
            'descricao' => 'Concluído',
        ]);

        Schema::table('models_signatarios', function (Blueprint $table) {
            $table->integer('model_termo_recebimento_provisorio_id')->nullable();

            $table->foreign('model_termo_recebimento_provisorio_id')
                ->references('id')
                ->on('termos_recebimento_provisorio')
                ->onDelete('cascade');
        });

        Schema::table('entregas', function (Blueprint $table) {
            $table->decimal('valor_total', 17)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('models_signatarios', function (Blueprint $table) {
            $table->dropColumn('model_termo_recebimento_provisorio_id');
        });
        Schema::dropIfExists('termo_recebimento_provisorio_itens');
        Schema::dropIfExists('termo_recebimento_provisorio_entrega');
        Schema::dropIfExists('termos_recebimento_provisorio');

        $codigo = Codigo::where('descricao', 'Situação Termo de Recebimento Provisório')->firstOrFail();
        $codigo->forceDelete();
    }
}
