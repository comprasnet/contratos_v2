<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;

class InsertCodigoItemSituacaoIndicacaoPreposto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::firstOrCreate(
            ['descricao' => 'Situação Indicação Preposto'],
            ['visivel' => true]
        );

        $codigoItens = [
            ['descricao' => 'Pendente', 'descres' => 'sit_indica_prepost_1'],
            ['descricao' => 'Aceita', 'descres' => 'sit_indica_prepost_2'],
            ['descricao' => 'Recusada', 'descres' => 'sit_indica_prepost_3'],
        ];

        foreach ($codigoItens as $item) {
            CodigoItem::firstOrCreate(
                ['codigo_id' => $codigo->id, 'descres' => $item['descres']],
                ['descricao' => $item['descricao'], 'visivel' => true]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = Codigo::where('descricao', 'Situação Indicação Preposto')->first();

        if ($codigo) {
            $codigo->forceDelete();
        }
    }
}
