<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterColumnUsuarioResponsavelIdOnAutorizacaoexecucaoHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_historico', function (Blueprint $table) {
            $table->unsignedInteger('usuario_responsavel_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_historico', function (Blueprint $table) {
             $table->unsignedInteger('usuario_responsavel_id')->change();
        });
    }
}
