<?php

use App\Models\Permissions;
use App\Models\Role;
use App\Models\RoleHasPermission;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;

class InsertDeleteCascateAtaRegistroPreco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // $permissoes = ["arp_criar", "arp_editar", "arp_visualizar", "arp_deletar"];
        // foreach ($permissoes as $permissao) {
        //     Permissions::updateOrCreate(["name" => $permissao, "guard_name" => "web"]);
        // }

        Schema::table('arp_autoridade_signataria', function (Blueprint $table) {
            $table->dropForeign('arp_autoridade_signataria_arp_id_foreign');
            $table->foreign('arp_id')->references('id')->on('arp')->onDelete('cascade');
        });

        Schema::table('arp_unidades', function (Blueprint $table) {
            $table->dropForeign('arp_unidades_arp_item_id_foreign');
            $table->foreign('arp_item_id')->references('id')->on('arp_item')->onDelete('cascade')->change();
        });

        Schema::table('arp_item', function (Blueprint $table) {
            $table->dropForeign('arp_item_arp_id_foreign');
            $table->foreign('arp_id')->references('id')->on('arp')->onDelete('cascade')->change();
        });

        Schema::table('arp_historico', function (Blueprint $table) {
            $table->dropForeign('arp_historico_arp_id_foreign');
            $table->foreign('arp_id')->references('id')->on('arp')->onDelete('cascade')->change();
        });

        Schema::table('arp_gestor', function (Blueprint $table) {
            $table->dropForeign('arp_gestor_arp_id_foreign');
            $table->foreign('arp_id')->references('id')->on('arp')->onDelete('cascade')->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
