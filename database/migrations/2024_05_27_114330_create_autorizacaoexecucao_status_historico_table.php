<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutorizacaoexecucaoStatusHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $codigo = Codigo::where('descricao', 'Situação Autorização Execução')->first();

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_status_6',
            'descricao' => 'Assinatura Recusada',
            'visivel' => true,
            'tipo_minuta' => false,
        ]);

        $codigo = Codigo::create([
            'descricao' => 'Status Histórico Autorização Execução',
            'visivel' => true,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_st_historico_1',
            'descricao' => 'Informou OS/F originada em sistema externo',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_st_historico_2',
            'descricao' => 'Enviou OS/F para assinatura',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_st_historico_3',
            'descricao' => 'Assinou OS/F',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_st_historico_4',
            'descricao' => 'Recusou assinatura da OS/F',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_st_historico_5',
            'descricao' => 'Alterou a OS/F',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_st_historico_6',
            'descricao' => 'Extinguiu a OS/F',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_st_historico_7',
            'descricao' => 'Retificou a OS/F',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        Schema::create('autorizacaoexecucao_status_historico', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('autorizacaoexecucao_id');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('usuario_id');
            $table->text('observacao')->nullable();
            $table->timestamps();

            $table->foreign('autorizacaoexecucao_id')
                ->references('id')
                ->on('autorizacaoexecucoes')
                ->onDelete('cascade');
            $table->foreign('status_id')
                ->references('id')
                ->on('codigoitens')
                ->onDelete('cascade');
            $table->foreign('usuario_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autorizacaoexecucao_status_historico');

        CodigoItem::where('descres', 'ae_status_6')->forceDelete();
    }
}
