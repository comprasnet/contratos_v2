<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\CompraItemUnidade;
use \App\Http\Traits\CompraTrait;
use \Illuminate\Support\Facades\DB;

class FixSaldoRemanejamentoIssue491 extends Migration
{
    use CompraTrait;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        try {
            # Critério 1
            $ciuReceberCriterio1Item41 = $this->recuperarCompraItemUnidade(6541591);
            $ciuReceberCriterio1Item41 = $this->incluirQuantidadeAutorizada($ciuReceberCriterio1Item41, 96);
            $ciuReceberCriterio1Item41 = $this->salvarSaldoAtualizado($ciuReceberCriterio1Item41);

            # Critério 2
            $ciuReceberCriterio2Item1 = $this->recuperarCompraItemUnidade(6876698);
            $ciuReceberCriterio2Item1 = $this->incluirQuantidadeAutorizada($ciuReceberCriterio2Item1, 2639);
            $ciuReceberCriterio2Item1 = $this->salvarSaldoAtualizado($ciuReceberCriterio2Item1);

            $ciuCederCriterio2Item9 =  $this->recuperarCompraItemUnidade(6876883);
            $ciuCederCriterio2Item9 = $this->salvarSaldoAtualizado($ciuCederCriterio2Item9);

            $ciuReceberCriterio2Item9 = $this->recuperarCompraItemUnidade(6876705);
            $ciuReceberCriterio2Item9 = $this->incluirQuantidadeAutorizada($ciuReceberCriterio2Item9, 830);
            $ciuReceberCriterio2Item9 = $this->salvarSaldoAtualizado($ciuReceberCriterio2Item9); # Não alterou o saldo

            $ciuCederCriterio2Item14 = $this->recuperarCompraItemUnidade(6876950);
            $ciuCederCriterio2Item14 = $this->salvarSaldoAtualizado($ciuCederCriterio2Item14);

            $ciuReceberCriterio2Item14 = $this->recuperarCompraItemUnidade(6876710);
            $ciuReceberCriterio2Item14 = $this->incluirQuantidadeAutorizada($ciuReceberCriterio2Item14, 3000);
            $ciuReceberCriterio2Item14 = $this->salvarSaldoAtualizado($ciuReceberCriterio2Item14);

            $ciuCederCriterio2Item36 = $this->recuperarCompraItemUnidade(6877169);
            $ciuCederCriterio2Item36 = $this->incluirQuantidadeAutorizada($ciuCederCriterio2Item36, 68);
            $ciuCederCriterio2Item36 = $this->salvarSaldoAtualizado($ciuCederCriterio2Item36);

            $ciuReceberCriterio2Item36 = $this->recuperarCompraItemUnidade(6876732);
            $ciuReceberCriterio2Item36 = $this->incluirQuantidadeAutorizada($ciuReceberCriterio2Item36, 3826);
            $ciuReceberCriterio2Item36 = $this->salvarSaldoAtualizado($ciuReceberCriterio2Item36);

            $ciuReceberCriterio2Item42 = $this->recuperarCompraItemUnidade(6877216);
            $ciuReceberCriterio2Item42 = $this->salvarSaldoAtualizado($ciuReceberCriterio2Item42);

            $ciuCederCriterio2Item42 = $this->recuperarCompraItemUnidade(6876738);
            $ciuCederCriterio2Item42 = $this->incluirQuantidadeAutorizada($ciuCederCriterio2Item42, 4000);
            $ciuCederCriterio2Item42 = $this->salvarSaldoAtualizado($ciuCederCriterio2Item42);

            $ciuCederCriterio2Item54 = $this->recuperarCompraItemUnidade(6877389);
            $ciuCederCriterio2Item54 = $this->salvarSaldoAtualizado($ciuCederCriterio2Item54);

            $ciuReceberCriterio2Item54 = $this->recuperarCompraItemUnidade(6876750);
            $ciuReceberCriterio2Item54 = $this->incluirQuantidadeAutorizada($ciuReceberCriterio2Item54, 1500);
            $ciuReceberCriterio2Item54 = $this->salvarSaldoAtualizado($ciuReceberCriterio2Item54);

            $ciuReceberCriterio2Item66 = $this->recuperarCompraItemUnidade(6876762);
            $ciuReceberCriterio2Item66 = $this->incluirQuantidadeAutorizada($ciuReceberCriterio2Item66, 1077);
            $ciuReceberCriterio2Item66 = $this->salvarSaldoAtualizado($ciuReceberCriterio2Item66);

            # Critério 3
            $ciuReceberCriterio3Item03 = $this->recuperarCompraItemUnidade(8177927);
            $ciuReceberCriterio3Item03 = $this->incluirQuantidadeAutorizada($ciuReceberCriterio3Item03, 1);
            $ciuReceberCriterio3Item03 = $this->salvarSaldoAtualizado($ciuReceberCriterio3Item03);

            # Critério 4

            $ciuReceberCriterio4Item11Unidade160414 = $this->recuperarCompraItemUnidade(7518928);
            $ciuReceberCriterio4Item11Unidade160414 =
                $this->salvarSaldoAtualizado($ciuReceberCriterio4Item11Unidade160414);


            $ciuReceberCriterio4Item11 = $this->recuperarCompraItemUnidade(7518697);
            $ciuReceberCriterio4Item11 = $this->incluirQuantidadeAutorizada($ciuReceberCriterio4Item11, 4);
            $this->salvarSaldoAtualizado($ciuReceberCriterio4Item11);


            $ciuReceberCriterio5Item79 = $this->recuperarCompraItemUnidade(9131345);
            $ciuReceberCriterio4Item11 = $this->incluirQuantidadeAutorizada($ciuReceberCriterio5Item79, 1000);
            $this->salvarSaldoAtualizado($ciuReceberCriterio4Item11);

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    private function recuperarCompraItemUnidade(int $id)
    {
        return CompraItemUnidade::find($id);
    }

    private function incluirQuantidadeAutorizada(CompraItemUnidade $compraItemUnidade, int $quantidadeAumentar)
    {
        $compraItemUnidade->quantidade_autorizada += $quantidadeAumentar;
        $compraItemUnidade->save();

        return $compraItemUnidade;
    }

    private function calcularSaldo(int $compraItemId, int $unidadeAutorizadaId, int $compraItemUnidadeId)
    {
        return $this->retornaSaldoAtualizado($compraItemId, $unidadeAutorizadaId, $compraItemUnidadeId)->saldo;
    }

    private function salvarSaldoAtualizado(CompraItemUnidade $compraItemUnidade)
    {
        $saldoAtualizado = $this->calcularSaldo(
            $compraItemUnidade->compra_item_id,
            $compraItemUnidade->unidade_id,
            $compraItemUnidade->id
        );

        $compraItemUnidade->quantidade_saldo = $saldoAtualizado;
        $compraItemUnidade->save();

        return $compraItemUnidade;
    }

    private function retirarQuantidadeAutorizada(CompraItemUnidade $compraItemUnidade, int $quantidadeReduzir)
    {
        $compraItemUnidade->quantidade_autorizada -= $quantidadeReduzir;
        $compraItemUnidade->save();

        return $compraItemUnidade;
    }
}
