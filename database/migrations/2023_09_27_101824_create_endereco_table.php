<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnderecoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('endereco', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('municipios_id');
            $table->foreign('municipios_id')->references('id')
                ->on('municipios')->onDelete('cascade');
            $table->string('cep', 10);
            $table->string('bairro', 255);
            $table->string('logradouro', 255);
            $table->string('complemento', 255)->nullable();
            $table->string('numero', 10)->nullable();
            $table->bigInteger('endereco_id_novo_divulgacao');
            $table->timestamps(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('endereco');
    }
}
