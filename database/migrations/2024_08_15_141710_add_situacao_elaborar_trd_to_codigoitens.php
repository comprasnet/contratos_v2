<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSituacaoElaborarTrdToCodigoitens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Situação Autorizacao Execução Entrega')->first();

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_entrega_status_10',
            'descricao' => 'Elaborar TRD',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        CodigoItem::where('descres', 'ae_entrega_status_10')->forceDelete();
    }
}
