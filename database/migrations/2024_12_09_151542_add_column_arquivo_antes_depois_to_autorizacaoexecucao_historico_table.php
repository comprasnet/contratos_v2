<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnArquivoAntesDepoisToAutorizacaoexecucaoHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_historico', function (Blueprint $table) {
            $table->text('url_arquivo_antes')->nullable();
            $table->text('url_arquivo_depois')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_historico', function (Blueprint $table) {
            $table->dropColumn('url_arquivo_antes');
            $table->dropColumn('url_arquivo_depois');
        });
    }
}
