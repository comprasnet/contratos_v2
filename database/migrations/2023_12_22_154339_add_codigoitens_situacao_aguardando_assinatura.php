<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCodigoitensSituacaoAguardandoAssinatura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Situação Autorização Execução')->first();

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_status_5',
            'descricao' => 'Aguardando assinatura',
            'visivel' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigoitem::where('descres', 'ae_status_5')->delete();
    }
}
