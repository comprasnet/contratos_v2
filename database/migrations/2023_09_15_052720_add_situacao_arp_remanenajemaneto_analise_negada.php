<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Codigo;
use App\Models\CodigoItem;

class AddSituacaoArpRemanenajemanetoAnaliseNegada extends Migration
{
    private function situacaoRemanejamentoAjustar()
    {
        return [
            'Cancelado Parcialmente' => 'Cancelado parcialmente',
            'Analisado pela Unidade Gerenciadora da ata' => 'Analisado pela unidade gerenciadora da ata',
            'Aguardando Aceitação da Unidade Participante e Gerenciadora' =>
                'Aguardando aceitação da unidade participante e gerenciadora',
            'Aguardando Aceitação da Unidade Participante' => 'Aguardando aceitação da unidade participante',
            'Aguardando Aceitação da Unidade Gerenciadora' => 'Aguardando aceitação da unidade gerenciadora',
            ];
    }
    
    private function updateSituacao(int $codigoId, string $valorFiltro, string $valorAtualizar)
    {
        CodigoItem::where(
            [
                'descres' => 'status_remanejamento',
                'codigo_id' => $codigoId,
                'descricao' => $valorFiltro
            ]
        )
            ->update(
                [
                    'descricao' => $valorAtualizar
                ]
            );
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        # Recupera os status
        $todosStatus = config('arp.arp.status_remanejamento');
        $codigo = Codigo::where('descricao', 'Tipo de Ata de Registro de Preços')->first();
        
        $situacoesAtualizar = $this->situacaoRemanejamentoAjustar();
        
        foreach ($situacoesAtualizar as $de => $para) {
            $this->updateSituacao($codigo->id, $de, $para);
        }
        
        foreach ($todosStatus as $status) {
            CodigoItem::updateOrCreate(
                [
                    'codigo_id' => $codigo->id,
                    'descres' => 'status_remanejamento',
                    'descricao' => $status
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        CodigoItem::where("descres", "status_remanejamento")
            ->whereIn(
                'descricao',
                [
                    'Negado pela unidade participante',
                    'Negado pela unidade gerenciadora da ata',
                    'Cancelado',
                    'Aceito'
                ]
            )
            ->forceDelete();
        
        $situacoesAtualizar = $this->situacaoRemanejamentoAjustar();
        $codigo = Codigo::where('descricao', 'Tipo de Ata de Registro de Preços')->first();
        
        foreach ($situacoesAtualizar as $de => $para) {
            $this->updateSituacao($codigo->id, $para, $de);
        }
    }
}
