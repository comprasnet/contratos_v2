<?php

use App\Models\Catmatclasse;
use App\Models\Catmatsergrupo;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertCatmatclassesGenerico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        # Recupera o id do GRUPO GENERICO MATERIAIS
        $idGrupoGenerico = Catmatsergrupo::where('descricao', 'GRUPO GENERICO MATERIAIS')
                            ->where("tipo_id", 149)
                            ->first();

        # Insere a classe genérica
        Catmatclasse::updateOrCreate(
            [
                'catmatsergrupo_id' => $idGrupoGenerico->id,
                'codigo' => '99999',
                'descricao' => 'CLASSE GENERICA MATERIAIS'
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
