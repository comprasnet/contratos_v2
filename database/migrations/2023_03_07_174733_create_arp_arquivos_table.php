<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArpArquivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arp_arquivos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('arp_id')->constrained('arp')->cascadeOnDelete();
            $table->foreignId('tipo_id')->constrained('arp_arquivo_tipos')->nullOnDelete();
            $table->string('nome');
            $table->string('url');
            $table->text('descricao')->nullable();
            $table->boolean('restrito')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arp_arquivos');
    }
}
