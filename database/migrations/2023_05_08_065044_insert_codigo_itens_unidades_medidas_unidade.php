<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertCodigoItensUnidadesMedidasUnidade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigoMaterial = Codigo::where('descricao', 'Material')->first();

        $descricao = 'Unidade';

        CodigoItem::create([
            'codigo_id' => $codigoMaterial->id,
            'descres' => strtoupper(substr($descricao, 0, 20)),
            'descricao' => $descricao,
            'visivel' => false
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigoMaterial = Codigo::where('descricao', 'Material')->first();

        CodigoItem::where('descricao', 'Unidade')->where('codigo_id', $codigoMaterial->id)->delete();
    }
}
