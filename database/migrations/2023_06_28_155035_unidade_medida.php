<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Codigo;
use App\Models\CodigoItem;

class UnidadeMedida extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->upDrops();
        Schema::create('unidade_medida',function (Blueprint $table){
            $table->id();
            $table->string('tipo',32);
            $table->string('sigla',32);
            $table->string('nome',32);
            $table->integer('codigo_siasg')->nullable();
            $table->timestamps();
        });
        $this->upAdd();
    }

    private function upDrops(){
        Schema::table('autorizacaoexecucao_itens', function (Blueprint $table){
            $table->dropColumn('unidade_medida_id');
        });
        $codigoMaterial = Codigo::where('descricao', 'Material')->first();
        CodigoItem::where('descricao', 'Unidade')->where('codigo_id', $codigoMaterial->id)->delete();
    }

    private function upAdd(){
        Schema::table('autorizacaoexecucao_itens', function (Blueprint $table) {
            $table->integer('unidade_medida_id')->nullable();
            $table->foreign('unidade_medida_id')->references('id')->on('unidade_medida');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidade_medida');
    }
}
