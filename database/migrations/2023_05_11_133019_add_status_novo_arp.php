<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusNovoArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     /**
      * Método responsável em recuperar o ID da tabela código
      */
    private function recuperarIdCodigo()
    {
        $codigoMaterial = Codigo::where('descricao', 'Tipo de Ata de Registro de Preços')->first();
        return $codigoMaterial->id;
    }

    # Novos status
    # Carregando Compra = Quando a ata for criada e o JOB para os participantes não foram concluídos
    # Enviar PNCP = Ata Pronta para Enviar ao PNCP
    # Publicando PNCP = Arquivo sendo enviado para o PNCP
    private $novosStatus = ['Carregando Compra', 'Enviar PNCP', 'Publicando PNCP', 'Erro Publicar'];

    public function up()
    {
        foreach ($this->novosStatus as $status) {
            CodigoItem::updateOrInsert([
                'codigo_id' => $this->recuperarIdCodigo(),
                'descres' => strtoupper(substr($status, 0, 20)),
                'descricao' => $status,
                'visivel' => true,
                'created_at' =>  Carbon::now()
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigoMaterial = $this->recuperarIdCodigo();
        foreach ($this->novosStatus as $status) {
            $t = CodigoItem::where("descricao", $status)->where("codigo_id", $codigoMaterial)->first();
            $t->delete();
        }
    }
}
