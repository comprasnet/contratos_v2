<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnHistoricoArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_historico', function (Blueprint $table) {
            if (!Schema::hasColumn('arp_historico', 'modalidade_id')) {
                $table->integer('modalidade_id');
            }
            if (!Schema::hasColumn('arp_historico', 'objeto')) {
                $table->text('objeto');
            }
            if (!Schema::hasColumn('arp_historico', 'compra_centralizada')) {
                $table->boolean('compra_centralizada');
            }
            if (Schema::hasColumn('arp_historico', 'aceita_adesao')) {
                $table->dropColumn('aceita_adesao');
            }
            if (!Schema::hasColumn('arp_historico', 'vigencia_inicial_alteracao')) {
                $table->date('vigencia_inicial_alteracao')->nullable();
            }
            if (!Schema::hasColumn('arp_historico', 'vigencia_final_alteracao')) {
                $table->date('vigencia_final_alteracao')->nullable();
            }
            if (!Schema::hasColumn('arp_historico', 'data_assinatura_alteracao')) {
                $table->date('data_assinatura_alteracao')->nullable();
            }
            if (!Schema::hasColumn('arp_historico', 'compra_centralizada_alteracao')) {
                $table->boolean('compra_centralizada_alteracao')->nullable();
            }
            if (!Schema::hasColumn('arp_historico', 'justificativa_motivo_cancelamento')) {
                $table->text('justificativa_motivo_cancelamento')->nullable();
            }

            if (!Schema::hasColumn('arp_historico', 'modalidade_id')) {
                $table->foreign('modalidade_id')->references('id')->on('codigoitens');
            }
        });

        Schema::create('arp_autoridade_signataria_historico', function (Blueprint $table) {
            // Definição das colunas
        });
    }

    public function down()
    {
        Schema::dropIfExists('arp_autoridade_signataria_historico');

        Schema::table('arp_historico', function (Blueprint $table) {
            $table->dropColumn('vigencia_inicial_alteracao');
            $table->dropColumn('vigencia_final_alteracao');
            $table->dropColumn('data_assinatura_alteracao');
            $table->dropColumn('compra_centralizada_alteracao');
        });
    }

}
