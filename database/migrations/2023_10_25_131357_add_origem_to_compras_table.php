<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrigemToComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('compras', 'origem')) {
            Schema::table('compras', function (Blueprint $table) {
                $table->smallInteger('origem')->default(2)->nullable();
            });
            
            \Illuminate\Support\Facades\DB::statement("COMMENT ON COLUMN compras.origem IS '1 = NDC; 2 = SIASG'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compras', function (Blueprint $table) {
            $table->dropColumn('origem');
        });
    }
}
