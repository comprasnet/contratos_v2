<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableArpItemHistorico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        # Se a tabela não existir, então cria
        if (!Schema::hasTable('arp_item_historico')) {
            Schema::create('arp_item_historico', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('arp_historico_id');
                $table->integer('arp_item_id');
                
                $table->decimal('valor', 15, 2)->nullable();

                $table->date('vigencia_inicial')->nullable();
                $table->date('vigencia_final')->nullable();
                $table->boolean('item_cancelado')->default(false);
                $table->boolean('valor_alterado')->default(false);
                $table->timestamps();

                $table->foreign('arp_historico_id')->references('id')->on('arp_historico')->onDelete('cascade');
                $table->foreign('arp_item_id')->references('id')->on('arp_item');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arp_item_historico');
    }
}
