<?php

use Illuminate\Support\Facades\DB;
use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertCodigoitensRelatarExecucaoAta extends Migration
{
    const RELATAR_EXECUCAO_ATA = 'Relatar execução da ata';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {

            # A lógica abaixo é feita para garantir o funcionamento mesmo que executado o rollback várias vezes

            // Busca o registro, incluindo registros soft-deleted
            $codigoItem = CodigoItem::withTrashed()
                ->where('descres', 'statushistorico')
                ->where('descricao', self::RELATAR_EXECUCAO_ATA)
                ->first();

            if ($codigoItem) {
                // Se o registro existir, atualiza o deleted_at para null
                $codigoItem->deleted_at = null;
                $codigoItem->save();
            } else {
                # Para buscar o codigo_id da tabela codigos
                $descricaoTipoAta = 'Tipo de Ata de Registro de Preços';
                $tipoAtaCodigo = Codigo::where('descricao', $descricaoTipoAta)->first();

                // Se o registro não existir, cria um novo
                CodigoItem::updateOrCreate([
                    'codigo_id' => $tipoAtaCodigo->id,
                    'descres' => 'statushistorico',
                    'descricao' => self::RELATAR_EXECUCAO_ATA,
                    'visivel' => true,
                    'ordem' => null,
                ]);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        CodigoItem::withTrashed()
            ->where(function ($query) {
                $query->where('descres', 'statushistorico')
                    ->where('descricao', self::RELATAR_EXECUCAO_ATA);
            })
            ->delete();
    }
}
