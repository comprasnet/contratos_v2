<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RetirarCampoObrigatorioArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp', function (Blueprint $table) {
            $table->date('data_assinatura')->nullable()->change();
            $table->date('vigencia_inicial')->nullable()->change();
            $table->date('vigencia_final')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp', function (Blueprint $table) {
            $table->date('data_assinatura')->change();
            $table->date('vigencia_inicial')->change();
            $table->date('vigencia_final')->change();
        });
    }
}
