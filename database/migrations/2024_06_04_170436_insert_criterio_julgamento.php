<?php

use App\Models\AutorizacaoExecucao;
use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertCriterioJulgamento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Codigo::create([
            'descricao' => 'Codigo Fase Externa vindo do NDC',
            'visivel' => false,
        ]);

        Codigo::create([
            'descricao' => 'Codigo PNCP vindo do NDC',
            'visivel' => false,
        ]);

        $codigo = Codigo::create([
            'descricao' => 'Critério de Julgamento vindo do NDC',
            'visivel' => false,
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Não se aplica',
            'visivel' => false,
            'descres' => '0'
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Menor preço',
            'visivel' => false,
            'descres' => '1'
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Maior desconto',
            'visivel' => false,
            'descres' => '2'
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Melhor Técnica',
            'visivel' => false,
            'descres' => '3'
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Tecnica e preço',
            'visivel' => false,
            'descres' => '4'
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Conteúdo Artístico',
            'visivel' => false,
            'descres' => '5'
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descricao' => 'Maior Retorno economico',
            'visivel' => false,
            'descres' => '6'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigo::where('descricao', 'Criterio Julgamento vindo do NDC')->forceDelete();
        Codigo::where('descricao', 'Codigo Fase Externa vindo do NDC')->forceDelete();
        Codigo::where('descricao', 'Codigo PNCP vindo do NDC')->forceDelete();
    }
}
