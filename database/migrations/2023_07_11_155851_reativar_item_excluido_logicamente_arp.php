<?php

use App\Http\Traits\LogTrait;
use App\Models\Catmatseritem;
use App\Models\CompraItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ReativarItemExcluidoLogicamenteArp extends Migration
{
    use LogTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        # Recupera os registros da tabela compra item excluidos logicamente
        # que estão vinculados a uma ata de registro de preço
        $itemExcluidoLogicamente = CompraItem::join(
            "compra_item_fornecedor",
            "compra_item_fornecedor.compra_item_id",
            "compra_items.id"
        )
        ->join("arp_item", "compra_item_fornecedor.id", "arp_item.compra_item_fornecedor_id")
        ->onlyTrashed()
        ->select("compra_items.*")
        ->get();

        DB::beginTransaction();
        try {
            foreach ($itemExcluidoLogicamente as $item) {
                # Recupera o registro na catmatseritem deletado
                $catMatSerItemDeletado = Catmatseritem::withTrashed()->find($item->catmatseritem_id);

                # Recupera o registro da catmatsetitem ativo
                $catMatSerItemAtivo = Catmatseritem::where("situacao", true)
                ->where("grupo_id", $catMatSerItemDeletado->grupo_id)
                ->where("codigo_siasg", $catMatSerItemDeletado->codigo_siasg)
                ->whereNotNUll("catmatpdm_id")
                ->first();

                if (isset($catMatSerItemAtivo->id)) {
                    # Substitui pelo ID da catmatsetitem que está ativo
                    $item->catmatseritem_id = $catMatSerItemAtivo->id;

                    # Reativa o item retirando o preenchimento na coluna de deletado
                    // $item->deleted_at = null;

                    # Salva as informações
                    $item->save();

                    DB::commit();
                }
            }
        } catch (Exception $ex) {
            DB::rollBack();
            $this->inserirLogCustomizado('gestao_ata', 'error', $ex);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
