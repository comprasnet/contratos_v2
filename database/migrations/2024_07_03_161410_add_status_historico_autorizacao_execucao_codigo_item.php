<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusHistoricoAutorizacaoExecucaoCodigoItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Status Histórico Autorização Execução')->first();

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_st_historico_8',
            'descricao' => 'Enviou alteração da OS/F para assinatura',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_st_historico_9',
            'descricao' => 'Assinou alteração da OS/F',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'ae_st_historico_10',
            'descricao' => 'Recusou assinatura da alteração da OS/F',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        CodigoItem::where('descres', 'ae_st_historico_8')->forceDelete();
        CodigoItem::where('descres', 'ae_st_historico_9')->forceDelete();
        CodigoItem::where('descres', 'ae_st_historico_10')->forceDelete();
    }
}
