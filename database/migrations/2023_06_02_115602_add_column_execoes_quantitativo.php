<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnExecoesQuantitativo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->boolean('aquisicao_emergencial_medicamento_material')->nullable();
            $table->boolean('execucao_descentralizada_programa_projeto_federal')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->dropColumn('aquisicao_emergencial_medicamento_material');
            $table->dropColumn('execucao_descentralizada_programa_projeto_federal');
        });
    }
}
