<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableArpArquivoGenerico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arquivo_generico', function (Blueprint $table) {
            $table->id();
            $table->morphs('arquivoable');
            $table->integer('tipo_id');
            $table->longText('nome');
            $table->longText('descricao');
            $table->longText('url');
            $table->boolean('restrito');
            $table->integer('user_id');
            $table->timestamps();

            $table->foreign('tipo_id')->references('id')->on('codigoitens');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arquivo_generico');
    }
}
