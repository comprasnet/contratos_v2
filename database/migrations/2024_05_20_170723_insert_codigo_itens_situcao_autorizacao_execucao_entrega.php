<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertCodigoItensSitucaoAutorizacaoExecucaoEntrega extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $codigo = Codigo::updateOrCreate([
            'descricao' => 'Situação Autorizacao Execução Entrega',
            'visivel' => true
        ]);

        CodigoItem::updateOrCreate([
            'codigo_id' => $codigo->id,
            'descricao' => 'Avaliação Sumária',
            'visivel' => true,
            'descres' => 'ae_entrega_status_1'
        ]);

        CodigoItem::updateOrCreate([
            'codigo_id' => $codigo->id,
            'descricao' => 'TRP em Elaboração',
            'visivel' => true,
            'descres' => 'ae_entrega_status_2'
        ]);

        CodigoItem::updateOrCreate([
            'codigo_id' => $codigo->id,
            'descricao' => 'Correção Sumária',
            'visivel' => true,
            'descres' => 'ae_entrega_status_3'
        ]);

        CodigoItem::updateOrCreate([
            'codigo_id' => $codigo->id,
            'descricao' => 'Aguardando Assinatura do TRP',
            'visivel' => true,
            'descres' => 'ae_entrega_status_4'
        ]);

        CodigoItem::updateOrCreate([
            'codigo_id' => $codigo->id,
            'descricao' => 'Avaliando Execução',
            'visivel' => true,
            'descres' => 'ae_entrega_status_5'
        ]);

        CodigoItem::updateOrCreate([
            'codigo_id' => $codigo->id,
            'descricao' => 'Correção Execução',
            'visivel' => true,
            'descres' => 'ae_entrega_status_6'
        ]);

        CodigoItem::updateOrCreate([
            'codigo_id' => $codigo->id,
            'descricao' => 'TRD em Elaboração',
            'visivel' => true,
            'descres' => 'ae_entrega_status_7'
        ]);

        CodigoItem::updateOrCreate([
            'codigo_id' => $codigo->id,
            'descricao' => 'Aguardando Assinatura do TRD',
            'visivel' => true,
            'descres' => 'ae_entrega_status_8'
        ]);

        CodigoItem::updateOrCreate([
            'codigo_id' => $codigo->id,
            'descricao' => 'Entrega em elaboração',
            'visivel' => true,
            'descres' => 'ae_entrega_status_9'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = Codigo::where('descricao', 'Situação Autorizacao Execução Entrega')->first();
        $codigo->forceDelete();
    }
}
