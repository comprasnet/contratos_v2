<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\CompraItemUnidade;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Log;

class FixQuantidadeAdquirirCaronaLei14133ParaAdesao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $caronasQtdAdquirirErrada = CompraItemUnidade::select(
            'compra_item_unidade.id',
            DB::raw('sum(arp_solicitacao_item.quantidade_aprovada) as total_aprovada'),
            'compra_item_unidade.quantidade_adquirir'
        )
            ->join('compra_items', function ($query) {
                $query->on('compra_items.id', '=', 'compra_item_unidade.compra_item_id')
                    ->where('compra_item_unidade.situacao', true);
            })
            ->join('compra_item_fornecedor', 'compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')
            ->join('arp_item', 'compra_item_fornecedor.id', '=', 'arp_item.compra_item_fornecedor_id')
            ->join('arp_solicitacao_item', 'arp_item.id', '=', 'arp_solicitacao_item.item_arp_fornecedor_id')
            ->join('arp_solicitacao', function ($join) {
                $join->on('arp_solicitacao.id', '=', 'arp_solicitacao_item.arp_solicitacao_id')
                    ->on('compra_item_unidade.unidade_id', 'arp_solicitacao.unidade_origem_id')
                    ->where('arp_solicitacao.rascunho', false);
            })
            ->join('codigoitens as ciSoliticaoItem', function ($join) {
                $join->on('arp_solicitacao_item.status_id', '=', 'ciSoliticaoItem.id')
                    ->whereIn('ciSoliticaoItem.descricao', ['Aceitar', 'Aceitar Parcialmente']);
            })
            ->join('compras', 'compra_items.compra_id', '=', 'compras.id')
            ->join('codigoitens', function ($join) {
                $join->on('compras.tipo_compra_id', '=', 'codigoitens.id')
                    ->where('codigoitens.descres', '02');
            })
            ->where('compra_item_unidade.tipo_uasg', 'C')
            ->groupBy('compra_item_unidade.id', 'compra_item_unidade.quantidade_adquirir')
            ->havingRaw('sum(arp_solicitacao_item.quantidade_aprovada) <> compra_item_unidade.quantidade_adquirir')
            ->get();

        try {
            DB::beginTransaction();
            foreach ($caronasQtdAdquirirErrada as $carona) {
                $compraItemUnidade = CompraItemUnidade::find($carona->id);
                $mensagem = "ID: {$carona->id}, quantidade_adquirir_atual: {$compraItemUnidade->quantidade_adquirir}
                quantidade_adquirir_nova: {$carona->total_aprovada}";
                
                $carona->quantidade_adquirir = $carona->total_aprovada;
                $carona->quantidade_adquirida = 0;
                $carona->save();
                
                Log::info($mensagem);
            }
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            Log::error("Erro ao executar a migration FixQuantidadeAdquirirCaronaLei14133ParaAdesao");
            Log::error($exception);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
