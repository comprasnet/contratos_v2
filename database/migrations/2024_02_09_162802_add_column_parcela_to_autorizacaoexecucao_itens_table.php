<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddColumnParcelaToAutorizacaoexecucaoItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_itens', function (Blueprint $table) {
            $table->integer('parcela')->nullable();
        });

        Schema::table('autorizacaoexecucao_itens_historico', function (Blueprint $table) {
            $table->integer('parcela')->nullable()->default(1);
        });

        $statusEmElaboracao = DB::table('codigoitens')->where('descres', 'ae_status_1')->first();

        DB::table('autorizacaoexecucao_itens')
            ->join(
                'autorizacaoexecucoes',
                'autorizacaoexecucoes.id',
                '=',
                'autorizacaoexecucao_itens.autorizacaoexecucoes_id'
            )
            ->where('situacao_id', '<>', $statusEmElaboracao->id)
            ->update(['autorizacaoexecucao_itens.parcela' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_itens', function (Blueprint $table) {
            $table->dropColumn('parcela');
        });

        Schema::table('autorizacaoexecucao_itens_historico', function (Blueprint $table) {
            $table->dropColumn('parcela');
        });
    }
}
