<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;

class AddCodigoitensSituacaoNumseqpenEnvioPncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Situação Envia Dados PNCP')->first();

        Codigoitem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'NUMSEQPEN',
            'descricao' => 'Número Sequencial do Pncp Pendente',
            'visivel' => false
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigoitem::where('descres', 'NUMSEQPEN')->delete();
    }
}
