<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContratoOpmIdToContratoArquivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contrato_arquivos', function (Blueprint $table) {
            $table->unsignedBigInteger('contrato_opm_id')->nullable();

            $table->foreign('contrato_opm_id')
                ->references('id')
                ->on('contrato_opm');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contrato_arquivos', function (Blueprint $table) {
            $table->dropForeign(['contrato_opm_id']);
            $table->dropColumn('contrato_opm_id');
        });
    }
}
