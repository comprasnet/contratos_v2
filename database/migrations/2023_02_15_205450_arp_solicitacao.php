<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ArpSolicitacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arp_solicitacao', function (Blueprint $table) {
            $table->id();
            $table->integer('unidade_origem_id');
            $table->integer('compra_id');
            $table->integer('status');
            $table->integer('sequencial');
            $table->integer('ano');

            $table->text('processo_adesao')->nullable();

            $table->text('texto_justificativa')->nullable();
            $table->text('justificativa')->nullable();
            $table->text('demonstracao')->nullable();
            $table->text('aceitacao')->nullable();
            
            $table->boolean('demonstracao_valores_registrados');
            $table->boolean('consulta_aceitacao_fornecedor');

            $table->boolean('rascunho');
            $table->timestamps();

            $table->foreign('unidade_origem_id')->references('id')->on('unidades');
            $table->foreign('compra_id')->references('id')->on('compras');
            $table->foreign('status')->references('id')->on('codigoitens');
        });

        Schema::create('arp_solicitacao_item', function (Blueprint $table) {
            $table->id();
            $table->integer('arp_solicitacao_id');
            $table->integer('item_arp_fornecedor_id');
            $table->decimal('quantidade_solicitada',15,5);
            $table->timestamps();

            $table->foreign('arp_solicitacao_id')->references('id')->on('arp_solicitacao')->onDelete('cascade');
            $table->foreign('item_arp_fornecedor_id')->references('id')->on('arp_item');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arp_solicitacao');
        Schema::dropIfExists('arp_solicitacao_item');
    }
}
