<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertCodigoItensTrpEmitido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigoItem = \App\Models\CodigoItem::where('descres', 'ae_entrega_status_7');

        $codigoItem->update([
            'descricao' => 'TRD Emitido',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigoItem = \App\Models\CodigoItem::where('descres', 'ae_entrega_status_7');

        $codigoItem->update([
            'descricao' => 'TRD em Elaboração',
        ]);
    }
}
