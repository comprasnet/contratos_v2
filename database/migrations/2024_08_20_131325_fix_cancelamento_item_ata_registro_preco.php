<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\ArpItemHistorico;
use \Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FixCancelamentoItemAtaRegistroPreco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $itensComErroCancelamento = ArpItemHistorico::leftJoin('arp_item', function ($join) {
                $join->on('arp_item.id', '=', 'arp_item_historico.arp_item_id')
                    ->whereNull('arp_item.deleted_at');
        })
            ->join('arp', 'arp_item.arp_id', '=', 'arp.id')
            ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
            ->join('codigoitens', 'arp.tipo_id', '=', 'codigoitens.id')
            ->join('unidades', 'arp.unidade_origem_id', '=', 'unidades.id')
            ->join('arp_historico', 'arp_item_historico.arp_historico_id', '=', 'arp_historico.id')
            ->join('codigoitens as ciAtaHistorico', 'ciAtaHistorico.id', '=', 'arp_historico.tipo_id')
            ->select('arp_item_historico.*')
            ->where('codigoitens.descricao', 'Ata de Registro de Preços')
            ->where('ciAtaHistorico.descricao', 'Cancelamento de item(ns)')
            ->where('compra_item_fornecedor.valor_unitario', 0)
            ->where('arp_item_historico.item_cancelado', false)
            ->get();
        
        DB::beginTransaction();
        try {
            foreach ($itensComErroCancelamento as $item) {
                Log::info('ID item histórico alterado: '. $item->id);
                $item->item_cancelado = true;
                $item->valor = 0;
                $item->save();
            }
            DB::commit();
        } catch (Exception $e) {
            dd($e);
            DB::rollBack();
            Log::error('Erro ao executar a migration FixCancelamentoItemAtaRegistroPreco');
            Log::error($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
