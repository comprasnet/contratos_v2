<?php

use App\Models\AutorizacaoexecucaoHistorico;
use App\Models\AutorizacaoexecucaoItensHistorico;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAutorizacaoexecucaoItensColumnToAutorizacaoexecucaoItensHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_itens_historico', function (Blueprint $table) {
            $table->unsignedBigInteger('autorizacaoexecucao_itens_id')
                ->nullable();
            $table->foreign('autorizacaoexecucao_itens_id')
                ->references('id')
                ->on('autorizacaoexecucao_itens')
                ->nullOnDelete();
        });

        Schema::table('autorizacaoexecucao_entrega_itens', function (Blueprint $table) {
            $table->dropForeign(['autorizacao_execucao_itens_id']);

            $table->foreign('autorizacao_execucao_itens_id')
                ->references('id')
                ->on('autorizacaoexecucao_itens')
                ->onDelete('cascade');
        });


        $aeItensHistoricos = AutorizacaoexecucaoItensHistorico::all();
        foreach ($aeItensHistoricos as $aeItemHistorico) {
            $aeHistorico = AutorizacaoexecucaoHistorico::find($aeItemHistorico->autorizacaoexecucao_historico_id);
            $aeItem = $aeHistorico->autorizacaoexecucao->autorizacaoexecucaoItens()
                ->where('contratohistorico_id', $aeItemHistorico->contratohistorico_id)
                ->where('saldohistoricoitens_id', $aeItemHistorico->saldohistoricoitens_id)
                ->first();
            if (!empty($aeItem)) {
                $aeItemHistorico->autorizacaoexecucao_itens_id = $aeItem->id;
            }

            $aeItemHistorico->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_itens_historico', function (Blueprint $table) {
            $table->dropForeign(['autorizacaoexecucao_itens_id']);
            $table->dropColumn('autorizacaoexecucao_itens_id');
        });

        Schema::table('autorizacaoexecucao_entrega_itens', function (Blueprint $table) {
            $table->dropForeign(['autorizacao_execucao_itens_id']);

            $table->foreign('autorizacao_execucao_itens_id')
                ->references('id')
                ->on('autorizacaoexecucao_itens');
        });
    }
}
