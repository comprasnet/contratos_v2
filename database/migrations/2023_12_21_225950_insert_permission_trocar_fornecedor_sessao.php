<?php

use App\Models\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;

class InsertPermissionTrocarFornecedorSessao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        $permission = Permission::where('name', 'trocar_fornecedor_sessao')->first();

        if (!isset($permission->id)) {
            Permission::create([
                'name' => 'trocar_fornecedor_sessao',
                'guard_name' => 'web'
            ]);
        }
        $role = Role::where('name', 'Administrador')->first();
        $role->givePermissionTo('trocar_fornecedor_sessao');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('trocar_fornecedor_sessao');

        Permission::where(['name' => 'trocar_fornecedor_sessao'])->forceDelete();
    }
}
