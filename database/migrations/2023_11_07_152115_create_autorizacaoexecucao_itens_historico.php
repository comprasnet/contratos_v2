<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutorizacaoexecucaoItensHistorico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autorizacaoexecucao_itens_historico', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('autorizacaoexecucao_historico_id');
            $table->unsignedInteger('contratohistorico_id');
            $table->unsignedInteger('saldohistoricoitens_id');
            $table->unsignedInteger('unidade_medida_id');
            $table->decimal('quantidade', 30, 17);
            $table->time('horario')->nullable();
            $table->time('horario_fim')->nullable();
            $table->boolean('subcontratacao');
            $table->decimal('valor_unitario', 17);
            $table->string('numero_demanda_sistema_externo')->nullable();
            $table->string('localidade_de_execucao')->nullable();
            $table->enum('tipo_historico', ['antes', 'depois']);

            $table->timestamps();

            $table->foreign('autorizacaoexecucao_historico_id')
                ->references('id')
                ->on('autorizacaoexecucao_historico')
                ->onDelete('cascade');
            $table->foreign('unidade_medida_id')
                ->references('id')
                ->on('unidade_medida')
                ->onDelete('cascade');
            $table->foreign('contratohistorico_id')
                ->references('id')
                ->on('contratohistorico')
                ->onDelete('cascade');
            $table->foreign('saldohistoricoitens_id')
                ->references('id')
                ->on('saldohistoricoitens')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autorizacaoexecucao_itens_historico');
    }
}
