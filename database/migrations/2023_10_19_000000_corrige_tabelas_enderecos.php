<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CorrigeTabelasEnderecos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('compra_item_unidade_local_entrega');
        Schema::dropIfExists('endereco');
        
        Schema::create('enderecos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('municipios_id');
            $table->foreign('municipios_id')->references('id')
                ->on('municipios')->onDelete('cascade');
            $table->string('cep', 10);
            $table->string('bairro', 255);
            $table->string('logradouro', 255);
            $table->string('complemento', 255)->nullable();
            $table->string('numero', 10)->nullable();
            $table->timestamps(0);
        });
        Schema::create('compra_item_unidade_local_entrega', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('compra_item_unidade_id');
            $table->foreign('compra_item_unidade_id')->references('id')->on('compra_item_unidade')->onDelete('cascade');
            $table->unsignedBigInteger('endereco_id');
            $table->foreign('endereco_id')->references('id')->on('enderecos')->onDelete('cascade');
            $table->decimal('quantidade', 15, 5);
            $table->bigInteger('endereco_id_novo_divulgacao');
            $table->timestamps(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enderecos');
        Schema::dropIfExists('compra_item_unidade_local_entrega');
    }
}
