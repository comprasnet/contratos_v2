<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutorizacaoexecucaoContratoLocalExecucaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autorizacaoexecucao_contrato_local_execucao', function (Blueprint $table) {
            $table->unsignedInteger('autorizacaoexecucao_id');
            $table->unsignedInteger('contrato_local_execucao_id');
            $table->timestamps();

            $table->unique(
                ['autorizacaoexecucao_id', 'contrato_local_execucao_id'],
                'autorizacaoexecucao_contrato_local_execucao_unique'
            );
            $table->foreign('autorizacaoexecucao_id')
                ->references('id')
                ->on('autorizacaoexecucoes')
                ->onDelete('cascade');
            $table->foreign('contrato_local_execucao_id')
                ->references('id')
                ->on('contrato_local_execucao')
                ->onDelete('cascade');
        });

        Schema::create('autorizacaoexecucao_contrato_local_execucao_historico', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('autorizacaoexecucao_historico_id');
            $table->unsignedInteger('contrato_local_execucao_id');
            $table->enum('tipo_historico', ['antes', 'depois']);
            $table->timestamps();

            $table->foreign('autorizacaoexecucao_historico_id')
                ->references('id')
                ->on('autorizacaoexecucao_historico')
                ->onDelete('cascade');
            $table->foreign('contrato_local_execucao_id')
                ->references('id')
                ->on('contrato_local_execucao')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autorizacaoexecucao_contrato_local_execucao');
        Schema::dropIfExists('autorizacaoexecucao_contrato_local_execucao_historico');
    }
}
