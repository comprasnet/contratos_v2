<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertTipoAdesaoArp2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where("descricao","Tipo de Ata de Registro de Preços")->first();
        $status = config("arp.arp.status_adesao_inicial");
        foreach ($status as $key => $status) {
            CodigoItem::updateOrInsert(['codigo_id' => $codigo->id, 'descres' => $key, 'descricao' => $status]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
