<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateContratoOpmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();

        try {
                $codigo = Codigo::create([
                    'descricao' => 'Situações da Declaração OPM',
                    'visivel' => true,
                ]);

                // Criação dos itens para a nova categoria com descrições mais curtas
                CodigoItem::create([
                    'codigo_id' => $codigo->id,
                    'descres' => 'opm_ativa',
                    'descricao' => 'Ativa',
                ]);

                CodigoItem::create([
                    'codigo_id' => $codigo->id,
                    'descres' => 'opm_cancelada',
                    'descricao' => 'Cancelada',
                ]);

                CodigoItem::create([
                    'codigo_id' => $codigo->id,
                    'descres' => 'opm_elaboracao',
                    'descricao' => 'Em elaboração',
                ]);

                CodigoItem::create([
                    'codigo_id' => $codigo->id,
                    'descres' => 'opm_revogada',
                    'descricao' => 'Revogada',
                ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
        }

        // Criação da tabela contrato_opm
        Schema::create('contrato_opm', function (Blueprint $table) {
            $table->id();
            $table->foreignId('contrato_id')->constrained('contratos');
            $table->string('quantidade_familiar')->nullable();
            $table->string('quantidade_contratadas')->nullable();
            $table->string('sequencial')->nullable();
            //$table->string('ano');
            $table->foreignId('situacao_id')->constrained('codigoitens');
            $table->boolean('percentual_minimo')->default(false);
            $table->boolean('rascunho')->default(false);
            $table->date('data_cancelamento')->nullable();
            $table->date('data_assinatura')->nullable();
            $table->string('justificativa_cancelamento')->nullable();
            $table->foreignId('usuario_id_cancelamento')->nullable()->constrained('users');
            $table->foreignId('usuario_id')->nullable()->constrained('users');
            $table->foreignId('contrato_responsavel_id')
                    ->nullable()
                    ->constrained('contratoresponsaveis')
                    ->after('contrato_id')
                    ->nullable();
            $table->foreignId('unidade_id')->nullable()->constrained('unidades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrato_opm');
    }
}
