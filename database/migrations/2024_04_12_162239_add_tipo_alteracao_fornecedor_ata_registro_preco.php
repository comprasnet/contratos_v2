<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\CodigoItem;
use \App\Models\Codigo;

class AddTipoAlteracaoFornecedorAtaRegistroPreco extends Migration
{
    private function getIdCodigo()
    {
        return Codigo::where('descricao', 'Tipo de Ata de Registro de Preços')->first()->id;
    }
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $idCodigo = $this->getIdCodigo();

        CodigoItem::create([
            'codigo_id' => $idCodigo,
            'descres' => 'statushistorico',
            'descricao' => 'Fornecedor',
            'visivel' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        CodigoItem::where('descres', 'statushistorico')->where('descricao', 'Fornecedor')->delete();
    }
}
