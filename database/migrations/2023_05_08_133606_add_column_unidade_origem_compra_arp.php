<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUnidadeOrigemCompraArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp', function (Blueprint $table) {
            $table->integer('unidade_origem_compra_id')->nullable();
            $table->foreign('unidade_origem_compra_id')->references('id')->on('unidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp', function (Blueprint $table) {
            $table->dropColumn('unidade_origem_compra_id');
        });
    }
}
