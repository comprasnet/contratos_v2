<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\ArpRemanejamento;
use \App\Models\CodigoItem;
use \Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Log;

class FixRemanejamentoSituacaoDiferenteItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $remanejamentos = ArpRemanejamento::select(
            'arp_remanejamento.id as id_remanejamento',
            'unidades.codigo as unidade_solicitante',
            'arp_remanejamento.sequencial'
        )
            ->join('arp_remanejamento_itens', 'arp_remanejamento.id', 'arp_remanejamento_itens.remanejamento_id')
            ->join('unidades', 'arp_remanejamento.unidade_solicitante_id', 'unidades.id')
            ->join('codigoitens', function ($join) {
                $join->on('arp_remanejamento.situacao_id', 'codigoitens.id')
                    ->where('codigoitens.descricao', 'Aguardando aceitação da unidade participante');
            })
            ->join('codigoitens as ciItemRemanejamento', function ($join) {
                $join->on('ciItemRemanejamento.id', 'arp_remanejamento_itens.situacao_id')
                    ->where('ciItemRemanejamento.descricao', 'Aguardando aceitação da unidade gerenciadora');
            })
            ->where('arp_remanejamento.rascunho', false)
            ->groupBy('arp_remanejamento.id', 'unidades.codigo', 'arp_remanejamento.sequencial')
            ->having(DB::raw('count(arp_remanejamento_itens.remanejamento_id)'), '=', 1)
            ->get();
        
        $situacaoCorreta = CodigoItem::where('descricao', 'Aguardando aceitação da unidade gerenciadora')
            ->where('descres', 'status_remanejamento')
            ->first();
        try {
            DB::beginTransaction();
            foreach ($remanejamentos as $remanejamento) {
                $remanejamentoCorrigir = ArpRemanejamento::find($remanejamento->id_remanejamento);
                $remanejamentoCorrigir->situacao_id = $situacaoCorreta->id;
                $remanejamentoCorrigir->save();
                
                $mensagem = 'Id remanejamento corrigido '.$remanejamentoCorrigir->id;
                Log::debug($mensagem);
            }
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            $mensagem = 'Erro ao executar a migration FixRemanejamentoSituacaoDiferenteItem';
            Log::error($mensagem);
            Log::error($exception);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
