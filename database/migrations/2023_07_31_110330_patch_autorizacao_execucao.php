<?php

use App\Models\AutorizacaoExecucao;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PatchAutorizacaoExecucao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = AutorizacaoExecucao::query()
            ->where('data_vigencia_inicio','like','%/%')
            ->orWhere('data_vigencia_fim','like','%/%');
        $data = $query->get()->toArray();

        foreach ($data as $item){
            $itemObj = AutorizacaoExecucao::find($item['id']);
            if(!is_null($item['data_vigencia_inicio']) and str_contains($item['data_vigencia_inicio'],'/')) {
                $dateInicio = date_create_from_format('d/m/Y', $item['data_vigencia_inicio']);
                $itemObj->data_vigencia_inicio = $dateInicio->format('Y-m-d');
            }
            if(!is_null($item['data_vigencia_fim']) and str_contains($item['data_vigencia_fim'],'/')) {
                $dateFim = date_create_from_format('d/m/Y',$item['data_vigencia_fim']);
                $itemObj->data_vigencia_fim = $dateFim->format('Y-m-d');
            }
            $itemObj->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
