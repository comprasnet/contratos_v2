<?php

use App\Models\AutorizacaoExecucaoEntrega;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddColumnContratoIdToAutorizacaoexecucaoEntregaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->integer('contrato_id')->nullable();

            $table->foreign('contrato_id')
                ->references('id')
                ->on('contratos')
                ->onDelete('cascade');
        });

        $entregas = AutorizacaoExecucaoEntrega::withTrashed()->get();

        foreach ($entregas as $entrega) {
            DB::table('autorizacaoexecucao_entrega')
                ->where('id', $entrega->id)
                ->update(['contrato_id' => $entrega->autorizacao->contrato_id]);
        }

        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->integer('contrato_id')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->dropColumn('contrato_id');
        });
    }
}
