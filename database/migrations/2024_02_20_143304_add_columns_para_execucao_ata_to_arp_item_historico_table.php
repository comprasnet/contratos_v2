<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsParaExecucaoAtaToArpItemHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_item_historico', function (Blueprint $table) {
            $table->double('execucao_quantidade_empenhada', 8, 5)
                ->nullable()
                ->comment('Quantidade empenhada durante a execução da ata');
            $table->text('justificativa')
                ->nullable()
                ->comment('Justificativa para a execução da ata');
            $table->unsignedBigInteger('user_id')
                ->nullable()
                ->comment('ID do usuário responsável pela execução da ata');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_item_historico', function (Blueprint $table) {
            Schema::table('arp_item_historico', function (Blueprint $table) {
                $table->dropColumn('execucao_quantidade_empenhada');
                $table->dropColumn('justificativa');
                $table->dropForeign(['user_id']);
                $table->dropColumn('user_id');
            });
        });
    }
}
