<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsPeriodosToAutorizacaoExecucaoItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_itens', function (Blueprint $table) {
            $table->date('periodo_vigencia_inicio')->nullable();
            $table->date('periodo_vigencia_fim')->nullable();
        });

        Schema::table('autorizacaoexecucao_itens_historico', function (Blueprint $table) {
            $table->date('periodo_vigencia_inicio')->nullable();
            $table->date('periodo_vigencia_fim')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_itens', function (Blueprint $table) {
            $table->dropColumn(['periodo_vigencia_inicio', 'periodo_vigencia_fim']);
        });

        Schema::table('autorizacaoexecucao_itens_historico', function (Blueprint $table) {
            $table->dropColumn(['periodo_vigencia_inicio', 'periodo_vigencia_fim']);
        });
    }
}
