<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class InsertPermissionAutorizacaoexecucao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        Permission::create(['name' => 'autorizacaoexecucao_inserir']);
        Permission::create(['name' => 'autorizacaoexecucao_editar']);
        Permission::create(['name' => 'autorizacaoexecucao_deletar']);

        $role = Role::where(['name' => 'Administrador'])->first();
        $role->givePermissionTo('autorizacaoexecucao_inserir');
        $role->givePermissionTo('autorizacaoexecucao_editar');
        $role->givePermissionTo('autorizacaoexecucao_deletar');

        $role = Role::where(['name' => 'Setor Contratos'])->first();
        $role->givePermissionTo('autorizacaoexecucao_inserir');
        $role->givePermissionTo('autorizacaoexecucao_editar');
        $role->givePermissionTo('autorizacaoexecucao_deletar');

        $role = Role::where(['name' => 'Responsável por Contrato'])->first();
        $role->givePermissionTo('autorizacaoexecucao_inserir');
        $role->givePermissionTo('autorizacaoexecucao_editar');
        $role->givePermissionTo('autorizacaoexecucao_deletar');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('autorizacaoexecucao_inserir');
        $role->revokePermissionTo('autorizacaoexecucao_editar');
        $role->revokePermissionTo('autorizacaoexecucao_deletar');

        $role = Role::where(['name' => 'Setor Contratos'])->first();
        $role->revokePermissionTo('autorizacaoexecucao_inserir');
        $role->revokePermissionTo('autorizacaoexecucao_editar');
        $role->revokePermissionTo('autorizacaoexecucao_deletar');

        $role = Role::where(['name' => 'Responsável por Contrato'])->first();
        $role->revokePermissionTo('autorizacaoexecucao_inserir');
        $role->revokePermissionTo('autorizacaoexecucao_editar');
        $role->revokePermissionTo('autorizacaoexecucao_deletar');

        Permission::where(['name' => 'autorizacaoexecucao_inserir'])->forceDelete();
        Permission::where(['name' => 'autorizacaoexecucao_editar'])->forceDelete();
        Permission::where(['name' => 'autorizacaoexecucao_deletar'])->forceDelete();
    }
}
