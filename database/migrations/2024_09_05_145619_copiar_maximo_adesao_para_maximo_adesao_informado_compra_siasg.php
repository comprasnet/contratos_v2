<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use \App\Models\CompraItem;
use \App\Http\Traits\LogTrait;
use \Illuminate\Support\Facades\Log;

class CopiarMaximoAdesaoParaMaximoAdesaoInformadoCompraSiasg extends Migration
{
    use LogTrait;
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $itensCopiar = CompraItem::select([
            'compra_items.id',
            DB::raw("CASE WHEN origem = 1 THEN 'NDC' WHEN origem = 2 THEN 'SIASG' END as origem_compra"),
            'unidades.codigo as unidadeCompra',
            'uSubrogada.codigo as unidadeSubrogada',
            'compras.numero_ano as numero_compra',
            'codigoitens.descres',
            'ciTipoCompra.descricao',
            'maximo_adesao',
            'compra_items.qtd_total',
            'compras.lei'
        ])
            ->join('compras', 'compra_items.compra_id', '=', 'compras.id')
            ->join('unidades', 'compras.unidade_origem_id', '=', 'unidades.id')
            ->leftJoin('unidades as uSubrogada', 'uSubrogada.id', '=', 'compras.unidade_subrrogada_id')
            ->join('codigoitens', 'compras.modalidade_id', '=', 'codigoitens.id')
            ->join('codigoitens as ciTipoCompra', 'compras.tipo_compra_id', '=', 'ciTipoCompra.id')
            ->where('compra_items.situacao', true)
            ->whereNull('compra_items.deleted_at')
            ->whereNull('compras.deleted_at')
            ->where('maximo_adesao', '>', 0)
            ->where('compras.origem', 2)
            ->get();
        try {
            DB::beginTransaction();
            foreach ($itensCopiar as $item) {
                $itemCopiar = CompraItem::find($item->id);
                $itemCopiar->maximo_adesao_informado_compra = $itemCopiar->maximo_adesao;
                $itemCopiar->save();
                $mensagem = "CompraItemId: {$item->id}, maximo_adesao_copiado: {$itemCopiar->maximo_adesao}";
                
                Log::channel('migrations')->info($mensagem);
            }
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            $this->inserirLogCustomizado('migrations', 'error', $exception);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
