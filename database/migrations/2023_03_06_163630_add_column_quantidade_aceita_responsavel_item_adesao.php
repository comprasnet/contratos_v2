<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnQuantidadeAceitaResponsavelItemAdesao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_solicitacao_item', function (Blueprint $table) {
            $table->decimal('quantidade_aprovada',15,5)->nullable();
            $table->integer('responsavel_aprovacao_id')->nullable();

            $table->foreign('responsavel_aprovacao_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_solicitacao_item', function (Blueprint $table) {
            $table->dropColumn('quantidade_aprovada');
            $table->dropColumn('responsavel_aprovacao_id');
        });
    }
}
