<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\Codigo;
use \App\Models\CodigoItem;

class AddSituacaoCanceladaGerenciadoraAdesaoArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::where('descricao', 'Tipo de Ata de Registro de Preços')->first();
        
        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'status_adesao',
            'descricao' => 'Cancelada pela gerenciadora',
            'visivel' => true
        ]);
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = Codigo::where('descricao', 'Tipo de Ata de Registro de Preços')->first();
        CodigoItem::where('codigo_id', $codigo->id)
            ->where('descres', 'status_adesao')->where('descricao', 'Cancelada pela gerenciadora')->forceDelete();
    }
}
