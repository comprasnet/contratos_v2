<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnVigenciaCompraItemFornecedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compra_item_fornecedor', function (Blueprint $table) {
            $table->date('ata_vigencia_inicio')->nullable();
            $table->date('ata_vigencia_fim')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compra_item_fornecedor', function (Blueprint $table) {
            $table->dropColumn('ata_vigencia_inicio');
            $table->dropColumn('ata_vigencia_fim');
        });
    }
}
