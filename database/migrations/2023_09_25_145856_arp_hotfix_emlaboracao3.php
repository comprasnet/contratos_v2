<?php

use App\Models\Arp;
use App\Models\ArpItemHistorico;
use App\Models\CodigoItem;
use App\Models\CompraItemFornecedor;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ArpHotfixEmlaboracao3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $success = $this->getCodigoId('SUCESSO', 'Sucesso');
        $elaboracao = $this->getCodigoId('3', 'Em elaboração');
        $ataDePreco = $this->getCodigoId('0', 'Ata de Registro de Preços');
        $ativa = $this->getCodigoId('7', 'Ativa');

        /* @var Arp[] $arps */
        $arps = Arp::join('envia_dados_pncp as e', function ($join) {
            $join->on('e.pncpable_type', '=', DB::raw("'App\\Models\\Arp'"))
                ->on('e.pncpable_id', '=', 'arp.id');
        })
            ->where('arp.rascunho', false)
            ->whereNotNull('arp.vigencia_final')
            ->whereNotNull('arp.vigencia_inicial')
            ->whereIn('arp.tipo_id', [$elaboracao,$ativa])
            ->where('e.situacao', $success)
            ->select(['arp.*'])
            ->get();

        foreach ($arps as $arp) {
            $arp->tipo_id = $ataDePreco;
            $arp->save();


            CompraItemFornecedor::join('arp_item as a', 'a.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
                ->where('a.arp_id', $arp->id)
                ->whereNull('compra_item_fornecedor.ata_vigencia_inicio')
                ->whereNull('compra_item_fornecedor.ata_vigencia_fim')
                ->whereNull('a.deleted_at')
                ->update([
                    'ata_vigencia_inicio'=>$arp->vigencia_inicial,
                    'ata_vigencia_fim'=>$arp->vigencia_final,
                ]);
        }
    }

    private function getCodigoId($descres, $description)
    {
        $query = CodigoItem::query();
        $query->where('descres', '=', $descres)
            ->where('descricao', '=', $description);
        return $query->first()->id;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
