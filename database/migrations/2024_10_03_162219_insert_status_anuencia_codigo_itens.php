<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertStatusAnuenciaCodigoItens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigo = Codigo::updateOrCreate([
            'descricao' => 'Situação Anuência ARP Fornecedor',
            'visivel' => true
        ]);

        CodigoItem::updateOrCreate([
            'codigo_id' => $codigo->id,
            'descricao' => 'Aguardando Aceitação do Fornecedor',
            'visivel' => true,
            'descres' => 'anuencia_status_1'
        ]);

        CodigoItem::updateOrCreate([
            'codigo_id' => $codigo->id,
            'descricao' => 'Aceita pelo Fornecedor',
            'visivel' => true,
            'descres' => 'anuencia_status_2'
        ]);

        CodigoItem::updateOrCreate([
            'codigo_id' => $codigo->id,
            'descricao' => 'Aceita Parcialmente pelo Fornecedor',
            'visivel' => true,
            'descres' => 'anuencia_status_3'
        ]);

        CodigoItem::updateOrCreate([
            'codigo_id' => $codigo->id,
            'descricao' => 'Negada pelo Fornecedor',
            'visivel' => true,
            'descres' => 'anuencia_status_4'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $codigo = Codigo::where('descricao', 'Situação Anuência ARP Fornecedor')->first();
        $codigo->forceDelete();
    }
}
