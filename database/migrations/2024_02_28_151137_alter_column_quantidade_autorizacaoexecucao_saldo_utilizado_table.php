<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterColumnQuantidadeAutorizacaoexecucaoSaldoUtilizadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_saldo_utilizado', function (Blueprint $table) {
            $table->decimal('quantidade', 25, 17, true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_saldo_utilizado', function (Blueprint $table) {
            $table->integer('quantidade')->change();
        });
    }
}
