<?php

use App\Http\Traits\Arp\ArpAlteracaoCancelamentoTrait;
use App\Models\ArpHistorico;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Arp;
use App\Models\ArpItemHistorico;
use App\Models\CodigoItem;

class UpdateCancelamentoArpItemArp extends Migration
{
    use ArpAlteracaoCancelamentoTrait;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();
        try {
            $cancelledArps = ArpHistorico::distinct('arp_id')
                ->join('codigoitens', 'arp_historico.tipo_id', '=', 'codigoitens.id')
                ->where('codigoitens.descricao', 'Cancelamento de item(ns)')
                ->get();
            foreach ($cancelledArps as $arp) {
                $totalItensAta = count($arp->item);
                $dadosFormulario['arp_id'] = $arp->arp_id;
                $this->verificarCancelamentoArp($dadosFormulario['arp_id'], $totalItensAta);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
