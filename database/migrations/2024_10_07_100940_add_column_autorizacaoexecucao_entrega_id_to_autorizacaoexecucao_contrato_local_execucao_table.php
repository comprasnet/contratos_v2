<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnAutorizacaoexecucaoEntregaIdToAutorizacaoexecucaoContratoLocalExecucaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_contrato_local_execucao', function (Blueprint $table) {
            if (!Schema::hasColumn('autorizacaoexecucao_contrato_local_execucao', 'autorizacaoexecucao_entrega_id')) {
                $table->integer('autorizacaoexecucao_entrega_id')->unsigned()->nullable();
            }
            $table->foreign('autorizacaoexecucao_entrega_id', 'fk_autorizacaoexecucao_entrega')
                ->references('id')->on('autorizacaoexecucao_entrega')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_contrato_local_execucao', function (Blueprint $table) {
            $table->dropForeign('fk_autorizacaoexecucao_entrega');
            $table->dropColumn('autorizacaoexecucao_entrega_id');
        });
    }
}
