<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftdeleArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('arp_arquivos', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('arp_autoridade_signataria', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('arp_autoridade_signataria_historico', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('arp_gestor', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('arp_historico', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('arp_item', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('arp_solicitacao_item', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('arp_unidades', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
