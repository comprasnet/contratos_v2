<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutorizacaoexecucaoHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autorizacaoexecucao_historico', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('autorizacaoexecucoes_id');
            $table->unsignedInteger('usuario_responsavel_id');
            $table->unsignedInteger('tipo_id_antes')->nullable();
            $table->unsignedInteger('tipo_id_depois')->nullable();
            $table->string('processo_antes', 25)->nullable();
            $table->string('processo_depois', 25)->nullable();
            $table->string('numero_antes', 15)->nullable();
            $table->string('numero_depois', 15)->nullable();
            $table->date('data_assinatura_antes')->nullable();
            $table->date('data_assinatura_depois')->nullable();
            $table->text('informacoes_complementares_antes')->nullable();
            $table->text('informacoes_complementares_depois')->nullable();
            $table->date('data_vigencia_inicio_antes')->nullable();
            $table->date('data_vigencia_inicio_depois')->nullable();
            $table->date('data_vigencia_fim_antes')->nullable();
            $table->date('data_vigencia_fim_depois')->nullable();

            $table->text('justificativa_motivo')->nullable();

            $table->timestamps();

            $table->foreign('autorizacaoexecucoes_id')
                ->references('id')
                ->on('autorizacaoexecucoes')
                ->cascadeOnDelete();
            $table->foreign('usuario_responsavel_id')
                ->references('id')
                ->on('users')
                ->cascadeOnDelete();
            $table->foreign('tipo_id_antes')
                ->references('id')
                ->on('codigoitens')
                ->nullOnDelete();
            $table->foreign('tipo_id_depois')
                ->references('id')
                ->on('codigoitens')
                ->nullOnDelete();
        });

        Schema::create('autorizacaoexecucao_unidade_requisitante_historico', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('autorizacaoexecucoes_id');
            $table->foreignId('autorizacaoexecucoes_historico_id')->cascadeOnDelete();
            $table->enum('tipo_operacao', ['insert', 'update', 'delete'])->nullable();
            $table->timestamps();

            $table->foreign('autorizacaoexecucoes_id')
                ->references('id')
                ->on('autorizacaoexecucoes')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autorizacaoexecucao_historico');
        Schema::dropIfExists('autorizacaoexecucao_unidade_requisitante_historico');
    }
}
