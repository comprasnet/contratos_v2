<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnMotivoJustificativaFornecedorOnArpSolicitacaoItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_solicitacao_item', function (Blueprint $table) {
            $table->text('motivo_justificativa_fornecedor')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_solicitacao_item', function (Blueprint $table) {
            $table->dropColumn('motivo_justificativa_fornecedor');
        });
    }
}
