<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnAmparoLegalRenovacaoQuantitativoAlteracaoArpHistorico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_historico', function (Blueprint $table) {
            $table->boolean('quantitativo_renovado_vigencia')->default(false);
            $table->string('amparo_legal_renovacao_quantitativo')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_historico', function (Blueprint $table) {
            $table->dropColumn('quantitativo_renovado_vigencia')->default(false);
            $table->dropColumn('amparo_legal_renovacao_quantitativo');
        });
    }
}
