<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsTrpTrdToModelsSignatariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('models_signatarios', function (Blueprint $table) {
            $table->dropColumn('model_autorizacaoexecucao_entrega_id');

            $table->integer('model_autorizacaoexecucao_entrega_trp_id')->nullable();
            $table->integer('model_autorizacaoexecucao_entrega_trd_id')->nullable();

            $table->foreign('model_autorizacaoexecucao_entrega_trp_id')
                ->references('id')
                ->on('autorizacaoexecucao_entrega')
                ->onDelete('cascade');
            $table->foreign('model_autorizacaoexecucao_entrega_trd_id')
                ->references('id')
                ->on('autorizacaoexecucao_entrega')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('models_signatarios', function (Blueprint $table) {
            $table->dropColumn('model_autorizacaoexecucao_entrega_trp_id');
            $table->dropColumn('model_autorizacaoexecucao_entrega_trd_id');

            $table->integer('model_autoracaoexecucao_entrega_id')->nullable();
            $table->foreign('model_autoracaoexecucao_entrega_id')
                ->references('id')
                ->on('autorizacaoexecucao_entrega')
                ->onDelete('cascade');
        });
    }
}
