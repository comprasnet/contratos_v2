<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\ArpItemHistorico;
use \App\Models\CompraItemFornecedor;
use \Illuminate\Support\Facades\Log;

class FixValorUnitarioAlteracaoArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $itensProblema = ArpItemHistorico::select(
            'unidades.codigo',
            'arp.numero',
            'arp.ano',
            'arp.id',
            'compra_item_fornecedor.id as compra_item_id',
            'arp_item_historico.valor'
        )
        ->join('arp_historico', 'arp_historico.id', '=', 'arp_item_historico.arp_historico_id')
        ->join('arp_alteracao', 'arp_alteracao.id', '=', 'arp_historico.arp_alteracao_id')
        ->join('arp_item', 'arp_item.id', '=', 'arp_item_historico.arp_item_id')
        ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
        ->join('arp', 'arp_item.arp_id', '=', 'arp.id')
        ->join('unidades', 'arp.unidade_origem_compra_id', '=', 'unidades.id')
        ->where('arp_alteracao.rascunho', false)
        ->where('arp_item_historico.valor_alterado', true)
        ->where('compra_item_fornecedor.valor_unitario', 0)
        ->get();
        
        foreach ($itensProblema as $item) {
            $compraItemFornecedor = CompraItemFornecedor::find($item->compra_item_id);
            $compraItemFornecedor->valor_unitario =$item->valor;
            $compraItemFornecedor->save();
            $mensagem =
                "ID compra item fornecedor atualizado migration FixValorUnitarioAlteracaoArp: {$item->compra_item_id}";
            Log::info($mensagem);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
