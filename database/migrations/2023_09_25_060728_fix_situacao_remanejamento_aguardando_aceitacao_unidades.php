<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixSituacaoRemanejamentoAguardandoAceitacaoUnidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Models\CodigoItem::where(
            [
                'descricao'=> 'Aguardando aceitação da unidade participante e gerenciadora',
                'descres' => 'status_remanejamento'
            ]
        )->update(['descricao'=> 'Aguardando aceitação']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\CodigoItem::where(
            [
                'descricao'=> 'Aguardando aceitação',
                'descres' => 'status_remanejamento'
            ]
        )->update(['descricao'=> 'Aguardando aceitação da unidade participante e gerenciadora']);
    }
}
