<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertDeleteCascateArpHistorico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     /**
     * Essa migration está sendo comentada 
     * o conteúdo refrenete ao cascade migrado para 2023_05_23_153203_create_table_arp_item_historico.php
     * 
     * Durante execução das migrations percebeu-se que essa migration de 20/03/2023 apaga uma foreignkey
     * de uma tabela que só foi criada em 23/05/2023 (2023_05_23_153203_create_table_arp_item_historico.php)
     * Assim sendo, a criação do cascade está sendo comentada e adicionada no arquiovo citado
     *
     */
    public function up()
    {
    //    Schema::table('arp_item_historico', function (Blueprint $table) {
    //        $table->dropForeign('arp_item_historico_arp_historico_id_foreign');
    //        $table->foreign('arp_historico_id')->references('id')->on('arp_historico')->onDelete('cascade')->change();
    //    });

        Schema::table('arp_historico', function (Blueprint $table) {
            $table->text('objeto_alteracao')->change()->nullable();
            // $table->text('descricao_anexo')->change()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_historico', function (Blueprint $table) {
            $table->dropColumn('objeto_alteracao');
        //    $table->dropColumn('descricao_anexo');
        });
    }
}
