<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertCodigoItensUnidadesMedidas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $codigoMaterial = Codigo::create([
            'descricao' => 'Material',
            'visivel' => false
        ]);

        $unidadesMaterial = [
            'Caixa 10 Unidade',
            'Caixa 12 Unidade',
            'Caixa 20 Unidade',
            'Caixa 24 Unidade',
            'Caixa 6 Unidade',
            'Caixa 30 Unidade',
            'Caixa 36 Unidade',
            'Caixa 48 Unidade',
            'Caixa 50 Unidade',
            'Caixa 40 Unidade',
            'Caixa 100 Unidade',
            'Conjunto',
            'Frasco-Ampola',
            'Metro',
            'Metro Cúbico',
            'Peça 1 Unidade',
            'Rolo 3 Metro',
            'Rolo 30 Metro',
            'Rolo 10 Metro',
            'Rolo 20 Metro',
            'Rolo 50 Metro',
            'Rolo 25 Metro',
            'Rolo 15 Metro',
            'Pacote 10 Unidade',
            'Pacote 100 Unidade',
            'Pacote 50 Unidade',
            'Pacote 500 Unidade',
            'Pacote 1000 Unidade',
            'Caixa 100 Grama',
            'Quilograma',
            'Caixa 2.1 Quilograma',
            'Caixa 2.02 Quilograma',
            'Caixa 4 Quilograma',
            'Caixa 5 Quilograma',
            'Caixa 6 Quilograma',
            'Caixa 10 Folha',
            'Caixa 20 Folha',
            'Caixa 100 Folha',
            'Caixa 500 Folha',
            'Pacote 5 Quilograma',
            'Caixa 6.2 Quilograma',
            'Caixa 9 Quilograma',
            'Caixa 15 Quilograma',
            'Caixa 40 Quilograma',
            'Embalagem 8 Grama',
            'Embalagem 2 Unidade',
            'Embalagem 7 Grama',
            'Embalagem 15 Grama',
            'Embalagem 75 Unidade',
            'Embalagem 250 Grama',
            'Embalagem 10 Unidade',
            'Embalagem 9 Grama',
            'Pacote 68 Grama',
            'Pacote 40 Grama',
            'Pacote 35 Grama',
            'Pacote 250 Grama',
            'Pacote 75 Grama',
            'Pacote 156 Grama',
            'Pacote 65 Grama',
            'Pacote 185 Grama',
            'Pacote 400 Grama',
            'Pacote 25 Grama',
            'Pacote 24 Grama',
            'Pacote 160 Grama',
            'Pacote 120 Grama',
            'Pacote 800 Grama',
            'Pacote 50 Grama',
            'Pacote 145 Grama',
            'Pacote 500 Grama',
            'Pacote 300 Grama',
            'Pacote 200 Grama',
            'Pacote 165 Grama',
            'Pacote 32 Grama',
            'Pacote 110 Grama',
            'Pacote 360 Grama',
            'Pacote 26 Grama',
            'Pacote 144 Grama',
            'Pacote 375 Grama',
            'Pacote 170 Grama',
            'Pacote 60 Grama',
            'Pacote 70 Grama',
            'Pacote 150 Grama',
            'Pacote 279 Grama',
            'Pacote 42 Grama',
            'Pacote 180 Grama',
            'Pacote 140 Grama',
            'Pacote 30 Grama',
            'Pacote 9 Grama',
            'Pacote 500 Folha',
            'Pacote 100 Folha',
            'Pacote 175 Folha',
            'Pacote 200 Folha',
            'Pacote 50 Folha',
            'Pacote 10 Folha',
            'Pacote 150 Folha',
            'Pacote 125 Folha',
            'Pacote 100 Folha',
            'Sachê 9 Grama',
            'Par',
            'Tambor 5 Litro',
            'Tubete 0.48 Milimetro',
            'Seringa 0.5 Milimetro',
            'Seringa 1.5 Milimetro',
            'Seringa 0.75 Milimetro',
            'Seringa 0.5 Milimetro',
            'Seringa 0.36 Milimetro',
            'Seringa 1.08 Milimetro',
            'Seringa 2.16 Milimetro',
            'Seringa 0.48 Milimetro',
            'Seringa 0.72 Milimetro',
            'Seringa 1.44 Milimetro',
            'Lata 450 Grama',
            'Lata 500 Grama',
            'Lata 5 Litro',
            'Grama',
        ];

        foreach ($unidadesMaterial as $unidade) {
            CodigoItem::create([
                'codigo_id' => $codigoMaterial->id,
                'descres' => strtoupper(substr($unidade, 0, 20)),
                'descricao' => $unidade,
                'visivel' => false
            ]);
        }

        $codigoServicos = Codigo::create([
            'descricao' => 'Serviços',
            'visivel' => false
        ]);

        $unidadesServicos = [
            'Unidade',
            'Posto',
            'Metro Quadrado',
            'Hora Serviço Técnico',
            'Ponto de Função',
            'UND Serviço Técnico',
            'Perfil Profissional',
            'Sprint',
            'Mega Bits/Segundo',
            'Giga Bits/Segundo',
            'Página',
            'Páginas Mês',
            'Cópia',
        ];

        foreach ($unidadesServicos as $unidade) {
            CodigoItem::create([
                'codigo_id' => $codigoServicos->id,
                'descres' => strtoupper(substr($unidade, 0, 20)),
                'descricao' => $unidade,
                'visivel' => false
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Codigo::where('descricao', 'Material')->delete();
        Codigo::where('descricao', 'Serviços')->delete();
    }
}
