<?php

use App\Models\AutorizacaoExecucaoEntrega;
use Illuminate\Database\Migrations\Migration;

class FixAlterarNumeroEntregas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $entregaPorContratos = AutorizacaoExecucaoEntrega::select('contrato_id')->groupBy('contrato_id')->get();

        $entregaPorContratos->each(function ($contrato) {
            $entregas = AutorizacaoExecucaoEntrega::where('contrato_id', $contrato->contrato_id)
                ->orderBy('created_at')
                ->get();

            $sequencial = 1;
            $entregas->each(function ($entrega) use (&$sequencial) {
                $entrega->update(['sequencial' => $sequencial]);
                $sequencial++;
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $entregaPorAutorizacao = AutorizacaoExecucaoEntrega::select('autorizacaoexecucao_id')
            ->groupBy('autorizacaoexecucao_id')
            ->get();


        $entregaPorAutorizacao->each(function ($autorizacao) {
            $entregas = AutorizacaoExecucaoEntrega::where(
                'autorizacaoexecucao_id',
                $autorizacao->autorizacaoexecucao_id
            )
                ->orderBy('created_at')
                ->get();

            $sequencial = 1;
            $entregas->each(function ($entrega) use (&$sequencial) {
                $entrega->update(['sequencial' => $sequencial]);
                $sequencial++;
            });
        });
    }
}
