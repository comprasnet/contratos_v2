<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToAutorizacaoexecucaoEntregaItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->text('introducao_trp')->nullable();
            $table->text('recebimento')->nullable();
            $table->text('nivel_qualidade_entrega')->nullable();
        });

        Schema::table('autorizacaoexecucao_entrega_itens', function (Blueprint $table) {
            $table->date('mes_ano_competencia')->nullable();
            $table->string('processo_sei')->nullable();
            $table->date('data_inicio')->nullable();
            $table->date('data_fim')->nullable();
            $table->time('horario')->nullable();
            $table->string('local')->nullable();
            $table->string('ocorrencia')->nullable();
            $table->string('requisitante')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->dropColumn('introducao_trp');
            $table->dropColumn('recebimento');
            $table->dropColumn('nivel_qualidade_entrega');
        });

        Schema::table('autorizacaoexecucao_entrega_itens', function (Blueprint $table) {
            $table->dropColumn('mes_ano_competencia');
            $table->dropColumn('processo_sei');
            $table->dropColumn('data_inicio');
            $table->dropColumn('data_fim');
            $table->dropColumn('horario');
            $table->dropColumn('local');
            $table->dropColumn('ocorrencia');
            $table->dropColumn('requisitante');
        });
    }
}
