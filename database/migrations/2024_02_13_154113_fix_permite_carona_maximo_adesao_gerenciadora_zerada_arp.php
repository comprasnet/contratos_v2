<?php

use \App\Api\Externa\ApiSiasg;
use Illuminate\Database\Migrations\Migration;
use \App\Models\CompraItem;
use \Illuminate\Support\Facades\Log;

class FixPermiteCaronaMaximoAdesaoGerenciadoraZeradaArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $itemCompraSemCaronaMaximoAdesao = CompraItem::select(
            'compras.id as compra_id',
            'compras.numero_ano as numero_compra',
            'unidades.codigo as codigo_unidade',
            'ci2.descres as modalidade',
            'unidadeBeneficiaria.codigo as codigo_unidade_beneficiaria',
            'compra_items.numero',
            'compra_items.id as compra_item_id'
        )
            ->join('compras', function ($join) {
                $join->on('compras.id', '=', 'compra_items.compra_id')
                    ->where('compra_items.situacao', true);
            })
            ->join('compra_item_unidade', function ($join) {
                $join->on('compra_item_unidade.compra_item_id', '=', 'compra_items.id')
                    ->where('compra_item_unidade.situacao', true);
            })
            ->join('unidades', 'unidades.id', '=', 'compras.unidade_origem_id')
            ->leftjoin('unidades as unidadeBeneficiaria', 'unidadeBeneficiaria.id', '=', 'compras.uasg_beneficiaria_id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compras.modalidade_id')
            ->join('codigoitens as ci1', function ($join) {
                $join->on('ci1.id', '=', 'compras.tipo_compra_id')
                    ->where('ci1.descricao', 'SISRP');
            })
            ->join('codigoitens as ci2', 'ci2.id', '=', 'compras.modalidade_id')
            ->where('compra_item_unidade.quantidade_autorizada', 0)
            ->where('tipo_uasg', 'G')
            ->where('compras.lei', 'LEI14133')
            ->where(function ($query) {
                $query->whereNull('compra_items.permite_carona')
                    ->orWhereNull('compra_items.maximo_adesao');
            })
            ->orderBy('compras.numero_ano')
            ->get();

        $apiSiasg = new ApiSiasg();

        foreach ($itemCompraSemCaronaMaximoAdesao as $itemCompra) {
            $unidade = $itemCompra->codigo_unidade;
            $modalidade = $itemCompra->modalidade;
            $numeroAno = str_replace("/", "", $itemCompra->numero_compra);

            $parametros = [
                'compra' => $unidade.$modalidade.$numeroAno,
                'item' => $itemCompra->numero
            ];
            $consultaAdesaoMaximo =  json_decode($apiSiasg->executaConsulta('ITEMCOMPRASISRP', $parametros));

            if (empty($consultaAdesaoMaximo->data)) {
                continue;
            }

            $maximoAdesao  =  $consultaAdesaoMaximo->data->maximoAdesao;
            $permiteCarona =  $consultaAdesaoMaximo->data->permiteCarona;

            $mensagemLogInfo = "Id item atualizado: {$itemCompra->compra_item_id}";
            Log::info($mensagemLogInfo);

            $itemSalvoBanco = CompraItem::find($itemCompra->compra_item_id);

            $itemSalvoBanco->maximo_adesao = $maximoAdesao;
            $itemSalvoBanco->permite_carona = $permiteCarona;

            $itemSalvoBanco->save();
        }
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
