<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\CompraItemUnidade;
use \App\Http\Traits\CompraTrait;
use \Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FixQtdAutorizadaSaldoUnidadeIssue679 extends Migration
{
    use CompraTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();
            $ciuReceberCriterio1Item01 = $this->recuperarCompraItemUnidade(6876835);

            if (!empty($ciuReceberCriterio1Item01)) {
                $this->salvarSaldoAtualizado($ciuReceberCriterio1Item01);
            }

            $ciuReceberCriterio1Item36Unidade160413 = $this->recuperarCompraItemUnidade(6876732);

            if (!empty($ciuReceberCriterio1Item36Unidade160413)) {
                $ciuReceberCriterio1Item36Unidade160413 = $this->retirarQuantidadeAutorizada(
                    $ciuReceberCriterio1Item36Unidade160413,
                    68
                );
                $this->salvarSaldoAtualizado($ciuReceberCriterio1Item36Unidade160413);
            }

            $ciuReceberCriterio1Item66 = $this->recuperarCompraItemUnidade(6877465);

            if (!empty($ciuReceberCriterio1Item66)) {
                $this->salvarSaldoAtualizado($ciuReceberCriterio1Item66);
            }

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            Log::error($exception);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    private function recuperarCompraItemUnidade(int $id)
    {
        return CompraItemUnidade::find($id);
    }

    private function incluirQuantidadeAutorizada(CompraItemUnidade $compraItemUnidade, int $quantidadeAumentar)
    {
        $compraItemUnidade->quantidade_autorizada += $quantidadeAumentar;
        $compraItemUnidade->save();

        return $compraItemUnidade;
    }

    private function calcularSaldo(int $compraItemId, int $unidadeAutorizadaId, int $compraItemUnidadeId)
    {
        return $this->retornaSaldoAtualizado($compraItemId, $unidadeAutorizadaId, $compraItemUnidadeId)->saldo;
    }

    private function salvarSaldoAtualizado(CompraItemUnidade $compraItemUnidade)
    {
        $saldoAtualizado = $this->calcularSaldo(
            $compraItemUnidade->compra_item_id,
            $compraItemUnidade->unidade_id,
            $compraItemUnidade->id
        );

        $compraItemUnidade->quantidade_saldo = $saldoAtualizado;
        $compraItemUnidade->save();

        return $compraItemUnidade;
    }

    private function retirarQuantidadeAutorizada(CompraItemUnidade $compraItemUnidade, int $quantidadeReduzir)
    {
        $compraItemUnidade->quantidade_autorizada -= $quantidadeReduzir;
        $compraItemUnidade->save();

        return $compraItemUnidade;
    }
}
