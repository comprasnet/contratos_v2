<?php

use App\Models\Codigo;
use App\Models\CodigoItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntregasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entregas', function (Blueprint $table) {
            $table->id();
            $table->integer('contrato_id');
            $table->integer('situacao_id');
            $table->string('numero');
            $table->boolean('rascunho')->default(false);
            $table->decimal('valor_total')->nullable();
            $table->date('mes_ano_referencia')->nullable();
            $table->date('data_inicio')->nullable();
            $table->date('data_fim')->nullable();
            $table->date('data_entrega')->nullable();
            $table->date('data_prevista_trp')->nullable();
            $table->date('data_prevista_trd')->nullable();
            $table->text('informacoes_complementares')->nullable();
            $table->boolean('originado_sistema_externo')->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('situacao_id')
                ->references('id')
                ->on('codigoitens');

            $table->foreign('contrato_id')
                ->references('id')
                ->on('contratos');
        });

        Schema::create('locais_execucao', function (Blueprint $table) {
            $table->unsignedInteger('modelable_id');
            $table->string('modelable_type');
            $table->unsignedInteger('contrato_local_execucao_id');

            $table->foreign('contrato_local_execucao_id')
                ->references('id')
                ->on('contrato_local_execucao')
                ->onDelete('cascade');
        });

        Schema::create('entrega_autorizacaoexecucao', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('autorizacaoexecucao_id');
            $table->unsignedInteger('entrega_id');

            $table->foreign('entrega_id')
                ->references('id')
                ->on('entregas')
                ->cascadeOnDelete();

            $table->foreign('autorizacaoexecucao_id')
                ->references('id')
                ->on('autorizacaoexecucoes')
                ->cascadeOnDelete();
        });

        Schema::create('entrega_itens', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('entrega_id');
            $table->unsignedInteger('itemable_id');
            $table->string('itemable_type');
            $table->decimal('quantidade_informada', 30, 17);
            $table->decimal('valor_glosa', 19)->default(0);
            $table->timestamps();

            $table->foreign('entrega_id')
                ->references('id')
                ->on('entregas')
                ->cascadeOnDelete();
        });

        $codigo = Codigo::create([
            'descricao' => 'Situação Entrega',
            'visivel' => true,
        ]);

        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'entrega_status_1',
            'descricao' => 'Em Elaboração',
        ]);
        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'entrega_status_2',
            'descricao' => 'Em Análise',
        ]);
        CodigoItem::create([
                'codigo_id' => $codigo->id,
                'descres' => 'entrega_status_3',
                'descricao' => 'Em Execução',
        ]);
        CodigoItem::create([
            'codigo_id' => $codigo->id,
            'descres' => 'entrega_status_4',
            'descricao' => 'Concluída',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locais_execucao');
        Schema::dropIfExists('entrega_itens');
        Schema::dropIfExists('entrega_autorizacaoexecucao');
        Schema::dropIfExists('entregas');

        $codigo = Codigo::where('descricao', 'Situação Entrega')->firstOrFail();
        $codigo->forceDelete();
    }
}
