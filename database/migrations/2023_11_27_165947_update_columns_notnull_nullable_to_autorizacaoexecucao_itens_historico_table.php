<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnsNotnullNullableToAutorizacaoexecucaoItensHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_itens_historico', function (Blueprint $table) {
            $table->integer('unidade_medida_id')->nullable()->change();
            $table->decimal('quantidade', 30, 17)->nullable()->change();
            $table->boolean('subcontratacao')->nullable()->change();
            $table->decimal('valor_unitario', 17)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_itens_historico', function (Blueprint $table) {
            $table->integer('unidade_medida_id')->change();
            $table->decimal('quantidade', 30, 17)->change();
            $table->boolean('subcontratacao')->change();
            $table->decimal('valor_unitario', 17)->change();
        });
    }
}
