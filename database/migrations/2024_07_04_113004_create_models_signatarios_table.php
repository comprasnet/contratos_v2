<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelsSignatariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Drop na tabela devido a dudança de estrategia na persistência de dados dos signatários
        Schema::dropIfExists('autorizacaoexecucao_signatarios');

        // Para não gerar 4 colunas polimorficas para atender tanto os módulos quanto os signatários,
        //  foi acordado entre os devs a inclusão explicita das colunas fk de cada model.
        Schema::create('models_signatarios', function (Blueprint $table) {
            $table->id();
            $table->integer('model_autorizacaoexecucao_id')->nullable();
            $table->integer('model_autorizacaoexecucao_historico_id')->nullable();
            $table->integer('model_autorizacaoexecucao_entrega_id')->nullable();
            $table->integer('signatario_contratoresponsavel_id')->nullable();
            $table->integer('signatario_contratopreposto_id')->nullable();
            $table->integer('arquivo_generico_id');
            $table->integer('status_assinatura_id');
            $table->float('posicao_x_assinatura');
            $table->float('posicao_y_assinatura');
            $table->integer('pagina_assinatura');
            $table->dateTime('data_operacao')->nullable();
            $table->timestamps();

            $table->foreign('model_autorizacaoexecucao_id')
                ->references('id')
                ->on('autorizacaoexecucoes')
                ->onDelete('cascade');
            $table->foreign('model_autorizacaoexecucao_historico_id')
                ->references('id')
                ->on('autorizacaoexecucao_historico')
                ->onDelete('cascade');
            $table->foreign('model_autorizacaoexecucao_entrega_id')
                ->references('id')
                ->on('autorizacaoexecucao_entrega')
                ->onDelete('cascade');
            $table->foreign('signatario_contratoresponsavel_id')
                ->references('id')
                ->on('contratoresponsaveis')
                ->onDelete('cascade');
            $table->foreign('signatario_contratopreposto_id')
                ->references('id')
                ->on('contratopreposto')
                ->onDelete('cascade');
            $table->foreign('status_assinatura_id')
                ->references('id')
                ->on('codigoitens')
                ->onDelete('cascade');
            $table->foreign('arquivo_generico_id')
                ->references('id')
                ->on('arquivo_generico')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('models_signatarios');
    }
}
