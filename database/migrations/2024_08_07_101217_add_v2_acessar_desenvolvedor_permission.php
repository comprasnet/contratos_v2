<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddV2AcessarDesenvolvedorPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Limpa o cache de permissões
        app()['cache']->forget('spatie.permission.cache');

        // Cria as permissões
        $permissions = [
            'V2_acessar_desenvolvedor',
            'V2_acessar_desenvolvedor_log',
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission, 'guard_name' => 'web']);
        }

        // Recupera a role desenvolvedor
        $role = Role::where('name', 'desenvolvedor')->first();

        // Associa as permissões à role, se ela existir
        if ($role) {
            foreach ($permissions as $permission) {
                $role->givePermissionTo($permission);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Recupera a role desenvolvedor
        $role = Role::where('name', 'desenvolvedor')->first();

        // Revoga as permissões da role, se ela existir
        $permissions = [
            'V2_acessar_desenvolvedor',
            'V2_acessar_desenvolvedor_log',
        ];

        if ($role) {
            foreach ($permissions as $permission) {
                $role->revokePermissionTo($permission);
            }
        }

        // Deleta as permissões
        Permission::whereIn('name', $permissions)->delete();
    }
}
