<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableRemanejamento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arp_remanejamento', function (Blueprint $table) {
            $table->id();
            $table->integer("unidade_solicitante_id");
            $table->integer("arp_id");
            $table->integer("sequencial");
            $table->integer("situacao_id");

            $table->boolean('remanejamento_entre_unidade_estado_df_municipio_distinto')->nullable();
            $table->boolean('fornecedor_de_acordo_novo_local')->nullable();

            $table->boolean('rascunho');
            $table->dateTime("data_envio_solicitacao")->nullable();

            $table->foreign('arp_id')->references('id')->on('arp');

            $table->foreign('unidade_solicitante_id')->references('id')->on('unidades');
            $table->foreign('situacao_id')->references('id')->on('codigoitens');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('arp_remanejamento_itens', function (Blueprint $table) {
            $table->id();
            $table->integer("remanejamento_id");
            $table->decimal("quantidade_solicitada", 15, 5);

            $table->decimal("quantidade_aprovada_participante", 15, 5)->nullable();
            $table->dateTime("data_aprovacao_participante")->nullable();

            $table->decimal("quantidade_aprovada_gerenciadora", 15, 5)->nullable();
            $table->dateTime("data_aprovacao_gerenciadora")->nullable();

            $table->integer("id_item_unidade");

            $table->integer("id_usuario_solicitante");
            $table->integer("id_usuario_aprovacao_participante")->nullable();
            $table->integer("id_usuario_aprovacao_gerenciadora")->nullable();

            $table->foreign('remanejamento_id')->references('id')->on('arp_remanejamento')->onDelete('cascade');

            $table->foreign('id_item_unidade')->references('id')->on('compra_item_unidade');

            $table->foreign('id_usuario_solicitante')->references('id')->on('users');
            $table->foreign('id_usuario_aprovacao_participante')->references('id')->on('users');
            $table->foreign('id_usuario_aprovacao_gerenciadora')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arp_remanejamento_itens');
        Schema::dropIfExists('arp_remanejamento');
    }
}
