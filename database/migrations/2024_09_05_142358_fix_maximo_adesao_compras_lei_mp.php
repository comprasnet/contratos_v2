<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use \App\Models\CompraItem;
use \App\Http\Traits\LogTrait;
use \Illuminate\Support\Facades\Log;

class FixMaximoAdesaoComprasLeiMp extends Migration
{
    use LogTrait;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $itensCorrecao = CompraItem::select([
                'compra_items.id',
                'maximo_adesao',
                'compra_items.qtd_total'
            ])
            ->join('compras', 'compra_items.compra_id', '=', 'compras.id')
            ->join('unidades', 'compras.unidade_origem_id', '=', 'unidades.id')
            ->leftJoin('unidades as uSubrogada', 'uSubrogada.id', '=', 'compras.unidade_subrrogada_id')
            ->join('codigoitens', 'compras.modalidade_id', '=', 'codigoitens.id')
            ->join('codigoitens as ciTipoCompra', 'compras.tipo_compra_id', '=', 'ciTipoCompra.id')
            ->where('compra_items.situacao', true)
            ->whereNull('compra_items.deleted_at')
            ->whereNull('compras.deleted_at')
            ->where('maximo_adesao', '>', 0)
            ->where('compras.lei', 'MP1221')
            ->get();
        
        try {
            DB::beginTransaction();
            foreach ($itensCorrecao as $item) {
                $itemAtualizar = CompraItem::find($item->id);
                $MaximoAdesaoCorreto = $item->qtd_total * 5;
                
                $mensagem = "CompraItemId: {$item->id}, maximo_adesao_atual: {$itemAtualizar->maximo_adesao}, ".
                            "novo: {$MaximoAdesaoCorreto}";
                
                $itemAtualizar->maximo_adesao = $MaximoAdesaoCorreto;
                $itemAtualizar->save();
                
                Log::channel('migrations')->info($mensagem);
            }
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            $this->inserirLogCustomizado('migrations', 'error', $exception);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
