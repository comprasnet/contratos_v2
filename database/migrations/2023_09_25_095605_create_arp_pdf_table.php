<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArpPdfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arp_pdf', function (Blueprint $table) {
            $table->id();
            $table->integer('arp_id');
            $table->string('caminho_pdf')->nullable();
            $table->boolean('gerado')->default(false);
            $table->timestamps();
            $table->foreign('arp_id')->references('id')->on('arp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arp_pdf');
    }
}
