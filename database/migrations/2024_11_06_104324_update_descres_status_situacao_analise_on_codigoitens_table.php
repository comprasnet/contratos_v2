<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CodigoItem;

class UpdateDescresStatusSituacaoAnaliseOnCodigoitensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->alteraDescresCodigoItem('Aceita', '1', 'situacao_analise_act');
        $this->alteraDescresCodigoItem(
            'Aceito Parcial',
            '6',
            'situacao_analise_acp'
        );
        $this->alteraDescresCodigoItem('Negada', '4', 'situacao_analise_ngd');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->alteraDescresCodigoItem('Aceita', 'situacao_analise_act', '1');
        $this->alteraDescresCodigoItem(
            'Aceito Parcial',
            'situacao_analise_acp',
            '6'
        );
        $this->alteraDescresCodigoItem('Negada', 'situacao_analise_ngd', '4');
    }

    /**
     * Altera o valor do campo 'descres' em registros da tabela CodigoItem
     * com base nos parâmetros de 'descricao' e 'descres' fornecidos.
     *
     * @param string $descricao A descrição do item para filtrar o registro.
     * @param string $descres O valor atual do campo 'descres' para filtrar o registro.
     * @param string $informacaoUpdate O novo valor que será atribuído ao campo 'descres'.
     * @return int O número de registros afetados pela atualização.
     */
    private function alteraDescresCodigoItem(string $descricao, string $descres, string $informacaoUpdate): int
    {
        return CodigoItem::where('descricao', $descricao)
            ->where('descres', $descres)
            ->update(['descres' => $informacaoUpdate]);
    }
}
