<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \App\Models\Arp;
use \App\Http\Traits\Arp\ArpTrait;

class FixAtaSituacaoPublicando extends Migration
{
    use ArpTrait;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $arpSituacaoPublicando = Arp::join("codigoitens", "codigoitens.id", "arp.tipo_id")
                                    ->whereIN("codigoitens.descricao", ['Publicando PNCP', 'Enviar PNCP'])
                                    ->select('arp.*')
                                    ->get();
        $idSituacaoErroPNCP = $this->getSituacao('Erro Publicar');

        foreach ($arpSituacaoPublicando as $arp) {
            $arp->tipo_id = $idSituacaoErroPNCP;
            $arp->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
