<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutorizacaoexecucaoEntregaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autorizacaoexecucao_entrega', function (Blueprint $table) {
            $table->id();
            $table->integer('autorizacaoexecucao_id');
            $table->integer('situacao_id')->nullable();
            $table->integer('sequencial')->nullable();
            $table->integer('ano')->nullable();
            $table->boolean('rascunho')->default(false)->nullable();
            $table->decimal('valor_entrega', 11, 2)->nullable();
            $table->date('data_entrega')->nullable()->nullable();
            $table->text('informacoes_complementares')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('autorizacaoexecucao_id')->references('id')
                ->on('autorizacaoexecucoes')->cascadeOnDelete();
            $table->foreign('situacao_id')->references('id')
                ->on('codigoitens')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autorizacaoexecucao_entrega');
    }
}
