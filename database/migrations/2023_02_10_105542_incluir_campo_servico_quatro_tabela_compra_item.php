<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IncluirCampoServicoQuatroTabelaCompraItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compra_items', function (Blueprint $table) {
            $table->decimal('maximo_adesao',15,5)->nullable();
            $table->boolean('permite_carona')->nullable();
        });

        Schema::table('compra_item_unidade', function (Blueprint $table) {
            $table->dropColumn('maximo_adesao');
            $table->dropColumn('permite_carona');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compra_items', function (Blueprint $table) {
            $table->dropColumn('maximo_adesao');
            $table->dropColumn('permite_carona');
        });
    }
}
