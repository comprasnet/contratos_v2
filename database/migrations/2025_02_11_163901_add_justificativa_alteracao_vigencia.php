<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJustificativaAlteracaoVigencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_historico', function (Blueprint $table) {
            $table->text('justificativa_alteracao_vigencia')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_historico', function (Blueprint $table) {
            $table->dropColumn('justificativa_alteracao_vigencia');
        });
    }
}
