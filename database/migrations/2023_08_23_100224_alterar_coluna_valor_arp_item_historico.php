<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterarColunaValorArpItemHistorico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_item_historico', function (Blueprint $table) {
            $table->decimal('valor', 15, 4)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_item_historico', function (Blueprint $table) {
            $table->decimal('valor', 15, 4)->change();
        });
    }
}
