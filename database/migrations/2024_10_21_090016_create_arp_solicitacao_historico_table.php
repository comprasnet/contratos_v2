<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArpSolicitacaoHistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arp_solicitacao_historico', function (Blueprint $table) {
            $table->id();

            ## FK para arp_solicitacao
            $table->foreignId('arp_solicitacao_id')->constrained('arp_solicitacao');

            ## Campos dateTime
//            $table->dateTime('data_envio_analise')->nullable();
//            $table->dateTime('data_aprovacao_analise')->nullable();

            ## Campo de texto
            //$table->text('motivo_justificativa_fornecedor')->nullable();

            ## FK para usuario (users)
            $table->foreignId('usuario_id')->constrained('users');

            $table->foreignId('unidade_id')->nullable()->constrained('unidades');

            ## FK para situacao (codigoitens)
            $table->foreignId('situacao_id')->constrained('codigoitens');

            ## FK para arp
           // $table->foreignId('arp_id')->constrained('arp');

            ## FK para item_arp_fornecedor (arp_item)
//            $table->foreignId('item_arp_fornecedor_id')->constrained('arp_item');

            ## Campos de timestamp padrão (created_at e updated_at)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arp_solicitacao_historico');
    }
}
