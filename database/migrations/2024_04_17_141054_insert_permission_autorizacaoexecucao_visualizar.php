<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class InsertPermissionAutorizacaoexecucaoVisualizar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');

        Permission::create(['name' => 'autorizacaoexecucao_visualizar']);

        $role = Role::where(['name' => 'Administrador'])->first();
        $role->givePermissionTo('autorizacaoexecucao_visualizar');

        $role = Role::where(['name' => 'Setor Contratos'])->first();
        $role->givePermissionTo('autorizacaoexecucao_visualizar');

        $role = Role::where(['name' => 'Responsável por Contrato'])->first();
        $role->givePermissionTo('autorizacaoexecucao_visualizar');

        $role = Role::where(['name' => 'Fornecedor'])->first();
        $role->givePermissionTo('autorizacaoexecucao_visualizar');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::where(['name' => 'Administrador'])->first();
        $role->revokePermissionTo('autorizacaoexecucao_visualizar');

        $role = Role::where(['name' => 'Setor Contratos'])->first();
        $role->revokePermissionTo('autorizacaoexecucao_visualizar');

        $role = Role::where(['name' => 'Responsável por Contrato'])->first();
        $role->revokePermissionTo('autorizacaoexecucao_visualizar');

        $role = Role::where(['name' => 'Fornecedor'])->first();
        $role->revokePermissionTo('autorizacaoexecucao_visualizar');

        Permission::where(['name' => 'autorizacaoexecucao_visualizar'])->forceDelete();
    }
}
