<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoverCampoObrigatorioAdesaoArp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->boolean('demonstracao_valores_registrados')->change()->nullable();
            $table->boolean('consulta_aceitacao_fornecedor')->change()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arp_solicitacao', function (Blueprint $table) {
            $table->boolean('demonstracao_valores_registrados')->change();
            $table->boolean('consulta_aceitacao_fornecedor')->change();
        });
    }
}
