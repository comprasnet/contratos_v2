<?php

use Illuminate\Database\Migrations\Migration;
use App\Services\Pncp\Arp\CancelarArpPncpService;
use App\Services\Pncp\PncpManagerService;
use App\Services\Pncp\Responses\PncpPutResponseService;
use App\Services\Pncp\Arp\ConsultarArpPncpService;
use App\Models\Arp;
use \Illuminate\Support\Facades\Log;

class CancelarAtaAtivaPncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $atas = Arp::whereIn('id', [38863, 75806, 30652, 109208])->get();

        $consultarAtaPncp = new ConsultarArpPncpService();
        $pncpManagerService = new PncpManagerService();

        foreach ($atas as $ata) {
            $retornoPncp = $consultarAtaPncp->sendToPncp($ata);
            $retornoPncpFormatado = (object) json_decode($retornoPncp->getBody()->getContents(), true);

            if (!isset($retornoPncpFormatado->cancelado)) {
                continue;
            }

            if ($retornoPncpFormatado->cancelado) {
                continue;
            }

            $historicoFiltrado = $ata->arpHistorico
                ->filter(function ($item) {
                    return $item->status->descricao === 'Cancelamento de item(ns)';
                })
                ->sortByDesc('created_at')
                ->first();

            $mensagem = "Id ata {$ata->id}, da unidade {$ata->unidades->codigo}".
                " com a data de cancelamento {$historicoFiltrado->data_assinatura_alteracao_vigencia}".
                " e id alteração {$historicoFiltrado->id}";
            Log::channel('migrations')->info($mensagem);

            try {
                $pncpManagerService->managePncp(
                    new CancelarArpPncpService($historicoFiltrado->data_assinatura_alteracao_vigencia),
                    new PncpPutResponseService(),
                    $ata
                );
                $mensagem = "Cancelada ata com o id {$ata->id}, da unidade {$ata->unidades->codigo}".
                    " com a data de cancelamento {$historicoFiltrado->data_assinatura_alteracao_vigencia}".
                    " e id alteração {$historicoFiltrado->id}";
                Log::channel('migrations')->info($mensagem);
            } catch (Exception $exception) {
                $mensagem = "Erro na ata com o id {$ata->id}, da unidade {$ata->unidades->codigo}".
                    " com a data de cancelamento {$historicoFiltrado->data_assinatura_alteracao_vigencia}".
                    " e id alteração {$historicoFiltrado->id}";

                Log::channel('migrations')->error($mensagem);
                Log::channel('migrations')->error($exception);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
