<?php

use Illuminate\Database\Migrations\Migration;
use App\Services\Pncp\Arp\CancelarArpPncpService;
use App\Services\Pncp\PncpManagerService;
use App\Services\Pncp\Responses\PncpPutResponseService;
use App\Services\Pncp\Arp\ConsultarArpPncpService;
use App\Models\Arp;
use \Illuminate\Support\Facades\Log;

class AtaCanceladaSistemaAtivaPncp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $atasCanceladas = Arp::select('arp.*')
            ->join('unidades', 'arp.unidade_origem_compra_id', '=', 'unidades.id')
            ->join('compras', 'arp.compra_id', '=', 'compras.id')
            ->join('codigoitens', 'arp.tipo_id', '=', 'codigoitens.id')
            ->where('codigoitens.descricao', 'Cancelada')
            ->get();

        $consultarAtaPncp = new ConsultarArpPncpService();
        $pncpManagerService = new PncpManagerService();

        foreach ($atasCanceladas as $ata) {
            $retornoPncp = $consultarAtaPncp->sendToPncp($ata);
            $retornoPncpFormatado = (object) json_decode($retornoPncp->getBody()->getContents(), true);

            Log::channel('migrations')->debug(json_encode($retornoPncpFormatado));

            usleep(500000);

            if (!isset($retornoPncpFormatado->cancelado)) {
                continue;
            }

            if ($retornoPncpFormatado->cancelado) {
                continue;
            }

            $historicoFiltrado = $ata->arpHistorico
                ->filter(function ($item) {
                    return $item->status->descricao === 'Cancelamento de item(ns)';
                })
                ->sortByDesc('created_at')
                ->first();

            $mensagem = "Id ata {$ata->id}, da unidade {$ata->unidades->codigo}".
                " com a data de cancelamento {$historicoFiltrado->data_assinatura_alteracao_vigencia}".
                " e id alteração {$historicoFiltrado->id}";
            Log::channel('migrations')->info($mensagem);

            try {
                Log::channel('migrations')->debug($historicoFiltrado->data_assinatura_alteracao_vigencia);
                Log::channel('migrations')->debug($ata);
                $pncpManagerService->managePncp(
                    new CancelarArpPncpService($historicoFiltrado->data_assinatura_alteracao_vigencia),
                    new PncpPutResponseService(),
                    $ata
                );
                $mensagem = "Cancelada ata com o id {$ata->id}, da unidade {$ata->unidades->codigo}".
                    " com a data de cancelamento {$historicoFiltrado->data_assinatura_alteracao_vigencia}".
                    " e id alteração {$historicoFiltrado->id}";
                Log::channel('migrations')->info($mensagem);
            } catch (Exception $exception) {
                $mensagem = "Erro na ata com o id {$ata->id}, da unidade {$ata->unidades->codigo}".
                    " com a data de cancelamento {$historicoFiltrado->data_assinatura_alteracao_vigencia}".
                    " e id alteração {$historicoFiltrado->id}";

                Log::channel('migrations')->error($mensagem);
                Log::channel('migrations')->error($exception);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
