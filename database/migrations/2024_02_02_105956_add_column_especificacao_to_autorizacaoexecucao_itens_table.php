<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnEspecificacaoToAutorizacaoexecucaoItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizacaoexecucao_itens', function (Blueprint $table) {
            Schema::table('autorizacaoexecucao_itens', function (Blueprint $table) {
                $table->string('especificacao', 100)->nullable();
            });

            Schema::table('autorizacaoexecucao_itens_historico', function (Blueprint $table) {
                $table->string('especificacao', 100)->nullable();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizacaoexecucao_itens', function (Blueprint $table) {
            Schema::table('autorizacaoexecucao_itens', function (Blueprint $table) {
                $table->dropColumn('especificacao');
            });

            Schema::table('autorizacaoexecucao_itens_historico', function (Blueprint $table) {
                $table->dropColumn('especificacao');
            });
        });
    }
}
