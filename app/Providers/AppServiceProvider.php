<?php

namespace App\Providers;

use App\View\Components\PncpReenviarIcon;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (config('app.protocolo_http') === 'https') {
            \URL::forceScheme('https');
        }

        if (Config::get("app.app_amb") != 'Ambiente Produção' &&
            Config::get("app.app_amb") != 'Ambiente Homologação' &&
            Config::get("app.app_amb") != 'Ambiente Treinamento') {
            $appPath = Config::get("app.app_path");

            $stringfromfile = file("{$appPath}.git/HEAD", FILE_USE_INCLUDE_PATH);

            $firstLine = $stringfromfile[0]; //get the string from the array

            $explodedstring = explode("/", $firstLine, 3); //seperate out by the "/" in the string

            $branchname = $explodedstring[2]; //get the one that is always the branch name
            Config::set('app.app_version', $branchname);
        }

        Blade::component('pncp-reenviar-icon', PncpReenviarIcon::class);
        Blade::component('crud::autorizacaoexecucao.vinculo.adicionar-vinculos-component', 'adicionar-vinculos');
        Blade::component(
            'crud::autorizacaoexecucao.vinculo.adicionar-vinculo-itens-component',
            'adicionar-vinculo-itens'
        );
    }
}
