<?php

namespace App\Providers;

use App\Models\Adesao;
use App\Models\Arp;
use App\Models\ArpArquivo;
use App\Models\ArpHistorico;
use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoExecucaoEntrega;
use App\Models\EnviaDadosPncp;
use App\Observers\ArpArquivoObserver;
use App\Observers\ArpHistoricoObserver;
use App\Observers\ArpObserver;
use App\Observers\AutorizacaoExecucaoEntregaObserver;
use App\Observers\AutorizacaoExecucaoObserver;
use App\Observers\EnviaDadosPncpObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Observers\AdesaoObserver;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Arp::observe(ArpObserver::class);
        EnviaDadosPncp::observe(EnviaDadosPncpObserver::class);
        ArpArquivo::observe(ArpArquivoObserver::class);
        AutorizacaoExecucaoEntrega::observe(AutorizacaoExecucaoEntregaObserver::class);
        AutorizacaoExecucao::observe(AutorizacaoExecucaoObserver::class);
    }
}
