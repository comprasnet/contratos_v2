<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificarPrepostoAdesao extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $dadosRetorno;
    public function __construct($dadosRetorno)
    {
        $this->dadosRetorno = $dadosRetorno;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $link = config('app.url') . '/fornecedor/arp/adesao/analisar/' . $this->dadosRetorno->adesao->id . '/edit';

        return $this->markdown('emails.notificar_preposto_adesao', [
            'dados' => $this->dadosRetorno,
            'link' => $link,
        ]);
    }
}
