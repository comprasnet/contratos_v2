<?php

namespace App\Mail;

use App\Models\TermoRecebimentoProvisorio;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificarAssinaturaTRPMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var TermoRecebimentoProvisorio
     */
    private $trp;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TermoRecebimentoProvisorio $trp)
    {

        $this->trp = $trp->load('contrato');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Termo de Recebimento Provisório ' . $this->trp->numero .
                '  do contrato ' . $this->trp->contrato->numero . ' disponível para assinatura')
            ->markdown(
                'emails.assinar-trp',
                [
                'trp' => $this->trp,
                'link' => config('app.url') . '/trp/' . $this->trp->contrato_id . '/' . $this->trp->id .
                    '/assinatura/create',
                ]
            );
    }
}
