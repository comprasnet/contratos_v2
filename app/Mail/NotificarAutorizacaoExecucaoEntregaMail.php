<?php

namespace App\Mail;

use App\Models\AutorizacaoExecucaoEntrega;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificarAutorizacaoExecucaoEntregaMail extends Mailable
{
    use Queueable, SerializesModels;

    private $fields;
    private $autorizacaoExecucao;
    private $contrato;
    private $entrega;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AutorizacaoExecucaoEntrega $entrega)
    {
        $autorizacaoExecucaoService = new AutorizacaoExecucaoService();
        $autorizacaoExecucaoService->setContrato($entrega->getContrato());
        $autorizacaoExecucaoService->setAutorizacaoexecucao($entrega->autorizacao);

        $this->fields = $autorizacaoExecucaoService->getFieldLabels();
        $this->autorizacaoExecucao = $autorizacaoExecucaoService->getAutorizacaoexecucao();
        $this->contrato = $autorizacaoExecucaoService->getContrato();
        $this->entrega = $entrega;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $button = null;
        $title = 'Notificação da Entrega da Ordem de Serviço / Fornecimento';
        if ($this->entrega->situacao->descres == 'ae_entrega_status_1') {
            $button = [
                'link' => config('app.url') . '/autorizacaoexecucao/' . $this->contrato->id . '/' .
                    $this->autorizacaoExecucao->id . '/entrega/' . $this->entrega->id .
                    '/analisar/create',
                'texto' => 'Analisar Entrega  da Ordem de Serviço / Fornecimento',
            ];
        } elseif ($this->entrega->situacao->descres == 'ae_entrega_status_4') {
            $title = 'Notificação de Assinatura do Termo de Recebimento Provisório';
            $button = [
                'link' => route(
                    'entrega/trp/{entrega_id}/assinatura.create',
                    [
                        'contrato_id' => $this->contrato->id,
                        'autorizacaoexecucao_id' => $this->autorizacaoExecucao->id,
                        'entrega_id' => $this->entrega->id
                    ]
                ),
                'texto' => 'Assinar TRP',
            ];
        } elseif ($this->entrega->situacao->descres == 'ae_entrega_status_8') {
            $title = 'Notificação de Assinatura do Termo de Recebimento Definitivo';
            $button = [
                'link' => route(
                    'entrega/trp/{entrega_id}/assinatura.create',
                    [
                        'contrato_id' => $this->contrato->id,
                        'autorizacaoexecucao_id' => $this->autorizacaoExecucao->id,
                        'entrega_id' => $this->entrega->id
                    ]
                ),
                'texto' => 'Assinar TRD',
            ];
        }

        return $this->subject('[' . $this->entrega->situacao->descricao . '] Entrega número ' .
            $this->entrega->getFormattedSequencial() . ' da Ordem de Serviço / Fornecimento número ' .
            $this->fields['numero']['value'] . '  do contrato ' . $this->fields['contrato']['value'])
            ->markdown(
                'emails.analisar-autorizacao-execucao-entrega',
                [
                    'title' => $title,
                    'fields' => $this->fields,
                    'numeroEntrega' => $this->entrega->getFormattedSequencial(),
                    'situacaoEntrega' => $this->entrega->situacao->descricao,
                    'dataPrazoTrp' => $this->entrega->getDataPrazoTrp(),
                    'dataPrazoTrd' => $this->entrega->getDataPrazoTrd(),
                    'button' => $button,
                ]
            );
    }
}
