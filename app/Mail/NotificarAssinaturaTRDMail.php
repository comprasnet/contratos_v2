<?php

namespace App\Mail;

use App\Models\TermoRecebimentoDefinitivo;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificarAssinaturaTRDMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var TermoRecebimentoDefinitivo
     */
    private $trd;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TermoRecebimentoDefinitivo $trd)
    {

        $this->trd = $trd->load('contrato');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Termo de Recebimento Definitivo ' . $this->trd->numero .
                '  do contrato ' . $this->trd->contrato->numero . ' disponível para assinatura')
            ->markdown(
                'emails.assinar-trd',
                [
                'trd' => $this->trd,
                'link' => config('app.url') . '/trd/' . $this->trd->contrato_id . '/' . $this->trd->id .
                    '/assinatura/create',
                ]
            );
    }
}
