<?php

namespace App\Mail;

use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AssinarAutorizacaoExecucaoMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var mixed
     */
    private $fields;
    private $autorizacaoExecucao;
    private $contrato;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AutorizacaoExecucaoService $autorizacaoExecucaoService)
    {
        $this->fields = $autorizacaoExecucaoService->getFieldLabels();
        $this->autorizacaoExecucao = $autorizacaoExecucaoService->getAutorizacaoexecucao();
        $this->contrato = $autorizacaoExecucaoService->getContrato();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Ordem de Serviço / Fornecimento número ' . $this->fields['numero']['value'] .
            '  do contrato ' . $this->fields['contrato']['value'] . ' disponível para assinatura')
            ->markdown(
                'emails.assinar-autorizacao-execucao',
                [
                    'fields' => $this->fields,
                    'link' => config('app.url') .
                        '/autorizacaoexecucao/' . $this->contrato->id . '/' . $this->autorizacaoExecucao->id
                        . '/assinatura/create' ,
                ]
            );
    }
}
