<?php

namespace App\Mail;

use App\Models\AutorizacaoexecucaoHistorico;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AssinarAlteracaoAutorizacaoExecucaoMail extends Mailable
{
    use Queueable, SerializesModels;

    private $fields;
    private $autorizacaoExecucao;
    private $contrato;
    private $autorizacaoExecucaoHistoricoAlterar;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AutorizacaoexecucaoHistorico $autorizacaoExecucaoHistorico)
    {
        $autorizacaoExecucaoService = new AutorizacaoExecucaoService();
        $autorizacaoExecucaoService->setContrato($autorizacaoExecucaoHistorico->getContrato());
        $autorizacaoExecucaoService->setAutorizacaoexecucao($autorizacaoExecucaoHistorico->autorizacaoexecucao);

        $this->fields = $autorizacaoExecucaoService->getFieldLabels();
        $this->autorizacaoExecucao = $autorizacaoExecucaoService->getAutorizacaoexecucao();
        $this->contrato = $autorizacaoExecucaoService->getContrato();
        $this->autorizacaoExecucaoHistoricoAlterar = $autorizacaoExecucaoHistorico;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Alteração da Ordem de Serviço / Fornecimento número ' .
            $this->fields['numero']['value'] . '  do contrato ' . $this->fields['contrato']['value'] .
            ' disponível para assinatura')
            ->markdown(
                'emails.assinar-alteracao-autorizacao-execucao',
                [
                    'fields' => $this->fields,
                    'numeroAlteracao' => $this->autorizacaoExecucaoHistoricoAlterar->getSequencial(),
                    'tipoAlteracao' => $this->autorizacaoExecucaoHistoricoAlterar->getTipoAlteracao(),
                    'link' => config('app.url') . '/autorizacaoexecucao/' . $this->contrato->id . '/' .
                        $this->autorizacaoExecucao->id . '/alterar/' . $this->autorizacaoExecucaoHistoricoAlterar->id .
                        '/assinatura/create' ,
                ]
            );
    }
}
