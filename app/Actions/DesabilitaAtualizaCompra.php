<?php

namespace App\Actions;

use App\Http\Traits\AmparoTrait;
use App\Models\CodigoItem;
use App\Models\CompraItem;
use App\Models\CompraItemFornecedor;
use App\Models\CompraItemUnidade;
use App\Models\Compras;
use App\Models\Unidade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\CompraTrait;

class DesabilitaAtualizaCompra
{
    use CompraTrait;
    use AmparoTrait;

    public function execute(int $compraId)
    {
        $dadosCompra = Compras::find($compraId);
        if (isset($dadosCompra)) {
            # Consulta a compra, serviço 1
            $retornoSiasg = $this->consultaCompraSiasg($dadosCompra);

            # Se não for encontrado nenhum valor no SIASG, possivelmente problema com a VPN
            if (empty($retornoSiasg)) {
                return null;
            }

            # Se o erro for retornado pelo SIASG
            if (isset($retornoSiasg->code)) {
                return null;
            }

            # Se não existir a compra, verifica na base de dados a informação da compra
            if (is_null($retornoSiasg->data)) {
                $compra = $this->verificaCompraExiste($dadosCompra, [$dadosCompra->unidade_origem_id]);

                # Se não encontrar compra, retorna que a compra não existe
                if (count($compra) == 0) {
                    return null;
                }

                $retornoSiasg = $compra;
            }

            $amparosLegaisDerivadas14133 = $this->getDerivadas14133Formatado();

            # Se a compra informada for diferente da LEI 14.133
            if (isset($retornoSiasg->data)
                && !in_array($retornoSiasg->data->compraSispp->lei, $amparosLegaisDerivadas14133)) {
                return null;
            }

            # Se a compra for da LEI 14.133 mas não for do tipo SISRP
            if (isset($retornoSiasg->data) && empty($retornoSiasg->data->linkSisrpCompleto)) {
                return null;
            }

            DB::beginTransaction();
            try {
                $request = new Request($dadosCompra->toArray());
                $request->merge(['unidade_origem_compra_id' => $dadosCompra->unidade_origem_id]);

                # Grava/Atualiza os dados da compra
                $compraOrigem = $this->updateOrCreateCompra($request);

                # Variável responsável para inativar os itens
                $situacaoItem = false;
                # Recupera os itens para poder inativar
                $itensCompra = CompraItem::where('compra_id', (int)$compraOrigem->id)->get();
                foreach ($itensCompra as $item) {
                    # Inativa o item na tabela compra_items
                    $item->situacao = $situacaoItem;

                    # Inativa o item na tabela compra_item_unidade
                    CompraItemUnidade::where("compra_item_id", $item->id)->update(['situacao' => $situacaoItem]);

                    # Inativa o item na tabela compra_item_fornecedor
                    CompraItemFornecedor::where("compra_item_id", $item->id)->update(['situacao' => $situacaoItem]);
                    $item->save();
                }

                #Inclui os itens gravados/atualizados para os participantes
                $this->gravaParametroItensdaCompraSISRP($retornoSiasg, $compraOrigem);

                DB::commit();
            } catch (Exception $exc) {
                DB::rollback();
                return null;
            }
        }
    }

    public function consultaCompraSiasg(Compras $request, bool $compraSispp = true)
    {
        $modalidade = Codigoitem::find($request->modalidade_id);
        $uasgCompra = Unidade::find($request->unidade_origem_id);

        # Se for compra do tipo sispp, utiliza o serviço um para recuperar as informações
        if ($compraSispp) {
            // uasg beneficiária
            $codigoUasgBeneficiaria = $this->recuperarCodigoUasgBeneficiaria($request->uasg_beneficiaria_id);

            return $this->retornarExecucaoCompraSispp(
                $modalidade,
                $uasgCompra,
                $request->numero_ano,
                $codigoUasgBeneficiaria
            );
        }

        # Retorna as informações do serviço quatro para os participantes
        return $this->retornarExecucaoCompraSisrp(
            $modalidade,
            $uasgCompra,
            $request->numero_ano
        );
    }
}
