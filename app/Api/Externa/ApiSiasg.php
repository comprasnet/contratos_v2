<?php

namespace App\Api\Externa;

use Exception;

class ApiSiasg extends Estrutura{

    protected $url_servico;
    protected $token;
    protected $opt;

    public function __construct()
    {
        $this->url_servico = config('api.siasg.url');
        $this->token = config('api.siasg.token');
    }

    public function executaConsulta(string $tipo, array $dado, string $method = null)
    {
        $nome_funcao = 'consulta' . $tipo;

        if ($method == 'POST') {
            $nome_funcao = 'envia' . $tipo;
            return $this->$nome_funcao($dado, $method);
        }

        $this->opt = $this->montarHeader("GET",["X-Authentication: {$this->token}"]);

        return $this->$nome_funcao($dado);
    }

    private function submit(string $servico, array $params, string $method = null)
    {
        $retorno = '';

        $servico_especifico = $this->retornaServicoEspecifico($servico);
        $url = $this->url_servico . $servico_especifico;

        if ($method == 'POST') {
            $retorno = $this->executaEnvioPost($url,$params);
            return $retorno;
        }

        return $this->enviarDados($url, $this->opt,$params);
    }

    private function executaEnvioPost(string $url, array $params)
    {

        $opts = $this->montarHeader("POST",$params,[ "X-Authentication: " . $this->token, 'Content-Type: application/json']);
        $data = $this->enviarDados($url, $opts);

        return json_decode($data, true);

    }

    public function retornaServicoEspecifico(string $servico)
    {
        $complemento_url = '';
        switch ($servico) {
            case 'CONTRATOCOMPRA':
                $complemento_url = 'contrato/v1/contratos?';
                break;
            case 'CONTRATOSISG':
                $complemento_url = 'contrato/v1/contrato?';
                break;
            case 'CONTRATONAOSISG':
                $complemento_url = 'contrato/v1/contratonsisg?';
                break;
            case 'DADOSCONTRATO':
                $complemento_url = 'contrato/v1/dadoscontrato?';
                break;
            case 'COMPRASISPP':
                $complemento_url = 'compra/v1/sispp?';
                break;
            case 'ATUALIZASIASGEMPENHO':
                $complemento_url = 'compra/v1/atualizaSiasgEmpenho';
                break;
            case 'ITEMCOMPRASISRP':
                $complemento_url = 'compra/v1/itemCompraSisrp?';
                break;
            case 'RETORNAITEMSISRP':
                $complemento_url = 'compra/v1/sisrp?';
                break;
        }
        return $complemento_url;
    }

    private function consultaCompra(array $dado_consulta)
    {
        return $this->submit('CONTRATOCOMPRA', $dado_consulta);
    }

    private function enviaEmpenho(array $dado, string $method)
    {
        return $this->submit('ATUALIZASIASGEMPENHO', $dado, $method);
    }


    private function consultaContratoCompra(array $dado_consulta)
    {
        return $this->submit('CONTRATOCOMPRA', $dado_consulta);
    }

    private function consultaContratoSisg(array $dado_consulta)
    {
        return $this->submit('CONTRATOSISG', $dado_consulta);
    }

    private function consultaContratoNaoSisg(array $dado_consulta)
    {
        return $this->submit('CONTRATONAOSISG', $dado_consulta);
    }

    private function consultaDadosContrato(array $dado_consulta)
    {
        return $this->submit('DADOSCONTRATO', $dado_consulta);
    }

    private function consultaCompraSispp(array $dado_consulta)
    {
        return $this->submit('COMPRASISPP', $dado_consulta);
    }

    private function consultaItemCompraSisrp(array $dado_consulta)
    {
        return $this->submit('ITEMCOMPRASISRP', $dado_consulta);
    }

    public function consultaCompraByUrl($url)
    {

        if(empty($this->opt)){
            $this->opt = stream_context_create($this->montarHeader("GET",["X-Authentication: {$this->token}"]));
        }

        try {
            $retorno = file_get_contents($url, false, $this->opt);
        } catch (Exception $ex) {
            $retorno = '{"data": null,"messagem":'.$ex->getMessage().',"codigoRetorno":500}';
        }

        return json_decode($retorno, true);
    }


}