<?php
namespace App\Api\Externa;

class Estrutura {

    public function montarHeader(string $method, array $header, ?array $parametros = null) {

        $http = [ "method" => $method, "header" => $header ];

        if(!empty($parametros)) {
            $http = [ "method" => $method, "header" => $header, 'content' => json_encode($parametros) ];
        }

        return  [ "http" => $http , 'ssl' => [ 'verify_peer' => false ] ];
    }

    public function enviarDados(string $url, ?array $opts = null, array $parametrosGet = null ) {

        $context = stream_context_create($opts);
     
        try {
            if(!empty($parametrosGet)) {
                $parametros = $this->trataParametros($parametrosGet);
                $urlCompleta = $url . $parametros;
                $data = file_get_contents($urlCompleta, false, $context);
            } else {
                $data = file_get_contents($url, false, $context);
            }
            
        } catch (\Exception $e) {
            if (str_contains($e->getMessage(), '403 Forbidden')) { 
                $quebramensagem = explode(':',$e->getMessage());
                $mensagem ="";
                for($i =2; $i < count($quebramensagem) ; $i++) {
                    $mensagem .= $quebramensagem[$i] . " ";
                }
                $jsonString = '{"code":203,"message":"'.trim($mensagem).'", "type":"error"}';
                return $jsonString;
            }
            $data = false;
        }

        return $data;
    }

    private function trataParametros(array $params)
    {
        return http_build_query($params, '', '&');
    }

}