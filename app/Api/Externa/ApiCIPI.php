<?php

namespace App\Api\Externa;

class ApiCIPI extends Estrutura{

    protected $url_servico;
    protected $codigoOrgao;
    protected $sistema;
    protected $context;

    public function __construct()
    {
        $this->url_servico = config('api.cipi.url');
        $this->codigoOrgao = config('api.cipi.codigo_orgao');
        $this->sistema = config('api.cipi.sistema');
    }

    public function executaConsulta(string $tipo, array $dado)
    {
        $nome_funcao = "consulta{$tipo}";

        return $this->$nome_funcao($dado);
    }

    private function submit(string $servico, array $params)
    {
        $retorno = '';

        $servico_especifico_login = $this->retornaServicoEspecifico('GERARTOKEN');
        $url_login = $this->url_servico . $servico_especifico_login;
        $token = $this->realizaLogin(config('api-cipi.sistema'),config('api-cipi.senha'),$url_login);

        if($token){
            $servico_especifico = $this->retornaServicoEspecifico($servico);
            $url = $this->url_servico . $servico_especifico;
            $arrToken = json_decode($token,true);
            $retorno = $this->executaEnvioPost($url, $params, $arrToken['token']);
            return $retorno;
        }else {
            return false;
        }

    }

    private function realizaLogin($usuario,$senha,$url)
    {

        $array_credenciais = [ "senha" => $senha, 'usuario' => $usuario ];
        $opts = $this->montarHeader("POST",[ 'Content-Type: application/json' ],$array_credenciais);
        return $this->enviarDados($url, $opts);
    }

    private function executaEnvioPost(string $url, array $params, $token)
    {

        $array_credenciais = [
            "credenciaisAcesso" => [
                "codigoOrgao" => $this->codigoOrgao,
                "cpf" => backpack_user()->cpf,
                "sistema" => $this->sistema
            ]
        ];

        $parametros = array_merge($array_credenciais, $params);
        $opts = $this->montarHeader("POST",[ 'Content-Type: application/json', "Authorization: Bearer {$token}" ],$parametros);
        return $this->enviarDados($url, $opts);
    }

    private function retornaServicoEspecifico(string $servico)
    {
        $complemento_url = '';

        switch ($servico) {
            case 'VALIDACIPI':
                $complemento_url = 'protected/projetoinvestimento/validar';
                break;
            case 'GERARTOKEN':
                $complemento_url = 'public/sistema/gerar-token';
                break;
        }
        return $complemento_url;
    }

    private function consultaValidaCipi(array $dado, string $method = null)
    {
        return $this->submit('VALIDACIPI', $dado, $method);
    }
}