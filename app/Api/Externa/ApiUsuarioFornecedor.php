<?php

namespace App\Api\Externa;

use App\Http\Traits\ExternalServices;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use function PHPUnit\Framework\stringContains;

class ApiUsuarioFornecedor
{
    use ExternalServices;

    protected $url;

    public function __construct()
    {
        $this->url = config('api.usuariofornecedor.host');
    }

    public function sessaoUsuarioFornecedor(string $comprasId)
    {
        $urlSessaoUsuario = config('api.usuariofornecedor.sessao_usuario');
        $ipAplicacao = request()->getClientIp();
        $urlFormatada = str_replace('{compras_id}', $comprasId, $urlSessaoUsuario);
        $urlFormatada = str_replace('{ip}', $ipAplicacao, $urlFormatada);

        try {
            $resposeusuarioCompras = Cache::get('responseUsuarioCompras'. ' ' . $comprasId);
            if (!empty($resposeusuarioCompras)) {
                return $resposeusuarioCompras;
            }
            $request = $this->makeRequest(
                'GET',
                $this->url,
                $urlFormatada,
                null,
                [],
                null,
                []
            );

            $jsonRetorno = json_decode($request->getBody(), true);

            if (empty($resposeusuarioCompras)) {
                Cache::put('responseUsuarioCompras' . ' ' . $comprasId, $jsonRetorno, now()->addHour());
                session()->put('chave_responseUsuarioCompras', 'responseUsuarioCompras' . ' ' . $comprasId);
            }
            return $jsonRetorno;
        } catch (\Exception $e) {
            Log::error('erro no service ao buscar o user logado');
            Log::error($e);
            if ($e->getCode() == '500') {
                $error = json_decode($e->getResponse()->getBody()->getContents());
                if (stringContains($error->message, 'Invalid UUID string')) {
                    abort('403');
                }
            }
        }
    }
}
