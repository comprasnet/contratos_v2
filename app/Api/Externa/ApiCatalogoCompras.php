<?php

namespace App\Api\Externa;

use App\Http\Traits\ExternalServices;
use App\Http\Traits\LogTrait;
use App\Repositories\UnidadeMedidaRepository;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;

class ApiCatalogoCompras
{
    use ExternalServices;
    use LogTrait;

    const API_SERVICE_DEFAULT = 'https://cnbs.estaleiro.serpro.gov.br/';
    private $apiServico;

    public function __construct()
    {
        $this->apiServico = env('API_CATALOGO_COMPRAS', self::API_SERVICE_DEFAULT);
    }

    public function getServiceByCode($code)
    {
        $url = $this->apiServico . 'cnbs-api/servico/v1/unidadeMedidaPorCodigo';
        $client = new Client();
        $response = $client->get($url, ['query' => ['codigo_servico' => $code]]);
        if ($response->getStatusCode() != 200) {
            throw new Exception('Erro ao acessar Catalogo de compras e recuperar unidade de medida');
        }
        return json_decode($response->getBody());
    }


    public function getMaterialByCode($code)
    {
        $url = $this->apiServico . 'cnbs-api/material/v1/unidadeFornecimentoPorCodigoPdm';
        $client = new Client();
        $response = $client->get($url, ['query' => ['codigo_pdm' => $code]]);
        if ($response->getStatusCode() != 200) {
            throw new Exception('Erro ao acessar Catalogo de compras e recuperar unidade de medida');
        }
        return json_decode($response->getBody());
    }

    public function getPdmMaterialByCode($code)
    {
        $url = $this->apiServico . 'cnbs-api/material/v1/recuperaDadosItemMaterialPorCodigo';
        $client = new Client();
        $response = $client->get($url, ['query' => ['codigo_item_material' => $code]]);
        if ($response->getStatusCode() != 200) {
            throw new Exception('Erro ao acessar Catalogo de compras e recuperar unidade de medida');
        }
        $json = json_decode($response->getBody());
        if (!is_null($json)) {
            return $json->codigoPdm;
        }
        return null;
    }

    /**
     *
     * Método responsável em retornar as informações do PDM a partir do código do SIASG
     *
     * @param string $codigoSiasg
     * @return mixed|null
     * @throws GuzzleException
     */
    public function getPdmByCodigoSiasg(string $codigoSiasg)
    {
        $dataAPI = null;

        $queryString = http_build_query(['codigo_item_material' => $codigoSiasg]);
        $endpoint = "cnbs-api/material/v1/recuperaDadosItemMaterialPorCodigo?{$queryString}";

        try {
            $response = $this->makeRequest(
                'GET',
                $this->apiServico,
                $endpoint,
                null,
                [],
                null,
                []
            );
            $dataAPI = json_decode($response->getBody()->getContents(), true);
        } catch (ClientException $clientException) {
            $this->inserirLogCustomizado('cnbs', 'error', $clientException);
        }

        return $dataAPI;
    }

    /**
     * @throws Exception
     */
    public static function getByTypeAndCode($type, $code)
    {
        if ($type == UnidadeMedidaRepository::TYPE_MATERIAL) {
            return (new ApiCatalogoCompras)->getMaterialByCode($code);
        }
        return (new ApiCatalogoCompras)->getServiceByCode($code);
    }
}
