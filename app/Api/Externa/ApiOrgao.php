<?php

namespace App\Api\Externa;

use App\Http\Traits\ExternalServices;
use App\Http\Traits\LogTrait;
use GuzzleHttp\Exception\GuzzleException;

class ApiOrgao
{
    use ExternalServices, LogTrait;
    
    public function __construct()
    {
        $this->url = config('api.orgao.url');
    }
    
    /**
     * @param string $codigoOrgao
     * @return mixed|void
     * @throws GuzzleException
     */
    protected function getOrgao(string $codigoOrgao)
    {
        try {
            $endpoint = config('api.orgao.servico.orgaoPorCodigo');
            $endpoint = str_replace("{numero}", $codigoOrgao, $endpoint);
            
            $request = $this->makeRequest(
                'GET',
                $this->url,
                $endpoint,
                null,
                [],
                null,
                []
            );
            
            $data = (object) json_decode($request->getBody()->getContents(), true);
            
            return $data;
        } catch (\Exception $ex) {
            $this->inserirLogCustomizado('orgao', 'error', $ex);
        }
    }
}
