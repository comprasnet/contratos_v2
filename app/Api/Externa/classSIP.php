<?php

namespace App\Api\Externa;

use Exception;

class classSIP {
    private $urlWsdl;
    private $soapClient;
    public  $url;

    public function __construct($url = 'http://homo-sei.inss.gov.br/sip/controlador_ws.php?servico=wsdl'){
        //definição no homecontroller
        $this->urlWsdl = $this->url;
        try {
            $this->soapClient = new \SoapClient($this->urlWsdl, ['exceptions' => true]);
            $this->SiglaSistema = config('app.siglaSistema');
            $this->IdentificacaoServico = config('app.identificacaoServico');
            
        } catch ( Exception $e ) { 

            if ($e->getMessage() == 'Service Unavailable') {
                echo 'Não foi possível conectar ao WEBService do SIP... Tente novamente em alguns minutos!';
                return;
            }
        }
    }

    public function listaPermissao($CPF){
        $IdSistema = '100000100';
        $IdOrgaoUsuario = '0';
        $response = $this->soapClient->listarPermissao($IdSistema, $IdOrgaoUsuario, NULL, $CPF, NULL, NULL,NULL, NULL);
        return $response;
    }

    public function listaPermissaoPrincipal($CPF){
        $IdSistema = '100000100';
        $IdOrgaoUsuario = '0';
        $response = $this->soapClient->listarPermissao($IdSistema, $IdOrgaoUsuario, NULL, $CPF, NULL, NULL,NULL, NULL);
        foreach($response as $resp){
            if ($resp->IdPerfil == '100000938' && $resp->IdUnidade != '110000000' && strlen($resp->IdOrigemUnidade)<=8){
                if(is_null($resp->DataFinal)){
                    $responsa["IdSistema"] = $resp->IdSistema;
                    $responsa["IdOrgaoUsuario"] = $resp->IdOrgaoUsuario;
                    $responsa["IdUsuario"] = $resp->IdUsuario;
                    $responsa["IdOrigemUsuario"] = $resp->IdOrigemUsuario;
                    $responsa["IdOrgaoUnidade"] = $resp->IdOrgaoUnidade;
                    $responsa["IdUnidade"] = $resp->IdUnidade;
                    $responsa["IdOrigemUnidade"] = $resp->IdOrigemUnidade;
                    $responsa["IdPerfil"] = $resp->IdPerfil;
                    $responsa["DataInicial"] = $resp->DataInicial;
                    $responsa["DataFinal"] = $resp->DataFinal;
                    $responsa["SinSubunidades"] = $resp->SinSubunidades;
                    $responsa["ativo"] = "S";
                } else {
                    $responsa["ativo"] = "N";
                }
            }
        }
        return (object)$responsa;
    }

    public function listaUnidades($IdSistema, $IdUnidade){
        $response = $this->soapClient->carregarUnidades($IdSistema, NULL, $IdUnidade);
        return $response;
    }

}