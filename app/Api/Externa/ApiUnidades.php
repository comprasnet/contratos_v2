<?php

namespace App\Api\Externa;

use App\Http\Traits\ExternalServices;
use App\Http\Traits\LogTrait;
use GuzzleHttp\Exception\GuzzleException;

class ApiUnidades
{
    use ExternalServices, LogTrait;
    protected $url;
    public function __construct()
    {
        $this->url = config('api.unidades.url');
    }
    
    /**
     * @param string $codigo
     * @return mixed|void
     * @throws GuzzleException
     */
    protected function getUnidadePorCodigoService(string $codigo)
    {
        try {
            $endpoint = config('api.unidades.servico.uasg.uasgPorCodigo');
            $endpoint = str_replace("{numero}", $codigo, $endpoint);
            
            $request = $this->makeRequest(
                'GET',
                $this->url,
                $endpoint,
                null,
                [],
                null,
                []
            );
            
            $data = (object) json_decode($request->getBody()->getContents(), true);
            
            return $data;
        } catch (\Exception $e) {
            $this->inserirLogCustomizado('unidades', 'error', $e);
        }
    }
}
