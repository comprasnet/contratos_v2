<?php

namespace App\Library\Auth;

use Backpack\CRUD\app\Library\Auth\RedirectsUsers;
use Backpack\CRUD\app\Library\Auth\ThrottlesLogins;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

trait AuthenticatesUsers
{
    use RedirectsUsers, ThrottlesLogins;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $this->data['title'] = trans('backpack::base.login'); // set the page title
        $this->data['username'] = $this->username();

        $blade = 'auth.login-nativo';

        if (config('app.app_amb') == 'Ambiente Treinamento') {
            $blade = 'auth.login-treinamento';
        }

        $acessoSomenteLoginGov = $this->acessarSomenteGov();

        if ($acessoSomenteLoginGov) {
            $blade = 'auth.login-govbr';
        }

        return view(backpack_view($blade), $this->data);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */


    public function login(Request $request)
    {
        // Validar os campos de login
        $this->validateLogin($request);

        // Verificar se o Recaptcha está habilitado
        $habilitarRecaptcha = env('HABILITAR_RECAPTCHA');

        // Verificar o Recaptcha apenas se estiver habilitado
        if ($habilitarRecaptcha) {
            $recaptchaResult = $this->verificarRecaptcha($request);

            // Verificar se o Recaptcha foi bem-sucedido
            if (!$recaptchaResult['success']) {
                // Recaptcha falhou
                return $this->sendFailedLoginResponse(
                    $request,
                    'Falha na verificação do Recaptcha.'
                );
            }

            // Verificar se a pontuação é suficiente
            if (!$this->verificarScoreRecaptcha($recaptchaResult['score'])) {
                // Pontuação do Recaptcha insuficiente
                Log::info(
                    $request->input('cpf') . " - Pontuação do Recaptcha insuficiente: " . $recaptchaResult['score']
                );
                return $this->sendFailedLoginResponse(
                    $request,
                    'Pontuação do Recaptcha insuficiente.'
                );
            }
        }

        // Verificar se há muitas tentativas de login
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        // Tentar realizar o login
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // Incrementar as tentativas de login e retornar a resposta de falha
        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }


    private function verificarRecaptcha(Request $request)
    {
        $recaptchaSecret = env('RECAPTCHA_SECRET_KEY');
        $recaptchaResponse = $request->input('g-recaptcha-response');

        $client = new Client();
        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            [
                'form_params' => [
                    'secret' => $recaptchaSecret,
                    'response' => $recaptchaResponse,
                ],
            ]
        );

        $responseData = json_decode($response->getBody()->getContents());

        $score = $responseData->score ?? null;

        return [
            'success' => $responseData->success,
            'score' => $score,
        ];
    }
    private function verificarScoreRecaptcha($score)
    {
        return $score > env('SCORE_LIMIT_RECAPTCHA', 0.5);
    }
    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request),
            $request->filled('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if ($response = $this->authenticated($request, $this->guard()->user())) {
            return $response;
        }

        return $request->wantsJson()
            ? new Response('', 204)
            : redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request, $message = '')
    {
        throw ValidationException::withMessages([
            $this->username() => [$message ?: trans('auth.failed')],
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new Response('', 204)
            : redirect('/');
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        //
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginFormBpUsr()
    {
        $this->data['title'] = trans('backpack::base.login'); // set the page title
        $this->data['username'] = $this->username();

        $blade = 'auth.login';

        return view(backpack_view($blade), $this->data);
    }
}
