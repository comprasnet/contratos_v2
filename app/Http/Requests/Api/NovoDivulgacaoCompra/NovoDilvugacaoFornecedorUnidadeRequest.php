<?php

namespace App\Http\Requests\Api\NovoDivulgacaoCompra;

use App\Http\Traits\NovoDivulgacaoCompra\NovoDivulgacaoTrait;
use App\Rules\CEPRule;
use App\Rules\MunicipioRule;
use App\Rules\UFRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class NovoDilvugacaoFornecedorUnidadeRequest extends FormRequest
{
    use NovoDivulgacaoTrait;
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->validationRequest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cep' => ['required' , new CEPRule()],
            'municipio' => ['required', new MunicipioRule($this->uf, $this->codigoMunicipio)],
            'uf' => ['required', new UFRule()],
            'logradouro' => 'required',
            'bairro' => 'required'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'uf' => 'UF'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
    
    /**
     * Método responsável em retornar os erros na API
     */
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(
                $validator->errors(),
                400,
                ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                JSON_UNESCAPED_UNICODE
            )
        );
    }
}
