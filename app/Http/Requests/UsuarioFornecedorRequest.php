<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuarioFornecedorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        //return backpack_auth()->check();
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'email' => 'required|email|unique:users,email|min:5|max:255'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'email' => 'E-mail'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'O campo :attribute é obrigatório',
            'email.email' => 'O campo :attribute deve ser um email válido',
            'email.unique' => 'Este :attribute já existe na nossa base de dados,
             devendo ser informado um diferente para o novo usuário.',
            'email.min' => 'O campo :attribute deve ter no mínimo 5 caracteres',
            'email.max' => 'O campo :attribute deve ter no máximo 255 caracteres'
        ];
    }
}
