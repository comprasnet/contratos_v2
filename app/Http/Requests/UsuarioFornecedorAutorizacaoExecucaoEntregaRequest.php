<?php

namespace App\Http\Requests;

use App\Models\AutorizacaoExecucao;
use App\Models\ContratoLocalExecucao;
use App\Repositories\Contrato\ContratoParametroRepository;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Traits\Formatador;
use Illuminate\Validation\Rule;

class UsuarioFornecedorAutorizacaoExecucaoEntregaRequest extends FormRequest
{
    use Formatador;
    protected function prepareForValidation()
    {
        $this->request->set('data_entrega', $this->convertDateGovToBd($this->data_entrega));
        $this->request->set('data_prazo_trp', $this->convertDateGovToBd($this->data_prazo_trp));
        $this->request->set('data_prazo_trd', $this->convertDateGovToBd($this->data_prazo_trd));

        if (empty($this->itens)) {
            $this->request->set('itens', []);
        }

        $this->itens = array_map(function ($item) {
            $item['quantidade_informada'] = $item['quantidade_informada'] ?
                $this->retornaFormatoAmericano($item['quantidade_informada']) : 0;
            $item['valor_glosa'] = $item['valor_glosa'] ?
                $this->retornaFormatoAmericano($item['valor_glosa']) : null;
            $item['quantidade_solicitada'] -= $item['quantidade_informada'] + $item['quantidade_analise'];
            return $item;
        }, $this->itens);

        $this->request->set('itens', $this->itens);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
        //return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'informacoes_complementares' => 'required_if:rascunho,0',
            'itens' => 'required_if:rascunho,0|nullable|array',
            'itens.*.itens_selecionados' => 'required_if:rascunho,0|nullable|integer|min:1',
            'itens.*.quantidade_informada' => 'required_if:rascunho,0|nullable|numeric',
            'rascunho' => 'required|boolean',
            'anexos' => 'nullable|array',
            'anexos.*.nome_arquivo_anexo' => 'required_with:anexos|string',
            'anexos.*.url_arquivo_anexo' => 'required_with:anexos|string',
            'anexos.*.remove_file' => 'required_with:anexos|boolean',
            'anexos.*.descricao_arquivo_anexo' => 'required_with:anexos|string',
            'mes_ano_referencia' => 'required_if:rascunho,0|nullable',
            'data_entrega' => 'required_if:rascunho,0|nullable|date',
            'data_prazo_trp' => 'required_if:rascunho,0|nullable|date|after_or_equal:data_entrega',
            'data_prazo_trd' => 'required_if:rascunho,0|nullable|date|after_or_equal:data_prazo_trp',
            'locais_execucao_entrega' => [
                Rule::requiredIf(function () {
                    return !$this->rascunho && ContratoLocalExecucao::whereHas('autorizacoesExecucao', function ($q) {
                            $q->where('autorizacaoexecucao_id', request()->autorizacaoexecucao_id);
                    })->count();
                }),
                'nullable',
                'array'
            ],
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'informacoes_complementares' => 'Informações Complementares',
            'itens' => 'Item da Ordem de Serviço / Fornecimento',
            'itens.*.itens_selecionados' => 'Item',
            'itens.*.quantidade_informada' => 'Quantidade Informada na Entrega',
            'mes_ano_referencia' => 'Mês/Ano de Referência',
            'data_entrega' => 'Data efetiva da entrega',
            'data_prazo_trp' => 'Data prevista para o recebimento provisório',
            'data_prazo_trd' => 'Data prevista para o recebimento definitivo',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'informacoes_complementares.required_if' => 'O campo :attribute é obrigatório',
            'itens.required_if' => 'Adicione ao menos um :attribute',
            'itens.*.itens_selecionados.required_if'=> 'Selecioone ao menos um :attribute',
            'itens.*.quantidade_informada.required_if' => 'O campo :attribute é obrigatório',
            'itens.*.numeric' => 'O campo :attribute deve ser um valor numérico',
            'mes_ano_referencia.required_if' => 'O campo :attribute é obrigatório',
            'data_entrega.required_if' => 'O campo :attribute é obrigatório.',
            'data_prazo_trp.required_if' => 'O campo :attribute é obrigatório.',
            'data_prazo_trd.required_if' => 'O campo :attribute é obrigatório.',
        ];
    }
}
