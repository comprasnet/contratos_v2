<?php

namespace App\Http\Requests;

use App\Models\Adesao;
use App\Rules\FornecedoresAdesaoDiferentesRule;
use App\Rules\ValidarAnexosAceitacaoAdesao;
use App\Rules\ValidarAnexosDemonstracaoAdesao;
use App\Rules\ValidarJustificativaItemIsoladoArpAdesaoRule;
use App\Rules\ValidarQuantidadeCaronaUnidadeSolicitanteArpAdesao;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\ArpItem;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\InputBag;

class AdesaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

     /**
      * Método responsável em filtrar a quantidade solicitada com os itens que realmente podem ser inseridos
      */
    private function filtrarQuantidadeSolicitada(array $quantidadeSolicitada, array $quantidadeLancamento)
    {
        # Filtra o array para retirar os valores nulos
        $quantidadeSolicitada = array_filter($quantidadeSolicitada);
        $arrayExclusao = array();

        # Percorre os itens recuperados no form e inclui no array os itens que serão excluídos
        foreach ($quantidadeSolicitada as $key => $item) {
            if (!in_array($key, $quantidadeLancamento)) {
                $arrayExclusao [] = $key;
            }
        }

        # Percorre os itens excluídos e remove do array principal
        foreach ($arrayExclusao as $excluir) {
            unset($quantidadeSolicitada[$excluir]);
        }

        return $quantidadeSolicitada;
    }
    
    private function getQuantidadeDados(array $quantidadeSolicitada)
    {
        $idItem = array_key_first(array_filter($quantidadeSolicitada));
        return ArpItem::find($idItem);
    }

    public function prepareForValidation()
    {
        $inputBag = $this->request;// correspondente aos dados de entrada da requisição e atribuímos à variável

        // Verifica se o campo 'enviar_fornecedor'
        // está presente, caso contrário define um valor padrão

        if (!$inputBag->has('enviar_fornecedor')) {
            $this->request->set('enviar_fornecedor', 0);
        }

        $quantidadeSolicitada =
        $this->filtrarQuantidadeSolicitada(
            $inputBag->get('quantidade_solicitada'),
            $inputBag->get('item_solicitacao_lancamento')
        );
        $this->request->set('quantidade_solicitada', $quantidadeSolicitada);
        
        $intersectIdUnidadeGerenciadora =
            $this->getIdsUnicos($quantidadeSolicitada, $inputBag, 'unidades_gerenciadoras');
        $this->request->set('count_unidades_gerenciadoras', count($intersectIdUnidadeGerenciadora));
        
        $intersectIdCompra = $this->getIdsUnicos($quantidadeSolicitada, $inputBag, 'compras_ids');
        $this->request->set('count_compras', count($intersectIdCompra));
        
        $quantidadeDados = $this->getQuantidadeDados($quantidadeSolicitada);
        
        $dadosGerais = $this->request->unidade_gerenciadora_id ?? $quantidadeDados->arp->unidade_origem_id;
        $this->request->set('unidade_gerenciadora_id', $dadosGerais);
        
        $compraId = $quantidadeDados->arp->compra_id;
        $this->request->set('compra_id', $compraId);
        
        $numeroAno = $quantidadeDados->arp->getNumeroCompra();
        $this->request->set('numero_compra', $numeroAno);
        
        $fornecedorId = $quantidadeDados->item_fornecedor_compra->fornecedor_id;
        $this->request->set('fornecedor_id', $fornecedorId);
        
        $arpId = $quantidadeDados->arp->id;
        $this->request->set('arp_id', $arpId);

        $this->request->remove('arp_item');
//        dd($this->all());
    }
    
    private function requiresJustificativaValidation()
    {
        $idAdesao = Route::current()->parameter("id");

        $adesao = Adesao::find($idAdesao);

        $arraySaida = [];

        # Se tiver um arquivo de justificativa, exibe para o usuário
        if (!empty($adesao->justificativa)) {
            $arrayArquivo = explode('-', $adesao->justificativa);

            // Adiciona o nome do arquivo ao array de saída
            $arraySaida['Nome'] = [str_replace("_", " ", $arrayArquivo[1])];
        }
        //dd(empty($arraySaida['Nome']));

        // true ou false
        return empty($arraySaida['Nome']);
    }
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
    public function rules()
    {
        $idAdesao = Route::current()->parameter("id");

        $validationRules = [
            'compra_id' => 'required',
            'count_unidades_gerenciadoras' => 'max:1',
            'count_compras' => 'max:1',
            
            // itens selecionados
            'quantidade_solicitada' => [
                'array',
                'min:1',
                new ValidarQuantidadeCaronaUnidadeSolicitanteArpAdesao(
                    $this->unidade_origem_id,
                    $this->aquisicao_emergencial_medicamento_material,
                    $this->execucao_descentralizada_programa_projeto_federal
                )
            ],
            
            // radio
            'demonstracao_valores_registrados' => 'required_if:rascunho,0',
            'consulta_aceitacao_fornecedor' => 'required_if:rascunho,0',
            
            'texto_justificativa' => 'required_if:rascunho,0',
            'processo_adesao' => 'required',
            
            'texto_justificativa_item_isolado' => [
                new ValidarJustificativaItemIsoladoArpAdesaoRule(
                    $this->grupo_compra_quantidade_solicitada,
                    $this->rascunho
                )
            ],
            'justificativa.*' => 'nullable|file|mimes:ppt,xls,pdf|max:30720|required_if:rascunho,0',
            'demonstracao.*' => 'nullable',
            'demonstracao' => [
                'nullable',
                new ValidarAnexosDemonstracaoAdesao($this->rascunho, $idAdesao, $this->getMethod())
            ],
            'aceitacao.*' => 'nullable',
            'aceitacao' => [
                'nullable',
                new ValidarAnexosAceitacaoAdesao($this->rascunho, $idAdesao, $this->getMethod())
            ]

        ];

        if (empty($idAdesao)) {
            $validationRules['ata_enfrentando_impacto_decorrente_calamidade_publica'] = 'required';
        }

        // Adiciona a regra para 'justificativa' apenas se necessário
        // se meu return for true então ele faz a validação do campo
        // pois quer dizer que não existe arquivo vinculado.
        if ($this->requiresJustificativaValidation()) {
            $validationRules['justificativa'] =
                'nullable|file|mimes:ppt,xls,pdf|max:30720|required_if:rascunho,0';
        }
        
        return $validationRules;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
           'count_unidades_gerenciadoras' => 'Unidade Gerenciadora',
           'count_compras' => 'Compra'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'compra_id.required' => 'Favor preencher os campos de unidade gerenciadora, número da compra, modalidade e fornecedor.',
            'count_unidades_gerenciadoras.max' => 'Atenção não é permitida a inclusão de mais de uma :attribute por Solicitação.',
            'count_compras.max' => 'Atenção não é permitida a inclusão de mais de uma :attribute por Solicitação.',

            'demonstracao.max' => 'O campo Anexo demonstração não pode ser maior que 30 MB.',
            'aceitacao.max' => 'O campo Anexo aceitação não pode ser maior que 30 MB.',
            'justificativa.max' => 'O campo Anexo aceitação não pode ser maior que 30 MB.',


            'demonstracao.*.max' => 'O campo anexo demonstração não pode ser superior a 30MB.',
            'aceitacao.*.max' => 'O campo anexo aceitação não pode ser superior a 30MB.',
            'justificativa.*.max' => 'O campo anexo justificativa não pode ser superior a 30MB.',

        ];
    }
    /**
     * @param $quantidadeSolicitada
     * @param InputBag $inputBag
     * @return array
     */
    public function getIdsUnicos($quantidadeSolicitada, InputBag $inputBag, $campo)
    {
        $idsItens = array_filter($quantidadeSolicitada);

        $inputValues = $inputBag->get($campo);
        $intersectId = [];

        if (is_array($inputValues)) {
            $intersectId = array_intersect_key($inputValues, $idsItens);
            $intersectId = array_unique($intersectId);
        }


        return $intersectId;
    }
}
