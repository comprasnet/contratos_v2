<?php

namespace App\Http\Requests;

use App\Rules\EnviarArquivoAtaPncp;
use Illuminate\Foundation\Http\FormRequest;

class ArpArquivoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo_id' => [
                'required',
                'exists:arp_arquivo_tipos,id',
                new EnviarArquivoAtaPncp($this->restrito, $this->arp_id)
            ],
            'descricao' => 'required',
            'arquivo' => 'required|mimes:ppt,xls,pdf|max:30720'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'tipo_id' => 'tipo'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'tipo_id.required'=>"Campo :attribute é obrigatório",
            'tipo_id.exists'=>"Selecione um tipo válido para o arquivo",
            'descricao.required'=>"Campo Descrição é obrigatório",
            'arquivo.required'=>"Campo Arquivo é obrigatório",
        ];
    }
}
