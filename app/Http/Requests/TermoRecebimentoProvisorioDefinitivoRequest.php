<?php

namespace App\Http\Requests;

use App\Http\Traits\EntregaTrpTrd\EntregaTrpTrdValidationTrait;
use App\Http\Traits\EntregaTrpTrd\TrpTrdValidationTrait;
use App\Http\Traits\Formatador;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TermoRecebimentoProvisorioDefinitivoRequest extends FormRequest
{
    use Formatador;
    use EntregaTrpTrdValidationTrait;

    private $acao;

    protected function prepareForValidation()
    {
        $this->acao = strstr($this->route()->uri, 'trp/') ? 'trp' : 'trd';

        $this->request->set('valor_total', round($this->valor_total, 2));

        if (empty($this->vinculos)) {
            $this->request->set('vinculos', []);
        } else {
            $vinculosFormatado = $this->vinculos;

            foreach ($this->vinculos as $key => $vinculo) {
                if (!empty($vinculosFormatado[$key]['itens'])) {
                    $vinculosFormatado[$key]['itens'] = array_map(function ($item) {
                        $item['quantidade_informada'] = $item['quantidade_informada'] ?
                            $this->retornaFormatoAmericano($item['quantidade_informada']) : 0;
                        $item['valor_glosa'] = $item['valor_glosa'] ?
                            $this->retornaFormatoAmericano($item['valor_glosa']) : null;
                        $item['data_inicio'] = $item['data_inicio'] ?
                            $this->convertDateGovToBd($item['data_inicio']) : null;
                        $item['data_fim'] = $item['data_fim'] ? $this->convertDateGovToBd($item['data_fim']) : null;
                        return $item;
                    }, $vinculosFormatado[$key]['itens']);
                }
            }

            $this->request->set('vinculos', $vinculosFormatado);
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array_merge(
            $this->getDefaultRules(),
            [
                'introducao' => 'required_if:rascunho,0|nullable|string',
                'vinculos.*.itens.*.mes_ano_competencia' => 'nullable|date',
                'vinculos.*.itens.*.processo_sei' => 'nullable|string',
                'vinculos.*.itens.*.data_inicio' => [
                    'nullable',
                    'date',
                    'before_or_equal:vinculos.*.itens.*.data_inicio'
                ],
                'vinculos.*.itens.*.data_fim' => [
                    'nullable',
                    'date',
                ],
                'vinculos.*.itens.*.horario' => 'nullable|date_format:H:i',
                'modal_select_prepostos_responsaveis' => [
                    Rule::requiredIf(function () {
                        return !$this->rascunho;
                    }),
                    'nullable',
                    'array'
                ],
            ]
        );

        if ($this->acao === 'trd') {
            $rules = array_merge(
                $rules,
                ['incluir_instrumento_cobranca' => 'required_if:rascunho,0|nullable|boolean']
            );
        } elseif ($this->acao === 'trp') {
            $rules = array_merge(
                $rules,
                [
                    'vinculos.*.mes_ano_referencia' => 'required_if:rascunho,0|nullable|date',
                    'vinculos.*.locais_execucao' => 'nullable|array',
                    'vinculos.*.locais_execucao.*' => 'nullable|integer',
                ]
            );
        }

        return $rules;
    }

    public function attributes()
    {
        return array_merge(
            $this->getDefaultAttributes()
        );
    }

    public function messages()
    {
        return $this->getDefaultMessages();
    }
}
