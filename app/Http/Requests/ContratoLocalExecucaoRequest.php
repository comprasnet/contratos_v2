<?php

namespace App\Http\Requests;

use App\Rules\CEPRule;
use Illuminate\Foundation\Http\FormRequest;

class ContratoLocalExecucaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_user()->can('autorizacaoexecucao_inserir');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descricao' => 'required',
            'municipios_id' => 'required|exists:municipios,id',
            'cep' => ['required', 'string', new CEPRule()],
            'bairro' => 'required',
            'logradouro' => 'required',
            'complemento' => 'nullable',
            'numero_endereco' => 'nullable',
        ];
    }

    public function attributes()
    {
        return [
            'descricao' => 'Descrição',
            'cep' => 'CEP',
            'municipios_id' => 'Localidade/UF',
            'logradouro' => 'Logradouro/Nome',
            'bairro' => 'Bairro/Distrito',
        ];
    }

    public function messages()
    {
        return [
            'descricao.required' => 'O campo :attribute é obrigatório.',
            'bairro.required' => 'O campo :attribute é obrigatório.',
            'cep.required' => 'O campo :attribute é obrigatório.',
            'municipios_id.required' => 'O campo :attribute é obrigatório.',
        ];
    }
}
