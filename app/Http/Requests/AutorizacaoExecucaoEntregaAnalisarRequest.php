<?php

namespace App\Http\Requests;

use App\Http\Traits\Formatador;
use App\Models\AutorizacaoExecucaoEntrega;
use App\Repositories\Contrato\ContratoResponsavelRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AutorizacaoExecucaoEntregaAnalisarRequest extends FormRequest
{
    use Formatador;

    private $autorizacaoExecucaoEntrega;

    protected function prepareForValidation()
    {
        $this->request->set('data_entrega', $this->convertDateGovToBd($this->data_entrega));
        $this->request->set('data_prazo_trp', $this->convertDateGovToBd($this->data_prazo_trp));
        $this->request->set('data_prazo_trd', $this->convertDateGovToBd($this->data_prazo_trd));

        if (empty($this->itens)) {
            $this->request->set('itens', []);
        }

        $this->itens = array_map(function ($item) {
            $item['quantidade_informada'] = $item['quantidade_informada'] ?
                $this->retornaFormatoAmericano($item['quantidade_informada']) : 0;
            $item['valor_glosa'] = $item['valor_glosa'] ?
                $this->retornaFormatoAmericano($item['valor_glosa']) : null;
            $item['quantidade_solicitada'] -= $item['quantidade_informada'] + $item['quantidade_analise'];
            return $item;
        }, $this->itens);

        $this->autorizacaoExecucaoEntrega = AutorizacaoExecucaoEntrega::find($this->entrega_id);

        $this->request->set('itens', $this->itens);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $contratoResponsavelRepository = new ContratoResponsavelRepository($this->contrato_id);
        return $contratoResponsavelRepository->isUsuarioResponsavel() &&
            $this->autorizacaoExecucaoEntrega->situacao->descres == 'ae_entrega_status_1';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'acao' => 'required|string|in:aprovar,recusar,cancelar',
            'justificativa_motivo_analise' => [
                Rule::requiredIf(function () {
                    if ($this->acao == 'recusar') {
                        return true;
                    }
                    // verifica se as datas foram alteradas
                    if ($this->autorizacaoExecucaoEntrega->data_entrega != $this->data_entrega ||
                        $this->autorizacaoExecucaoEntrega->data_prazo_trp != $this->data_prazo_trp ||
                        $this->autorizacaoExecucaoEntrega->data_prazo_trd != $this->data_prazo_trd) {
                        return true;
                    }


                    // verifica se foi alterado algum item na análise
                    $entregaItens = $this->autorizacaoExecucaoEntrega->entregaItens;
                    $possuiAlteracao = false;
                    foreach ($this->itens as $item) {
                        $entregaItem = $entregaItens
                            ->where('autorizacao_execucao_itens_id', $item['itens_selecionados'])
                            ->where('quantidade_informada', $item['quantidade_informada'])
                            ->where('valor_glosa', $item['valor_glosa'])
                            ->first();

                        if (!$entregaItem) {
                            $possuiAlteracao = true;
                            break;
                        }
                    }
                    return $possuiAlteracao;
                }),
                'nullable',
                'string',
            ],
            'itens' => 'required|nullable|array',
            'itens.*.itens_selecionados' => 'required|nullable|integer|min:1',
            'itens.*.quantidade_informada' => 'required|nullable|numeric',
            'itens.*.quantidade_solicitada' => 'required|nullable|numeric|min:0',
            'itens.*.valor_glosa' => 'nullable|numeric',
            'itens.*.valor_total_entrega' => 'required|nullable|numeric|min:0',
            'data_entrega' => 'required|nullable|date',
            'data_prazo_trp' => 'required|nullable|date|after_or_equal:data_entrega',
            'data_prazo_trd' => 'required|nullable|date|after_or_equal:data_prazo_trp',
        ];
    }

    public function attributes()
    {
        return [
            'itens' => 'Item da Ordem de Serviço / Fornecimento',
            'itens.*.itens_selecionados' => 'Item',
            'itens.*.quantidade_informada' => 'Quantidade Informada na Entrega',
            'itens.*.quantidade_solicitada' => 'Quantidade Solicitada na Entrega',
            'itens.*.valor_glosa' => 'Glosa',
            'itens.*.valor_total_entrega' => 'Valor Total da Entrega',
            'data_entrega' => 'Data efetiva da entrega',
            'data_prazo_trp' => 'Data prevista para o recebimento provisório',
            'data_prazo_trd' => 'Data prevista para o recebimento definitivo',
            'justificativa_motivo_analise' => 'Justificativa / Motivo da Análise',
        ];
    }

    public function messages()
    {
        return [
            'justificativa_motivo_analise' => 'O campo :attribute é obrigatório.',
            'data_entrega.required_if' => 'O campo :attribute é obrigatório.',
            'data_prazo_trp.required_if' => 'O campo :attribute é obrigatório.',
            'data_prazo_trd.required_if' => 'O campo :attribute é obrigatório.',
            'itens.required' => 'Adicione ao menos um :attribute.',
            'itens.*.itens_selecionados.required'=> 'Selecioone ao menos um :attribute.',
            'itens.*.quantidade_informada.required' => 'O campo :attribute é obrigatório.',
            'itens.*.quantidade_solicitada.required' => 'O campo :attribute é obrigatório.',
            'itens.*.quantidade_solicitada.min' => 'Quantidade informada e em análise não pode ser maior que a 
                quantidade solicitada na OS/F.',
            'itens.*.valor_glosa.required' => 'O campo :attribute é obrigatório.',
            'itens.*.numeric' => 'O campo :attribute deve ser um valor numérico.',
            'itens.*.valor_total_entrega.min' => 'O campo :attribute não pode ser negativo.',
        ];
    }
}
