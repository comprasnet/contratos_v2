<?php

namespace App\Http\Requests;

use App\Http\Traits\ArquivoGenericoTrait;
use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoValidationTrait;
use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoexecucaoHistorico;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoHistoricoAlterarService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AutorizacaoExecucaoAlterarRequest extends FormRequest
{
    use AutorizacaoexecucaoValidationTrait;
    use ArquivoGenericoTrait;

    private $autorizacaoexecucao;

    protected function prepareForValidation()
    {
        $this->prepareFields();

        $this->autorizacaoexecucao = AutorizacaoExecucao::findOrFail($this->autorizacaoexecucao_id);

        $this->request->set('data_vigencia_inicio', $this->autorizacaoexecucao->data_vigencia_inicio);
        $this->request->set('data_assinatura', $this->autorizacaoexecucao->data_assinatura);
        $this->request->set('data_inicio_alteracao', $this->convertDateGovToBd($this->data_inicio_alteracao));
        $this->request->set('data_extincao', $this->convertDateGovToBd($this->data_extincao));

        if ($this->tipo_alteracao['itens'] == 0) {
            $this->request->set('autorizacaoexecucaoItens', null);
        }
        $this->merge(['autorizacaoexecucoes_id' => $this->autorizacaoexecucao_id]);
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $aehAlterarService = new AutorizacaoExecucaoHistoricoAlterarService($this->autorizacaoexecucao);

        if ($this->request->get('rascunho') == 0) {
            return backpack_auth()->check() &&
                (
                    $this->autorizacaoexecucao->situacao->descres == 'ae_status_2' ||
                    $this->autorizacaoexecucao->situacao->descres == 'ae_status_8'
                ) &&
             !$aehAlterarService->verifyAlteracoesAguardandoAssinaturaOuAssinada();
        }
        return backpack_auth()->check() &&
            (
                $this->autorizacaoexecucao->situacao->descres == 'ae_status_2' ||
                $this->autorizacaoexecucao->situacao->descres == 'ae_status_8'
            );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            $this->getItensRules(),
            [
                'rascunho' => 'required|boolean',
                'tipo_alteracao' => [
                    'required_array_keys:vigencia,itens,extingir',
                    'required',
                    'array',
                    function ($attribute, $value, $fail) {
                        if (!in_array(1, $value)) {
                            $fail('Selecione um tipo de alteração');
                        }
                    },
                ],
                'data_inicio_alteracao' => [
                    'required_if:rascunho,0',
                    'nullable',
                    'date',
                    'after_or_equal:today',
                ],
                'justificativa_motivo' => [
                    'required_if:rascunho,0',
                    'nullable',
                    'string'
                ],
                'data_vigencia_fim' => [
                    'bail',
                    Rule::requiredIf(function () {
                        if ($this->request->count()) {
                            return $this->rascunho == 0 && $this->tipo_alteracao['vigencia'] == 1;
                        }
                        return false;
                    }),
                    'nullable',
                    'date',
                    'after_or_equal:data_vigencia_inicio_contrato',
                    $this->addBeforeOrEqualDataVigenciaFimContratoIfExist(),
                    'after_or_equal:data_vigencia_inicio',
                ],
                'data_extincao' => [
                    'bail',
                    Rule::requiredIf(function () {
                        if ($this->request->count()) {
                            return $this->rascunho == 0 && $this->tipo_alteracao['extingir'] == 1;
                        }
                        return false;
                    }),
                    'nullable',
                    'date'
                ],
                'autorizacaoexecucaoItens' => [
                    Rule::requiredIf(function () {
                        if ($this->request->count()) {
                            return $this->rascunho == 0 && $this->tipo_alteracao['itens'] == 1;
                        }
                        return false;
                    }),
                    'nullable',
                    'array'
                ],
                'modal_select_prepostos_responsaveis' => 'required_if:rascunho,0|nullable|array'
            ],
        );
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return array_merge(
            $this->getDefaultAttributes(),
            [
                'tipo_alteracao' => 'Tipo de alteração',
                'data_inicio_alteracao' => 'Data de início da alteração',
                'justificativa_motivo' => 'Justificativa da alteração',
                'data_extincao' => 'Data de extinção',
            ],
        );
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            $this->getDefaultMessages(),
            [
                'justificativa_motivo.required_if' => 'O campo :attribute é obrigatório.',
                'data_inicio_alteracao.required_if' => 'O campo :attribute é obrigatório.',
                'data_extincao.required_if' => 'O campo :attribute é obrigatório.',
            ]
        );
    }

    private function existArquivoGenerico()
    {
        return $this->id && $this->getArquivoGenerico($this->id, AutorizacaoexecucaoHistorico::class) != null;
    }
}
