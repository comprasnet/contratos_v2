<?php

namespace App\Http\Requests;

use App\Rules\ItemAdesaoExisteMinutaContratoRule;
use App\Rules\ItemCaronaRecebeuRemanejamentoRule;
use Illuminate\Foundation\Http\FormRequest;

class ArpAdesaoCancelamentoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'adesaoIdCancelamento' => [
                'required',
                'integer',
                new ItemAdesaoExisteMinutaContratoRule(),
                new ItemCaronaRecebeuRemanejamentoRule()
            ],
            'justificativaCancelamento' => 'required|string|max:250'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'adesaoIdCancelamento' => 'id da adesão',
            'justificativaCancelamento' => 'justificativa do cancelamento'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'adesaoIdCancelamento.required' => 'A :attribute é obrigatório!',
            'adesaoIdCancelamento.integer' => 'O campo :attribute deve ser um número inteiro.',
            'justificativaCancelamento.required' => 'A :attribute é obrigatório!',
            'justificativaCancelamento.string' => 'O campo :attribute deve ser uma string.',
            'justificativaCancelamento.max' => 'O campo :attribute não pode ter mais de 250 caracteres.'
        ];
    }
}
