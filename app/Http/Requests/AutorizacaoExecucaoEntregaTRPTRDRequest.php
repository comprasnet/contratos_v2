<?php

namespace App\Http\Requests;

use App\Http\Traits\Formatador;
use App\Models\AutorizacaoExecucaoEntrega;
use App\Models\ContratoLocalExecucao;
use App\Repositories\Contrato\ContratoResponsavelRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AutorizacaoExecucaoEntregaTRPTRDRequest extends FormRequest
{
    use Formatador;

    private $acao;
    private $autorizacaoExecucaoEntrega;

    protected function prepareForValidation()
    {
        $this->acao = strstr($this->route()->uri, '/trp/') ? 'trp' : 'trd';

        if (empty($this->itens)) {
            $this->request->set('itens', []);
        }

        $this->itens = array_map(function ($item) {
            $item['quantidade_informada'] = $item['quantidade_informada'] ?
                $this->retornaFormatoAmericano($item['quantidade_informada']) : 0;
            $item['valor_glosa'] = $item['valor_glosa'] ?
                $this->retornaFormatoAmericano($item['valor_glosa']) : null;
            $item['quantidade_solicitada'] -= $item['quantidade_informada'] + $item['quantidade_analise'] +
                $item['quantidade_avaliacao'] + $item['quantidade_executada'];
            $item['data_inicio'] = $item['data_inicio'] ? $this->convertDateGovToBd($item['data_inicio']) : null;
            $item['data_fim'] = $item['data_fim'] ? $this->convertDateGovToBd($item['data_fim']) : null;
            return $item;
        }, $this->itens);

        $this->autorizacaoExecucaoEntrega = AutorizacaoExecucaoEntrega::find($this->id);

        $this->request->set('itens', $this->itens);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $contratoResponsavelRepository = new ContratoResponsavelRepository($this->contrato_id);
        return $contratoResponsavelRepository->isUsuarioResponsavel() && $this->verificaSituacaoByAcao();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            [
                'rascunho' => 'required|boolean',
                'itens' => 'required|nullable|array',
                'itens.*.itens_selecionados' => 'required|nullable|integer|min:1',
                'itens.*.quantidade_informada' => 'required|nullable|numeric|gt:0',
                'itens.*.quantidade_solicitada' => 'required|nullable|numeric|min:0',
                'itens.*.valor_glosa' => 'nullable|numeric',
                'itens.*.valor_total_entrega' => 'required|nullable|numeric|min:0',
                'itens.*.mes_ano_competencia' => 'nullable|date',
                'itens.*.processo_sei' => 'nullable|string',
                'itens.*.data_inicio' => [
                    'nullable',
                    'date',
                    'before_or_equal:itens.*.data_fim'
                ],
                'itens.*.data_fim' => [
                    'nullable',
                    'date',
                ],
                'itens.*.horario' => 'nullable|date_format:H:i',
                'locais_execucao_entrega' => 'nullable|array',
                'locais_execucao_entrega.*' => 'nullable|integer',
                'mes_ano_referencia' => 'required_if:rascunho,0|nullable',
                'modal_select_prepostos_responsaveis' => 'required_if:rascunho,0|nullable|array',
            ],
            $this->getRulesByAcao()
        );
    }

    public function attributes()
    {
        return [
            'introducao_trp' => 'Introdução',
            'informacoes_complementares_trp' => 'Informações Complementares',

            'introducao_trd' => 'Introdução',
            'informacoes_complementares_trd' => 'Informações Complementares',
            'incluir_instrumento_cobranca' => 'Incluir Instrumento de Cobrança?',


            'itens' => 'Item da Ordem de Serviço / Fornecimento',
            'itens.*.itens_selecionados' => 'Item',
            'itens.*.quantidade_informada' => 'Quantidade Informada na Entrega',
            'itens.*.quantidade_solicitada' => 'Quantidade Solicitada na Entrega',
            'itens.*.valor_glosa' => 'Glosa',
            'itens.*.valor_total_entrega' => 'Valor Total da Entrega',
            'locais_execucao_entrega' => 'Local de Execução da Entrega',
            'mes_ano_referencia' => 'Mês/Ano de Referência',
        ];
    }

    public function messages()
    {
        return [
            'introducao_trp.required_if' => 'O campo :attribute é obrigatório.',
            'informacoes_complementares_trp.required_if' => 'O campo :attribute é obrigatório.',

            'introducao_trd.required_if' => 'O campo :attribute é obrigatório.',
            'informacoes_complementares_trd.required_if' => 'O campo :attribute é obrigatório.',
            'incluir_instrumento_cobranca.required_if' => 'O campo :attribute é obrigatório.',

            'itens.required' => 'Adicione ao menos um :attribute.',
            'itens.*.itens_selecionados.required'=> 'Selecione ao menos um :attribute.',
            'itens.*.quantidade_informada.required' => 'O campo :attribute é obrigatório.',
            'itens.*.quantidade_solicitada.required' => 'O campo :attribute é obrigatório.',
            'itens.*.quantidade_solicitada.min' => 'Quantidade informada e em análise não pode ser maior que a 
                quantidade solicitada na OS/F.',
            'itens.*.valor_glosa.required' => 'O campo :attribute é obrigatório.',
            'itens.*.data_inicio.before_or_equal' => 'A data de inicio deve ser menor ou igual a data de fim.',
            'itens.*.numeric' => 'O campo :attribute deve ser um valor numérico.',
            'itens.*.valor_total_entrega.min' => 'O campo :attribute não pode ser negativo.',
            'locais_execucao_entrega.required_if' => 'O campo :attribute é obrigatório.',
            'mes_ano_referencia.required_if' => 'O campo :attribute é obrigatório.',
        ];
    }

    private function verificaSituacaoByAcao(): bool
    {
        if ($this->acao === 'trp') {
            return $this->autorizacaoExecucaoEntrega->situacao->descres == 'ae_entrega_status_2'  ||
                $this->autorizacaoExecucaoEntrega->situacao->descres == 'ae_entrega_status_6';
        } elseif ($this->acao === 'trd') {
            return $this->autorizacaoExecucaoEntrega->situacao->descres == 'ae_entrega_status_10' ||
            $this->autorizacaoExecucaoEntrega->situacao->descres == 'ae_entrega_status_11';
        }

        return false;
    }

    private function getRulesByAcao(): array
    {
        if ($this->acao === 'trp') {
            return [
                'introducao_trp' => 'required_if:rascunho,0|nullable|string',
                'informacoes_complementares_trp' => 'required_if:rascunho,0|nullable|string',
            ];
        }

        return [
            'introducao_trd' => 'required_if:rascunho,0|nullable|string',
            'informacoes_complementares_trd' => 'required_if:rascunho,0|nullable|string',
            'incluir_instrumento_cobranca' => 'required_if:rascunho,0|nullable|boolean',
        ];
    }
}
