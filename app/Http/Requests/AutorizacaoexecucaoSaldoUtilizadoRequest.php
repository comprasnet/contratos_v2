<?php

namespace App\Http\Requests;

use App\Http\Traits\Formatador;
use App\Models\Contrato;
use App\Models\Contratoitem;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AutorizacaoexecucaoSaldoUtilizadoRequest extends FormRequest
{
    use Formatador;

    private $tipoMaterial;

    protected function prepareForValidation()
    {
        if ($this->contratoitens_id) {
            $contratoItem = Contratoitem::findOrFail($this->contratoitens_id);
            $this->tipoMaterial = $contratoItem->tipo->descres;

            $this->quantidade = str_replace(',', '.', $this->quantidade);
            $this->merge(['quantidade' => $this->quantidade]);
        }
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $result = [
            'contratoitens_id' => [
                'required',
                'integer',
                'exists:contratoitens,id',
                Rule::unique('autorizacaoexecucao_saldo_utilizado', 'contratoitens_id')->ignore($this->id),
            ],
            'quantidade' => 'required|integer',
            'valor_unitario' => 'nullable|numeric',
            'numero_autorizacao' => 'nullable|string'
        ];

        if ($this->tipoMaterial === 'SERVIÇO') {
            $result['quantidade'] = 'required|numeric';
        }

        return $result;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'contratoitens_id' => 'contrato item',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'vigencia_inicio.before' => 'O campo :attribute deve ser uma data anterior à ' .
                $this->formatDatePtBR($this->contrato_vigencia_fim) . '.',
            'vigencia_inicio.after' => 'O campo :attribute deve ser uma data posterior à ' .
                $this->formatDatePtBR($this->contrato_vigencia_inicio) . '.',
            'vigencia_fim.before' => 'O campo :attribute deve ser uma data anterior à ' .
                $this->formatDatePtBR($this->contrato_vigencia_fim) . '.',
            'vigencia_fim.after' => 'O campo :attribute deve ser uma data posterior à ' .
                $this->formatDatePtBR($this->contrato_vigencia_inicio) . '.',
        ];
    }
}
