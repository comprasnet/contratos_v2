<?php

namespace App\Http\Requests;

use App\Http\Traits\NovoDivulgacaoCompra\NovoDivulgacaoTrait;
use App\Models\BackpackUser;
use App\Rules\ValidarCompraSisrp14133DerivadasRule;
use App\Rules\ValidarUsuarioAtualizarCompraV1;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class AtualizarCompraV1Request extends FormRequest
{
    use NovoDivulgacaoTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->usuarioHabilitado();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'compra_id' => [
                'required',
                'exists:compras,id',
                new ValidarCompraSisrp14133DerivadasRule()
            ],
            'user_id' => [
                'required',
                'exists:users,id',
            ]
        ];
    }
    
    public function attributes()
    {
        return [
            'compra_id' => 'id da compra'
        ];
    }
    
    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'compra_id.required' => 'O campo :attribute é obrigatório.'
        ];
    }
    
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(
                $validator->errors(),
                400,
                ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                JSON_UNESCAPED_UNICODE
            )
        );
    }

    private function usuarioHabilitado()
    {
        $user = BackpackUser::find($this->all()['user_id']);

        if (empty($user)) {
            return false;
        }

        $grupoDeAcesso = $user->roles->pluck('name')->toArray();

        $authApi = auth('api');

        if ($authApi->check() && in_array('Administrador', $grupoDeAcesso)) {
            return true;
        }

        return  false;
    }
}
