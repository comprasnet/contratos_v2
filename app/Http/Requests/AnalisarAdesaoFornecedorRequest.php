<?php

namespace App\Http\Requests;

use App\Models\CodigoItem;
use App\Repositories\Arp\ArpAdesaoItemRepository;
use App\Rules\AnalisarItemAdesaoRule;
use App\Services\Arp\ArpItemHistoricoService;
use Illuminate\Foundation\Http\FormRequest;

class AnalisarAdesaoFornecedorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }
    
    protected function prepareForValidation()
    {
        $this->formatarStatusId();
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantidade_aprovada' => [
                'array',
                'min:1',
                'required',
                new AnalisarItemAdesaoRule($this->status_id, $this->motivo_analise, $this->id)
            ],
            'quantidade_aprovada.*' => [
                'nullable',
                'numeric'
            ],
            'status_id' => [
                'array',
                'min:1',
                'required'
            ],
            'status_id.*' => [
                'nullable',
                'integer'
            ],
            'motivo_analise' => [
                'array',
                'nullable'
            ],
            'motivo_analise.*' => [
                'nullable',
                'string'
            ],
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {

        return [
            'quantidade_aprovada' => 'quantidade solicitada',
            'status_id' => 'analisar',
            'motivo_analise' => 'justificativa/motivação da análise',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'quantidade_aprovada.array' => 'O campo :attribute deve ser um array',
            'quantidade_aprovada.min' => 'O campo :attribute deve ao menos um item para analisar',
            'quantidade_aprovada.required' => 'O campo :attribute é obrigatório',
            'quantidade_aprovada.*.numeric' => 'O campo :attribute deve ser um número',
            'status_id.array' => 'O campo :attribute deve ser um array',
            'status_id.min' => 'O campo :attribute deve ao menos um item para analisar',
            'status_id.required' => 'O campo :attribute é obrigatório',
            'status_id.*.integer' => 'O campo :attribute deve ser um inteiro',
            'motivo_analise.array' => 'O campo :attribute deve ser um array',
            'motivo_analise.string' => 'O campo :attribute deve ser uma string',
        ];
    }
    
    private function formatarStatusId()
    {
        $arpAdesaoItemRepository = new ArpAdesaoItemRepository();
        $arpItemHistoricoService = new ArpItemHistoricoService();
        $arraySituacao = array();
        $arrayMotivo = array();
        
        foreach ($this->quantidade_aprovada as $idItemAdesao => $quantidadeAprovada) {
            $itemAdesao = $arpAdesaoItemRepository->getItemAdesaoUnico($idItemAdesao);
            $itemCanceladoRemovido =
                $arpItemHistoricoService->itemAtaCanceladoRemovido($itemAdesao->item_arp_fornecedor_id);
            if ($itemCanceladoRemovido) {
                $situacaoNegada = CodigoItem::whereHas('codigo', function ($query) {
                    $query->where('descricao', '=', 'Tipo de Ata de Registro de Preços')
                        ->whereNull('deleted_at');
                })
                    ->whereNull('deleted_at')
                    ->where('descricao', 'Negar')
                    ->first();
                $arraySituacao[$idItemAdesao] = $situacaoNegada->id;
                $arrayMotivo[$idItemAdesao] = 'Item e/ou ata estão cancelados. Não é possível aceitar adesão.';
                continue;
            }
            $arraySituacao[$idItemAdesao] = (int) $this->status_id[$idItemAdesao];
            $arrayMotivo[$idItemAdesao] = $this->motivo_analise[$idItemAdesao] ?? null;
        }
        $this->merge([
            'status_id' => $arraySituacao,
            'motivo_analise' => $arrayMotivo
        ]);
    }
}
