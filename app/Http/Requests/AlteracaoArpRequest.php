<?php

namespace App\Http\Requests;

use App\Http\Traits\Formatador;
use App\Models\CodigoItem;
use App\Rules\ValidarAlteracaoAtaVigencia;
use App\Rules\ValidarCancelamentoItemAlteracaoAta;
use App\Rules\ValidarDataAssinaturaAlteracaoAta;
use App\Rules\ValidarFornecedorAlteracaoAta;
use App\Rules\ValidarInformativoAlteracaoAta;
use Illuminate\Foundation\Http\FormRequest;

class AlteracaoArpRequest extends FormRequest
{
    use Formatador;
    
    private function itemAlteracaoFornecedor()
    {
        $novoCnpjFornecedor = array_filter($this->novo_cnpj_fornecedor ?? []);
        $this->request->set('novo_cnpj_fornecedor', $novoCnpjFornecedor);
        $keyNovoCnpj = array_keys($this->novo_cnpj_fornecedor);
        $keyNovoNome = array_keys(array_filter($this->novo_nome_cnpj_fornecedor?? []));

        $itemAlteracaoFornecedor = array();
        $itemAlteracaoFornecedor = array_unique(array_merge($itemAlteracaoFornecedor, $keyNovoCnpj, $keyNovoNome));
        
        $this->request->set('item_alteracao_fornecedor', $itemAlteracaoFornecedor);
    }
    
    public function prepareForValidation()
    {
        $this->request->set(
            'data_assinatura_alteracao_vigencia',
            $this->convertDateGovToBd($this->data_assinatura_alteracao_vigencia)
        );
        
        $this->request->set(
            'data_assinatura_alteracao_fim_vigencia',
            $this->convertDateGovToBd($this->data_assinatura_alteracao_fim_vigencia)
        );
        if ($this->novo_valor_desconto !== null) {
            $novoValorDesconto = array_filter($this->novo_valor_desconto);
            $this->request->set('novo_valor_desconto', $novoValorDesconto);
            if (empty($novoValorDesconto)) {
                $this->request->remove('novo_valor_desconto');
            }
        }

        $alteracaoVigencia = false;
        $alteracaoFornecedor = false;
        $alteracaoValorRegistrado = false;
        $alteracaoCancelamentoItem = false;
        $alteracaoInformativo = false;
        foreach ($this->tipo_id as $tipoAlteracao) {
            $tipo = CodigoItem::find($tipoAlteracao);

            switch ($tipo->descricao) {
                case 'Fornecedor':
                    $alteracaoFornecedor = true;
                    break;
                case 'Vigência':
                    $alteracaoVigencia = true;
                    break;
                case 'Valor(es) registrado(s)':
                    $alteracaoValorRegistrado = true;
                    break;
                case 'Cancelamento de item(ns)':
                    $alteracaoCancelamentoItem = true;
                    break;
                case 'Informativo':
                    $alteracaoInformativo = true;
                    break;
            }
        }

        $this->request->set('alteracao_vigencia', $alteracaoVigencia);
        $this->request->set('alteracao_fornecedor', $alteracaoFornecedor);
        $this->request->set('alteracao_valor_registrado', $alteracaoValorRegistrado);
        $this->request->set('alteracao_cancelamento_item', $alteracaoCancelamentoItem);
        $this->request->set('alteracao_informativo', $alteracaoInformativo);

        $this->itemAlteracaoFornecedor();
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->request);
        return [
            'tipo_id' => [
                'array',
                'min:1',
                'required_if:rascunho,0',
            ],
            'data_assinatura_alteracao_vigencia' => [
                'required_if:rascunho,0',
                new ValidarDataAssinaturaAlteracaoAta(
                    $this->arp_id,
                    $this->data_assinatura_alteracao_fim_vigencia,
                    $this->data_assinatura_alteracao_vigencia
                )
            ],
            'descricao_anexo' => 'required_if:rascunho,0',
            'arquivo_alteracao' => 'nullable|file|mimes:ppt,xls,pdf|max:30720',
            'alteracao_vigencia' => new ValidarAlteracaoAtaVigencia(
                $this->arp_id,
                $this->data_assinatura_alteracao_fim_vigencia,
                $this->data_assinatura_alteracao_vigencia
            )
            ,
            'alteracao_cancelamento_item' => new ValidarCancelamentoItemAlteracaoAta(
                $this->arp_id,
                $this->item_cancelado,
                $this->rascunho,
                $this->justificativa_motivo_cancelamento
            )
            ,
            'alteracao_informativo' => new ValidarInformativoAlteracaoAta(
                $this->arp_id,
                $this->objeto_alteracao,
                $this->rascunho
            ),
            'alteracao_fornecedor' => new ValidarFornecedorAlteracaoAta(
                $this->novo_cnpj_fornecedor,
                $this->novo_nome_cnpj_fornecedor,
            )
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'tipo_id' => 'Tipo de alteração',
            'data_assinatura_alteracao_vigencia' => 'Data de assinatura da alteração',
            'descricao_anexo' => 'Descrição do anexo',
            'arquivo_alteracao' => 'Arquivo de alteração',

        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'tipo_id.required_if' => 'A :attribute é obrigatório!',
            'data_assinatura_alteracao_vigencia.required_if' => 'A :attribute é obrigatório!',
            'descricao_anexo.required_if' => 'A :attribute é obrigatório!',
            'arquivo_alteracao.file' => 'O :attribute deve ser um arquivo.',
            'arquivo_alteracao.mimes' => 'O :attribute deve ser um arquivo do tipo: :values.',
            'arquivo_alteracao.max' => 'O :attribute não pode ser superior a 30MB',
        ];
    }
}
