<?php

namespace App\Http\Requests;

use App\Models\AutorizacaoExecucaoEntrega;
use App\Models\ContratoLocalExecucao;
use App\Repositories\Contrato\ContratoParametroRepository;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Traits\Formatador;
use Illuminate\Validation\Rule;

class EntregaRequest extends FormRequest
{
    use Formatador;

    private $validaArquivoTRP = true;
    private $validaArquivoTRD = true;

    protected function prepareForValidation()
    {
        $this->request->set('data_entrega', $this->convertDateGovToBd($this->data_entrega));
        $this->request->set('data_inicio', $this->convertDateGovToBd($this->data_inicio));
        $this->request->set('data_fim', $this->convertDateGovToBd($this->data_fim));
        $this->request->set('data_prevista_trp', $this->convertDateGovToBd($this->data_prevista_trp));
        $this->request->set('data_prevista_trd', $this->convertDateGovToBd($this->data_prevista_trd));
        $this->request->set('valor_total', round($this->valor_total, 2));

        if (empty($this->vinculos)) {
            $this->request->set('vinculos', []);
        } else {
            $vinculosFormatado = $this->vinculos;

            foreach ($this->vinculos as $key => $vinculo) {
                if (!empty($vinculosFormatado[$key]['itens'])) {
                    $vinculosFormatado[$key]['itens'] = array_map(function ($item) {
                        $item['quantidade_informada'] = $item['quantidade_informada'] ?
                            $this->retornaFormatoAmericano($item['quantidade_informada']) : 0;
                        $item['valor_glosa'] = $item['valor_glosa'] ?
                            round($this->retornaFormatoAmericano($item['valor_glosa']), 2) : 0;
                        return $item;
                    }, $vinculosFormatado[$key]['itens']);
                }
            }

            $this->request->set('vinculos', $vinculosFormatado);
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'rascunho' => 'required|boolean',
            'vinculos' => 'required|nullable|array',
            'vinculos.*.id' => 'required_if:rascunho,0|nullable|integer',
            'vinculos.*.itens' => 'required_if:rascunho,0|nullable|array',
            'vinculos.*.itens.*.id' => 'required_if:rascunho,0|nullable|integer|min:1',
            'vinculos.*.itens.*.quantidade_informada' => 'required_if:rascunho,0|nullable|numeric|gt:0',
            'vinculos.*.itens.*.valor_glosa' => 'nullable|numeric',
            'valor_total' => 'required_if:rascunho,0|nullable|numeric|min:0',

            'locais_execucao' => 'nullable|array',
            'locais_execucao.*' => 'nullable|integer',
            'mes_ano_referencia' => 'required_if:rascunho,0|nullable',
            'data_inicio' => 'required_if:rascunho,0|nullable|date',
            'data_fim' => 'required_if:rascunho,0|nullable|date|after_or_equal:data_inicio',
            'data_entrega' => 'required_if:rascunho,0|nullable|date',
            'data_prevista_trp' => [
                Rule::requiredIf(function () {
                    return !$this->rascunho && !$this->originado_sistema_externo;
                }),
                'nullable',
                'date',
                'after_or_equal:data_entrega'
            ],
            'data_prevista_trd' => [
                Rule::requiredIf(function () {
                    return !$this->rascunho && !$this->originado_sistema_externo;
                }),
                'nullable',
                'date',
                'after_or_equal:data_prevista_trp'
            ],
            'anexos' => 'nullable|array',
            'anexos.*.nome_arquivo_anexo' => 'required_with:anexos|string',
            'anexos.*.url_arquivo_anexo' => 'required_with:anexos|string',
            'anexos.*.remove_file' => 'required_with:anexos|boolean',

            'informacoes_complementares' => 'nullable|string',
            'originado_sistema_externo' => 'required|boolean',
        ];
        return $rules;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'informacoes_complementares' => 'Informações Complementares',
            'vinculos' => 'Ordem de Serviço / Fornecimento',
            'vinculos.*.itens' => 'Item',
            'vinculos.*.itens.*.id' => 'Item',
            'vinculos.*.itens.*.quantidade_informada' => 'Quantidade Informada na Entrega',
            'vinculos.*.itens.*.valor_glosa' => 'Glosa',
            'valor_total' => 'Valor Total',
            'locais_execucao' => 'Local de Execução da Entrega',
            'mes_ano_referencia' => 'Mês/Ano de Referência',
            'data_inicio' => 'Período ínicio da entrega',
            'data_fim' => 'Período fim da entrega',
            'data_entrega' => 'Data efetiva da entrega',
            'data_prevista_trp' => 'Data prevista para o recebimento provisório',
            'data_prevista_trd' => 'Data prevista para o recebimento definitivo',

            'informar_trp' => 'Informar Termo de Recebimento Provisório',
            'arquivo_trp' => 'Arquivo Assinado do Termo de Recebimento Provisório',
            'informar_trd' => 'Informar Termo de Recebimento Definitivo',
            'arquivo_trd' => 'Arquivo Assinado do Termo de Recebimento Definitivo',
            'incluir_instrumento_cobranca' => 'Incluir Instrumento de Cobrança?',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'vinculos.required' => 'Adicione ao menos uma :attribute.',
            'vinculos.*.itens.required_if' => 'Adicione ao menos um :attribute de cada OS/F',
            'vinculos.*.itens.*.required_if' => 'Adicione ao menos um :attribute de cada OS/F',
            'vinculos.*.itens.*.id.required_if'=> 'Selecione ao menos um :attribute.',
            'vinculos.*.itens.*.quantidade_informada.required_if' => 'O campo :attribute é obrigatório.',
            'vinculos.*.itens.*.valor_glosa.required_if' => 'O campo :attribute é obrigatório.',
            'valor_total.required_if' => 'O campo :attribute é obrigatório.',
            'locais_execucao.required_if' => 'O campo :attribute é obrigatório.',
            'mes_ano_referencia.required_if' => 'O campo :attribute é obrigatório.',
            'data_inicio.required_if' => 'O campo :attribute é obrigatório.',
            'data_fim.required_if' => 'O campo :attribute é obrigatório.',
            'data_entrega.required_if' => 'O campo :attribute é obrigatório.',
            'data_prevista_trp.required_if' => 'O campo :attribute é obrigatório.',
            'data_prevista_trd.required_if' => 'O campo :attribute é obrigatório.',
            'informacoes_complementares.required_if' => 'O campo :attribute é obrigatório.',
            'itens.*.quantidade_solicitada.required_if' => 'O campo :attribute é obrigatório.',
            'itens.*.quantidade_solicitada.min' => 'Quantidade informada e em análise não pode ser maior que a 
                quantidade solicitada na OS/F.',
            'itens.*.numeric' => 'O campo :attribute deve ser um valor numérico.',
            'itens.*.valor_total_entrega.min' => 'O campo :attribute não pode ser negativo.',

            'informar_trp.required_if' => 'O campo :attribute e obrigatorio.',
            'arquivo_trp.required_if' => 'O campo :attribute e obrigatorio.',
            'informar_trd.required_if' => 'O campo :attribute e obrigatorio.',
            'arquivo_trd.required_if' => 'O campo :attribute e obrigatorio.',
            'incluir_instrumento_cobranca.required_if' => 'O campo :attribute e obrigatorio.',
        ];
    }
}
