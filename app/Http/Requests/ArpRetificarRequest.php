<?php

namespace App\Http\Requests;

use App\Http\Traits\Formatador;
use App\Models\Arp;
use App\Models\ArpHistorico;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Traits\Arp\ArpTrait;

class ArpRetificarRequest extends FormRequest
{
    use Formatador;
    use ArpTrait;

    /**
     * Método responsável em formatar as informações para ser usado na validação
     */
    public function prepareForValidation()
    {
        # Altera a formatação da data no padrão GOV para o padrão banco de dados
        $this->request->set('vigencia_inicial_alteracao', $this->convertDateGovToBd($this->vigencia_inicial_alteracao));
        $this->request->set('vigencia_final_alteracao', $this->convertDateGovToBd($this->vigencia_final_alteracao));
        $this->request->set('data_assinatura_alteracao', $this->convertDateGovToBd($this->data_assinatura_alteracao));

        $compraCentralizadaForm = $this->compra_centralizada;
        if ($this->compra_centralizada == "false") {
            $compraCentralizadaForm = "0";
        }

        $this->request->set('compra_centralizada', $compraCentralizadaForm);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    # Responsável por recuperar os valores do form e banco de dados
    private function recuperarCampo(string $campoRequest, Arp $arpAtual, string $campoBanco)
    {
        if (empty($this->$campoRequest)) {
            return $arpAtual->$campoBanco;
        }

        return $this->$campoRequest;
    }

    # Validar a data de assinatura da retificação
    private function validacaoDataAssinatura(Arp $arpAtual, ArpRetificarRequest $arpRequest)
    {
        $validacao = 'different:data_assinatura|';
        $dataInicialValidacao = '';
        $dataFinalValidacao = '';

        # Se existir valor para alteração da data de assinatura
        if (!empty($arpRequest->data_assinatura_alteracao)) {
            $dataInicialValidacao = $this->recuperarCampo('vigencia_inicial_alteracao', $arpAtual, 'vigencia_inicial');
            $dataFinalValidacao = $this->recuperarCampo('vigencia_final_alteracao', $arpAtual, 'vigencia_final');
        }

        # Se não for alterado nenhum valor, retorna a validação inicial
        if (empty($dataInicialValidacao) && empty($dataFinalValidacao)) {
            return $validacao;
        }

        return $validacao . 'before:' . $dataInicialValidacao . '|before:' . $dataFinalValidacao . '';
    }

    # Validar a data de vigencial iniclal da retificação
    private function validacaoVigenciaInicial(Arp $arpAtual, ArpRetificarRequest $arpRequest)
    {
        $validacao = 'different:vigencia_inicial|';

        # Se a vigência inicial for alterada
        if (!empty($arpRequest->vigencia_inicial_alteracao)) {
            $dataAssinatura = $this->recuperarCampo('data_assinatura_alteracao', $arpAtual, 'data_assinatura');
            $dataFinalValidacao = $this->recuperarCampo('vigencia_final_alteracao', $arpAtual, 'vigencia_final');
        }

        # Se não for alterado nenhum valor, retorna a validação inicial
        if (empty($dataAssinatura) && empty($dataFinalValidacao)) {
            return $validacao;
        }
        
        return $validacao . 'after:' . $dataAssinatura . '|before:' . $dataFinalValidacao . '';
    }

    # Validar a data de vigencial final da retificação
    private function validacaoVigenciaFinal(Arp $arpAtual, ArpRetificarRequest $arpRequest)
    {
        $validacao = 'different:vigencia_final|';
        $dataAssinatura = '';
        $dataInicialValidacao = '';

        # Se a vigência final for alterada
        if (!empty($arpRequest->vigencia_final_alteracao)) {
            $dataAssinatura = $this->recuperarCampo('data_assinatura_alteracao', $arpAtual, 'data_assinatura');
            $dataInicialValidacao = $this->recuperarCampo('vigencia_inicial_alteracao', $arpAtual, 'vigencia_inicial');
        }

        # Se não for alterado nenhum valor, retorna a validação inicial
        if (empty($dataAssinatura) && empty($dataInicialValidacao)) {
            return;
        }

        return $validacao . 'after:' . $dataAssinatura . '|after:' . $dataInicialValidacao . '';
    }

    private function validacaoNumero(Arp $arpAtual, ArpRetificarRequest $arpRequest)
    {

        $validacao = [];

        if ($arpAtual->numero == $arpRequest->numero_alteracao) {
            return 'different:numero_ata|';
        }

        if ($this->ataJaExiste($arpRequest->numero_alteracao, $arpAtual['ano'], $arpAtual['unidade_origem_id'])) {
            return 'boolean:true';
        }

        return $validacao;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validacoes = [];

        if ($this->rascunho) {
            return $validacoes;
        }

        $arpAtual = Arp::find($this->arp_id);

        $validacoes['numero_alteracao'] =  $this->validacaoNumero($arpAtual, $this) ?? '';
        $validacoes['objeto_alteracao'] = 'different:objeto';
        $validacoes['compra_centralizada_alteracao'] = 'different:compra_centralizada';
        $validacoes['justificativa_motivo_cancelamento'] = 'required';

        $validacoes['data_assinatura_alteracao'] = $this->validacaoDataAssinatura($arpAtual, $this) ?? '';
        $validacoes['vigencia_inicial_alteracao'] = $this->validacaoVigenciaInicial($arpAtual, $this) ?? '';
        $validacoes['vigencia_final_alteracao'] = $this->validacaoVigenciaFinal($arpAtual, $this) ?? '';
        
        $validacoes['objeto_alteracao'] = 'nullable';
        $validacoes['data_assinatura_alteracao'] = 'nullable';
        $validacoes['vigencia_inicial_alteracao'] = 'nullable';
        $validacoes['vigencia_final_alteracao'] = 'nullable';
        $validacoes['compra_centralizada_alteracao'] = 'nullable';
        $validacoes['numero_alteracao'] = 'nullable';
        $validacoes['autoridadesignataria_ata'] = 'nullable|array|empty_array';
        $validacoes['itens_remover'] = 'nullable';
        
        
        $anoMaximo = Carbon::now()->format('Y');
        $anoMinimo = config('arp.arp.ano_minimo_arp');

       // $validacoes['ano'] = "different:ano_ata|integer|min:{$anoMimino}|max:$anoMaximo|integer";

        // Verificação para garantir que pelo menos um dos dois valores é diferente
        $validacoes['ano'] = [
            $this->numero_alteracao ? 'required' : 'nullable',  // Se numero_alteracao existir, 'ano' é obrigatório
            'integer',
            'min:' . $anoMinimo,  // Ano mínimo configurado
            'max:' . $anoMaximo,  // Ano máximo (ano atual)
            function ($attribute, $value, $fail) {
                // Verifica se o ano da retificação é igual ao ano da ata original
                //e o número de alteração é igual ao número da ata
                if ($value == $this->ano_ata && $this->numero_alteracao == $this->numero_ata) {
                    $fail("A retificação deve ter o ano ou número diferentes da ata original!");
                }

                // Verifica se o número da ata é igual ao número de alteração e o ano não foi preenchido
                if ($this->numero_alteracao && !$value) {
                    $fail("O ano deve ser informado se o número de alteração for preenchido!");
                }
            }
        ];


        return $validacoes;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'numero_alteracao' => 'número',
            'vigencia_final_alteracao' => 'vigência final',
            'vigencia_inicial_alteracao' => 'vigência inicial',
            'objeto_alteracao' => 'objeto',
            'data_assinatura_alteracao' => 'data da assinatura',
            'justificativa_motivo_cancelamento' => 'justificativa/motivo da retificação',
            'compra_centralizada_alteracao' => 'compra centralizada',
            
            'objeto_alteracao'  => 'nullable',
            'data_assinatura_alteracao' => 'nullable',
            'vigencia_inicial_alteracao' => 'nullable',
            'vigencia_final_alteracao' => 'nullable',
            'compra_centralizada_alteracao' => 'nullable',
            'numero_alteracao' => 'nullable',
            'autoridadesignataria_ata' => 'nullable|array|empty_array',
            'itens_remover' => 'nullable',
            'ano' => 'ano da ata'
            
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'numero_alteracao.different' => 'A retificação do :attribute deve ser diferente do atual!',
            'numero_alteracao.boolean' => 'Já existe uma ata com o mesmo :attribute registrada para essa unidade!',
            'vigencia_inicial_alteracao.after' => "A :attribute deve ser posterior à data de assinatura!",
            'vigencia_inicial_alteracao.before' => "A :attribute deve ser anterior à data final de vigência!",
            'vigencia_inicial_alteracao.different' => 'A retificação da :attribute deve ser diferente da atual!',
            'vigencia_inicial_alteracao.before_or_equal' => "A :attribute deve ser uma data anterior a vigência final!",
            'vigencia_final_alteracao.after' => "A :attribute deve ser uma data posterior a vigência inicial!",
            'vigencia_final_alteracao.different' => 'A retificação da :attribute deve ser diferente da atual!',
            'objeto_alteracao.different' => 'A retificação do :attribute deve ser diferente da atual!',
            'data_assinatura_alteracao.before' =>
            'A retificação da :attribute deve ser anterior que a vigência inicial e final!',
            'data_assinatura_alteracao.different' => 'A retificação da :attribute deve ser diferente da atual!',
            'justificativa_motivo_cancelamento.required' => 'A :attribute é obrigatório!',
            'compra_centralizada_alteracao.different' => 'A retificação do :attribute deve ser diferente da atual!',
            
            'objeto_alteracao.nullable' => 'Preencha ao menos um item para retificar!',
            'data_assinatura_alteracao.nullable' => 'Preencha ao menos um item para retificar!',
            'vigencia_inicial_alteracao.nullable' => 'Preencha ao menos um item para retificar!',
            'vigencia_final_alteracao.nullable' => 'Preencha ao menos um item para retificar!',
            'compra_centralizada_alteracao.nullable' => 'Preencha ao menos um item para retificar!',
            'numero_alteracao.nullable' => 'Preencha ao menos um item para retificar!',
            'autoridadesignataria_ata.nullable|array|empty_array',
            'autoridadesignataria_ata.array' => 'Preencha ao menos um item para retificar!',
            'autoridadesignataria_ata.empty_array' => 'Preencha ao menos um item para retificar!',
            'itens_remover.nullable' => 'Preencha ao menos um item para retificar!',
            'ano.nullable' => 'Preencha ao menos um item para retificar!',
            'ano.different' => 'A retificação do :attribute deve ser diferente da atual!'
        ];
    }
}
