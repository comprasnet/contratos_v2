<?php

namespace App\Http\Requests;

use App\Rules\ValidarCompraGestaoAta;
use App\Rules\ValidarNumeroAta;
use App\Rules\ValidarNumeroProcessoUnidadeAta;
use Illuminate\Foundation\Http\FormRequest;

class ArpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'unidade_origem_id' => [
                'required',
                'exists:unidades,id'
            ],
            'modalidade_id' => [
                'required',
                'exists:codigoitens,id'
            ],
            'numero' => [
                'required',
                'numeric',
                new ValidarNumeroAta($this->ano, $this->unidade_origem_id, $this->rascunho)
            ],
            'ano' => [
                'required',
                'integer'
            ],
            'vigencia_final' => "required_if:rascunho,0",
            'vigencia_inicial' => "required_if:rascunho,0",
            'data_assinatura' => "required_if:rascunho,0",
            'arquivo' => "nullable|mimes:ppt,xls,pdf|max:30720",
            'id_item_fornecedor' => 'required|array|min:1',
            'id_item_fornecedor.*' => [
                'required',
                'integer',
                'exists:compra_item_fornecedor,id'
            ],
            'unidade_origem_compra_id' => [
                'required',
                'exists:unidades,id'
            ],
            'numero_ano' => [
                'required',
                new ValidarCompraGestaoAta(
                    $this->unidade_origem_id,
                    $this->modalidade_id,
                    $this->unidade_origem_compra_id
                )
            ],
            'numero_processo' => [
                'required_if:rascunho,0',
                new ValidarNumeroProcessoUnidadeAta($this->unidade_origem_id)
            ],
            'objeto' => "required_if:rascunho,0",
            'compra_centralizada' => [
                "required_if:rascunho,0",
                'boolean'
            ]
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'unidade_origem_compra_id' => 'unidade origem da compra',
            'data_assinatura' => 'data de assinatura',
            'vigencia_inicial' => 'data inicial da vigência',
            'vigencia_final' => 'data final da vigência',
            'compra_centralizada' => 'compra centralizada',
            'id_item_fornecedor' => 'item da ata',
            'ano' => 'ano da ata',
            'numero' => 'número da ata',
            'modalidade_id' => 'modalidade da licitação',
            'unidade_origem_id' => 'unidade de origem da ata',
            'arquivo' => 'arquivo da ata',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'unidade_origem_id.required' => 'O campo :attribute é obrigatório.',
            'modalidade_id.required' => 'O campo :attribute é obrigatório.',
            'numero.required' => 'O campo :attribute é obrigatório.',
            'numero.numeric' => 'O campo :attribute deve ser um número.',
            'ano.required' => 'O campo :attribute é obrigatório',
            'ano.integer' => 'O campo :attribute deve ser um inteiro',
            'id_item_fornecedor.required' => 'O :attribute é obrigatório.',
            'id_item_fornecedor.array' => 'Selecione ao menos um :attribute.',
            'id_item_fornecedor.min' => 'Selecione ao menos um :attribute.',
            'id_item_fornecedor.*.integer' => 'O :attribute deve ser um inteiro.',
            'id_item_fornecedor.*.required' => 'O :attribute é obrigatório.',
            'vigencia_final.required_if' => 'O campo :attribute é obrigatório',
            'vigencia_inicial.required_if' => 'O campo :attribute é obrigatório',
            'data_assinatura.required_if' => 'O campo :attribute é obrigatório',
            'arquivo.mimes' => 'O :attribute deve ser de uns desses tipos: ppt,xls,pdf.',
            'arquivo.max' => 'O :attribute deve ter o tamanho máximo de 30MB.',
            'unidade_origem_compra_id.required' => 'O campo :attribute é obrigatório',
            'compra_centralizada.required' => 'O campo :attribute é obrigatório',
            'compra_centralizada.boolean' => 'O campo :attribute deve ser boleano',
        ];
    }
}
