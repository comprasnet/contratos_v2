<?php

namespace App\Http\Requests;

use App\Rules\ValidarQuantidadeItemRemanejamentoCaronaSolicitacao;
use App\Rules\ValidarFornecedorAcordoNovoLocalRemanejamento;
use App\Rules\ValidarSolicitarItemRemanejamento;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;

class ArpRemanejamentoRequest extends FormRequest
{
    /**
     * Método responsável em formatar os dados da quantidade solicitada por
     * id da compra item unidade
     */
    private function formatarDadosQuantidadeSolicitada(?array $quantidadeSolicitada)
    {
        if (empty($quantidadeSolicitada)) {
            $quantidadeSolicitada = $this->quantidade_solicitada_fornecedor ?? [];
        }

        if (!empty($quantidadeSolicitada)) {
            $quantidadeSolicitada = $quantidadeSolicitada + ($this->quantidade_solicitada_fornecedor ?? []);
        }

        $arrayVazio = $this->todosValoresVazios($quantidadeSolicitada);

        # Se não for enviada nenhuma quantidade, então inclui com o valor nulo
        if (empty($quantidadeSolicitada) || $arrayVazio) {
            # Remove todos os campos que não foram preenchidos
            $this->request->set('quantidade_solicitada', null);
            return null;
        }

        # Filtra os campos enviados e retira os nulos
        $campoQuantidadeSolicitadaFiltrado = array_filter($quantidadeSolicitada);
        if (empty($campoQuantidadeSolicitadaFiltrado)) {
            $this->request->set('quantidade_solicitada', null);
            return $campoQuantidadeSolicitadaFiltrado;
        }

        $idAta = array();
        # Remove todos os ids do fornecedores que não foram utilizados
        foreach ($campoQuantidadeSolicitadaFiltrado as $compraItemUnidadeId => $quantidade) {
            $idAtaItemUnidade = $this->ata_solicitada_por_compra_item_unidade[$compraItemUnidadeId];
            $idAta [$idAtaItemUnidade][] = $compraItemUnidadeId;
        }

        # Remove todos os campos que não foram preenchidos
        $this->request->set('quantidade_solicitada', $campoQuantidadeSolicitadaFiltrado);

        return $idAta;
    }

    /**
     * Método responsável em formatar as informações para ser usado na validação
     */
    public function prepareForValidation()
    {
        # Formata os dados para o necessário na controller
        $id_ata = $this->formatarDadosQuantidadeSolicitada($this->quantidade_solicitada);

        # Insere somente os Ids que foram utilizados para o remanejamento
        $this->request->set('id_ata', $id_ata);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantidade_solicitada' => [
                'array',
                'present',
                'min:1'
            ],
            'quantidade_solicitada.*' => [
                'nullable',
                'numeric',
                new ValidarSolicitarItemRemanejamento()
            ],
            'remanejamento_entre_unidade_estado_df_municipio_distinto' => 'required_if:rascunho,0',
            'fornecedor_de_acordo_novo_local' =>[
                'required_if:remanejamento_entre_unidade_estado_df_municipio_distinto,1',
                new ValidarFornecedorAcordoNovoLocalRemanejamento(
                    $this->remanejamento_entre_unidade_estado_df_municipio_distinto
                )
            ],
            'ata_solicitada_por_compra_item_unidade' => [
                'array',
                'present',
                'min:1'
            ],
            'ata_solicitada_por_compra_item_unidade.*' => 'exists:arp,id',
            'item_principal_solicitado' => [
                'array',
                'present',
                'min:1'
            ],
            'item_principal_solicitado.*' => 'exists:compra_item_unidade,id',
            'id_ata' => [
                'array',
                'present',
                'min:1',
                'required',
                new ValidarQuantidadeItemRemanejamentoCaronaSolicitacao(
                    $this->item_principal_solicitado,
                    $this->quantidade_solicitada,
                    $this->unidade_solicitante
                )
            ],
            // 'arquivoRemanejamento' => "required_if:rascunho,0|min:{$this->qtd_ata_selecionada}",

            'arquivoRemanejamento.*' => 'nullable|file|mimes:ppt,xls,pdf|max:30720',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'quantidade_solicitada' => 'item para o remanejamento',
            'remanejamento_entre_unidade_estado_df_municipio_distinto' =>
                'o remanejamento está sendo feito entre unidades de estado, distrito federal ou município distintos',
            'fornecedor_de_acordo_novo_local' => 'o fornecedor beneficiário da
            ata de registro de preços está de acordo com o fornecimento no novo local de entrega',
            'arquivoRemanejamento' => 'arquivo(s) para o remanejamento'

        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'quantidade_solicitada.array' => 'Preencha ao menos um :attribute',
            'quantidade_solicitada.min' => 'Preencha ao menos um :attribute',
            'quantidade_solicitada.numeric' => 'O(s) valor(es) digitados no campo :attribute deve ser somente número',

            'remanejamento_entre_unidade_estado_df_municipio_distinto.required_if' =>
                'Seleciona uma opção para o campo :attribute',
            'fornecedor_de_acordo_novo_local.required_if' =>
                'Seleciona uma opção para o campo :attribute',

            'arquivoRemanejamento.required_if' => 'O :attribute é obrigatório',
            'arquivoRemanejamento.min' => 'A quantidade de anexo para o remanejamento deve ser igual ao número de atas',
            'arquivoRemanejamento.*.max' => 'O campo arquivo do remanejamento não pode ser superior a 30MB.',

        ];
    }

    public function todosValoresVazios(array $array): bool
    {
        return (new Collection($array))->flatten()->filter()->isEmpty();
    }
}
