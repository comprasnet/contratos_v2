<?php

namespace App\Http\Requests;

use App\Models\Contrato;
use App\Models\Contratoarquivo;
use App\Models\ContratoOpm;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;

class DeclaracaoOpmRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }


    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {

        $this->merge([
            'percentual_minimo' => filter_var($this->percentual_minimo, FILTER_VALIDATE_BOOLEAN),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $contratoId = Route::current()->parameter('contrato_id');
        $id = Route::current()->parameter('id') ?? 0;
        $dataInicioDecreto = Carbon::create(2023, 3, 30, 0, 0, 0);

        // Verifique se já existe um anexo associado ao ContratoOpm
        $contratoOpm = ContratoOpm::find($id);
        $anexoExiste = false;

        if ($contratoOpm) {
            $anexoExiste = Contratoarquivo::where('contrato_id', $contratoOpm->contrato_id)
                ->where('contrato_opm_id', $id)
                ->exists();
        }

        return [

            'quantidade_familiar' => 'required_if:rascunho,0|nullable|integer|min:0',
            'quantidade_contratadas' => 'required_if:rascunho,0|nullable|integer|min:0',
            'data_assinatura_opm' => [
                'required_if:rascunho,0',
                'nullable',
                function ($attribute, $value, $fail) use ($dataInicioDecreto) {
                    $dataAssinatura = Carbon::createFromFormat('d/m/Y', $value);

                    // Validar se a data é anterior à data de início do decreto
                    if ($dataAssinatura->lessThan($dataInicioDecreto)) {
                        $fail('Não é possível inserir uma data de assinatura anterior à data de início da ' .
                            'vigência do Decreto 11.430/2023.');
                    }

                    // Validar se a data é uma data futura
                    if ($dataAssinatura->isFuture()) {
                        $fail('Não é possível inserir uma data de assinatura com data futura.');
                    }
                },
            ],
            'percentual_minimo' => 'required_if:rascunho,0|boolean',
            'contrato_responsavel_id' => 'required_if:rascunho,0|nullable|exists:contratoresponsaveis,id',
            'sequencial' => [
                Rule::unique('contrato_opm', 'sequencial')->where(function ($query) use ($contratoId) {
                    return $query->where('contrato_id', $contratoId)->whereNull('deleted_at');
                })->ignore($id),
            ],
            'anexo_emitida_orgao' => [
                Rule::requiredIf(function () use ($anexoExiste) {
                    return !$this->rascunho && !$anexoExiste;
                }),
                'nullable',
                'file',
                'mimes:ppt,xls,pdf',
                'max:30720'
            ],
        ];
    }


    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'quantidade_familiar' => 'Quantidade de vagas para mulheres em situação de violência doméstica e familiar',
            'quantidade_contratadas' => 'Quantidade de mulheres em situação de violência doméstica' .
                                            ' e familiar contratadas',
            'percentual_minimo' => 'Percentual mínimo de cumprimento do Decreto nº 11.430/2023',
            'data_assinatura_opm' => 'Data de assinatura',
            'anexo_emitida_orgao' => 'Anexo da declaração emitida pelo órgão',

        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'quantidade_familiar.required' => 'O campo "Quantidade de vagas para mulheres em situação de violência' .
                                                ' doméstica e familiar" é obrigatório.',
            'quantidade_contratadas.required' => 'O campo "Quantidade de mulheres em situação de violência doméstica' .
                                                    ' e familiar contratadas" é obrigatório.',
            'percentual_minimo.required' => 'O campo "Percentual mínimo de cumprimento do' .
                                                ' Decreto nº 11.430/2023" é obrigatório.',
            'quantidade_familiar.integer' => 'O campo "Quantidade de vagas para mulheres em situação de violência' .
                                                ' doméstica e familiar" deve ser um número inteiro.',
            'quantidade_contratadas.integer' => 'O campo "Quantidade de mulheres em situação de violência doméstica' .
                                                    ' e familiar contratadas" deve ser um número inteiro.',
            'percentual_minimo.boolean' => 'O campo "Percentual mínimo de cumprimento do Decreto nº 11.430/2023"' .
                                            ' deve ser Sim ou Não.',
            'sequencial.required' => 'O número sequencial é obrigatório.',
            'sequencial.regex' => 'O número sequencial deve estar no formato 00000/0000.',
            'contrato_responsavel_id.required' => 'O campo "Responsável pelo contrato" é obrigatório',
            'data_assinatura_opm.required' => 'O campo "Data assinatura" é obrigatório',

            'anexo_emitida_orgao.required_if' => 'O campo "Anexo da declaração emitida pelo órgão" é
             obrigatório quando não for um rascunho.',
            'anexo_emitida_orgao.file' => 'O campo "Anexo da declaração emitida pelo órgão" deve ser um arquivo.',
            'anexo_emitida_orgao.mimes' => 'O campo "Anexo da declaração emitida pelo órgão" deve ser
             um arquivo dos tipos ppt,xls,pdf.',
            'anexo_emitida_orgao.max' => 'O campo "Anexo da declaração emitida pelo órgão" 
                              não pode ser maior que 30MB.',


        ];
    }
}
