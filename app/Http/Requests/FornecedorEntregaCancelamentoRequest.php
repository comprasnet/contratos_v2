<?php

namespace App\Http\Requests;

use App\Rules\ItemAdesaoExisteMinutaContratoRule;
use App\Rules\ItemCaronaRecebeuRemanejamentoRule;
use Illuminate\Foundation\Http\FormRequest;

class FornecedorEntregaCancelamentoRequest extends FormRequest
{
    protected function prepareForValidation()
    {
        $this->merge(['acao' => 'cancelar']);
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'entregaIdCancelamento' => [
                'required',
                'integer',
                'exists:autorizacaoexecucao_entrega,id'
            ],
            'justificativa_motivo_analise' => 'required|string|max:250',
            'acao' =>'required|string',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'entregaIdCancelamento' => 'id da entrega',
            'justificativa_motivo_analise' => 'justificativa do cancelamento'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'entregaIdCancelamento.required' => 'A :attribute é obrigatório!',
            'entregaIdCancelamento.integer' => 'O campo :attribute deve ser um número inteiro.',
            'justificativa_motivo_analise.required' => 'A :attribute é obrigatório!',
            'justificativa_motivo_analise.string' => 'O campo :attribute deve ser uma string.',
            'justificativa_motivo_analise.max' => 'O campo :attribute não pode ter mais de 250 caracteres.'
        ];
    }
}
