<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlterarFornecedorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cpf_cnpj_idgener' => 'required|min:5|max:255'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'cpf_cnpj_idgener' => 'Identificador do Fornecedor'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'cpf_cnpj_idgener.required' => 'O campo :attribute é obrigatório',
            'cpf_cnpj_idgener.min' => 'O campo :attribute deve ter no mínimo 5 caracteres',
            'cpf_cnpj_idgener.max' => 'O campo :attribute deve ter no máximo 255 caracteres'
        ];
    }
}
