<?php

namespace App\Http\Requests;

use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoValidationTrait;
use App\Models\AutorizacaoExecucao;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AutorizacaoExecucaoRequest extends FormRequest
{
    private $validaArquivo = true;

    use AutorizacaoexecucaoValidationTrait;

    protected function prepareForValidation()
    {
        $this->prepareFields();

        if ($this->enviar_assinatura) {
            $this->request->set('rascunho', 0);
        }

        if (empty($this->arquivo_clear) && $this->id) {
            $autorizacaoexecucao = AutorizacaoExecucao::findOrFail($this->id);
            if ($autorizacaoexecucao->arquivo) {
                $this->validaArquivo = false;
            }
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in

        if ($this->id) {
            $autorizacaoexecucao = AutorizacaoExecucao::findOrFail($this->id);

            return backpack_auth()->check() && (
                $autorizacaoexecucao->situacao->descres == 'ae_status_1' ||
                $autorizacaoexecucao->situacao->descres == 'ae_status_6'
            );
        }

        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = array_merge(
            $this->getDefaultRules(),
            [
                'anexos' => 'nullable|array',
                'anexos.*.nome_arquivo_anexo' => 'required_with:anexos|string',
                'anexos.*.url_arquivo_anexo' => 'required_with:anexos|string',
                'anexos.*.remove_file' => 'required_with:anexos|boolean',
                'originado_sistema_externo' => 'required|boolean',
                'modal_select_prepostos_responsaveis' => [
                    Rule::requiredIf(function () {
                        return $this->enviar_assinatura;
                    }),
                    'nullable',
                    'array'
                ],
            ]
        );

        if ($this->validaArquivo) {
            $rule['arquivo'] = [
                Rule::requiredIf(function () {
                    return !$this->enviar_assinatura && !$this->rascunho;
                }),
                'nullable',
                'file',
                'max:30720'
            ];
        }


        return $rule;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->getDefaultAttributes();
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            $this->getDefaultMessages(),
            ['arquivo.required' => 'O upload do :attribute da Ordem de Serviço / Fornecimento é obrigatório.']
        );
    }
}
