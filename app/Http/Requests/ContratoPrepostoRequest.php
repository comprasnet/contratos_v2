<?php

namespace App\Http\Requests;

use App\Http\Traits\Formatador;
use App\Rules\CPFRule;
use App\Rules\UppercaseRule;
use Illuminate\Foundation\Http\FormRequest;

class ContratoPrepostoRequest extends FormRequest
{
    use Formatador;

    protected function prepareForValidation()
    {
        $this->request->set('data_inicio', $this->convertDateGovToBd($this->data_inicio));
        $this->request->set('data_fim', $this->convertDateGovToBd($this->data_fim));
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contrato_id' => 'required|integer|size:' . $this->contrato_id,
            'cpf' => ['required', new CPFRule()],
            'nome' => ['required', 'string', new UppercaseRule()],
            'email' => 'required|email',
            'telefonefixo' => 'nullable|string',
            'celular' => 'nullable|string',
            'doc_formalizacao' => 'nullable|string',
            'informacao_complementar' => 'nullable|string',
            'data_inicio' => 'required|date',
            'data_fim' => 'nullable|date|after_or_equal:data_inicio',
            'situacao' => 'required|boolean',
        ];
    }

    public function attributes()
    {
        return [
            'cpf' => 'CPF',
            'nome' => 'Nome',
            'email' => 'E-mail',
            'telefonefixo' => 'Telefone Fixo',
            'celular' => 'Celular',
            'doc_formalizacao' => 'Doc. Formalização',
            'informacao_complementar' => 'Informação Complementar',
            'data_inicio' => 'Data Início',
            'data_fim' => 'Data Fim',
            'situacao' => 'Situação',
        ];
    }


    public function messages()
    {
        return [
            'cpf.required' => 'O campo :attribute é obrigatório.',
            'nome.required' => 'O campo :attribute é obrigatório.',
            'email.required' => 'O campo :attribute é obrigatório.',
            'email.email' => 'O campo :attribute deve ser um e-mail.',
            'data_inicio.required' => 'O campo :attribute é obrigatório.',
            'situacao.required' => 'O campo :attribute é obrigatório.',
        ];
    }
}
