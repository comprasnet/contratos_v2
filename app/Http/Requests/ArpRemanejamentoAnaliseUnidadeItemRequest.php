<?php

namespace App\Http\Requests;

use App\Rules\ValidarQuantidadeAnalisadaRemanejamentoUnidadeGerenciadoraAta;
use App\Rules\ValidarQuantidadeItemRemanejamentoCarona;
use Illuminate\Foundation\Http\FormRequest;

class ArpRemanejamentoAnaliseUnidadeItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantidade_analisada' => [
                'array',
                'present',
                'min:1',
                new ValidarQuantidadeAnalisadaRemanejamentoUnidadeGerenciadoraAta($this->rascunho, $this->status_id)
            ],
            'quantidade_analisada.*' => [
                'nullable',
                'numeric'
            ]
        ];
    }
    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'quantidade_analisada' => 'item para o remanejamento'
        
        ];
    }
    
    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'quantidade_analisada.array' => 'Preencha ao menos um :attribute',
            'quantidade_analisada.min' => 'Preencha ao menos um :attribute',
            'quantidade_analisada.numeric' => 'O(s) valor(es) digitados no campo :attribute deve ser somente número',
            'quantidade_analisada.present' => 'Preencha ao menos um :attribute'
        ];
    }
}
