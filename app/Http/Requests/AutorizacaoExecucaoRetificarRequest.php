<?php

namespace App\Http\Requests;

use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoValidationTrait;
use App\Models\AutorizacaoExecucao;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AutorizacaoExecucaoRetificarRequest extends FormRequest
{
    use AutorizacaoexecucaoValidationTrait;

    private $validaArquivo = true;

    protected function prepareForValidation()
    {
        $this->prepareFields();

        $this->merge(['autorizacaoexecucoes_id' => $this->autorizacaoexecucao_id, 'rascunho' => 0]);

        if (empty($this->arquivo_clear) && $this->autorizacaoexecucao_id && !$this->has('arquivo')) {
            $autorizacaoexecucao = AutorizacaoExecucao::findOrFail($this->autorizacaoexecucao_id);
            if ($autorizacaoexecucao->arquivo) {
                $this->validaArquivo = false;
            }
        }
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        $autorizacaoexecucao = AutorizacaoExecucao::findOrFail($this->autorizacaoexecucao_id);
        return backpack_auth()->check() &&
            (
                $autorizacaoexecucao->situacao->descres == 'ae_status_2' ||
                $autorizacaoexecucao->situacao->descres == 'ae_status_8'
            );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = array_merge(
            $this->getDefaultRules(),
            ['justificativa_motivo' => 'required']
        );

        if ($this->validaArquivo) {
            $rule['arquivo'] = [
                Rule::requiredIf(function () {
                    return !$this->enviar_assinatura && !$this->rascunho;
                }),
                'nullable',
                'file',
            ];
        }

        return $rule;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return array_merge(
            $this->getDefaultAttributes(),
            ['justificativa_motivo' => 'justificativa/motivo da retificação']
        );
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return array_merge(
            $this->getDefaultMessages(),
            ['arquivo.required' => 'O upload do :attribute da Ordem de Serviço / Fornecimento é obrigatório.']
        );
    }
}
