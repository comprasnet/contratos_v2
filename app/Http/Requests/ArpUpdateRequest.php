<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArpUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numero' => 'required',
            'ano' => 'required',
            'vigencia_final' => "required_if:rascunho,0",
            'vigencia_inicial' => "required_if:rascunho,0",
            'data_assinatura' => "required_if:rascunho,0"
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'numero.required' => 'O campo Número da Ata é obrigatório.',
            'ano.required' => 'O campo Ano é obrigatório',
            'vigencia_final.required_if' => 'O campo Data final da Vigência da Ata é obrigatório',
            'vigencia_inicial.required_if' => 'O campo Data inicial da Vigência da Ata é obrigatório',
            'data_assinatura.required_if' => 'O campo Data de Assinatura é obrigatório'
        ];
    }
}
