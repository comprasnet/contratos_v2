<?php

namespace App\Http\Requests;

use App\Models\AutorizacaoExecucaoEntrega;
use App\Models\ContratoLocalExecucao;
use App\Repositories\Contrato\ContratoParametroRepository;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Traits\Formatador;
use Illuminate\Validation\Rule;

class AutorizacaoExecucaoEntregaRequest extends FormRequest
{
    use Formatador;

    private $validaArquivoTRP = true;
    private $validaArquivoTRD = true;

    protected function prepareForValidation()
    {
        $this->request->set('data_entrega', $this->convertDateGovToBd($this->data_entrega));
        $this->request->set('data_prazo_trp', $this->convertDateGovToBd($this->data_prazo_trp));
        $this->request->set('data_prazo_trd', $this->convertDateGovToBd($this->data_prazo_trd));

        if (empty($this->itens)) {
            $this->request->set('itens', []);
        }

        $this->itens = array_map(function ($item) {
            $item['quantidade_informada'] = $item['quantidade_informada'] ?
                $this->retornaFormatoAmericano($item['quantidade_informada']) : 0;
            $item['valor_glosa'] = $item['valor_glosa'] ?
                $this->retornaFormatoAmericano($item['valor_glosa']) : null;
            $item['quantidade_solicitada'] -= $item['quantidade_informada'] + $item['quantidade_analise'];
            return $item;
        }, $this->itens);

        $this->request->set('itens', $this->itens);

        // garante que não seja feito nenhum upload de arquivo TRD caso o campo de informar TRD esteja desmarcado
        if (isset($this->informar_trd) && !$this->informar_trd) {
            $this->request->set('arquivo_trd_clear', true);
            $this->request->remove('arquivo_trd');
            $this->request->remove('incluir_instrumento_cobranca');
        }

        if (empty($this->arquivo_trp_clear) && empty($this->arquivo_trp) && $this->id) {
            $aeEntrega = AutorizacaoExecucaoEntrega::findOrFail($this->id);
            if ($aeEntrega->getArquivoTRP()) {
                $this->validaArquivoTRP = false;
            }
        }

        if (empty($this->arquivo_trd_clear) && empty($this->arquivo_trd) && $this->id) {
            $aeEntrega = AutorizacaoExecucaoEntrega::findOrFail($this->id);
            if ($aeEntrega->getArquivoTRD()) {
                $this->validaArquivoTRD = false;
            }
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
        //return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'rascunho' => 'required|boolean',
            'itens' => 'required_if:rascunho,0|nullable|array',
            'itens.*.itens_selecionados' => 'required_if:rascunho,0|nullable|integer|min:1',
            'itens.*.quantidade_informada' => 'required_if:rascunho,0|nullable|numeric|gt:0',
            'itens.*.quantidade_solicitada' => 'required_if:rascunho,0|nullable|numeric|min:0',
            'itens.*.valor_glosa' => 'nullable|numeric',
            'itens.*.valor_total_entrega' => 'required_if:rascunho,0|nullable|numeric|min:0',
            'informacoes_complementares' => Rule::requiredIf(function () {
                return !$this->rascunho && !$this->originado_sistema_externo;
            }),
            'locais_execucao_entrega' => 'nullable|array',
            'locais_execucao_entrega.*' => 'nullable|integer',
            'mes_ano_referencia' => 'required_if:rascunho,0|nullable',
            'data_entrega' => 'required_if:rascunho,0|nullable|date',
            'data_prazo_trp' => [
                Rule::requiredIf(function () {
                    return !$this->rascunho && !$this->originado_sistema_externo;
                }),
                'nullable',
                'date',
                'after_or_equal:data_entrega'
            ],
            'data_prazo_trd' => [
                Rule::requiredIf(function () {
                    return !$this->rascunho && !$this->originado_sistema_externo;
                }),
                'nullable',
                'date',
                'after_or_equal:data_prazo_trp'
            ],
            'anexos' => 'nullable|array',
            'anexos.*.nome_arquivo_anexo' => 'required_with:anexos|string',
            'anexos.*.url_arquivo_anexo' => 'required_with:anexos|string',
            'anexos.*.remove_file' => 'required_with:anexos|boolean',

            'originado_sistema_externo' => 'required|boolean',
            'informar_trp' => 'required_if:originado_sistema_externo,1|boolean',
            'arquivo_trp_clear' => 'nullable|integer',
            'informar_trd' => 'boolean',
            'arquivo_trd_clear' => 'nullable|integer',
            'incluir_instrumento_cobranca' => [
                Rule::requiredIf(function () {
                    return $this->informar_trd && !$this->rascunho;
                }),
                'boolean'
            ],
        ];

        if ($this->validaArquivoTRP) {
            $rules['arquivo_trp'] = [
                Rule::requiredIf(function () {
                    return $this->originado_sistema_externo && !$this->rascunho;
                }),
                'file',
                'mimes:pdf',
                'max:30720'
            ];
        }

        if ($this->validaArquivoTRD) {
            $rules['arquivo_trd'] = [
                Rule::requiredIf(function () {
                    return $this->informar_trd && !$this->rascunho;
                }),
                'nullable',
                'file',
                'mimes:pdf',
                'max:30720'
            ];
        }

        return $rules;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'informacoes_complementares' => 'Informações Complementares',
            'itens' => 'Item da Ordem de Serviço / Fornecimento',
            'itens.*.itens_selecionados' => 'Item',
            'itens.*.quantidade_informada' => 'Quantidade Informada na Entrega',
            'itens.*.quantidade_solicitada' => 'Quantidade Solicitada na Entrega',
            'itens.*.valor_glosa' => 'Glosa',
            'itens.*.valor_total_entrega' => 'Valor Total da Entrega',
            'locais_execucao_entrega' => 'Local de Execução da Entrega',
            'mes_ano_referencia' => 'Mês/Ano de Referência',
            'data_entrega' => 'Data efetiva da entrega',
            'data_prazo_trp' => 'Data prevista para o recebimento provisório',
            'data_prazo_trd' => 'Data prevista para o recebimento definitivo',

            'informar_trp' => 'Informar Termo de Recebimento Provisório',
            'arquivo_trp' => 'Arquivo Assinado do Termo de Recebimento Provisório',
            'informar_trd' => 'Informar Termo de Recebimento Definitivo',
            'arquivo_trd' => 'Arquivo Assinado do Termo de Recebimento Definitivo',
            'incluir_instrumento_cobranca' => 'Incluir Instrumento de Cobrança?',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'locais_execucao_entrega.required_if' => 'O campo :attribute é obrigatório.',
            'mes_ano_referencia.required_if' => 'O campo :attribute é obrigatório.',
            'data_entrega.required_if' => 'O campo :attribute é obrigatório.',
            'data_prazo_trp.required_if' => 'O campo :attribute é obrigatório.',
            'data_prazo_trd.required_if' => 'O campo :attribute é obrigatório.',
            'informacoes_complementares.required_if' => 'O campo :attribute é obrigatório.',
            'itens.required_if' => 'Adicione ao menos um :attribute.',
            'itens.*.itens_selecionados.required_if'=> 'Selecione ao menos um :attribute.',
            'itens.*.quantidade_informada.required_if' => 'O campo :attribute é obrigatório.',
            'itens.*.quantidade_solicitada.required_if' => 'O campo :attribute é obrigatório.',
            'itens.*.quantidade_solicitada.min' => 'Quantidade informada e em análise não pode ser maior que a 
                quantidade solicitada na OS/F.',
            'itens.*.valor_glosa.required_if' => 'O campo :attribute é obrigatório.',
            'itens.*.numeric' => 'O campo :attribute deve ser um valor numérico.',
            'itens.*.valor_total_entrega.min' => 'O campo :attribute não pode ser negativo.',

            'informar_trp.required_if' => 'O campo :attribute e obrigatorio.',
            'arquivo_trp.required_if' => 'O campo :attribute e obrigatorio.',
            'informar_trd.required_if' => 'O campo :attribute e obrigatorio.',
            'arquivo_trd.required_if' => 'O campo :attribute e obrigatorio.',
            'incluir_instrumento_cobranca.required_if' => 'O campo :attribute e obrigatorio.',
        ];
    }
}
