<?php

namespace App\Http\Traits\Arp;

use App\Models\ArpItem;
use App\Models\ArpItemHistorico;

trait ArpItemHistoricoTrait
{

    private function inserirItemArpHistorico(
        array $itensCadastrados,
        int $idArpHistorico,
        string $vigenciaInicial,
        string $vigenciaFinal
    ) {
        # Percorre todos os itens cadastrados para incluir na arp_item_historico
        foreach ($itensCadastrados as $itemHistorico) {
            $arpItemHistorico = new ArpItemHistorico();
            $arpItemHistorico->arp_historico_id =$idArpHistorico;
            $arpItemHistorico->arp_item_id = $itemHistorico;
            $arpItemHistorico->vigencia_inicial = $vigenciaInicial;
            $arpItemHistorico->vigencia_final = $vigenciaFinal;

            # Busca os itens a partir do histórico para
            # poder pegar as datas de vigência
            $arpItem = ArpItem::find($itemHistorico);

            # Recupera o valor unitário do item para salvar no histórico
            $arpItemHistorico->valor = $arpItem->item_fornecedor_compra->valor_unitario;

            $arpItemHistorico->save();
        }
    }
}
