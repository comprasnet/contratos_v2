<?php

namespace App\Http\Traits\Arp;

use App\Models\Arp;
use App\Models\ArpItem;
use App\Models\CompraItemUnidade;
use App\Http\Traits\CompraTrait;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

trait ArpQuantidadeAutorizadaAlteracaoTrait
{
    use CompraTrait;

    public function atualizarQuantidadeAutorizadaAlteracao($dadosHistoricoArp, $dadosFormulario)
    {
        $unidade_id = Arp::find($dadosHistoricoArp->arp_id);

        //dd($unidade_id);
        $itensArp = ArpItem::join('compra_item_fornecedor', function ($join) {
            $join->on(
                'arp_item.compra_item_fornecedor_id',
                '=',
                'compra_item_fornecedor.id'
            )
                ->whereNull('compra_item_fornecedor.deleted_at')
                ->where('compra_item_fornecedor.situacao', true);
        })
            ->join('compra_items', function ($join) {
                $join->on(
                    'compra_item_fornecedor.compra_item_id',
                    '=',
                    'compra_items.id'
                )
                    ->whereNull('compra_items.deleted_at')
                    ->where('compra_items.situacao', true);
            })
            ->join('compra_item_unidade', function ($join) {
                $join->on(
                    'compra_item_unidade.compra_item_id',
                    '=',
                    'compra_item_fornecedor.compra_item_id'
                )
                    ->whereNull('compra_item_unidade.deleted_at')
                    ->where('compra_item_unidade.situacao', true);
            })
            ->where('arp_item.arp_id', $dadosHistoricoArp->arp_id)
                ->select(
                    'arp_item.id',
                    'compra_item_unidade.id as compra_item_unidade_id',
                    'compra_item_fornecedor.compra_item_id as compra_item',
                    'compra_item_unidade.quantidade_autorizada',
                    'compra_item_unidade.quantidade_adquirir',
                    'compra_item_unidade.quantidade_adquirida',
                    'compra_item_unidade.quantidade_saldo'
                    //  'compra_item_unidade.quantidade_autorizada_original'
                )

                ->get();

        foreach ($itensArp as $item) {
            DB::beginTransaction();

            try {
                // Calcula o saldo consumido
                $saldo_consumido = $item->quantidade_autorizada - $item->quantidade_saldo;

                // Nova quantidade autorizada é a quantidade autorizada original +
                // o saldo consumido
              //  $nova_quantidade_autorizada = $item->quantidade_autorizada_original + $saldo_consumido;
                $nova_quantidade_autorizada = $item->quantidade_adquirir + $saldo_consumido;

                // Atualiza a quantidade autorizada na tabela CompraItemUnidade
                $compraItemUnidade = CompraItemUnidade::find($item->compra_item_unidade_id);


                $compraItemUnidade->quantidade_autorizada = $nova_quantidade_autorizada;
                $compraItemUnidade->save();

                // Instancia um novo objeto para evitar sobrescrita
               // $compraItemUnidade = CompraItemUnidade::find($item->compra_item_unidade_id);

                // Obter saldo atualizado usando a função retornaSaldoAtualizado
                $saldo_atualizado = $this->retornaSaldoAtualizado(
                    $item->compra_item,
                    $compraItemUnidade->unidade_id,
                    $compraItemUnidade->id
                );


                $compraItemUnidade->quantidade_saldo = $saldo_atualizado->saldo;

                $compraItemUnidade->save();
                DB::commit();
                Log::info($compraItemUnidade);
            } catch (\Exception $e) {
                // Tratamento de exceções
                DB::rollback();
                # Se acontecer erro, insere no arquivo de log gestao_ata
                $this->inserirLogCustomizado('gestao_ata', 'error', $e);
            }
        }
    }
}
