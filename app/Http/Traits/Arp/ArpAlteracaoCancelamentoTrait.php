<?php

namespace App\Http\Traits\Arp;

use App\Models\Arp;
use App\Models\ArpHistorico;
use App\Models\Codigo;

trait ArpAlteracaoCancelamentoTrait
{
    private function verificarCancelamentoArp(
        int $arpId,
        $totalItensAta
    ) {
        $itensCancelados = $this->queryBaseQuantidadeItemCanceladoPorAta($arpId);

        if ($itensCancelados === $totalItensAta) {
            $descricaoCancelada = Codigo::join(
                'codigoitens',
                'codigos.id',
                '=',
                'codigoitens.codigo_id'
            )
                ->where(
                    'codigos.descricao',
                    'Tipo de Ata de Registro de Preços'
                )
                ->where('codigoitens.descricao', 'Cancelada')
                ->first();

            if ($descricaoCancelada) {
                $arp = Arp::find($arpId);
                $arp->tipo_id = $descricaoCancelada->id;
                $arp->save();
            }
        }
    }

    public function queryBaseQuantidadeItemCanceladoPorAta(int $arpId)
    {
        return  Arp::join('arp_historico', 'arp.id', 'arp_historico.arp_id')
            ->join('arp_item_historico', 'arp_historico.id', 'arp_item_historico.arp_historico_id')
            ->where('arp.id', $arpId)
            ->where('arp_item_historico.item_cancelado', true)
            ->distinct('arp_item_historico.arp_item_id')
            ->count();
    }

    public function recuperarUltimoCancelamentoItens(int $arpId)
    {
        return ArpHistorico::join('arp_alteracao', 'arp_historico.arp_alteracao_id', 'arp_alteracao.id')
            ->join('codigoitens', 'arp_historico.tipo_id', 'codigoitens.id')
            ->where('arp_historico.arp_id', $arpId)
            ->where('arp_alteracao.rascunho', false)
            ->where('codigoitens.descricao', 'Cancelamento de item(ns)')
            ->orderBy('arp_alteracao.updated_at', 'desc')
            ->first();
    }
}
