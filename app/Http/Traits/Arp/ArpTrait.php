<?php

namespace App\Http\Traits\Arp;

use App\Http\Traits\UploadArquivoTrait;
use App\Models\Arp;
use App\Models\ArpAlteracao;
use App\Models\ArpArquivo;
use App\Models\ArpArquivoTipo;
use App\Models\ArpHistorico;
use App\Models\ArpItem;
use App\Models\ArpItemHistorico;
use App\Models\CompraItemFornecedor;
use App\Repositories\Compra\CompraItemFornecedorRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\BuscaCodigoItens;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait ArpTrait
{
    use UploadArquivoTrait;
    use BuscaCodigoItens;

    private $descricaoCodigo = 'Tipo de Ata de Registro de Preços';

    /**
     * Método responsável em formatar o sequencial
     */

    private function sequencialFormatado(int $sequencial, int $ano, bool $rascunho)
    {
        $numeroSequencial = str_pad($sequencial, 5, '0', STR_PAD_LEFT) . "/" . $ano;

        if ($rascunho) {
            return "$numeroSequencial-R";
        }

        return $numeroSequencial;
    }

    /**
     * Método responsável em gerar o sequencial da alteração
     */
    private function gerarSequencial(bool $rascunho, int $idAta)
    {
        $sequencial = ArpAlteracao::where("rascunho", $rascunho)
            ->where("arp_id", $idAta)
            ->max("sequencial");

        if (empty($sequencial)) {
            return 1;
        }

        return $sequencial + 1;
    }

    /**
     * Método responsável em gravar o arquivo no tabela arp_arquivos
     */
    private function salvarArquivoArp(
        Request $request,
        string  $tipoArquivo,
        string  $destination,
        string  $name
    ) {
        # Se não encontrar da alteração salvo, pega o ID
        $arpAlteracaoId = $request->arp_alteracao_id ?? $request->id;

        # Se nenhum arquivo for selecionado, atualiza a descrição do arquivo
        if (empty($request->$name)) {
            # Recupera as informações do arquivo para atualizar somente a descrição
            $arpArquivo = ArpArquivo::where("arp_alteracao_id", $arpAlteracaoId)->first();

            # Se não encontrar registro, porque o arquivo foi deletado da página
            if (empty($arpArquivo)) {
                return;
            }

            $arpArquivo->restrito = $request->get('rascunho') ?? true;
            $arpArquivo->descricao = $request->descricao_anexo ?? null;
            $arpArquivo->user_id = Auth::id();
            $arpArquivo->save();

            return;
        }

        # Se existir um arquivo, será substituído pelo enviado no form
        $this->substituirArquivoUpload(
            ArpArquivo::class,
            [
                "arp_id" => $request->arp_id,
                'arp_alteracao_id' => $arpAlteracaoId
            ]
        );

        #Salvar as informações no banco de dados
        $anexos = new ArpArquivo();
        $anexos->arp_id = $request->arp_id;
        $anexos->tipo_id = ArpArquivoTipo::where('nome', $tipoArquivo)->first()->id;
        $anexos->descricao = $request->descricao_anexo ?? null;
        $anexos->nome = $request->$name->getClientOriginalName();
        $anexos->restrito = $request->get('rascunho') ?? true;
        $anexos->url = $this->saveFile($destination, $request, $name);
        $anexos->arp_alteracao_id = $arpAlteracaoId;
        $anexos->user_id = Auth::id();
        $anexos->save();
    }

    /**
     * Método responsável em retornar o ID da situação
     */
    private function getSituacao(string $situacao)
    {
        return $this->retornaIdCodigoItem($this->descricaoCodigo, $situacao);
    }

    /**
     * Método responsável em retornar mais de uma situação
     */
    private function getSituacoes(array $situacoes)
    {
        return $this->retornaIdsCodigoItem($this->descricaoCodigo, $situacoes);
    }


    /**
     * Verifica se uma ata já existe baseado no número, ano e unidade gerenciadora
     *
     * @param string $numeroAta
     * @param string $anoAta
     * @param int $unidadeGerenciadora
     * @return bool
     */
    public function ataJaExiste($numeroAta, $anoAta, $unidadeGerenciadora)
    {
        $arp =
            Arp::where(
                [
                    "numero" => $numeroAta,
                    "ano" => $anoAta,
                    "unidade_origem_id" => $unidadeGerenciadora,
                    'rascunho' => false
                ]
            )->count();

        if ($arp > 0) {
            return true;
        }

        return false;
    }

    /**
     * Método evitar duplicação de código na busca pelo histórico de um item de compra
     * Através do id da compra_item_fornecedor, busca todo o histórico do item independente de que ata esteja
     * @param $idCompraItemFornecedor
     * @return mixed
     */
    private function buscarHistoricoItemDeCompra($idCompraItemFornecedor)
    {
        return CompraItemFornecedor::join(
            'arp_item',
            'arp_item.compra_item_fornecedor_id',
            '=',
            'compra_item_fornecedor.id'
        )
            ->join('arp_item_historico', 'arp_item_historico.arp_item_id', '=', 'arp_item.id')
            ->join('arp_historico', 'arp_historico.id', '=', 'arp_item_historico.arp_historico_id')
            ->join('codigoitens', 'arp_historico.tipo_id', '=', 'codigoitens.id')
            ->leftjoin('arp_alteracao', function ($query) {
                $query->on('arp_historico.arp_alteracao_id', 'arp_alteracao.id')
                    ->where('arp_alteracao.rascunho', false);
            })
            ->where('compra_item_fornecedor.id', $idCompraItemFornecedor)
            ->where('compra_item_fornecedor.situacao', true);
    }

    protected function itemValidoParaSelecionar(
        CompraItemFornecedor $compraItemFornecedor,
        array $arpItemid = [],
        ?int $idata = null
    ) {
        # Verifica se há histórico de ata para o item
        $historicoAta = ArpHistorico::leftjoin(
            'arp_item_historico',
            'arp_historico.id',
            'arp_item_historico.arp_historico_id'
        )
            ->leftjoin('codigoitens', 'codigoitens.id', 'arp_historico.tipo_id')
            ->join('arp_item', 'arp_item.id', 'arp_item_historico.arp_item_id')
            ->leftjoin('arp', 'arp.id', 'arp_item.arp_id')
            ->join('compra_item_fornecedor', function ($query) {
                $query->on('compra_item_fornecedor.id', 'arp_item.compra_item_fornecedor_id')
                    ->where("compra_item_fornecedor.situacao", true);
            })
            ->leftjoin('arp_alteracao', function ($query) {
                $query->on('arp_historico.arp_alteracao_id', 'arp_alteracao.id');
            })
            ->where("arp.id", "<>", $idata)
            ->where('arp_item.compra_item_fornecedor_id', $compraItemFornecedor->id)
            ->whereNotIn('arp_item.id', $arpItemid)
            ->selectRaw('distinct on (arp_item.compra_item_fornecedor_id)
                         arp_item.compra_item_fornecedor_id as numero_item,
                            codigoitens.descricao,
                            arp_historico.updated_at,
                            arp_alteracao.rascunho')
            ->orderBy('arp_item.compra_item_fornecedor_id')
            ->orderByDesc('arp_historico.updated_at')
            ->first();

        if (empty($historicoAta)) {
            return true;
        }
        
            $situacaoPodeSelecionarItem = ['Cancelamento de item(ns)', 'Retificar - Dados da ata e exclusão de item'];

        if (!$historicoAta->rascunho && in_array($historicoAta->descricao, $situacaoPodeSelecionarItem)) {
            return true;
        }

            return false;
    }


    /**
     * Utilizado para exibir cabeçalho com dados básicos de uma ata
     * Deve ser utilizado por controllers backpack que utilizam instância do CRUD
     *
     * @return void
     */
    private function areaDadosAta()
    {
        $arpAlteracao = Arp::find($this->returnIdArp());

        $this->crud->addField(
            [
                'name'      => 'numero_ata', // The db column name
                'label'     => 'Número/Ano da Ata', // Table column heading
                'type'      => 'label',
                'wrapperAttributes' => [
                    'class' => 'col-md-4 pt-3 mb-3'
                ],
                'value' => $arpAlteracao->getNumeroAno()
            ]
        );

        $this->crud->addField(
            [
                'name'      => 'numero_ano_compra', // The db column name
                'label'     => 'Número/Ano da Compra', // Table column heading
                'type'      => 'label',
                'wrapperAttributes' => [
                    'class' => 'col-md-4 pt-3 mb-3'
                ],
                'value' => $arpAlteracao->compras->numero_ano
            ]
        );

        $this->crud->addField(
            [
                'name'      => 'modalidade_compra', // The db column name
                'label'     => 'Modalidade da Compra', // Table column heading
                'type'      => 'label',
                'wrapperAttributes' => [
                    'class' => 'col-md-4 pt-3 mb-3'
                ],
                'value' => $arpAlteracao->compras->getModalidade()
            ]
        );

        $this->crud->addField(
            [
                'name'      => 'data_assinatura', // The db column name
                'label'     => 'Data da Assinatura da Ata', // Table column heading
                'type'      => 'label',
                'date'      => true,
                'wrapperAttributes' => [
                    'class' => 'col-md-4 pt-3 mb-3'
                ],
                'value' => $arpAlteracao->data_assinatura
            ]
        );

        $this->crud->addField(
            [
                'name'      => 'data_inicio_vigencia', // The db column name
                'label'     => 'Data da Vigência inicial da Ata', // Table column heading
                'type'      => 'label',
                'date'      => true,
                'wrapperAttributes' => [
                    'class' => 'col-md-4 pt-3 mb-3'
                ],
                'value' => $arpAlteracao->vigencia_inicial
            ]
        );

        $this->crud->addField(
            [
                'name'      => 'data_fim_vigencia', // The db column name
                'label'     => 'Data da Vigência final da Ata', // Table column heading
                'type'      => 'label',
                'date'      => true,
                'wrapperAttributes' => [
                    'class' => 'col-md-4 pt-3 mb-3'
                ],
                'value' => $arpAlteracao->vigencia_final
            ]
        );
    }

    public function queryBaseVerificarHistoricoAta()
    {
        return Arp::join('arp_historico', 'arp.id', 'arp_historico.arp_id')
            ->join('arp_item', 'arp_item.arp_id', 'arp.id')
            ->join('codigoitens', 'codigoitens.id', 'arp_historico.tipo_id')
            ->join('compra_item_fornecedor', function ($query) {
                $query->on('compra_item_fornecedor.id', 'arp_item.compra_item_fornecedor_id')
                    ->where("compra_item_fornecedor.situacao", true);
            });
    }

    public function queryVerificarHistoricoAtaPorCompraNumeroItem(int $compraId, array $numeroItem)
    {
        return $this->queryBaseVerificarHistoricoAta()
            ->join('compra_items', function ($query) {
                $query->on('compra_items.id', 'compra_item_fornecedor.compra_item_id')
                    ->where("compra_items.situacao", true);
            })
            ->join('arp_item_historico', function ($query) {
                $query->on('arp_item_historico.arp_item_id', 'arp_item.id')
                    ->whereColumn('arp_item_historico.arp_historico_id', 'arp_historico.id');
            })
            ->where('arp.rascunho', false)
            ->where('compra_items.compra_id', $compraId)
            ->whereIn('compra_items.numero', $numeroItem)
            ->selectRaw('distinct on (compra_item_fornecedor.id)
                         compra_items.numero as numero_item,
                            codigoitens.descricao,
                            arp_historico.updated_at')
            ->orderBy('compra_item_fornecedor.id')
            ->orderByDesc('arp_historico.updated_at')
            ->get()
            ->toArray();
    }
}
