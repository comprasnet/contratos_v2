<?php

namespace App\Http\Traits\Arp;

use App\Models\ArpAlteracao;

trait ArpAlteracaoTrait
{
    private function gerarSequencialArpAlteracao(bool $rascunho, int $idAta)
    {
        $sequencial = ArpAlteracao::where("rascunho", $rascunho)
                                    ->where("arp_id", $idAta)
                                    ->max("sequencial");
        
        if (empty($sequencial)) {
            return 1;
        }

        return $sequencial + 1;
    }

    private function montarDadosAlteracao(array $dados)
    {
        $dadosAlteracao = [];
        $dadosAlteracao['arp_id'] = $dados['arp_id'];
        $dadosAlteracao['rascunho'] = $dados['rascunho'];
        $dadosAlteracao['sequencial'] = $this->gerarSequencialArpAlteracao($dadosAlteracao['rascunho'], $dadosAlteracao['arp_id']);

        return $dadosAlteracao;
    }

    private function inserirArpAlteracao(array $dadosAlteracao)
    {
        return ArpAlteracao::create($dadosAlteracao)->id;
    }
}