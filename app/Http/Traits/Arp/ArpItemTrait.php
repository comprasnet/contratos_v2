<?php

namespace App\Http\Traits\Arp;

use App\Models\Arp;
use App\Models\ArpItem;
use App\Repositories\Arp\ArpItemRepository;
use FontLib\TrueType\Collection;

trait ArpItemTrait
{
    private function inserirVigenciaItem(Arp $ata)
    {
        foreach ($ata->item as $itemArp) {
            // Altera a vigência inicial do item
            $itemArp->item_fornecedor_compra->ata_vigencia_inicio = $ata->vigencia_inicial;
            // Altera a vigência final do item
            $itemArp->item_fornecedor_compra->ata_vigencia_fim = $ata->vigencia_final;
            // salvar os dados
            $itemArp->item_fornecedor_compra->save();
        }
    }

    /**
     * Monta uma mensagem com as minutas de empenho vinculadas ao item
     *
     * @param $minutasDeEmpenhoVinculadasAoItem
     * @return string $mensagem Mensagem montada
     */
    private function montarMensagemMinutasEmpenhoVinculadasAoItem($minutasDeEmpenhoVinculadasAoItem)
    {
        # Se o objeto não tiver registros, retorna string vazia
        if ($minutasDeEmpenhoVinculadasAoItem->isEmpty()) {
            return '';
        }

        $empenhosPorUnidade = [];
        # Percorre os empenhos e organiza para em um array para exibição
        foreach ($minutasDeEmpenhoVinculadasAoItem as $key => $minuta) {
            $unidade = "{$minuta->codigo} - {$minuta->nome}";
            $empenhosPorUnidade[$unidade][$key] = $minuta->mensagem_siafi;
        }

        # Mensagem de exibição para o usuário
        $mensagem = 'Existem minuta(s) de empenho do tipo compra vinculadas para esse item, sendo elas:<br>';
        foreach ($empenhosPorUnidade as $unidade => $empenhos) {
            # Inicia o contador de minutas em andamento
            $count = 0;
            # Insere a unidade na mensagem
            $mensagem .= "Unidade $unidade:<br> Empenho(s): ";
            foreach ($empenhos as $empenho) {
                if (empty($empenho)) {
                    $count++;
                    // $empenho = "EM ANDAMENTO";
                }
                $mensagem .= "$empenho, ";
            }

            # Remove a última vírgula
            $mensagem = substr($mensagem, 0, -2);

            # Se encontrar algum minuta de empenho, insere a quantidade
            # que está com o status em andamento
            if ($count > 0) {
                $count = $this->preencherCasasDireitaEsquerda($count);
                $mensagem .= " EM ANDAMENTO ({$count})";
            }

            # Pula para a próxima unidade
            $mensagem .= "<br>";
        }

        return $mensagem;
    }

    /**
     * Monta uma mensagem com contratos de compra vinculadas ao item
     *
     * @param $minutasDeEmpenhoVinculadasAoItem
     * @return string $mensagem Mensagem montada
     */
    private function montarMensagemContratosVinculadosAoItem($contratosVinculadosAoItem)
    {

        # Se o objeto não tiver registros, retorna string vazia
        if ($contratosVinculadosAoItem->isEmpty()) {
            return '';
        }

        # Percorre os contratos e organiza para em um array para exibição
        foreach ($contratosVinculadosAoItem as $key => $contrato) {
            $unidade = "{$contrato->codigo} - {$contrato->nomeresumido}";
            $contratoPorUnidade[$unidade][$key] = $contrato->numero;
        }

        # Mensagem de exibição para o usuário
        $mensagem = 'Existem contrato(s) vinculados para esse item, sendo eles:<br>';
        foreach ($contratoPorUnidade as $unidade => $contrato) {
            # Insere a unidade na mensagem
            $mensagem .= "Unidade $unidade:<br> Contrato(s): ";
            foreach ($contrato as $empenho) {
                $mensagem .= "$empenho, ";
            }

            # Remove a última vírgula
            $mensagem = substr($mensagem, 0, -2);

            # Pula para a próxima unidade
            $mensagem .= "<br>";
        }

        return $mensagem;
    }

    /**
     * Monta uma mensagem com as adesões do item
     *
     * @param $adesoesDoItem
     * @return string $mensagem Mensagem montada
     */
    private function montarMensagemAdesoesDoItem($adesoesDoItem)
    {
        # Se o objeto não tiver registros, retorna string vazia
        if ($adesoesDoItem->isEmpty()) {
            return '';
        }

        # Mensagem de exibição para o usuário
        $mensagem = 'Este item possui adesão(ões), sendo elas:<br>';
        foreach ($adesoesDoItem as $adesao) {
            # Insere a unidade na mensagem
            $mensagem .= "Unidade: $adesao->unidade<br>";
            $mensagem .= "Quantidade solicitada: $adesao->quantidade_solicitada<br>";
            $mensagem .= "Quantidade aprovada: $adesao->quantidade_aprovada<br><br>";
        }

        return $mensagem;
    }

    private function montarMensagemRemanejamentosDoItem($remanejamentosDoItem)
    {
        # Se o objeto não tiver registros, retorna string vazia
        if ($remanejamentosDoItem->isEmpty()) {
            return '';
        }

        # Mensagem de exibição para o usuário
        $mensagem = 'Este item possui remanejamento(s), sendo eles:<br>';
        foreach ($remanejamentosDoItem as $remanejamento) {
            # Insere a unidade na mensagem
            $mensagem .= "Unidade: $remanejamento->unidade<br>";
            $mensagem .= "Quantidade solicitada: $remanejamento->quantidade_solicitada<br>";
            $mensagem .= "Quantidade aprovada: $remanejamento->quantidade_aprovada<br><br>";
        }

        return $mensagem;
    }

    /**
     * Busca todos os itens de uma ata e, para cada item, seleciona os valores referentes à unidade desejada
     * Os valores retornados são voltados para a rotina de relatar execução, agrupados por item de compra
     *
     * @param int $arpId
     * @param int $unidadeId
     * @return array
     */
    public function getItensParaExecucaoPorUmaUnidade(int $arpId, int $unidadeId): array
    {
        $arpItemRepository = new ArpItemRepository();

        # Recupera os itens da ata por unidade
        $itensDaAta = $arpItemRepository->getItensDeUmaAtaAgrupadosPorItemDaCompraEPorUnidade($arpId, $unidadeId);

        $itensDaAtaParaUnidade = [];
        foreach ($itensDaAta as $item) {
            # Se o item ainda não foi colocado na lista
            if (!isset($itensDaAtaParaUnidade[$item->id])) {
                # Cria um novo item
                $itensDaAtaParaUnidade[$item->id] = clone $item;
                $itensDaAtaParaUnidade[$item->id]->quantidade_empenhada = 0;
            }

            $itensDaAtaParaUnidade[$item->id]->quantidade_empenhada = $item->quantidade_empenhada ?? 0;
            $itensDaAtaParaUnidade[$item->id]->unidade_participa = $item->unidade_participa;
            $itensDaAtaParaUnidade[$item->id]->nome_fornecedor = $item->nome_fornecedor;
        }

        return $itensDaAtaParaUnidade;
    }

    public function itemPertenceGrupo($grupoCompra)
    {
        // Verifica se o campo está vazio ou nulo
        if (empty($grupoCompra)) {
            return false;
        }

        // Verifica se o campo é igual a "00000" usando strcmp
        if (strcmp($grupoCompra, "00000") === 0) {
            return false;
        }

        return true;
    }

    /**
     * Calcula quantos itens tem por grupo utilizando objeto vindo do banco
     *
     * @param $itens
     * @return array
     */
    public function calculaQuantidadeDeItensPorGrupo($itens)
    {
        # Calcula quantos itens tem por grupo
        $quantidadeDeItensPorGrupo = [];
        foreach ($itens as $item) {
            if ($this->itemPertenceGrupo($item->grupo_compra)) {
                isset($quantidadeDeItensPorGrupo[$item->grupo_compra]) ?
                    $quantidadeDeItensPorGrupo[$item->grupo_compra]++ :
                    $quantidadeDeItensPorGrupo[$item->grupo_compra] = 1;
            }
        }

        return $quantidadeDeItensPorGrupo;
    }

    /**
     * Este método faz o mesmo que o anterior porém com um array
     * A ideia é unificar este métodos como melhoria
     * O método já recebe somente os grupos válidos
     *
     * @param $itens
     * @return array
     */
    public function calculaQuantidadeDeItensPorGrupoArray($itens)
    {
        # Calcula quantos itens tem por grupo
        $quantidadeDeItensPorGrupo = [];
        foreach ($itens as $compraItemId => $item) {
            if ($this->itemPertenceGrupo($item['grupo'])) {
                isset($quantidadeDeItensPorGrupo[$item['grupo']]) ?
                    $quantidadeDeItensPorGrupo[$item['grupo']]++ :
                    $quantidadeDeItensPorGrupo[$item['grupo']] = 1;
            }
        }

        return $quantidadeDeItensPorGrupo;
    }

    public function calculaQuantidadeDeItensPorGrupoParaUmaUnidade($itens)
    {

        $quantidadeDeItensPorGrupo = [];

        foreach ($itens as $item) {
            if ($this->itemPertenceGrupo($item->grupo_compra)) {
                if (!isset($quantidadeDeItensPorGrupo[$item->grupo_compra])) {
                    $quantidadeDeItensPorGrupo[$item->grupo_compra] = 0;
                }

                if ($item->unidade_participa) {
                    $quantidadeDeItensPorGrupo[$item->grupo_compra]++;
                }
            }
        }

        return $quantidadeDeItensPorGrupo;
    }

    public function calculaQuantidadeDeItensPorGrupoParaUmaUnidadeArray($itens)
    {

        foreach ($itens as $itemCompraId => $item) {
            if ($this->itemPertenceGrupo($item['grupo'])) {
                if (!isset($quantidadeDeItensPorGrupo[$item['grupo']])) {
                    $quantidadeDeItensPorGrupo[$item['grupo']] = 0;
                }

                if ($item['unidade_participa']) {
                    $quantidadeDeItensPorGrupo[$item['grupo']]++;
                }
            }
        }

        return $quantidadeDeItensPorGrupo;
    }

    public function getItemDaCompraPeloItemDaAta($arpItemId, $arpId, $unidadeId)
    {
        return ArpItem::join(
            'compra_item_fornecedor',
            'arp_item.compra_item_fornecedor_id',
            '=',
            'compra_item_fornecedor.id'
        )
            ->join(
                'compra_item_unidade',
                'compra_item_unidade.compra_item_id',
                '=',
                'compra_item_fornecedor.compra_item_id'
            )
            ->where('arp_item.id', $arpItemId)
            ->where('compra_item_unidade.unidade_id', $unidadeId)
            ->where('arp_item.arp_id', $arpId)
            ->whereNull('arp_item.deleted_at')
            ->where('compra_item_fornecedor.situacao', true)
            ->pluck('compra_item_fornecedor.compra_item_id')
            ->first();
    }
}
