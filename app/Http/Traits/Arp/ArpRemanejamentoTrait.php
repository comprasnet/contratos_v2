<?php

namespace App\Http\Traits\Arp;

use App\Http\Traits\Formatador;
use App\Models\Arp;
use App\Models\ArpRemanejamento;
use App\Models\ArpRemanejamentoItem;
use App\Models\BackpackUser;
use App\Models\CodigoItem;
use App\Models\CompraItemUnidade;
use App\Models\Compras;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

trait ArpRemanejamentoTrait
{
    use Formatador;

    /**
     * Método responsável em exibir as informações do anexo e itens solicitado no remanejamento
     */
    private function exibirAnexoItemRemanejamento(CrudPanel $crud)
    {
        # Recupera as informações sobre o remanejamento
        $remanejamento = $crud->getEntry($crud->getCurrentEntryId());

        $arraySaida = [];
        # Se tiver um arquivo, exibe para o usuário
        if (!empty($remanejamento->arpRemanejamentoArquivo)) {
            $arraySaida['Nome'][0] = $remanejamento->arpRemanejamentoArquivo->nome;
            $arraySaida['Visualizar'][0] =
                '<a
                target="_blank"
                href="' . url("storage/{$remanejamento->arpRemanejamentoArquivo->url}") . '">
                <i class="fas fa-eye"></i>
            </a>';
        }

        $this->addColumnTable('anexos', 'Anexo(s)', $arraySaida);

        $itens = array();
        # Se tiver um item, exibe para o usuário
        if (!empty($remanejamento->itemRemanejamento)) {
            foreach ($remanejamento->itemRemanejamento as $key => $item) {
                $itens['Número'][$key] = $item->itemUnidade->compraItens->numero;
                $itens['Descrição'][$key] = $item->itemUnidade->compraItens->descricaodetalhada;
                $itens['Unidade_do_item'][$key] = $item->itemUnidade->unidades->exibicaoCompletaUASG();
                $itens['Situação'][$key] = $item->situacaoItem->descricao ?? null;
                $itens['Quantidade_solicitada'][$key] = $item->quantidade_solicitada;

                $itens['Quantidade_aprovada_participante'][$key] = '';
                
                # Se existir uma análise pela unidade participante
                if (!empty($item->id_usuario_aprovacao_participante)) {
                    $itens['Quantidade_aprovada_participante'][$key] = $item->quantidade_aprovada_participante;
                }

                $itens['Quantidade_aprovada_gerenciadora'][$key] = '';
                
                # Se existir uma análise pela unidade gerenciadora da ata
                if (!empty($item->id_usuario_aprovacao_gerenciadora)) {
                    $itens['Quantidade_aprovada_gerenciadora'][$key] = $item->quantidade_aprovada_gerenciadora;
                }
            }
        }

        $this->addColumnTable('itens', 'item(ns)', $itens);
    }

    /**
     * Método responsável em realizar os JOINS necessário para a exibição
     */
    private function joinRemanejamentoSetup(
        ArpRemanejamento $remanejamento,
        string $situacaoItem,
        int $idUnidadeUsuarioLogado,
        bool $unidadeGerenciadoraItem
    ) {
        # JOIN para Recuperar os itens que foram solicitados
        $remanejamentoJoin = $remanejamento->join(
            'arp_remanejamento_itens',
            'arp_remanejamento.id',
            'arp_remanejamento_itens.remanejamento_id'
        );

        # JOIN para vincular as unidades que pertencem aos itens e filtrar pela unidade do usuário logado
        # para exibir somente as que ele pode analisar
        $remanejamentoJoin->join(
            'compra_item_unidade',
            'compra_item_unidade.id',
            'arp_remanejamento_itens.id_item_unidade'
        );

        if (!$unidadeGerenciadoraItem) {
            $remanejamentoJoin->where('compra_item_unidade.unidade_id', $idUnidadeUsuarioLogado);
            
            # JOIN para recuperar os remanejamentos com base na situação dos itens no remanejamento
//            $remanejamentoJoin->join('codigoitens', function ($query) use ($situacaoItem) {
//                $query->on('codigoitens.id', 'arp_remanejamento.situacao_id')
//                    ->where('codigoitens.descricao','<>' ,$situacaoItem);
//            });
        }

        # JOIN para vincular a unidade ao item da unidade
        $remanejamentoJoin->join(
            'unidades',
            function ($query) {
                $query->on('unidades.id', 'compra_item_unidade.unidade_id');
            }
        );

        # Recuperar remanejamento solicitado
        $remanejamentoJoin->where('arp_remanejamento.rascunho', false);

//        $arraySituacoesRemanejamento = [$situacaoItem, 'Aguardando Aceitação da Unidade de Origem e Gerenciadora'];
//
//        # JOIN para recuperar os remanejamentos com base na situação do remanejamento
//        $remanejamentoJoin->join('codigoitens', function ($query) use ($arraySituacoesRemanejamento) {
//            $query->on('codigoitens.id', 'arp_remanejamento.situacao_id')
//                ->whereIN('codigoitens.descricao', $arraySituacoesRemanejamento);
//        });

        # JOIN para recuperar a ata vinculada ao remanejamento
        $remanejamentoJoin->join(
            'arp',
            'arp.id',
            'arp_remanejamento.arp_id'
        );

        # Se a unidade for gerenciadora da ata
        if ($unidadeGerenciadoraItem) {
            $remanejamentoJoin->where('arp.unidade_origem_id', $idUnidadeUsuarioLogado);
            # JOIN para recuperar os remanejamentos com base na situação dos itens no remanejamento
//            $remanejamentoJoin->join('codigoitens as c1', function ($query) use ($situacaoItem) {
//                $query->on('c1.id', 'arp_remanejamento_itens.situacao_id')
//                    ->where('c1.descricao', "<>", $situacaoItem);
//            });
        }

        # JOIN para recuperar a unidade de origem da solicitação do remanejamento
        $remanejamentoJoin->join(
            'unidades as u1',
            'u1.id',
            'arp_remanejamento.unidade_solicitante_id'
        );

        return $remanejamentoJoin;
    }

    /**
     *
     */
    private function selectSQLAnaliseRemanejamento(Builder $remanejamento)
    {
        # Exibe somente os campos necessários
        $remanejamentoSelect = $remanejamento->select(
            'arp_remanejamento.arp_id',
            'arp_remanejamento.unidade_solicitante_id',
            'arp_remanejamento_itens.situacao_id',
            'arp_remanejamento.id',
            'arp_remanejamento.data_envio_solicitacao',
            'arp_remanejamento.sequencial',
            'arp_remanejamento.remanejamento_entre_unidade_estado_df_municipio_distinto',
            'arp_remanejamento.fornecedor_de_acordo_novo_local',
            'arp_remanejamento.created_at',
            DB::raw("COUNT(CASE WHEN arp_remanejamento_itens.data_aprovacao_gerenciadora is null THEN 1 END) as
            item_faltando_aprovar")
        );

        # Agrupar os campos para exibir somente um único registro do remanejamento
        $remanejamentoSelect->groupby(
            'arp_remanejamento.arp_id',
            'arp_remanejamento.unidade_solicitante_id',
            'arp_remanejamento_itens.situacao_id',
            'arp_remanejamento.id',
            'arp_remanejamento.data_envio_solicitacao',
            'arp_remanejamento.sequencial',
            'arp_remanejamento.remanejamento_entre_unidade_estado_df_municipio_distinto',
            'arp_remanejamento.fornecedor_de_acordo_novo_local'
        );

        return $remanejamentoSelect;
    }

    /**
     * Método responsável em recuperar o SQL padrão para Análise do item
     */
    private function montarSQLAnaliseRemanejamento(
        ArpRemanejamento $remanejamento,
        string $situacaoItem,
        int $idUnidadeUsuarioLogado,
        bool $unidadeGerenciadoraItem = false
    ) {
        # JOIN com as tabelas
        $remanejamento =
            $this->joinRemanejamentoSetup(
                $remanejamento,
                $situacaoItem,
                $idUnidadeUsuarioLogado,
                $unidadeGerenciadoraItem
            );

        # SELECT das colunas
        $remanejamento = $this->selectSQLAnaliseRemanejamento($remanejamento);

        return $remanejamento->get();
    }

    /**
     * Método responsável em exibir os campos na tela de Análise
     * para o dono do item  e unidade gerenciadora da ata
     */
    private function exibirCamposLabelAnalise(
        CrudPanel $crud,
        ArpRemanejamento $remanejamento,
        string $descricaoSituacaoAnaliseItem
    ) {
        $crud->addField(
            [
                'name' => 'unidade_destino',
                'type' => 'label',
                'label' => 'Unidade solicitante',
                'wrapperAttributes' => ['class' => 'col-md-4'],
                'value' => $remanejamento->getUnidadeDestino(),
            ]
        );

        $crud->addField(
            [
                'name' => 'numero_solicitacao',
                'type' => 'label',
                'label' => 'Número da solicitação',
                'wrapperAttributes' => ['class' => 'col-md-4'],
                'value' => $remanejamento->getNumeroSolicitacao(),
            ]
        );

        $crud->addField(
            [
                'name' => 'situacao',
                'type' => 'label',
                'label' => 'Situação da análise do item',
                'wrapperAttributes' => ['class' => 'col-md-4'],
                'value' => $descricaoSituacaoAnaliseItem,
            ]
        );
        
        $respostaItemRemanejamentoEntreUnidadesEstadoDFMunicipio =
            $remanejamento->getRemanejamentoEntreUnidadeEstadoDfMunicipioDistinto();
        
        $crud->addField(
            [
                'name' => 'remanejamento_entre_unidade_estado_df_municipio_distinto',
                'type' => 'label',
                'label' => 'O remanejamento está sendo feito entre
                unidades de estado, distrito federal ou
                município distintos?',
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value' => $respostaItemRemanejamentoEntreUnidadesEstadoDFMunicipio,
            ]
        );

        if ($respostaItemRemanejamentoEntreUnidadesEstadoDFMunicipio === "Sim") {
            $crud->addField(
                [
                    'name' => 'fornecedor_de_acordo_novo_local',
                    'type' => 'label',
                    'label' => 'O fornecedor beneficiário da ata de
                registro de preços está de acordo com o
                fornecimento no novo local de entrega?',
                    'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                    'value' => $remanejamento->getFornecedorDeAcordoNovoLocal(),
                ]
            );
        }
    }

    /**
     * Método responsável em montar o botão HTML para exibir na tabela para o usuário
     */
    private function montarHtmlBotao(
        Compras $compra,
        Arp $ata,
        BackpackUser $usuarioSolicitante,
        ArpRemanejamentoItem $itemRemanejamento
    ) {
        # Informações referente a compra
        $numeroCompra = $compra->numero_ano;
        $modalidade = $compra->getModalidade();
        $unidadeGerenciadoraCompra = $compra->getUnidadeOrigem();

        # Informações referente a ata
        $numeroAta = $ata->getNumeroAno();
        $vigenciaAta = $ata->getExibirVigenciaAtaCompleta();
        $unidadeOrigemAta = $ata->getUnidadeOrigem();
        $valorTotal = $ata->getValorTotalFormatado();

        # Informações referente ao usuário solicitante
        $nomeUsuario = $usuarioSolicitante->name;
        $email = $usuarioSolicitante->email;
        $cpfEscondido = $usuarioSolicitante->exibirCPFEscondido();

        # Informação sobre o número do item
        $numeroItem = $itemRemanejamento->itemUnidade->compraItens->numero;

        # Informação sobre a unidade do item
        $unidadeOrigemItem = $itemRemanejamento->itemUnidade->unidades->exibicaoCompletaUASG();

        return "<button
                    class='br-button circle mr-3'
                    type='button'
                    data-numero-compra='{$numeroCompra}'
                    data-modalidade='{$modalidade}'
                    data-unidade-gerenciadora-compra='{$unidadeGerenciadoraCompra}'
                    data-numero-ata='{$numeroAta}'
                    data-vigencia-ata='{$vigenciaAta}'
                    data-unidade-origem-ata='{$unidadeOrigemAta}'
                    data-valor-total='{$valorTotal}'
                    data-nome-usuario='{$nomeUsuario}'
                    data-email='{$email}'
                    data-cpf-escondido='{$cpfEscondido}'
                    data-numero-item='{$numeroItem}'
                    data-unidade-origem-item='{$unidadeOrigemItem}'
                    onclick='exibirInformacaoGeralItem(this)'>
                    <i class='fas fa-info-circle'></i>
                </button>";
    }

    /**
     * Método responsável em retornar o valor do step para o campo de quantidade de análise
     */
    private function retornarStepCampoQuantidade(CodigoItem $codigoItem)
    {
        # Inicialmente começa com o tipo de Material
        $step = 1;

        # Se for do tipo serviço, pode ser lançado valor decimal de até 5 dígitos
        if ($codigoItem->descres == 'SERVIÇO') {
            $step = 0.00001;
        }

        return $step;
    }

    /**
     * Método responsável em montar o campo HTML para exibir na quantidade para que o usuário digitar
     */
    private function montarCampoHtmlQuantidade(
        int $idItemRemanejamento,
        ?float $max,
        float $step,
        ?float $quantidadeDigitadaUsuario,
        bool $readonly = false
    ) {
        $attributeReadonly = '';
        if ($readonly) {
            $attributeReadonly = 'readonly';
        }
        $dados = ArpRemanejamentoItem::find($idItemRemanejamento);
        $quantidadeSolicitada = $dados->quantidade_solicitada ?? null;


        $this->crud->addField([
            'name' => "quantidade_solicitada_analise[{$idItemRemanejamento}]",
            'type' => 'hidden',
            'default' => $quantidadeSolicitada,
            'attributes' => ['id' => "quantidade_solicitada_analise_{$idItemRemanejamento}"]
        ]);
        return "<div class='br-input'>
                    <input
                        type='number'
                        name='quantidade_analisada[{$idItemRemanejamento}]'
                        min = '0'
                        max = '{$max}'
                        step='{$step}'
                        value='{$quantidadeDigitadaUsuario}'
                        style='width: 200px;'
                        {$attributeReadonly}
                    />
                </div>";
    }

    /**
     * Método responsável em montar o radio para o usuário selecionar o status da análise
     */
    private function montarRadioAnaliseItem(
        array $statusAnalise,
        int $idItem,
        ?int $idStatusSalvo,
        ?string $quantidadeLancamento
    ) {
        $campo = '';

        foreach ($statusAnalise as $label => $id) {
            $checked = '';
            # Se o status salvo for igual ao do item, então marca o radio
            if ($id == $idStatusSalvo) {
                $checked = 'checked';
            }

            $idCampo = "status_id_{$id}_{$idItem}";

            $campo .= "<div class='mr-5 pt-2'>
                        <div class='br-radio'>
                            <input
                                id='{$idCampo}'
                                type='radio'
                                name='status_id[{$idItem}]'
                                value='$id'
                                data-id-item='$idItem'
                                data-status='$label'
                                data-quantidade-autorizada = '$quantidadeLancamento'
                                onchange='alterarEstadoJustificativa(this)'
                                onclick='removerMarcacaoAtual(this)'
                                {$checked}
                            />
                            <label class='analisar_adesao_radio' for='{$idCampo}'></label>
                            <label>{$label}</label>
                        </div>
                      </div>";
        }

        return $campo;
    }

    /**
     * Método responsável em montar o textarea da coluna Justificativa
     */
    private function montarHtmlCampoJustificativa(
        int $idItemRemanejamento,
        ?string $justificativa,
        ?string $analiseItem
    ) {
        $disabled = 'disabled';
        $required = '';

        if ($analiseItem == 'Negar' || $analiseItem == 'Aceitar Parcialmente') {
            $disabled = '';
            $required = 'required';
        }

        return "<div class='br-textarea'>
                    <textarea name='motivo_analise[{$idItemRemanejamento}]' id='textarea-{$idItemRemanejamento}'
                    placeholder='Descreva o justificativa/motivo'
                    style='height: 100px;'
                    {$disabled}
                    {$required}
                    >{$justificativa}</textarea>
                    <div class='text-base mt-1'>
                        <span class='characters'>
                            <strong>0</strong> caracteres digitados
                        </span>
                    </div>
                </div>";
    }

    /**
     * Método responsável em montar o botão para exibir a modal contendo as informações da compra
     */
    private function botaoExibirInformacoesGerais(ArpRemanejamentoItem $itemRemanejamento)
    {
        $textTooltip = 'Para exibir as informações gerais sobre o item.';

        $toolTip = $this->montarTootipBotao($textTooltip);

        $botao = $this->montarHtmlBotao(
            $itemRemanejamento->remanejamento->arp->compras,
            $itemRemanejamento->remanejamento->arp,
            $itemRemanejamento->usuarioSolicitante,
            $itemRemanejamento
        );

        return "{$botao} {$toolTip}";
    }

    /**
     * Método responsável em montar o tooltip para exibir no botão
     */
    private function montarTootipBotao(string $textTooltip)
    {
        return "<div
                    class='br-tooltip text-justify'
                    role='tooltip'
                    info='info'
                    place='top'>
                        <span class='subtext'>{$textTooltip}</span>
                </div>";
    }

    /**
     * Método responsável em exibir as informações no Show
     */
    private function camposShow()
    {
        $this->addColumnDate(true, true, true, true, 'data_envio_solicitacao', 'Data do envio');
        $this->addColumnModelFunction(
            'numero_solicitacao',
            'Nº da solicitação',
            'getNumeroSolicitacao'
        );

        $this->addColumnModelFunction(
            'numero_ata',
            'Nº da ata',
            'getNumeroAta'
        );

        $this->addColumnModelFunction(
            'numero_compra',
            'Nº da compra',
            'getNumeroCompra'
        );

        $this->addColumnModelFunction(
            'modalidade_compra',
            'Modalidade',
            'getModalidadeCompra'
        );

        $this->addColumnModelFunction(
            'unidade_destino',
            'Unidade destino',
            'getUnidadeDestino'
        );

        $this->addColumnModelFunction(
            'situacao',
            'Situação',
            'getSituacao'
        );
    }
    
    public function existeSaldoUnidadeParaRemanejar(CompraItemUnidade $itemUnidade, $quantidadeAnalisada)
    {
        if (empty($quantidadeAnalisada)) {
            $quantidadeAnalisada = 0;
        }
        
        if ($itemUnidade->quantidade_autorizada <= 0 || $itemUnidade->quantidade_saldo <= 0) {
            return false;
        }
     
        if ($quantidadeAnalisada > $itemUnidade->quantidade_autorizada ||
            $quantidadeAnalisada > $itemUnidade->quantidade_saldo) {
            return false;
        }
        
        return true;
    }
}
