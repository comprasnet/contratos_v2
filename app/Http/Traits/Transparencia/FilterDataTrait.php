<?php

namespace App\Http\Traits\Transparencia;

use App\Models\Arp;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;

trait FilterDataTrait
{
    protected function filterData($filter, $orderBy)
    {
        $url = request()->fullUrl();
        $serialize = serialize($url);

        $serialize = hash('sha256', $serialize);
        $cacheKey = 'filtro_arp_'.$serialize;

        $timeStamp = now()->format('YmdHis');


        $data = Cache::get($cacheKey);


        if (is_null($data)) {
            if ($orderBy === 'inicio') {
                $data = Arp::select([
                       'arp.id',
                       'arp.tipo_id',
                       'arp.data_assinatura',
                       'arp.unidade_origem_compra_id',
                       'arp.compra_id',
                       'arp.unidade_origem_id',
                       'arp.modalidade_id',
                       'arp.numero',
                       'arp.ano',
                       'arp.vigencia_inicial',
                       'arp.vigencia_final',
                       'arp.valor_total',
                       'arp.created_at',
                       'arp.updated_at',
                       'arp.objeto'
                   ])
                    ->join('codigoitens', 'arp.tipo_id', '=', 'codigoitens.id')
                    ->orderBy('arp.id', 'ASC')
                   ->where('codigoitens.descricao', '!=', 'Cancelada')
                   ->where('arp.rascunho', false)
                   ->groupBy(
                       'arp.id',
                       'arp.tipo_id',
                       'arp.data_assinatura',
                       'arp.unidade_origem_compra_id',
                       'arp.compra_id',
                       'arp.unidade_origem_id',
                       'arp.modalidade_id',
                       'arp.numero',
                       'arp.ano',
                       'arp.vigencia_inicial',
                       'arp.vigencia_final',
                       'arp.valor_total',
                       'arp.created_at',
                       'arp.updated_at',
                       'arp.objeto',
                   )
                   ->take(10)
                   ->get();
                Cache::put($cacheKey, $data, 60);
                return $data;
            }

            $data = Arp::select([
                'arp.id',
                'arp.tipo_id',
                'arp.data_assinatura',
                'arp.unidade_origem_compra_id',
                'arp.compra_id',
                'arp.unidade_origem_id',
                'arp.modalidade_id',
                'arp.numero',
                'arp.ano',
                'arp.vigencia_inicial',
                'arp.vigencia_final',
                'arp.valor_total',
                'arp.created_at',
                'arp.updated_at',
                'arp.objeto'
            ])
            ->join('codigoitens', 'arp.tipo_id', '=', 'codigoitens.id')
             ->orderBy('arp.id', 'ASC');

            if ($orderBy == 'antigo') {
                $data->orderBy('created_at', 'DESC');
            }

            if ($filter['palavra_chave']) {
                $search = $filter['palavra_chave'];
                $data->join('compras as cpl', 'cpl.id', 'arp.compra_id')
                  //  ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compras.id')
                    ->join('unidades as uni', 'uni.id', '=', 'arp.unidade_origem_id')
                    ->join('orgaos as org', 'org.id', 'uni.orgao_id')
                    ->where(function ($query) use ($search) {
                          $query->where('uni.nomeresumido', 'ilike', '%' . $search . '%')
                            ->orWhere('cpl.numero_ano', 'ilike', '%' . $search . '%')
                            ->orWhere('uni.codigo', 'ilike', '%' . $search . '%')
                             ->orWhere('org.nome', 'ilike', '%' . $search . '%')
                            ->orWhere("arp.numero", "ilike", "%" . $search . "%")
                            ->orWhere("arp.ano", "ilike", "%" . $search . "%")
                              ->orWhere(
                                  DB::raw("concat(arp.numero, '/', arp.ano)"),
                                  "ilike",
                                  "%" . $search . "%"
                              );
                    });
            }

            if ($filter['data_vigencia_inicio']) {
                $dataVigenciaInicio = Carbon::createFromFormat(
                    'd/m/Y',
                    $filter['data_vigencia_inicio']
                )->format('Y-m-d');
                $data->where('arp.vigencia_inicial', '>=', $dataVigenciaInicio);
            }

            if ($filter['data_vigencia_fim']) {
                $dataVigenciaFim = Carbon::createFromFormat(
                    'd/m/Y',
                    $filter['data_vigencia_fim']
                )->format('Y-m-d');
                $data->where('arp.vigencia_final', '<=', $dataVigenciaFim);
            }


            if ($filter['ano_compra']) {
                $anoCompra = $filter['ano_compra'];
                $data->join('compras as cp', 'cp.id', '=', 'arp.compra_id')
                    ->where('cp.numero_ano', 'ilike', '%' . $anoCompra . '%');
            }

            if ($filter['numero_compra']) {
                $numeroCompra = $filter['numero_compra'];
                $data->join('compras as cop', 'cop.id', '=', 'arp.compra_id')
                    ->where('cop.numero_ano', 'ilike', '%' . $numeroCompra . '%');
            }



            if ($filter['numero_ata']) {
                $data->numeroAta($filter['numero_ata']);
            }
            if ($filter['ano_ata']) {
                $data->anoAta($filter['ano_ata']);
            }

            $dataAtual = Carbon::now();
            if ($filter['status'] && $filter['status'] == 'vigente') {
                $data->where(function ($q) use ($dataAtual) {
                    $q->where('vigencia_inicial', '<=', $dataAtual)
                        ->where('vigencia_final', '>=', $dataAtual);
                });
            }

            if ($filter['status'] && $filter['status'] == 'nao_vigente') {
                $data->where(function ($q) use ($dataAtual) {
                    $q->where('vigencia_final', '<', $dataAtual)
                        ->orWhere('vigencia_inicial', '>', $dataAtual);
                });
            }

            if ($filter['modalidade_licitacao'][0] != null) {
                $data->join('compras', 'compras.id', '=', 'arp.compra_id')
                    ->join(
                        'codigoitens as codigoitens_arp',
                        'codigoitens_arp.id',
                        '=',
                        'arp.tipo_id'
                    )
                    ->join(
                        'codigoitens as codigoitens_compras',
                        'codigoitens_compras.id',
                        '=',
                        'compras.modalidade_id'
                    )
                    ->whereIn(
                        'codigoitens_compras.descricao',
                        $filter['modalidade_licitacao']
                    );
            }

            if ($filter['orgaos_gerenciadores'] ||
                $filter['unidades_gerenciadoras'] ||
                $filter['orgaos_participantes'] ||
                $filter['unidades_participantes'] ||
                $filter['unidade_federacao']) {
                $data->join('compras as compra', 'compra.id', 'arp.compra_id')
                    ->join('compra_items', 'compra_items.compra_id', 'compra.id')
                    ->join(
                        'compra_item_unidade',
                        'compra_item_unidade.compra_item_id',
                        '=',
                        'compra_items.id'
                    )
                    ->join('unidades as unit', 'unit.id', '=', 'compra_item_unidade.unidade_id')
                    ->join('orgaos', 'orgaos.id', '=', 'unit.orgao_id')
                    ->join('municipios', 'municipios.id', '=', 'unit.municipio_id')
                    ->join('estados', 'estados.id', '=', 'municipios.estado_id');

                $dataFilter = [];

                if ($filter['orgaos_gerenciadores'] || $filter['unidades_gerenciadoras']) {
                    array_push($dataFilter, 'G');
                }
                if ($filter['orgaos_participantes'] || $filter['unidades_participantes']) {
                    array_push($dataFilter, 'P');
                }
                 $data->whereIn('compra_item_unidade.tipo_uasg', $dataFilter);

                if ($filter['orgaos_gerenciadores'] || $filter['orgaos_participantes']) {
                    $orgaoFilter = array_merge(
                        (array)$filter['orgaos_gerenciadores'],
                        (array)$filter['orgaos_participantes']
                    );
                    $data->whereIn('orgaos.id', $orgaoFilter);
                }

                if ($filter['unidades_gerenciadoras'] || $filter['unidades_participantes']) {
                    $unidadeFilter = array_merge(
                        (array)$filter['unidades_gerenciadoras'],
                        (array)$filter['unidades_participantes']
                    );

                    $data->whereIn('compra_item_unidade.unidade_id', $unidadeFilter);
                }
                if ($filter['unidade_federacao']) {
                    $data->whereIn('estados.id', (array)$filter['unidade_federacao']);
                }
            }

//            if ($filter['orgaos_gerenciadores']) {
//                $data->join('compras', 'compras.id', 'arp.compra_id')
//                    ->join('compra_items', 'compra_items.compra_id', 'compras.id')
//                    ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compras.id')
//                    ->join('unidades', 'unidades.id', '=', 'compra_item_unidade.unidade_id')
//                    ->join('orgaos', 'orgaos.id', '=', 'unidades.orgao_id')
//                    ->where('compra_item_unidade.tipo_uasg', 'G')
//                    ->whereIn('orgaos.id', $filter['orgaos_gerenciadores']);
//            }
//            if ($filter['unidades_gerenciadoras']) {
//                $data->join('compras', 'compras.id', 'arp.compra_id')
//                    ->join('compra_items', 'compra_items.compra_id', 'compras.id')
//                    ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compras.id')
//                    ->join('unidades', 'unidades.id', '=', 'compra_item_unidade.unidade_id')
//                    ->where('compra_item_unidade.tipo_uasg', 'G')
//                  //  ->whereIn('arp.unidade_origem_id', $filter['unidades_gerenciadoras']);
//                    ->whereIn('compra_item_unidade.unidade_id', $filter['unidades_gerenciadoras']);
//            }
//            if ($filter['orgaos_participantes']) {
//                $data->join('compras', 'compras.id', 'arp.compra_id')
//                    ->join('compra_items', 'compra_items.compra_id', 'compras.id')
//                     ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compras.id')
//                    ->join('unidades', 'unidades.id', '=', 'compra_item_unidade.unidade_id')
//                    ->join('orgaos', 'orgaos.id', '=', 'unidades.orgao_id')
//                    ->where('compra_item_unidade.tipo_uasg', 'P')
//                    ->whereIn('orgaos.id', $filter['orgaos_participantes']);
//            }
//            if ($filter['unidades_participantes']) {
//                $data->join('compras', 'compras.id', 'arp.compra_id')
//                    ->join('compra_items', 'compra_items.compra_id', 'compras.id')
//                    ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compras.id')
//                   // ->join('unidades', 'unidades.id', '=', 'compra_item_unidade.unidade_id')
//                    ->where('compra_item_unidade.tipo_uasg', 'P')
//                    ->whereIn('compra_item_unidade.unidade_id', $filter['unidades_participantes']);
//            }

            if ($filter['fornecedor_ata']) {
                $data->join('arp_item', 'arp_item.arp_id', '=', 'arp.id')
                    ->join(
                        'compra_item_fornecedor',
                        'compra_item_fornecedor.id',
                        '=',
                        'arp_item.compra_item_fornecedor_id'
                    )
                    ->join(
                        'fornecedores',
                        'fornecedores.id',
                        '=',
                        'compra_item_fornecedor.fornecedor_id'
                    )
                    ->whereIn('fornecedores.id', $filter['fornecedor_ata']);
            }



            $data = $data->select([
               'arp.*'
           //    'orgaos.nome as orgao_nome'
            ])
              // ->where('compra_item_unidade.tipo_uasg', 'G')
               ->where('arp.rascunho', false)
               ->where('codigoitens.descricao', '=', 'Ata de Registro de Preços')
              // ->groupBy('arp.id', 'orgao_nome')
               ->groupBy('arp.id')
               ->paginate(10);
            Cache::put($cacheKey, $data, 60);
        }
        return $data;


            // teste fim
//            $data = Arp::join('compras', 'compras.id', 'arp.compra_id')
//               ->join('compra_items', 'compra_items.compra_id', 'compras.id')
//               ->join('compra_item_unidade', function ($join) {
//                   $join->on('compra_item_unidade.compra_item_id', '=', 'compra_items.id')
//                       ->where('compra_item_unidade.situacao', true);
//               })
//               ->join('unidades', 'unidades.id', 'compra_item_unidade.unidade_id')
//               ->join('orgaos', 'orgaos.id', 'unidades.orgao_id')
//               ->join('arp_item', 'arp_item.arp_id', 'arp.id')
//               ->join('compra_item_fornecedor', function ($join) {
//                   $join->on('compra_item_fornecedor.id', '=', 'arp_item.compra_item_fornecedor_id')
//                       ->where('compra_item_fornecedor.situacao', true);
//               })
//               ->join(
//                   'fornecedores',
//                   'fornecedores.id',
//                   'compra_item_fornecedor.fornecedor_id'
//               )
//               ->join('codigoitens', 'codigoitens.id', 'compras.modalidade_id')
//               ->orderBy('arp.id', 'ASC');




//            if ($filter['palavra_chave']) {
//                $search = $filter['palavra_chave'];
//                $data->where(function ($query) use ($search) {
//                    $query->where(
//                        'compra_item_unidade.tipo_uasg',
//                        'G'
//                    )->where('unidades.nome', 'ilike', '%' . $search . '%')
//                       ->orWhere('compras.numero_ano', 'ilike', '%' . $search . '%')
//                       ->orWhere('orgaos.nome', 'ilike', '%' . $search . '%')
//                       ->orwhere("arp.numero", "ilike", "%" . $search . "%")
//                       ->orwhere("arp.ano", "ilike", "%" . $search . "%");
//                });
//            }

//
//
//            if ($filter['data_vigencia_inicio']) {
//                $data->vigenciaInicial($filter['data_vigencia_inicio']);
//            }
//
//            if ($filter['data_vigencia_fim']) {
//                $data->vigenciaFinal($filter['data_vigencia_fim']);
//            }
//
//            if ($filter['ano_compra']) {
//                $data->anoCompra($filter['ano_compra']);
//            }
//
//            if ($filter['numero_ata']) {
//                $data->numeroAta($filter['numero_ata']);
//            }
//            if ($filter['ano_ata']) {
//                $data->anoAta($filter['ano_ata']);
//            }
//
//            if ($filter['numero_compra']) {
//                $data->numeroCompra($filter['numero_compra']);
//            }
//
//            $dataAtual = Carbon::now();
//            if ($filter['status'] && $filter['status'] == 'vigente') {
//                $data->whereRaw("'$dataAtual' between vigencia_inicial  and  vigencia_final");
//            }
//
//            if ($filter['status'] && $filter['status'] == 'nao_vigente') {
//                $data->whereRaw("'$dataAtual' not between vigencia_inicial  and  vigencia_final");
//            }
//
//            if ($filter['modalidade_licitacao'][0] != null) {
//                $data->whereIn(
//                    'codigoitens.descricao',
//                    $filter['modalidade_licitacao']
//                );
//            }
//
//            if ($filter['unidade_federacao']) {
//                $data->join(
//                    'municipios',
//                    'municipios.id',
//                    'unidades.municipio_id'
//                )
//                   ->join(
//                       'estados',
//                       'estados.id',
//                       'municipios.estado_id'
//                   )
//                   ->where(
//                       'estados.nome',
//                       'like',
//                       '%' . $filter['unidade_federacao'] . '%'
//                   );
//            }
//
//            if ($filter['orgaos_gerenciadores']) {
//                $data->where('compra_item_unidade.tipo_uasg', 'G')
//                   ->whereIn('orgaos.id', $filter['orgaos_gerenciadores']);
//            }
//
//            if ($filter['unidades_gerenciadoras']) {
//                $data->where('compra_item_unidade.tipo_uasg', 'G')
//                   ->whereIn('unidades.id', $filter['unidades_gerenciadoras']);
//            }
//
//            if ($filter['orgaos_participantes']) {
//                $data->where('compra_item_unidade.tipo_uasg', 'P')
//                   ->whereIn('orgaos.id', $filter['orgaos_participantes']);
//            }
//
//            if ($filter['unidades_participantes']) {
//                $data->where('compra_item_unidade.tipo_uasg', 'P')
//                   ->whereIn('unidades.id', $filter['unidades_participantes']);
//            }
//            if ($filter['fornecedor_ata']) {
//                $data->whereIn('fornecedores.id', $filter['fornecedor_ata']);
//            }
//
    }
}
