<?php

namespace App\Http\Traits;

use App\Models\Catmatsergrupo;
use App\Models\CodigoItem;
use Illuminate\Support\Facades\DB;

trait BuscaCodigoItens
{
    public function retornaArrayCodigosItens($descCodigo, bool $exibeCodigo = false)
    {
        if ($exibeCodigo) {
            return CodigoItem::whereHas('codigo', function ($query) use ($descCodigo) {
                $query->where('descricao', '=', $descCodigo)
                    ->whereNull('deleted_at');
            })
            ->whereRaw('LENGTH(descres) <= 2')
            ->orderBy('descres')
            ->select(DB::raw("CONCAT(descres,' - ',descricao) AS descres_descricao"), 'id')
            ->pluck('descres_descricao', 'id');
        } else {
            return CodigoItem::whereHas('codigo', function ($query) use ($descCodigo) {
                $query->where('descricao', '=', $descCodigo)
                    ->whereNull('deleted_at');
            })
                ->whereNull('deleted_at')
                ->orderBy('descricao')
                ->pluck('descricao', 'id')
                ->toArray();
        }
    }

    public function retornaDescCodigoItem($id)
    {
        return Codigoitem::where('id', $id)
            ->select('descricao')->first()->descricao;
    }

    public function retornaDescresCodigoItemById($id)
    {
        return Codigoitem::where('id', $id)
            ->select('descres')->first()->descres;
    }

    public function retornaIdCodigoItem($descCodigo, $descCodItem)
    {
        return Codigoitem::whereHas('codigo', function ($query) use ($descCodigo) {
            $query->where('descricao', '=', $descCodigo)
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->where('descricao', '=', $descCodItem)
            ->first()->id;
    }


    public function retornaIdCodigoItemPorDescres($descres, $descricao)
    {
        $codigo_item = Codigoitem::whereHas('codigo', function ($query) use ($descricao) {
            $query->where('descricao', '=', $descricao)
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->where('descres', '=', $descres)
            ->first();
        return $codigo_item->id ?? null;
    }

    public function retornaDescresCodigoItem($descCodigo, $descCodItem)
    {
        return Codigoitem::whereHas('codigo', function ($query) use ($descCodigo) {
            $query->where('descricao', '=', $descCodigo)
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->where('descricao', '=', $descCodItem)
            ->first()->descres;
    }

    public function retornaIdCatMatSerGrupo($descCodigo)
    {
        return Catmatsergrupo::whereNull('deleted_at')
            ->where('descricao', '=', $descCodigo)
            ->first()->id;
    }

    /**
     * Método responsável em retornar o(s) ID(s) com base no array do filtro
     */
    public function retornaIdsCodigoItem(string $descCodigo, array $descCodItem)
    {
        return Codigoitem::whereHas('codigo', function ($query) use ($descCodigo) {
            $query->where('descricao', '=', $descCodigo)
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->whereIN('descricao', $descCodItem)
            ->pluck('codigoitens.id', 'codigoitens.descricao')
            ->toArray();
    }
}
