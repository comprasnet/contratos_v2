<?php

namespace App\Http\Traits;

use App\Models\CodigoItem;
use App\Models\Contratopreposto;
use App\Models\Contratoresponsavel;
use App\Services\ModelSignatario\ModelDTO;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Smalot\PdfParser\Parser;

trait SignatarioTrait
{
    public function signatariosResponsaveis()
    {
        return $this->belongsToMany(
            Contratoresponsavel::class,
            'models_signatarios',
            (new ModelDTO($this))->fkColumn,
            'signatario_contratoresponsavel_id',
        )->withPivot([
            'id',
            'pagina_assinatura',
            'posicao_x_assinatura',
            'posicao_y_assinatura',
            'status_assinatura_id',
            'data_operacao',
        ]);
    }

    public function signatariosPrepostos()
    {
        return $this->belongsToMany(
            Contratopreposto::class,
            'models_signatarios',
            (new ModelDTO($this))->fkColumn,
            'signatario_contratopreposto_id',
        )->withPivot([
            'id',
            'pagina_assinatura',
            'posicao_x_assinatura',
            'posicao_y_assinatura',
            'status_assinatura_id',
            'data_operacao',
        ]);
    }


    public function converteModalSelectPrepostosResponsaveisParaSignatarios(
        array $modalSelectPrepostosResponsaveis
    ): Collection {
        list($prepostosIds, $responsaveisIds) = array_reduce(
            $modalSelectPrepostosResponsaveis,
            function ($carry, $id) {
                $index = strstr($id, 'preposto') !== false ? 0 : 1;
                $carry[$index][] = explode('-', $id)[1];
                return $carry;
            },
            [[], []]
        );

        $prepostos = Contratopreposto::where('situacao', true)->whereIn('id', $prepostosIds)->get();
        $responsaveis = Contratoresponsavel::whereIn('id', $responsaveisIds)->get();

        return $prepostos->merge($responsaveis);
    }

    public function notificarAssinatura(Collection $signatarios, Mailable $assinarAEMail)
    {
        $emails = $signatarios->map(function ($signatario) {
            return $signatario->email ?? $signatario->user->email;
        });

        dispatch(function () use ($assinarAEMail, $emails) {
            Mail::to($emails->toArray())->send($assinarAEMail);
        })->onQueue('notifica_assinatura_autorizacao_execucao_v2')->afterCommit();
    }

    public function getPosicoesAssinaturas(string $pathPDF, Collection $assinantes): array
    {
        $parser = new Parser();
        $pdfParser = $parser->parseFile(Storage::disk('public')->path($pathPDF));
        $pages = $pdfParser->getPages();
        $countPages = count($pages);
        $pagesReverse = array_reverse($pages);

        $pointToMilimiter = 0.352777778;
        foreach ($assinantes as $key => $assinante) {
            $posicaoXAssinatura = 0;
            $posicaoYAssinatura = 0;
            $paginaAssinatura = 0;
            $nomeAssinante = $assinante->user ? $assinante->user->name : $assinante->nome;
            foreach ($pagesReverse as $keyPage => $page) {
                $break = false;
                $details = $page->getDetails();
                $heightPage = $details['MediaBox'][3];
                $dataTm = $page->getDataTm();
                foreach ($dataTm as $keyTm => $tm) {
                    if (isset($tm[1]) && $tm[1] === $nomeAssinante) {
                        $break = true;
                        $posicaoXAssinatura = $dataTm[$keyTm - 1][0][4] * $pointToMilimiter;
                        $posicaoYAssinatura = ($heightPage - $dataTm[$keyTm - 1][0][5]) * $pointToMilimiter;
                        $paginaAssinatura = $countPages - $keyPage;
                        break;
                    }
                }
                if ($break) {
                    break;
                }
            }

            $result[$key] = [
                'arquivo_generico_id' => null,
                'posicao_x_assinatura' => $posicaoXAssinatura,
                'posicao_y_assinatura' => $posicaoYAssinatura,
                'pagina_assinatura' => $paginaAssinatura,
            ];
        }

        return $result;
    }

    public function getViewStatusAssinaturasSignatarios()
    {
        $signarios = [];
        $statusAssinatura = CodigoItem::where('descres', 'status_assinatura')->get();

        foreach ($this->signatariosResponsaveis()->with(['funcao', 'user'])->get() as $signtario) {
            $signarios[] = [
                'nome' => $signtario->usuario_nome,
                'funcao' => $signtario->funcao->descricao,
                'data_operacao' => $signtario->pivot->data_operacao ?
                    Carbon::parse($signtario->pivot->data_operacao)->format('d/m/Y') :
                    '',
                'status_assinatura_id' => $signtario->pivot->status_assinatura_id,
            ];
        }

        foreach ($this->signatariosPrepostos()->with(['user'])->get() as $signtario) {
            $signarios[] = [
                'nome' => $signtario->user ? $signtario->user->name : $signtario->nome,
                'funcao' => 'Preposto',
                'data_operacao' => $signtario->pivot->data_operacao ?
                    Carbon::parse($signtario->pivot->data_operacao)->format('d/m/Y') :
                    '',
                'status_assinatura_id' => $signtario->pivot->status_assinatura_id,
            ];
        }

        return view(
            'crud::autorizacaoexecucao.status-assinatura-signatarios',
            [
                'signtarios' => $signarios,
                'statusAssinatura' => $statusAssinatura
            ]
        )->render();
    }

    public function getSignatarioByUsuario(): ?Model
    {
        $result = $this->signatariosResponsaveis()
            ->whereNull('models_signatarios.data_operacao')
            ->where('user_id', backpack_user()->id)
            ->first();

        if (!$result) {
            $result = $this->load(['signatariosPrepostos' => function ($query) {
                $query ->where('user_id', backpack_user()->id)
                    ->orWhere('cpf', backpack_user()->cpf);
            }]);

            if ($result->signatariosPrepostos) {
                $result = $result->signatariosPrepostos->first();
            }
        }

        return $result;
    }
}
