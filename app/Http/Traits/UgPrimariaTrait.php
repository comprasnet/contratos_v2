<?php

namespace App\Http\Traits;

use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use Backpack\CRUD\app\Library\Widget;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

trait UgPrimariaTrait
{
    use Formatador;
    use UsuarioFornecedorTrait;
    
    private function setarUnidadeUsuario()
    {
        if (backpack_user()) {
            $this->alterarAdministradorusuarioFornecedorLogout(backpack_user());

            $dadosUsuario = backpack_user()->toArray();
            // RETIRAR QUANDO O REDIS ESTIVER FUNCIONANDO
            $dadosUsuarioRedis = null;
            
            $tipoSessao = config("session.driver");
            if ($tipoSessao == 'redis') {
                $dadosUsuarioRedis = $this->recuperarDadosUsuarioRedis(auth()->id());
                //dd($dadosUsuarioRedis);
                if (!empty($dadosUsuarioRedis) && !$this->verificarUsuarioTemApenasPerfilFornecedor()) {
                    $this->salvarDadosUsuarioRedis($dadosUsuarioRedis);
                    $unidade = backpack_user()->unidadeprimaria($dadosUsuarioRedis['ugprimaria']);
                    if ($unidade) {
                        $this->inserirRemoverInformacaoUsuarioSessao(
                            true,
                            $unidade->codigo,
                            $unidade->id,
                            $unidade->orgao_id,
                            $unidade->sisg,
                            $unidade->nomeresumido
                        );
                    }
                    return ;
                }
            }
            
            if (!empty(session()->get('user_ug')) and !empty(session()->get('user_ug_id'))) {
                if (backpack_user()->ugprimaria) {
                    $unidade = backpack_user()->unidadeprimaria(session()->get('user_ug_id'));
                    if ($unidade) {
                        $userUg = $dadosUsuarioRedis['user_ug'] ?? $unidade->codigo;
                        $userUgId = $dadosUsuarioRedis['user_ug_id'] ?? $unidade->id;
                        $userOrgaoId = $dadosUsuarioRedis['user_orgao_id'] ?? $unidade->orgao_id;
                        
                        $this->inserirRemoverInformacaoUsuarioSessao(
                            true,
                            $userUg,
                            $userUgId,
                            $userOrgaoId,
                            $unidade->sisg,
                            $unidade->nomeresumido
                        );
                        
                        $dadosUsuario['user_ug'] = $userUg;
                        $dadosUsuario['user_ug_id'] = $userUgId;
                        $dadosUsuario['user_orgao_id'] =  $userOrgaoId;
                    } else {
                        $this->inserirRemoverInformacaoUsuarioSessao(false);
                        
                        $dadosUsuario['user_ug'] = null;
                        $dadosUsuario['user_ug_id'] = null;
                        $dadosUsuario['user_orgao_id'] = null;
                    }
                } else {
                    $this->inserirRemoverInformacaoUsuarioSessao(false);
                }
            } elseif (session()->get('user_ug') == null and session()->get('user_ug_id') == null) {
                if (backpack_user()->ugprimaria) {
                    $unidade = backpack_user()->unidadeprimaria(backpack_user()->ugprimaria);
                    if ($unidade) {
                        $this->inserirRemoverInformacaoUsuarioSessao(
                            true,
                            $unidade->codigo,
                            $unidade->id,
                            $unidade->orgao_id,
                            $unidade->sisg,
                            $unidade->nomeresumido
                        );
                    } else {
                        $this->inserirRemoverInformacaoUsuarioSessao(false);
                    }
                } else {
                    $this->inserirRemoverInformacaoUsuarioSessao(false);
                }
            } else {
                # se chegou aqui é porque tem o user_ug e user_ug_id -
                # se for adm, vamos deixar passar - mvascs@gmail.com.
                if (backpack_user()->hasRole('Administrador') || backpack_user()->hasRole('Administrador Suporte')) {
                    $ok = true;
                } else {
                    $ok = backpack_user()->havePermissionUg(session()->get('user_ug_id'));
                }
                if ($ok == false) {
                    \Session::flush();
                    backpack_auth()->logout();
                    return Redirect::to('/inicio');
                }
            }
            
            if ($tipoSessao == 'redis') {
                $this->salvarDadosUsuarioRedis($dadosUsuario);
            }
        }
    }
    
    /**
     *
     * Método responsável em inserir ou limpar as informações do usuário na sessão
     *
     * @param bool $inserirInformacao
     * @param string|null $codigoUnidade
     * @param int|null $unidadeId
     * @param int|null $orgaoUnidadeId
     * @param bool|null $unidadeSisg
     * @param string|null $nomeUnidadeResumido
     * @return void
     */
    public function inserirRemoverInformacaoUsuarioSessao(
        bool $inserirInformacao,
        ?string $codigoUnidade = null,
        ?int $unidadeId = null,
        ?int $orgaoUnidadeId = null,
        ?bool $unidadeSisg = null,
        ?string $nomeUnidadeResumido = null
    ) {
        if ($inserirInformacao) {
            $this->dataUserSession($codigoUnidade, $unidadeId, $orgaoUnidadeId, $unidadeSisg, $nomeUnidadeResumido);
            return;
        }
        
        $this->dataUserSession();
    }
    
    /**
     *
     * Método responsável em inserir as informações na sessão do usuário
     *
     * @param string|null $codigoUnidade
     * @param int|null $unidadeId
     * @param int|null $orgaoUnidadeId
     * @param bool|null $unidadeSisg
     * @param string|null $nomeUnidadeResumido
     * @return void
     */
    public function dataUserSession(
        ?string $codigoUnidade = null,
        ?int $unidadeId = null,
        ?int $orgaoUnidadeId = null,
        ?bool $unidadeSisg = null,
        ?string $nomeUnidadeResumido = null
    ) {
        session(['user_ug' => $codigoUnidade]);
        session(['user_ug_id' => $unidadeId]);
        session(['user_orgao_id' => $orgaoUnidadeId]);
        session(['user_ug_sisg' => $unidadeSisg]);
        session(['user_ug_nome_resumido' => $nomeUnidadeResumido]);
    }
    
    public function unidadeUsuarioPertenceCentralCompras(?string $unidadeUsuario = null)
    {
        if (empty($unidadeUsuario)) {
            $unidadeUsuario = session('user_ug');
        }
        if ($unidadeUsuario == '201057') {
            return true;
        }
        
        return false;
    }
}
