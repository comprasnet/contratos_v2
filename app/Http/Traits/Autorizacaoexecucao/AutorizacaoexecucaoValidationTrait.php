<?php

namespace App\Http\Traits\Autorizacaoexecucao;

use App\Http\Traits\Formatador;
use App\Models\Contrato;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use Illuminate\Validation\Rule;

trait AutorizacaoexecucaoValidationTrait
{
    use Formatador;

    protected function prepareFields()
    {
        $this->request->set('data_assinatura', $this->convertDateGovToBd($this->data_assinatura));
        $this->request->set('data_vigencia_inicio', $this->convertDateGovToBd($this->data_vigencia_inicio));
        $this->request->set('data_vigencia_fim', $this->convertDateGovToBd($this->data_vigencia_fim));

        if ($this->autorizacaoexecucaoItens) {
            $autorizacaoItensFormatada = [];
            foreach ($this->autorizacaoexecucaoItens as $key => $item) {
                $item['quantidade_itens_entrega'] =  $item['quantidade_itens_entrega'] ?
                    (float) $item['quantidade_itens_entrega'] : 0;
                $item['quantidade'] =  $item['quantidade'] ? $this->retornaFormatoAmericano($item['quantidade']) : null;
                $item['valor_unitario'] = $item['valor_unitario'] ?
                    $this->retornaFormatoAmericano($item['valor_unitario']):
                    null;

                $item['quantidade_total'] = $item['quantidade'] * $item['parcela'];

                $autorizacaoItensFormatada[$key] = $item;
            }

            $this->request->set('autorizacaoexecucaoItens', $autorizacaoItensFormatada);
        }

        $contrato = Contrato::findOrFail($this->contrato_id);
        $this->request->set('data_vigencia_inicio_contrato', $contrato->vigencia_inicio);
        if (!!$contrato->vigencia_fim) {
            $this->request->set('data_vigencia_fim_contrato', $contrato->vigencia_fim);
        }
    }


    protected function getDefaultRules()
    {
        return array_merge(
            [
                'processo' => 'required_if:rascunho,0',
                'numero_sistema_origem' => 'nullable|string',
                'tipo_id' => 'required',
                'numero' => [
                    'required_if:rascunho,0',
                    'nullable',
                    Rule::unique('autorizacaoexecucoes', 'numero')->where(function ($query) {
                        return $query->where('contrato_id', $this->contrato_id)
                            ->whereNull('deleted_at');
                    })->ignore($this->autorizacaoexecucao_id ?? $this->id),
                ],
                'data_assinatura' => [
                    Rule::requiredIf(function () {
                        return $this->request->get('rascunho') == 0 &&
                            $this->request->get('originado_sistema_externo');
                    }),
                    'nullable',
                    'date',
                    'before_or_equal:data_vigencia_inicio',
                ],
                'data_vigencia_inicio' => [
                    'bail',
                    'required_if:rascunho,0',
                    'nullable',
                    'date',
                    $this->addAfterOrEqualTodayIfOriginadoDentroSistema(),
                    'after_or_equal:data_vigencia_inicio_contrato',
                    $this->addBeforeOrEqualDataVigenciaFimContratoIfExist(),
                    'before_or_equal:data_vigencia_fim',
                ],
                'data_vigencia_fim' => [
                    'bail',
                    'required_if:rascunho,0',
                    'nullable',
                    'date',
                    'after_or_equal:data_vigencia_inicio_contrato',
                    $this->addBeforeOrEqualDataVigenciaFimContratoIfExist(),
                    'after_or_equal:data_vigencia_inicio',
                ],
                'unidaderequisitante_ids' => 'required_if:rascunho,0|nullable|array',
                'unidaderequisitante_ids.*' => 'required_if:rascunho,0|nullable|integer',
                'contrato_local_execucao_ids' => 'nullable|array',
                'contrato_local_execucao_ids.*' => 'nullable|integer',
                'informacoes_complementares' => 'nullable|string',
                'empenhos' => 'required_if:rascunho,0|nullable|array',
                'empenhos.*' => 'required_if:rascunho,0|nullable|integer',
            ],
            $this->getItensRules()
        );
    }

    protected function getItensRules()
    {
        return [
            'autorizacaoexecucaoItens' => 'required_if:rascunho,0|nullable|array',
            'autorizacaoexecucaoItens.*.quantidade' => 'required_if:rascunho,0|nullable|numeric',
            'autorizacaoexecucaoItens.*.parcela' => 'required_if:rascunho,0|nullable|integer|min:1',
            'autorizacaoexecucaoItens.*.quantidade_total' => [
                'nullable',
                'numeric',
                'gte:autorizacaoexecucaoItens.*.quantidade_itens_entrega'
            ],
            'autorizacaoexecucaoItens.*.valor_unitario' => 'required_if:rascunho,0|nullable|numeric',
            'autorizacaoexecucaoItens.*.horario' => [
                'nullable',
                'date_format:H:i',
                'before_or_equal:autorizacaoexecucaoItens.*.horario_fim',
            ],
            'autorizacaoexecucaoItens.*.horario_fim' => [
                'nullable',
                'date_format:H:i',
                'after_or_equal:autorizacaoexecucaoItens.*.horario',
            ],
            'autorizacaoexecucaoItens.*.subcontratacao' => 'required',
            'autorizacaoexecucaoItens.*.especificacao' => 'nullable|string|max:50',
            'autorizacaoexecucaoItens.*.unidade_medida_id' => 'required_if:rascunho,0',
            'autorizacaoexecucaoItens.*.autorizacaoexecucao_itens_id' => [
                'nullable',
                'numeric',
                'exists:autorizacaoexecucao_itens,id'
            ],
        ];
    }

    protected function getDefaultAttributes(): array
    {
        $fields = (new AutorizacaoExecucaoService())->getFieldLabels(false);

        return [
            'processo' => $fields['processo']['label'],
            'tipo_id' => $fields['tipo_id']['label'],
            'numero' => $fields['numero']['label'],
            'data_assinatura' => $fields['data_assinatura']['label'],
            'data_vigencia_inicio' => $fields['data_vigencia_inicio']['label'],
            'data_vigencia_fim' => $fields['data_vigencia_fim']['label'],
            'data_vigencia_inicio_contrato' => $fields['vigencia_inicial_label']['label'] . ' do Contrato',
            'data_vigencia_fim_contrato' => $fields['vigencia_final_label']['label'] . ' do Contrato',
            'unidaderequisitante_ids' => $fields['unidaderequisitante_ids']['label'],
            'contrato_local_execucao_ids' => $fields['locais_execucao']['label'],
            'empenhos' => $fields['empenhos']['label'],
            'autorizacaoexecucaoItens' => 'Item da Ordem de Serviço / Fornecimento',
            'autorizacaoexecucaoItens.*.quantidade' => $fields['autorizacaoexecucaoItens']['label']['quantidade'],
            'autorizacaoexecucaoItens.*.parcela' => $fields['autorizacaoexecucaoItens']['label']['parcela'],
            'autorizacaoexecucaoItens.*.valor_unitario' =>
                $fields['autorizacaoexecucaoItens']['label']['valor_unitario'],
            'autorizacaoexecucaoItens.*.quantidade_total' =>
                $fields['autorizacaoexecucaoItens']['label']['quantidade_total'],
            'autorizacaoexecucaoItens.*.quantidade_itens_entrega' =>
                $fields['autorizacaoexecucaoItens']['label']['quantidade_itens_entrega'],
            'autorizacaoexecucaoItens.*.horario' => $fields['autorizacaoexecucaoItens']['label']['horario'],
            'autorizacaoexecucaoItens.*.horario_fim' => $fields['autorizacaoexecucaoItens']['label']['horario_fim'],
            'autorizacaoexecucaoItens.*.subcontratacao' =>
                $fields['autorizacaoexecucaoItens']['label']['subcontratacao'],
            'autorizacaoexecucaoItens.*.especificacao' => $fields['autorizacaoexecucaoItens']['label']['especificacao'],
            'autorizacaoexecucaoItens.*.unidade_medida_id' =>
                $fields['autorizacaoexecucaoItens']['label']['unidade_medida_id'],
        ];
    }

    protected function getDefaultMessages()
    {
        return [
            'processo.required_if' => 'O campo :attribute é obrigatório.',
            'numero.required_if' => 'O campo :attribute é obrigatório.',
            'data_assinatura.required_if' => 'O campo :attribute é obrigatório.',
            'data_vigencia_inicio.required_if' => 'O campo :attribute é obrigatório.',
            'data_vigencia_fim.required_if' => 'O campo :attribute é obrigatório.',
            'unidaderequisitante_ids.required_if' => 'O campo :attribute é obrigatório.',
            'contrato_local_execucao_ids.required_if' => 'O campo :attribute é obrigatório.',
            'empenhos.required_if' => 'O campo :attribute é obrigatório.',
            'autorizacaoexecucaoItens.required_if' => 'Adicione ao menos um :attribute.',
            'autorizacaoexecucaoItens.*.quantidade.required_if' => 'O campo :attribute é obrigatório.',
            'autorizacaoexecucaoItens.*.parcela.required_if' => 'O campo :attribute é obrigatório.',
            'autorizacaoexecucaoItens.*.valor_unitario.required_if' => 'O campo :attribute é obrigatório.',
            'autorizacaoexecucaoItens.*.quantidade_total.gte' =>
                "O campo :attribute deve ser maior ou igual a Quantidade já entregue.",
            'autorizacaoexecucaoItens.*.horario.required_if' => 'O campo :attribute execução é obrigatório.',
            'autorizacaoexecucaoItens.*.horario.before_or_equal' =>
                'O campo :attribute deve ser um horário anterior ou igual a Horário fim.',
            'autorizacaoexecucaoItens.*.horario_fim.required_if' => 'O campo :attribute execução é obrigatório.',
            'autorizacaoexecucaoItens.*.horario_fim.after_or_equal' =>
                'O campo :attribute deve ser um horário posterior ou igual a Horário inicio.',
            'autorizacaoexecucaoItens.*.subcontratacao.required_if' => 'O campo :attribute é obrigatório.',
            'autorizacaoexecucaoItens.*.unidade_medida_id.required_if' => 'O campo :attribute é obrigatório.',
        ];
    }

    private function addBeforeOrEqualDataVigenciaFimContratoIfExist(): string
    {
        if ($this->request->has('data_vigencia_fim_contrato')) {
            return 'before_or_equal:data_vigencia_fim_contrato';
        }

        return '';
    }

    private function addAfterOrEqualTodayIfOriginadoDentroSistema()
    {
        if ($this->request->get('originado_sistema_externo')) {
            return '';
        }

        return 'after_or_equal:today';
    }
}
