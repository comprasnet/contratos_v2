<?php

namespace App\Http\Traits\Autorizacaoexecucao;

use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Http\Traits\LogTrait;
use App\Http\Traits\Unidade\ProcessoTrait;
use App\Models\AutorizacaoexecucaoItens;
use App\Models\Contrato;
use App\Models\Contratohistorico;
use App\Models\Contratoitem;
use App\Models\Empenho;
use App\Models\Unidade;
use App\Repositories\AutorizacaoExecucao\AutorizacaoExecucaoRepository;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Library\Widget;
use Illuminate\Support\Facades\DB;

trait AutorizacaoexecucaoCrudTrait
{
    use BuscaCodigoItens;
    use Formatador;
    use ListOperation;
    use CreateOperation;
    use ShowOperation;
    use ProcessoTrait;
    use ImportContent;
    use LogTrait;

    protected $contrato;
    private $autorizacaoexecucao;

    private $fields;

    private function cabecalho()
    {
        $this->crud->cabecalhoAta = true;

        $this->crud->addField([
            'name' => 'title_contrato',
            'type' => 'custom_html',
            'value' => '<h2 class="h6 my-0">Dados do Contrato</h2>',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $this->crud->addField([
            'name' => 'contrato',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['contrato']['value'],
            'model' => Contrato::class,
            'label' => $this->fields['contrato']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);

        $this->crud->addField([
            'name' => 'unidade_id',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['unidade_gestora']['value'],
            'model' => Contrato::class,
            'label' => $this->fields['unidade_gestora']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);

        $this->crud->addField([
            'name' => 'fornecedor',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['fornecedor']['value'],
            'label' => $this->fields['fornecedor']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);

        $this->crud->addField([
            'name' => 'contratante',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['contratante']['value'],
            'label' => $this->fields['contratante']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);

        $objectField = view('vendor.backpack.crud.fields.label_hint', [
            'value' => $this->fields['restrito']['value'],
            'label' => $this->fields['restrito']['label'],
            'max' => 180
        ]);

        $this->crud->addField([   // CustomHTML
            'name' => 'restrito',
            'type' => 'custom_html',
            'value' => $objectField,
            'wrapperAttributes' => ['class' => 'col-md-6 restrito-wrapper'],
        ]);

        $this->crud->addField([
            'name' => 'vigencia_inicial_label',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['vigencia_inicial_label']['value'],
            'label' => $this->fields['vigencia_inicial_label']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-3',
            ],
        ]);


        $this->crud->addField([
            'name' => 'vigencia_final_label',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['vigencia_final_label']['value'],
            'label' => $this->fields['vigencia_final_label']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-3',
            ],
        ]);

        $this->crud->addField([
            'name' => 'amparo_legal',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['amparo_legal']['value'],
            'label' => $this->fields['amparo_legal']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);

        $this->crud->addField([
            'name' => 'contrato_processo',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['contrato_processo']['value'],
            'label' => $this->fields['contrato_processo']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ]
        ]);

        $this->crud->addField([
            'name' => 'preposto',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['preposto']['value'],
            'label' => $this->fields['preposto']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);


        $this->crud->addField([
            'name' => 'gestores',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['gestores']['value'],
            'label' => $this->fields['gestores']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ]
        ]);
    }

    private function campos()
    {
        $this->camposAutorizacaoExecucao();

        $this->crud->addField([
            'attribute' => 'unidade_codigo_nomeresumido',
            'name' => 'unidaderequisitante_ids',
            'minimum_input_length' => 0,
            'type' => 'select2_from_ajax_multiple_custom',
            'label' => $this->fields['unidaderequisitante_ids']['label'],
            'required' => true,
            'default' => $this->fields['unidaderequisitante_ids']['value']['ids'],
            'model' => Unidade::class,
            'data_source' => url("autorizacaoexecucao/{$this->contrato->id}/unidades"),
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $this->crud->addField([
            'attribute' => 'numero_unidade',
            'name' => 'empenhos',
            'type' => 'select2_from_ajax_multiple_custom',
            'label' => $this->fields['empenhos']['label'],
            'required' => true,
            'model' => Empenho::class,
            'data_source' => url("autorizacaoexecucao/{$this->contrato->id}/empenhos"),
            'minimum_input_length' => 0,
            'pivot' => true,
            'default' => $this->fields['empenhos']['value']['ids'],
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $this->camposAutorizacaoExecucaoItens($this->fields['autorizacaoexecucaoItens']['value']);

        $this->crud->addField([
            'name' => 'contrato_local_execucao_ids',
            'type' => 'select2_from_array',
            'label' => $this->fields['locais_execucao']['label'],
            'default' => $this->fields['locais_execucao']['value']['ids'],
            'options' => $this->contrato->locaisExecucao()->pluck('descricao', 'id')->toArray(),
            'allows_multiple' => true,
            'required' => false,
            'attributes' => [
                'id' => 'contrato_local_execucao_ids'
            ],
            'wrapper' => [
                'class' => 'form-group col-md-10 pr-0',
            ],
        ]);

        $this->crud->addField([
            'name' => 'modal_criar_local_execucao',
            'type' => 'modal_criar_local_execucao',
        ]);

        $this->crud->addField([
            'name' => 'informacoes_complementares',
            'type' => 'tinymce',
            'label' => $this->fields['informacoes_complementares']['label'],
            'value' => $this->fields['informacoes_complementares']['value'],
            'options' => [
                'language' => 'pt_BR',
                'menubar' => false,
                'plugins' => [
                    'print autolink lists link table',
                ],
                'toolbar' => 'print | undo redo | copy cut paste |' .
                    ' bold italic underline strikethrough superscript subscript | 
                    backcolor forecolor link unlink blockquote | alignleft aligncenter alignright alignjustify | 
                    bullist numlist outdent indent | hr table removeformat',
            ],
        ]);
    }

    private function camposAutorizacaoExecucao(bool $typeLabel = false)
    {
        $this->crud->addField([
            'name' => 'separator',
            'type' => 'custom_html',
            'value' => '<hr>',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $this->crud->addField([
            'name' => 'title_osf',
            'type' => 'custom_html',
            'value' => '<h2 class="h6 my-0">Dados da OS/F</h2>',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $processo = [
            'name' => 'processo',
            'type' => $typeLabel ? 'label' : 'text',
            'label' => $this->fields['processo']['label'],
            'attributes' => [
                'required' => 'required'
            ],
            'required' => true,
            'wrapper' => [
                'class' => 'form-group col-md-4',
            ],
            'value' => $this->fields['processo']['value'],
        ];

        $mask = $this->getProcessoMask();
        if ($mask) {
            $processo['attributes']['data-mask'] = $mask;
            $processo['attributes']['placeholder'] = $mask;
        }

        $this->crud->addField($processo);

        $optionsTipos = $this->retornaArrayCodigosItens('Tipo Autorizacao Execução');

        $this->crud->addField([
            'name' => 'originado_sistema_externo',
            'type' => 'select_from_array',
            'options' => [1 => 'Sim', 0 => 'Não'],
            'label' => $this->fields['originado_sistema_externo']['label'],
            'value' => $this->fields['originado_sistema_externo']['value'],
            'attributes' => [
                'readonly'=>'readonly'
            ],
            'wrapper' => [
                'class' => 'd-none ',
            ],
        ]);

        $this->crud->addField([
            'name' => 'tipo_id',
            'type' => $typeLabel ? 'label' : 'select2_from_array',
            'options' => $optionsTipos,
            'label' => $this->fields['tipo_id']['label'],
            'value' => $typeLabel ?
                $this->fields['tipo_id']['value']['descricao'] :
                $this->fields['tipo_id']['value']['id'],
            'wrapper' => [
                'class' => 'form-group col-md-4',
            ],
        ]);


        $this->crud->addField([
            'name' => 'numero',
            'type' => $typeLabel ? 'label' : 'text',
            'label' => $this->fields['numero']['label'],
            'attributes' => [
                'data-mask' => '00000/0000',
                'placeholder' => "12345/" . date('Y'),
            ],
            'value' => $this->fields['numero']['value'],
            'required' => true,
            'wrapper' => [
                'class' => 'form-group col-md-4',
            ],
        ]);

        if ($this->crud->getRequest()->query('external') || $this->fields['originado_sistema_externo']['value']) {
            $this->crud->addField([
                'name' => 'data_assinatura',
                'type' => $typeLabel ? 'label' : 'date',
                'label' => $this->fields['data_assinatura']['label'],
                'attributes' => [
                    'required' => 'required'
                ],
                'required' => true,
                'value' => $typeLabel ?
                    $this->fields['data_assinatura']['value']['formated'] :
                    $this->fields['data_assinatura']['value']['original'],
                'wrapper' => [
                    'class' => 'form-group col-md-4',
                ],
            ]);
        }

        $this->crud->addField([
            'name' => 'data_vigencia_inicio',
            'type' => $typeLabel ? 'label' : 'date',
            'label' => $this->fields['data_vigencia_inicio']['label'],
            'attributes' => [
                'required' => 'required'
            ],
            'required' => true,
            'value' => $typeLabel ?
                $this->fields['data_vigencia_inicio']['value']['formated'] :
                $this->fields['data_vigencia_inicio']['value']['original'],
            'wrapper' => [
                'class' => 'form-group col-md-4',
                'id' => $typeLabel ? 'data_vigencia_inicio_label' : null,
            ],
        ]);

        $nameDataVigenciaFim = 'data_vigencia_fim';
        if ($typeLabel) {
            $nameDataVigenciaFim .= '_label';
        }

        $this->crud->addField([
            'name' => $nameDataVigenciaFim,
            'type' => $typeLabel ? 'label' : 'date',
            'label' => $this->fields['data_vigencia_fim']['label'],
            'attributes' => [
                'required' => 'required'
            ],
            'required' => true,
            'value' => $typeLabel ?
                $this->fields['data_vigencia_fim']['value']['formated'] :
                $this->fields['data_vigencia_fim']['value']['original'],
            'wrapper' => [
                'class' => 'form-group col-md-4',
                'id' => $typeLabel ? 'data_vigencia_fim_label' : null,
            ],
        ]);

        $this->crud->addField([
            'name' => 'numero_sistema_origem',
            'type' => $typeLabel ? 'label' : 'text',
            'label' => $this->fields['numero_sistema_origem']['label'],
            'value' => $this->fields['numero_sistema_origem']['value'],
            'wrapper' => [
                'class' => 'form-group col-md-4',
            ],
        ]);

        $this->crud->addField([
            'name' => 'separator_2',
            'type' => 'custom_html',
            'value' => '<hr>',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);
    }

    private function camposAutorizacaoExecucaoItens($value, $hidden = false)
    {
        $this->importarDatatableForm();

        $this->importarScriptCss([
            'assets/css/autorizacaoexecucao/progress-bar.css'
        ]);
        $this->importarScriptJs([
            'packages/tableBackpackToDataTable/InserTableToDataTable.js',
            'assets/js/numberField.js',
            'packages/backpack/base/js/padrao_gov/jquery.mask.js',
            'assets/js/admin/forms/autorizacaoExecucao/submit_assinatura.js',
            'assets/js/admin/forms/autorizacaoExecucao/adicionar.js',
            'assets/js/autorizacaoexecucao/functions.js',
        ]);

        $contratoItens = Contratoitem::with('tipo', 'item')
            ->where('contrato_id', $this->contrato->id)
            ->orderBy('numero_item_compra')
            ->get(['id', 'numero_item_compra', 'tipo_id', 'catmatseritem_id']);

        $this->crud->addField([
            'name' => 'adiciona_itens',
            'type' => 'adiciona_itens',
            'options' => $contratoItens,
            'hidden' => $hidden
        ]);

        $this->crud->addField([
            'name' => 'autorizacaoexecucaoItens',
            'type' => 'relationship_table',
            'relation_type' => 'HasMany',
            'label' => 'Itens',
            'model' => AutorizacaoexecucaoItens::class,
            'value' => $value,
            'multiple' => true,
            'subfields' => [
                [
                    'name' => 'contratohistorico_id',
                    'type' => 'hidden',
                    'attributes' => [
                        'class' => 'contratohistorico_id'
                    ],
                ],
                [
                    'name' => 'saldohistoricoitens_id',
                    'type' => 'hidden',
                    'attributes' => [
                        'class' => 'saldohistoricoitens_id'
                    ],
                ],
                [
                    'name' => 'contratoitem_id',
                    'type' => 'hidden',
                    'attributes' => [
                        'class' => 'contratoitem_id'
                    ],
                ],
                [
                    'name' => 'autorizacaoexecucao_itens_id',
                    'type' => 'hidden',
                    'attributes' => [
                        'class' => 'autorizacaoexecucao_itens_id'
                    ],
                ],
                [
                    'name' => 'periodo_vigencia_inicio',
                    'type' => 'hidden',
                    'attributes' => [
                        'class' => 'periodo_vigencia_inicio'
                    ],
                ],
                [
                    'name' => 'periodo_vigencia_fim',
                    'type' => 'hidden',
                    'attributes' => [
                        'class' => 'periodo_vigencia_fim'
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['periodo'],
                    'name' => 'periodo',
                    'type' => 'text',
                    'attributes' => [
                        'disabled' => true,
                        'class' => 'form-control periodo'
                    ],
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['tipo_item'],
                    'name' => 'tipo_item',
                    'type' => 'text',
                    'attributes' => [
                        'disabled' => true,
                        'class' => 'form-control tipo_item'
                    ],
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['numero_item_compra'],
                    'name' => 'numero_item_compra',
                    'type' => 'text',
                    'attributes' => [
                        'disabled' => true,
                        'class' => 'form-control numero_item_compra'
                    ],
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['item'],
                    'name' => 'item',
                    'type' => 'text',
                    'attributes' => [
                        'disabled' => true,
                        'class' => 'form-control item'
                    ],
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'name' => 'quantidade_item',
                    'type' => 'hidden',
                    'attributes' => [
                        'class' => 'quantidade_item'
                    ],
                ],
                [
                    'name' => 'numero_parcelas',
                    'type' => 'hidden',
                    'attributes' => [
                        'class' => 'numero_parcelas'
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['quantidade_contratada'],
                    'name' => 'quantidade_contratada',
                    'type' => 'number',
                    'attributes' => [
                        'disabled' => true,
                        'class' => 'form-control quantidade_contratada'
                    ],
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['valor_unitario_contratado'],
                    'name' => 'valor_unitario_contratado',
                    'type' => 'text',
                    'attributes' => [
                        'disabled' => true,
                        'class' => 'form-control valor_unitario_contratado'
                    ],
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['unidade_medida_id'],
                    'name' => 'unidade_medida_id',
                    'type' => 'select_from_array',
                    'required' => true,
                    'options' => [],
                    'attributes' => [
                        'class' => 'form-control unidade_medida_select'
                    ],
                    'wrapper' => [
                        'class' => 'form-group col-md-4 unidade_medida_id required',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['quantidade'],
                    'name' => 'quantidade',
                    'type' => 'text',
                    'attributes' => [
                        'class' => 'form-control quantidade',
                        'min' => 0,
                        'data-item' => 'quantidade'
                    ],
                    'required' => true,
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['parcela'],
                    'name' => 'parcela',
                    'type' => 'number',
                    'attributes' => [
                        'class' => 'form-control parcela',
                        'min' => 1,
                        'data-item' => 'parcela'
                    ],
                    'required' => true,
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['quantidade_total'],
                    'name' => 'quantidade_total',
                    'type' => 'text',
                    'attributes' => [
                        'readonly' => true,
                        'class' => 'form-control quantidade_total disable-item',
                        'data-item' => 'quantidade_total'
                    ],
                    'required' => true,
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['quantidade_itens_entrega'],
                    'name' => 'quantidade_itens_entrega',
                    'type' => 'text',
                    'attributes' => [
                        'readonly' => true,
                        'class' => 'form-control quantidade_itens_entrega disable-item',
                        'data-item' => 'quantidade_itens_entrega'
                    ],
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['valor_unitario'],
                    'name' => 'valor_unitario',
                    'type' => 'text',
                    'attributes' => [
                        'class' => 'form-control valor_unitario',
                        'data-item' => 'valorUnitario'

                    ],
                    'required' => true,
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['valor_total'],
                    'name' => 'valor_total',
                    'type' => 'text',
                    'attributes' => [
                        'class' => 'form-control valor_total',
                        'data-item' => 'total'
                    ],
                    'required' => true,
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['subcontratacao'],
                    'name' => 'subcontratacao',
                    'type' => 'select2_from_array',
                    'options' => [
                        true => 'Sim',
                        false => 'Não',
                    ],
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['horario'],
                    'name' => 'horario',
                    'type' => 'time',
                    'attributes' => [
                        'data-mask' => '00:00',
                    ],
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['horario_fim'],
                    'name' => 'horario_fim',
                    'type' => 'time',
                    'attributes' => [
                        'data-mask' => '00:00',
                    ],
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['especificacao'],
                    'name' => 'especificacao',
                    'type' => 'text',
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                    'attributes' => ['maxlength' => 50]
                ],
                [
                    'label' => $this->fields['autorizacaoexecucaoItens']['label']['numero_demanda_sistema_externo'],
                    'name' => 'numero_demanda_sistema_externo',
                    'type' => 'text',
                    'wrapper' => [
                        'class' => 'form-group col-md-4',
                    ],
                ],
            ],
        ]);
    }

    private function getContrato(bool $administradorFornecedor = false)
    {
        $contratoId = request()->contrato_id;

        $contratoQuery = Contrato::where('id', '=', $contratoId);
        if (!$administradorFornecedor) {
            $contratoQuery->where(function ($query) {
                $query->whereHas('responsaveis', function ($query) {
                    $query->where('user_id', '=', backpack_user()->id);
                })
                    ->orWhereHas('prepostos', function ($query) {
                        $query->where('cpf', backpack_user()->cpf);
                    });
            });
        }

        $contrato = $contratoQuery->first();

        if (!$contrato) {
            abort('403', config('app.erro_permissao'));
        }

        return $contrato;
    }
}
