<?php

namespace App\Http\Traits\Autorizacaoexecucao;

use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Http\Traits\LogTrait;
use App\Http\Traits\Unidade\ProcessoTrait;
use App\Models\AutorizacaoexecucaoItens;
use App\Models\Contrato;
use App\Models\Contratoitem;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Illuminate\Support\Facades\DB;

trait FiscalizacaoCabecalhoTrait
{
    use BuscaCodigoItens;
    use Formatador;
    use ListOperation;
    use CreateOperation;
    use ShowOperation;
    use ProcessoTrait;
    use ImportContent;
    use LogTrait;

    protected $contrato;
    private $autorizacaoexecucao;

    private $fields;

    private function cabecalho()
    {
        $this->crud->cabecalhoAta = true;

        $this->crud->addField([
            'name' => 'contrato',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['contrato']['value'],
            'model' => Contrato::class,
            'label' => $this->fields['contrato']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-2',
            ],
        ]);

        $this->crud->addField([
            'name' => 'fornecedor',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['fornecedor']['value'],
            'label' => $this->fields['fornecedor']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-4',
            ],
        ]);

        $this->crud->addField([
            'name' => 'contratante',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['contratante']['value'],
            'label' => $this->fields['contratante']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-4',
            ],
        ]);

        $objectField = view('vendor.backpack.crud.fields.label_hint', [
            'value' => $this->fields['restrito']['value'],
            'label' => $this->fields['restrito']['label'],
            'max' => 180
        ]);

        $this->crud->addField([   // CustomHTML
            'name' => 'restrito',
            'type' => 'custom_html',
            'value' => $objectField,
            'wrapperAttributes' => ['class' => 'col-md-6 restrito-wrapper'],
        ]);

        $this->crud->addField([
            'name' => 'vigencia_inicial_label',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['vigencia_inicial_label']['value'],
            'label' => $this->fields['vigencia_inicial_label']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-2',
            ],
        ]);


        $this->crud->addField([
            'name' => 'vigencia_final_label',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['vigencia_final_label']['value'],
            'label' => $this->fields['vigencia_final_label']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-2',
            ],
        ]);

        $this->crud->addField([
            'name' => 'amparo_legal',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['amparo_legal']['value'],
            'label' => $this->fields['amparo_legal']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);

        $this->crud->addField([
            'name' => 'contrato_processo',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['contrato_processo']['value'],
            'label' => $this->fields['contrato_processo']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ]
        ]);

        $this->crud->addField([
            'name' => 'preposto',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['preposto']['value'],
            'label' => $this->fields['preposto']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);


        $this->crud->addField([
            'name' => 'gestores',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'value' => $this->fields['gestores']['value'],
            'label' => $this->fields['gestores']['label'],
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ]
        ]);
    }
    private function getContrato()
    {
        $contratoId = request()->contrato_id;

        $contrato = Contrato::where('id', '=', $contratoId)
            ->where(function ($query) {
                $query->whereHas('responsaveis', function ($query) {
                    $query->where('user_id', '=', backpack_user()->id);
                })
                ->orWhereHas('prepostos', function ($query) {
                    $query->where('cpf', backpack_user()->cpf);
                });
            })
            ->first();

        if (!$contrato) {
            abort('403', config('app.erro_permissao'));
        }

        return $contrato;
    }
}
