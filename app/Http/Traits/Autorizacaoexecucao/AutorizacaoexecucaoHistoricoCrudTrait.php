<?php

namespace App\Http\Traits\Autorizacaoexecucao;

use App\Http\Traits\CommonColumns;
use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoexecucaoHistorico;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

trait AutorizacaoexecucaoHistoricoCrudTrait
{
    use AutorizacaoexecucaoCrudTrait;
    use CommonColumns;

    private $tipoHistorico;

    private function settingsSetup(string $type)
    {
        abort_if(!in_array($type, ['alterar', 'retificar']), 500, 'Tipo Histórico é inválido');

        $this->tipoHistorico = $type;

        $this->contrato = $this->getContrato();
        $this->autorizacaoexecucao = AutorizacaoExecucao::where('id', request()->autorizacaoexecucao_id)
            ->where('contrato_id', request()->contrato_id)
            ->firstOrFail();

        $autorizacaoExecucaoService = new AutorizacaoExecucaoService();
        $autorizacaoExecucaoService->setContrato($this->contrato);
        $autorizacaoExecucaoService->setAutorizacaoExecucao($this->autorizacaoexecucao);
        $this->fields = $autorizacaoExecucaoService->getFieldLabels();

        CRUD::setRoute(config('backpack.base.route_prefix') . '/autorizacaoexecucao/'. request()->contrato_id . '/' .
            request()->autorizacaoexecucao_id . '/' . $type);

        CRUD::setModel(AutorizacaoexecucaoHistorico::class);
        CRUD::addclause(
            "join",
            "codigoitens",
            "codigoitens.id",
            "=",
            "autorizacaoexecucao_historico.tipo_historico_id"
        );
        CRUD::addclause("where", 'autorizacaoexecucoes_id', '=', request()->autorizacaoexecucao_id);
        CRUD::addclause("where", 'codigoitens.descres', '=', 'aeh_' . $type);

        CRUD::addclause("select", 'autorizacaoexecucao_historico.*');
        CRUD::addclause("orderBy", "sequencial", "DESC");
        $this->exibirTituloPaginaMenu(
            ucfirst($type) .
            ' Ordem de Serviço / Fornecimento do Contrato ' . $this->contrato->numero . ' - ' .
                $this->contrato->unidade->codigo,
        );
    }

    private function breadCrumb(bool $listOperation = false)
    {
        $linkVoltar = backpack_url('autorizacaoexecucao/' .
            $this->contrato->id . '/' .
            $this->autorizacaoexecucao->id . '/' . $this->tipoHistorico);

        if ($listOperation) {
            $linkVoltar = backpack_url('autorizacaoexecucao/' . $this->contrato->id);
        }

        $this->data['breadcrumbs'] = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
            'Ordens de Serviço / Fornecimento do Contrato ' . $this->contrato->numero =>
                backpack_url('autorizacaoexecucao/' . $this->contrato->id),
            ucfirst($this->tipoHistorico) .  " Ordem de Serviço / Fornecimento" => false,
            // 'Voltar' => $linkVoltar,
        ];
    }

    private function addColumnUsuarioResponsavel()
    {
        $label = ($this->tipoHistorico == 'alterar' ?
            'Responsável pela Alteração' :
            'Responsável pela Retificação');

        CRUD::addColumn([
            'name' => 'usuario_responsavel',
            'type' => 'model_function_raw',
            'label' => $label,
            'function_name' => 'getUsuarioResponsavel',
            'escaped' => false,
        ]);
    }

    private function addColumnItensAntesDepois()
    {
        CRUD::addColumn([
            'name' => 'itens_antes',
            'type' => 'model_function_raw',
            'label' => 'Itens Antes',
            'function_name' => 'getItensAntes',
            'escaped' => false,
        ]);
        CRUD::addColumn([
            'name' => 'itens_depois',
            'type' => 'model_function_raw',
            'label' => 'Itens Depois',
            'function_name' => 'getItensDepois',
            'escaped' => false,
        ]);
    }
}
