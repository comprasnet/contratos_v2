<?php

namespace App\Http\Traits\Autorizacaoexecucao;

use App\Http\Traits\BuscaCodigoItens;
use App\Models\AutorizacaoExecucao;
use App\Models\Contratounidadedescentralizada;
use App\Models\Empenho;
use App\Models\Unidade;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

trait AutorizacaoexecucaoSetupRootTrait
{
    use BuscaCodigoItens;

    protected function setupRoot()
    {
        if (!backpack_user()->can('autorizacaoexecucao_visualizar')) {
            return abort(403);
        }

        CRUD::setModel(AutorizacaoExecucao::class);
        CRUD::setSubheading('Criar', 'create');
        CRUD::setSubheading('Editar', 'edit');
        CRUD::setSubheading('Visualizar', 'index');

        CRUD::addClause('select', 'autorizacaoexecucoes.*');

        $this->crud->addButtonFromModelFunction(
            'line',
            'getViewButtons',
            'getViewButtons',
        );
    }

    protected function setupListOperationRoot(?int $contratoId, ?int $fornecedorId)
    {
        $fields = $this->autorizacaoExecucaoService->getFieldLabels(false);

        $this->filtros($fields, $contratoId, $fornecedorId);

        $this->crud->addColumn([
            'name' => 'situacao',
            'type' => 'model_function',
            'label' => 'Situação',
            'function_name' => 'getSituacaoChipComponent',
            'limit' => 999999999,
            'priority' => 1
        ]);

        if ($fornecedorId) {
            $this->crud->addColumn([
                'name' => 'contrato',
                'type' => 'model_function_raw',
                'label' => $fields['contrato']['label'],
                'function_name' => 'getContrato',
                'priority' => 2
            ]);
        }

        $this->crud->addColumn([
            'name' => 'processo',
            'type' => 'text',
            'label' => $fields['processo']['label'],
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'numero_sistema_origem',
            'type' => 'text',
            'label' => $fields['numero_sistema_origem']['label'],
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'tipo_id',
            'type' => 'model_function',
            'label' => $fields['tipo_id']['label'],
            'function_name' => 'getTipo',
        ]);

        $this->crud->addColumn([
            'name' => 'originado_sistema_externo',
            'type' => 'model_function',
            'label' => $fields['originado_sistema_externo']['label'],
            'function_name' => 'getOriginadoSistemaExterno',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'numero',
            'type' => 'text',
            'label' => $fields['numero']['label_short']
        ]);

        $this->crud->addColumn([
            'name' => 'progressbar-fornecedor-osf',
            'type' => 'progressbar-fornecedor-osf',
            'label' => 'Evolução',
            'priority' => 4
        ]);

        $this->crud->addColumn([
            'name' => 'data_assinatura',
            'type' => 'date',
            'label' => $fields['data_assinatura']['label'],
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'status_assinaturas_signatarios',
            'type' => 'model_function',
            'label' => 'Status Assinaturas dos Signatários',
            'function_name' => 'getViewStatusAssinaturasSignatarios',
            'limit' => 999999999
        ]);

        $this->crud->addColumn([
            'name' => 'data_vigencia_inicio',
            'type' => 'date',
            'label' => $fields['data_vigencia_inicio']['label'],
        ]);

        $this->crud->addColumn([
            'name' => 'data_vigencia_fim',
            'type' => 'date',
            'label' => $fields['data_vigencia_fim']['label'],
        ]);

        $this->crud->addColumn([
            'name' => 'valor_total',
            'type' => 'model_function',
            'label' => 'Valor Total',
            'function_name' => 'getValorTotal',
        ]);

        $this->crud->addColumn([
            'name' => 'unidaderequisitante_ids',
            'type' => 'model_function',
            'label' => $fields['unidaderequisitante_ids']['label'],
            'function_name' => 'getUnidadeRequisitantes',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'empenhos',
            'type' => 'model_function',
            'label' => $fields['empenhos']['label'],
            'function_name' => 'getEmpenhos',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'informacoes_complementares',
            'type' => 'text',
            'label' => $fields['informacoes_complementares']['label'],
            'visibleInTable' => false
        ]);

        $this->crud->enableExportButtons();

        $this->importarScriptCss([
            'assets/css/autorizacaoexecucao/progress-bar.css'
        ]);
    }

    protected function setupShowOperationRoot()
    {
        $fields = $this->autorizacaoExecucaoService->getFieldLabels(false);

        $this->crud->addColumn([
            'name' => 'contrato',
            'type' => 'model_function_raw',
            'label' => $fields['contrato']['label'],
            'function_name' => 'getContrato',
        ]);

        $this->crud->addColumn([
            'name' => 'fornecedor',
            'type' => 'model_function_raw',
            'label' => $fields['fornecedor']['label'],
            'function_name' => 'getFornecedor',
        ]);

        $this->crud->addColumn([
            'name' => 'processo',
            'type' => 'text_custom',
            'label' => $fields['processo']['label'],
        ]);

        $this->crud->addColumn([
            'name' => 'tipo_id',
            'type' => 'model_function_raw',
            'label' => $fields['tipo_id']['label'],
            'function_name' => 'getTipo',
        ]);

        $this->crud->addColumn([
            'name' => 'numero',
            'type' => 'text_custom',
            'label' => $fields['numero']['label'],
        ]);

        $this->crud->addColumn([
            'name' => 'data_assinatura',
            'type' => 'date',
            'label' => $fields['data_assinatura']['label'],
        ]);

        $this->crud->addColumn([
            'name' => 'data_vigencia_inicio',
            'type' => 'date',
            'label' => $fields['data_vigencia_inicio']['label'],
        ]);

        $this->crud->addColumn([
            'name' => 'data_vigencia_fim',
            'type' => 'date',
            'label' => $fields['data_vigencia_fim']['label'],
        ]);

        $this->crud->addColumn([
            'name' => 'historico_alteracoes',
            'type' => 'model_function_raw',
            'label' => 'Histórico das alterações anteriores',
            'function_name' => 'getHistoricoAlteracoesViewLink',
        ]);

        $this->crud->addColumn([
            'name' => 'numero_sistema_origem',
            'type' => 'text_custom',
            'label' => $fields['numero_sistema_origem']['label'],
        ]);

        $this->crud->addColumn([
            'name' => 'unidaderequisitante_ids',
            'type' => 'model_function_raw',
            'label' => $fields['unidaderequisitante_ids']['label'],
            'function_name' => 'getUnidadeRequisitantes',
        ]);

        $this->crud->addColumn([
            'name' => 'empenhos',
            'type' => 'model_function_raw',
            'label' => $fields['empenhos']['label'],
            'function_name' => 'getEmpenhos',
        ]);

        $this->crud->addColumn([
            'name' => 'itens',
            'type' => 'model_function_raw',
            'label' => 'Itens',
            'function_name' => 'getItens',
            'escaped' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'valor_total',
            'type' => 'model_function',
            'label' => 'Valor total',
            'function_name' => 'getValorTotal',
        ]);

        $this->crud->addColumn([
            'name' => 'contrato_local_execucao_ids',
            'type' => 'model_function_raw',
            'label' => $fields['locais_execucao']['label'],
            'function_name' => 'getLocaisExecucao',
        ]);

        $this->crud->addColumn([
            'name' => 'informacoes_complementares',
            'type' => 'text_custom',
            'label' => $fields['informacoes_complementares']['label'],
            'escaped' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'arquivo',
            'type' => 'model_function_raw',
            'label' => 'Arquivo OS/F - Assinada',
            'function_name' => 'getArquivoViewLink',
        ]);

        $autorizacaoExecucao = $this->crud->getEntry(\request()->id);

        if (!$autorizacaoExecucao->originado_sistema_externo) {
            $this->crud->addColumn([
                'name' => 'anexos',
                'type' => 'model_function_raw',
                'label' => 'Anexos',
                'function_name' => 'getAnexosViewLink',
            ]);
        }

        $this->crud->addColumn([
            'name' => 'situacao',
            'type' => 'model_function',
            'label' => $fields['situacao']['label'],
            'function_name' => 'getSituacao',
        ]);
    }


    private function filtros(array $fields, ?int $contratoId, ?int $fornecedorId)
    {
        $this->crud->addFilter(
            [
                'type'  => 'select2',
                'name'  => 'situacao_id',
                'label' => 'Situação',
            ],
            $this->retornaArrayCodigosItens('Situação Autorização Execução'),
            function ($value) {
                $this->crud->addClause('where', 'situacao_id', '=', "$value");
            }
        );

        $this->crud->addFilter(
            [
                'type'  => 'select2',
                'name'  => 'tipo_id',
                'label' => $fields['tipo_id']['label'],
            ],
            $this->retornaArrayCodigosItens('Tipo Autorizacao Execução'),
            function ($value) {
                $this->crud->addClause('where', 'tipo_id', '=', "$value");
            }
        );

        $this->crud->addFilter(
            [
                'type'  => 'select2',
                'name'  => 'originado_sistema_externo',
                'label' => $fields['originado_sistema_externo']['label'],
            ],
            ['não', 'sim'],
            function ($value) {
                $this->crud->addClause('where', 'originado_sistema_externo', '=', (bool) $value);
            }
        );

        $this->crud->addFilter(
            [
                'type'  => 'text',
                'name'  => 'numero',
                'label' => $fields['numero']['label_short'],
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'numero', 'LIKE', "%$value%");
            }
        );

        $this->crud->addFilter(
            [
                'type'  => 'text',
                'name'  => 'numero_sistema_origem',
                'label' => $fields['numero_sistema_origem']['label_short'],
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'numero_sistema_origem', 'LIKE', "%$value%");
            }
        );

        $this->crud->addFilter(
            [
                'type'  => 'text',
                'name'  => 'processo',
                'label' => $fields['processo']['label_short'],
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'processo', 'LIKE', "%$value%");
            }
        );

        $this->crud->addFilter(
            [
                'type' => 'date',
                'name' => 'data_vigencia_inicio',
                'label' => $fields['data_vigencia_inicio']['label'],
                'language' => 'pt-BR'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'data_vigencia_inicio', '>=', $value);
            }
        );

        $this->crud->addFilter(
            [
                'type' => 'date',
                'name' => 'data_vigencia_fim',
                'label' => $fields['data_vigencia_fim']['label'],
                'language' => 'pt-BR'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'data_vigencia_fim', '<=', $value);
            }
        );


        $urlFiltroUnidadeSolicitante = "autorizacaoexecucao/{$contratoId}/unidades?pluck=true";

        $urlFiltroEmpenho = "autorizacaoexecucao/{$contratoId}/empenhos?pluck=true";

        if ($fornecedorId && $contratoId) {
            $urlFiltroUnidadeSolicitante =
                "fornecedor/autorizacaoexecucao/{$contratoId}/{$fornecedorId}/unidadesfornecedor?pluck=true";
            $urlFiltroEmpenho =
                "fornecedor/autorizacaoexecucao/{$contratoId}/{$fornecedorId}/empenhosfornecedor?pluck=true";
        }

        if ($fornecedorId) {
            $urlFiltroUnidadeSolicitante =
                "fornecedor/autorizacaoexecucao/{$fornecedorId}/unidadesfornecedor?pluck=true";
            $urlFiltroEmpenho =
                "fornecedor/autorizacaoexecucao/{$fornecedorId}/empenhosfornecedor?pluck=true";
        }

        $this->filtroUnidadeSolicitante(url($urlFiltroUnidadeSolicitante), $fields);
        $this->filtroEmpenhos(url($urlFiltroEmpenho), $fields);
    }

    public function filtroUnidadeSolicitante(string $url, array $fields)
    {

        $this->crud->addFilter(
            [
                'name'        => 'unidaderequisitante_ids',
                'type'        => 'select2_ajax',
                'label'       => $fields['unidaderequisitante_ids']['label'],
                'placeholder' => '',
                'minimum_input_length' => 0,
                'method'      => 'GET',
                'select_attribute' => 'unidade_codigo_nomeresumido',
                'select_key' => 'id'
            ],
            $url,
            function ($value) {
                $this->crud->addClause(
                    'whereHas',
                    'autorizacaoexecucaoUnidadeRequisitantes',
                    function ($query) use ($value) {
                        $query->where('unidade_requisitante_id', $value);
                    }
                );
            }
        );
    }

    public function filtroEmpenhos(string $url, array $fields)
    {
        $this->crud->addFilter(
            [
                'name'        => 'empenho_id',
                'type'        => 'select2_ajax',
                'label'       => $fields['empenhos']['label'],
                'placeholder' => '',
                'minimum_input_length' => 0,
                'method'      => 'GET',
                'select_attribute' => 'numero_unidade',
                'select_key' => 'id'
            ],
            $url,
            function ($value) {
                $this->crud->addClause(
                    'whereHas',
                    'autorizacaoexecucaoEmpenhos',
                    function ($query) use ($value) {
                        $query->where('empenhos_id', $value);
                    }
                );
            }
        );
    }

    public function unidadesFornecedorTrait(Request $request, bool $administrador)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');
        $contractId = $request->contrato_id ?? null;
        $fornecedorId = $request->fornecedor_id ?? null;

        $subQuery = $this->queryBaseContratoUnidadeDescentralizadaFornecedor($fornecedorId, $administrador);

        $query = Unidade::select(
            'unidades.id',
            'unidades.codigo',
            'unidades.nome',
            'unidades.nomeresumido',
        )->leftJoin('contratos', 'unidades.id', '=', 'contratos.unidade_id')
            ->where('contratos.fornecedor_id', $fornecedorId)
            ->distinct();


        if ($search_term) {
            $results = $query
                ->where(function ($query) use ($search_term) {
                    $query->where('nome', 'LIKE', '%' . strtoupper($search_term) . '%')
                        ->orWhere('nomeresumido', 'LIKE', '%' . strtoupper($search_term) . '%')
                        ->orWhere('codigo', 'LIKE', '%' . strtoupper($search_term) . '%');
                })
               /* ->where(function ($query) use ($contractId, $subQuery) {
                    $query->where('contratos.id', $contractId)
                        ->orWhereIn('unidades.id', $subQuery);
                })*/
                ->paginate(10);
        } else {
            $results = $query
                //->where('contratos.id', $contractId)
                ->orWhereIn('unidades.id', $subQuery)
                ->paginate(10);
        }

        $results->append('unidade_codigo_nomeresumido');

        if ($request->input('pluck')) {
            $results->pluck('unidade_codigo_nomeresumido', 'id');
        }

        return $results;
    }

    public function empenhosFornecedorTrait(Request $request, bool $administrador)
    {
        $search_term = $request->input('q');
        $contractId = $request->contrato_id ?? null;
        $fornecedorId = $request->fornecedor_id ?? null;

        $options = Empenho::query()
            ->select(
                'empenhos.id',
                'empenhos.numero',
                'empenhos.unidade_id',
                'empenhos.empenhado',
                'empenhos.aliquidar',
                'empenhos.liquidado',
                'empenhos.pago',
                DB::raw("'' AS numero_unidade")
            )->join('contratoempenhos', 'empenhos.id', '=', 'contratoempenhos.empenho_id', 'left')
            ->join('fornecedores', 'fornecedores.id', '=', 'empenhos.fornecedor_id', 'left')
            ->join('unidades', 'unidades.id', '=', 'empenhos.unidade_id', 'left')
            ->where('fornecedores.id', $fornecedorId)
            ->distinct()
            ->orderBy('empenhos.numero');

        $this->queryBaseContratoPreposto($options, $administrador);

        if ($search_term) {
            $results = $options->where('empenhos.numero', 'ILIKE', '%' . $search_term . '%')->paginate(10);
        } else {
            $results = $options->paginate(10);
        }

        if ($request->input('pluck')) {
            $results->pluck('numero_unidade', 'id');
        }
        return $results;
    }

    public function queryBaseContratoUnidadeDescentralizadaFornecedor(?int $fornecedorId, bool $administrador)
    {
        $subQuery =
            Contratounidadedescentralizada::join(
                'contratos',
                'contatos.id',
                'contratounidadesdescentralizadas.contrato_id'
            )
            ->where('contratos.fornecedor_id', $fornecedorId);
            $this->queryBaseContratoPreposto($subQuery, $administrador);

           return $subQuery->select('contratounidadesdescentralizadas.unidade_id');
    }

    public function queryBaseContratoPreposto(Builder $query, bool $administrador)
    {
        if (!$administrador) {
            $query->join('contratopreposto', 'contratos.id', 'contratopreposto.contrato_id')
                ->where('contratopreposto.cpf', '=', backpack_user()->cpf)
                ->where('contratopreposto.situacao', '=', true);
        }
    }
}
