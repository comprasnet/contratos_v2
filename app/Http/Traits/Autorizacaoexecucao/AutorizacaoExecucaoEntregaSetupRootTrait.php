<?php

namespace App\Http\Traits\Autorizacaoexecucao;

use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoExecucaoEntrega;
use App\Models\CodigoItem;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

trait AutorizacaoExecucaoEntregaSetupRootTrait
{
    use AutorizacaoexecucaoCrudTrait;

    protected function setupRoot()
    {
        $this->contrato = $this->getContrato();
        $this->autorizacaoexecucao = AutorizacaoExecucao::where('id', request()->autorizacaoexecucao_id)
            ->where('contrato_id', $this->contrato->id)
            ->whereIn('situacao_id', CodigoItem::whereIn('descres', ['ae_status_2', 'ae_status_4', 'ae_status_8'])
            ->pluck('id'))
            ->firstOrFail();

        $autorizacaoExecucaoService = new AutorizacaoExecucaoService();
        $autorizacaoExecucaoService->setContrato($this->contrato);
        $autorizacaoExecucaoService->setAutorizacaoExecucao($this->autorizacaoexecucao);
        $this->fields = $autorizacaoExecucaoService->getFieldLabels();

        CRUD::setModel(AutorizacaoExecucaoEntrega::class);
        CRUD::addclause("where", 'autorizacaoexecucao_id', '=', request()->autorizacaoexecucao_id);
    }

    protected function setupCreateOperationRoot()
    {
        $this->importarScriptCss([
            'assets/css/autorizacaoexecucao/progress-bar.css'
        ]);
        $this->importarScriptJs([
            'assets/js/numberField.js'
        ]);

        $this->cabecalho();

        $this->camposAutorizacaoExecucao(true);
    }
}
