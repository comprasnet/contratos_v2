<?php

namespace App\Http\Traits\Autorizacaoexecucao;

use App\Http\Traits\Formatador;
use App\Models\CodigoItem;
use App\Models\Contratopreposto;
use App\Models\Contratoresponsavel;
use App\Models\ModelSignatario;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use App\Services\ModelSignatario\ModelDTO;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait AutorizacaoexecucaoTrait
{
    use Formatador;

    private $modelDTO = null;

    /*
   |--------------------------------------------------------------------------
   | FUNCTIONS
   |--------------------------------------------------------------------------
   */

    public function getArquivoViewLink()
    {
        $result = $this->getArquivo();

        return $result ? $result->getViewLink() : null;
    }

    public function getArquivo()
    {
        $codigoItem = CodigoItem::where('descres', 'ae_arquivo_assinado')->first();
        $query = $this->arquivo()->where('tipo_id', $codigoItem->id);
        if ($query->exists()) {
            return $query->first();
        }

        return null;
    }

    public function generateTableItens(Collection $itens)
    {
        if ($itens->isNotEmpty()) {
            $autorizacaoExecucaoService = new AutorizacaoExecucaoService();

            $fields = $autorizacaoExecucaoService->getFieldLabels(false);
            $fields['autorizacaoexecucaoItens']['value'] = $itens;

            return view('crud::autorizacaoexecucao.table-itens', [
                'fields' => $fields,
            ])->render();
        }

        return '';
    }

    public function generateTableAnexos(MorphMany $anexos)
    {
        $codigoItem = CodigoItem::where('descres', 'ae_arquivo_anexo')->first();
        $query = $anexos->where('tipo_id', $codigoItem->id);

        if ($query->exists()) {
            return view('crud::autorizacaoexecucao.table-anexos', [
                'anexos' => $query->get(),
            ])->render();
        }
        return '';
    }

    public function getSituacaoChipComponent()
    {
        switch ($this->situacao->descres) {
            case 'ae_status_1':
                $cor = '#a3a3a3';
                $corFont = '#fff';
                break;
            case 'ae_status_2':
                $cor = '#168821';
                $corFont = '#fff';
                break;
            case 'ae_status_3':
                $cor = '#e52207';
                $corFont = '#fff';
                break;
            case 'ae_status_4':
                $cor = '#1351b4';
                $corFont = '#fff';
                break;
            case 'ae_status_5':
                $cor = '#f5a623';
                $corFont = '#fff';
                break;
            case 'ae_status_6':
                $cor = '#e07008';
                $corFont = '#fff';
                break;
            case 'ae_status_7':
                $cor = '#0ae68b';
                $corFont = '#fff';
                break;
            case 'ae_status_8':
                $cor = '#e60a0a';
                $corFont = '#fff';
                break;
            default:
                $cor = '#000';
                $corFont = '#fff';
                break;
        }

        return view(
            'components_v2.chip_status',
            [
                'conteudo' => $this->getSituacao(),
                'cor' => $cor,
                'corFont' => $corFont
            ]
        );
    }

    public function getSituacao()
    {
        return $this->situacao->descricao;
    }

    public function getViewStatusAssinaturasSignatarios()
    {
        $signarios = [];
        $statusAssinatura = CodigoItem::where('descres', 'status_assinatura')->get();

        foreach ($this->signatariosResponsaveis()->with(['funcao', 'user'])->get() as $signtario) {
            $signarios[] = [
                'nome' => $signtario->usuario_nome,
                'funcao' => $signtario->funcao->descricao,
                'data_operacao' => $signtario->pivot->data_operacao ?
                    Carbon::parse($signtario->pivot->data_operacao)->format('d/m/Y') :
                    '',
                'status_assinatura_id' => $signtario->pivot->status_assinatura_id,
            ];
        }

        foreach ($this->signatariosPrepostos()->with(['user'])->get() as $signtario) {
            $signarios[] = [
                'nome' => $signtario->user ? $signtario->user->name : $signtario->nome,
                'funcao' => 'Preposto',
                'data_operacao' => $signtario->pivot->data_operacao ?
                    Carbon::parse($signtario->pivot->data_operacao)->format('d/m/Y') :
                    '',
                'status_assinatura_id' => $signtario->pivot->status_assinatura_id,
            ];
        }

        return view(
            'crud::autorizacaoexecucao.status-assinatura-signatarios',
            [
                'signtarios' => $signarios,
                'statusAssinatura' => $statusAssinatura
            ]
        )->render();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function signatarios()
    {
        return $this->hasMany(ModelSignatario::class, $this->getModelTDO()->fkColumn);
    }

    public function signatariosResponsaveis()
    {
        return $this->belongsToMany(
            Contratoresponsavel::class,
            'models_signatarios',
            $this->getModelTDO()->fkColumn,
            'signatario_contratoresponsavel_id',
        )->withPivot([
            'id',
            'pagina_assinatura',
            'posicao_x_assinatura',
            'posicao_y_assinatura',
            'status_assinatura_id',
            'data_operacao',
        ]);
    }

    public function signatariosPrepostos()
    {
        return $this->belongsToMany(
            Contratopreposto::class,
            'models_signatarios',
            $this->getModelTDO()->fkColumn,
            'signatario_contratopreposto_id',
        )->withPivot([
            'id',
            'pagina_assinatura',
            'posicao_x_assinatura',
            'posicao_y_assinatura',
            'status_assinatura_id',
            'data_operacao',
        ]);
    }

    private function getModelTDO(): ModelDTO
    {
        return $this->modelDTO ?? new ModelDTO($this);
    }
}
