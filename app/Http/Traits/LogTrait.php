<?php

namespace App\Http\Traits;

use Backpack\CRUD\app\Library\Widget;
use Exception;
use Illuminate\Support\Facades\Log;

trait LogTrait
{

    public function inserirLogCustomizado(string $channel, string $acao, Exception $e, string $linhaOpcional = null)
    {
        if (!empty($linhaOpcional)) {
            $e =  $linhaOpcional."\n ". $e->getMessage(). "\n ".$e->getTraceAsString();
        }

        switch ($acao) {
            case 'info':
                Log::channel($channel)->info($e);
                break;
            case 'error':
                Log::channel($channel)->error($e);
                break;
        }
    }
}
