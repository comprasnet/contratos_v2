<?php

namespace App\Http\Traits;

use App\Models\Catmatsergrupo;
use App\Models\CodigoItem;
use App\Models\Compras;
use App\Models\JobUser;
use Illuminate\Support\Facades\DB;

trait JobUserTrait
{
    /**
     * Método responsável em recuperar a quantidade de registros por compra
     * na tabela job_user
     */
    private function recuperarQuantidadeJobCompra(int $idCompra)
    {
        return JobUser::where("job_user_id", $idCompra)
        ->where("job_user_type", 'App\Models\Compras')
        ->count();
    }
}
