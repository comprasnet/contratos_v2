<?php

namespace App\Http\Traits;

use App\Models\AmparoLegal;

trait AmparoTrait
{
    /**
     * Retrieves the distinct formatted ato_normativo values from the AmparoLegal model where complemento_14133 is true.
     *
     * @return array The distinct formatted ato_normativo values.
     */
    public function getDerivadas14133Formatado(): array
    {
        return AmparoLegal::where('complemento_14133', true)
            ->selectRaw(
                'DISTINCT REGEXP_REPLACE(SPLIT_PART(ato_normativo, \'/\', 1), \'[.\s]\', \'\', \'g\')
                as ato_normativo_formatado'
            )
            ->pluck('ato_normativo_formatado')->toArray();
    }
}
