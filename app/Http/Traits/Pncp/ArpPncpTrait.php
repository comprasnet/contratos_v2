<?php

namespace App\Http\Traits\Pncp;

use App\Models\Arp;
use App\Models\ArpArquivo;
use App\Models\ArpArquivoTipo;
use App\Models\EnviaDadosPncpHistorico;
use App\Services\Pncp\Arp\InserirArquivoArpPncpService;
use App\Services\Pncp\Arp\InsertArpPncpService;
use App\Services\Pncp\PncpManagerService;
use App\Services\Pncp\Responses\PncpPostResponseService;

trait ArpPncpTrait
{
    /**
     * Método responsável em recuperara Ata na tabela de histórico do Envio de dados para o PNCP
     */
    private function getArpPNCP(Arp $arp)
    {
        return EnviaDadosPncpHistorico::whereIn(
            'status_code',
            PncpPostResponseService::$successCode
        )
        ->where('pncpable_id', $arp->id)
        ->where('pncpable_type', $arp->getMorphClass())
        ->get();
    }

    /**
     * Método responsável em recuperar o arquivo para enviar ao PNCP
     */
    private function recuperarArquivoAta(Arp $arp)
    {
        $idTipoArquivo = ArpArquivoTipo::where('nome', 'Ata de Registro de Preços')->first()->id;
        
        return ArpArquivo::where('arp_id', $arp->id)
        ->where('tipo_id', $idTipoArquivo)
        ->first();
    }

    /**
     * Método responsável em inserir o arquivo no PNCP
     */
    private function publicarArquivoPNCP(
        Arp $arp,
        PncpManagerService $pncpManagerService,
        InserirArquivoArpPncpService $inserirArquivoArpPncpService,
        PncpPostResponseService $pncpPostResponseService
    ) {
        # Recupera o arquivo para enviar para o PNCP
        $arquivo = $this->recuperarArquivoAta($arp);

        # Serviço responsável em enviar o arquivo
        $pncpManagerService->managePncp($inserirArquivoArpPncpService, $pncpPostResponseService, $arquivo);
    }
}
