<?php

namespace App\Http\Traits\Pncp;

//requests
use App\Models\EnviaDadosPncpHistorico;
use App\Services\Pncp\Arp\InsertArpPncpService;
use App\Services\Pncp\Arp\RetificarArpPncpService;
use App\Services\Pncp\Arp\InserirArquivoArpPncpService;
use App\Services\Pncp\Arp\RemoverArquivoArpPncpService;

//responses
use App\Services\Pncp\Responses\PncpPostResponseService;
use App\Services\Pncp\Responses\PncpPutResponseService;
use App\Services\Pncp\Responses\PncpDeleteResponseService;

trait CommonPncpTrait
{

    public function resolveRequestPncpByLogName(string $situacao)
    {
        switch ($situacao) {
            case 'inserir_ata':
                return new InsertArpPncpService();
            case 'retificar_ata':
                return new RetificarArpPncpService();
            case 'inserir_arquivo_ata':
                return new InserirArquivoArpPncpService();
            case 'deletar_arquivo_ata':
                return new RemoverArquivoArpPncpService();
        }
    }

    public function resolveResponsePncpByLogName(string $situacao)
    {
        switch ($situacao) {
            case 'inserir_ata':
                return new PncpPostResponseService();
            case 'retificar_ata':
                return new PncpPutResponseService();
            case 'inserir_arquivo_ata':
                return new PncpPostResponseService();
            case 'deletar_arquivo_ata':
                return new PncpDeleteResponseService();
        }

        return new PncpPostResponseService();
    }

    /**
     * Verifica se um Instrumento existe no pncp
     *
     * Retorna true se o elemento existir
     *
     * @param $instrumento
     * @return boolean
     */
    public function checkIfInstrumentoAlreadyExistsInPncp($instrumento)
    {
        $alreadyExistsInPncp = EnviaDadosPncpHistorico::whereIn('status_code', PncpPostResponseService::$successCode)
            ->where('pncpable_id', $instrumento->id)
            ->where('pncpable_type', $instrumento->getMorphClass())
            ->get();

        return !$alreadyExistsInPncp->isEmpty();
    }
}
