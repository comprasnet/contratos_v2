<?php

namespace App\Http\Traits\Pncp;

use App\Http\Traits\ExternalServices;
use App\Models\PncpUsuario;
use App\Services\Pncp\PncpService;
use Illuminate\Http\Response;

trait PncpLogin
{
    use ExternalServices;

    private $pncpUserId;

    public function getPncpUserid()
    {
        if (!isset($this->pncpUserId)) {
            $this->pncpUserId = PncpService::getUsuarioPncp()->id_pncp;
        }
        return $this->pncpUserId;
    }

    public function getToken()
    {
        $usuarioPncp = PncpUsuario::query()
            ->whereNotNull('expires')
            ->where('expires', '>=', date('Y-m-d H:i:s'))
            ->first();

        if (is_null($usuarioPncp)) {
            return $this->login();
        }
        return $usuarioPncp->token;
    }

    public function login()
    {
        $usuarioPncp = PncpService::getUsuarioPncp();

        $response = $this->makeRequest(
            'POST',
            config('api.pncp.base_uri'),
            'v1/usuarios/login',
            null,
            ['Content-Type' => 'application/json'],
            json_encode(
                ['login' => $usuarioPncp->login, 'senha' => $usuarioPncp->senha]
            ),
            []
        );
        if ($response->getStatusCode() === Response::HTTP_OK) {
            PncpService::setTokenAndExpires(
                $response->getHeader('authorization')[0],
                $response->getHeader('expires')[0]
            );
        }
        $usuarioPncp = PncpService::getUsuarioPncp();
        return $usuarioPncp->token;
    }

    public function getAuthHeader()
    {

        return $header = [
            'Content-Type' => 'application/json',
            'Authorization' => $this->getToken()
        ];
    }

    /**
     * @param $method
     * @param $endpoint
     * @param $data
     * @return \GuzzleHttp\Psr7\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function makeRequestAuthenticated($method, $endpoint, $data = null)
    {
        return $this->makeRequest(
            $method,
            config('api.pncp.base_uri'),
            $endpoint,
            null,
            $this->getAuthHeader(),
            is_null($data) ? null : json_encode($data),
            []
        );
    }
}
