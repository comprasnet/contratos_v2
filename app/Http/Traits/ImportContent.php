<?php

namespace App\Http\Traits;

use Backpack\CRUD\app\Library\Widget;
use Illuminate\Support\Facades\Log;

trait ImportContent
{
    /**
     * Método responsável em importar o javascript para a tela do usuário
     */
    public function importarScriptJs(array $arquivos)
    {
        $timestamp = microtime();
        $timestamp = str_replace(" ", "_", $timestamp);
        
        # Percorre o array de arquivos que serão incluídos na página
        foreach ($arquivos as $arquivo) {
            $arquivo .= "?time={$timestamp}";
            Widget::add()->type('script')->content($arquivo);
        }
    }

    /**
     * Método responsável em importar o css para a tela do usuário
     */
    public function importarScriptCss(array $arquivos)
    {
        # Percorre o array de arquivos que serão incluídos na página
        foreach ($arquivos as $arquivo) {
            Widget::add()->type('style')->content($arquivo)->as('style');
        }
    }

    /**
     * Método responsável em importar todos os arquivos para trabalhar com o
     * datatable na tela do usuário
     */
    public function importarDatatableForm()
    {
        # Arquivos JS necessários
        $arquivoJS = [  'packages/datatables.net/js/jquery.dataTables.min.js',
                        'packages/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
                        'packages/datatables.net-responsive/js/dataTables.responsive.min.js',
                        'packages/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js',
                        'packages/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
                        'packages/datatables.net-fixedheader-bs4/js/fixedHeader.bootstrap4.min.js'
                    ];

        $this->importarScriptJs($arquivoJS);

        # Arquivos CSS necessários
        $arquivoCss = [  'packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
                        'packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css',
                        'packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css'
                    ];

        $this->importarScriptCss($arquivoCss);
    }

    /**
     * Método responsável em importar todos os arquivos para trabalhar com o
     * dashboard na tela do usuário fornecedor
     */
    public function importarDashboardWidget($caminhoJsDasboardWidget)
    {
        $arquivoJS = [
            'assets/js/blockui/jquery.blockUI.js',
            'packages/laravel-paginate/laravel-paginate.js',
            $caminhoJsDasboardWidget
        ];

        $this->importarScriptJs($arquivoJS);

        $this->importarScriptCss([
            'packages/backpack/base/css/dashboard/dashboard.css'
        ]);
    }
}
