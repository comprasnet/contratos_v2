<?php

namespace App\Http\Traits;

use Carbon\Carbon;

trait NumeroTrait
{
    public function getProximoNumero(string $model, int $contrato_id)
    {
        $ano = Carbon::now()->format('Y');
        $ultimoNumero = $model::withTrashed()
            ->where('contrato_id', $contrato_id)
            ->where('numero', 'like', "%/$ano")
            ->orderBy('numero', 'desc')
            ->first('numero');

        $result = "00001/$ano";
        if ($ultimoNumero) {
            $numero = explode('/', $ultimoNumero->numero);
            $nextNumber = str_pad($numero[0] + 1, 5, '0', STR_PAD_LEFT);

            $result = $nextNumber . '/' . $numero[1];
        }

        return $result;
    }
}
