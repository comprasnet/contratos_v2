<?php

namespace App\Http\Traits;

trait DateFormat
{
    public function checkDateFormat($date, $format = 'Y-m-d')
    {
        $dateTime = \DateTime::createFromFormat($format, $date);
        return $dateTime && $dateTime->format($format) === $date;
    }

    public function dateToDb($date)
    {
        if ($this->checkDateFormat($date, 'd/m/Y')) {
            $dateTime = \DateTime::createFromFormat('d/m/Y', $date);
            $date = $dateTime->format('Y-m-d');
        }
        return $date;
    }
}
