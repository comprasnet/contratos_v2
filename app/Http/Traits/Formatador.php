<?php

namespace App\Http\Traits;

use App\Models\Feriado;
use App\Models\User;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use DateTime;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redis;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Log;

trait Formatador
{
    /**
     * Retorna $campo data formatado no padrão pt-Br: dd/mm/yyyy
     *
     * @param $campo
     * @return string
     * @author Anderson Sathler <asathler@gmail.com>
     */
    public function retornaDataAPartirDeCampo($campo, $formatoOrigem = 'Y-m-d', $formatoDestino = 'd/m/Y')
    {
        if (is_null($campo)) {
            return '';
        }

        try {
            $data = DateTime::createFromFormat($formatoOrigem, $campo);
            $retorno = ($data !== false) ? $data->format($formatoDestino) : '';
        } catch (Exception $e) {
            $retorno = '';
        }

        return $retorno;
    }

    /**
     * Retorna $campo numérico formatado no padrão pt-Br: 0.000,00, incluindo ou não 'R$ ' segundo $prefix
     *
     * @param $campo
     * @return string
     * @author Anderson Sathler <asathler@gmail.com>
     */
    public function retornaCampoFormatadoComoNumero($campo, $prefix = false, $decimal = 2)
    {
        try {
            $numero = number_format($campo, $decimal, ',', '.');
            $numeroPrefixado = ($prefix === true ? 'R$ ' : '') . $numero;
            $retorno = ($campo < 0) ? "($numeroPrefixado)" : $numeroPrefixado;
        } catch (Exception $e) {
            $retorno = '';
        }

        return $retorno;
    }

    public function formataProcesso($mask, $str)
    {
        $str = str_replace(" ", "", $str);

        for ($i = 0; $i < strlen($str); $i++) {
            $mask[strpos($mask, "#")] = $str[$i];
        }

        return $mask;
    }

    public function formataDataSiasg($data)
    {
        return $this->retornaDataAPartirDeCampo($data, 'Ymd', 'Y-m-d');
    }

    public function formataDecimalSiasg($dado)
    {
        return number_format($dado, 2, '.', '');
    }

    public function formataIntengerSiasg($dado)
    {
        return number_format($dado, 0, '', '');
    }

    public function formataNumeroContratoLicitacao($dado): string
    {
        $d[0] = substr($dado, 0, 5);
        $d[1] = substr($dado, 5, 4);

        return $d[0] . '/' . $d[1];
    }

    public function formataCnpjCpf($dado)
    {
        $retorno = $dado;
        $tipo = $this->retornaTipoFornecedor($dado);

        if ($tipo == 'JURIDICA') {
            $retorno = $this->formataCnpj($dado);
        }

        if ($tipo == 'FISICA') {
            $retorno = $this->formataCpf($dado);
        }

        return $retorno;
    }

    public function retornaTipoFornecedor($dado)
    {

        if (strlen($dado) == 9 || $dado === 'ESTRANGEIRO' || ctype_alpha(substr($dado, 0, 2))) {
            return 'IDGENERICO';
        }

        if (strlen($dado) == 11) {
            return 'FISICA';
        }

        if (strlen($dado) == 14 || !ctype_alpha(substr($dado, 0, 2))) {
            return 'JURIDICA';
        }
        return 'UG';
    }

    public function formataCnpj($numero)
    {
        $d[0] = substr($numero, 0, 2);
        $d[1] = substr($numero, 2, 3);
        $d[2] = substr($numero, 5, 3);
        $d[3] = substr($numero, 8, 4);
        $d[4] = substr($numero, 12, 2);

        return $d[0] . '.' . $d[1] . '.' . $d[2] . '/' . $d[3] . '-' . $d[4];
    }

    public function formataCpf($numero)
    {
        if (strlen($numero) < 11) {
            $numero = str_pad($numero, 11, "0", STR_PAD_LEFT);
        }

        $d[0] = substr($numero, 0, 3);
        $d[1] = substr($numero, 3, 3);
        $d[2] = substr($numero, 6, 3);
        $d[3] = substr($numero, 9, 2);

        return $d[0] . '.' . $d[1] . '.' . $d[2] . '-' . $d[3];
    }

    public function retornaMascaraCpf($cpf)
    {
        return '***' . substr($cpf, 3, 9) . '**';
    }

    public function retornaFormatoAmericano($valor)
    {
        return str_replace(',', '.', str_replace('.', '', $valor));
    }

    /**
     * Retorna campo com a descricao detalhada para visualização na tabela
     * @param string $descricao
     * @param string $descricaocompleta
     * @return string
     */
    public function retornaDescricaoDetalhada(string $descricao = null, string $descricaocompleta = null): string
    {
        if ($descricao == null) {
            return '';
        }

        $retorno = $descricao . ' <i class="fa fa-info-circle" title="' . $descricaocompleta . '"></i>';

        return $retorno;
    }

    public function removeMascaraCPF($cpfComMask)
    {
        $cpf = str_replace('.', '', $cpfComMask);
        $cpf = str_replace('-', '', $cpf);
        return $cpf;
    }


    public function verificaDataDiaUtil($data)
    {
        $hoje = date('Y-m-d');
        $data_publicacao = Carbon::createFromFormat('Y-m-d', $data);
        $proximoDiaUtil = $data_publicacao;
        $hoje_ate18hs = Carbon::createFromFormat('Y-m-d', $hoje)->setTime(18, 00, 00);
        $hoje_pos18hs = Carbon::createFromFormat('Y-m-d', $hoje)->setTime(23, 59, 59);
        $feriados = Feriado::select('data')->pluck('data')->toArray();

        if ($data_publicacao->lessThanOrEqualTo($hoje_ate18hs)) {
            $proximoDiaUtil = $data_publicacao->nextWeekday();
            (in_array($proximoDiaUtil->toDateString(), $feriados)) ? $proximoDiaUtil->nextWeekday() : '';
        }

        if ($data_publicacao->isAfter($hoje_ate18hs) && $data_publicacao->lessThan($hoje_pos18hs)) {
            $proximoDiaUtil = $data_publicacao->nextWeekday()->addWeekday();
            (in_array($proximoDiaUtil->toDateString(), $feriados)) ? $proximoDiaUtil->nextWeekday() : '';
        }

        if ($data_publicacao->isAfter($hoje_pos18hs)) {
            $proximoDiaUtil = (!$data_publicacao->isWeekday()) ? $data_publicacao->nextWeekday() : $proximoDiaUtil;
            (in_array($proximoDiaUtil->toDateString(), $feriados)) ? $proximoDiaUtil->nextWeekday() : '';
        }

        return ($proximoDiaUtil->toDateString());
    }

    public function validaCPF($cpf)
    {

        // Extrai somente os números
        $cpf = preg_replace('/[^0-9]/is', '', $cpf);

        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }

        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }

        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return false;
            }
        }
        return true;
    }

    public function retornaSomenteNumeros($value)
    {
        return preg_replace("/\D/", '', $value);
    }

    public function exibirTituloPaginaMenu(string $singular, ?string $plural = '', bool $exibirAcao = true)
    {

        if (empty($plural)) {
            $plural  = $singular;
        }

        CRUD::setEntityNameStrings($singular, $plural);
        if ($exibirAcao) {
            CRUD::setSubheading('Criar', 'create');
            CRUD::setSubheading('Editar', 'edit');
            CRUD::setSubheading('Visualizar', 'index');
        }
    }

    public function bloquearBotaoPadrao(CrudPanel $crud, ?array $botoes)
    {
        if (empty($botoes)) {
            return;
        }

        foreach ($botoes as $botao) {
            $crud->denyAccess($botao);
        }
    }

    public function montarRespostaAlertJs(int $codigo, $mensagem, $type)
    {
        $jsonString = '{"code":' . $codigo . ',"message":"' . trim($mensagem) . '", "type":"' . $type . '"}';
        return json_decode($jsonString);
    }

    public function formatarDataBanco(array $colunas, array $input)
    {
        foreach ($colunas as $coluna) {
            $arrayData = explode('/', $input[$coluna]);
            if (count($arrayData) == 1) {
                $input[$coluna] = null;
            } else {
                $input[$coluna] = "{$arrayData[2]}-{$arrayData[1]}-{$arrayData[0]}";
            }
        }
        return $input;
    }



    public function salvarDadosUsuarioRedis(array $dadosUsuario)
    {
        $keyUsuarioRedis = "user_{$dadosUsuario['id']}";
        $dadosUsuario['sessionVersaoDois'] = session()->getId();

        $dadosUsuario = json_encode($dadosUsuario);

        Redis::set($keyUsuarioRedis, $dadosUsuario);
    }

    public function recuperarDadosUsuarioRedis(int $idUsuario)
    {
        $keyUsuarioRedis = "user_{$idUsuario}";
        $dadosUsuarioRedis = Redis::get($keyUsuarioRedis);

        return json_decode($dadosUsuarioRedis, true);
    }

    public static function recuperarDadosUsuarioRedisStatic(int $idUsuario)
    {
        $keyUsuarioRedis = "user_{$idUsuario}";
        $dadosUsuarioRedis = Redis::get($keyUsuarioRedis);

        return json_decode($dadosUsuarioRedis, true);
    }

    public function removerDadosUsuarioRedis(int $idUsuario)
    {
        $keyUsuarioRedis = "user_{$idUsuario}";

        $dadosUsuarioRedis = $this->recuperarDadosUsuarioRedis($idUsuario);
        if (isset($dadosUsuarioRedis['sessionVersaoUm'])) {
            $prefix = config('cache.prefix');
            $nomeKeyRedis = "{$prefix}:{$dadosUsuarioRedis['sessionVersaoUm']}";

            Redis::del($nomeKeyRedis);
        }

        Redis::del($keyUsuarioRedis);
    }

    public function montarDadosUsuarioBanco(int $idUsuario)
    {

        $dadosUsuario = User::join("unidades", "users.ugprimaria", "=", "unidades.id")
            ->where("users.id", $idUsuario)
            ->select("unidades.codigo AS user_ug", "users.name")
            ->limit(1)
            ->get();


        return $dadosUsuario->toArray()[0];
    }

    public static function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function convertDateGovToBd(?string $date)
    {
        if (empty($date)) {
            return $date;
        }

        $arrayData = explode('/', $date);
        return "{$arrayData[2]}-$arrayData[1]-$arrayData[0]";
    }

    public function formataRealParaBanco($valor)
    {
        // Remover símbolo de moeda e espaços em branco
        $valorSemSimbolo = str_replace(['R$', ' '], '', $valor);

        // Remover separadores de milhares
        $valorSemSeparadorMilhar = str_replace('.', '', $valorSemSimbolo);

        // Substituir a vírgula decimal por um ponto decimal
        $valorFormatado = str_replace(',', '.', $valorSemSeparadorMilhar);

        // Converter a string formatada em um valor numérico (float)
        $valorNumerico = floatval($valorFormatado);

        return $valorNumerico;
    }

    public function preencherCasasDireitaEsquerda(
        $string,
        int $quantidadeCasa = 2,
        string $caracter = '0',
        int $ladoPrenchimento = STR_PAD_LEFT
    ) {
        return str_pad($string, $quantidadeCasa, $caracter, $ladoPrenchimento);
    }

    /**
     * Método responsável em salvar a query no arquivo de LOG
     */
    public function debugarSQLArquivoLOG($query, bool $dd = false)
    {
        # Recupera o SQL executado na tela
        $sql = $query->toSql();

        # Recupera todos os parâmetros informados
        $bindings = $query->getBindings();

        # Substitui a interrogação pelos parâmetros informados conforme a ordem do array
        $sql_with_bindings = preg_replace_callback('/\?/', function ($match) use ($sql, &$bindings) {
            return json_encode(array_shift($bindings), JSON_UNESCAPED_UNICODE);
        }, $sql);

        # Se for exibir em tela, passar o parâmetro dd como true
        if ($dd) {
            dd($sql_with_bindings);
        }

        # Insere o SQL dentro do arquivo
        Log::channel('log_sql')->info($sql_with_bindings);
    }

    /**
     * Método responsável em converter as iniciais dos tipos das gerenciadoras
     * para o nome completo quando recuperado da tabela compra_item_unidade
     * na coluna tipo_uasg
     */
    public function tipoUasgConvertido(?string $tipoUasg)
    {
        switch ($tipoUasg) {
            case 'G':
                return 'Gerenciadora';
                break;
            case 'P':
                return 'Participante';
                break;
            case 'C':
                return 'Carona';
                break;
        }
    }

    /**
     * Método responsável em formatar o tamanho do arquivo
     */
    public static function formatBytes($size, $precision = 2)
    {
        if ($size > 0) {
            $size = (int) $size;
            $base = log($size) / log(1024);
            $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');

            return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
        } else {
            return $size;
        }
    }

    /**
     * Método responsável em formatar a data no padrão brasileiro
     */
    public static function formatDatePtBR(?string $date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }

    /**
     * Método responsável em formatar o valor para o padrão brasileiro
     */
    public static function formatValuePtBR(float $valor, int $precisao = 2)
    {
        return number_format($valor, $precisao, ',', '.');
    }

    /**
     * Método responsável em formatar quantidades e valores com casas decimais
     */
    public static function formatQuantidade(float $quantidade, int $precisao = 5)
    {
        return number_format($quantidade, $precisao, ',', '.');
    }

    /**
     * Método responsável em exibir a cor do status conforme a descrição
     */
    public static function exibirCorStatus(string $descricao)
    {
        $textoStatus = '';
        $textoStatus = $descricao;
        switch ($descricao) {
            case 'Aceitar':
                $corStatus = 'bg-success';
                $textoStatus = 'Aceito';
                break;
            case 'Negar':
                $corStatus = 'bg-danger';
                $textoStatus = 'Negado';
                break;
            case 'Aceitar Parcialmente':
                $corStatus = 'bg-warning';
                $textoStatus = 'Aceito Parcialmente';
                break;

            case 'Aceitar Parcialmente':
                $corStatus = 'bg-black';
                $textoStatus = 'Aceito Parcialmente';
                break;

            default:
                $corStatus = 'bg-black';
                break;
        }

        return "<div class='align-items-center' style='text-align: center'>
                    <span class='br-tag status {$corStatus} large'></span>
                </div>
                <div class='br-tooltip' role='tooltip' info='info' place='left'>
                <span class='subtext'>{$textoStatus}</span></div>";
    }

    /**
     * Pode receber um array de valores ou um valor único
     * Verifica se o valor é fracionado ou não (se contém casas decimais ou é inteiro)
     * Este método considera que um valor como 12.000 é inteiro
     *
     * @param mixed $valor Pode ser um valor qualquer ou array
     * @return bool Se tiver pelo menos um valor que seja fracionado, retorna true
     */
    public function temValorFracionado($valor)
    {
        if (is_array($valor)) {
            foreach ($valor as $item) {
                if ($this->temValorFracionado($item)) {
                    return true;
                }
            }
        } else {
            if (!(is_int($valor) || ((int)$valor == $valor))) {
                return true;
            }
        }

        return false;
    }

    public function valorPorExtenso($valor = 0, bool $monetario = true, bool $feminino = false)
    {
        if ($monetario) {
            $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
            $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões", "quatrilhões");
        } else {
            $singular = array("", "", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
            $plural = array("", "", "mil", "milhões", "bilhões", "trilhões", "quatrilhões");
        }

        $c = array("", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos",
            "oitocentos", "novecentos");
        $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa");
        $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito",
            "dezenove");
        $u = array("", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");


        if ($feminino) {
            if ($valor == 1) {
                $u = array("", "uma", "duas", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");
            } else {
                $u = array("", "um", "duas", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");
            }


            $c = array("", "cem", "duzentas", "trezentas", "quatrocentas", "quinhentas", "seiscentas",
                "setecentas", "oitocentas", "novecentas");
        }


        $z = 0;

        $valor = number_format($valor, 2, ".", ".");
        $inteiro = explode(".", $valor);

        for ($i = 0; $i < count($inteiro); $i++) {
            for ($ii = mb_strlen($inteiro[$i]); $ii < 3; $ii++) {
                $inteiro[$i] = "0" . $inteiro[$i];
            }
        }

        // $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
        $rt = null;
        $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
        for ($i = 0; $i < count($inteiro); $i++) {
            $valor = $inteiro[$i];
            $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
            $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
            $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

            $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd && $ru) ? " e " : "") . $ru;
            $t = count($inteiro) - 1 - $i;
            $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
            if ($valor == "000") {
                $z++;
            } elseif ($z > 0) {
                $z--;
            }

            if (($t == 1) && ($z > 0) && ($inteiro[0] > 0)) {
                $r .= (($z > 1) ? " de " : "") . $plural[$t];
            }

            if ($r) {
                $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ?
                        (($i < $fim) ? ", " : " e ") : " ") . $r;
            }
        }

        $rt = mb_substr($rt, 1);

        return ($rt ? trim($rt) : "zero");
    }

    /**
     * Formata um número de CPF.
     *
     * @param string $numero O número de CPF a ser formatado.
     * @return string Retorna o número de CPF formatado no padrão ###.###.###-##.
     */
    public function formataCpf2($numero): string
    {
        // Remove qualquer caractere não numérico
        $numero = preg_replace('/[^0-9]/', '', $numero);

        if (strlen($numero) < 11) {
            $numero = str_pad($numero, 11, "0", STR_PAD_LEFT);
        }

        $d[0] = substr($numero, 0, 3);
        $d[1] = substr($numero, 3, 3);
        $d[2] = substr($numero, 6, 3);
        $d[3] = substr($numero, 9, 2);

        return $d[0] . '.' . $d[1] . '.' . $d[2] . '-' . $d[3];
    }

    /**
     * Retorna o tipo de fornecedor com base no código fornecido pela Área de Trabalho do Compras.
     *
     * @param string $tipo O código do tipo de fornecedor (F, J, E).
     * @return string O tipo de fornecedor correspondente.
     */
    public function retornaTipoFornecedorAcessoComprasGov(String $tipo): string
    {
        switch (strtoupper($tipo)) {
            case 'F':
                return 'FISICA';
            case 'J':
                return 'JURIDICA';
            case 'E':
                return 'IDGENERICO';
            default:
                return 'UG';
        }
    }

    /**
     * Retorna o CPF, CNPJ ou identificador genérico do fornecedor com base
     * no tipo fornecido pela Área de Trabalho do Compras.
     *
     * @param string $tipo O tipo do fornecedor (F, J, UG, E).
     * @param string $identificadorFornecedor O identificador do fornecedor.
     * @return string O CPF, CNPJ ou identificador genérico formatado conforme as regras.
     */
    public function retornaCpfCnpjFornecedorAcessoComprasGov(String $tipo, String $identificadorFornecedor): string
    {
        switch (strtoupper($tipo)) {
            case 'F':
                return $this->formataCpf2($identificadorFornecedor);
            case 'J':
                return $this->formataCnpj($identificadorFornecedor);
            case 'UG':
            case 'E':
                return $identificadorFornecedor;
            default:
                return 'TIPO DESCONHECIDO';
        }
    }

    /**
     * Retorna o CPF, CNPJ ou identificador genérico do usuário fornecedor com base no tipo
     * fornecido pela Área de Trabalho do Compras.
     * @param string $tipo O tipo do fornecedor (F, J, E).
     * @param string $identificadorUsuario O identificador do usuario(cpf ou estrang).
     * @return string O CPF formatado do usuario (J e F) ou identificador do estrangeiro.
     */
    public function retornaCpfCnpjUsuarioFornecedorAcessoComprasGov(String $tipo, String $identificadorUsuario): string
    {
        switch (strtoupper($tipo)) {
            case 'F':
            case 'J':
                return $this->formataCpf2($identificadorUsuario);
            case 'UG':
            case 'E':
                return $identificadorUsuario;
            default:
                return 'TIPO DESCONHECIDO';
        }
    }

    public function colors(int $quantidade)
    {
        $colors = [];
        for ($i = 0; $i < $quantidade; $i++) {
            $r = number_format(rand(0, 255), 0, '', '');
            $g = number_format(rand(0, 255), 0, '', '');
            $b = number_format(rand(0, 255), 0, '', '');

            $colors[] = "rgba(" . $r . "," . $g . "," . $b . ", 0.5)";
        }
        return $colors;
    }
}
