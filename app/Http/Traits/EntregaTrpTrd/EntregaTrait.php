<?php

namespace App\Http\Traits\EntregaTrpTrd;

use App\Http\Traits\Formatador;
use App\Models\CodigoItem;
use App\Models\Contrato;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

trait EntregaTrait
{
    use HasFactory;
    use CrudTrait;
    use SoftDeletes;
    use Formatador;

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function situacao()
    {
        return $this->belongsTo(CodigoItem::class, 'situacao_id');
    }

    public function contrato()
    {
        return $this->belongsTo(Contrato::class);
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getSituacao()
    {
        return $this->situacao->descricao;
    }

    public function getValorTotalFormatado()
    {
        return $this->retornaCampoFormatadoComoNumero($this->valor_total, true);
    }
}
