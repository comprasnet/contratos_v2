<?php

namespace App\Http\Traits\EntregaTrpTrd;

trait EntregaTrpTrdValidationTrait
{
    public function getDefaultRules()
    {
        return [
            'rascunho' => 'required|boolean',

            'vinculos' => 'required_if:rascunho,0|nullable|array',
            'vinculos.*.id' => 'required_if:rascunho,0|nullable|integer',
            'vinculos.*.itens' => 'required_if:rascunho,0|nullable|array',
            'vinculos.*.itens.*.id' => 'required_if:rascunho,0|nullable|integer|min:1',
            'vinculos.*.itens.*.quantidade_informada' => 'required_if:rascunho,0|nullable|numeric|gt:0',
            'vinculos.*.itens.*.valor_glosa' => 'nullable|numeric',

            'valor_total' => 'required_if:rascunho,0|nullable|numeric|min:0',
            'informacoes_complementares' => 'nullable|string',
        ];
    }

    public function getDefaultAttributes()
    {
        return [
            'introducao' => 'Introdução',
            'informacoes_complementares' => 'Informações Complementares',
            'vinculos' => 'Vínculo',
            'vinculos.*.itens' => 'Item',
            'vinculos.*.itens.*.id' => 'Item',
            'vinculos.*.itens.*.quantidade_informada' => 'Quantidade Informada na Entrega',
            'vinculos.*.itens.*.valor_glosa' => 'Glosa',
            'valor_total' => 'Valor Total',
            'locais_execucao' => 'Local de Execução da Entrega',
            'mes_ano_referencia' => 'Mês/Ano de Referência',
            'data_inicio' => 'Período ínicio da entrega',
            'data_fim' => 'Período fim da entrega',
            'data_entrega' => 'Data efetiva da entrega',
            'data_prevista_trp' => 'Data prevista para o recebimento provisório',
            'data_prevista_trd' => 'Data prevista para o recebimento definitivo',

            'incluir_instrumento_cobranca' => 'Incluir Instrumento de Cobrança?',
        ];
    }

    public function getDefaultMessages()
    {
        return [
            'introducao.required_if' => 'O campo :attribute é obrigatório.',
            'vinculos.required' => 'Adicione ao menos uma :attribute.',
            'vinculos.required_if' => 'Adicione ao menos uma :attribute.',
            'vinculos.*.mes_ano_referencia.required_if' => 'O campo :attribute é obrigatório.',
            'vinculos.*.itens.required_if' => 'Adicione ao menos um :attribute de cada Entrega',
            'vinculos.*.itens.*.required_if' => 'Adicione ao menos um :attribute de cada Entrega',
            'vinculos.*.itens.*.id.required_if'=> 'Selecione ao menos um :attribute.',
            'vinculos.*.itens.*.quantidade_informada.required_if' => 'O campo :attribute é obrigatório.',
            'vinculos.*.itens.*.quantidade_disponivel_consumo.gte' => 'A :attribute deve ser maior ou igual a zero.',
            'vinculos.*.itens.*.valor_glosa.required_if' => 'O campo :attribute é obrigatório.',
            'vinculos.*.itens.*.data_inicio.before_or_equal' => 'A data de inicio deve ser menor ou igual a data de 
                fim.',
            'locais_execucao.required_if' => 'O campo :attribute é obrigatório.',
            'mes_ano_referencia.required_if' => 'O campo :attribute é obrigatório.',
            'data_inicio.required_if' => 'O campo :attribute é obrigatório.',
            'data_fim.required_if' => 'O campo :attribute é obrigatório.',
            'data_entrega.required_if' => 'O campo :attribute é obrigatório.',
            'data_prevista_trp.required_if' => 'O campo :attribute é obrigatório.',
            'data_prevista_trd.required_if' => 'O campo :attribute é obrigatório.',
            'valor_total.required_if' => 'O campo :attribute é obrigatório.',
            'informacoes_complementares.required_if' => 'O campo :attribute é obrigatório.',
            'incluir_instrumento_cobranca.required_if' => 'O campo :attribute e obrigatorio.',
        ];
    }
}
