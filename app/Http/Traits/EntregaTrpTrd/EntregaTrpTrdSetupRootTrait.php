<?php

namespace App\Http\Traits\EntregaTrpTrd;

use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoCrudTrait;
use App\Http\Traits\CommonColumns;
use App\Models\Entrega;
use App\Models\TermoRecebimentoDefinitivo;
use App\Models\TermoRecebimentoProvisorio;
use App\Repositories\Contrato\ContratoResponsavelRepository;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

trait EntregaTrpTrdSetupRootTrait
{
    use AutorizacaoexecucaoCrudTrait;
    use CommonColumns;
    use UpdateOperation;
    use DeleteOperation;

    private $modelName;

    public function setupRoot(string $model)
    {
        $this->setModelName($model);

        $this->contrato = $this->getContrato();
        $autorizacaoExecucaoService = new AutorizacaoExecucaoService();
        $autorizacaoExecucaoService->setContrato($this->contrato);
        $this->fields = $autorizacaoExecucaoService->getFieldLabels();

        CRUD::setModel($model);
        CRUD::addClause('where', 'contrato_id', '=', $this->contrato->id);

        $contratoResponsavelRepository = new ContratoResponsavelRepository($this->contrato->id);
        $this->crud->isUsuarioResponsavelContrato = $contratoResponsavelRepository->isUsuarioResponsavel();
        abort_unless($this->crud->isUsuarioResponsavelContrato, 403, 'Não possui permissão');
    }

    protected function setupListOperationRoot()
    {
        $this->cabecalho();

        $this->crud->addFilter(
            [
                'type'  => 'text',
                'name'  => 'numero',
                'label' => 'Número',
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'numero', 'LIKE', "%$value%");
            }
        );

        $this->crud->addColumn([
            'name' => 'situacao',
            'label' => 'Situação',
            'type' => 'model_function',
            'function_name' => 'getSituacaoChipComponent',
            'limit' => 9999,
            'priority' => 1,
        ]);

        $this->crud->addColumn([
            'name' => 'numero',
            'type' => 'text',
            'label' => 'Número',
        ]);

        $this->crud->addColumn([
            'name' => 'osfs',
            'type' => 'model_function',
            'label' => 'OS/Fs',
            'function_name' => 'getRedirectAutorizacoesComponent',
            'limit' => 99999999999999999,
        ]);

        $this->crud->addColumn([
            'name' => 'valor_total',
            'label' => 'Valor Total',
            'type' => 'model_function',
            'function_name' => 'getValorTotalFormatado',
        ]);

        $this->crud->enableExportButtons();

        $this->breadcrumb();
    }

    protected function setupCreateOperationRoot()
    {
        $this->importarDatatableForm();

        $this->crud->setCreateContentClass('col-md-12');

        $this->cabecalho();
    }

    protected function breadcrumb(bool $acao = false, $texto = 'Adicionar'): void
    {
        $breadcrumb = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
            'Ordens de Serviço / Fornecimento do Contrato ' . $this->contrato->numero =>
                backpack_url('autorizacaoexecucao/' . $this->contrato->id),
            $this->modelName => false,
            'Lista' => false,
        ];

        if ($acao) {
            $breadcrumb = [
                trans('backpack::crud.admin') => backpack_url('dashboard'),
                'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
                'Ordens de Serviço / Fornecimento do Contrato ' . $this->contrato->numero =>
                    backpack_url('autorizacaoexecucao/' . $this->contrato->id),
                $this->modelName => $this->crud->getRoute(),
                $texto => false,
            ];
        }

        $this->data['breadcrumbs'] = $breadcrumb;
    }

    private function setModelName(string $model)
    {
        if ($model == Entrega::class) {
            $this->modelName = 'Entregas';
        } elseif ($model == TermoRecebimentoProvisorio::class) {
            $this->modelName = 'Termos de Recebimento Provisório';
        } elseif ($model == TermoRecebimentoDefinitivo::class) {
            $this->modelName = 'Termos de Recebimento Definitivo';
        }
    }
}
