<?php

namespace App\Http\Traits\EntregaTrpTrd;

use App\Models\ArquivoGenerico;
use App\Models\CodigoItem;

trait TrpTrdTrait
{
    use EntregaTrait;

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function arquivo()
    {
        return $this->morphOne(ArquivoGenerico::class, 'arquivoable');
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getTableItensView()
    {
        $itens = $this->autorizacaoItens()->with('autorizacaoexecucao')->get();
        return view('crud::autorizacaoexecucao.vinculo.table-vinculo-itens', [
            'itens' => $itens,
        ])->render();
    }

    public function getArquivoViewLink()
    {
        $result = $this->getArquivo();

        return $result ? $result->getViewLink() : null;
    }

    public function getArquivo()
    {
        $codigoItem = CodigoItem::where('descres', 'ae_arquivo_assinado')->first();
        $query = $this->arquivo()->where('tipo_id', $codigoItem->id);
        if ($query->exists()) {
            return $query->first();
        }

        return null;
    }
}
