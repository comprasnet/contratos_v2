<?php

namespace App\Http\Traits\UsuarioFornecedor;

use App\Models\BackpackUser;
use App\Models\Contrato;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Route;

trait UsuarioFornecedorTrait
{
    private function permissionRule(array $permissions = [])
    {
        if (empty(backpack_user())) {
            abort('403');
        }

        if (!empty(backpack_user())) {
            $arrayPermission = [];

            foreach ($permissions as $permission) {
                array_push($arrayPermission, backpack_user()->can($permission));
            }

            $valoresNaoFalsos = array_filter($arrayPermission, function ($valor) {
                return $valor !== false;
            });
            if (count($valoresNaoFalsos) == 0) {
                abort('403');
            }
        }
        //dd('não tá vazio', backpack_user()->roles);
    }

    public function verificarUsuarioTemApenasPerfilFornecedor()
    {
        $user = backpack_user();

        if ($user->roles->count() == 1 && $user->roles->first()->name == 'Fornecedor') {
            return true;
        }

        return false;
    }


    /*
     * Garante a lógica de logout dos usuários Fornecedor e Contratos
     * caso o usuário não deslogue pelo botão mas apenas feche o navegador
    */
    public function alterarAdministradorusuarioFornecedorLogout(BackpackUser $user)
    {
        if ($user->ugprimaria) {
            $user->acesso_compras_fornecedor = false;
            $user->save();
        }
    }

    public function bredcrumbs(
        bool $administradorFornec,
        string $textoFinal,
        string $textoUrlIntermediaria = null,
        string $urlItermediaria = null,
        bool $voltar = false
    ) {
        $urlPrincipal = '/fornecedor/inicio/administrador';

        if (!$administradorFornec) {
            $urlPrincipal = '/fornecedor/inicio/preposto';
        }

        $arrayBreadcrumb = [trans('backpack::crud.admin') => $urlPrincipal ];

        if (!empty($textoUrlIntermediaria)) {
            $arrayBreadcrumb[$textoUrlIntermediaria] = $urlItermediaria;
        }

        $arrayBreadcrumb[$textoFinal] = false;

        if ($voltar) {
            $arrayBreadcrumb['Voltar'] = $urlItermediaria;
        }


        $this->data['breadcrumbs'] = $arrayBreadcrumb;
    }

    public function adicionarQueryFiltroPreposto($administradorFornec): void
    {
        if (!$administradorFornec) {
            CRUD::addClause('join', 'contratopreposto', 'contratos.id', '=', 'contratopreposto.contrato_id');
            CRUD::addClause('where', 'contratopreposto.cpf', '=', backpack_user()->cpf);
            CRUD::addClause('where', 'contratopreposto.situacao', '=', true);
        }
    }

    public function adicionarQueryFiltroPrepostoForaDoSetup($administradorFornec, Builder $query)
    {
        if (!$administradorFornec) {
            $query->join('contratopreposto', 'contratos.id', '=', 'contratopreposto.contrato_id');
            $query->where('contratopreposto.cpf', '=', backpack_user()->cpf);
            $query->where('contratopreposto.situacao', '=', true);
        }
        return $query;
    }

    public function verificarPermissaoByOsf()
    {
        $osf_id = Route::current()->parameter('autorizacaoexecucao_id');
        $contrato = Contrato::join('autorizacaoexecucoes', 'autorizacaoexecucoes.contrato_id', 'contratos.id')
            ->where('autorizacaoexecucoes.id', '=', $osf_id)
            ->where('contratos.fornecedor_id', '=', session()->get('fornecedor_id'))
            ->where('contratos.elaboracao', 'false')->first();
        if (!$contrato) {
            abort('403', config('app.erro_permissao'));
        }
        return true;
    }

    public function verificarPermissaoByContrato()
    {
        $contrato_id = Route::current()->parameter('contrato_id');

        if ($contrato_id != null) {
            $contrato = Contrato::leftJoin('autorizacaoexecucoes', 'autorizacaoexecucoes.contrato_id', 'contratos.id')
                ->where('contratos.id', '=', $contrato_id)
                ->where('contratos.fornecedor_id', '=', session()->get('fornecedor_id'))
                ->where('contratos.elaboracao', 'false')
                ->first();
            if (!$contrato) {
                abort('403', config('app.erro_permissao'));
            }
        }
        return true;
    }

    public function verificarPermissaoByFornecedor()
    {
        $fornecedor_id = Route::current()->parameter('fornecedor_id');

        if ($fornecedor_id != null && $fornecedor_id != session()->get('fornecedor_id')) {
            abort('403', config('app.erro_permissao'));
        }
        return true;
    }

    public function verificarPermissaoShowByContrato($contrato_id = null)
    {
        $contrato_id = $contrato_id == null ? Route::current()->parameter('contrato_id') : $contrato_id;

        if ($contrato_id != null) {
            $contrato = Contrato::where('contratos.id', '=', $contrato_id)
                ->where('contratos.fornecedor_id', '=', session()->get('fornecedor_id'))
                ->where('contratos.elaboracao', 'false')->first();
            if (!$contrato) {
                abort('403', config('app.erro_permissao'));
            }
        }
        return true;
    }

    public static function exibirCorStatusIndicacaoPreposto($descres)
    {
        if ($descres) {
            $textoStatus = $descres;
            $corFont = '#fff';
            $corStatus = '';
            switch ($descres) {
                case 'sit_indica_prepost_2':
                    $corStatus = '#168821';
                    $textoStatus = 'Aceita';
                    break;
                case 'sit_indica_prepost_3':
                    $corStatus = '#e52207';
                    $textoStatus = 'Recusada';
                    break;
                case 'sit_indica_prepost_1':
                    $corStatus = '#f5a623';
                    $textoStatus = 'Pendente';
                    break;
                default:
                    break;
            }

            return view(
                'components_v2.chip_status',
                [
                    'conteudo' => $textoStatus,
                    'cor' => $corStatus,
                    'corFont' => $corFont
                ]
            );
        }
    }
}
