<?php

namespace App\Http\Traits;

use App\Models\AmparoLegal;
use App\Models\ArpItemHistoricoAlteracaoFornecedor;
use App\Models\Catmatseritem;
use App\Models\CompraItem;
use App\Models\CompraItemFornecedor;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\CompraItemUnidade;
use App\Models\Compras;
use App\Models\Fornecedor;
use App\Models\Unidade;
use App\Api\Externa\ApiSiasg;
use App\Models\Catmatclasse;
use App\Models\Catmatpdms;
use App\Models\Catmatsergrupo;
use App\Models\ArpItemHistorico;
use App\Models\CodigoItem;
use App\Models\Contratoitem;
use App\Repositories\Arp\ArpRemanejamentoItemRepository;
use Illuminate\Support\Facades\DB;
use stdClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

trait CompraTrait
{
    public function retornaSaldoAtualizado($compraitem_id, $unidadeAutorizadaId, $compraItemUnidadeId)
    {
        $unidade_id = $unidadeAutorizadaId ?? session('user_ug_id');

        return CompraItemMinutaEmpenho::select(
            DB::raw("
            coalesce(coalesce(compra_item_unidade.quantidade_autorizada, 0)
                    - (
                    select coalesce(sum(cime.quantidade), 0)
                    from compra_item_minuta_empenho cime
                             join minutaempenhos m on cime.minutaempenho_id = m.id
                             left join contrato_minuta_empenho_pivot cmep on m.id = cmep.minuta_empenho_id
                    where cime.compra_item_id = $compraitem_id
                    and unidade_id = $unidade_id
                    and cime.compra_item_unidade_id = $compraItemUnidadeId
                      and cmep.contrato_id is null
                    )
           , 0) AS saldo
            ")
        )
        ->join(
            'compra_items',
            'compra_items.id',
            '=',
            'compra_item_minuta_empenho.compra_item_id'
        )
        ->rightJoin('compra_item_unidade', function ($query) {
                $query->on('compra_item_unidade.compra_item_id', 'compra_items.id')
                ->where("compra_item_unidade.situacao", true);
        })
        ->where('compra_item_unidade.compra_item_id', $compraitem_id)
        ->where('compra_item_unidade.unidade_id', $unidade_id)
        ->where('compra_item_unidade.id', $compraItemUnidadeId)
        ->groupBy('compra_item_unidade.quantidade_autorizada', 'compra_item_unidade.id')
        ->first();
    }

    public function gravaParametroItensdaCompraSISPP($compraSiasg, $compra): void
    {
        $unidade_autorizada_id = session('user_ug_id');
        $this->gravaParametroSISPP($compraSiasg, $compra, $unidade_autorizada_id);
    }

    public function gravaParametroItensdaCompraSISPPCommand($compraSiasg, $compra): void
    {
        $unidade_autorizada_id = $compra->unidade_origem_id;
        $this->gravaParametroSISPP($compraSiasg, $compra, $unidade_autorizada_id);
    }

    private function gravaParametroSISPP($compraSiasg, $compra, $unidade_autorizada_id): void
    {
        if (!is_null($compraSiasg->data->itemCompraSisppDTO)) {
            foreach ($compraSiasg->data->itemCompraSisppDTO as $key => $item) {
                $catmatseritem = $this->gravaCatmatseritem($item);

                $compraitem = $this->updateOrCreateCompraItemSispp($compra, $catmatseritem, $item);

                $fornecedor = $this->retornaFornecedor($item);

                $this->gravaCompraItemFornecedor($compraitem->id, $item, $fornecedor);

                $this->gravaCompraItemUnidadeSispp($compraitem->id, $item, $unidade_autorizada_id, $fornecedor);
            }
        }
    }

    private function gravaParametrosSuprimento(Compras $compra, string $fornecedor_empenho_id): void
    {
        $fornecedor = Fornecedor::find($fornecedor_empenho_id);

        $item = new stdClass;
        $item->tipo = 'S';
        $item->numero = '00001';
        $item->descricaoDetalhada = 'Serviço';
        $item->quantidadeTotal = 1;

        //campos para gravar no CompraItemFornecedor
        $item->classicacao = '';
        $item->situacaoSicaf = '-';
        $item->quantidadeHomologadaVencedor = 0;
        $item->valorUnitario = 1;
        $item->valorTotal = 0;
        $item->quantidadeEmpenhada = 0;

        $this->gravaSuprimento('SERVIÇO PARA SUPRIMENTO DE FUNDOS', $compra, $item, $fornecedor);

        $item->tipo = 'M';
        $item->numero = '00002';
        $item->descricaoDetalhada = 'Material';

        $this->gravaSuprimento('MATERIAL PARA SUPRIMENTO DE FUNDOS', $compra, $item, $fornecedor);
    }

    private function gravaSuprimento($descricao, $compra, $item, $fornecedor): void
    {
        $catmatseritem = Catmatseritem::where('descricao', $descricao)
            ->select('id')->first();

        $compraitem = $this->updateOrCreateCompraItemSispp($compra, $catmatseritem, $item);
        $this->gravaCompraItemFornecedor($compraitem->id, $item, $fornecedor);
        $this->gravaCompraItemUnidadeSuprimento($compraitem->id);
    }

    # Método responsável em salvar os itens da compra para o tipo SISRP
    public function gravaParametroItensdaCompraSISRP($compraSiasg, $compra): void
    {

        # Verifica se a compra tem unidade sub-rogada
        # Se tiver, ela será a gerenciadora da compra
        # Se não, a unidade de origem será a unidade autorizada
        $unidade_autorizada_id = $this->retornaUnidadeAutorizadaBuscaCompra($compra);
        // $compra->unidade_subrrogada_id ?? $compra->unidade_origem_id;

        $this->gravaParametroSISRP($compraSiasg, $compra, $unidade_autorizada_id);
    }

    public function gravaParametroItensdaCompraSISRPCommand($compraSiasg, $compra): void
    {
        $unidade_autorizada_id = $compra->unidade_origem_id;
        $this->gravaParametroSISRP($compraSiasg, $compra, $unidade_autorizada_id);
    }

    private function gravaParametroSISRP($compraSiasg, $compra, $unidade_autorizada_id): void
    {
        # Inicia a utlização para consumir a API do SIASG
        $consultaCompra = new ApiSiasg();


        # Se não existir valor recuperado pela API, retorna do método
        if (empty($compraSiasg->data)) {
            return ;
        }

        # Se existir o link do item, será incluido nas tabelas correspondentes à compra
        if (!is_null($compraSiasg->data->linkSisrpCompleto)) {
            foreach ($compraSiasg->data->linkSisrpCompleto as $key => $item) {
                $item = (object) $item;
                # Executa o serviço dois recuperando os valores dos itens
                $dadosItemCompra = ($consultaCompra->consultaCompraByUrl($item->linkSisrpCompleto));
                $tipoUasg = (substr($item->linkSisrpCompleto, -1));

                $this->salvarItemSisrp($dadosItemCompra, $tipoUasg, $compra);
            }
        }
    }

    public function retornaUnidadeAutorizada($compraSiasg, $compra)
    {
        $SISPP = 1;
        $SISRP = 2;

        $unidade_autorizada_id = null;
        $tipoCompra = $compraSiasg->data->compraSispp->tipoCompra;
        $subrrogada = $compraSiasg->data->compraSispp->subrogada;
        if ($tipoCompra == $SISPP) {
            ($subrrogada <> '000000')
                ? $unidade_autorizada_id = (int)$this->buscaIdUnidade($subrrogada)
                : $unidade_autorizada_id = $compra->unidade_origem_id;
        }
        if ($tipoCompra == $SISRP) {
            ($subrrogada <> '000000')
                ? $unidade_autorizada_id = (int)$this->buscaIdUnidade($subrrogada)
                : $unidade_autorizada_id = $compra->unidade_origem_id;
        }

        return $unidade_autorizada_id;
    }

    public function buscaIdUnidade($uasg)
    {
        return Unidade::where('codigo', $uasg)->first()->id;
    }

    /**
     * Método responsável em salvar as informações do PDM
     */
    private function gravarPDM(string $codigo, string $descricao)
    {
        # Se o código ou a descrição do PDM estiver vazia, retorna nulo
        # Para não incluir o id do catmatpdm
        if (empty($codigo) || empty($descricao)) {
            return null;
        }

        return Catmatpdms::firstOrCreate(
            [
                'codigo' =>  $codigo
            ],
            [
                'descricao' => $descricao,
                'catmatclasse_id' => Catmatclasse::where("codigo", "99999")->first()->id
            ]
        );
    }

     /**
      * Método responsável em salvar as informações do catmaseritens
    */
    public function gravaCatmatseritem($item)
    {

        $MATERIAL = [149, 194];
        $SERVICO = [150, 195];

        $codigo_siasg = (isset($item->codigo)) ? $item->codigo : $item->codigoItem;
        $tipo = ['S' => $SERVICO[0], 'M' => $MATERIAL[0]];
        $catGrupo = ['S' => $SERVICO[1], 'M' => $MATERIAL[1]];

        $catMatPdmId = null;


        # Verifica se o tipo do item é material, se for, busca o PDM
        if ($item->tipo == 'M' && isset($item->pdm) && isset($item->nomePdm)) {
            # Recupera o id do PDM salvo na tabela catmatpdms
            $catMatPdmId = $this->gravarPDM($item->pdm, $item->nomePdm);
        }

        # Colunas para o filtro se na tabela catmatseritem vai ter o registro criado ou atualizado
        $filtroUpdateCreateCatMatSerItem = [
            'codigo_siasg' => (int)$codigo_siasg,
            'grupo_id' => (int)$catGrupo[$item->tipo],
            // 'deleted_at' => null
        ];

        # Monta a informação básica para inserir na tabela catmatseritem
        $informacoesInserir = [ 'grupo_id' => $catGrupo[$item->tipo] ];

        # Se encontrar o PDM, insere o ID do PM na tabela catmatseritem
        $informacoesInserir['catmatpdm_id'] = (isset($catMatPdmId->id)) ? $catMatPdmId->id : $catMatPdmId;

        # Se a descricao for vazia, insere o texto pelo sistema
        if (empty($item->descricao)) {
            # Insere a descrição gerada pelo sistema
            $informacoesInserir['descricao'] = $codigo_siasg . " - Descrição não informada pelo serviço.";
            # Retorna as informações do registro atualizado ou criado
            return Catmatseritem::updateOrCreate($filtroUpdateCreateCatMatSerItem, $informacoesInserir);
        }

        # Se existir uma descrição, insere a descrição enviada pelo serviço
        $informacoesInserir['descricao'] = $item->descricao;

        # Retorna as informações do registro atualizado ou criado
        return Catmatseritem::updateOrCreate($filtroUpdateCreateCatMatSerItem, $informacoesInserir);
    }

    public function updateOrCreateCompraItemSispp($compra, $catmatseritem, $item)
    {
        $MATERIAL = [149, 194];
        $SERVICO = [150, 195];
        $tipo = ['S' => $SERVICO[0], 'M' => $MATERIAL[0]];

        $compraitem = CompraItem::updateOrCreate(
            [
                'compra_id' => (int)$compra->id,
                'tipo_item_id' => (int)$tipo[$item->tipo],
                'catmatseritem_id' => (int)$catmatseritem->id,
                'numero' => (string)$item->numero,
            ],
            [
                'descricaodetalhada' => (string)$item->descricaoDetalhada,
                'qtd_total' => $item->quantidadeTotal,
                'criterio_julgamento' => $item->criterioJulgamento ?? null,
                'criterio_julgamento_id' => $item->criterioJulgamentoId ?? null,
                'codigo_ncmnbs' => $item->codigoNcmNbs ?? null,
                'aplica_margem_ncmnbs' => $item->aplicaMargemNcmNbs ?? false,
                'descricao_ncmnbs' => $item->descricaoNcmNbs ?? null,
            ]
        );
        return $compraitem;
    }

    public function retornaFornecedor($item)
    {
        $fornecedor = new Fornecedor();

        if (mb_strtoupper($item->niFornecedor, 'UTF-8') === 'ESTRANGEIRO'
            || ctype_alpha(substr(mb_strtoupper($item->niFornecedor, 'UTF-8'), 0, 2))
            || strlen(mb_strtoupper($item->niFornecedor, 'UTF-8')) == 9) {
            $cpf_cnpj_idgener = 'ESTRANGEIRO_' . mb_strtoupper(
                preg_replace('/\s/', '_', $item->nomeFornecedor),
                'UTF-8'
            );

            $retorno = $fornecedor->buscaFornecedorPorNumero($cpf_cnpj_idgener);

            if (is_null($retorno)) {
                $fornecedor->tipo_fornecedor = 'IDGENERICO';
                $fornecedor->cpf_cnpj_idgener = $cpf_cnpj_idgener;
                $fornecedor->nome = $item->nomeFornecedor;
                $fornecedor->save();
                return $fornecedor;
            }
            return $retorno;
        }

        $retorno = $fornecedor->buscaFornecedorPorNumero($item->niFornecedor);

        //TODO UPDATE OR INSERT FORNECEDOR
        if (is_null($retorno)) {
            $fornecedor->tipo_fornecedor = $fornecedor->retornaTipoFornecedor($item->niFornecedor);
            $fornecedor->cpf_cnpj_idgener = $fornecedor->formataCnpjCpf($item->niFornecedor);
            $fornecedor->nome = $item->nomeFornecedor;
            $fornecedor->save();
            return $fornecedor;
        }
        return $retorno;
    }

    /**
     * Método responsável em verificar se o item que será cadastrado / atualizado
     * sofreu alteração de valor pela função da alteração da ata
     */
    private function itemCanceladoArp(int $idCompraItem, int $idFornecedor)
    {
        # Query responsável em recuperar da tabela item histórico da arp
        # Se o item do fornecedor teve o valor alterado pelo sistema
        $valorAlterado =
        ArpItemHistorico::join(
            "arp_historico",
            "arp_historico.id",
            "=",
            "arp_item_historico.arp_historico_id"
        )
        ->join("arp_alteracao", "arp_alteracao.id", "=", "arp_historico.arp_alteracao_id")
        ->join("arp_item", "arp_item.id", "=", "arp_item_historico.arp_item_id")
        ->join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
        ->where(["arp_alteracao.rascunho" => false, "arp_item_historico.item_cancelado" => true])
        ->where("compra_item_fornecedor.compra_item_id", $idCompraItem)
        ->where("compra_item_fornecedor.fornecedor_id", $idFornecedor)
        ->count();

        # Se encontrar um registro ou mais, porque o item foi alterado e então
        # não deverá atualizar o valor unitário na tabela do compra_item_fornecedor
        if ($valorAlterado > 0) {
            return true;
        }

        # Se não encontrar nenhum registro, pode atualizar o item
        # com os valores do serviço
        return false;
    }

    /**
     * Método responsável em verificar se o item que será cadastrado / atualizado
     * sofreu alteração de valor pela função da alteração da ata
     */
    private function itemAlteradoValorCancelamentoArp(int $idCompraItem, ?int $idFornecedor)
    {
        # Query responsável em recuperar da tabela item histórico da arp
        # Se o item do fornecedor teve o valor alterado pelo sistema ou se existe um Cancelamento de item
        $valorAlterado =
        ArpItemHistorico::join(
            "arp_historico",
            function ($query) {
                $query->on("arp_historico.id", "arp_item_historico.arp_historico_id")
                    ->whereNull('arp_historico.deleted_at');
            }
        )
        ->join("arp_alteracao", "arp_alteracao.id", "=", "arp_historico.arp_alteracao_id")
        ->join("arp_item", "arp_item.id", "=", "arp_item_historico.arp_item_id")
        ->join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
        ->join('arp', 'arp.id', 'arp_item.arp_id')
        ->join('codigoitens', function ($query) {
            $query->on('codigoitens.id', 'arp.tipo_id')
                ->whereIn('codigoitens.descricao', ['Ata de Registro de Preços', 'Em elaboração']);
        })
        ->where("arp_alteracao.rascunho", false)
        ->where(function ($query) {
            $query->where("arp_item_historico.valor_alterado", true)
              ->orWhere("arp_item_historico.item_cancelado", true);
        })
        ->where("compra_item_fornecedor.compra_item_id", $idCompraItem)
        ->where("compra_item_fornecedor.fornecedor_id", $idFornecedor)
        ->orderBy('arp_item_historico.updated_at', 'DESC')
        ->select('arp_item_historico.*')
        ->first();
        
        # Não existe alteração de valor ou cancelamento para o item
        if (empty($valorAlterado)) {
            return false;
        }
        
        
        # Se encontrar um registro com o tipo de alteração Valor(es) registrado(s)
        # não deverá atualizar o valor unitário na tabela do compra_item_fornecedor
        if ($valorAlterado->arp_historico->getTipoAlteracao() == 'Valor(es) registrado(s)') {
            return true;
        }

        # Caso encontre o tipo de alterção Cancelamento de item(ns)
        # pode alterar com os valores do serviço
        return false;
    }

    public function gravaCompraItemFornecedor(
        $compraitem_id,
        $item,
        $fornecedor,
        $percentualMaiorDesconto = '0',
        bool $novoGestaoAta = false
    ) {
        
        $itemAlteradoFornecedor =
            ArpItemHistoricoAlteracaoFornecedor::join('arp_historico', 'arp_historico_id', 'arp_historico.id')
            ->join('codigoitens', function ($query) {
                $query->on('arp_historico.tipo_id', 'codigoitens.id')
                    ->where('codigoitens.descricao', 'Fornecedor');
            })
            ->join('arp_alteracao', function ($query) {
                $query->on('arp_alteracao.id', 'arp_alteracao_id')
                    ->where('arp_alteracao.rascunho', false);
            })
            ->join('compra_item_fornecedor', 'compra_item_fornecedor.id', 'compra_item_fornecedor_origem_id')
            ->where('compra_item_fornecedor.fornecedor_id', $fornecedor->id)
            ->where('compra_item_fornecedor.compra_item_id', $compraitem_id)
            ->get();
        
        # se existir uma alteração do fornecedor, não pode deixar atualizar o item na compra
        if ($itemAlteradoFornecedor->count() > 0) {
            return ;
        }
        
        $percentMaiorDesconto = $item->percentualMaiorDesconto ?? $percentualMaiorDesconto;
        
        $qtd_empenhada = 0;
        
        if ((isset($item->quantidadeEmpenhada))) {
            if (is_numeric($item->quantidadeEmpenhada)) {
                $qtd_empenhada = (float) $item->quantidadeEmpenhada;
            }
            
            if (!is_numeric($item->quantidadeEmpenhada)) {
                $qtd_empenhada = (float) preg_replace('/[^0-9]/', '', $item->quantidadeEmpenhada);
            }
        }

        # Responsável em verificar se pode alterar o valor do item
        # conforme o valor recuperado pelo serviço
        $valorAlteradoPelaArp = $this->itemAlteradoValorCancelamentoArp($compraitem_id, $fornecedor->id);

        $arrayDadosCompraItemFornecedor =
            [
            'ni_fornecedor' => $fornecedor->cpf_cnpj_idgener,
            'classificacao' => (isset($item->classicacao)) ? $item->classicacao : '',
            'situacao_sicaf' => $item->situacaoSicaf,
            'quantidade_homologada_vencedor' =>
                (isset($item->quantidadeHomologadaVencedor)) ? $item->quantidadeHomologadaVencedor : 0,
            'quantidade_empenhada' => $qtd_empenhada,
            'percentual_maior_desconto' => $percentMaiorDesconto,
            'novo_gestao_ata' => $novoGestaoAta,
            'situacao' => true,
            'classificacao' => $item->classificacao
        ];
        
        # Se não encontrar valor alterado, pode utilizar o valor obtido no serviço
        if (!$valorAlteradoPelaArp) {
            $arrayDadosCompraItemFornecedor['valor_unitario'] = $item->valorUnitario;
            $arrayDadosCompraItemFornecedor['valor_negociado'] =
                (isset($item->valorTotal)) ? $item->valorTotal : $item->valorNegociado;
        }

        return CompraItemFornecedor::updateOrCreate(
            [
                'compra_item_id' => $compraitem_id,
                'fornecedor_id' => $fornecedor->id
            ],
            $arrayDadosCompraItemFornecedor
        )->id;
    }

    public function gravaCompraItemFornecedorSuprimento($minuta, $fornecedor_id)
    {
        $fornecedor = Fornecedor::find($fornecedor_id);

        foreach ($minuta->compra->compra_item as $compra_item) {
            CompraItemFornecedor::updateOrCreate(
                [
                    'compra_item_id' => $compra_item->id,
                    'fornecedor_id' => $fornecedor->id
                ],
                [
                    'ni_fornecedor' => $fornecedor->cpf_cnpj_idgener,
                    'classificacao' => '',
                    'situacao_sicaf' => '-',
                    'quantidade_homologada_vencedor' => 0,
                    'valor_unitario' => 1,
                    'valor_negociado' => 0,
                    'quantidade_empenhada' => 0
                ]
            );
        }
    }


    public function gravaCompraItemUnidadeSispp($compraitem_id, $item, $unidade_autorizada_id, $fornecedor)
    {
        $compraItemUnidade = CompraItemUnidade::updateOrCreate(
            [
                'compra_item_id' => $compraitem_id,
                'unidade_id' => $unidade_autorizada_id,
                'fornecedor_id' => $fornecedor->id
            ],
            [
                'quantidade_saldo' => $item->quantidadeTotal,
                'quantidade_autorizada' => $item->quantidadeTotal

            ]
        );

        $saldo = $this->retornaSaldoAtualizado($compraitem_id, $unidade_autorizada_id, $compraItemUnidade->id);

        $compraItemUnidade->quantidade_saldo = $saldo->saldo;
        $compraItemUnidade->save();
    }

    public function gravaCompraItemUnidadeSuprimento($compraitem_id): void
    {
        CompraItemUnidade::updateOrCreate(
            [
                'compra_item_id' => $compraitem_id,
                'unidade_id' => session('user_ug_id'),
            ],
            [
                'quantidade_saldo' => 1,
                'quantidade_autorizada' => 1
            ]
        );
    }

    private function retornarExecucaoCompraItem(array $parametros)
    {
        $apiSiasg = new ApiSiasg();

        return  json_decode($apiSiasg->executaConsulta('ITEMCOMPRASISRP', $parametros));
    }

    public function gravaCompraItemUnidadeSisrp(
        $compraitem,
        $unidade_autorizada_id,
        $dadosGerenciadoraParticipante,
        $carona,
        $dadosFornecedor,
        $tipoUasg,
        bool $novoGestaoAta = false
    ) {

        $fornecedor_id = null;
        $qtd_autorizada =
            $dadosGerenciadoraParticipante->quantidadeAAdquirir - $dadosGerenciadoraParticipante->quantidadeAdquirida;

        $quantidadeAAdquirir = $dadosGerenciadoraParticipante->quantidadeAAdquirir;
        $quantidadeAdquirida = $dadosGerenciadoraParticipante->quantidadeAdquirida;


        # Se a unidade que está sendo inserida for Carona, então recuperamos as
        # informações do fornecedor para inserir no banco de dados
        if (!is_null($carona)) {
            $carona = (object)$carona[0];
            $qtd_autorizada = $carona->quantidadeAutorizada;
            $fornecedor = $this->retornaFornecedor((object)$dadosFornecedor[0]);
            $fornecedor_id = $fornecedor->id;
            $quantidadeAAdquirir = $qtd_autorizada;
            $quantidadeAdquirida = 0;
        }
//        else {
//            $qtd_autorizada =
//            $dadosGerenciadoraParticipante->quantidadeAAdquirir - $dadosGerenciadoraParticipante->quantidadeAdquirida;
//            $quantidadeAAdquirir = $dadosGerenciadoraParticipante->quantidadeAAdquirir;
//            $quantidadeAdquirida = $dadosGerenciadoraParticipante->quantidadeAdquirida;
//        }

        # Recupera o registro na tabela compra item unidade
        $compraItemUnidade = CompraItemUnidade::where(
            [
                'compra_item_id' => $compraitem->id,
                'unidade_id' => $unidade_autorizada_id,
                'fornecedor_id' => $fornecedor_id,

            ]
        )->first();

        # Responsável em verificar se pode alterar o valor do saldo do item
        # conforme o valor recuperado pelo serviço.
        $valorAlteradoPelaArp = $this->itemAlteradoValorCancelamentoArp($compraitem->id, $fornecedor_id);

        # Se o registro não existir, então vamos criar o registro
        if (is_null($compraItemUnidade)) {
            $compraItemUnidade = new CompraItemUnidade;
            $compraItemUnidade->compra_item_id = $compraitem->id;
            $compraItemUnidade->unidade_id = $unidade_autorizada_id;
            $compraItemUnidade->fornecedor_id = $fornecedor_id;
        }


        # Se não encontrar item cancelado, pode utilizar o valor obtido no serviço
        if (!$valorAlteradoPelaArp) {
            $compraItemUnidade->quantidade_saldo = $qtd_autorizada;
        }

        $compraItemUnidade->tipo_uasg = $tipoUasg;
        $compraItemUnidade->quantidade_adquirir = $quantidadeAAdquirir;
        $compraItemUnidade->quantidade_adquirida = $quantidadeAdquirida;
        $compraItemUnidade->novo_gestao_ata = $novoGestaoAta;

        $compraItemUnidade->save();

        $arpItemRemanejamentoRepository = new ArpRemanejamentoItemRepository();

        # Recuperar o registro no item do remanejamento para avaliar se pode alterar o valor da quantidade autorizada
        $itemRemanejamentoAprovado =
            $arpItemRemanejamentoRepository->getItemRemanejamentoAprovadoPorUnidade(
                $unidade_autorizada_id,
                $compraItemUnidade->id
            );

        # Se não existir o item da unidade aprovada no remanejamento, então atualizamos a quantidade autorizada
        if (is_null($itemRemanejamentoAprovado)) {
            $compraItemUnidade->quantidade_autorizada = $qtd_autorizada;
            $compraItemUnidade->save();
        }

        # Retorna o saldo da minuta de empenho para atualizar com a quantidade empenhada
        $saldo = $this->retornaSaldoAtualizado($compraitem->id, $unidade_autorizada_id, $compraItemUnidade->id);

        # Se não encontrar item cancelado, pode utilizar o valor obtido no serviço
        if (!$valorAlteradoPelaArp) {
            $compraItemUnidade->quantidade_saldo = ($saldo->saldo > 0) ? $saldo->saldo : $qtd_autorizada;
        }

        $compraItemUnidade->situacao = true;
        $compraItemUnidade->save();

        return $compraItemUnidade->id;
    }

    private function setCondicaoFornecedor(
        $minuta,
        $itens,
        string $descricao,
        $fornecedor_id,
        $fornecedor_compra_id
    ) {
        //SE FOR ESTRANGEIRO
        if ($fornecedor_id != $fornecedor_compra_id) {
            $fornecedor_id = $fornecedor_compra_id;
        }
        if ($descricao === 'Suprimento') {
            return $itens->where('compra_item_fornecedor.fornecedor_id', $fornecedor_id);
        }
        $tipo_compra = $minuta->tipo_compra;

        if ($tipo_compra === 'SISPP') {
            return $itens->where('compra_item_unidade.fornecedor_id', $fornecedor_id)
                ->where('compra_item_fornecedor.fornecedor_id', $fornecedor_id);
        }

        if ($tipo_compra === 'SISRP') {
            return $itens->where('compra_item_fornecedor.fornecedor_id', $fornecedor_id);
        }
    }

    private function setColunaContratoQuantidade(array $item, $tipos = null): string
    {
        $quantidade = $item['quantidade'];
        if ($tipos !== null) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $quantidade *= -1;
            }
            $quantidade = sprintf('%.5f', (float)$quantidade);
        }

        $qtd_field = " <input  type='number' max='" . $item['qtd_item'] . "' min='1' " .
            "class='form-control qtd qtd" . $item['contrato_item_id'] . "' " .
            "id='qtd" . $item['contrato_item_id'] . "' " .
            "data-tipo='' name='qtd[]' value='$quantidade' " .
            "data-item-id='" . $item['contrato_item_id'] . "' " .
            "data-qtd_item='" . $item['qtd_item'] . "' name='valor_total[]' value='" . $item['valor'] . "' " .
            "data-contrato_item_id='" . $item['contrato_item_id'] . "' ";

        if (isset($item['vlr_unitario_item'])) {
            $qtd_field .= "data-vlr_unitario_item='" . $item['vlr_unitario_item'] . "' ";
        }
        $qtd_field .= "data-valor_unitario='" . $item['valorunitario'] . "' " .
            "onchange='calculaValorTotal(this)' >" .
            "<input  type='hidden' id='quantidade_total" . $item['contrato_item_id'] . "' " .
            "data-tipo='' name='quantidade_total[]' value='" . $item['qtd_item'] . " '> ";
        return $qtd_field;
    }

    private function setColunaContratoValorTotal(array $item, $tipos = null): string
    {
        $valor = (float)$item['valor'];

        if (!is_null($tipos)) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $valor *= -1;
            }
        }
        $valor = number_format($valor, '2', '.', '');

        $numseq = $item['numseq'] ?? '';

        $valorTotal = " <input type='text' id='vrtotal" . $item['contrato_item_id'] . "' " .
            "class='form-control col-md-12 valor_total vrtotal" . $item['contrato_item_id'] . "' " .
            "data-qtd_item='" . $item['qtd_item'] . "' name='valor_total[]' value='$valor' " .
            "data-contrato_item_id='" . $item['contrato_item_id'] . "' " .
            "data-item-id='" . $item['contrato_item_id'] . "' " .
            "data-numseq='$numseq' " .
            "data-qtditem='" . $item['valortotal'] . "' " .
            "data-valor_unitario='" . $item['valorunitario'] . "' ";
        if (isset($item['vlr_unitario_item'])) {
            $valorTotal .= "data-vlr_unitario_item='" . $item['vlr_unitario_item'] . "' ";
        }
        $valorTotal .= "onkeyup='calculaQuantidade(this)' >";
        return $valorTotal;
    }

    private function setColunaSuprimentoQuantidade(array $item, $tipos = null): string
    {
        $quantidade = $item['quantidade'];
        if ($tipos !== null) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $quantidade *= -1;
            }
            $quantidade = sprintf('%.5f', (float)$quantidade);
        }

        return " <input  type='number' max='" . $item['qtd_item'] . "' min='1' " .
            "class='form-control qtd" . $item['compra_item_id'] . "' id='qtd" . $item['compra_item_id'] . "' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-tipo='' name='qtd[]' value='$quantidade' readonly  > "
            . " <input  type='hidden' id='quantidade_total" . $item['compra_item_id']
            . "' data-tipo='' name='quantidade_total[]' value='"
            . $item['qtd_item'] . " readonly'> ";
    }

    private function setColunaSuprimentoValorTotal(array $item, $tipos = null): string
    {
        $valor = (float)$item['valor'];
        if (!is_null($tipos)) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $valor *= -1;
            }
        }
        $valor = number_format($valor, '2', '.', '');

        $numseq = $item['numseq'] ?? null;

        $valorTotal = " <input type='text' id='vrtotal" . $item['compra_item_id'] . "' " .
            "class='form-control col-md-12 valor_total vrtotal" . $item['compra_item_id'] . "' " .
            "data-qtd_item='" . $item['qtd_item'] . "' name='valor_total[]' value='$valor' " .
            "data-compra_item_id='" . $item['compra_item_id'] . "' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-numseq='$numseq' " .
            "data-qtditem='" . $item['valortotal'] . "' " .
            "data-valor_unitario='" . $item['valorunitario'] . "' ";
        if (isset($item['vlr_unitario_item'])) {
            $valorTotal .= "data-vlr_unitario_item='" . $item['vlr_unitario_item'] . "' ";
        }
        $valorTotal .= "onkeyup='calculaQuantidade(this)' >";
        return $valorTotal;
    }

    private function setColunaCompraSisrpQuantidade(array $item, $tipos = null): string
    {
        $quantidade = $item['quantidade'];
        if ($tipos !== null) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $quantidade *= -1;
            }
            $quantidade = sprintf('%.5f', (float)$quantidade);
        }

        $qtd_field = " <input type='number' max='" . $item['qtd_item'] . "' min='1' " .
            "id='qtd" . $item['compra_item_id'] . "' " .
            "data-compra_item_id='" . $item['compra_item_id'] . "' " .
            "data-valor_unitario='" . $item['valorunitario'] . "' name='qtd[]' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-qtditem='" . $item['qtd_item'] . "' " .
            "class='form-control qtd' value='$quantidade' ";

        if (isset($item['vlr_unitario_item'])) {
            $qtd_field .= "data-vlr_unitario_item='" . $item['vlr_unitario_item'] . "' ";
        }
        $qtd_field .= "onchange='calculaValorTotal(this)' >" .
            "<input  type='hidden' id='quantidade_total" . $item['compra_item_id'] . "' " .
            "data-tipo='' name='quantidade_total[]' value='" . $item['qtd_item'] . "'> ";
        return $qtd_field;
    }

    private function setColunaCompraSisrpValorTotal(array $item, $tipos = null): string
    {
        $valor = (float)$item['valor'];
        if (!is_null($tipos)) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $valor *= -1;
            }
        }
        $valor = number_format($valor, '2', '.', '');

        $numseq = $item['numseq'] ?? null;

        return " <input  type='text' class='form-control valor_total vrtotal" . $item['compra_item_id'] . "' " .
            "id='vrtotal" . $item['compra_item_id'] . "' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-numseq='$numseq' " .
            "data-tipo='' name='valor_total[]' value='$valor' disabled > ";
    }

    private function setColunaCompraSisppMaterialQuantidade(array $item, $tipos = null): string
    {
        $quantidade = $item['quantidade'];
        if ($tipos !== null) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $quantidade *= -1;
            }
            $quantidade = sprintf('%.5f', (float)$quantidade);
        }

        $qtd_field = " <input type='number' max='" . $item['qtd_item'] . "' min='1' " .
            "id='qtd" . $item['compra_item_id'] . "' data-compra_item_id='" . $item['compra_item_id'] . "' " .
            "data-valor_unitario='" . $item['valorunitario'] . "' name='qtd[]' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-qtditem='" . $item['qtd_item'] . "' " .
            "class='form-control qtd' value='$quantidade' ";
        if (isset($item['vlr_unitario_item'])) {
            $qtd_field .= "data-vlr_unitario_item='" . $item['vlr_unitario_item'] . "' ";
        }
        $qtd_field .= "onchange='calculaValorTotal(this)' >" .
            "<input  type='hidden' id='quantidade_total" . $item['compra_item_id'] . "' " .
            "data-tipo='' name='quantidade_total[]' value='" . $item['qtd_item'] . "'> ";
        return $qtd_field;
    }

    private function setColunaCompraSisppMaterialValorTotal(array $item, $tipos = null): string
    {
        $valor = (float)$item['valor'];
        if (!is_null($tipos)) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $valor *= -1;
            }
        }
        $valor = number_format($valor, '2', '.', '');

        $numseq = $item['numseq'] ?? null;

        return " <input  type='text' class='form-control valor_total vrtotal" . $item['compra_item_id'] . "' " .
            "id='vrtotal" . $item['compra_item_id'] . "' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-numseq='$numseq' " .
            "data-tipo='' name='valor_total[]' value='$valor' disabled > ";
    }

    private function setColunaCompraSisppServicoQuantidade(array $item, $tipos = null): string
    {
        $quantidade = $item['quantidade'];
        if ($tipos !== null) {
            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $quantidade *= -1;
            }
            $quantidade = sprintf('%.5f', (float)$quantidade);
        }
        return " <input  type='number' max='" . $item['qtd_item'] . "' min='1' " .
            "class='form-control qtd" . $item['compra_item_id'] . "' " .
            "id='qtd" . $item['compra_item_id'] . "' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-tipo='' name='qtd[]' value='$quantidade' readonly> " .
            "<input type='hidden' id='quantidade_total" . $item['compra_item_id'] . "' " .
            "data-tipo='' name='quantidade_total[]' value='" . $item['qtd_item'] . " '> ";
    }

    private function setColunaCompraSisppServicoValorTotal(array $item, $tipos = null): string
    {
        $valor = (float)$item['valor'];
        $disabled = '';
        if (!is_null($tipos)) {
            $disabled = 'disabled';

            if (array_key_exists($item['operacao_id'], $tipos) && $tipos[$item['operacao_id']] === "ANULAÇÃO") {
                $valor *= -1;
            }
        }
        $valor = number_format($valor, '2', '.', '');

        $numseq = $item['numseq'] ?? null;

        $valorTotal = " <input type='text' " .
            "class='form-control col-md-12 valor_total vrtotal" . $item['compra_item_id'] . "' " .
            "id='vrtotal" . $item['compra_item_id'] . "' data-qtd_item='" . $item['qtd_item'] . "' " .
            "name='valor_total[]' value='$valor' " .
            "data-compra_item_id='" . $item['compra_item_id'] . "' " .
            "data-item-id='" . $item['compra_item_id'] . "' " .
            "data-numseq='$numseq' " .
            "data-qtditem='" . $item['valortotal'] . "' " .
            "data-valor_unitario='" . $item['valorunitario'] . "' ";
        if (isset($item['vlr_unitario_item'])) {
            $valorTotal .= "data-vlr_unitario_item='" . $item['vlr_unitario_item'] . "' ";
        }
        $valorTotal .= "onkeyup='calculaQuantidade(this)' $disabled>";
        return $valorTotal;
    }

    /**
     * @author: Aruã Magalhães
     * Issue 156
     * Valida se o Amparo Legal percente a lei normativa 14.133/2021
    */
    private function validarIdAmparoLegal(array $listaAmparoLegal, string $atoNormativo = '14.133/2021')
    {
        $arrayIdAmparoLegal = AmparoLegal::select('id')
        ->whereNull('deleted_at')
        ->where('ato_normativo', 'LIKE', "%{$atoNormativo}%")->pluck('id')->toArray();

        foreach ($listaAmparoLegal as $amparoLegal) {
            if (in_array($amparoLegal, $arrayIdAmparoLegal)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @author: Aruã Magalhães
     * Issue 156
     * Recupera se o uasg utiliza o sisg conforme salvo no banco de dados
    */
    private function validarUasgNaoSisg(?int $idUasg)
    {
        $uasgRecuperada = Unidade::select('sisg')
        ->whereNull('deleted_at')
        ->where("id", $idUasg)->pluck('sisg')->toArray();

        if ($uasgRecuperada) {
            return $uasgRecuperada[0];
        }

        return false;
    }

    private function validarCompraSiasg(\Illuminate\Http\Request $request)
    {

        $dados = (object) $request->all()[0];

        $retornoSiasg = $this->consultaCompraSiasg($dados);

        try {
            $this->validaRetornoSiasg($retornoSiasg);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @author: Aruã Magalhães
     * Issue 156
     * Valida se o Codigo do item selecionado e diferente de Não se aplica
    */
    private function validarIDCodigoItem(int $idCodigoItem, string $descricao = 'Não se Aplica')
    {
        $arrayIdCodigoItem =CodigoItem::select('id')
        ->whereNull('deleted_at')
        ->where('descricao', $descricao)->pluck('id')->toArray();

        if (in_array($idCodigoItem, $arrayIdCodigoItem)) {
            return false;
        }

        return true;
    }

    public function validaRetornoSiasg($retornoSiasg)
    {

        if (is_null($retornoSiasg->data)) {
            if (isset($retornoSiasg->messagem)) {
                throw new \Exception($retornoSiasg->messagem);
            }
            throw new \Exception('Erro ao consultar e atualizar compra SIASG');
        }
        return;
    }

    private function recuperarCodigoUasgBeneficiaria(?string $uasg_beneficiaria_id)
    {
        $codigoUasgBeneficiaria = null;
        $idUasgBeneficiaria = $uasg_beneficiaria_id ? $uasg_beneficiaria_id : null;
        if ($idUasgBeneficiaria) {
            $uasgBeneficiaria = Unidade::find($idUasgBeneficiaria);
            $codigoUasgBeneficiaria = $uasgBeneficiaria->codigo;
            if ($codigoUasgBeneficiaria=='') {
                $codigoUasgBeneficiaria = null;
            }
        }

        return $codigoUasgBeneficiaria;
    }

    private function retornarExecucaoCompraSispp(
        Codigoitem $modalidade,
        Unidade $uasgCompra,
        string $numeroCompra,
        ?string $codigoUasgBeneficiaria
    ) {
        $numero_ano = explode('/', $numeroCompra);
        $apiSiasg = new ApiSiasg();
        $params = $this->montarInformacoesConsultaCompraSISPP(
            $modalidade->descres,
            $numero_ano[0] . $numero_ano[1],
            $uasgCompra->codigo,
            $codigoUasgBeneficiaria
        );

        return  json_decode($apiSiasg->executaConsulta('COMPRASISPP', $params));
    }

    private function retornarExecucaoCompraSisrp(
        Codigoitem $modalidade,
        Unidade $uasgCompra,
        string $numeroCompra,
        string $uasgParticipante,
        $tipoUasg = 'P'
    ) {
        $numero_ano = explode('/', $numeroCompra);
        $apiSiasg = new ApiSiasg();
        $params = $this->montarInformacoesConsultaCompraSISRP(
            $modalidade->descres,
            $numero_ano[0] . $numero_ano[1],
            $uasgCompra->codigo,
            $uasgParticipante,
            $tipoUasg
        );

        return  json_decode($apiSiasg->executaConsulta('COMPRASISPP', $params));
    }

    /**
     * Método responsável em buscar a compra, usando o serviço um
     */
    public function consultaCompraSiasg(Request $request, bool $compraSispp = true)
    {
        $modalidade = Codigoitem::find($request->modalidade_id);
        $uasgCompra = Unidade::find($request->unidade_origem_id);

        # Se for compra do tipo sispp, utiliza o serviço um para recuperar as informações
        if ($compraSispp) {
            // uasg beneficiária
            $codigoUasgBeneficiaria = $this->recuperarCodigoUasgBeneficiaria($request->uasg_beneficiaria_id);

            # Se for marcado o campo de unidade origem da compra,
            # vamos buscar as informações da compra da sub-rogada
            if (!empty($request->get('unidade_origem_compra_id'))) {
                # Recupera as informações da unidade para recuperar o código da unidade de origem
                $uasgCompra = Unidade::find($request->get('unidade_origem_compra_id'));
            }

            return $this->retornarExecucaoCompraSispp(
                $modalidade,
                $uasgCompra,
                $request->get('numero_ano'),
                $codigoUasgBeneficiaria
            );
        }

        # Recupera os dados da unidade participante
        $unidadeParticipante = Unidade::find($request->get('participante'));

        # Retorna as informações do serviço quatro para os participantes
        return $this->retornarExecucaoCompraSisrp(
            $modalidade,
            $uasgCompra,
            $request->get('numero_ano'),
            $unidadeParticipante->codigosiasg
        );
    }

    /**
     * Retorna os dados da compra com base na unidade de origem e o numero e ano da compra
     */
    public function retornaDadosCompra(array $input, string $tipoCompra = 'SISRP')
    {
        $compra =  Compras::join("codigoitens", "codigoitens.id", "compras.tipo_compra_id")
        ->where('compras.unidade_origem_id', $input['unidade_origem_compra_id'])
        ->where('codigoitens.descricao', $tipoCompra)
        ->where('numero_ano', $input['numero_ano'])
        ->where("modalidade_id", $input['modalidade_id']);

        # Se o campo Unidade origem da compra for diferente do campo Unidade gerenciadora
        # Então o usuário está buscando uma compra sub-rogada, então incluímos o id da
        # Unidade Sub-rogada
        if ($input['unidade_origem_id'] != $input['unidade_origem_compra_id']) {
            $compra->where('compras.unidade_subrrogada_id', $input['unidade_origem_id']);
        }

        $compra = $compra->select('compras.*')->first();

        return $compra;
    }

    /**
     * Método responsável em verificar se a compra existe na nossa base de dados
     */
    public function verificaCompraExiste($request, ?array $unidadeOrigem = null, bool $retornaSomenteId = false)
    {

        $compra = Compras::whereIn('unidade_origem_id', $unidadeOrigem)
            ->where('modalidade_id', $request->modalidade_id)
            ->where('numero_ano', $request->numero_ano)
            ->where('tipo_compra_id', $request->tipo_compra_id)
            ->get();

        if ($retornaSomenteId) {
            return $compra->pluck('id');
        }

        return $compra;
    }

    public function verificaPermissaoUasgCompra($compraSiasg, $request, ?int $idUasgUsuarioLogado)
    {
        $unidade_autorizada_id = null;
        $tipoCompra = $compraSiasg->data->compraSispp->tipoCompra;
        $subrrogada = $compraSiasg->data->compraSispp->subrogada;
        if ($tipoCompra == config('api.siasg.SISPP')) {
            if ($subrrogada != '000000') {
                $unidade = Unidade::find($idUasgUsuarioLogado);
                ($subrrogada == $unidade->codigo) ? $unidade_autorizada_id = $idUasgUsuarioLogado : '';
            } else {
                ($request->unidade_origem_id == session('user_ug_id'))
                    ? $unidade_autorizada_id = $request->unidade_origem_id : '';
            }
        } else {
            $unidade_autorizada_id = $idUasgUsuarioLogado;
        }

        return $unidade_autorizada_id;
    }

    public function montaParametrosCompra($compraSiasg, $request): void
    {

        $unidade_subrogada = $compraSiasg->data->compraSispp->subrogada;
        $request->request->set(
            'unidade_subrrogada_id',
            ($unidade_subrogada <> '000000') ? (int)$this->buscaIdUnidade($unidade_subrogada) : null
        );
        $request->request->set('tipo_compra_id', $this->buscaTipoCompra($compraSiasg->data->compraSispp->tipoCompra));
        $request->request->set('inciso', $compraSiasg->data->compraSispp->inciso);
        $request->request->set('lei', $compraSiasg->data->compraSispp->lei);
        $request->request->set('id_unico', $compraSiasg->data->compraSispp->idUnico);
        $request->request->set('cnpj_orgao', $compraSiasg->data->compraSispp->cnpjOrgao);
        
        # 2 = Origem do SIASG
        $origemCompra = 2;

        if ($compraSiasg->origemConsulta === 'api_novo_divulgacao') {
            # 1 = Novo divulgação de compra
            $origemCompra = 1;
        }
        
        $request->request->set('origem', $origemCompra);
    }

    public function buscaTipoCompra($descres)
    {
        $tipocompra = Codigoitem::wherehas('codigo', function ($q) {
            $q->where('descricao', '=', 'Tipo Compra');
        })
            ->where('descres', '0' . $descres)
            ->first();
        return $tipocompra->id;
    }

    /**
     * Método responsável em Gravar/Atualizar a compra recuperada pelo serviço um
     */
    public function updateOrCreateCompra($request)
    {
        $unidadeOrigem = $request->get('unidade_origem_compra_id');
        $unidadeSubRogada = $request->get('unidade_subrrogada_id');

        $compra = Compras::updateOrCreate(
            [
                'unidade_origem_id' => $unidadeOrigem,
                'modalidade_id' => $request->get('modalidade_id'),
                'numero_ano' => $request->get('numero_ano'),
                'tipo_compra_id' => $request->get('tipo_compra_id')
            ],
            [
                'unidade_subrrogada_id' => $unidadeSubRogada,
                'tipo_compra_id' => $request->get('tipo_compra_id'),
                'inciso' => $request->get('inciso'),
                'lei' => $request->get('lei'),
                'artigo' => $request->get('artigo'),
                'id_unico' => $request->get('id_unico'),
                'cnpjOrgao' => $request->get('cnpj_orgao'),
                'origem' => $request->get('origem'),
            ]
        );
        return $compra;
    }

    private function montarInformacoesConsultaItemSISRP(CompraItem $item)
    {

        $unidade = $item->compra->unidadeOrigem->codigo;
        $modalidade = $item->compra->modalidade->descres;
        $numeroAno = str_replace("/", "", $item->compra->numero_ano);

        return [
            'compra' => $unidade.$modalidade.$numeroAno,
            'item' => $item->numero
        ];
    }

    /**
     * Método responsável em montar as informações para enviar ao serviço um
     */
    private function montarInformacoesConsultaCompraSISPP(
        ?string $descricao,
        ?string $numeroAno,
        ?string $uasgCompra,
        ?string $codigoUasgBeneficiaria
    ) {
        return [
            'modalidade' => $descricao,
            'numeroAno' => $numeroAno,
            'uasgCompra' => $uasgCompra,
            'uasgBeneficiaria' => $codigoUasgBeneficiaria,
            'uasgUsuario' => session('user_ug')
        ];
    }

    /**
     * Método responsável em montar as informações para enviar ao serviço quatro
     */
    private function montarInformacoesConsultaCompraSISRP(
        ?string $descricao,
        ?string $numeroAno,
        ?string $uasgCompra,
        string $uasgParticipante,
        string $tipoUasg
    ) {
        return [
            'modalidade' => $descricao,
            'numeroAno' => $numeroAno,
            'uasgCompra' => $uasgCompra,
            'uasgBeneficiaria' => '',
            'uasgUsuario' => $uasgParticipante
        ];
    }

    private function mensagemValidacaoDadosCompraInvalidos()
    {
        return  [
                    "texto_exibicao.validarcompra"=>'Dados da compra inválidos!',
                    "unidadecompra_id.validarcompra"=>'Unidade Da Compra.',
                    "modalidade_id.validarcompra"=>'Modalidade Da Licitação.',
                    "licitacao_numero.validarcompra"=>'Número Da Licitação.',
                ];
    }

    /**
     * Método responsável em montar a query da url para enviar ao COMPRAS
     * para retornar as informações do item
     */
    private function montarQueryUrlItem(
        string $numeroAnoCompra,
        string $uasgCompra,
        string $uasgUsuario,
        string $modalidade,
        string $numeroItem,
        string $tipoUasg = 'P'
    ) {
        $uasgUsuario = trim($uasgUsuario);
        $uasgUsuario = "uasgUsuario={$uasgUsuario}&";

        $uasgCompra = trim($uasgCompra);
        $uasgCompra = "uasgCompra={$uasgCompra}&";

        $modalidade = trim($modalidade);
        $modalidade =  "modalidade={$modalidade}&";

        $numeroAnoCompra = trim($numeroAnoCompra);
        $numeroAnoCompra = str_replace("/", "", $numeroAnoCompra);
        $numeroAnoCompra = "numeroAnoCompra={$numeroAnoCompra}&";

        $numeroItem = trim($numeroItem);
        $numeroItem = "numeroItem={$numeroItem}&";

        $tipoUasg = trim($tipoUasg);
        $tipoUasg = "tipoUASG={$tipoUasg}";

        return $uasgUsuario.$uasgCompra.$modalidade.$numeroAnoCompra.$numeroItem.$tipoUasg;
    }

    /**
     * Método responsável em salvar os itens a partir da URL do item do SISRP
     */
    public function salvarItemSisrp($dadosItemCompra, string $tipoUasg, $compra)
    {
        # Se não conseguir recuperar a informação do item, então sai do método
        if (is_null($dadosItemCompra['data'])) {
            return ;
        }

        // $tipoUasg = (substr($item->linkSisrpCompleto, -1));
        $dadosata = (object)$dadosItemCompra['data']['dadosAta'];
        $gerenciadoraParticipante = (object)$dadosItemCompra['data']['dadosGerenciadoraParticipante'];
        $carona = $dadosItemCompra['data']['dadosCarona'];
        $dadosFornecedor = $dadosItemCompra['data']['dadosFornecedor'];

        if (empty($dadosata->percentualMaiorDesconto)) {
            $dadosata->percentualMaiorDesconto = 0;
        }

        # Grava/Atualiza o item na tabela catmatseritem

        $catmatseritem = $this->gravaCatmatseritem($dadosata);
        # Grava/Atualiza o item na tabela compra_items
        $modcompraItem = new CompraItem();

        $compraItem = $modcompraItem->updateOrCreateCompraItemSisrp($compra, $catmatseritem, $dadosata);
        $dadosFornecedor = is_array($dadosFornecedor) || is_object($dadosFornecedor) ? $dadosFornecedor : null;


        # Se não recuperar as informações do fornecedor, então não continue a interação
        if (is_null($dadosFornecedor)) {
            return ;
        }

        # Percorre as informações do fornecedor para o item da compra
        foreach ($dadosFornecedor as $key => $itemfornecedor) {
            # Recupera as informações do fornecedor, se não existir, cadastra na tabela de fornecedores
            $fornecedor = $this->retornaFornecedor((object)$itemfornecedor);



            # Grava/Atualiza o item para a tabela de fornecedores (compra_item_fornecedor)
            $compraItemFornecedor[] = $this->gravaCompraItemFornecedor(
                $compraItem->id,
                (object)$itemfornecedor,
                $fornecedor,
                $dadosata->percentualMaiorDesconto,
                true
            );
        }

        $unidade_autorizada_id = $this->retornaUnidadeAutorizadaBuscaCompra($compra);


        # Grava/Atualiza o item para a tabela das unidades "UASG" (compra_item_unidade)
        $compraItemUnidade[] = $this->gravaCompraItemUnidadeSisrp(
            $compraItem,
            $unidade_autorizada_id,
            $gerenciadoraParticipante,
            $carona,
            $dadosFornecedor,
            $tipoUasg,
            true
        );
    }

    /**
     * Método responsável em retornar o ID da unidade autorizada da compra
     * Podendo ser a unidade de origem da compra ou a sub-rogada
     */
    public function retornaUnidadeAutorizadaBuscaCompra($compra)
    {
        # Verifica se a compra tem unidade sub-rogada
        # Se tiver, ela será a gerenciadora da compra
        # Se não, a unidade de origem será a unidade autorizada
        return $compra->unidade_subrrogada_id ?? $compra->unidade_origem_id;
    }
    
    public function montarURLItemParticipante()
    {
        $consultaCompra = new ApiSiasg();
        
        # Retorna a URL principal do COMPRAS
        $urlPrincipal = config('api.siasg.url');
        
        # Retorna a URI do SISRP complementar o link
        $uri = $consultaCompra->retornaServicoEspecifico('RETORNAITEMSISRP');
        
        return $urlPrincipal.$uri;
    }
    
    public function compraPertenceMPCalamidadePublica(?string $lei)
    {
        $mp = config('arp.arp.resultado_lei');
        
        $resultado = false;
        
        if (preg_match($mp, $lei, $matches)) {
            $resultado = $matches[0];
        }
        
        return $resultado;
    }

    public function formatarNomeFonteApiCompra(string $origemConsulta)
    {
        $nomeFormatado = '';

        switch ($origemConsulta) {
            case 'api_novo_divulgacao':
                $nomeFormatado = 'NDC';
                break;
            case 'api_siasg':
                $nomeFormatado = 'SIASG';
                break;
        }

        return $nomeFormatado;
    }
}
