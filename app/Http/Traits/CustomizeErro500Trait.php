<?php

namespace App\Http\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Exception;

trait CustomizeErro500Trait
{
    public function mensagemUsuario(string $errorCode)
    {
        return  'Essa situação pode ser temporária, então pedimos que tente realizar a ação novamente.<br>'.
                "Caso o problema persista, <a href='https://portaldeservicos.economia.gov.br/'>informe o erro</a>, ".
                'indicando os passos que executou antes de ocorrer o problema, juntamente com a URL e o código do '.
                 "erro para que a equipe possa rastrear e corrigir.<br> Código: $errorCode";
    }

    public function errorCode()
    {
        $date = Carbon::now()->format('Y-m-d H:i:s.u');
        return preg_replace("/[^0-9]/", "", $date);
    }

    public function inserirLogCustomize500(string $errorCode, Exception $exception)
    {
        $texto = $this->textoLog($errorCode, $exception);
    }

    public function textoLog(string $errorCode, Exception $exception)
    {
        return "Código de erro: $errorCode ". $exception->getMessage();
    }
}
