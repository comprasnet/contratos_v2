<?php

namespace App\Http\Traits;

use App\Models\ArquivoGenerico;
use App\Repositories\ArquivoGenerico\ArquivoGenericoRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

trait ArquivoGenericoTrait
{
    private $disk = 'public';

    public function salvarArquivoFormatoGenerico(
        UploadedFile $file,
        string $classModel,
        int $id,
        int $tipoId,
        string $destinationPath,
        string $descricao = '',
        bool $restrito = false
    ) {
        # Se existir o arquivo, insere na tabela
        if ($file) {
            # O disk que será salvo baseando na configuração realizada
            # no arquivo filesystems
            // $disk = "public";

            $anexos = new ArquivoGenerico();

            # Salva a classe da model
            $anexos->arquivoable_type = $classModel;

            # Salva o id da Model
            $anexos->arquivoable_id = $id;

            # Salva o tipo que será o arquivo
            $anexos->tipo_id = $tipoId;

            # Salva a descrição para o arquivo, por padrão está sendo salvo
            # sem conteúdo
            $anexos->descricao = $descricao;

            # Recupera o nome do arquivo enviado pelo usuário
            $anexos->nome = $file->getClientOriginalName();

            # Por padrão todo os arquivos estão sendo salvos como false (público)
            $anexos->restrito = $restrito;

            # Recupera o ano atual para salvar o arquivo
            $ano = Carbon::now()->format('Y');
            # Recupera o mes atual para salvar o arquivo
            $mes = Carbon::now()->format('m');

            # Transforma o caminho informado em array
            $arrayDestinationPath = explode('/', $destinationPath);

            # Inicia a variável para ser inserido o caminho formatado
            $pathDestinationFormat = '';

            # Se for um único nível de diretório
            if (count($arrayDestinationPath) == 1) {
                $pathDestinationFormat = "{$arrayDestinationPath[0]}/{$ano}/{$mes}";
            }

            # Se no parâmetro da pasta conter mais de um nível de diretório
            if (count($arrayDestinationPath) > 1) {
                # Remove a última posição do array e retorna para variável para ser incluído
                # após o ano e mês
                $endPath = array_pop($arrayDestinationPath);

                # Adiciona o caminho intermediário, em seguida ano e mês e por fim
                # a última pasta informada no parâmetro
                array_push($arrayDestinationPath, $ano, $mes, $endPath);

                # Percorre o array de caminho formatado e monta o caminho final
                foreach ($arrayDestinationPath as $path) {
                    $pathDestinationFormat .= "$path/";
                }

                # Remove a última barra no caminho do arquivo
                $pathDestinationFormat = substr($pathDestinationFormat, 0, -1);
            }

            # Criptografa o nome do arquivo
            $new_file_name = $file->hashName();

            # Salva o arquivo no diretório informado e retorna a URL para salvar
            # na coluna URL
            $file_path = $file->storeAs($pathDestinationFormat, $new_file_name, $this->disk);
            $anexos->url = $file_path;

            # Salva o id do usuário que realizou a inserção do arquivo
            $anexos->user_id = Auth::id();
            $anexos->save();
        }
    }

    /**
     * Método responsável em recuperar o registro no banco de dados
     */
    public function getArquivoGenerico(int $id, string $classModel)
    {
        $arquivoGenericoRepository = new ArquivoGenericoRepository();

        # Recupera o registro na arquivo_generico
        $arquivoGenerico = $arquivoGenericoRepository->getArquivoGenerico($id, $classModel);

        # Se encontrar o registro, então recupera o registro na tabela principal do morph
        # então criamos um atributo com o nome tabela_principal para receber as informações
        # do registro na tabela principal
        if (!empty($arquivoGenerico)) {
            $arquivoGenerico->tabela_principal = $arquivoGenerico->arquivoable;
        }

        return $arquivoGenerico;
    }

    /**
     * Método responsável em deletar fisicamente o arquivo na pasta storage
     */
    private function deletarArquivoFiscamente(
        ArquivoGenericoRepository $arquivoGenericoRepository,
        int $idArquivoGenerico
    ) {
        # Recupera a URL do arquivo para poder excluir fisicamente
        $urlArquivo = $arquivoGenericoRepository->getURLArquivoGenerico($idArquivoGenerico);

        # Remove o arquivo fisicamente da pasta storage
        return Storage::disk($this->disk)->delete($urlArquivo);
    }

    /**
     * Método responsável em deletar o registro do arquivo na tabela e fisicamente quando o
     * usuário remover na tela
     */
    public function deletarArquivoGenericoFront(Request $request)
    {
        # Se o usuário excluir o arquivo através da tela
        if ($request->removerArquivoGenerico === "true") {
            $arquivoGenericoRepository = new ArquivoGenericoRepository();

            # Remover o arquivo fisicamente
            $arquivoRemovido = $this->deletarArquivoFiscamente($arquivoGenericoRepository, $request->idArquivoGenerico);

            # Se o arquivo for removido fiscamente, então remove no banco de dados
            if ($arquivoRemovido) {
                # Remover o arquivo no banco de dados
                return $arquivoGenericoRepository->deleteRegistroArquivoGenerico($request->idArquivoGenerico);
            }

            return false;
        }
    }

    /**
     * Método responsável em atualizar o registro no banco de dados
    */
    private function atualizarRegistroArquivoGenerico(
        ArquivoGenericoRepository $arquivoGenericoRepository,
        UploadedFile $file,
        int $idArquivoGenerico
    ) {
        # Recupera o registro no banco de dados para atualizar
        $arquivo = $arquivoGenericoRepository->getArquivoGenericoUnico($idArquivoGenerico);

        # Recupera o nome do arquivo enviado pelo usuário
        $arquivo->nome = $file->getClientOriginalName();
        # Criptografa o nome do arquivo
        $new_file_name = $file->hashName();

        # Converter a string em array para remover o nome do arquivo e manter o mesmo caminho
        $arrayDestinationPath = explode('/', $arquivo->url);

        # Retira o nome do arquivo do array
        array_pop($arrayDestinationPath);

        $pathDestinationFormat = '';

        # Percorre o array completo com o nome do novo arquivo para criar a string da URL
        foreach ($arrayDestinationPath as $newPath) {
            $pathDestinationFormat .= "$newPath/";
        }

        # Remove a última barra no caminho do arquivo
        $pathDestinationFormat = substr($pathDestinationFormat, 0, -1);

        # Salva o arquivo no diretório informado e retorna a URL para salvar
        # na coluna URL
        $file_path = $file->storeAs($pathDestinationFormat, $new_file_name, $this->disk);
        $arquivo->url = $file_path;

        # Salva o id do usuário que realizou a inserção do arquivo
        $arquivo->user_id = Auth::id();

        return $arquivo->save();
    }


    /**
     * Método responsável em atualizar o arquivo informado pelo usuário na tela
     */
    public function atualizarArquivoGenericoFront(
        Request $request,
        string $nameFile,
        string $classModel,
        int $tipoId,
        string $destinationPath,
        string $descricao = '',
        bool $restrito = false
    ) {
        # Se o usuário apenas remover o arquivo
        $this->deletarArquivoGenericoFront($request);

        $arquivoGenericoRepository = new ArquivoGenericoRepository();
        // dd($request->idArquivoGenerico);
        # Se o id da tabela arquivo_generico for vazio, então será criado o registro e salvo o arquivo
        if (empty($request->idArquivoGenerico) && !empty($request->$nameFile)) {
            return $this->salvarArquivoFormatoGenerico(
                $request->$nameFile,
                $classModel,
                $request->id,
                $tipoId,
                $destinationPath,
                $descricao,
                $restrito
            );
        }

        if (!empty($request->idArquivoGenerico) && !empty($request->$nameFile)) {
            # Remover o arquivo fisicamente na pasta storage
            $arquivoRemovido = $this->deletarArquivoFiscamente($arquivoGenericoRepository, $request->idArquivoGenerico);

            # Se o arquivo for removido com sucesso, então insere o novo arquivo
            if ($arquivoRemovido) {
                # Atualiza o arquivo físico e na tabela arquivo_generico
                return $this->atualizarRegistroArquivoGenerico(
                    $arquivoGenericoRepository,
                    $request->$nameFile,
                    $request->idArquivoGenerico
                );
            }
        }
    }
}
