<?php

namespace App\Http\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;

trait ExternalServices
{

    /**
     * @param string $method
     * @param string $uri
     * @param string $endpoint
     * @param callable|null $stack
     * @param array|null $headers
     * @param string|null $body
     * @param array|null $requestOptions
     * @return Response
     * @throws GuzzleException
     */
    public function makeRequest(
        string $method,
        string $uri,
        string $endpoint,
        ?callable $stack,
        ?array $headers,
        ?string $body,
        ?array $requestOptions
    ) {

        $client = new Client([
            'base_uri' => $uri,
            'headers' => $headers,
            'handler' => $stack,
            'body' => $body,
            'verify' => false
        ]);

        return $client->request($method, $endpoint, $requestOptions);
    }
}
