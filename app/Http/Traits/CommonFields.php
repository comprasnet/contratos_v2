<?php

namespace App\Http\Traits;

use App\Models\ArpGestor;
use App\Models\BackpackUser;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Database\Eloquent\Builder;

trait CommonFields
{
    protected function addFieldEstadoCombo($tab = null)
    {
        CRUD::addField([
            'name' => 'estado_id',
            'label' => 'Estado',
            'type' => 'relationship',
            'model' => 'App\Models\Estado',
            'entity' => 'estado',
            'attribute' => 'nome',
            'placeholder' => 'Selecione o Estado',
            'allows_null' => true,
            'options' => (function (Builder $query) {
                return $query->orderBy('nome', 'ASC')
                    ->get();
            }),
            'tab' => $tab

        ]);
    }

    protected function addFieldMunicipioCombo($tab = null)
    {
        CRUD::addField([
            'name' => 'municipio_id',
            'label' => 'Município',
            'type' => 'relationship',
            'model' => 'App\Models\Municipio',
            'entity' => 'municipio',
            'ajax' => true,
            'attribute' => 'nome',
            'data_source' => url("admin/fetch/municipios"),
            'placeholder' => 'Selecione município',
            'dependencies' => ['estado_id'],
            'minimum_input_length' => 0,
            'allows_null' => true,
            'tab' => $tab

        ]);
    }

    protected function addFieldNomeText($tab = null, $upper = false)
    {
        CRUD::addField([
            'name' => 'nome',
            'label' => 'Nome',
            'type' => 'text',
            'attributes' => [
                'onkeyup' => $upper ? 'maiuscula(this)' : ''
            ],
            'tab' => $tab
        ]);
    }

    protected function addFieldNomeResumidoText($tab = null, $upper = false)
    {
        CRUD::addField([
            'name' => 'nome_resumido',
            'label' => 'Nome Resumido',
            'type' => 'text',
            'attributes' => [
                'onkeyup' => $upper ? 'maiuscula(this)' : '',
                'maxlength' => "19"
            ],
            'tab' => $tab
        ]);
    }

    protected function addFieldCnpj($tab = null)
    {
        CRUD::addField([
            'name' => 'cnpj',
            'label' => 'CNPJ',
            'type' => 'number',
            'tab' => $tab
        ]);
    }

    protected function addFieldIe($tab = null)
    {
        CRUD::addField([
            'name' => 'ie',
            'label' => 'IE',
            'type' => 'number',
            'tab' => $tab
        ]);
    }

    protected function addFieldIm($tab = null)
    {
        CRUD::addField([
            'name' => 'im',
            'label' => 'IM',
            'type' => 'number',
            'tab' => $tab
        ]);
    }

    protected function addFieldCodigoUnidadeNumber($tab = null)
    {
        CRUD::addField([
            'name' => 'codigo_unidade',
            'label' => 'Código Unidade',
            'type' => 'number',
            'tab' => $tab
        ]);
    }

    protected function addFieldCertificadoPassPassword($tab = null)
    {
        CRUD::addField([
            'name' => 'certificado_pass',
            'label' => 'Senha Certificado',
            'type' => 'password',
            'tab' => $tab
        ]);
    }

    protected function addFieldCertificadoPathUpload($tab = null)
    {
        CRUD::addField([   // Upload
            'name' => 'certificado_path',
            'label' => 'Certificado e-CNPJ A1',
            'type' => 'upload',
            'upload' => true,
            'disk' => 'local',
            'tab' => $tab
        ]);
    }


    protected function addFieldSelect(
        string $label,
        string $campoRelacionamento,
        string $metodoRelacionamentoModel,
        string $campoParaExibirSelect,
        string $url,
        array $demaisAtributos = null,
        $tab = null,
        $fake = false,
        $required = false,
        $default = null,
        $mensagemSelectError = null,
        $mensagemSelect = null,
        array $attributes = []
    ) {
        CRUD::addField([   // Upload
            'label'     => $label,
            'type'      => 'select2_from_ajax',
            'name'      => $campoRelacionamento, // the column that contains the ID of that connected entity;
            'entity'    => $metodoRelacionamentoModel, // the method that defines the relationship in your Model
            'attribute' => $campoParaExibirSelect, // foreign key attribute that is shown to user
            'data_source' => url($url),
            'tab' => $tab,
            'wrapperAttributes' => $demaisAtributos,
            'placeholder' => "Buscar registro",
            'fake' => $fake,
            'required' => $required,
            'default' => $default,
            'mensagemSelectError' => $mensagemSelectError,
            'mensagemSelect' => $mensagemSelect,
            'attributes' => $attributes

        ]);
    }

    protected function addFieldSelectMultiple(
        string $label,
        string $name,
        string $metodoRelacionamentoModel,
        string $campoParaExibirSelect,
        string $url,
        array $demaisAtributos = null,
        $tab = null,
        string $column_value_option = '',
        array $value = null,
        ?string $model = null
    ) {
        CRUD::addField([   // Upload
            'label'     => $label,
            'type'      => 'select2_from_ajax_multiple_custom',
            'name'      => $name, // the column that contains the ID of that connected entity;
            'entity'    => $metodoRelacionamentoModel, // the method that defines the relationship in your Model
            'attribute' => $campoParaExibirSelect, // foreign key attribute that is shown to user
            'data_source' => url($url),
            'tab' => $tab,
            'wrapperAttributes' => $demaisAtributos,
            'placeholder' => "Buscar registro(s)",
            'pivot' => true,
            'column_value_option' => $column_value_option,
            'value' => $value,
            'model' => $model
        ]);
    }

    protected function addAreaCustom(
        string $nomeBlade,
        ?string $area = null,
        ?string $idArea = null,
        ?array $value = null,
        ?string $hint = null,
        ?string $id = null,
        ?string $textTooltip = null,
        ?string $tab = null,
        ?string $text = null
    ) {
        $this->crud->addField(
            [
                'name'  => $nomeBlade,
                'type'  => $nomeBlade,
                'classArea' => $area,
                'idArea' => $idArea,
                'value' => $value,
                'hint' => $hint,
                'id' => $id,
                'texttooltip' => $textTooltip,
                'tab' => $tab,
                'text' => $text
            ]
        );
    }

    protected function addTableCustom(
        array $column,
        string $idTable,
        string $emptyTable,
        ?string $classArea = null,
        ?string $idArea = null,
        ?string $tab = null,
        ?int $pageLength = null,
        ?array $jsonItem = null,
        ?string $labelInstrucaoTabela = null,
        ?string $orderTable = null,
        string $type = 'table_selecionar_item_adesao_arp',
        string $name = 'table_selecionar_item_adesao_arp'
    ) {
        $this->crud->addField(
            [
                'name'  => $type,
                'type'  => $name,
                'idTable' => $idTable,
                'emptyTable' => $emptyTable,
                'classArea' => $classArea,
                'idArea' => $idArea,
                'column' => $column,
                'tab' => $tab,
                'pageLength' => $pageLength,
                'value' => $jsonItem,
                'label_instrucao_tabela' => $labelInstrucaoTabela,
                'order_table' => $orderTable
            ]
        );
    }

    protected function addModalCustom(string $nameModal, string $idModalCustom, string $tituloModalCustom)
    {
        $this->crud->addField(
            [
                'name'  => $nameModal,
                'idModalCustom'  => $idModalCustom,
                'type'  => 'modal_custom',
                'tituloModalCustom' => $tituloModalCustom
            ]
        );
    }
}
