<?php

namespace App\Http\Traits\Contrato;

use App\Models\Contrato;

trait ContratoTrait
{

    public static function recuperarContratoPorItemFornecedor(int $compraItemId, int $compraItemFornecedorId)
    {
        return Contrato::join('contratoitens', function ($join) {
            $join->on('contratoitens.contrato_id', '=', 'contratos.id')
                ->whereNull('contratoitens.deleted_at');
        })
        ->join('compras', function ($join) {
            $join->on('contratos.unidadecompra_id', '=', 'compras.unidade_origem_id')
                ->on('contratos.modalidade_id', '=', 'compras.modalidade_id')
                ->on('contratos.licitacao_numero', '=', 'compras.numero_ano');
        })
        ->join('compra_items', function ($join) {
            $join->on('compra_items.compra_id', '=', 'compras.id')
                ->where('compra_items.situacao', true)
                ->whereColumn('compra_items.numero', '=', 'contratoitens.numero_item_compra');
        })
        ->join('compra_item_fornecedor', function ($join) {
            $join->on('compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
                ->where('compra_item_fornecedor.situacao', true)
                ->whereColumn('compra_item_fornecedor.fornecedor_id', '=', 'contratos.fornecedor_id');
        })
        ->join('unidades', 'contratos.unidade_id', '=', 'unidades.id')
        ->join('codigoitens', 'codigoitens.id', 'contratoitens.tipo_id')
        ->where('compra_items.id', $compraItemId)
        ->where('compra_item_fornecedor.id', $compraItemFornecedorId)
        ->select([
            'contratos.numero',
            'contratos.data_assinatura',
            'contratos.vigencia_inicio',
            'contratos.vigencia_fim',
            'contratoitens.*',
            'codigoitens.descres',
            'unidades.codigo',
            'unidades.nomeresumido'
        ])
        ->get();
    }
}
