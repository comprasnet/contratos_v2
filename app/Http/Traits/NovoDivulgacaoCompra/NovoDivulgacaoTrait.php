<?php

namespace App\Http\Traits\NovoDivulgacaoCompra;

trait NovoDivulgacaoTrait
{
    /**
     * Método responsável em validar a requisição enviada pelo usuário
     * @param string $permission
     * @return bool
     */
    private function validationRequest(string $permission = 'administrador_novo_dc')
    {
        $authApi = auth('api');

        if ($authApi->check() && $authApi->user()->can($permission)) {
            return true;
        }

        return false;
    }
}
