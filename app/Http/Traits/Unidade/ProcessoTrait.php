<?php

namespace App\Http\Traits\Unidade;

use App\Models\UnidadeConfiguracao;
use App\Models\Unidade;

trait ProcessoTrait
{
    /**
     * Retorna a máscara do processo de acordo com a configuração da unidade e do órgão.
     *
     * @return string|null
     */
    public function getProcessoMask(int $idUnidade = null)
    {
        if (empty($idUnidade)) {
            $idUnidade = session('user_ug_id');
        }
        
        $unidadeConfiguracao = UnidadeConfiguracao::where("unidade_id", $idUnidade)->first();
        $mascaraProcessoAdesao = null;
        $unidade = Unidade::find($idUnidade);

        if (!empty($unidadeConfiguracao) && !empty($unidadeConfiguracao->padrao_processo_mascara)) {
            // Verifica se a máscara da unidade está configurada
            $mascaraProcessoAdesao = str_replace("9", "0", $unidadeConfiguracao->padrao_processo_mascara);
        } else {
            if (!empty($unidade->orgao) && !empty($unidade->orgao->configuracao) && !empty($unidade->orgao->configuracao->padrao_processo_marcara)) {
                // Unidade com máscara do órgão configurada
                $mascaraProcessoAdesao = str_replace("9", "0", $unidade->orgao->configuracao->padrao_processo_marcara);
            } elseif ($unidade->sisg) {
                // Unidade com SISG ativo
                $mascaraProcessoAdesao = '99999.999999/9999-99';
            }
        }

        return $mascaraProcessoAdesao;
    }
    
    public function validarNumeroProcesso($numeroProcesso, $mascaraProcesso = '', $unidadeSISG = true)
    {
        // Remover caracteres não numéricos e não alfabéticos
        $valorSemMascara = preg_replace('/[^0-9A-Za-z]/', '', $numeroProcesso);
        
        // Verifica se a máscara do processo está configurada
        if (!$mascaraProcesso) {
            if ($unidadeSISG) {
                return true;
            } else {
                // Permitir preenchimento sem máscara, mas aplicar valor mínimo de digitação
                if (strlen($valorSemMascara) === 0) {
                    return false;
                }
            }
        }
        
        // Obter tamanho da máscara sem caracteres não numéricos e não alfabéticos
        $tamanhoMinimo = strlen(preg_replace('/[^0-9A-Za-z]/', '', $mascaraProcesso));
        
        if (strlen($valorSemMascara) < $tamanhoMinimo) {
            return false;
        }
        
        return true;
    }
}
