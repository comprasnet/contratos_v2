<?php

namespace App\Http\Traits;

use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Database\Eloquent\Builder;

trait CommonColumns
{
    protected function addColumnEstado($table = false, $modal = true, $show = true, $export = true): void
    {
        CRUD::addColumn([
            'name' => 'estado_id',
            'label' => 'Estado',
            'type' => 'select',
            'model' => 'App\Models\Estado',
            'entity' => 'estado',
            'attribute' => 'nome',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhereHas('estado', function ($q) use ($column, $searchTerm) {
                    $q->where(
                        'nome',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                });
            }
        ]);
    }

    protected function addColumnMunicipio($table = false, $modal = true, $show = true, $export = true): void
    {
        CRUD::addColumn([
            'name' => 'municipio_id',
            'label' => 'Município',
            'type' => 'select',
            'model' => 'App\Models\municipio',
            'entity' => 'municipio',
            'attribute' => 'nome',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhereHas('municipio', function ($q) use ($column, $searchTerm) {
                    $q->where(
                        'nome',
                        'iLike',
                        '%' . $searchTerm . '%'
                    );
                });
            }
        ]);
    }

    protected function addColumnCodigoUnidade(): void
    {
        CRUD::addColumn([
            'name' => 'codigo_unidade',
            'label' => 'Código Unidade',
            'type' => 'number',
            'thousands_sep' => '',
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInShow' => true,
            'visibleInExport' => true,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere(
                    'codigo_unidade',
                    'iLike',
                    '%' . $searchTerm . '%'
                );
            }
        ]);
    }

    protected function addColumnCnpj($table = false, $modal = true, $show = true, $export = true)
    {
        CRUD::addColumn([
            'name' => 'cnpj',
            'label' => 'CNPJ',
            'type' => 'text',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnEndereco($table = false, $modal = true, $show = true, $export = true)
    {
        CRUD::addColumn([
            'name' => 'endereco',
            'label' => 'Endereço',
            'type' => 'text',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnEnderecoNumero($table = false, $modal = true, $show = true, $export = true)
    {
        CRUD::addColumn([
            'name' => 'endereco_numero',
            'label' => 'Número',
            'type' => 'text',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnEmail($table = false, $modal = true, $show = true, $export = true)
    {
        CRUD::addColumn([
            'name' => 'email',
            'label' => 'E-mail',
            'type' => 'email',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnBairro($table = false, $modal = true, $show = true, $export = true)
    {
        CRUD::addColumn([
            'name' => 'bairro',
            'label' => 'Bairro',
            'type' => 'text',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnCep($table = false, $modal = true, $show = true, $export = true)
    {
        CRUD::addColumn([
            'name' => 'cep',
            'label' => 'Cep',
            'type' => 'text',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnTelefone($table = false, $modal = true, $show = true, $export = true)
    {
        CRUD::addColumn([
            'name' => 'cep',
            'label' => 'Cep',
            'type' => 'text',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnCnae($table = false, $modal = true, $show = true, $export = true)
    {
        CRUD::addColumn([
            'name' => 'cep',
            'label' => 'Cep',
            'type' => 'text',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnIe($table = false, $modal = true, $show = true, $export = true)
    {
        CRUD::addColumn([
            'name' => 'ie',
            'label' => 'IE',
            'type' => 'text',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnIm($table = false, $modal = true, $show = true, $export = true)
    {
        CRUD::addColumn([
            'name' => 'im',
            'label' => 'IM',
            'type' => 'text',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnNomeResumido(): void
    {
        CRUD::addColumn([
            'name' => 'nome_resumido',
            'label' => 'Nome Resumido',
            'type' => 'text',
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInShow' => true,
            'visibleInExport' => true,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere(
                    'nome_resumido',
                    'iLike',
                    '%' . $searchTerm . '%'
                );
            }
        ]);
    }

    protected function addColumnText(
        $table = false,
        $modal = true,
        $show = true,
        $export = true,
        $name = 'text',
        $label = 'text',
        $limit = 9999
    ): void {
        CRUD::addColumn([
            'name' => $name,
            'label' => $label,
            'type' => 'text',
            'limit' => $limit,
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'escaped' => false,
            'searchLogic' => function (Builder $query, $column, $searchTerm) use ($name) {
                $query->orWhere(
                    $name,
                    'iLike',
                    '%' . $searchTerm . '%'
                );
            }
        ]);
    }

    protected function addColumnTextArea(
        $table = false,
        $modal = true,
        $show = true,
        $export = true,
        $name = 'text',
        $label = 'text',
        $limit = 9999
    ): void {
        CRUD::addColumn([
            'name' => $name,
            'label' => $label,
            'limit' => $limit,
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'escaped' => false,
            'searchLogic' => function (Builder $query, $column, $searchTerm) use ($name) {
                $query->orWhere(
                    $name,
                    'iLike',
                    '%' . $searchTerm . '%'
                );
            }
        ]);
    }

    protected function addColumnCreatedAt(
        $table = false,
        $modal = true,
        $show = true,
        $export = true,
        ?string $tab = null
    ): void {
        CRUD::addColumn([
            'name' => 'created_at',
            'label' => 'Criado em',
            'type' => 'datetime',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnDateHour(
        $table = false,
        $modal = true,
        $show = true,
        $export = true,
        $name = 'date',
        $label = 'date',
        ?string $tab = null
    ): void {
        CRUD::addColumn([
            'name' => $name,
            'label' => $label,
            'type' => 'datetime',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'tab' => $tab
        ]);
    }

    protected function addColumnDate(
        $table = false,
        $modal = true,
        $show = true,
        $export = true,
        $name = 'date',
        $label = 'date',
        ?string $tab = null
    ): void {
        CRUD::addColumn([
            'name' => $name,
            'label' => $label,
            'type' => 'date',
            'format' => 'L',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'tab' => $tab
        ]);
    }

    protected function addColumnCompraItemUnidade(
        $table = false,
        $modal = true,
        $show = true,
        $export = true,
        ?string $tab = null
    ): void {
        CRUD::addColumn([
            'name' => 'compraItemUnidade',
            'label' => 'Unidades Participantes',
            'type' => 'compraitemunidadetable',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'tab' => $tab
        ]);
    }

    protected function addColumnCompraItemFornecedor(
        $table = false,
        $modal = true,
        $show = true,
        $export = true,
        ?string $tab = null
    ): void {
        CRUD::addColumn([
            'name' => 'compraItemFornecedor',
            'label' => 'Fornecedores Homologados',
            'type' => 'compraitemfornecedortable',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'tab' => $tab
        ]);
    }

    protected function addColumnCompraItemArp(
        $table = false,
        $modal = true,
        $show = true,
        $export = true,
        ?string $tab = null
    ): void {
        CRUD::addColumn([
            'name' => 'compraItemArp',
            'label' => 'Atas de Registro de Preços',
            'type' => 'compra-item-arp-table',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'tab' => $tab
        ]);
    }

    protected function addColumnCompraItemFornecedorEmpenhos(
        $table = false,
        $modal = true,
        $show = true,
        $export = true
    ): void {
        CRUD::addColumn([
            'name' => 'compraItemFornecedorEmpenhos',
            'label' => 'Empenhos',
            'type' => 'compra_item_fornecedor_empenhos_table',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnCompraItemFornecedorEmpenhosDetalhados(
        $table = false,
        $modal = true,
        $show = true,
        $export = true
    ): void {
        CRUD::addColumn([
            'name' => 'compraItemFornecedorEmpenhosDetalhados',
            'label' => 'Alterações',
            'type' => 'compra_item_fornecedor_empenhos_detalhados_table',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnCompraItemFornecedorContratos(
        $table = false,
        $modal = true,
        $show = true,
        $export = true
    ): void {
        CRUD::addColumn([
            'name' => 'compraItemFornecedorContratos',
            'label' => 'Contratos',
            'type' => 'compra_item_fornecedor_contratos_table',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnGetUnidade(
        $table = false,
        $modal = true,
        $show = true,
        $export = true,
        $name = 'unidade',
        $label = 'Unidade',
        $functionName = 'getUnidade',
        $joinName = 'unidades'
    ): void {
        CRUD::addColumn([
            'name' => $name,
            'label' => $label,
            'type' => 'model_function',
            'function_name' => $functionName,
//            'limit' => 1000,
            'orderable' => true,
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'searchLogic' => function (Builder $query, $column, $searchTerm) use ($joinName) {
                $query->orWhere($joinName . '.codigo', 'ilike', "%" . strtoupper($searchTerm) . "%");
                $query->orWhere($joinName . '.nomeresumido', 'ilike', "%" . strtoupper($searchTerm) . "%");
                $query->orWhere($joinName . '.nome', 'ilike', "%" . strtoupper($searchTerm) . "%");
            },
        ]);
    }

    protected function addColumnGetCodigoItens(
        $table = false,
        $modal = true,
        $show = true,
        $export = true,
        $name = 'codigoitens',
        $label = 'Codigo Itens',
        $functionName = 'getCodigoItens',
        $joinName = 'codigoitens',
        $limit = 9999
    ): void {
        CRUD::addColumn([
            'name' => $name,
            'label' => $label,
            'type' => 'model_function',
            'function_name' => $functionName,
            'limit' => $limit,
            'orderable' => true,
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'searchLogic' => function (Builder $query, $column, $searchTerm) use ($joinName) {
                $query->orWhere($joinName . '.descres', 'ilike', "%" . strtoupper($searchTerm) . "%");
                $query->orWhere($joinName . '.descricao', 'ilike', "%" . strtoupper($searchTerm) . "%");
            },
        ]);
    }

    protected function addColumnGetCatMatSerItem(
        $table = false,
        $modal = true,
        $show = true,
        $export = true,
        $name = 'catmatseritens',
        $label = 'Descrição Item',
        $functionName = 'getCatMatSerItem',
        $joinName = 'catmatseritens',
        $limit = 9999
    ): void {
        CRUD::addColumn([
            'name' => $name,
            'label' => $label,
            'type' => 'model_function',
            'function_name' => $functionName,
            'limit' => $limit,
            'orderable' => true,
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'searchLogic' => function (Builder $query, $column, $searchTerm) use ($joinName) {
                $query->orWhere($joinName . '.codigo_siasg', 'ilike', "%" . strtoupper($searchTerm) . "%");
                $query->orWhere($joinName . '.descricao', 'ilike', "%" . strtoupper($searchTerm) . "%");
            },
        ]);
    }


    protected function addColumnGetFornecedorCnpj($table = false, $modal = true, $show = true, $export = true): void
    {
        CRUD::addColumn([
            'name' => 'fornecedor_cnpj',
            'label' => 'Fornecedor CNPJ',
            'type' => 'model_function',
            'function_name' => 'getFornecedorCnpj',
//            'limit' => 1000,
            'orderable' => true,
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('fornecedores.cnpj', 'like', "%" . strtoupper($searchTerm) . "%");
            },
        ]);
    }

    protected function addColumnGetFornecedorNome($table = false, $modal = true, $show = true, $export = true): void
    {
        CRUD::addColumn([
            'name' => 'fornecedor_nome',
            'label' => 'Fornecedor Nome',
            'type' => 'model_function',
            'function_name' => 'getFornecedorNome',
            'limit' => 30,
            'orderable' => true,
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('fornecedores.nome', 'like', "%" . strtoupper($searchTerm) . "%");
            },
        ]);
    }

    protected function addColumnGetMunicipioFornecedor(
        $table = false,
        $modal = true,
        $show = true,
        $export = true
    ): void {
        CRUD::addColumn([
            'name' => 'municipio_fornecedor',
            'label' => 'Município Fornecedor',
            'type' => 'model_function',
            'function_name' => 'getMunicipioFornecedor',
//            'limit' => 1000,
            'orderable' => true,
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('municipios.nome', 'ilike', "%" . $searchTerm . "%");
            },
        ]);
    }

    protected function addColumnUpdatedAt($table = false, $modal = true, $show = true, $export = true): void
    {
        CRUD::addColumn([
            'name' => 'updated_at',
            'label' => 'Alterado em',
            'type' => 'datetime',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnDataEmissao($table = false, $modal = true, $show = true, $export = true): void
    {
        CRUD::addColumn([
            'name' => 'data_emissao',
            'label' => 'Data Emissão',
            'type' => 'date',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnNumeroNfe($table = false, $modal = true, $show = true, $export = true): void
    {
        CRUD::addColumn([
            'name' => 'numero',
            'label' => 'Número',
            'type' => 'text',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnNaturezaOperacaoNfe($table = false, $modal = true, $show = true, $export = true): void
    {
        CRUD::addColumn([
            'name' => 'natureza_operacao',
            'label' => 'Natureza Operação',
            'type' => 'text',
            'limit' => 40,
            'escaped' => false,
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnSerieNfe($table = false, $modal = true, $show = true, $export = true): void
    {
        CRUD::addColumn([
            'name' => 'serie',
            'label' => 'Série',
            'type' => 'text',
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnChaveAcessoNfe($table = false, $modal = true, $show = true, $export = true): void
    {
        CRUD::addColumn([
            'name' => 'chave',
            'label' => 'Chave Acesso',
            'type' => 'text',
            'limit' => 44,
            'priority' => 1,
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnValorNfe($table = false, $modal = true, $show = true, $export = true): void
    {
        CRUD::addColumn([
            'name' => 'valor',
            'label' => 'Valor (R$)',
            'type' => 'number',
//            'prefix' => 'R$',
            'dec_point' => ',',
            'thousands_sep' => '.',
            'decimals' => 2,
            'visibleInTable' => $table,
            'visibleInModal' => $modal,
            'visibleInShow' => $show,
            'visibleInExport' => $export
        ]);
    }

    protected function addColumnModelFunction(
        string $name,
        string $label,
        string $functionName,
        array $visible = ['table' => true, 'modal' => true, 'show' => true, 'export' => true],
        int $limit = 99999999999999999,
        $columnSearch = ''
    ): void {
        CRUD::addColumn([
            'name' => $name,
            'label' => $label,
            'type' => 'model_function',
            'function_name' => $functionName,
            'visibleInTable' => $visible['table'],
            'visibleInModal' => $visible['modal'],
            'visibleInShow' => $visible['show'],
            'visibleInExport' => $visible['export'],
            'limit' => $limit,
            'searchLogic' => function ($query, $column, $searchTerm) use ($columnSearch) {
                if (!empty($columnSearch)) {
                    $query->orWhereRaw("$columnSearch ILIKE '%{$searchTerm}%'");
                }
            }
        ]);
    }

    protected function addColumnValorMonteraio(
        string $name,
        string $label,
        array $visible = ['table' => true, 'modal' => true, 'show' => true, 'export' => true]
    ): void {
        CRUD::addColumn([
            'name' => $name, // The db column name
            'label' => $label, // Table column heading
            'type' => 'number',
            'prefix' => 'R$ ',
            'decimals' => 2,
            'dec_point' => ',',
            'thousands_sep' => '.',
            'visibleInTable' => $visible['table'],
            'visibleInModal' => $visible['modal'],
            'visibleInShow' => $visible['show'],
            'visibleInExport' => $visible['export']
        ]);
    }

    protected function addColumnQuantidade(
        string $name,
        string $label,
        array $visible = ['table' => true, 'modal' => true, 'show' => true, 'export' => true],
        float $decimal = 2
    ): void {
        CRUD::addColumn([
            'name' => $name, // The db column name
            'label' => $label, // Table column heading
            'type' => 'number',
            'decimals' => $decimal,
            'dec_point' => ',',
            'thousands_sep' => '.',
            'visibleInTable' => $visible['table'],
            'visibleInModal' => $visible['modal'],
            'visibleInShow' => $visible['show'],
            'visibleInExport' => $visible['export']
        ]);
    }


    protected function addColumnTable(
        string $name,
        string $label,
        array $value,
        array $visible = ['table' => true, 'modal' => true, 'show' => true, 'export' => true],
        ?string $tab = null
    ): void {
        CRUD::addColumn([
            'name' => $name, // The db column name
            'label' => $label, // Table column heading
            'type' => 'table_show',
            'value' => $value,
            'visibleInTable' => $visible['table'],
            'visibleInModal' => $visible['modal'],
            'visibleInShow' => $visible['show'],
            'visibleInExport' => $visible['export'],
            'tab' => $tab,

        ]);
    }

    protected function addColumnTablePlain(
        string $name,
        string $label,
        array $columns,
        array $visible = ['table' => true, 'modal' => true, 'show' => true, 'export' => true],
        ?string $tab = null
    ): void {
        CRUD::addColumn([
            'name' => $name,
            'label' => $label,
            'type' => 'table_plain',
            'columns' => $columns,
            'visibleInTable' => $visible['table'],
            'visibleInModal' => $visible['modal'],
            'visibleInExport' => $visible['show'],
            'visibleInShow' => $visible['export'],
            'tab' => $tab,

        ]);
    }

    protected function addColumnTableCentralizada(
        string $name,
        string $label,
        array $value,
        array $visible = ['table' => true, 'modal' => true, 'show' => true, 'export' => true],
        ?string $tab = null,
        array $posicao = []
    ): void {
        CRUD::addColumn([
            'name' => $name, // The db column name
            'label' => $label, // Table column heading
            'type' => 'table_centralizada_show',
            'value' => $value,
            'visibleInTable' => $visible['table'],
            'visibleInModal' => $visible['modal'],
            'visibleInShow' => $visible['show'],
            'visibleInExport' => $visible['export'],
            'tab' => $tab,
            'posicao' => $posicao,

        ]);
    }

    protected function addColumnTableContratoHistorico(
        string $name,
        string $label,
        array $visible = ['table' => true, 'modal' => true, 'show' => true, 'export' => true],
        ?string $tab = null
    ): void {
        CRUD::addColumn([
            'name' => $name,
            'label' => $label,
            'type' => 'table_contrato_historico',
            'visibleInTable' => $visible['table'],
            'visibleInModal' => $visible['modal'],
            'visibleInExport' => $visible['show'],
            'visibleInShow' => $visible['export'],
            'tab' => $tab,

        ]);
    }

    protected function addColumnTableEmpenhos(
        string $name,
        string $label,
        array $visible = ['table' => true, 'modal' => true, 'show' => true, 'export' => true],
        ?string $tab = null
    ): void {
        CRUD::addColumn([
            'name' => $name,
            'label' => $label,
            'type' => 'table_empenhos',
            'visibleInTable' => $visible['table'],
            'visibleInModal' => $visible['modal'],
            'visibleInExport' => $visible['show'],
            'visibleInShow' => $visible['export'],
            'tab' => $tab,

        ]);
    }

    protected function addColumnModelFunctionV1(
        string $name,
        string $label,
        string $functionName,
        array $functionParameters,
        array $visible = ['table' => true, 'modal' => true, 'show' => true, 'export' => true],
        int $limit = 99999999999999999,
        $columnSearch = ''
    ): void {
        CRUD::addColumn([
            'name' => $name,
            'label' => $label,
            'type' => 'model_function_v1',
            'function_name' => $functionName,
            'function_parameters' => $functionParameters,
            'visibleInTable' => $visible['table'],
            'visibleInModal' => $visible['modal'],
            'visibleInShow' => $visible['show'],
            'visibleInExport' => $visible['export'],
            'limit' => $limit,
            'searchLogic' => function ($query, $column, $searchTerm) use ($columnSearch) {
                if (!empty($columnSearch)) {
                    $query->whereRaw("$columnSearch ILIKE '%{$searchTerm}%'");
                }
            }
        ]);
    }

    protected function addColumnArquivos(
        string $name,
        string $label,
        string $disk,
        array $visible = ['table' => true, 'modal' => true, 'show' => true, 'export' => true],
        ?string $tab = null
    ): void {
        CRUD::addColumn([
            'name' => $name,
            'label' => $label,
            'type' => 'arquivos',
            'disk' => $disk,
            'visibleInTable' => $visible['table'],
            'visibleInModal' => $visible['modal'],
            'visibleInExport' => $visible['show'],
            'visibleInShow' => $visible['export'],
            'tab' => $tab,

        ]);
    }
}
