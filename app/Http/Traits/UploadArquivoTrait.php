<?php

namespace App\Http\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

trait UploadArquivoTrait
{
    # Driver do filesystem para salvar o arquivo
    private $disk = "public";

    # Método responsável por salvar o arquivo
    private function saveFile(string $destination, Request $request, string $name) : string
    {
        # Criptografa o nome do arquivo para salvar
        $new_file_name = $request->file($name)->hashName();

        # Salva o arquivo
        $filePath = $request->file($name)->storeAs($destination, $new_file_name, $this->disk);

        # Retorna o caminho completo que será salvo o arquivo
        return $filePath;
    }

    # Método responsável em remover o arquivo físico
    private function deleteFile(string $filename)
    {
        \Storage::disk($this->disk)->delete($filename);
    }

    # Método responsável por remover o arquivo na parte lógica e física
    private function removerArquivoUpload(string $model, ?int $idRegistro, $isContratoArquivo = null)
    {
        # Remove o arquivo se o ID do registro está no form
        if (!empty($idRegistro)) {
            # Inicializa a model
            $model = new $model;

            # Recupera o registro no banco de dados
            $registroRecuperado = $model::find($idRegistro);

            //para diferenciar os campos corretos dependendo da model, se entrar no if é contratoarquivo
            if ($isContratoArquivo != null) {
                $this->deleteFile($registroRecuperado->arquivos);
            } else {
                # Deleta o arquivo na pasta
                $this->deleteFile($registroRecuperado->url);
            }

            # Deleta o registro no banco de dados
            $registroRecuperado->delete();
        }
    }

    # Método responsável em substituir o arquivo
    private function substituirArquivoUpload(string $model, array $filter)
    {
        #caminho da classe da Model
        $caminhoModel = $model;
        
        # Inicializa a model
        $model = new $caminhoModel;

        # Recupera todos os registros de anexo
        $arquivoArquivo = $model::where($filter)->get();

        # Se existir registro, realiza a remoção
        foreach ($arquivoArquivo as $arquivo) {
            $this->removerArquivoUpload($caminhoModel, $arquivo->id);
        }
    }
}
