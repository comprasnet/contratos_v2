<?php

namespace App\Http\Traits;

use Jenssegers\Agent\Agent;
use Carbon\Carbon;

trait TracksUserLogin
{
    public function trackLogin($user)
    {
        activity()->disableLogging();
        $agent = new Agent();
        $user->last_login_at = Carbon::now();
        $user->last_login_ip = request()->ip();
        $user->last_login_browser = $agent->browser();
        $user->save();
        activity()->enableLogging();
    }
}
