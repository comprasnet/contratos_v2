<?php

namespace App\Http\Traits\Compra;

use App\Api\Externa\ApiSiasg;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\LogTrait;
use App\Models\Compras;
use App\Repositories\Compra\CompraItemRepository;
use App\Http\Traits\Formatador;
use App\Models\Unidade;
use App\Repositories\UnidadeRepository;
use App\Services\Unidades\UnidadesService;
use Illuminate\Support\Facades\Log;

trait CompraItemTrait
{
    use Formatador, CompraTrait, LogTrait;

    /**
     * Obtém uma relação de fornecedores homologados para um item de compra
     * e faz a formatação para passar para o backpack exibir em tabela
     *
     * @param int $compraItemId Id do item de compra
     * @return array Lista de fornecedores homologados do item com link para visualização dos dados
     */
    public function getFornecedoresHomologados($compraItemId)
    {
        $compraItemRepository = new CompraItemRepository();
        $fornecedoresHomologados = $compraItemRepository->getFornecedoresHomologadosItem($compraItemId);
        $listaFornecedor = [];
        foreach ($fornecedoresHomologados as $key => $fornecedor) {
            $listaFornecedor['Fornecedor'][$key] = $fornecedor->fornecedor;
            $listaFornecedor['Quantidade Homologada'][$key] =
                $this->formatQuantidade($fornecedor->quantidade_homologada_vencedor);
            $listaFornecedor['Val. Unitário'][$key] = $this->formatValuePtBR($fornecedor->valor_unitario);
            $listaFornecedor['Val. Negociado'][$key] = $this->formatValuePtBR($fornecedor->valor_negociado);
            $listaFornecedor['Qtd. Empenhada'][$key] = $this->formatQuantidade($fornecedor->quantidade_empenhada);
            $route = "/transparencia/compras/" .
                $fornecedor->compra_id .
                "/itens/" .
                $compraItemId .
                "/compra-item-fornecedor/" .
                $fornecedor->compra_item_fornecedor_id .
                "/show";
            $listaFornecedor['Ação'][$key] = '<a href="' . $route . '"><i class="fas fa-eye"></i></a>';
        }

        return $listaFornecedor;
    }

    /**
     * Obtém a relação de unidades participantes para um item de compra
     * e faz a formatação para passar para o backpack exibir em tabela
     *
     * @param int $compraItemId Id do item de compra
     * @return array Lista de unidades participantes do item
     */
    public function getUnidadesParticipantesItem($compraItemId)
    {
        $compraItemRepository = new CompraItemRepository();
        $unidadesParticipantes = $compraItemRepository->getUnidadesParticipantesItem($compraItemId);
        $listaUnidade = [];
        foreach ($unidadesParticipantes as $key => $unidade) {
            $listaUnidade['Unidade'][$key] = $unidade->codigo_nomeresumido;
            $listaUnidade['Tipo UASG'][$key] = $this->tipoUasgConvertido($unidade->tipo_uasg);
            $listaUnidade['Qtd. Autorizada'][$key] = $this->formatQuantidade($unidade->quantidade_autorizada);
            $listaUnidade['Qtd. Saldo'][$key] = $this->formatQuantidade($unidade->quantidade_saldo);
        }
        return $listaUnidade;
    }

    /**
     * Obtém a relação de atas do item de compra
     * e faz a formatação para passar para o backpack exibir em tabela
     *
     * @param int $compraItemId Id do item de compra
     * @return array Lista de atas do item de compra
     */
    public function getAtasDoItemDeCompra($compraItemId)
    {
        $compraItemRepository = new CompraItemRepository();
        $atasDoItemDeCompra = $compraItemRepository->getAtasDoItemDeCompra($compraItemId);
        $listaAta = [];
        foreach ($atasDoItemDeCompra as $key => $ata) {
            $listaAta['Número'][$key] = $ata->numero_ano;
            $listaAta['Unidade Gerenciadora'][$key] = $ata->codigo_unidade;
            $listaAta['Fornecedor'][$key] = $ata->fornecedor;
            $listaAta['Data Assinatura'][$key] = $this->formatDatePtBR($ata->data_assinatura);
            $listaAta['Vigência Inicial'][$key] = $this->formatDatePtBR($ata->vigencia_inicial);
            $listaAta['Vigência Final'][$key] = $this->formatDatePtBR($ata->vigencia_final);
            $listaAta['Quantidade'][$key] = $this->formatQuantidade($ata->quantidade);
            $listaAta['Valor Unitário'][$key] = $this->formatValuePtBR($ata->valor_unitario);
            $listaAta['Valor Total'][$key] = $this->formatValuePtBR($ata->quantidade * $ata->valor_unitario);
        }
        return $listaAta;
    }
    
    public function salvarItemParticipanteAta(
        $dadosCompraParticipante,
        string $urlCompraSisrp,
        ApiSiasg $consultaCompra
    ) {
        $unidadeRepository = new UnidadeRepository();
        $unidadeService = new UnidadesService();

        foreach ($dadosCompraParticipante['dataApi'] as $uasg) {
            # Se a UASG for diferente da Gerenciadora, então buscamos os dados dos itens
            if (!empty($uasg->tipoUasg) && $uasg->tipoUasg != 'G') {
                # Recupera o código da unidade de origem da compra para buscar no serviço
                $unidadeCompra = $dadosCompraParticipante['codigo_unidade_origem'];
                $modalidadeCompra = $dadosCompraParticipante['codigo_modalidade'];
                $numeroCompra = $dadosCompraParticipante['numero_ano'];
                $numeroItem = $dadosCompraParticipante['numero_item'];
                
                # Se não existir a unidade de origem da compra, então pula o registro
                if (!isset($unidadeCompra)) {
                    continue;
                }
                
                $codigoUnidadeParticipante = $this->preencherCasasDireitaEsquerda(trim($uasg->uasg), 6) ;
                
                # Recupera as informações da unidade com base no código
                $uasgParticipante = $unidadeRepository->getUnidadePorCodigo($codigoUnidadeParticipante);

                if (empty($uasgParticipante)) {
                    $mensagem = " Unidade {$codigoUnidadeParticipante} não existe na base de dados,
                    sendo participante do item {$numeroItem} e compra $numeroCompra da modalidade {$modalidadeCompra}";
                    
                    $exception = new \Exception($mensagem);
                    $this->inserirLogCustomizado('unidades', 'error', $exception);
                    $uasgParticipante = $unidadeService->getUnidadePorCodigo($codigoUnidadeParticipante);
                }
                
                # Monta a QUERY da URL para poder buscar no serviço do Compras
                $queryUrl = $this->montarQueryUrlItem(
                    $numeroCompra,
                    $unidadeCompra,
                    $codigoUnidadeParticipante,
                    $modalidadeCompra,
                    $numeroItem
                );
                
                # Inclui a query na URL principal
                $urlCompraSisrpInterno = $urlCompraSisrp.$queryUrl;
                
                # Recupera os itens consultando o serviço
                $dadosItemCompra = $consultaCompra->consultaCompraByUrl($urlCompraSisrpInterno);
                
                # Converte para objeto os dados da compra
                $dadosCompraObjeto =  (object) $dadosCompraParticipante;

                # Insere o ID da unidade participante na origem da compra para poder inserir na coluna
                # unidade_id na tabela compra item unidade
                $dadosCompraObjeto->unidade_origem_id = $uasgParticipante->id;
                
                if ($dadosItemCompra['codigoRetorno'] != 200) {
                    $mensagemErro = 'Erro ao salvar a informação do participante na compra';
                    Log::channel('gestao_ata')->error($mensagemErro);
                    Log::channel('gestao_ata')->error($urlCompraSisrpInterno);
                    Log::channel('gestao_ata')->error(json_encode($dadosItemCompra));
                    throw new \Exception($mensagemErro, $dadosItemCompra['codigoRetorno']);
                    continue;
                }
                
                # Salva os itens para os participantes
                $this->salvarItemSisrp($dadosItemCompra, $uasg->tipoUasg, $dadosCompraObjeto);
            }
        }
    }
}
