<?php

namespace App\Http\Controllers\FornecedorCompras;

use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoSetupRootTrait;
use App\Http\Traits\ImportContent;
use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use App\Models\AutorizacaoExecucao;
use App\Models\Contrato;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use App\Services\UsuarioFornecedor\UsuarioFornecedorService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Library\Widget;
use Illuminate\Http\Request;

class UsuarioFornecedorAutorizacaoExecucaoCrudController extends CrudController
{
    use AutorizacaoexecucaoSetupRootTrait;
    use UsuarioFornecedorTrait;
    use ListOperation;
    use ShowOperation;
    use ImportContent;

    protected $autorizacaoExecucaoService;
    protected $usuarioFornecedorService;
    private $administradorFornec;
    public $contrato_id;
    private $contrato;

    public function __construct(
        AutorizacaoExecucaoService $autorizacaoExecucaoService,
        UsuarioFornecedorService $usuarioFornecedorService
    ) {
        parent::__construct();

        $this->autorizacaoExecucaoService = $autorizacaoExecucaoService;
        $this->usuarioFornecedorService = $usuarioFornecedorService;
    }

    public function setup()
    {
        $this->permissionRule([
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);

        $this->administradorFornec = $this->usuarioFornecedorService->verificaAdministradorOuPrespostoFornecedorLogado(
            backpack_user(),
            session('fornecedor_id')
        );

        $this->verificarPermissaoByContrato();
        $this->verificarPermissaoByFornecedor();

        $this->setupRoot();

        CRUD::addButtonFromView(
            'line',
            'button_assinar_autorizacaoexecucao',
            'button_assinar_autorizacaoexecucao',
            'end'
        );

        $this->contrato_id = \Route::current()->parameter('contrato_id') ?: null;

        if ($this->contrato_id) {
            //dd('contrato');
            $this->contrato = Contrato::find($this->contrato_id);
            CRUD::addClause('where', 'autorizacaoexecucoes.contrato_id', '=', $this->contrato->id);
            CRUD::setEntityNameStrings(
                "Ordem de Serviço / Fornecimento",
                "Ordens de Serviço / Fornecimento do Contrato {$this->contrato->numero} - 
                    {$this->contrato->unidade->codigo}"
            );
            $this->crud->menuAcessoRapido = false;
            CRUD::setRoute(
                config('backpack.base.route_prefix') . "/fornecedor/autorizacaoexecucao/{$this->contrato->id}"
            );
        } else {
            // dd('acesso rapido');
            CRUD::setEntityNameStrings(
                "Ordem de Serviço / Fornecimento",
                "Ordens de Serviço / Fornecimento do Fornecedor"
            );
            $this->crud->menuAcessoRapido = true;
            CRUD::setRoute(config('backpack.base.route_prefix') . "/fornecedor/autorizacaoexecucao");
        }

        CRUD::addClause('join', 'contratos', 'contratos.id', '=', 'autorizacaoexecucoes.contrato_id');
        CRUD::addClause('join', 'fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id');
        CRUD::addClause('join', 'codigoitens', 'codigoitens.id', '=', 'autorizacaoexecucoes.situacao_id');
        CRUD::addClause('where', 'contratos.fornecedor_id', '=', session('fornecedor_id'));

        // if (!$this->administradorFornec) {
        CRUD::addClause('where', 'codigoitens.descres', '!=', 'ae_status_1');
        // }
        $this->adicionarQueryFiltroPreposto($this->administradorFornec);

        $this->crud->denyAccess(['create', 'update', 'delete']);

        $this->crud->setListView('vendor.backpack.crud.usuario-fornecedor.list');
        $this->crud->setShowView('vendor.backpack.crud.usuario-fornecedor.show');

        $this->crud->urlAutorizacaoExecucaoEntrega =
            config('backpack.base.route_prefix') . "/fornecedor/autorizacaoexecucao";
    }

    protected function setupListOperation()
    {
        $this->permissionRule([
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);

        $this->crud->setOperationSetting('persistentTable', false);

        if ($this->contrato_id) {
            $this->bredcrumbs(
                $this->administradorFornec,
                'Ordem de Serviço / Fornecimento',
                'Contratos',
                url('fornecedor/contrato'),
                true
            );
        } else {
            $this->bredcrumbs(
                $this->administradorFornec,
                'Ordem de Serviço / Fornecimento',
                null,
                url("fornecedor/inicio/" . strtolower(session()->get('tipo_acesso'))),
                true
            );
        }

        $this->setupListOperationRoot($this->contrato_id, session('fornecedor_id'));

        $this->crud->addButtonFromView(
            'line',
            'os_entregar_servicos_bens',
            'os_entregar_servicos_bens',
            'end'
        );

        $this->crud->addButtonFromView(
            'line',
            'button_download_osf_on_list',
            'button_download_osf_on_list',
            'end'
        );

        $this->crud->addButtonFromView(
            'line',
            'button_resumo_osf_fornecedor',
            'button_resumo_osf_fornecedor',
            'beginning'
        );

        $this->importarScriptJs([
            'assets/js/autorizacaoexecucao/quadro_resumo_osf.js'
        ]);

        Widget::add([
            'type'     => 'view',
            'view'     => 'vendor.backpack.base.widgets.modal-quadro-resumo-osf',
            'id' => 'modalquadroresumo',
        ]);
    }

    protected function setupShowOperation()
    {
        $this->permissionRule([
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);


        if ($this->contrato_id) {
            $this->bredcrumbs(
                $this->administradorFornec,
                'Visualizar',
                'Contratos',
                url('fornecedor/autorizacaoexecucao/' . $this->contrato->id),
                true
            );
        } else {
            $this->bredcrumbs(
                $this->administradorFornec,
                'Visualizar',
                null,
                url('fornecedor/autorizacaoexecucao'),
                true
            );
        }


        $this->setupShowOperationRoot();
    }

    public function unidadesFornecedor(Request $request)
    {
        return $this->unidadesFornecedorTrait($request, $this->administradorFornec);
    }

    public function empenhosFornecedor(Request $request)
    {
        return $this->empenhosFornecedorTrait($request, $this->administradorFornec);
    }

    public function informacoesQuadroResumo(Request $request)
    {
        return $this->autorizacaoExecucaoService
            ->getQuantidadeTotalAutorizacaoExecucao($request->autorizacao_execucao_id);
    }
}
