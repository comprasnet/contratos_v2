<?php

namespace App\Http\Controllers\FornecedorCompras;

use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Http\Traits\BuscaCodigoItens;
use App\Models\Contratofatura;
use App\Models\AmparoLegal;
use App\Models\Codigoitem;
use App\Models\Contrato;
use App\Models\ContratoFaturaEmpenho;
use App\Models\ContratoFaturaMesAno;
use App\Models\ContratoFaturasItens;
use App\Models\Feriado;
use App\Models\Fornecedor;
use App\Models\FornecedorSicaf;
use App\Models\Paises;
use App\Models\Saldohistoricoitem;
use App\Models\Contratohistorico;
use App\Models\Empenho;
use App\Models\Tipolistafatura;
use App\Models\Unidade;
use App\Services\UsuarioFornecedor\UsuarioFornecedorService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class UsuarioFornecedorContratoInstrumentoDeCobrancaCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use UsuarioFornecedorTrait;
    use CommonColumns;
    use Formatador;
    use BuscaCodigoItens;

    protected $usuarioFornecedorService;
    private $administradorFornec;
    public $contrato_id;

    public function __construct(UsuarioFornecedorService $usuarioFornecedorService)
    {
        parent::__construct();
        $this->usuarioFornecedorService = $usuarioFornecedorService;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
       // dd(\Route::current()->parameter('contrato_id'));
        $this->permissionRule([
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);

        $this->contrato_id = \Route::current()->parameter('contrato_id') ?: null;
        //dd($this->contrato_id);

        $this->administradorFornec = $this->usuarioFornecedorService->verificaAdministradorOuPrespostoFornecedorLogado(
            backpack_user(),
            session('fornecedor_id')
        );
        //dd(\Route::current()->parameter('contrato_id'));

        CRUD::setModel(Contratofatura::class);

        $url = '/fornecedor/contratoinstrumentodecobranca';

        if ($this->contrato_id != null) {
            $url =  "/fornecedor/contratoinstrumentodecobranca/{$this->contrato_id}";
        }

        CRUD::setRoute(config('backpack.base.route_prefix') . $url);

        if ($this->contrato_id != null) {
            CRUD::setEntityNameStrings('Instrumento de Cobrança do Contrato', 'Instrumentos de Cobrança do Contrato');
        }
        if ($this->contrato_id == null) {
            CRUD::setEntityNameStrings(
                'Instrumento de Cobrança do Fornecedor',
                'Instrumentos de Cobrança do Fornecedor'
            );
        }
        CRUD::addClause('join', 'tipolistafatura', 'tipolistafatura.id', '=', 'contratofaturas.tipolistafatura_id');
        CRUD::addClause('join', 'contratos', 'contratos.id', '=', 'contratofaturas.contrato_id');
        CRUD::addClause('join', 'fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id');
        $this->adicionarQueryFiltroPreposto($this->administradorFornec);
        /*if (!$this->administradorFornec) {
            //dd('asdrubal preposto');
            CRUD::addClause('join', 'contratopreposto', 'contratos.id', '=', 'contratopreposto.contrato_id');
            CRUD::addClause('where', 'contratopreposto.cpf', '=', backpack_user()->cpf);
            CRUD::addClause('where', 'contratopreposto.situacao', '=', true);
        }*/
        if ($this->contrato_id != null) {
            CRUD::addClause('where', 'contrato_id', '=', $this->contrato_id);
        }
        CRUD::addClause('where', 'contratos.fornecedor_id', '=', session('fornecedor_id'));
        CRUD::addClause('select', 'contratofaturas.*');

        $this->crud->setListView('vendor.backpack.crud.usuario-fornecedor.list');
        $this->crud->setShowView('vendor.backpack.crud.usuario-fornecedor.show');

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->permissionRule([
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);

        $this->crud->setOperationSetting('persistentTable', false);

        $this->crud->addColumn([
            'name' => 'get_contrato',
            'label' => 'Contrato',
            'type' => 'model_function',
            'function_name' => 'getContrato',
        ]);

        $this->crud->addColumn([
        'name' => 'get_tipo_lista_fatura',
        'label' => 'Tipo Lista',
        'type' => 'model_function',
        'function_name' => 'getTipoListaFatura',
        'limit' => 1000,
        'searchLogic' => function (Builder $query, $column, $searchTerm) {
            $query->orWhere('tipolistafatura.nome', 'like', "%" . strtoupper($searchTerm) . "%");
        },
        ]);

        $this->crud->addColumn([
            'name' => 'tipoDeInstrumentoDeCobranca',
            'label' => 'Tipo de Instrumento',
            'type' => 'text',
            'limit' => 1000
        ]);

        $this->crud->addColumn([
            'name' => 'get_justica_fatura',
            'label' => 'Justificativa',
            'type' => 'model_function',
            'function_name' => 'getJustificativaFatura',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
                'name' => 'get_sfpadrao',
                'label' => 'Doc. Origem Siafi',
                'type' => 'model_function',
                'function_name' => 'getSfpadrao',
                'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'numero',
            'label' => 'Número',
            'type' => 'text'
        ]);

        $this->crud->addColumn([
            'name' => 'chave_nfe',
            'label' => 'Chave NFe',
            'type' => 'text',
            'visibleInTable' => false
        ]);

         $this->crud->addColumn([
             'name' => 'emissao',
             'label' => 'Dt. Emissão',
             'type' => 'date',
         ]);

         $this->crud->addColumn([
             'name' => 'vencimento',
             'label' => 'Dt. Limite de Pagamento',
             'type' => 'date',
         ]);

         $this->crud->addColumn([
             'name' => 'retornar_valor_total',
             'label' => 'Valor',
             'type' => 'model_function',
             'function_name' => 'retornarValorTotalPerfilFornec'
         ]);

        $this->crud->addColumn([
                'name' => 'format_juros',
                'label' => 'Juros',
                'type' => 'model_function',
                'function_name' => 'formatJuros',
                'visibleInTable' => false
        ]);

         $this->crud->addColumn([
             'name' => 'format_multa',
             'label' => 'Multa',
             'type' => 'model_function',
             'function_name' => 'formatMulta',
             'visibleInTable' => false
         ]);

         $this->crud->addColumn([
             'name' => 'format_glosa',
             'label' => 'Glosa',
             'type' => 'model_function',
             'function_name' => 'formatGlosa',
             'visibleInTable' => false
         ]);

        $this->crud->addColumn([
            'name' => 'format_valor_faturado',
            'label' => 'Valor Faturado',
            'type' => 'model_function',
            'function_name' => 'formatValorFaturado'
        ]);

        $this->crud->addColumn([
            'name' => 'processo',
            'label' => 'Processo',
            'type' => 'text',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'protocolo',
            'label' => 'Dt. Recebimento',
            'type' => 'date',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'ateste',
            'label' => 'Dt. Liquidação de Despesa',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'prazo',
            'label' => 'Dt. Prazo Pagto.',
            'type' => 'date',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'empenhos',
            'label' => 'Empenhos',
            'type' => 'select_multiple',
            'entity' => 'empenhos',
            'attribute' => 'numero',
            'model' => Empenho::class,
            'pivot' => true
        ]);

        $this->crud->addColumn([
            'name' => 'repactuacao',
            'label' => 'Repactuação',
            'type' => 'boolean',
            'visibleInTable' => false,
            'options' => [0 => 'Não', 1 => 'Sim']
        ]);

        $this->crud->addColumn([
            'name' => 'repactuacao',
            'label' => 'Repactuação',
            'type' => 'boolean',
            'visibleInTable' => false,
            'options' => [0 => 'Não', 1 => 'Sim']
        ]);

        $this->crud->addColumn([
            'name' => 'infcomplementar',
            'label' => 'Informações Complementares',
            'type' => 'text',
            'visibleInTable' => false,
            'limit' => 1000
        ]);

        $this->crud->addColumn([
            'name' => 'mes_ano_valor_fatura',
            'label' => 'Ano / Mês Referência',
            'type' => 'mes_ano_valor_fatura',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'situacao',
            'label' => 'Situação',
            'type' => 'select_from_array',
            'options' => config('app.situacao_fatura')
        ]);

        $this->crud->addButtonFromView(
            'line',
            'button_transparencia_instrumeto_cobranca_fornecedor',
            'button_transparencia_instrumeto_cobranca_fornecedor',
            'end'
        );

        $this->crud->enableExportButtons();

        $this->adicionaFiltros();

        if ($this->contrato_id) {
            $this->bredcrumbs(
                $this->administradorFornec,
                'Instrumentos de Cobrança',
                'Contratos',
                url('fornecedor/contrato'),
                true
            );
        } else {
            $this->bredcrumbs(
                $this->administradorFornec,
                'Instrumentos de Cobrança',
                null,
                url("fornecedor/inicio/" . strtolower(session()->get('tipo_acesso'))),
                true
            );
        }
    }

    protected function setupShowOperation()
    {
        $this->permissionRule([
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);

        $this->addColumnModelFunction(
            'get_contrato',
            'Contrato',
            'getContrato'
        );
        $this->addColumnModelFunction(
            'get_tipo_lista_fatura',
            'Tipo Lista',
            'getTipoListaFatura'
        );
        $this->addColumnText(
            true,
            true,
            true,
            true,
            'tipoDeInstrumentoDeCobranca',
            'Tipo de Instrumento'
        );
        $this->addColumnModelFunction(
            'get_justica_fatura',
            'Justificativa',
            'getJustificativaFatura'
        );
        $this->addColumnModelFunction(
            'tipoDeInstrumentoDeCobranca',
            'Tipo de Instrumento',
            'getTipoListaFatura'
        );
        $this->addColumnModelFunction('get_sfpadrao', 'Doc. Origem Siafi', 'getSfpadrao');
        $this->addColumnText(
            true,
            true,
            true,
            true,
            'tipoDeInstrumentoDeCobranca',
            'Tipo de Instrumento'
        );
        $this->addColumnText(
            true,
            true,
            true,
            true,
            'numero',
            'Número'
        );
        $this->addColumnText(
            true,
            true,
            true,
            true,
            'emissao',
            'Dt. Emissão'
        );
        $this->addColumnDate(
            true,
            true,
            true,
            true,
            'vencimento',
            'Dt. Limite de Pagamento'
        );
        $this->addColumnModelFunction('retornar_valor_total', 'Valor', 'retornarValorTotalPerfilFornec');
        $this->addColumnModelFunction('format_juros', 'Juros', 'formatJuros');
        $this->addColumnModelFunction('format_multa', 'Multa', 'formatMulta');
        $this->addColumnModelFunction('format_glosa', 'Processo', 'formatGlosa');
        $this->addColumnModelFunction(
            'format_valor_faturado',
            'Valor Faturado',
            'formatValorFaturado'
        );
        $this->addColumnText(
            true,
            true,
            true,
            true,
            'processo',
            'Processo'
        );
        $this->addColumnDate(
            true,
            true,
            true,
            true,
            'protocolo',
            'Dt. Recebimento'
        );
        $this->addColumnDate(
            true,
            true,
            true,
            true,
            'ateste',
            'Dt. Liquidação de Despesa'
        );
        $this->addColumnDate(
            true,
            true,
            true,
            true,
            'prazo',
            'Dt. Prazo Pagto.'
        );
        $this->addColumnTableEmpenhos('empenho', 'Empenhos');
        $this->addColumnText(
            true,
            true,
            true,
            true,
            'repactuacao',
            'Repactuação'
        );
        $this->crud->addColumn([
            'name' => 'repactuacao',
            'label' => 'Repactuação',
            'type' => 'boolean',
            'visibleInTable' => false,
            'options' => [0 => 'Não', 1 => 'Sim']
        ]);
        $this->addColumnTextArea(
            false,
            true,
            true,
            true,
            'infcomplementar',
            'Informações Complementares'
        );
        $this->crud->addColumn([
            'name' => 'mes_ano_valor_fatura',
            'label' => 'Ano / Mês Referência',
            'type' => 'mes_ano_valor_fatura',
            'visibleInTable' => false
        ]);
        $this->addColumnArquivos('arquivos', 'Arquivos', 'local');
        $this->addColumnText(
            true,
            true,
            true,
            true,
            'situacao',
            'Situação'
        );

        if ($this->contrato_id) {
            $this->bredcrumbs(
                $this->administradorFornec,
                'Visualizar',
                'Instrumentos de Cobrança',
                url('fornecedor/contratoinstrumentodecobranca/' . $this->contrato_id),
                true
            );
        } else {
            $this->bredcrumbs(
                $this->administradorFornec,
                'Visualizar',
                null,
                url('fornecedor/contratoinstrumentodecobranca'),
                true
            );
        }
    }

    public function adicionaFiltros()
    {

        if ($this->contrato_id == null) {
            //contrato
            $this->crud->addFilter(
                [
                    'name' => 'contrato',
                    'type' => 'select2',
                    'label' => 'Núm. Contrato'
                ],
                $this->usuarioFornecedorService->retornaFiltroContratosFornecedorService(),
                function ($value) {
                    $this->crud->addClause('where', 'contratos.numero', $value);
                }
            );
        }

        //número fatura
        $this->crud->addFilter(
            [
            'name' => 'numero',
            'type' => 'text',
            'label' => 'Núm. Fatura'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'contratofaturas.numero', $value);
            }
        );

        //tipo lista
        $this->crud->addFilter(
            [
            'name' => 'tipo_lista',
            'type' => 'select2',
            'label' => 'Tipo Lista'
            ],
            $this->usuarioFornecedorService->retornaTiposListaService(),
            function ($value) {
                $this->crud->addClause('where', 'contratofaturas.tipolistafatura_id', $value);
            }
        );

        //justificativa
        $this->crud->addFilter(
            [
            'name' => 'justificativa',
            'type' => 'select2',
            'label' => 'Justificativa'
            ],
            $this->usuarioFornecedorService->retornaJustificativasService(),
            function ($value) {
                $this->crud->addClause('where', 'contratofaturas.justificativafatura_id', $value);
            }
        );

        //data de emissão
        $this->crud->addFilter(
            [
                'name'  => 'dt_emissao',
                'type'  => 'date_range',
                'label' => 'Dt. Emissão'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $fromDate = Carbon::parse($dates->from);
                $toDate = Carbon::parse($dates->to)->endOfDay();

                $this->crud->addClause('where', 'contratofaturas.emissao', '>=', $fromDate);
                $this->crud->addClause('where', 'contratofaturas.emissao', '<=', $toDate);
            }
        );

        //data de ateste
        $this->crud->addFilter(
            [
                'name'  => 'dt_ateste',
                'type'  => 'date_range',
                'label' => 'Dt. Ateste'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $fromDate = Carbon::parse($dates->from);
                $toDate = Carbon::parse($dates->to)->endOfDay();

                $this->crud->addClause('where', 'contratofaturas.ateste', '>=', $fromDate);
                $this->crud->addClause('where', 'contratofaturas.ateste', '<=', $toDate);
            }
        );

        //data de vencimento
        $this->crud->addFilter(
            [
            'name'  => 'dt_vencimento',
            'type'  => 'date_range',
            'label' => 'Dt. Vencimento'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $fromDate = Carbon::parse($dates->from);
                $toDate = Carbon::parse($dates->to)->endOfDay();

                $this->crud->addClause('where', 'contratofaturas.vencimento', '>=', $fromDate);
                $this->crud->addClause('where', 'contratofaturas.vencimento', '<=', $toDate);
            }
        );

        //prazo
        $this->crud->addFilter(
            [
            'name'  => 'dt_prazo',
            'type'  => 'date_range',
            'label' => 'Prazo Pagamento'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $fromDate = Carbon::parse($dates->from);
                $toDate = Carbon::parse($dates->to)->endOfDay();

                $this->crud->addClause('where', 'contratofaturas.prazo', '>=', $fromDate);
                $this->crud->addClause('where', 'contratofaturas.prazo', '<=', $toDate);
            }
        );

        //data do protocolo
        $this->crud->addFilter(
            [
            'name'  => 'dt_protocolo',
            'type'  => 'date_range',
            'label' => 'Dt. Protocolo'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $fromDate = Carbon::parse($dates->from);
                $toDate = Carbon::parse($dates->to)->endOfDay();

                $this->crud->addClause('where', 'contratofaturas.protocolo', '>=', $fromDate);
                $this->crud->addClause('where', 'contratofaturas.protocolo', '<=', $toDate);
            }
        );

        //situacao
        $this->crud->addFilter(
            [
            'name'  => 'situacao',
            'type'  => 'select2_multiple',
            'label' => 'Situação'
            ],
            $situacoes = config('app.situacao_fatura'),
            function ($value) {
                $this->crud->addClause('whereIn', 'contratofaturas.situacao', json_decode($value));
            }
        );
    }
}
