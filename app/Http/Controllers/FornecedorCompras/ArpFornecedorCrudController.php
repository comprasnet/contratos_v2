<?php

namespace App\Http\Controllers\FornecedorCompras;

use App\Http\Controllers\Api\ConcentradorCompras\CompraSisrp;
use App\Http\Controllers\Operations\DeleteOperation;
use App\Http\Controllers\Operations\UpdateOperation;
use App\Http\Requests\ArpRequest as StoreRequest;
use App\Http\Requests\ArpUpdateRequest;
use App\Http\Traits\Arp\ArpItemHistoricoTrait;
use App\Http\Traits\Arp\ArpAlteracaoTrait;
use App\Http\Traits\Arp\ArpTrait;
use App\Http\Traits\Arp\ArpItemTrait;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\CommonFields;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\Formatador;
use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use App\Models\ArpArquivo;
use App\Models\ArpArquivoTipo;
use App\Models\CompraItem;
use App\Models\Compras;
use App\Repositories\Compra\CompraItemFornecedorRepository;
use App\Services\Pncp\PncpService;
use App\Services\UsuarioFornecedor\UsuarioFornecedorService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Traits\ImportContent;
use App\Http\Traits\JobUserTrait;
use App\Http\Traits\LogTrait;
use App\Jobs\CarregarUnidadeParticipanteCompraGestaoAtaJOB;
use App\Models\Arp;
use App\Models\ArpAlteracao;
use App\Models\ArpAutoridadeSignataria;
use App\Models\ArpAutoridadeSignatariaHistorico;
use App\Models\ArpGestor;
use App\Models\ArpHistorico;
use App\Models\ArpItem;
use App\Models\ArpItemHistorico;
use App\Models\ArpUnidades;
use App\Models\Autoridadesignataria;
use App\Models\CompraItemFornecedor;
use App\Models\CompraItemUnidade;
use App\Models\FailedJob;
use App\Models\JobUser;
use App\Models\Unidade;
use App\Http\Traits\Pncp\ArpPncpTrait;
use App\Models\UnidadeConfiguracao;
use App\Models\User;
use App\Services\Pncp\Arp\ConsultarTodasArpPncpService;
use App\Services\Pncp\Arp\InserirArquivoArpPncpService;
use App\Services\Pncp\Arp\InsertArpPncpService;
use App\Services\Pncp\PncpManagerService;
use App\Services\Pncp\Responses\PncpGetArpUnicaResponseService;
use App\Services\Pncp\Responses\PncpPostResponseService;
use DataTables;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;
use App\Http\Traits\Unidade\ProcessoTrait as ProcessoControllerTrait;

/**
 * Class ArpFornecedorCrudController
 * @package App\Http\Controllers\FornecedorCompras
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArpFornecedorCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use BuscaCodigoItens;
    use CommonFields;
    use CompraTrait;
    use Formatador;
    use ImportContent;
    use ArpTrait;
    use JobUserTrait;
    use ArpAlteracaoTrait;
    use LogTrait;
    use ArpPncpTrait;
    use ArpItemHistoricoTrait;
    use ArpItemTrait;
    use ProcessoControllerTrait;
    use UsuarioFornecedorTrait;

    protected $usuarioFornecedorService;

    private $administradorFornec;

    public function __construct(UsuarioFornecedorService $usuarioFornecedorService)
    {
        parent::__construct();
        $this->usuarioFornecedorService = $usuarioFornecedorService;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->permissionRule([
            'gestaodeatas_V2_acessar',
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);
        CRUD::setModel(\App\Models\Arp::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/fornecedor/arp');

        #CRUD::addClause('where', 'arp.unidade_origem_id', '=', session('user_ug_id'));
        $hoje = Carbon::now()->toDateString();

        $this->administradorFornec = $this->usuarioFornecedorService->verificaAdministradorOuPrespostoFornecedorLogado(
            backpack_user(),
            session('fornecedor_id')
        );
        # Busca todas atas que possuem vínculo com a unidade logada
        CRUD::addClause('select', 'arp.*');
        CRUD::addClause('distinct');
        CRUD::addClause('join', 'compras', 'compras.id', '=', 'arp.compra_id');
        CRUD::addClause('join', 'unidades', 'unidades.id', '=', 'arp.unidade_origem_id');
        CRUD::addClause('join', 'compra_items', function ($join) {
            $join->on('arp.compra_id', '=', 'compra_items.compra_id')->where('compra_items.situacao', true);
        });
        CRUD::addClause('join', 'arp_item', function ($join) {
            $join->on('arp_item.arp_id', 'arp.id')->whereNull('arp_item.deleted_at');
        });
        CRUD::addClause('join', 'compra_item_fornecedor', function ($join) {
            $join->on('compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
                ->where('compra_item_fornecedor.situacao', true)
                ->whereColumn('compra_item_fornecedor.id', 'arp_item.compra_item_fornecedor_id');
        });
        CRUD::addClause('join', 'compra_item_unidade', 'compra_items.id', '=', 'compra_item_unidade.compra_item_id');
        CRUD::addClause('join', 'codigoitens', 'arp.tipo_id', '=', 'codigoitens.id');
        
        $redirectVencimento = $this->crud->getRequest()->query()['redirect_vencimento'] ?? 0 ;
        if ($redirectVencimento) {
            $hoje = Carbon::now()->toDateString();
            CRUD::addClause(
                'whereRaw',
                "('{$hoje}' between vigencia_inicial  and  vigencia_final)"
            );
        }
        
        CRUD::addClause('where', 'compra_item_fornecedor.fornecedor_id', '=', session('fornecedor_id'));
        CRUD::addClause('where', 'compra_item_fornecedor.situacao', true);
        CRUD::addClause('where', 'compra_items.situacao', true);
        CRUD::addClause('where', 'compra_item_unidade.situacao', true);
        CRUD::addClause('whereNotIn', 'codigoitens.descricao', ['Em elaboração', 'Cancelada']);
        CRUD::addClause('where', 'arp.rascunho', false);
        CRUD::addClause('whereNull', 'compra_item_unidade.deleted_at');
        CRUD::addClause('whereNull', 'arp_item.deleted_at');
        CRUD::addClause('whereNull', 'compra_item_fornecedor.deleted_at');
        CRUD::addClause('whereNull', 'compra_items.deleted_at');

        CRUD::addClause('orderByDesc', 'arp.numero');

        CRUD::groupBy('arp.id');

        #dd(Str::replaceArray('?', $this->crud->query->getBindings(), $this->crud->query->toSql()));
        $this->crud->addButtonFromModelFunction('line', 'getViewButtonArp', 'getViewButtonArp', 'end');
        $this->crud->addButtonFromModelFunction('line', 'getViewButtonAlterar', 'getViewButtonAlterar', 'end');

        $this->exibirTituloPaginaMenu('Ata de Registro de Preços');

        $this->bloquearBotaoPadrao($this->crud, ['create', 'update', 'delete']);

        $this->crud->setListView('vendor.backpack.crud.usuario-fornecedor.list');
        $this->crud->setShowView('vendor.backpack.crud.usuario-fornecedor.show');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->permissionRule([
            'gestaodeatas_V2_acessar',
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);

        CRUD::setSubheading('Listar', 'index');

        $this->addColumnModelFunction('status_arp', 'Situação', 'getSituacaoPersonalizada');

        $this->visualizarListShow();
        $this->addColumnValorMonteraio('valor_total', 'Valor total');
        $this->crud->addButtonFromView(
            'line',
            'arp_arquivo_lista',
            'arp_arquivo_lista',
            'end'
        );
        $this->crud->addButtonFromView(
            'line',
            'arp_link_pncp',
            'arp_link_pncp',
            'end'
        );

        $this->crud->enableExportButtons();
        $this->crud->prefix_text_button_redirect_create = 'Criar';
        $this->crud->text_button_redirect_create = 'Ata';

        $this->bredcrumbs($this->administradorFornec, 'Ata de Registro de Preços');
        
        $this->crud->addFilter(
            [
                'name'  => 'vigencia_final',
                'type'  => 'date_range',
                'label' => 'Vigência Fim'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $fromDate = Carbon::parse($dates->from);
                $toDate = Carbon::parse($dates->to)->endOfDay();
                
                $this->crud->addClause('where', 'arp.vigencia_final', '>=', $fromDate);
                $this->crud->addClause('where', 'arp.vigencia_final', '<=', $toDate);
            }
        );
    }

    private function visualizarListShow()
    {
        $this->addColumnModelFunction('status_arp', 'Situação', 'getSituacaoPersonalizada');
        $this->addColumnModelFunction(
            'numero_ano',
            'Número',
            'getNumeroAno',
            ['table' => true, 'modal' => true, 'show' => true, 'export' => true],
            99999999999999999,
            "concat(arp.numero, '/' ,ano)"
        );
        $this->addColumnDate(
            true,
            true,
            true,
            true,
            'vigencia_inicial',
            'Vigência inicial'
        );
        $this->addColumnDate(
            true,
            true,
            true,
            true,
            'vigencia_final',
            'Vigência final'
        );
        $this->addColumnModelFunction(
            'unidade_origem',
            'Unidade gerenciadora da ata',
            'getUnidadeOrigem'
        );
        // $this->addColumnModelFunction('fornecedor', 'Fornecedor', 'getFornecedorPorAta');
    }

    protected function setupShowOperation()
    {
        $this->permissionRule([
            'gestaodeatas_V2_acessar',
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);
        $this->crud->set('show.contentClass', 'col-md-12');

        $this->crud->set('show.setFromDb', false);

        $idAta = Route::current()->parameter("id");

        $this->addColumnModelFunction('numero_ano', 'Número', 'getNumeroAno');

        $this->visualizarListShow();
        $this->addColumnModelFunction(
            'numero_compra',
            'Número da compra/Ano',
            'getNumeroCompra'
        );
        CRUD::addColumn([
            'name' => 'numero_processo',
            'label' => 'Número do processo',
            'visibleInShow' => true,
            // 'visibleInExport' => $export
        ]);

        $this->addColumnDate(true, true, true, true, 'data_assinatura', 'Data da assinatura');
        $this->addColumnValorMonteraio('valor_total', 'Valor total');

        $autoridades = Arp::getAutoridadeSignataria($idAta);
        $this->addColumnTable('autoridade_signataria_ata', 'Autoridades', $autoridades);

        $this->addColumnModelFunction('modalidade_compra', 'Modalidade da compra', 'getModalidadeCompra');

        $this->addColumnModelFunction('compra_centralizada', 'Compra centralizada', 'getCompraCentralizada');

        $unidadeParticipante = Arp::getUnidadeParticipante($idAta);
        $this->addColumnTable('unidade_participante', 'Unidade participante', $unidadeParticipante);

        $item = Arp::getItemAta($idAta);
        $this->addColumnTable('item_ata', 'Item da ata', $item);

        // $this->addColumnModelFunction('rascunho', 'Status', 'getStatus');

        $arp = $this->crud->getModel()->findOrFail($idAta);

        if (!$arp->rascunho) {
            $this->crud->denyAccess('update');
            $this->crud->denyAccess('delete');
        }

        $this->bredcrumbs(
            $this->administradorFornec,
            'Visualizar',
            'Ata de Registro de Preços',
            url('fornecedor/arp'),
            true
        );
    }
}
