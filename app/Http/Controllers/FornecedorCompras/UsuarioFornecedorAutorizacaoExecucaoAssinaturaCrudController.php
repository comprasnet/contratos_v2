<?php

namespace App\Http\Controllers\FornecedorCompras;

use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoCrudTrait;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use App\Models\AutorizacaoExecucao;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoSignatarioService;
use App\Services\UsuarioFornecedor\UsuarioFornecedorService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UsuarioFornecedorAutorizacaoExecucaoAssinaturaCrudController extends CrudController
{
    use AutorizacaoexecucaoCrudTrait;
    use CreateOperation;
    use CommonColumns;
    use Formatador;
    use UsuarioFornecedorTrait;

    private $aeSignatarioService;
    private $aeSignatario;
    private $arquivoGenerico;
    private $administradorFornec;
    protected $usuarioFornecedorService;

    public function __construct(
        UsuarioFornecedorService $usuarioFornecedorService
    ) {
        parent::__construct();

        $this->usuarioFornecedorService = $usuarioFornecedorService;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->permissionRule([
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);

        $this->administradorFornec = $this->usuarioFornecedorService->verificaAdministradorOuPrespostoFornecedorLogado(
            backpack_user(),
            session('fornecedor_id')
        );

        $administradorSessao = session()->get('tipo_acesso') == 'Administrador';

        $this->verificarPermissaoByContrato();

        $this->autorizacaoexecucao = AutorizacaoExecucao::findOrFail(request()->autorizacaoexecucao_id);

        $this->aeSignatarioService = new AutorizacaoExecucaoSignatarioService($this->autorizacaoexecucao);

        if (!$administradorSessao) {
            $this->aeSignatario = $this->aeSignatarioService->getSignatarioByUsuario();
        } else {
            $this->aeSignatario = $this->aeSignatarioService->getPrimeiroSignatarioPreposto();
        }

        $this->contrato = $this->getContrato($administradorSessao);

        $this->arquivoGenerico = $this->autorizacaoexecucao->getArquivo();


        CRUD::setModel(AutorizacaoExecucao::class);
        CRUD::addClause('where', 'id', $this->autorizacaoexecucao->id);

        $this->crud->setCreateView('vendor.backpack.crud.usuario-fornecedor.create');

        CRUD::setRoute(
            config('backpack.base.route_prefix') . "fornecedor/autorizacaoexecucao/" . request()->contrato_id
        );

        CRUD::setEntityNameStrings(
            "Ordem de Serviço / Fornecimento",
            "Ordens de Serviço / Fornecimento do Contrato {$this->contrato->numero} - 
                {$this->contrato->fornecedor->nome}"
        );

        CRUD::setSubheading('Assinar', 'create');
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->permissionRule([
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);

        $this->crud->setCreateContentClass('col-md-12');

        if ($this->aeSignatario) {
            $paginaAssinatura = $this->aeSignatario->pivot->pagina_assinatura;
            $posicaoXAssinatura = $this->aeSignatario->pivot->posicao_x_assinatura;
            $posicaoYAssinatura = $this->aeSignatario->pivot->posicao_y_assinatura;
            $routePrefix = config('backpack.base.route_prefix');
            $route = url($routePrefix . "/fornecedor/assinagov/autorizacao");
            $routeWithParams = $route . '?' . http_build_query([
                    'pathArquivo' => $this->arquivoGenerico->url,
                    'pagina_assinatura' => $paginaAssinatura,
                    'posicao_x_assinatura' => $posicaoXAssinatura,
                    'posicao_y_assinatura' => $posicaoYAssinatura,
                ]);

            CRUD::setRoute($routeWithParams);
        }


        $this->crud->addField([
            'name' => 'pdf_preview',
            'type' => 'custom_html',
            'value' => '
                <iframe class="pdf" style="width: 100%; aspect-ratio: 4 / 3; height: 100%" 
                    src="' . Storage::url($this->arquivoGenerico->url) . '?temp=' . time() . '"
                    width="800" height="500">
                </iframe>',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $autorizacaoExecucaoService = new AutorizacaoExecucaoService();
        $autorizacaoExecucaoService->setContrato($this->contrato);
        $autorizacaoExecucaoService->setAutorizacaoExecucao($this->autorizacaoexecucao);

        $urlAnterior = request()->headers->get('Referer');
        $arrayUrl = explode('/', $urlAnterior);
        $ultimaQuebra = end($arrayUrl);
        $this->menuAcessoContrato = true;

        if (!is_numeric($ultimaQuebra)) {
            $this->menuAcessoContrato = false;
        }

        $urlIntermediaria =
            $this->menuAcessoContrato
                ? url("fornecedor/autorizacaoexecucao/" . request()->contrato_id)
                : url("fornecedor/autorizacaoexecucao/");

        $this->crud->button_custom[] = [
            'button_text' => 'Cancelar', 'button_id' => 'assinar',
            'button_name_action' => 'voltar', 'button_value_action' => '0',
            'button_tipo' => 'secondary',
            'onclick' =>  "bloquearSubmit = true;window.location.href='{$urlIntermediaria}'"
        ];

        if ($this->aeSignatarioService->verifyUsuarioCanAssinar()) {
            $this->crud->button_custom[] = [
                'button_text' => 'Assinar', 'button_id' => 'assinar',
                'button_name_action' => 'recusar_assinatura', 'button_value_action' => '0',
                'button_tipo' => 'primary',
            ];
        }

        $this->bredcrumbs(
            $this->administradorFornec,
            'Assinar OSF',
            'Ordem de Serviço / Fornecimento',
            $urlIntermediaria,
            true
        );

        $this->crud->setOperationSetting('showCancelButton', false);
    }

    public function assinar(Request $request)
    {
        try {
            abort_if(
                !strstr(session()->get('_previous.url'), route('fornecedor.assinagov.token')) ||
                !$this->aeSignatarioService->verifyUsuarioCanAssinar(),
                403
            );

            DB::beginTransaction();

            $this->aeSignatarioService->assinar($this->aeSignatario);

            $pathArquivoExplodido = explode('.', $this->arquivoGenerico->url);

            Storage::disk('public')->move($this->arquivoGenerico->url, $this->arquivoGenerico->url . '.old');
            Storage::disk('public')->move(
                $pathArquivoExplodido[0] . '-assinado.' . $pathArquivoExplodido[1],
                $this->arquivoGenerico->url
            );
            Storage::disk('public')->delete($this->arquivoGenerico->url . '.old');

            DB::commit();

            \Alert::add('success', 'ordem de serviço / fornecimento assinada com sucesso.')->flash();
            return redirect(
                'fornecedor/autorizacaoexecucao/' . $this->contrato->id . '/' . request()->autorizacaoexecucao_id .
                '/assinatura/create'
            );
        } catch (Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao assinar a ordem de serviço / fornecimento.')->flash();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            return redirect()->back();
        }
    }
}
