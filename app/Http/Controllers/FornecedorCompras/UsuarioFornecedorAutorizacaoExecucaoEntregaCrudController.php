<?php

namespace App\Http\Controllers\FornecedorCompras;

use App\Http\Requests\FornecedorEntregaCancelamentoRequest;
use App\Http\Requests\UsuarioFornecedorAutorizacaoExecucaoEntregaRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Http\Traits\LogTrait;
use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use App\Http\Traits\ImportContent;
use App\Models\ArquivoGenerico;
use App\Models\AutorizacaoExecucaoEntrega;
use App\Models\CodigoItem;
use App\Models\Contrato;
use App\Models\ContratoLocalExecucao;
use App\Repositories\AutorizacaoExecucao\UsuarioFornecedorAutorizacaoExecucaoEntregaRepository;
use App\Repositories\Contrato\ContratoParametroRepository;
use App\Services\AutorizacaoExecucao\UsuarioFornecedorAutorizacaoExecucaoEntregaService;
use App\Services\UsuarioFornecedor\UsuarioFornecedorService;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Library\Widget;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class UsuarioFornecedorAutorizacaoExecucaoEntregaCrudController extends \Backpack\CRUD\app\Http\Controllers\CrudController
{

    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use UsuarioFornecedorTrait;
    use Formatador;
    use CommonColumns;
    use ImportContent;
    use LogTrait;

    protected $usuarioFornecedorService;

    private $administradorFornec;
    private $usuarioFornecedorAutorizacaoExecucaoEntregaService;
    private $menuAcessoContrato;

    public function __construct(
        UsuarioFornecedorService $usuarioFornecedorService,
        UsuarioFornecedorAutorizacaoExecucaoEntregaService $usuarioFornecedorAutorizacaoExecucaoEntregaService
    ) {
        parent::__construct();
        $this->usuarioFornecedorService = $usuarioFornecedorService;
        $this->usuarioFornecedorAutorizacaoExecucaoEntregaService = $usuarioFornecedorAutorizacaoExecucaoEntregaService;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->permissionRule([
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);

        $this->administradorFornec = $this->usuarioFornecedorService->verificaAdministradorOuPrespostoFornecedorLogado(
            backpack_user(),
            session('fornecedor_id')
        );

        $this->verificarPermissaoByOsf();

        //verificando se vem do contrato ou do menu acesso rápido
        $urlAnterior = request()->headers->get('Referer');
        $arrayUrl = explode('/', $urlAnterior);
        $ultimaQuebra = end($arrayUrl);
        $this->menuAcessoContrato = true;

        if (!is_numeric($ultimaQuebra)) {
            $this->menuAcessoContrato = false;
        }

        $this->autorizacaoexecucao_id = \Route::current()->parameter('autorizacaoexecucao_id') ?: null;

        CRUD::setModel(AutorizacaoExecucaoEntrega::class);
        CRUD::setRoute(
            config('backpack.base.route_prefix') .
            "fornecedor/autorizacaoexecucao/{$this->autorizacaoexecucao_id}/entrega"
        );
        CRUD::addClause('where', 'autorizacaoexecucao_id', $this->autorizacaoexecucao_id);

        CRUD::setEntityNameStrings(
            "Ordem de Serviço / Fornecimento - Entrega",
            "Ordens de Serviço / Fornecimento - Entregas"
        );

        $this->crud->prefix_text_button_redirect_create = 'Comunicar Entrega';

        $this->crud->addButtonFromModelFunction(
            'line',
            'getViewButtons',
            'getViewButtons',
        );

        CRUD::addButtonFromView('line', 'duplicar', 'duplicar', 'beginning');

        CRUD::addButtonFromView(
            'line',
            'button_pdf_trp_fornecedor',
            'button_pdf_trp_fornecedor',
            'end'
        );

        CRUD::addButtonFromView(
            'line',
            'button_pdf_trd_fornecedor',
            'button_pdf_trD_fornecedor',
            'end'
        );

        CRUD::addButtonFromView('line', 'fornecedor_entrega_cancelar', 'fornecedor_entrega_cancelar', 'end');

        $this->crud->autorizacaodeexecucao =
            $this->usuarioFornecedorAutorizacaoExecucaoEntregaService->retornaOrdemServicoService(
                $this->autorizacaoexecucao_id
            );

        CRUD::setEntityNameStrings(
            "Ordem de Serviço / Fornecimento Nº " .  $this->crud->autorizacaodeexecucao->numero,
            "Listagem de Entregas",
        );

        $this->crud->setListView('vendor.backpack.crud.autorizacaoexecucao.entrega.list');
        $this->crud->setCreateView('vendor.backpack.crud.autorizacaoexecucao.entrega.create');
        $this->crud->setUpdateView('vendor.backpack.crud.usuario-fornecedor.autorizacaoexecucao.entrega.edit');
        $this->crud->setShowView('vendor.backpack.crud.usuario-fornecedor.autorizacaoexecucao.entrega.show');

        $this->importarScriptJs(['assets/js/admin/forms/fornecedor/fornecedor_entrega.js']);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->permissionRule([
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);

        $this->crud->addColumn([
            'name' => 'situacao',
            'label' => 'Situação',
            'type' => 'model_function',
            'function_name' => 'retornaTextoSituacaoSpamColorido',
            'limit' => 9999,
        ]);

        $this->crud->addColumn([
            'name' => 'sequencial',
            'label' => 'Número Entrega',
            'type' => 'model_function',
            'function_name' => 'getFormattedSequencial'
        ]);

        $this->crud->addColumn([
            'name' => 'valor_entrega',
            'label' => 'Valor Entrega',
            'type' => 'model_function',
            'function_name' => 'retornaValorEntregaFormatado'
        ]);

        $this->crud->addColumn([
            'name' => 'data_entrega',
            'label' => 'Data Entrega',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_prazo_trp',
            'label' => 'Previsão para o TRP',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_prazo_trd',
            'label' => 'Previsão para o TRD',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_prazo_trp',
            'label' => 'Previsão para o TRP',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_prazo_trd',
            'label' => 'Previsão para o TRD',
            'type' => 'date'
        ]);

        $this->crud->enableExportButtons();

        if ($this->menuAcessoContrato) {
            $this->bredcrumbs(
                $this->administradorFornec,
                'Entregas OSF ' . $this->crud->autorizacaodeexecucao->numero,
                'Ordem de Serviço / Fornecimento',
                url("fornecedor/autorizacaoexecucao/{$this->crud->autorizacaodeexecucao->contrato_id}"),
                true
            );
        } else {
            $this->bredcrumbs(
                $this->administradorFornec,
                'Entregas OSF ' . $this->crud->autorizacaodeexecucao->numero,
                null,
                url("fornecedor/autorizacaoexecucao"),
                true
            );
        }

        Widget::add([
            'type'     => 'view',
            'view'     => 'vendor.backpack.base.widgets.modal-cancelar',
            'id' => 'modalcancelarentrega',
        ]);
    }

    protected function setupCreateOperation()
    {
        Crud::setEntityNameStrings(
            "Ordem de serviço Nº " . $this->crud->autorizacaodeexecucao->numero,
            "Comunicar Entregas"
        );
        $this->importarScriptJs([
            'assets/js/numberField.js'
        ]);

        $this->importarScriptCss([
            'assets/css/autorizacaoexecucao/progress-bar.css'
        ]);

        $this->crud->setCreateContentClass('col-md-12');

        $this->crud->addField(
            [
                'name' => 'processo_sei',
                'type' => 'label',
                'label' => 'Número do processo SEI',
                'wrapperAttributes' => ['class' => 'col-md-3 pt-3'],
                'value' =>  $this->crud->autorizacaodeexecucao->processo
            ]
        );

        $this->crud->addField(
            [
                'name' => 'tipo_os_fornecimento',
                'type' => 'label',
                'label' => 'Tipo',
                'wrapperAttributes' => ['class' => 'col-md-3 pt-3'],
                'value' => $this->crud->autorizacaodeexecucao->getTipo(),
            ]
        );

        $this->crud->addField(
            [
                'name' => 'num_ano_os_fornecimento',
                'type' => 'label',
                'label' => 'Número/Ano da Ordem de Serviço/Fornecimento',
                'wrapperAttributes' => ['class' => 'col-md-3 pt-3'],
                'value' =>$this->crud->autorizacaodeexecucao->numero,
            ]
        );

        $this->crud->addField(
            [
                'name' => 'num_ano_os_fornecimento',
                'type' => 'label',
                'label' => 'Número/Ano da Ordem de Serviço/Fornecimento',
                'wrapperAttributes' => ['class' => 'col-md-3 pt-3'],
                'value' =>$this->crud->autorizacaodeexecucao->numero,
            ]
        );

        $this->crud->addField(
            [
                'name' => 'data_assinatura_osf',
                'type' => 'label',
                'label' => 'Data de Asinatura  da OS/F',
                'wrapperAttributes' => ['class' => 'col-md-3 pt-3'],
                'value' => Carbon::createFromFormat(
                    'Y-m-d',
                    $this->crud->autorizacaodeexecucao->data_assinatura
                )->format('d/m/Y'),
            ]
        );

        $this->crud->addField(
            [
                'name' => 'data_vigencia_inicio_osf',
                'type' => 'label',
                'label' => 'Vigencia inicio da OS/F',
                'wrapperAttributes' => ['class' => 'col-md-3 pt-3'],
                'value' => Carbon::createFromFormat(
                    'Y-m-d',
                    $this->crud->autorizacaodeexecucao->data_vigencia_inicio
                )->format('d/m/Y'),
            ]
        );

        $this->crud->addField(
            [
                'name' => 'data_vigencia_fim_osf',
                'type' => 'label',
                'label' => 'Vigencia fim  da OS/F',
                'wrapperAttributes' => ['class' => 'col-md-3 pt-3'],
                'value' => Carbon::createFromFormat(
                    'Y-m-d',
                    $this->crud->autorizacaodeexecucao->data_vigencia_fim
                )->format('d/m/Y'),
            ]
        );

        $this->crud->addField(
            [
                'name' => 'numero_sistema_origem',
                'type' => 'label',
                'label' => 'Número da ordem de serviço no sistema de origem',
                'wrapperAttributes' => ['class' => 'col-md-3 pt-3'],
                'value' => $this->crud->autorizacaodeexecucao->numero_sistema_origem,
            ]
        );

        $this->crud->addField(
            [
                'name' => 'itens_osf_entregas',
                'type' => 'table_selecionar_entrega_itens',
                'label' => 'itens',
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'itens' => $this->crud->autorizacaodeexecucao
                    ->autorizacaoexecucaoItens
                    ->load(['itensEntrega' => function ($query) {
                        $query->where('autorizacao_execucao_entrega_id', \request()->id ?? 0);
                    }]),
            ]
        );

        $this->crud->addField([
            'name' => 'informacoes_complementares',
            'type' => 'tinymce',
            'label' => 'Informações Complementares',
            'required' => true,
            'options' => [
                'language' => 'pt_BR',
                'menubar' => false,
                'plugins' => [
                    'print autolink lists link table',
                ],
                'toolbar' => 'print | undo redo | copy cut paste |' .
                    ' bold italic underline strikethrough superscript subscript | 
                    backcolor forecolor link unlink blockquote | alignleft aligncenter alignright alignjustify | 
                    bullist numlist outdent indent | hr table removeformat',
            ],
        ]);

        $countLocaisExecucao = ContratoLocalExecucao::whereHas('autorizacoesExecucao', function ($q) {
            $q->where('autorizacaoexecucao_id', $this->crud->autorizacaodeexecucao->id);
        })->count();

        $this->crud->addField([
            'name' => 'locais_execucao_entrega',
            'label' => 'Locais de Execução para Entrega',
            'type' => 'select2_multiple',
            'entity' => 'locaisExecucao',
            'attribute' => 'descricao',
            'required' => $countLocaisExecucao ? true : false,
            'model' => 'App\Models\ContratoLocalExecucao',
            'pivot' => true,
            'value' => isset(request()->id)
                ? $this->crud->getEntry(request()->id)->locaisExecucao->pluck('id')->toArray()
                : [],
            'options' => (function ($query) {
                return $query->whereHas('autorizacoesExecucao', function ($q) {
                    $q->where('autorizacaoexecucao_id', $this->crud->autorizacaodeexecucao->id);
                })->get();
            }),
            'wrapperAttributes' => ['class' => 'col-md-12 mt-1'],
        ]);

        $this->crud->addField([
            'name' => 'mes_ano_referencia',
            'label' => 'Mês/Ano de Referência',
            'type' => 'date_month',
            'wrapperAttributes' => [
                'class' => 'col-md-12 mt-1'
            ],
            'required' => true,
        ]);

        $autorizacaoExecucaoEntrega = new AutorizacaoExecucaoEntrega();
        $contratoParametroRepository = new ContratoParametroRepository($this->crud->autorizacaodeexecucao->contrato_id);
        $dataPrazoRecebimentoProvisorio = $contratoParametroRepository->getDataPrazoRecebimentoProvisorio();
        $dataPrazoRecebimentoDefinitivo =  $contratoParametroRepository->getDataPrazoRecebimentoDefinitivo();
        if (request()->id) {
            $autorizacaoExecucaoEntrega = $this->crud->getEntry(request()->id);
        }

        $this->crud->addField([
            'name' => 'data_entrega',
            'label' =>  'Data efetiva da entrega',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'col-md-4 mt-1'
            ],
            'required' => true,
            'value' => $autorizacaoExecucaoEntrega->data_entrega ?? date('Y-m-d')
        ]);

        $this->crud->addField([
            'name' => 'data_prazo_trp',
            'label' =>  'Data prevista para o recebimento provisório',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'col-md-4 mt-1'
            ],
            'required' => true,
            'value' => $autorizacaoExecucaoEntrega->data_prazo_trp ?? $dataPrazoRecebimentoProvisorio
        ]);

        $this->crud->addField([
            'name' => 'data_prazo_trd',
            'label' =>  'Data prevista para recebimento definitivo',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'col-md-4 mt-1'
            ],
            'required' => true,
            'value' => $autorizacaoExecucaoEntrega->data_prazo_trd ?? $dataPrazoRecebimentoDefinitivo
        ]);

        $codigoItem = CodigoItem::where('descres', 'ae_entrega_arquivo')->first();

        $autorizacaoExecucaoEntrega =
            request()->id ? AutorizacaoExecucaoEntrega::find(request()->id) : new AutorizacaoExecucaoEntrega();

        $anexos = $autorizacaoExecucaoEntrega->anexos()->where('tipo_id', $codigoItem->id)->get();

        $this->crud->addField([
            'name' => 'files',
            'label' => 'Arquivos',
            'type' => 'upload_multiple_custom_osf_entrega', // Nome do campo personalizado
            'upload' => true,
            'disk' => 'public', // O disco onde os arquivos serão armazenados
            'anexos' => $anexos->toArray()
        ]);

        $this->crud->button_custom = [
            ['button_text' => 'Salvar Rascunho', 'button_id' => 'rascunho',
                'button_name_action' => 'rascunho', 'button_value_action' => '1',
                'button_icon' => 'fas fa-edit', 'button_tipo' => 'secondary'],
            ['button_text' => 'Comunicar', 'button_id' => 'informar',
                'button_name_action' => 'rascunho', 'button_value_action' => '0',
                'button_icon' => 'fas fa-save', 'button_tipo' => 'primary']
        ];

        $textoUrlFinal = str_ends_with(url()->current(), 'create') ? 'Criar Entrega' : 'Editar Entrega';
        $this->bredcrumbs(
            $this->administradorFornec,
            $textoUrlFinal,
            'Entregas OSF ' . $this->crud->autorizacaodeexecucao->numero,
            url("fornecedor/autorizacaoexecucao/{$this->autorizacaoexecucao_id}/entrega"),
            true
        );
    }

    protected function setupUpdateOperation()
    {
        $this->crud->setEditContentClass('col-md-12');
        $this->setupCreateOperation();
    }

    public function setupShowOperation()
    {
        $this->crud->addColumn([
            'name' => 'situacao',
            'label' => 'Situação',
            'type' => 'model_function',
            'function_name' => 'retornaTextoSituacao',
            'limit' => 9999,
        ]);

        $this->crud->addColumn([
            'name' => 'sequencial',
            'label' => 'Número Entrega',
            'type' => 'model_function',
            'function_name' => 'getFormattedSequencial'
        ]);

        $this->crud->addColumn([
            'name' => 'valor_entrega',
            'label' => 'Valor Total Entrega',
            'type' => 'model_function',
            'function_name' => 'retornaValorEntregaFormatado'
        ]);

        $this->crud->addColumn([
            'name' => 'data_entrega',
            'label' => 'Data Entrega',
            'type' => 'date'
        ]);

        $this->addColumnText(
            false,
            true,
            true,
            true,
            'informacoes_complementares',
            'Informações Complementares'
        );

        $this->crud->addColumn([
            'name' => 'mes_ano_referencia',
            'label' => 'Mês/Ano de Referência',
            'type' => 'model_function',
            'function_name' => 'getMesAnoReferencia'
        ]);

        $this->addColumnTablePlain(
            'entregaItens',
            'Itens entregues',
            [
                'tipo_material_os_entrega' => 'Tipo',
                'tipo_item' => 'Item',
                'quantidade_informada_formatado' => 'Quantidade',
                'valor_glosa_formatado' => 'Glosa',
                'valor_unitario_item' => 'Valor Unitário',
                'valor_total_item' => 'Valor Total'
            ]
        );

        $this->crud->addColumn([
            'name' => 'anexos',
            'type' => 'model_function_raw',
            'label' => 'Anexos',
            'function_name' => 'getAnexosViewLink',
        ]);

        $this->addColumnText(
            false,
            true,
            true,
            true,
            'justificativa_motivo_analise',
            'Justificativa/Motivo da Análise'
        );

        $this->bredcrumbs(
            $this->administradorFornec,
            'Vizualizar Entregas',
            'Entregas OSF ' . $this->crud->autorizacaodeexecucao->numero,
            url("fornecedor/autorizacaoexecucao/{$this->autorizacaoexecucao_id}/entrega"),
            true
        );
    }

    public function anexo(Request $request)
    {
        $request->validate(
            [
                'upload_arquivo_anexo' => 'required|file|mimes:pdf|max:30720',
            ],
            [
                'upload_arquivo_anexo.required' => 'Selecione um arquivo de anexo',
                'upload_arquivo_anexo.file' => 'Selecione um arquivo de anexo',
                'upload_arquivo_anexo.mimes' => 'O arquivo de anexo deve ser do tipo PDF',
                'upload_arquivo_anexo.max' => 'O arquivo de anexo deve ter no maximo 30MB',
            ]
        );
        $file = $request->file('upload_arquivo_anexo');
        return response()->json([
            'nome_arquivo_anexo' => $file->getClientOriginalName(),
            'url_arquivo_anexo' => $file->store('temporario', ['disk' => 'public']),
        ]);
    }

    public function store(UsuarioFornecedorAutorizacaoExecucaoEntregaRequest $request)
    {
        $dadosValidados = $request->validated();

        try {
            DB::beginTransaction();
            $this->usuarioFornecedorAutorizacaoExecucaoEntregaService->salvarEntrega(
                $dadosValidados,
                $this->crud->autorizacaodeexecucao->contrato_id,
                $this->crud->autorizacaodeexecucao->id,
            );
            DB::commit();
            \Alert::add('success', 'Entrega comunicada com sucesso')->flash();
        } catch (Exception $e) {
            \Log::error($e);
            DB::rollBack();
            \Alert::add('error', 'Erro ao criar a entrega!')->flash();
            return redirect()->back()->withInput();
        }

        return redirect('fornecedor/autorizacaoexecucao/' . $this->crud->autorizacaodeexecucao->id . '/entrega');
    }

    public function update(UsuarioFornecedorAutorizacaoExecucaoEntregaRequest $request)
    {
        $dadosValidados = $request->validated();

        try {
            DB::beginTransaction();
            $this->usuarioFornecedorAutorizacaoExecucaoEntregaService->alterarEntrega(
                $dadosValidados,
                $request->get('id')
            );
            DB::commit();
            \Alert::add('success', 'Entrega comunicada com sucesso')->flash();
        } catch (Exception $e) {
            \Log::error($e);
            DB::rollBack();
            \Alert::add('error', 'Erro ao alterar a entrega!')->flash();
            return redirect()->back()->withInput();
        }

        return redirect('fornecedor/autorizacaoexecucao/' . $this->crud->autorizacaodeexecucao->id . '/entrega');
    }

    public function duplicar(Request $request)
    {
        try {
            DB::beginTransaction();

            $this->usuarioFornecedorAutorizacaoExecucaoEntregaService->duplicarEntrega($request->entrega_id);

            DB::commit();

            \Alert::add('success', 'Duplicação realizada com sucesso!')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao duplicar a ordem de serviço / fornecimento!')->flash();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
        }

        return redirect()->back();
    }

    public function cancelarEntrega(FornecedorEntregaCancelamentoRequest $request)
    {
        try {
            DB::beginTransaction();

            $this->usuarioFornecedorAutorizacaoExecucaoEntregaService->salvarAnaliseEntregaFornecedor(
                $request->validated()
            );

            DB::commit();

            return $this->montarRespostaAlertJs(
                200,
                'Entrega cancelada com sucesso',
                "success"
            );
        } catch (Exception $e) {
            DB::rollBack();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            return $this->montarRespostaAlertJs(
                500,
                'Erro ao realizar o cancelamento',
                "error"
            );
        }
    }
}
