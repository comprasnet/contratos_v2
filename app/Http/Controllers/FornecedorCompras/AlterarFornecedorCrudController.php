<?php

namespace App\Http\Controllers\FornecedorCompras;

use App\Http\Requests\AlterarFornecedorRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Models\CodigoItem;
use App\Models\Compras;
use App\Models\Fornecedor;
use App\Models\Unidade;
use App\Services\UsuarioFornecedor\UsuarioFornecedorService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use function config;

/**
 * Class ComprasCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AlterarFornecedorCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;

    protected $usuarioFornecedorService;
    public function __construct(UsuarioFornecedorService $usuarioFornecedorService)
    {
        parent::__construct();
        $this->usuarioFornecedorService = $usuarioFornecedorService;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        //if (config('app.app_amb') == 'Ambiente Produção') {
        //    abort('403', config('app.erro_permissao'));
        //}
        $dadosUsuarioRedis = $this->recuperarDadosUsuarioRedis(auth()->id());
        $uasg_session = session(['user_ug_id']);
 
        $this->crud->text_button_redirect_create = 'Compras';
        CRUD::setModel(Fornecedor::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . 'fornecedor/alterar-fornecedor');

        $this->exibirTituloPaginaMenu('Escolher Fornecedor');

        $this->crud->denyAccess('list');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AlterarFornecedorRequest::class);

        $this->crud->addField(
            [   // radio
                'name'        => 'cpf_cnpj_idgener', // the name of the db column
                'label'       => 'Identificador do Fornecedor',
                'type'        => 'text',
                'required' => true
            ],
        );

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Acessar',
        ]);
    }

    public function store(AlterarFornecedorRequest $request)
    {
        $fornecedor  = $this->usuarioFornecedorService->verificarFornecedor($request->cpf_cnpj_idgener);
        if ($fornecedor) {
            session(['fornecedor_cpf_cnpj_idgener' => $fornecedor->cpf_cnpj_idgener]);
            session(['fornecedor_id' => $fornecedor->id]);
            $tipoAcesso = 'Administrador';
            session(['tipo_acesso' => $tipoAcesso]);
            session(['nomeRazaoSocialFornecedor' => $fornecedor->nome]);
            session(['acesso_pelo_compras' => false]);
            if (in_array('Administrador', backpack_user()->getRoleNames()->toArray())) {
                return redirect()->route('fornecedor.administrador.inicio', '');
            }
        }

        return  back()->withErrors(['error' => "Fornecedor não encontrado."]);
    }
}
