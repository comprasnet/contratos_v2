<?php

namespace App\Http\Controllers\FornecedorCompras;

use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Models\Fornecedor;
use App\Services\UsuarioFornecedor\UsuarioFornecedorService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Models\Contrato;
use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Library\Widget;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class UsuarioFornecedorContratosCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use UsuarioFornecedorTrait;
    use Formatador;
    use CommonColumns;
    use ImportContent;

    protected $usuarioFornecedorService;

    private $administradorFornec;

    public function __construct(UsuarioFornecedorService $usuarioFornecedorService)
    {
        parent::__construct();
        $this->usuarioFornecedorService = $usuarioFornecedorService;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->permissionRule([
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);

        CRUD::setModel(Contrato::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . 'fornecedor/contrato');

        $this->administradorFornec = $this->usuarioFornecedorService->verificaAdministradorOuPrespostoFornecedorLogado(
            backpack_user(),
            session('fornecedor_id')
        );
        CRUD::addClause('join', 'unidades', 'unidades.id', '=', 'contratos.unidade_id');
        CRUD::addClause('where', 'contratos.fornecedor_id', '=', session('fornecedor_id'));
        CRUD::addClause('where', 'contratos.situacao', '=', true);
        CRUD::addClause('where', 'contratos.elaboracao', '=', false);
        CRUD::addClause('where', 'unidades.sigilo', '=', false);

        $this->adicionarQueryFiltroPreposto($this->administradorFornec);

        $redirectVencimento = $this->crud->getRequest()->query()['redirect_vencimento'] ?? 0 ;
        if ($redirectVencimento) {
            $hoje = Carbon::now()->toDateString();
            CRUD::addClause(
                'whereRaw',
                "('{$hoje}' between vigencia_inicio  and  vigencia_fim or vigencia_fim is null)"
            );
        }

        CRUD::addClause('select', 'contratos.*');

        $this->exibirTituloPaginaMenu('Contratos');

        $this->crud->addButtonFromView(
            'line',
            'button_autorizacaoexecucao',
            'button_autorizacaoexecucao',
            'beginning'
        );

        $this->crud->addButtonFromView(
            'line',
            'button_contratoinstrumentodecobranca',
            'button_contratoinstrumentodecobranca',
            'end'
        );
        $this->crud->addButtonFromView(
            'line',
            'linkpncp',
            'linkpncp',
            'end'
        );

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->setListView('vendor.backpack.crud.usuario-fornecedor.list');
        $this->crud->setShowView('vendor.backpack.crud.usuario-fornecedor.show');
    }

    public function setupShowOperation()
    {
        $this->permissionRule([
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);

        $this->verificarPermissaoShowByContrato(Route::current()->parameter('id'));

        $this->addColumnModelFunction('orgao', 'Órgão', 'getOrgao');
        $this->addColumnModelFunction('unidade_gestora', 'Unidade Gestora', 'getOrgao');
        $this->addColumnModelFunction(
            'unidade_gestora_origem',
            'Unidade Gestora Origem do Contrato',
            'getUnidadeOrigem'
        );
        $this->addColumnModelFunction(
            'receita_despesa',
            'Receita / Despesa',
            'getReceitaDespesa'
        );
        $this->addColumnModelFunction('numero', 'Número Contrato', 'getNumeroContrato');
        $this->addColumnModelFunction(
            'unidade_compra',
            'Unidade Realizadora da Compra',
            'getUnidadeCompra'
        );
        $this->addColumnModelFunction('numero_compra', 'Número da Compra', 'getNumeroCompra');
        $this->addColumnModelFunction('modalidade', 'Modalidade da Compra', 'getModalidade');
        $this->addColumnModelFunction('amparo_legal', 'Amparo Legal', 'retornaAmparo');
        $this->addColumnText(
            false,
            true,
            true,
            true,
            'unidades_requisitantes',
            'Unidades Requisitantes'
        );
        $this->addColumnModelFunction('tipo_contrato', 'Tipo', 'getTipo');
        $this->addColumnModelFunction('categoria', 'Categoria', 'getCategoria');
        $this->addColumnModelFunction('fornecedor', 'Fornecedor', 'getFornecedor');
        $this->addColumnModelFunction('subcategoria', 'Subcategoria', 'getSubCategoria');
        $this->addColumnModelFunction('processo', 'Processo', 'getProcessoContrato');
        $this->addColumnModelFunction('objeto', 'Objeto', 'getObjetoContrato');
        $this->addColumnText(
            false,
            true,
            true,
            true,
            'informacoes_complementares',
            'Informações Complementares'
        );
        $this->addColumnModelFunction('vigencia_inicio', 'Vig. Início', 'getVigenciainicio');
        $this->addColumnModelFunction('vigencia_fim', 'Vig. Fim', 'getVigenciaFimPrazoIndeterminado');
        $this->addColumnModelFunction('valor_global', 'Valor Global', 'formatVlrGlobal');
        $this->addColumnText(false, true, true, true, 'num_parcelas', 'Núm. Parcelas');
        $this->addColumnModelFunction('valor_global', 'Valor da Parcela', 'formatVlrParcela');
        $this->addColumnText(false, true, true, true, 'unidades_requisitantes', 'Unidades Requisitantes');
        $this->addColumnTableContratoHistorico('historico', 'Histórico');
        $this->addColumnTablePlain(
            'despesasacessorias',
            'Despesas Acessórias',
            [
                'descricao_complementar' => 'Descrição',
                'vencimento' => 'Vencimento',
                'valor' => 'Valor'
            ]
        );
        $this->addColumnTableEmpenhos('empenho', 'Empenhos');
        $this->addColumnTablePlain(
            'faturas',
            'Faturas',
            [
                'numero' => 'Número',
                'emissao' => 'Data Emissão',
                'processo' => 'Processo',
                'ateste' => 'Data Ateste',
                'valor' => 'Valor'
            ],
        );
        $this->addColumnTablePlain(
            'garantias',
            'Garantias',
            [
                'descricao_tipo' => 'Tipo',
                'vencimento' => 'Vencimento',
                'valor' => 'Valor'
            ]
        );
        $this->addColumnTablePlain(
            'itens',
            'Itens',
            [
                'descricao_tipo' => 'Tipo',
                'descricao_item' => 'Item',
                'quantidade' => 'Quantidade',
                'valorunitario' => 'Valor Unitário',
                'valortotal' => 'Valor Total'
            ]
        );
        $this->addColumnTablePlain(
            'prepostos',
            'Prepostos',
            [
                'masked_cpf' => 'CPF',
                'nome' => 'Nome'
            ]
        );
        $this->addColumnTablePlain(
            'responsaveis',
            'Responsáveis',
            [
                'masked_cpf' => 'CPF',
                'usuario_nome' => 'Nome',
                'descricao_tipo' => 'Tipo'
            ]
        );
        $this->addColumnModelFunctionV1(
            'instrumentocobranca',
            'Instrumentos de Cobrança',
            'getLinkTransparenciaV1',
            ['faturas']
        );
        $this->addColumnModelFunctionV1(
            'terceirizados',
            'Terceirizados',
            'getLinkTransparenciaV1',
            ['terceirizados']
        );
        $this->addColumnArquivos('arquivos', 'Arquivos', 'local');

        $this->bredcrumbs(
            $this->administradorFornec,
            'Visualizar',
            'Contratos',
            url('fornecedor/contrato'),
            true
        );
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->permissionRule([
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);

        $this->adicionaFiltros();

        $this->crud->setOperationSetting('persistentTable', false);

        $this->crud->addClause('join', 'fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id');
        $this->crud->addClause('leftjoin', 'codigoitens', 'codigoitens.id', '=', 'contratos.modalidade_id');
        $this->crud->addClause('leftjoin', 'unidades as uc', 'uc.id', '=', 'contratos.unidadecompra_id');
       // CRUD::enableDetailsRow();

        $this->crud->addColumn([
            'name' => 'numero',
            'label' => 'Número do instrumento',
            'type' => 'text',
            'priority' => 1
        ]);

        $this->crud->addColumn([
            'name' => 'modalidade_compra',
            'label' => 'Modalidade da Compra',
            'type' => 'model_function',
            'function_name' => 'getModalidade',
            'visibleInTable' => false,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('codigoitens.descricao', 'ilike', "%$searchTerm%");
            },
        ]);

        $this->crud->addColumn([
            'name' => 'aparo_legal',
            'label' => 'Amparo Legal',
            'type' => 'model_function',
            'function_name' => 'retornaAmparo',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'numero_da_compra',
            'label' => 'Número Compra',
            'type' => 'model_function',
            'function_name' => 'getNumeroCompra',
            'visibleInTable' => false,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('contratos.licitacao_numero', 'ilike', "%$searchTerm%");
            },
        ]);

        $this->crud->addColumn([
            'name' => 'receita_despesa',
            'label' => 'Receita / Despesa',
            'type' => 'model_function',
            'function_name' => 'getReceitaDespesa',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'unidade_de_origem',
            'label' => 'Unidade Gestora Origem do Contrato',
            'type' => 'model_function',
            'function_name' => 'getUnidadeOrigem',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'unidade_da_compra',
            'label' => 'Unidade da Compra',
            'type' => 'model_function',
            'function_name' => 'getUnidadeCompra',
            'visibleInTable' => false,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('uc.codigo', '=', $searchTerm);
            },
        ]);

        $this->crud->addColumn([
            'name' => 'unidade',
            'label' => 'Unidade Gestora Atual',
            'type' => 'model_function',
            'function_name' => 'getUnidade'
        ]);

        $this->crud->addColumn([
            'name' => 'unidades_requisitantes',
            'label' => 'Unidades Requisitantes',
            'type' => 'text',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'tipo',
            'label' => 'Tipo',
            'type' => 'model_function',
            'function_name' => 'getTipo'
        ]);

        $this->crud->addColumn([
            'name' => 'subtipo',
            'label' => 'Subtipo',
            'type' => 'text',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'pega_categoria',
            'label' => 'Categoria',
            'type' => 'model_function',
            'function_name' => 'getCategoria',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'pega_subcategoria',
            'label' => 'Subcategoria',
            'type' => 'model_function',
            'function_name' => 'getSubCategoria',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'pega_fornecedor',
            'label' => 'Fornecedor',
            'type' => 'model_function',
            'function_name' => 'getFornecedor',
            'limit' => 50,
            'visibleInTable' => false,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->orWhere('fornecedores.cpf_cnpj_idgener', 'like', "%" . strtoupper($searchTerm) . "%");
                $query->orWhere('fornecedores.nome', 'like', "%" . strtoupper($searchTerm) . "%");
            },
        ]);

        $this->crud->addColumn([
            'name' => 'processo',
            'label' => 'Processo',
            'type' => 'text',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'objeto',
            'label' => 'Objeto',
            'type' => 'text',
            'limit' => 1000,
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'info_complementar',
            'label' => 'Informações Complementares',
            'type' => 'text',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'vigencia_inicio',
            'label' => 'Vig. Início',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'vigencia_fim',
            'label' => 'Vig. Fim',
            'type' => 'model_function',
            'function_name' => 'getVigenciaFimPrazoIndeterminado'
        ]);

        $this->crud->addColumn([
            'name' => 'formata_valor_global',
            'label' => 'Valor Global',
            'type' => 'model_function',
            'function_name' => 'formatVlrGlobal'
        ]);

        $this->crud->addColumn([
            'name' => 'num_parcelas',
            'label' => 'Núm. Parcelas',
            'type' => 'model_function',
            'function_name' => 'getNumeroParcelasPrazoIndeterminado',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'forma_valor_parcela_parcela',
            'label' => 'Valor Parcela',
            'type' => 'model_function',
            'function_name' => 'formatVlrParcela',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'format_vlr_acumulado',
            'label' => 'Valor Acumulado',
            'type' => 'model_function',
            'function_name' => 'formatVlrAcumulado',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'formata_total_despesas_acessorias',
            'label' => 'Total Despesas Acessórias',
            'type' => 'model_function',
            'function_name' => 'formatTotalDespesasAcessorias',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'pega_situacao',
            'label' => 'Situação',
            'type' => 'model_function',
            'function_name' => 'getSituacao',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'prorrogavel_coluna',
            'label' => 'Prorrogável',
            'type' => 'text',
            'visibleInTable' => false,
            'options' => [null => '', 0 => 'Não', 1 => 'Sim']
        ]);

        $this->crud->addColumn([
            'name' => 'created_at',
            'label' => 'Criado em',
            'type' => 'datetime',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'data_proposta_comercial',
            'label' => 'Data Proposta Comercial',
            'type' => 'datetime',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'codigo_sistema_externo',
            'label' => 'Código Sistema Externo',
            'type' => 'text',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'updated_at',
            'label' => 'Atualizado em',
            'type' => 'datetime',
            'visibleInTable' => false
        ]);
        $this->crud->enableExportButtons();

        $this->bredcrumbs($this->administradorFornec, 'Contratos');

        // habilitar apenas para testes
        if (config('app.app_amb') != 'Ambiente Produção') {
            $this->crud->addButtonFromView(
                'line',
                'button_prepostos_contrato_fornecedor',
                'button_prepostos_contrato_fornecedor',
                'end'
            );

            $this->importarScriptJs([
            'assets/js/usuario_fornecedor/prepostos_contrato.js'
            ]);

            Widget::add([
            'type'     => 'view',
            'view'     => 'vendor.backpack.base.widgets.modal-prepostos',
            'id' => 'modalprepostos',
            ]);
        }
    }

    public function adicionaFiltros()
    {
        $this->crud->addFilter([
            'name'  => 'receita_despesa',
            'type'  => 'select2_multiple',
            'label' => 'Receita / Despesa'
        ], function () {
            return [
                'R' => 'Receita',
                'D' => 'Despesa',
            ];
        }, function ($value) {
             $this->crud->addClause('whereIn', 'contratos.receita_despesa', json_decode($value));
        });

        $this->crud->addFilter(
            [
            'name'  => 'tipo',
            'type'  => 'select2_multiple',
            'label' => 'Tipo'
            ],
            $this->usuarioFornecedorService->retornaTiposService(),
            function ($value) {
                 $this->crud->addClause('whereIn', 'contratos.tipo_id', json_decode($value));
            }
        );

        $this->crud->addFilter(
            [
            'name'  => 'categorias',
            'type'  => 'select2_multiple',
            'label' => 'Categorias'
            ],
            $this->usuarioFornecedorService->retornaCategoriasService(),
            function ($value) {
                $this->crud->addClause('whereIn', 'contratos.categoria_id', json_decode($value));
            }
        );

        $this->crud->addFilter(
            [
            'name'  => 'vigencia_inicio',
            'type'  => 'date_range',
            'label' => 'Vigência Inicio'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $fromDate = Carbon::parse($dates->from);
                $toDate = Carbon::parse($dates->to)->endOfDay();

                $this->crud->addClause('where', 'contratos.vigencia_inicio', '>=', $fromDate);
                $this->crud->addClause('where', 'contratos.vigencia_inicio', '<=', $toDate);
            }
        );

        $this->crud->addFilter(
            [
            'name'  => 'vigencia_fim',
            'type'  => 'date_range',
            'label' => 'Vigência Fim'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $fromDate = Carbon::parse($dates->from);
                $toDate = Carbon::parse($dates->to)->endOfDay();

                $this->crud->addClause('where', 'contratos.vigencia_fim', '>=', $fromDate);
                $this->crud->addClause('where', 'contratos.vigencia_fim', '<=', $toDate);
            }
        );

        $this->crud->addFilter(
            [
            'name'  => 'valor_global',
            'type'  => 'range',
            'label' => 'Valor Global',
            'label_from' => 'Vlr Mínimo',
            'label_to' => 'Vlr Máximo'
            ],
            false,
            function ($value) {
                $range = json_decode($value);

                if ($range->from) {
                    $this->crud->addClause('where', 'contratos.valor_global', '>=', (float)$range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'contratos.valor_global', '<=', (float)$range->to);
                }
            }
        );

        $this->crud->addFilter(
            [
            'name'  => 'valor_parcela',
            'type'  => 'range',
            'label' => 'Valor Parcela',
            'label_from' => 'Vlr Mínimo',
            'label_to' => 'Vlr Máximo'
            ],
            false,
            function ($value) {
                $range = json_decode($value);

                if ($range->from) {
                    $this->crud->addClause('where', 'contratos.valor_parcela', '>=', (float)$range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'contratos.valor_parcela', '<=', (float)$range->to);
                }
            }
        );

        //situação
        $this->crud->addFilter(
            [
                'name' => 'situacao',
                'type' => 'select2_multiple',
                'label' => 'Situação'
            ],
            [
                'Ativo' => 'Ativo',
                'Inativo' => 'Inativo',
                'Rescindido' => 'Rescindido',
                'Encerrado' => 'Encerrado'
            ],
            function ($value) {
                $statuses = array_map('trim', explode(',', str_replace(['[', '"', ']'], '', $value)));

                $this->crud->addClause(
                    'leftjoin',
                    'codigoitens as justificativa',
                    'justificativa.id',
                    '=',
                    'contratos.justificativa_contrato_inativo_id'
                );

                $this->crud->addClause('where', function ($q) use ($statuses) {
                    $q->orWhereIn('justificativa.descricao', $statuses);

                    if (in_array('Inativo', $statuses)) {
                        $q->orWhere('contratos.situacao', 0);
                    } elseif (in_array('Ativo', $statuses)) {
                        $q->orWhere('contratos.situacao', 1);
                    }
                });
            }
        );

        $this->crud->addFilter(
            [
            'name'  => 'modalidade_compra',
            'type'  => 'select2_multiple',
            'label' => 'Modalidade Compra'
            ],
            $this->usuarioFornecedorService->retornaModalidadeCompraService(),
            function ($value) {
                $this->crud->addClause('whereIn', 'contratos.modalidade_id', json_decode($value));
            }
        );

        //amparo_legal
        $this->crud->addFilter(
            [
                'name' => 'amparo_legal',
                'type' => 'select2_ajax',
                'label' => 'Amparo Legal',
                'placeholder' => 'Selecione o Amparo Legal',
                'select_attribute' => 'campo_api_amparo',
                'select_key' => 'campo_api_amparo',
            ],
            url('api/amparolegalfiltercontratos'),
            function ($values) {
                $options = $this->usuarioFornecedorService->retornaOpcoesFiltroAmaparoLegalContratoService($values);
                $this->crud->addClause('whereIn', 'contratos.id', $options);
            }
        );

        $this->crud->addFilter(
            [
            'name'  => 'numero_compra',
            'type'  => 'text',
            'label' => 'Número Compra'
            ],
            $this->usuarioFornecedorService->retornaModalidadeCompraService(),
            function ($value) {
                $valor  = json_decode($value);
                $this->crud->addClause('where', 'contratos.licitacao_numero', 'ilike', "%$valor%");
            }
        );

        // unidade_compra
        $this->crud->addFilter(
            [
                'name' => 'unidade_compra',
                'type' => 'select2_ajax',
                'label' => 'Unidade Compra',
                'placeholder' => 'Selecione a Unidade da Compra',
                'select_attribute' => 'descricao',
                'select_key' => 'id'
            ],
            url('api/unidadefiltercontratos'),
            function ($values) {
                $this->crud->addClause('where', 'contratos.unidade_id', $values);
            }
        );
    }

    public function informacoesPrepostos(Request $request)
    {
        return $this->usuarioFornecedorService->retornaPrepostosContratoPorId($request->contrato_id);
    }
}
