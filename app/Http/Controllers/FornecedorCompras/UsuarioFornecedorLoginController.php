<?php

namespace App\Http\Controllers\FornecedorCompras;

use App\Http\Controllers\Controller;
use App\Http\Requests\UsuarioFornecedorRequest;
use App\Models\BackpackUser;
use App\Services\UsuarioFornecedor\UsuarioFornecedorService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Route;
use App\Http\Traits\TracksUserLogin;

/**
 * Class UsuarioFornecedorCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UsuarioFornecedorLoginController extends Controller
{
    use TracksUserLogin;
    public $data = []; // the information we send to the view
    protected $usuarioFornecedorService;

    public function __construct(UsuarioFornecedorService $usuarioFornecedorService)
    {
        $this->usuarioFornecedorService = $usuarioFornecedorService;
    }

    public function indexPreposto(Request $request)
    {
        if ($this->usuarioFornecedorService->verificaSeTemLoginRealizado()) {
            $this->usuarioFornecedorService->destruirLoginPrevio();
        }

        $compraId = request()->query('compras-id');

        if (!$compraId) {
            return response()->view('errors.403_custom_loginfornec', [], 403);
            //abort('403', config('app.erro_permissao'));
        }

        $retorno = $this->usuarioFornecedorService->sessaoUsuarioFornecedor($compraId);

        if (empty($retorno)) {
            return redirect('/login')->withErrors(['erro_usuario_compras' => 'Sessão do Compras.gov.br expirada.
             Favor logar novamente']);
        }

        $retorno['tipoUsuario'] = 'Preposto';
        $retorno['administrador'] = false;

        return $this->usuarioFornecedorService->tratarRedirecionarRetorno($retorno);
    }

    public function indexAdministrador(Request $request)
    {
        if ($this->usuarioFornecedorService->verificaSeTemLoginRealizado()) {
            $this->usuarioFornecedorService->destruirLoginPrevio();
        }

        $compraId = request()->query('compras-id');

        if (!$compraId) {
            return response()->view('errors.403_custom_loginfornec', [], 403);
            //abort('403', config('app.erro_permissao'));
        }

        $retorno = $this->usuarioFornecedorService->sessaoUsuarioFornecedor($compraId);

        if (empty($retorno)) {
            return redirect('/login')->withErrors(['erro_usuario_compras' => 'Sessão do Compras.gov.br expirada.
             Favor logar novamente']);
        }

        $retorno['tipoUsuario'] = 'Administrador';
        $retorno['administrador'] = true;
        return $this->usuarioFornecedorService->tratarRedirecionarRetorno($retorno);
    }

    public function salvarUsuarioFornecedor(UsuarioFornecedorRequest $request): RedirectResponse
    {
        try {
            $usuarioFornecedor = $this->usuarioFornecedorService->cadastraUsuarioFornecedor($request->all());

            if ($usuarioFornecedor) {
                //guarda dados da sessão do fornecedor logado
                backpack_auth()->login($usuarioFornecedor);

                $this->usuarioFornecedorService->salvarFornecedorNaSessao($request->all());

                $this->trackLogin($usuarioFornecedor);
            }

            $redireciona = $request->administrador == '1';

            if (!$redireciona) {
                return redirect()->route('fornecedor.preposto.inicio', '');
            }

            return redirect()->route('fornecedor.administrador.inicio', '');
        } catch (Exception $e) {
            return  back()->withErrors(['error' => "Erro ao cadastrar usuário: " . $e->getMessage()]);
        }
    }
}
