<?php

namespace App\Http\Controllers\FornecedorCompras;

use App\Services\UsuarioFornecedor\UsuarioFornecedorService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Traits\ImportContent;

class UsuarioFornecedorCrudController extends CrudController
{
    use ImportContent;
    protected $usuarioFornecedorService;

    public function __construct(UsuarioFornecedorService $usuarioFornecedorService)
    {
        $this->usuarioFornecedorService = $usuarioFornecedorService;
    }

    public function adminInicio()
    {
        if ($this->usuarioFornecedorService->verificarAcessoEFornecedor()) {
            return redirect('fornecedor/alterar-fornecedor/create');
        };

        $this->importarDashboardWidget('assets/js/usuario_fornecedor/dashboard.js');
        $this->importarScriptJs(['assets/js/usuario_fornecedor/dashboard_ata.js']);

        return view(backpack_view('usuario-fornecedor.preposto.inicio'));
    }

    public function prepostoInicio()
    {
        if ($this->usuarioFornecedorService->verificarAcessoEFornecedor()) {
            return redirect('fornecedor/alterar-fornecedor/create');
        };

        $this->importarDashboardWidget('assets/js/usuario_fornecedor/dashboard.js');
        $this->importarScriptJs(['assets/js/usuario_fornecedor/dashboard_ata.js']);

        return view(backpack_view('usuario-fornecedor.preposto.inicio'));
    }

    public function buscarContratosFornecedor()
    {
        $filterContratoSearch = request()->all()['filter-dashboard'] ?? '';
        $dados = $this->usuarioFornecedorService->buscarListasContratosFornecedorLogado(
            backpack_user()->toArray(),
            $filterContratoSearch
        );
        return response()->json($dados);
    }

    public function buscarAtasFornecedor()
    {
        $filterContratoSearch = request()->all()['filter-dashboard'] ?? '';
        $dados = $this->usuarioFornecedorService->buscarListasAtasFornecedorLogado(
            backpack_user()->toArray(),
            $filterContratoSearch
        );
        return response()->json($dados);
    }
}
