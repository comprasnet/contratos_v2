<?php

namespace App\Http\Controllers\AutorizacaoExecucao;

use App\Http\Controllers\Exception;
use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoCrudTrait;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Models\AutorizacaoExecucao;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoSignatarioService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class AutorizacaoExecucaoAssinaturaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AutorizacaoExecucaoAssinaturaCrudController extends CrudController
{
    use AutorizacaoexecucaoCrudTrait;
    use CreateOperation;
    use CommonColumns;
    use Formatador;

    private $aeSignatarioService;
    private $aeSignatario;
    private $arquivoGenerico;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->autorizacaoexecucao = AutorizacaoExecucao::findOrFail(request()->autorizacaoexecucao_id);

        $this->aeSignatarioService = new AutorizacaoExecucaoSignatarioService($this->autorizacaoexecucao);

        $this->aeSignatario = $this->aeSignatarioService->getSignatarioByUsuario();

        $this->contrato = $this->getContrato();

        $this->arquivoGenerico = $this->autorizacaoexecucao->getArquivo();

        CRUD::setModel(AutorizacaoExecucao::class);
        CRUD::addClause('where', 'id', $this->autorizacaoexecucao->id);

        CRUD::setRoute(config('backpack.base.route_prefix') . "/autorizacaoexecucao/" . request()->contrato_id);

        CRUD::setEntityNameStrings(
            "Ordem de Serviço / Fornecimento",
            "Ordens de Serviço / Fornecimento do Contrato {$this->contrato->numero} - 
                {$this->contrato->unidade->codigo}",
        );

        CRUD::setSubheading('Assinar', 'create');
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');

        $this->crud->addField([
            'name' => 'pdf_preview',
            'type' => 'custom_html',
            'value' => '
                <iframe class="pdf" style="width: 100%; aspect-ratio: 4 / 3; height: 100%" 
                    src="' . Storage::url($this->arquivoGenerico->url) . '?temp=' . time() . '"
                    width="800" height="500">
                </iframe>',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $autorizacaoExecucaoService = new AutorizacaoExecucaoService();
        $autorizacaoExecucaoService->setContrato($this->contrato);
        $autorizacaoExecucaoService->setAutorizacaoExecucao($this->autorizacaoexecucao);
        $fields = $autorizacaoExecucaoService->getFieldLabels();

        $this->camposAutorizacaoExecucaoItens($fields['autorizacaoexecucaoItens']['value'], true);

        if ($this->aeSignatario) {
            $this->crud->addField([
                'name' => 'pagina_assinatura',
                'label' => 'Página da assinatura',
                'type' => 'label',
                'wrapper' => [
                    'class' => 'pagina_assinatura d-none',
                ],
                'value' => $this->aeSignatario->pivot->pagina_assinatura
            ]);

            $this->crud->addField([
                'name' => 'posicao_x_assinatura',
                'label' => 'Posição X da assinatura',
                'type' => 'label',
                'wrapper' => [
                    'class' => 'posicao_x_assinatura d-none',
                ],
                'value' => $this->aeSignatario->pivot->posicao_x_assinatura
            ]);

            $this->crud->addField([
                'name' => 'posicao_y_assinatura',
                'label' => 'Posição Y da assinatura',
                'type' => 'label',
                'wrapper' => [
                    'class' => 'posicao_y_assinatura d-none',
                ],
                'value' => $this->aeSignatario->pivot->posicao_y_assinatura
            ]);
        }

        $this->crud->button_custom = [];
        if ($this->aeSignatarioService->verifyUsuarioResponsavelCanRecusar()) {
            $this->crud->button_custom[] = [
                'button_text' => 'Recusar assinatura', 'button_id' => 'recusar_assinatura',
                'button_name_action' => 'recusar_assinatura', 'button_value_action' => '1',
                'button_tipo' => 'danger'
            ];
        }
        if ($this->aeSignatarioService->verifyUsuarioCanAssinar()) {
            $this->crud->button_custom[] = [
                'button_text' => 'Assinar', 'button_id' => 'assinar',
                'button_name_action' => 'recusar_assinatura', 'button_value_action' => '0',
                'button_tipo' => 'primary',
            ];
        }

        $this->data['breadcrumbs'] = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
            'Ordens de Serviço / Fornecimento do Contrato ' . $this->contrato->numero =>
                backpack_url('autorizacaoexecucao/' . $this->contrato->id),
            'Assinatura' => false,
            // 'Voltar' => backpack_url('autorizacaoexecucao/' . $this->contrato->id),
        ];
    }

    public function assinar(Request $request)
    {
        try {
            abort_if(
                !strstr(session()->get('_previous.url'), route('assinagov.token')) ||
                !$this->aeSignatarioService->verifyUsuarioCanAssinar(),
                403
            );

            DB::beginTransaction();

            $this->aeSignatarioService->assinar($this->aeSignatario);

            $pathArquivoExplodido = explode('.', $this->arquivoGenerico->url);

            Storage::disk('public')->move($this->arquivoGenerico->url, $this->arquivoGenerico->url . '.old');
            Storage::disk('public')->move(
                $pathArquivoExplodido[0] . '-assinado.' . $pathArquivoExplodido[1],
                $this->arquivoGenerico->url
            );
            Storage::disk('public')->delete($this->arquivoGenerico->url . '.old');

            DB::commit();

            \Alert::add('success', 'ordem de serviço / fornecimento assinada com sucesso!')->flash();
            return redirect('autorizacaoexecucao/' . $this->contrato->id . '/' . request()->autorizacaoexecucao_id .
                '/assinatura/create');
        } catch (Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao assinar a ordem de serviço / fornecimento!')->flash();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            return redirect()->back();
        }
    }

    public function recusar(Request $request)
    {
        try {
            abort_if(
                !$this->aeSignatarioService->verifyUsuarioResponsavelCanRecusar(),
                403
            );

            $request->validate(['recusar_assinatura' => 'required', 'motivo_recusa' => 'required']);

            DB::beginTransaction();

            $this->aeSignatarioService->recusarAssinatura($this->aeSignatario, $request->motivo_recusa);

            DB::commit();

            \Alert::add('success', 'Assinatura recusada com sucesso!')->flash();
            return redirect('autorizacaoexecucao/' . $this->contrato->id);
        } catch (Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao recusar a assinatura, por favor tente novamente')->flash();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            return redirect()->back();
        }
    }
}
