<?php

namespace App\Http\Controllers\AutorizacaoExecucao;

use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoCrudTrait;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoExecucaoStatusHistorico;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;

/**
 * Class AutorizacaoExecucaoStatusHistoricoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AutorizacaoExecucaoStatusHistoricoCrudController extends CrudController
{
    use AutorizacaoexecucaoCrudTrait;
    use ListOperation;
    use CommonColumns;
    use Formatador;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->contrato = $this->getContrato();
        $this->autorizacaoexecucao = AutorizacaoExecucao::findOrFail(request()->autorizacaoexecucao_id);

        CRUD::setModel(AutorizacaoExecucaoStatusHistorico::class);
        CRUD::setRoute(config('backpack.base.route_prefix')  . '/autorizacaoexecucao/' . request()->contrato_id . '/' .
            request()->autorizacaoexecucao_id . '/status-historico');

        CRUD::addClause('where', 'autorizacaoexecucao_id', $this->autorizacaoexecucao->id);
        CRUD::addClause('orderBy', 'created_at');

        $this->exibirTituloPaginaMenu('Histórico da Ordem de Serviço / Fornecimento ' .
            $this->autorizacaoexecucao->numero);

        $this->crud->denyAccess(['update', 'create', 'delete', 'show']);

        $this->data['breadcrumbs'] = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
            'Ordens de Serviço / Fornecimento do Contrato ' . $this->contrato->numero =>
                backpack_url('autorizacaoexecucao/' . $this->contrato->id),
            'Histórico da OS/F' => false,
            // 'Voltar' => backpack_url('autorizacaoexecucao/' . $this->contrato->id),
        ];
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'label' => 'Responsável',
            'name' => 'usuario_id',
            'searchLogic' => false,
            'orderable' => false,
        ]);
        CRUD::addColumn([
            'label' => 'Status',
            'name' => 'status_id',
            'type' => 'relationship',
            'attribute' => 'descricao',
            'searchLogic' => false,
            'orderable' => false,
        ]);
        CRUD::addColumn([
            'label' => 'Observação',
            'name' => 'observacao',
            'searchLogic' => false,
            'orderable' => false,
        ]);
        CRUD::addColumn([
            'label' => 'Data',
            'name' => 'created_at',
            'searchLogic' => false,
            'orderable' => false
        ]);

        CRUD::removeAllButtons();
    }
}
