<?php

namespace App\Http\Controllers\AutorizacaoExecucao;

use App\Http\Requests\AutorizacaoexecucaoSaldoUtilizadoRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Models\Contrato;
use App\Models\Contratoitem;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Library\Widget;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class AutorizacaoexecucaoSaldoUtilizadoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AutorizacaoexecucaoSaldoUtilizadoCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    use ImportContent;

    private $contrato;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        abort(404, 'Funcionalidade Obsoleta');

        $this->contrato = $this->getContrato();

        $this->importarScriptCss([
            'assets/css/autorizacaoexecucao/saldoutilizado.css'
        ]);

        $this->importarScriptJs([
            'assets/js/numberField.js',
            'assets/js/autorizacaoexecucao/saldoutilizado.js'
        ]);

        CRUD::setModel(\App\Models\AutorizacaoexecucaoSaldoUtilizado::class);
        CRUD::addClause('where', 'contrato_id', $this->contrato->id);
        CRUD::setRoute(
            config('backpack.base.route_prefix') .
            '/autorizacaoexecucao/'.$this->contrato->id.
            '/saldo-utilizado'
        );

        CRUD::setEntityNameStrings('Saldo utilizado', 'Saldo utilizado do contrato ' . $this->contrato->numero);

        $this->breadCrumb();
    }

    private function getContrato()
    {
        $contratoId = request()->contrato_id;

        $contrato = Contrato::where('id', '=', $contratoId)
            ->whereHas('responsaveis', function ($query) {
                $query->where('user_id', '=', backpack_user()->id);
            })->first();

        if (!$contrato) {
            abort('403', config('app.erro_permissao'));
        }

        return $contrato;
    }

    private function breadCrumb(string $acaoTexto = '')
    {
        $breadcrumb = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
            'Listar saldo utilizado do contrato ' . $this->contrato->numero => false,
        ];

        if (!empty($acaoTexto)) {
            $breadcrumb['Listar saldo utilizado do contrato ' . $this->contrato->numero] = backpack_url(
                'autorizacaoexecucao/'.$this->contrato->id.'/saldo-utilizado'
            );
            $breadcrumb[$acaoTexto] = false;
        }

        $this->data['breadcrumbs'] = $breadcrumb;
    }


    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->setEntityNameStrings(
            'Listar saldo utilizado',
            'Saldo utilizado do contrato ' . $this->contrato->numero
        );
        $this->crud->text_button_redirect_create = 'Saldo utilizado';

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'quantidade',
            'Quantidade Solicitada'
        );

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'numero_autorizacao',
            'Número ordem de serviço / fornecimento'
        );

        $this->addColumnDateHour(true, true, true, true, 'created_at', 'Criado em');

        $this->crud->prefix_text_button_redirect_create = 'Adicionar';
        $this->crud->text_button_redirect_create = '';

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');
        CRUD::setValidation(AutorizacaoexecucaoSaldoUtilizadoRequest::class);

        $this->crud->addField([
            'name' => 'contrato_id',
            'type' => 'hidden',
            'value' => $this->contrato->id,
            'attributes' => [
                'class'=>'contrato-id'
            ]
        ]);

        $item = Contratoitem::with('tipo')
            ->where('contrato_id', $this->contrato->id)
            ->first(['tipo_id', 'contrato_id']);

        $this->crud->addField([
            'name' => 'tipo_item',
            'type' => 'hidden',
            'value' => $item->tipo->descres,
            'attributes' => [
                'readonly' => 'readonly',
            ],
            'attributes' => [
                'class'=> 'tipo_item'
            ]
        ]);

        $this->crud->addField([
            'name' => 'contratoitens_id',
            'label' => 'Contrato Item',
            'type' => 'select2',
            'attribute' => 'name',
            'model' => Contratoitem::class,
            'default' => '',
            'options' => (function ($query) {
                return $query->limit(10)
                    ->join('catmatseritens as c', 'c.id', '=', 'contratoitens.catmatseritem_id')
                    ->select(['contratoitens.id', 'c.descricao as name'])
                    ->where('contratoitens.contrato_id', '=', $this->contrato->id)
                    ->get();
            }),
            'wrapperAttributes' => [
                'class' => 'col-md-4 contrato-item'
            ],
        ]);

        $this->crud->addField([
            'name' => 'quantidade_disponivel',
            'label' => 'Quantidade disponível',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-4 quantidade_disponivel'
            ],
            'fake' => true
        ]);

        $this->crud->addField([
            'name' => 'quantidade',
            'label' => 'Quantidade',
            'attributes' => [
                'required' => 'required'
            ],
            'required' => true,
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'col-md-4 quantidade'
            ]
        ]);


        $this->crud->addField([
            'name' => 'valor_unitario',
            'label' => 'Valor Unitário',
            'type' => 'number',
            'attributes' => [
                'step' => '0.01'
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-4 valor-unitario'
            ],
        ]);

        $this->crud->addField([
            'name' => 'valor_total',
            'label' => 'Valor total',
            'attributes' => [
                'readonly' => 'readonly',
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-4 valor-total'
            ],
            'fake'=>true
        ]);

        $this->crud->addField([
            'name' => 'numero_autorizacao',
            'label' => 'Número ordem de serviço / fornecimento',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'col-md-4'
            ]
        ]);

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Salvar Saldo',
        ]);

        $this->breadCrumb('Adicionar');
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Atualizar AutorizacaoexecucaoSaldoUtilizado',
        ]);
        $this->setupCreateOperation();

        $this->breadCrumb('Editar');
    }

    public function getValorItem(Request $request)
    {
        $item = Contratoitem::find($request->item);
        return response()->json(['valor'=>$item->valorunitario]);
    }

    public function getQuantidadeItem(Request $request)
    {
        $item = Contratoitem::with('contrato:id,num_parcelas')->find($request->item);
        return response()->json(['quantidade' => $item->quantidade * $item->contrato->num_parcelas]);
    }
}
