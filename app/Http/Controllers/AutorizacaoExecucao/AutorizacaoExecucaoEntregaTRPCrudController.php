<?php

namespace App\Http\Controllers\AutorizacaoExecucao;

use App\Http\Requests\AutorizacaoExecucaoEntregaTRPTRDRequest;
use App\Http\Traits\Autorizacaoexecucao\AutorizacaoExecucaoEntregaSetupRootTrait;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Models\AutorizacaoExecucaoEntregaTRP;
use App\Models\ContratoLocalExecucao;
use App\Repositories\Contrato\ContratoResponsavelRepository;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoEntregaTRPTRDService;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use App\Services\AutorizacaoExecucao\UsuarioFornecedorAutorizacaoExecucaoEntregaService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class AutorizacaoExecucaoEntregaTRPCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AutorizacaoExecucaoEntregaTRPCrudController extends CrudController
{
    use AutorizacaoExecucaoEntregaSetupRootTrait;
    use CommonColumns;
    use \App\Http\Controllers\Operations\UpdateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->setupRoot();

        CRUD::setRoute(config('backpack.base.route_prefix') . '/autorizacaoexecucao/' . request()->contrato_id . '/' .
            request()->autorizacaoexecucao_id . '/entrega/trp');

        CRUD::setSubheading('', 'edit');

        $entrega = $this->crud->getEntry(\request()->id ?? \request()->entrega_id);
        $this->exibirTituloPaginaMenu(
            'Ordem de Serviço / Fornecimento nº ' .
                $this->autorizacaoexecucao->numero . ' do Contrato ' . $this->contrato->numero . ' - ' .
                $this->contrato->unidade->codigo,
            'Termo de Recebimento Provisório da Entrega nº ' . $entrega->getFormattedSequencial(),
            false
        );
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->importarScriptJs(['assets/js/admin/forms/autorizacaoExecucao/submit_assinatura.js']);
        $this->importarDatatableForm();

        $this->breadCrumb();
        $this->crud->setUpdateContentClass('col-md-12');

        $this->setupCreateOperationRoot();

        $this->crud->addField([
            'name' => 'introducao_trp',
            'type' => 'tinymce',
            'label' => 'Introdução',
            'required' => true,
            'options' => [
                'language' => 'pt_BR',
                'menubar' => false,
                'plugins' => [
                    'print autolink lists link table',
                ],
                'toolbar' => 'print | undo redo | copy cut paste |' .
                    ' bold italic underline strikethrough superscript subscript | 
                    backcolor forecolor link unlink blockquote | alignleft aligncenter alignright alignjustify | 
                    bullist numlist outdent indent | hr table removeformat',
            ],
        ]);

        $this->crud->addField(
            [
                'name' => 'itens_osf_entregas',
                'type' => 'table_selecionar_entrega_itens',
                'label' => 'itens',
                'wrapperAttributes' => ['class' => 'form-group col-md-4 pt-3'],
                'itens' => $this->autorizacaoexecucao
                    ->autorizacaoexecucaoItens
                    ->load(['itensEntrega' => function ($query) {
                        $query->where('autorizacao_execucao_entrega_id', \request()->id ?? 0);
                    }]),
            ]
        );

        $this->crud->addField([
            'name' => 'locais_execucao_entrega',
            'label' => 'Locais de Execução para Entrega',
            'type' => 'select2_multiple',
            'attribute' => 'descricao',
            'required' => false,
            'model' => 'App\Models\ContratoLocalExecucao',
            'value' => $this->crud->entry->locaisExecucao->pluck('id')->toArray(),
            'options' => (function ($query) {
                return $query->whereHas('autorizacoesExecucao', function ($q) {
                    $q->where('autorizacaoexecucao_id', request()->autorizacaoexecucao_id);
                })->get();
            }),
            'wrapperAttributes' => ['class' => 'form-group col-md-12 mt-1'],
        ]);

        $this->crud->addField([
            'name' => 'mes_ano_referencia',
            'label' => 'Mês/Ano de Referência',
            'type' => 'date_month',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12 mt-1'
            ],
            'required' => true,
        ]);

        $this->crud->addField([
            'name' => 'informacoes_complementares_trp',
            'type' => 'tinymce',
            'label' => 'Informações Complementares',
            'required' => true,
            'options' => [
                'language' => 'pt_BR',
                'menubar' => false,
                'plugins' => [
                    'print autolink lists link table',
                ],
                'toolbar' => 'print | undo redo | copy cut paste |' .
                    ' bold italic underline strikethrough superscript subscript | 
                    backcolor forecolor link unlink blockquote | alignleft aligncenter alignright alignjustify | 
                    bullist numlist outdent indent | hr table removeformat',
            ],
        ]);

        $contratoResponsavel = new ContratoResponsavelRepository(request()->contrato_id);

        $this->crud->addField([
            'type' => 'modal_select_signatarios_trp',
            'name' => 'select_prepostos_responsaveis[]',
            'responsaveis' => $contratoResponsavel->getResponsaveisContrato(),
        ]);

        $this->crud->setOperationSetting('showCancelButton', false);

        $backToPage = config('backpack.base.route_prefix') . '/autorizacaoexecucao/' . request()->contrato_id . '/'
            . request()->autorizacaoexecucao_id . '/entrega';
        $this->crud->button_custom = [
            ['button_text' => 'Voltar', 'button_id' => 'voltar',
                'button_name_action' => 'voltar', 'button_value_action' => '0',
                'button_icon' => 'fas fa-arrow-left', 'button_tipo' => 'secondary',
                'onclick' =>  "bloquearSubmit = true;window.location.href='$backToPage'"],
            ['button_text' => 'Salvar Rascunho', 'button_id' => 'rascunho',
                'button_name_action' => 'rascunho', 'button_value_action' => '1',
                'button_icon' => 'fas fa-edit', 'button_tipo' => 'secondary'],
            ['button_text' => 'Gerar', 'button_id' => 'assinatura',
                'button_name_action' => 'rascunho', 'button_value_action' => '0',
                'button_icon' => 'fas fa-save', 'button_tipo' => 'primary']
        ];
    }

    public function update(AutorizacaoExecucaoEntregaTRPTRDRequest $request)
    {
        try {
            DB::beginTransaction();
            $trpService = new AutorizacaoExecucaoEntregaTRPTRDService(new AutorizacaoExecucaoEntregaTRP());
            $trpService->gerar($request->validated(), request()->id);
            DB::commit();
            \Alert::add('success', 'Termo de Recebimento Provisório salvo com sucesso')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            \Alert::add('error', 'Erro ao gerar o TRP!')->flash();
            return redirect()->back()->withInput();
        }

        return redirect('/autorizacaoexecucao/' . request()->contrato_id . '/' .
            request()->autorizacaoexecucao_id . '/entrega');
    }

    public function ajustar(Request $request)
    {
        abort_if(
            $this->crud->getEntry($request->entrega_id)->situacao->descres !== 'ae_entrega_status_4',
            403,
            'Operacão não autorizada!'
        );

        try {
            DB::beginTransaction();
            $trpService = new AutorizacaoExecucaoEntregaTRPTRDService(new AutorizacaoExecucaoEntregaTRP());
            $trpService->ajustar(request()->entrega_id);
            DB::commit();
            \Alert::add('success', 'Termo de Recebimento Provisório retornado para em elaboração')->flash();
        } catch (Exception $e) {
            DB::rollBack();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            \Alert::add('error', 'Erro ao gerar o TRP!')->flash();
            return redirect()->back();
        }

        return redirect($this->crud->route . '/' . request()->entrega_id . '/edit');
    }

    private function breadCrumb()
    {
        $linkVoltar = config('backpack.base.route_prefix') . '/autorizacaoexecucao/' . request()->contrato_id . '/' .
            request()->autorizacaoexecucao_id . '/entrega';
        $this->data['breadcrumbs'] = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
            'Ordens de Serviço / Fornecimento do Contrato ' . $this->contrato->numero =>
                backpack_url('autorizacaoexecucao/' . $this->contrato->id),
            'Entregas Ordem de Serviço / Fornecimento' => $linkVoltar,
            "TRP Ordem de Serviço / Fornecimento" => false,
            // 'Voltar' => $linkVoltar,
        ];
    }
}
