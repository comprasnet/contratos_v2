<?php

namespace App\Http\Controllers\AutorizacaoExecucao;

use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoCrudTrait;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Models\AutorizacaoExecucaoEntregaTRP;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoEntregaSignatarioService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class AutorizacaoExecucaoEntregaTRPAssinaturaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AutorizacaoExecucaoEntregaTRPAssinaturaCrudController extends CrudController
{
    use AutorizacaoexecucaoCrudTrait;
    use CreateOperation;
    use CommonColumns;
    use Formatador;
    use ImportContent;

    private $trp;
    private $aeEntregaSignatarioService;
    private $aeSignatarioResponsavel;
    private $arquivoGenerico;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(AutorizacaoExecucaoEntregaTRP::class);

        CRUD::setRoute(config('backpack.base.route_prefix') . "/autorizacaoexecucao/" .
            request()->contrato_id . '/' . request()->autorizacaoexecucao_id . '/entrega/trp/' .
            request()->entrega_id . '/assinatura');

        $this->contrato = $this->getContrato();
        $this->exibirTituloPaginaMenu(
            'TRP Ordem de Serviço / Fornecimento do Contrato ' . $this->contrato->numero . ' - ' .
                $this->contrato->unidade->codigo,
            '',
            false
        );

        CRUD::setSubheading('Assinar', 'create');

        $this->trp = $this->crud->getEntry(request()->entrega_id);
        $this->arquivoGenerico = $this->trp->getArquivoTRP();
        $this->aeEntregaSignatarioService = new AutorizacaoExecucaoEntregaSignatarioService($this->trp);
        $this->aeSignatarioResponsavel = $this->aeEntregaSignatarioService->getSignatarioByUsuario();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        abort_if(
            in_array(
                $this->trp->situacao->descres,
                [
                    'ae_entrega_status_1',
                    'ae_entrega_status_2',
                    'ae_entrega_status_3',
                    'ae_entrega_status_5',
                    'ae_entrega_status_6',
                    'ae_entrega_status_9',
                ]
            ),
            403,
            'Não autorizado para assinar TRP.'
        );

        $this->crud->setCreateContentClass('col-md-12');

        $this->importarScriptJs([
            'assets/js/admin/forms/autorizacaoExecucao/submit_assinatura.js',
        ]);

        $linkVoltar = config('backpack.base.route_prefix') . '/autorizacaoexecucao/' . request()->contrato_id . '/' .
            request()->autorizacaoexecucao_id . '/entrega';
        $this->data['breadcrumbs'] = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
            'Ordens de Serviço / Fornecimento do Contrato ' . $this->contrato->numero =>
                backpack_url('autorizacaoexecucao/' . $this->contrato->id),
            'Entregas Ordem de Serviço / Fornecimento' => $linkVoltar,
            "Assinar TRP Ordem de Serviço / Fornecimento" => false,
            // 'Voltar' => $linkVoltar,
        ];

        $this->crud->addField([
            'name' => 'pdf_preview',
            'type' => 'custom_html',
            'value' => '
                <iframe class="pdf" style="width: 100%; aspect-ratio: 4 / 3; height: 100%"
                    src="' . Storage::url($this->arquivoGenerico->url) . '?temp=' . time() . '"
                    width="800" height="500">
                </iframe>',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        if ($this->aeSignatarioResponsavel) {
            $this->crud->addField([
                'name' => 'pagina_assinatura',
                'label' => 'Página da assinatura',
                'type' => 'label',
                'wrapper' => [
                    'class' => 'pagina_assinatura d-none',
                ],
                'value' => $this->aeSignatarioResponsavel->pivot->pagina_assinatura
            ]);

            $this->crud->addField([
                'name' => 'posicao_x_assinatura',
                'label' => 'Posição X da assinatura',
                'type' => 'label',
                'wrapper' => [
                    'class' => 'posicao_x_assinatura d-none',
                ],
                'value' => $this->aeSignatarioResponsavel->pivot->posicao_x_assinatura
            ]);

            $this->crud->addField([
                'name' => 'posicao_y_assinatura',
                'label' => 'Posição Y da assinatura',
                'type' => 'label',
                'wrapper' => [
                    'class' => 'posicao_y_assinatura d-none',
                ],
                'value' => $this->aeSignatarioResponsavel->pivot->posicao_y_assinatura
            ]);
        }

        $this->crud->setOperationSetting('showCancelButton', false);

        $this->crud->button_custom = [];
        $this->crud->button_custom[] = [
            'button_text' => 'Voltar', 'button_id' => 'voltar',
            'button_name_action' => 'voltar', 'button_value_action' => '0',
            'button_icon' => 'fas fa-arrow-left', 'button_tipo' => 'secondary',
            'onclick' =>  "bloquearSubmit = true;window.location.href='$linkVoltar'"
        ];
        if ($this->aeEntregaSignatarioService->verifyUsuarioCanAssinar()) {
            if ($this->trp->situacao->descres === 'ae_entrega_status_4') {
                $this->crud->button_custom[] = [
                    'button_text' => 'Ajustar', 'button_id' => 'ajustar_trp',
                    'button_name_action' => 'ajustar_trp', 'button_value_action' => '1',
                    'button_tipo' => 'warning'
                ];
            }

            $this->crud->button_custom[] = [
                'button_text' => 'Assinar', 'button_id' => 'assinar',
                'button_name_action' => 'recusar_assinatura', 'button_value_action' => '0',
                'button_tipo' => 'primary',
            ];
        }
    }

    public function assinar()
    {
        try {
            abort_if(
                !strstr(session()->get('_previous.url'), route('assinagov.token')) ||
                !$this->aeEntregaSignatarioService->verifyUsuarioCanAssinar(),
                403
            );

            DB::beginTransaction();

            $this->aeEntregaSignatarioService->assinar($this->aeSignatarioResponsavel);

            $pathArquivoExplodido = explode('.', $this->arquivoGenerico->url);

            Storage::disk('public')->move($this->arquivoGenerico->url, $this->arquivoGenerico->url . '.old');
            Storage::disk('public')->move(
                $pathArquivoExplodido[0] . '-assinado.' . $pathArquivoExplodido[1],
                $this->arquivoGenerico->url
            );
            Storage::disk('public')->delete($this->arquivoGenerico->url . '.old');

            DB::commit();
            \Alert::add('success', 'Termo de Recebimento Provisório assinada com sucesso!')->flash();
        } catch (Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao assinar o TRP!')->flash();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
        }

        return redirect($this->crud->route . '/create');
    }
}
