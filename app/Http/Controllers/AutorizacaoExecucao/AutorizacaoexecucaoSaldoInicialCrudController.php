<?php

namespace App\Http\Controllers\AutorizacaoExecucao;

use App\Http\Requests\AutorizacaoexecucaoSaldoInicialRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Models\Contrato;
use App\Models\Contratoitem;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;

/**
 * Class AutorizacaoexecucaoSaldoInicialCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AutorizacaoexecucaoSaldoInicialCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    use ImportContent;

    private $contratoId = 0;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->contratoId = request()->route('contrato_id');

        $this->importarScriptCss([
            'assets/css/autorizacaoexecucao/saldoinicial.css'
        ]);

        $this->importarScriptJs([
            'assets/js/autorizacaoexecucao/saldoinicial.js'
        ]);

        CRUD::setModel(\App\Models\AutorizacaoexecucaoSaldoInicial::class);
        CRUD::setRoute(
            config('backpack.base.route_prefix') . '/autorizacaoexecucao/'.$this->contratoId.'/saldo-inicial'
        );
        CRUD::setEntityNameStrings('Saldo inicial', 'Saldo inicial');

        $this->breadCrumb();
    }

    private function getContrato()
    {
        $contratoId = request()->contrato_id;

        $contrato = Contrato::where('id', '=', $contratoId)
            ->whereHas('responsaveis', function ($query) {
                $query->where('user_id', '=', backpack_user()->id);
            })->first();

        if (!$contrato) {
            abort('403', config('app.erro_permissao'));
        }

        return $contrato;
    }

    private function breadCrumb(bool $acao = false, $texto = 'Adicionar')
    {
        $contrato = $this->getContrato();


        $breadcrumb = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
            'Saldo Inicial do Contrato ' . $contrato->numero => false,
            'Lista Itens' => false,
        ];

        if ($acao) {
            $breadcrumb = [
                trans('backpack::crud.admin') => backpack_url('dashboard'),
                'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
                'Ordens de Serviço / Fornecimento do Contrato ' . $contrato->numero => false,
                $texto => false,
                // 'Voltar' => backpack_url('autorizacaoexecucao/' . $contrato->id),
            ];
        }

        $this->data['breadcrumbs'] = $breadcrumb;
    }


    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        $this->crud->text_button_redirect_create = 'Saldo inicial';
        $this->addColumnQuantidade(
            'quantidade',
            'Quantidade Solicitada',
            ['table' => true, 'modal' => true, 'show' => true, 'export' => true],
            17
        );

        $this->addColumnDate(true, true, true, true, 'vigencia_inicio', 'Vigência inicial');
        $this->addColumnDate(true, true, true, true, 'vigencia_fim', 'Vigência final');

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'numero_autorizacao',
            'Número ordem de serviço / fornecimento'
        );

        $this->crud->prefix_text_button_redirect_create = 'Adicionar';
        $this->crud->text_button_redirect_create = '';

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');
        CRUD::setValidation(AutorizacaoexecucaoSaldoInicialRequest::class);
        $contratoId = $this->contratoId;

        $this->crud->addField([
            'name' => 'contrato_id',
            'type' => 'hidden',
            'value' => $this->contratoId,
            'attributes' => [
                'class'=>'contrato-id'
            ]
        ]);

        $this->crud->addField([
            'name' => 'contratoitens_id',
            'label' => 'Contrato Item',
            'type' => 'select2',
            'attribute' => 'name',
            'model' => Contratoitem::class,
            'default' => '',
            'options' => (function ($query) use ($contratoId) {
                return $query->limit(10)
                    ->join('catmatseritens as c', 'c.id', '=', 'contratoitens.catmatseritem_id')
                    ->select(['contratoitens.id', 'c.descricao as name'])
                    ->where('contratoitens.contrato_id', '=', $contratoId)
                    ->get();
            }),
            'wrapperAttributes' => [
                'class' => 'col-md-4 contrato-item'
            ],
        ]);


        $this->crud->addField([
            'name' => 'quantidade',
            'label' => 'Quantidade',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'col-md-4 quantidade'
            ]
        ]);


        $this->crud->addField([
            'name' => 'valor_unitario',
            'label' => 'Valor Unitário',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'col-md-4 valor-unitario'
            ],
        ]);

        $this->crud->addField([
            'name' => 'valor_total',
            'label' => 'Valor total',
            'type' => 'text',
            'attributes' => [
                'readonly'=>'readonly'
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-4 valor-total'
            ],
            'fake'=>true
        ]);


        $this->crud->addField([
            'name' => 'vigencia_inicio',
            'label' => 'Vigência Inicial',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'col-md-4 vigencia_inicio'
            ]
        ]);


        $this->crud->addField([
            'name' => 'vigencia_fim',
            'label' => 'Vigência Final',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'col-md-4 vigencia_fim'
            ]
        ]);


        $this->crud->addField([
            'name' => 'numero_autorizacao',
            'label' => 'Número ordem de serviço / fornecimento',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'col-md-4'
            ]
        ]);

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Salvar Saldo',
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Atualizar AutorizacaoexecucaoSaldoInicial',
        ]);
        $this->setupCreateOperation();
    }

    public function getTipoItemContrato($item)
    {
        $item = Contratoitem::find($item);
        return response()->json(['tipo'=>$item->tipo->descres]);
    }


    public function getValorItem($item)
    {
        $item = Contratoitem::find($item);
        return response()->json(['valor'=>$item->valorunitario]);
    }
}
