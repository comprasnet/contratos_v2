<?php

namespace App\Http\Controllers\AutorizacaoExecucao;

use App\Http\Requests\AutorizacaoExecucaoAlterarRequest;
use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoHistoricoCrudTrait;
use App\Models\ArquivoGenerico;
use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoexecucaoHistorico;
use App\Repositories\AutorizacaoExecucao\AutorizacaoExecucaoHistoricoRepository;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoHistoricoAlterarService;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoItensService;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class AutorizacaoExecucaoAlterarCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AutorizacaoExecucaoAlterarCrudController extends CrudController
{
    use AutorizacaoexecucaoHistoricoCrudTrait;
    use UpdateOperation;
    use DeleteOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->settingsSetup('alterar');

        $this->crud->addButtonFromModelFunction(
            'line',
            'getViewButtonsAlterar',
            'getViewButtonsAlterar',
            'beginning'
        );

        CRUD::addButtonFromView(
            'line',
            'button_assinar_autorizacaoexecucao',
            'button_assinar_autorizacaoexecucao',
            'end'
        );
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->text_button_redirect_create = 'alteração';

        $this->addColumnModelFunction('situacao', 'Situação', 'getSituacaoChipComponent');
        $this->addColumnModelFunction('sequencial', 'Nº Alteração', 'getSequencial');
        $this->addColumnModelFunction('tipo_alteracao', 'Tipo da alteração', 'getTipoAlteracao');
        $this->addColumnModelFunction('data_alteração', 'Data da alteração', 'getDataInicioAlteracao');
        $this->addColumnModelFunction(
            'status_assinaturas_signatarios',
            'Status Assinaturas dos Signatários',
            'getViewStatusAssinaturasSignatarios'
        );

        $this->cabecalho();

        $this->breadCrumb(true);
    }

    protected function setupShowOperation()
    {
        $autorizacaoExecucaoHistorico = AutorizacaoexecucaoHistorico::findOrFail(request()->id);

        $this->addColumnUsuarioResponsavel();

        $this->crud->addColumn([
            'name' => 'sequencial',
            'type' => 'model_function_raw',
            'label' => 'Nº da Alteração',
            'function_name' => 'getSequencial',
        ]);

        $this->crud->addColumn([
            'name' => 'tipo_alteracao',
            'type' => 'model_function_raw',
            'label' => 'Tipo da alteração',
            'function_name' => 'getTipoAlteracao',
        ]);

        $this->crud->addColumn([
            'name' => 'data_assinatura_alteracao',
            'type' => 'model_function_raw',
            'label' => 'Data de assinatura da alteração',
            'function_name' => 'getDataAssinaturaAlteracao',
        ]);

        $this->crud->addColumn([
            'name' => 'arquivo',
            'type' => 'model_function_raw',
            'label' => 'Arquivo',
            'function_name' => 'getArquivoViewLink',
        ]);

        if ($autorizacaoExecucaoHistorico->verificaTipoAlteracao('vigencia')) {
            $this->addColumnTable(
                'data_vigencia_fim',
                $this->fields['data_vigencia_fim']['label'],
                $autorizacaoExecucaoHistorico->getAntesDepoisColumnTable(
                    'data_vigencia_fim',
                    $this->fields['data_vigencia_fim']['value']['formated'],
                )
            );
        }

        if ($autorizacaoExecucaoHistorico->verificaTipoAlteracao('extingir')) {
            $this->crud->addColumn([
                'name' => 'data_extincao',
                'type' => 'model_function_raw',
                'label' => 'Data da extinção',
                'function_name' => 'getDataExtincao',
            ]);
        }

        if ($autorizacaoExecucaoHistorico->verificaTipoAlteracao('itens')) {
            $this->addColumnItensAntesDepois();
        }

        $this->breadCrumb();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');
        CRUD::setValidation(AutorizacaoExecucaoAlterarRequest::class);

        $this->importarScriptJs([
            'assets/js/admin/forms/autorizacaoExecucao/alterar.js'
        ]);


        $aehAlterarService = new AutorizacaoExecucaoHistoricoAlterarService($this->autorizacaoexecucao);

        $button_id = 'alterar';
        $button_tipo = 'primary ajax';
        if ($aehAlterarService->verifyAlteracoesAguardandoAssinaturaOuAssinada()) {
            $button_id .= '_disabled';
            $button_tipo .= ' disabled';
        }

        $this->crud->button_custom = [
            ['button_text' => 'Salvar Rascunho', 'button_id' => 'rascunho',
                'button_name_action' => 'rascunho', 'button_value_action' => '1',
                'button_icon' => 'fas fa-edit', 'button_tipo' => 'secondary ajax'],
            ['button_text' => 'Enviar para Assinatura', 'button_id' => $button_id,
                'button_name_action' => 'rascunho', 'button_value_action' => '0',
                'button_icon' => 'fas fa-share', 'button_tipo' => $button_tipo],
        ];

        $this->cabecalho();

        $this->camposAutorizacaoExecucao(true);

        $this->crud->addField([
            'name' => 'tipo_alterar',
            'label' => 'Tipo de alteração',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'type' => 'label',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
            'value' => null
        ]);

        $aeHistorico = AutorizacaoexecucaoHistorico::find(request()->id);

        $this->crud->addField([
            'label' => 'Vigência',
            'name' => 'tipo_alteracao[vigencia]',
            'type' => 'checkbox',
            'wrapper' => [
                'class' => 'form-group col-md-4',
            ],
            'value' => $aeHistorico ? $aeHistorico->verificaTipoAlteracao('vigencia') : false
        ]);

        $this->crud->addField([
            'label' => 'Item(ns) (Acréscimo ou Supressão)',
            'name' => 'tipo_alteracao[itens]',
            'type' => 'checkbox',
            'wrapper' => [
                'class' => 'form-group col-md-4',
            ],
            'value' => $aeHistorico ? $aeHistorico->verificaTipoAlteracao('itens') : false
        ]);

        $this->crud->addField([
            'label' => 'Extinguir Ordem de Serviço / Fornecimento',
            'name' => 'tipo_alteracao[extingir]',
            'type' => 'checkbox',
            'wrapper' => [
                'class' => 'form-group col-md-4',
            ],
            'value' => $aeHistorico ? $aeHistorico->verificaTipoAlteracao('extingir') : false
        ]);

        $this->crud->addField([
            'name' => 'data_inicio_alteracao',
            'label' => 'Data de início da alteração',
            'type' => 'date',
            'required' => 'required',
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3'
            ],
        ]);

        $this->crud->addField([
            'name' => 'justificativa_motivo',
            'label' => 'Justificativa da alteração',
            'type' => 'textarea',
            'required' => 'required',
            'wrapperAttributes' => [
                'class' => 'col-md-8 br-textarea pt-3'
            ],
            'attributes' => ['id' => 'justificativa_motivo'],
            'escaped' => false,
        ]);

        $this->crud->addField([
            'name' => 'data_vigencia_fim',
            'type' => 'date',
            'label' => $this->fields['data_vigencia_fim']['label'],
            'required' => true,
            'value' => $aeHistorico ? $aeHistorico->getAttributes()['data_vigencia_fim_depois'] : null,
            'wrapper' => [
                'class' => 'form-group col-md-4',
            ],
        ]);


        $this->crud->addField([
            'name' => 'data_extincao',
            'type' => 'date',
            'label' => 'Data da extinção',
            'required' => true,
            'value' => null,
            'wrapper' => [
                'class' => 'form-group col-md-4',
            ],
        ]);

        $this->crud->addField([
            'name' => 'breakline',
            'type' => 'custom_html',
            'value' => '<br>',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $autorizacaoExecucaoService = new AutorizacaoExecucaoService();
        $autorizacaoExecucaoService->setContrato($this->contrato);
        $prepostosResonsaveis = $autorizacaoExecucaoService->getPrepostosResponsaveisContrato();
        $this->crud->addField([
            'type' => 'modal_select_signatarios',
            'name' => 'select_prepostos_responsaveis[]',
            'responsaveis' => $prepostosResonsaveis['responsaveis'],
            'prepostos' => $prepostosResonsaveis['prepostos']
        ]);

        if ($aeHistorico && $aeHistorico->verificaTipoAlteracao('itens')) {
            $valueRelationTable = AutorizacaoExecucaoItensService::selectItensAndSumEntregas(
                $aeHistorico->autorizacaoexecucaoItens()
            )->where('tipo_historico', 'depois')->get();
        } else {
            $valueRelationTable = $this->fields['autorizacaoexecucaoItens']['value'];
        }

        $this->camposAutorizacaoExecucaoItens($valueRelationTable);

        $this->breadCrumb();
    }

    public function store(AutorizacaoExecucaoAlterarRequest $request)
    {
        try {
            DB::beginTransaction();
            $autorizacaoexecucaoHistoricoAlterar = new AutorizacaoExecucaoHistoricoAlterarService(
                $this->autorizacaoexecucao
            );

            $autorizacaoexecucaoHistoricoAlterar->create($request->validated());

            \Alert::add('success', 'Cadastro Realizado com sucesso!')->flash();

            DB::commit();
            return response()->json(['redirect' => '/autorizacaoexecucao/' .
                request()->contrato_id . '/' . request()->autorizacaoexecucao_id . '/alterar']);
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao realizar a alteração!')->flash();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            return redirect()->back(500);
        }
    }


    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');
        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Atualizar AutorizacaoExecucaoAlterar',
        ]);
        $this->setupCreateOperation();
        if (!$this->crud->getModel() instanceof AutorizacaoexecucaoHistorico) {
            CRUD::setModel(AutorizacaoexecucaoHistorico::class);
            $this->crud->getEntry(request()->id);
        }
    }

    public function update(AutorizacaoExecucaoAlterarRequest $request)
    {
        try {
            DB::beginTransaction();
            $autorizacaoexecucaoHistoricoAlterar = new AutorizacaoExecucaoHistoricoAlterarService(
                $this->autorizacaoexecucao
            );

            $autorizacaoexecucaoHistoricoAlterar->update($request->validated(), request()->id);

            \Alert::add('success', 'Cadastro Realizado com sucesso!')->flash();

            DB::commit();

            return response()->json(['redirect' => '/autorizacaoexecucao/' .
                request()->contrato_id . '/' . request()->autorizacaoexecucao_id . '/alterar']);
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao realizar a alteração!')->flash();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            return redirect()->back();
        }
    }

    public function destroy(Request $request)
    {
        try {
            DB::beginTransaction();
            $autorizacaoExecucaoHistorico = new AutorizacaoExecucaoHistoricoRepository($this->autorizacaoexecucao);

            $autorizacaoExecucaoHistorico->deletar($request->id);

            \Alert::add('success', 'Exclusão Realizada com sucesso!')->flash();
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao realizar a exclusão!')->flash();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            return false;
        }
    }
}
