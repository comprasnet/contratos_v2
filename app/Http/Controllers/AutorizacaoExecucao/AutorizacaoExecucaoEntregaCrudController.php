<?php

namespace App\Http\Controllers\AutorizacaoExecucao;

use App\Http\Requests\AutorizacaoExecucaoEntregaRequest;
use App\Http\Traits\Autorizacaoexecucao\AutorizacaoExecucaoEntregaSetupRootTrait;
use App\Models\ArquivoGenerico;
use App\Models\CodigoItem;
use App\Repositories\Contrato\ContratoParametroRepository;
use App\Repositories\Contrato\ContratoResponsavelRepository;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use App\Services\AutorizacaoExecucao\UsuarioFornecedorAutorizacaoExecucaoEntregaService;
use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use App\Http\Traits\CommonColumns;
use App\Models\AutorizacaoExecucaoEntrega;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class AutorizacaoExecucaoEntregaCrudController extends CrudController
{
    use AutorizacaoExecucaoEntregaSetupRootTrait;
    use CommonColumns;
    use UpdateOperation;
    use DeleteOperation;

    private $usuarioFornecedorAutorizacaoExecucaoEntregaService;

    public function __construct(
        UsuarioFornecedorAutorizacaoExecucaoEntregaService $usuarioFornecedorAutorizacaoExecucaoEntregaService
    ) {
        parent::__construct();
        $this->usuarioFornecedorAutorizacaoExecucaoEntregaService = $usuarioFornecedorAutorizacaoExecucaoEntregaService;
    }

    public function setup()
    {
        $this->setupRoot();

        CRUD::setRoute(config('backpack.base.route_prefix') . '/autorizacaoexecucao/' . request()->contrato_id . '/' .
            request()->autorizacaoexecucao_id . '/entrega');

        CRUD::setSubheading('', 'create');
        CRUD::setSubheading('', 'edit');
        CRUD::setSubheading('', 'index');

        $this->crud->addButtonFromModelFunction(
            'line',
            'getViewButtons',
            'getViewButtons',
        );

        $contratoResponsavelRepository = new ContratoResponsavelRepository($this->contrato->id);

        $this->crud->isUsuarioResponsavelContrato = $contratoResponsavelRepository->isUsuarioResponsavel();

        CRUD::addButtonFromView('line', 'duplicar', 'duplicar', 'beginning');
        CRUD::modifyButton('duplicar', ['title' => 'Duplicar Entrega']);

        CRUD::addButtonFromView(
            'line',
            'more.entrega_old',
            'more.entrega_old',
            'end'
        );

        $this->exibirTituloPaginaMenu(
            'Contrato ' . $this->contrato->numero . ' - ' . $this->contrato->unidade->codigo,
            'Entregas da Ordem de Serviço / Fornecimento nº ' . $this->autorizacaoexecucao->numero,
            false
        );
    }

    protected function setupListOperation()
    {
        $this->crud->addButton('top', 'create', 'view', 'crud::buttons.button_criar_entrega');
        $this->crud->addButton('top', 'create_external', 'view', 'crud::buttons.button_criar_entrega');

        $this->cabecalho();

        $this->crud->addColumn([
            'name' => 'situacao',
            'label' => 'Situação',
            'type' => 'model_function',
            'function_name' => 'retornaTextoSituacaoSpamColorido',
            'limit' => 9999,
            'priority' => 1,
        ]);

        $this->crud->addColumn([
            'name' => 'sequencial',
            'label' => 'Número Entrega',
            'type' => 'model_function',
            'function_name' => 'getFormattedSequencial',
        ]);

        $this->crud->addColumn([
            'name' => 'valor_entrega',
            'label' => 'Valor Entrega',
            'type' => 'model_function',
            'function_name' => 'retornaValorEntregaFormatado'
        ]);

        $this->crud->addColumn([
            'name' => 'data_entrega',
            'label' => 'Data Entrega',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'status_assinaturas_signatarios',
            'type' => 'model_function',
            'label' => 'Status Assinaturas do TRP',
            'function_name' => 'getViewStatusAssinaturasSignatariosTRP',
            'limit' => 999999999,
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'status_assinaturas_signatarios_trd',
            'type' => 'model_function',
            'label' => 'Status Assinaturas do TRD',
            'function_name' => 'getViewStatusAssinaturasSignatariosTRD',
            'limit' => 999999999,
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'data_prazo_trp',
            'label' => 'Previsão para o TRP',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_prazo_trd',
            'label' => 'Previsão para o TRD',
            'type' => 'date'
        ]);

        $this->crud->enableExportButtons();

        $this->breadCrumb(true);
    }

    public function setupShowOperation()
    {
        $this->crud->addColumn([
            'name' => 'situacao',
            'label' => 'Situação',
            'type' => 'model_function',
            'function_name' => 'retornaTextoSituacao',
            'limit' => 9999,
        ]);

        $this->crud->addColumn([
            'name' => 'sequencial',
            'label' => 'Número Entrega',
            'type' => 'model_function',
            'function_name' => 'getFormattedSequencial'
        ]);

        $this->crud->addColumn([
            'name' => 'valor_entrega',
            'label' => 'Valor Total Entrega',
            'type' => 'model_function',
            'function_name' => 'retornaValorEntregaFormatado'
        ]);

        $this->crud->addColumn([
            'name' => 'data_entrega',
            'label' => 'Data Entrega',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_prazo_trp',
            'label' => 'Data prevista para o recebimento provisório',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_prazo_trd',
            'label' => 'Data prevista para o recebimento definitivo',
            'type' => 'date'
        ]);

        $this->addColumnText(
            false,
            true,
            true,
            true,
            'informacoes_complementares',
            'Informações Complementares'
        );

        $this->crud->addColumn([
            'name' => 'locais_execucao_entrega',
            'type' => 'model_function_raw',
            'label' => 'Locais de Execução',
            'function_name' => 'getLocaisExecucao',
        ]);

        $this->crud->addColumn([
            'name' => 'mes_ano_referencia',
            'label' => 'Mês/Ano de Referência',
            'type' => 'model_function',
            'function_name' => 'getMesAnoReferencia'
        ]);

        $this->addColumnTablePlain(
            'entregaItens',
            'Itens entregues',
            [
                'tipo_material_os_entrega' => 'Tipo',
                'tipo_item' => 'Item',
                'quantidade_informada_formatado' => 'Quantidade',
                'valor_glosa_formatado' => 'Glosa',
                'valor_unitario_item' => 'Valor Unitário',
                'valor_total_item' => 'Valor Total'
            ]
        );

        $this->crud->addColumn([
            'name' => 'anexos',
            'type' => 'model_function_raw',
            'label' => 'Anexos',
            'function_name' => 'getAnexosViewLink',
        ]);

        $this->addColumnText(
            false,
            true,
            true,
            true,
            'justificativa_motivo_analise',
            'Justificativa/Motivo da Análise'
        );

        $this->breadCrumb();
    }

    public function setupCreateOperation()
    {
        $this->setupCreateOperationRoot();

        $this->crud->setCreateContentClass('col-md-12');

        $autorizacaoExecucaoEntrega = new AutorizacaoExecucaoEntrega();
        $this->crud->external = request()->external ?? 0;
        if (request()->id) {
            $autorizacaoExecucaoEntrega = $this->crud->getEntry(request()->id);
            $this->crud->external = $autorizacaoExecucaoEntrega->originado_sistema_externo;
        }

        $this->crud->addField([
            'name' => 'originado_sistema_externo',
            'type' => 'hidden',
            'label' => 'Originado Sistema Externo',
            'value' => (int) $this->crud->external,
            'attributes' => ['readonly' => 'readonly'],
        ]);

        $this->crud->addField(
            [
                'name' => 'itens_osf_entregas',
                'type' => 'table_selecionar_entrega_itens',
                'label' => 'itens',
                'wrapperAttributes' => ['class' => 'form-group col-md-4 pt-3'],
                'itens' => $this->autorizacaoexecucao
                    ->autorizacaoexecucaoItens
                    ->load(['itensEntrega' => function ($query) {
                        $query->where('autorizacao_execucao_entrega_id', \request()->id ?? 0);
                    }]),
            ]
        );

        $this->crud->addField([
            'name' => 'informacoes_complementares',
            'type' => 'tinymce',
            'required' => !$this->crud->external,
            'label' => 'Informações Complementares',
            'options' => [
                'language' => 'pt_BR',
                'menubar' => false,
                'plugins' => [
                    'print autolink lists link table',
                ],
                'toolbar' => 'print | undo redo | copy cut paste |' .
                    ' bold italic underline strikethrough superscript subscript | 
                    backcolor forecolor link unlink blockquote | alignleft aligncenter alignright alignjustify | 
                    bullist numlist outdent indent | hr table removeformat',
            ],
        ]);

        $this->crud->addField([
            'name' => 'locais_execucao_entrega',
            'label' => 'Locais de Execução para Entrega',
            'type' => 'select2_multiple',
            'attribute' => 'descricao',
            'required' => false,
            'model' => 'App\Models\ContratoLocalExecucao',
            'value' => isset(request()->id)
                ? $autorizacaoExecucaoEntrega->locaisExecucao->pluck('id')->toArray()
                : [],
            'options' => (function ($query) {
                return $query->whereHas('autorizacoesExecucao', function ($q) {
                    $q->where('autorizacaoexecucao_id', request()->autorizacaoexecucao_id);
                })->get();
            }),
            'wrapperAttributes' => ['class' => 'form-group col-md-12 mt-1'],
        ]);

        $this->crud->addField([
            'name' => 'mes_ano_referencia',
            'label' => 'Mês/Ano de Referência',
            'type' => 'date_month',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12 mt-1'
            ],
            'required' => true,
        ]);

        $dataPrazoRecebimentoProvisorio = null;
        $dataPrazoRecebimentoDefinitivo = null;
        if (!$this->crud->external) {
            $contratoParametroRepository = new ContratoParametroRepository(request()->contrato_id);
            $dataPrazoRecebimentoProvisorio = $contratoParametroRepository->getDataPrazoRecebimentoProvisorio();
            $dataPrazoRecebimentoDefinitivo = $contratoParametroRepository->getDataPrazoRecebimentoDefinitivo();
        }

        $this->crud->addField([
            'name' => 'data_entrega',
            'label' =>  'Data efetiva da entrega',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4 mt-1'
            ],
            'required' => true,
            'value' => $autorizacaoExecucaoEntrega->data_entrega ?? date('Y-m-d')
        ]);

        $this->crud->addField([
            'name' => 'data_prazo_trp',
            'label' =>  'Data prevista para o recebimento provisório',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4 mt-1'
            ],
            'required' => !$this->crud->external,
            'value' => $autorizacaoExecucaoEntrega->data_prazo_trp ?? $dataPrazoRecebimentoProvisorio
        ]);

        $this->crud->addField([
            'name' => 'data_prazo_trd',
            'label' =>  'Data prevista para recebimento definitivo',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4 mt-1'
            ],
            'required' => !$this->crud->external,
            'value' => $autorizacaoExecucaoEntrega->data_prazo_trd ?? $dataPrazoRecebimentoDefinitivo
        ]);

        $codigoItem = CodigoItem::where('descres', 'ae_entrega_arquivo')->first();

        $autorizacaoExecucaoEntrega = request()->id ?
            AutorizacaoExecucaoEntrega::find(request()->id) :
            new AutorizacaoExecucaoEntrega();

        $anexos = $autorizacaoExecucaoEntrega->anexos()->where('tipo_id', $codigoItem->id)->get();

        $this->crud->addField([
            'name' => 'anexos',
            'label' => 'Anexos - Entrega',
            'type' => 'upload_anexos_autorizacaoexecucao',
            'anexos' => $anexos->toArray(),
            'accept' => '*',
        ]);

        if ($this->crud->external) {
            $this->crud->addField([
                'name' => 'informar_trp',
                'label' => 'Informar Termo de Recebimento Provisório',
                'type' => 'checkbox',
                'value' => true,
                'attributes' => [
                    'checked' => true,
                    'readonly' => 'readonly',
                    'disabled' => 'disabled',
                ]
            ]);

            $arquivoTRP = $autorizacaoExecucaoEntrega->getArquivoTRP();

            $this->crud->addField([
                'name' => 'arquivo_trp',
                'label' => 'Arquivo - Termo de Recebimento Provisório assinado',
                'type' => 'upload_custom_generic',
                'required' => true,
                'attributes' => ['id' => 'arquivo_trp'],
                'model' => ArquivoGenerico::class,
                'upload' => true,
                'subfields' => null,
                'colum_filter' => [
                    'id' => $arquivoTRP ? $arquivoTRP->id : null,
                ]
            ]);

            $this->importarScriptJs(['assets/js/autorizacaoexecucao/informar_trd.js']);

            $arquivoTRD = $autorizacaoExecucaoEntrega->getArquivoTRD();

            $this->crud->addField([
                'name' => 'informar_trd',
                'label' => 'Informar Termo de Recebimento Definitivo',
                'type' => 'checkbox',
                'value' => (bool) $arquivoTRD,
                'attributes' => [
                    'id' => 'informar_trd',
                ]
            ]);

            $this->crud->addField([
                'name' => 'arquivo_trd',
                'label' => 'Arquivo - Termo de Recebimento Definitivo assinado',
                'type' => 'upload_custom_generic',
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6',
                    'id' => 'arquivo_trd',
                    'style' => $arquivoTRD ? 'display: block;' : 'display: none;'
                ],
                'required' => true,
                'model' => ArquivoGenerico::class,
                'upload' => true,
                'subfields' => null,
                'colum_filter' => [
                    'id' => $arquivoTRD ? $arquivoTRD->id : null,
                ]
            ]);

            $this->crud->addField([
                'name' => 'incluir_instrumento_cobranca',
                'label' => 'Deseja incluir no Termo de Recebimento Definitivo a Autorização para Emissão do Instrumento 
                    de Cobrança?',
                'type' => 'radio',
                'default' => 0,
                'options' => [0 => 'Não', 1 => 'Sim'],
                'required' => true,
                'attributes' => [
                    'required' => 'required',
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6',
                    'id' => 'incluir_instrumento_cobranca',
                    'style' => $arquivoTRD ? 'display: block;' : 'display: none;'
                ],
                'inline' => 'd-inline-block'
            ]);
        }

        $buttonText = $this->crud->external ? 'Informar' : 'Elaborar TRP';
        $this->crud->button_custom = [
            ['button_text' => 'Salvar Rascunho', 'button_id' => 'rascunho',
                'button_name_action' => 'rascunho', 'button_value_action' => '1',
                'button_icon' => 'fas fa-edit', 'button_tipo' => 'secondary'],
            ['button_text' => $buttonText, 'button_id' => 'informar',
                'button_name_action' => 'rascunho', 'button_value_action' => '0',
                'button_icon' => 'fas fa-save', 'button_tipo' => 'primary']
        ];
    }

    public function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');

        $this->setupCreateOperation();
    }

    public function store(AutorizacaoExecucaoEntregaRequest $request)
    {
        try {
            DB::beginTransaction();
            $entrega = $this->usuarioFornecedorAutorizacaoExecucaoEntregaService->salvarEntrega(
                $request->validated(),
                $this->contrato->id,
                $this->autorizacaoexecucao->id
            );
            $aeService = new AutorizacaoExecucaoService();
            $aeService->setAutorizacaoexecucao($entrega->autorizacao);
            $aeService->alteraSituacaoSeAtingirSaldoSolicitadoAposTRD();
            DB::commit();
            \Alert::add('success', 'Entrega criada com sucesso')->flash();
        } catch (Exception $e) {
            DB::rollBack();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            \Alert::add('error', 'Erro ao criar a entrega!')->flash();
            return redirect()->back()->withInput();
        }

        return $this->redirect($entrega);
    }

    public function update(AutorizacaoExecucaoEntregaRequest $request)
    {
        try {
            DB::beginTransaction();
            $entrega = $this->usuarioFornecedorAutorizacaoExecucaoEntregaService->alterarEntrega(
                $request->validated(),
                $request->get('id')
            );
            $aeService = new AutorizacaoExecucaoService();
            $aeService->setAutorizacaoexecucao($entrega->autorizacao);
            $aeService->alteraSituacaoSeAtingirSaldoSolicitadoAposTRD();
            DB::commit();
            \Alert::add('success', 'Entrega alterada com sucesso')->flash();
        } catch (Exception $e) {
            DB::rollBack();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            \Alert::add('error', 'Erro ao alterada a entrega!')->flash();
            return redirect()->back()->withInput();
        }

        return $this->redirect($entrega);
    }

    public function duplicar(Request $request)
    {
        try {
            DB::beginTransaction();

            $this->usuarioFornecedorAutorizacaoExecucaoEntregaService->duplicarEntrega($request->entrega_id);

            DB::commit();

            \Alert::add('success', 'Duplicação realizada com sucesso!')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao duplicar a ordem de serviço / fornecimento!')->flash();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
        }

        return redirect()->back();
    }

    public function notificarFiscaisDataPrazoTRP()
    {
        $situacoes = CodigoItem::whereIn('descres', [
            'ae_entrega_status_1',
            'ae_entrega_status_2',
            'ae_entrega_status_4',
            'ae_entrega_status_6',
        ])->pluck('id');

        $entregas = AutorizacaoExecucaoEntrega::whereIn('situacao_id', $situacoes)
            ->where(function ($query) {
                $query->where('data_prazo_trp', Carbon::now()->format('Y-m-d'))
                    ->orwhere('data_prazo_trp', Carbon::now()->addDays(3)->format('Y-m-d'))
                    ->orwhere('data_prazo_trp', Carbon::now()->addDays(7)->format('Y-m-d'));
            })
            ->get();


        foreach ($entregas as $entrega) {
            $this->usuarioFornecedorAutorizacaoExecucaoEntregaService->notificarFiscaisContrato($entrega);
        }
    }

    private function redirect(AutorizacaoExecucaoEntrega $entrega)
    {
        if ($entrega->rascunho || $entrega->originado_sistema_externo) {
            return redirect('autorizacaoexecucao/' .
                $this->contrato->id . '/' .
                $this->autorizacaoexecucao->id . '/entrega');
        }

        return redirect('autorizacaoexecucao/' .
            $this->contrato->id . '/' .
            $this->autorizacaoexecucao->id . '/entrega/trp/' .
            $entrega->id . '/edit');
    }

    private function breadCrumb(bool $listOperation = false)
    {
        $linkVoltar = backpack_url('autorizacaoexecucao/' .
            $this->contrato->id . '/' .
            $this->autorizacaoexecucao->id . '/entrega');

        if ($listOperation) {
            $linkVoltar = backpack_url('autorizacaoexecucao/' . $this->contrato->id);
        }

        $this->data['breadcrumbs'] = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
            'Ordens de Serviço / Fornecimento do Contrato ' . $this->contrato->numero =>
                backpack_url('autorizacaoexecucao/' . $this->contrato->id),
            "Entrega Ordem de Serviço / Fornecimento" => false,
            // 'Voltar' => $linkVoltar,
        ];
    }
}
