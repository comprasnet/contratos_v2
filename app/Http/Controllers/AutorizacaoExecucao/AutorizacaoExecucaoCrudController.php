<?php

namespace App\Http\Controllers\AutorizacaoExecucao;

use App\Http\Requests\AutorizacaoExecucaoRequest;
use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoCrudTrait;
use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoSetupRootTrait;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\DateFormat;
use App\Models\ArquivoGenerico;
use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoexecucaoSaldoUtilizado;
use App\Models\CodigoItem;
use App\Models\Contratounidadedescentralizada;
use App\Models\Empenho;
use App\Models\Orgaoconfiguracao;
use App\Models\Unidade;
use App\Models\UnidadeConfiguracao;
use App\Repositories\AutorizacaoExecucao\AutorizacaoExecucaoRepository;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoHistoricoAlterarService;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Library\Widget;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class AutorizacaoExecucaoCrudController extends CrudController
{
    use AutorizacaoexecucaoSetupRootTrait;
    use AutorizacaoexecucaoCrudTrait;
    use CommonColumns;
    use DateFormat;

    use UpdateOperation;
    use DeleteOperation;

    /**
     * @var AutorizacaoExecucaoService
     */
    private $autorizacaoExecucaoService;

    public function __construct(AutorizacaoExecucaoService $autorizacaoExecucaoService)
    {
        parent::__construct();
        $this->autorizacaoExecucaoService = $autorizacaoExecucaoService;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->setupRoot();

        $this->contrato = $this->getContrato();

        CRUD::setRoute(config('backpack.base.route_prefix') . "/autorizacaoexecucao/{$this->contrato->id}");

        CRUD::setEntityNameStrings(
            "Ordem de Serviço / Fornecimento",
            "Ordens de Serviço / Fornecimento do Contrato {$this->contrato->numero} - 
                {$this->contrato->unidade->codigo}"
        );
        CRUD::addClause('where', 'autorizacaoexecucoes.contrato_id', '=', $this->contrato->id);
    }

    protected function setupListOperation()
    {
        $this->breadCrumb();
        $this->importarScriptJs(['assets/js/autorizacaoexecucao/functions.js']);

        if (backpack_user()->can('autorizacaoexecucao_inserir')) {
            $this->crud->addButton('top', 'create', 'view', 'crud::buttons.button_criar_autorizacaoexecucao');

            $this->crud->addButton('top', 'create_external', 'view', 'crud::buttons.button_criar_autorizacaoexecucao');
        }

        $this->setupListOperationRoot($this->contrato->id, null);

        $this->autorizacaoExecucaoService->setContrato($this->contrato);
        $this->crud->prepostosResponsaveis = $this->autorizacaoExecucaoService->getPrepostosResponsaveisContrato();

        $codigoSiasgUnidadeOrigem = $this->contrato->unidadeorigem->codigosiasg;
        $numeroContrato = str_replace('/', '', $this->contrato->numero);
        $this->crud->filename = 'OS-F' . $codigoSiasgUnidadeOrigem . $numeroContrato;

        $this->crud->addButtonFromView(
            'line',
            'button_resumo_osf_fiscal',
            'button_resumo_osf_fiscal',
            'beginning'
        );

        $this->importarScriptJs([
            'assets/js/autorizacaoexecucao/quadro_resumo_osf.js'
        ]);

        Widget::add([
            'type'     => 'view',
            'view'     => 'vendor.backpack.base.widgets.modal-quadro-resumo-osf',
            'id' => 'modalquadroresumo',
        ]);

        $this->acoes();
    }

    protected function setupShowOperation()
    {
        $fields = $this->autorizacaoExecucaoService->getFieldLabels(false);
        $this->importarScriptJs(['assets/js/autorizacaoexecucao/functions.js']);

        $this->autorizacaoExecucaoService->setContrato($this->contrato);
        $this->crud->prepostosResponsaveis = $this->autorizacaoExecucaoService->getPrepostosResponsaveisContrato();

        $this->setupShowOperationRoot();

        $this->breadCrumb(true, 'Visualizar');

        $this->acoes();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('show.contentClass', 'col-md-12');
        $this->crud->setUpdateContentClass('show.contentClass', 'col-md-12');

        $this->crud->setValidation(AutorizacaoExecucaoRequest::class);

        $autorizacaoexecucao = request()->id ?
            AutorizacaoExecucao::find(request()->id) :
            new AutorizacaoExecucao();

        $this->autorizacaoexecucao = $autorizacaoexecucao;

        $this->autorizacaoExecucaoService->setContrato($this->contrato);
        $this->autorizacaoExecucaoService->setAutorizacaoExecucao($autorizacaoexecucao);
        $this->fields = $this->autorizacaoExecucaoService->getFieldLabels();

        $this->crud->button_custom = [
            ['button_text' => 'Salvar Rascunho', 'button_id' => 'rascunho',
                'button_name_action' => 'rascunho', 'button_value_action' => '1',
                'button_icon' => 'fas fa-edit', 'button_tipo' => 'secondary'],
        ];

        $this->fields['originado_sistema_externo']['value'] =
            $this->fields['originado_sistema_externo']['value'] ??
            $this->crud->getRequest()->query('external');

        $this->cabecalho();
        $this->campos();

        if ($this->crud->getRequest()->query('external') || $this->fields['originado_sistema_externo']['value']) {
            $this->crud->button_custom[] = ['button_text' => 'Adicionar OS/F', 'button_id' => 'adicionar',
                'button_name_action' => 'rascunho', 'button_value_action' => '0',
                'button_icon' => 'fas fa-save', 'button_tipo' => 'primary'];

            $arquivo = $autorizacaoexecucao->arquivo;

            $this->crud->addField([
                'name' => 'arquivo',
                'label' => 'Arquivo - OS/F assinada',
                'type' => 'upload_custom_generic',
                'wrapperAttributes' => [
                    'class' => 'col-md-12'
                ],
                'required' => true,
                'attributes' => ['id' => 'arquivo'],
                'model' => ArquivoGenerico::class,
                'upload' => true,
                'subfields' => null,
                'colum_filter' => [
                    'id' => $arquivo ? $arquivo->id : null,
                ]
            ])->afterField('informacoes_complementares');
        } else {
            $this->crud->button_custom[] = ['button_text' => 'Enviar para Assinatura', 'button_id' => 'assinatura',
                'button_name_action' => 'enviar_assinatura', 'button_value_action' => '1',
                'button_icon' => 'fas fa-share', 'button_tipo' => 'primary'];

            $codigoItem = CodigoItem::where('descres', 'ae_arquivo_anexo')->first();
            $anexos = $autorizacaoexecucao->anexos()->where('tipo_id', $codigoItem->id)->get();
            $this->crud->addField([
                'name' => 'anexos',
                'label' => 'Anexos - OS/F',
                'type' => 'upload_anexos_autorizacaoexecucao',
                'anexos' => $anexos->toArray(),
            ]);
        }

        $prepostosResonsaveis = $this->autorizacaoExecucaoService->getPrepostosResponsaveisContrato();
        $this->crud->addField([
            'type' => 'modal_select_signatarios',
            'name' => 'select_prepostos_responsaveis[]',
            'responsaveis' => $prepostosResonsaveis['responsaveis'],
            'prepostos' => $prepostosResonsaveis['prepostos']
        ]);

        $this->breadCrumb(true);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

        $this->autorizacaoexecucao->getViewButtons($this->crud);

        $this->crud->setValidation(AutorizacaoExecucaoRequest::class);

        $this->breadCrumb(true, 'Editar');
    }

    public function store(AutorizacaoExecucaoRequest $request)
    {
        try {
            DB::beginTransaction();

            $autorizacaoExecucaoService = new AutorizacaoExecucaoService();

            $autorizacaoExecucaoService->setContrato($this->contrato);
            $autorizacaoExecucaoService->upsert($request->all());

            DB::commit();

            \Alert::add('success', 'Cadastro Realizado com sucesso!')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao cadastrar uma ordem de serviço / fornecimento!')->flash();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
        }

        return redirect('autorizacaoexecucao/' . $this->contrato->id);
    }

    public function update(AutorizacaoExecucaoRequest $request)
    {
        /**
         * update autorizacao de execucao
         */
        try {
            DB::beginTransaction();

            $autorizacaoExecucaoService = new AutorizacaoExecucaoService();

            $autorizacaoExecucao = AutorizacaoExecucao::findOrFail(request()->id);
            $autorizacaoExecucaoService->setContrato($this->contrato);
            $autorizacaoExecucaoService->setAutorizacaoexecucao($autorizacaoExecucao);

            $autorizacaoExecucaoService->upsert($request->all());

            DB::commit();

            \Alert::add('success', 'Atualização realizada com sucesso!')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao atualizar a ordem de serviço / fornecimento!')->flash();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
        }

        return redirect('autorizacaoexecucao/' . $autorizacaoExecucao->contrato_id);
    }

    public function unidades(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');
        $contract_id = $request->contrato_id;


        $subQuery = Contratounidadedescentralizada::select('contratounidadesdescentralizadas.unidade_id')
            ->where('contratounidadesdescentralizadas.contrato_id', $contract_id);

        $query = Unidade::select(
            'unidades.id',
            'unidades.codigo',
            'unidades.nome',
            'unidades.nomeresumido',
        )->leftJoin('contratos', 'unidades.id', '=', 'contratos.unidade_id')
            ->distinct();


        if ($search_term) {
            $results = $query
                ->where(function ($query) use ($search_term) {
                    $query->where('nome', 'LIKE', '%' . strtoupper($search_term) . '%')
                        ->orWhere('nomeresumido', 'LIKE', '%' . strtoupper($search_term) . '%')
                        ->orWhere('codigo', 'LIKE', '%' . strtoupper($search_term) . '%');
                })
                ->where(function ($query) use ($contract_id, $subQuery) {
                    $query->where('contratos.id', $contract_id)
                        ->orWhereIn('unidades.id', $subQuery);
                })
                ->paginate(10);
        } else {
            $results = $query
                ->where('contratos.id', $contract_id)
                ->orWhereIn('unidades.id', $subQuery)
                ->paginate(10);
        }

        $results->append('unidade_codigo_nomeresumido');

        if ($request->input('pluck')) {
            $results->pluck('unidade_codigo_nomeresumido', 'id');
        }

        return $results;
    }

    public function empenhos(Request $request)
    {
        $search_term = $request->input('q');
        $contract_id = $request->contrato_id;

        $options = Empenho::query()
            ->select(
                'empenhos.id',
                'empenhos.numero',
                'empenhos.unidade_id',
                'empenhos.empenhado',
                'empenhos.aliquidar',
                'empenhos.liquidado',
                'empenhos.pago',
                DB::raw("'' AS numero_unidade")
            )->join('contratoempenhos', 'empenhos.id', '=', 'contratoempenhos.empenho_id', 'left')
            ->join('fornecedores', 'fornecedores.id', '=', 'empenhos.fornecedor_id', 'left')
            ->join('unidades', 'unidades.id', '=', 'empenhos.unidade_id', 'left')
            ->where('contratoempenhos.contrato_id', $contract_id)
            ->distinct()
            ->orderBy('empenhos.numero');

        if ($search_term) {
            $results = $options->where('empenhos.numero', 'ILIKE', '%' . $search_term . '%')->paginate(10);
        } else {
            $results = $options->paginate(10);
        }

        if ($request->input('pluck')) {
            $results->pluck('numero_unidade', 'id');
        }
        return $results;
    }

    public function contratoItem(Request $request)
    {
        $contratohistoricoId = $request->get('id');
        $dataVigenciaFim = $request->get('vigencia_fim');
        return response()->json(
            (new AutorizacaoExecucaoRepository())
                ->getContratoItemSaldoHistorico($contratohistoricoId, $dataVigenciaFim)
        );
    }

    public function saldoHistoricoItemPorPeriodo(Request $request)
    {
        $request->validate([
            'id' => [
                'required',
                Rule::exists('contratoitens')->where(function ($query) use ($request) {
                    return $query->where('contrato_id', $request->contrato_id)
                        ->where('id', $request->get('id'));
                })
            ],
            'vigencia_inicio' => 'required|date|before_or_equal:vigencia_fim',
            'vigencia_fim' => 'required|date|after_or_equal:vigencia_inicio',
        ]);

        return response()->json(
            (new AutorizacaoExecucaoRepository())->getSaldoHistoricoItemByPeriodo(
                $request->id,
                $request->vigencia_inicio,
                $request->vigencia_fim
            )
        );
    }

    public function saldoHistoricoItem(Request $request)
    {
        $request->validate([
            'contratohistorico_id' => 'required|exists:contratohistorico,id',
            'saldohistoricoitens_id' => 'required|exists:saldohistoricoitens,id',
        ]);


        return response()->json(
            (new AutorizacaoExecucaoRepository())->getSaldoHistoricoItem(
                $request->get('contratohistorico_id'),
                $request->get('saldohistoricoitens_id')
            )
        );
    }

    public function quantidadeUsada(Request $request, $contrato_id)
    {
        try {
            $itensOs = (new AutorizacaoExecucaoRepository())
                ->getUsedSaldoByItemBySaldoHistoricoItemId(
                    $contrato_id,
                    $request->get('autorizacaoexecucao_id')
                );

            $autorizacaoexecucao = new AutorizacaoExecucao();
            $itensAlteracaoOs = (new AutorizacaoExecucaoHistoricoAlterarService($autorizacaoexecucao))
                ->getSaldoAlteradoDoSaldoHistoricoItensIds($contrato_id, $request->get('autorizacaoexecucao_id'));

            $itens = array_merge($itensOs->toArray(), $itensAlteracaoOs);

            return response()->json($itens);
        } catch (\Exception $e) {
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            return response()->json([]);
        }
    }

    public function itensUtilizados()
    {
        // obsoleto na issue #441
        return response()->json([]);
        try {
            $itens = AutorizacaoexecucaoSaldoUtilizado::where('contrato_id', request()->contrato_id)
                ->get(['contratoitens_id', 'quantidade']);
            return response()->json($itens);
        } catch (\Exception $e) {
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            return response()->json([]);
        }
    }

    public function duplicar()
    {
        $autorizacaoExecucao = AutorizacaoExecucao::findOrFail(request()->autorizacaoexecucao_id);

        abort_unless(
            in_array($autorizacaoExecucao->situacao->descres, ['ae_status_2', 'ae_status_4', 'ae_status_8']),
            403,
            'Esta Ordem de Serviço / Fornecimento não pode ser duplicada'
        );

        try {
            DB::beginTransaction();

            $autorizacaoExecucaoRepository = new AutorizacaoExecucaoRepository();

            $autorizacaoExecucaoRepository->duplicar($autorizacaoExecucao);

            DB::commit();

            \Alert::add('success', 'Duplicação realizada com sucesso!')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao duplicar a ordem de serviço / fornecimento!')->flash();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
        }

        return redirect()->back();
    }

    public function gerarPDF(Request $request)
    {
        $autorizacaoExecucao = AutorizacaoExecucao::findOrFail(request()->autorizacaoexecucao_id);

        abort_unless(
            in_array($autorizacaoExecucao->situacao->descres, ['ae_status_1']),
            403,
            'Não é possivel gerar o PDF desta ordem de serviço / fornecimento!'
        );

        $this->autorizacaoExecucaoService->setAutorizacaoexecucao($autorizacaoExecucao);
        $this->autorizacaoExecucaoService->setContrato($this->getContrato());

        if ($request->type === 'assinatura') {
            return $this->autorizacaoExecucaoService->generatePDFAssinatura($request->assinantes);
        }

        return $this->autorizacaoExecucaoService->generatePDFRelatorio();
    }

    private function configurarMascaraNumeroProcesso($contrato)
    {
        $unidadeConfiguracao = UnidadeConfiguracao::where("unidade_id", $contrato->unidade_id)->first();

        if (!empty($unidadeConfiguracao)) {
            return $unidadeConfiguracao->padrao_processo_mascara;
        } else {
            $orgaoConfiguracao = Orgaoconfiguracao::where("orgao_id", $contrato->unidade->orgao_id)->first();

            if ($orgaoConfiguracao) {
                return $orgaoConfiguracao->padrao_processo_marcara;
            }
        }

        return null;
    }

    public function anexo(Request $request)
    {
        $mimes = 'doc,docx,odt,ods,xls,xlsx,pdf';

        if (!str_contains(session()->get('_previous.url'), 'entrega')) {
            $mimes = 'pdf';
        }

        $request->validate(
            [
                'upload_arquivo_anexo' => "required|file|mimes:$mimes|max:30720",
            ],
            [
                'upload_arquivo_anexo.required' => 'Selecione um arquivo de anexo',
                'upload_arquivo_anexo.file' => 'Selecione um arquivo de anexo',
                'upload_arquivo_anexo.mimes' => 'O arquivo de anexo deve ser do tipo PDF',
                'upload_arquivo_anexo.max' => 'O arquivo de anexo deve ter no maximo 30MB',
            ]
        );

        $file = $request->file('upload_arquivo_anexo');

        return response()->json([
            'nome_arquivo_anexo' => $file->getClientOriginalName(),
            'url_arquivo_anexo' => $file->store('temporario', ['disk' => 'public']),
        ]);
    }

    private function breadCrumb(bool $acao = false, $texto = 'Adicionar')
    {
        $contrato = $this->getContrato();


        $breadcrumb = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
            'Ordens de Serviço / Fornecimento do Contrato ' . $contrato->numero => false,
            'Lista' => false,
        ];

        if ($acao) {
            $breadcrumb = [
                trans('backpack::crud.admin') => backpack_url('dashboard'),
                'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
                'Ordens de Serviço / Fornecimento do Contrato ' . $contrato->numero => false,
                $texto => false,
                // 'Voltar' => backpack_url('autorizacaoexecucao/' . $contrato->id),
            ];
        }

        $this->data['breadcrumbs'] = $breadcrumb;
    }

    public function informacoesQuadroResumo(Request $request)
    {
        return $this->autorizacaoExecucaoService
            ->getQuantidadeTotalAutorizacaoExecucao($request->autorizacao_execucao_id);
    }

    private function acoes(): void
    {
        if (backpack_user()->can('autorizacaoexecucao_inserir')) {
            CRUD::addButtonFromView(
                'line',
                'button_status_historico_autorizacaoexecucao',
                'button_status_historico_autorizacaoexecucao',
                'end'
            );

            CRUD::addButtonFromView(
                'line',
                'button_duplicar_autorizacaoexecucao',
                'button_duplicar_autorizacaoexecucao',
                'end'
            );

            CRUD::addButtonFromView(
                'line',
                'more.osf',
                'more.osf',
                'end'
            );
        }
    }
}
