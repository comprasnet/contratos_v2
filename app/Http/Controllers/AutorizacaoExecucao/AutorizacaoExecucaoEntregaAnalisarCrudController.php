<?php

namespace App\Http\Controllers\AutorizacaoExecucao;

use App\Http\Requests\AutorizacaoExecucaoEntregaAnalisarRequest;
use App\Http\Traits\Autorizacaoexecucao\AutorizacaoExecucaoEntregaSetupRootTrait;
use App\Http\Traits\CommonColumns;
use App\Repositories\Contrato\ContratoParametroRepository;
use App\Repositories\Contrato\ContratoResponsavelRepository;
use App\Services\AutorizacaoExecucao\UsuarioFornecedorAutorizacaoExecucaoEntregaService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class AutorizacaoExecucaoEntregaAnalisarCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AutorizacaoExecucaoEntregaAnalisarCrudController extends CrudController
{
    use AutorizacaoExecucaoEntregaSetupRootTrait;
    use CommonColumns;

    private $usuarioFornecedorAutorizacaoExecucaoEntregaService;

    public function __construct(
        UsuarioFornecedorAutorizacaoExecucaoEntregaService $usuarioFornecedorAutorizacaoExecucaoEntregaService
    ) {
        parent::__construct();
        $this->usuarioFornecedorAutorizacaoExecucaoEntregaService = $usuarioFornecedorAutorizacaoExecucaoEntregaService;
    }


    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->setupRoot();

        CRUD::setRoute(config('backpack.base.route_prefix') . '/autorizacaoexecucao/' . request()->contrato_id . '/' .
            request()->autorizacaoexecucao_id . '/entrega/' . request()->entrega_id . '/analisar');

        CRUD::setSubheading('Analisar', 'create');

        $this->autorizacaoExecucaoEntrega = $this->crud->getEntry(request()->entrega_id);
        $this->exibirTituloPaginaMenu(
            'Entrega',
            'Entrega nº ' . $this->autorizacaoExecucaoEntrega->getFormattedSequencial() .
            ' Ordem de Serviço / Fornecimento ' . $this->autorizacaoexecucao->numero .
            ' do Contrato ' . $this->contrato->numero . ' - ' . $this->contrato->unidade->codigo,
            false
        );

        $contratoResponsavelRepository = new ContratoResponsavelRepository($this->contrato->id);

        abort_if(
            !$contratoResponsavelRepository->isUsuarioResponsavel() ||
            $this->autorizacaoExecucaoEntrega->situacao->descres != 'ae_entrega_status_1',
            403,
            'Sem permissão para analisar esta entrega'
        );
    }

    protected function setupCreateOperation()
    {
        $this->breadCrumb();

        $this->crud->setCreateContentClass('col-md-12');

        $this->setupCreateOperationRoot();

        $this->crud->getEntry(request()->entrega_id);

        $this->crud->addField(
            [
                'name' => 'itens_osf_entregas',
                'type' => 'table_selecionar_entrega_itens',
                'label' => 'itens',
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'itens' => $this->autorizacaoexecucao
                    ->autorizacaoexecucaoItens
                    ->load(['itensEntrega' => function ($query) {
                        $query->where('autorizacao_execucao_entrega_id', \request()->entrega_id);
                    }]),
            ]
        );

        $this->crud->addField([
            'name' => 'separator_2',
            'type' => 'custom_html',
            'value' => '<hr>',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $this->crud->addField([
            'name' => 'informacoes_complementares',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'label' => 'Informação Complementares',
            'value' => $this->autorizacaoExecucaoEntrega->informacoes_complementares,
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $this->crud->addField([
            'name' => 'locais_execucao_entrega',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'label' => 'Locais de Execução para Entrega',
            'value' => $this->autorizacaoExecucaoEntrega->getLocaisExecucao(),
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $this->crud->addField([
            'name' => 'mes_ano_referencia',
            'label' => 'Mês/Ano de Referência',
            'type' => 'label',
            'attributes' => [
                'disabled' => 'disabled',
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-12 mt-1'
            ],
            'required' => true,
            'value' => Carbon::parse($this->autorizacaoExecucaoEntrega->mes_ano_referencia)->format('m/Y'),
        ]);

        $contratoParametroRepository = new ContratoParametroRepository(request()->contrato_id);
        $dataPrazoRecebimentoProvisorio = $contratoParametroRepository->getDataPrazoRecebimentoProvisorio();
        $dataPrazoRecebimentoDefinitivo =  $contratoParametroRepository->getDataPrazoRecebimentoDefinitivo();

        $this->crud->addField([
            'name' => 'data_entrega',
            'label' =>  'Data efetiva da entrega',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'col-md-4 mt-1'
            ],
            'required' => true,
            'value' => $this->autorizacaoExecucaoEntrega->data_entrega ?? date('Y-m-d'),
        ]);

        $this->crud->addField([
            'name' => 'data_prazo_trp',
            'label' =>  'Data prevista para o recebimento provisório',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'col-md-4 mt-1'
            ],
            'required' => true,
            'value' => $this->autorizacaoExecucaoEntrega->data_prazo_trp ?? $dataPrazoRecebimentoProvisorio,
        ]);

        $this->crud->addField([
            'name' => 'data_prazo_trd',
            'label' =>  'Data prevista para recebimento definitivo',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'col-md-4 mt-1'
            ],
            'required' => true,
            'value' => $this->autorizacaoExecucaoEntrega->data_prazo_trd ?? $dataPrazoRecebimentoDefinitivo,
        ]);

        $table = '<label class="mt-4 mb-2">Anexos</label>' . $this->autorizacaoExecucaoEntrega->getAnexosViewLink();
        $this->crud->addField([
            'name' => 'anexos',
            'type' => 'custom_html',
            'value' => $table,
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);
        $this->crud->setOperationSetting('showCancelButton', false);

        $this->crud->addField([
            'name' => 'justificativa_motivo_analise',
            'type' => 'textarea',
            'label' => 'Justificativa/Motivo da Análise',
            'required' => true
        ]);

        $backToPage = config('backpack.base.route_prefix') . '/autorizacaoexecucao/' . request()->contrato_id . '/'
            . request()->autorizacaoexecucao_id . '/entrega';
        $this->crud->button_custom = [
            [
                'button_text' => 'Voltar', 'button_id' => 'voltar_entrega',
                'button_name_action' => 'voltar', 'button_value_action' => '0',
                'button_icon' => 'fas fa-arrow-left', 'button_tipo' => 'secondary',
                'onclick' =>  "bloquearSubmit = true;window.location.href='$backToPage'"
            ],
//            SOLITADO NA ISSUE https://gitlab.com/comprasnet/contratos_v2/-/issues/514 CRITÉRIO C
//            [
//                'button_text' => 'Cancelar', 'button_id' => 'cancelar_entrega',
//                'button_name_action' => 'acao', 'button_value_action' => 'cancelar',
//                'button_icon' => 'la la-ban', 'button_tipo' => 'danger'
//            ],
            [
                'button_text' => 'Recusar', 'button_id' => 'recusar_entrega',
                'button_name_action' => 'acao', 'button_value_action' => 'recusar',
                'button_icon' => 'fas fa-times-circle', 'button_tipo' => 'warning'
            ],
            [
                'button_text' => 'Elaborar TRP', 'button_id' => 'aprovar_entrega',
                'button_name_action' => 'acao', 'button_value_action' => 'aprovar',
                'button_icon' => 'fas fa-save', 'button_tipo' => 'primary'
            ]
        ];
    }

    public function store(AutorizacaoExecucaoEntregaAnalisarRequest $request)
    {
        try {
            DB::beginTransaction();

            $this->usuarioFornecedorAutorizacaoExecucaoEntregaService->salvarAnaliseEntrega(
                $request->validated(),
                request()->entrega_id
            );

            DB::commit();

            \Alert::add('success', 'Entrega analisada com sucesso!')->flash();
        } catch (Exception $e) {
            DB::rollBack();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            \Alert::add('error', 'Erro ao analisar a entrega!')->flash();
            return redirect()->back()->withInput();
        }

        if ($request->acao === "aprovar") {
            return redirect('autorizacaoexecucao/' .
                $this->contrato->id . '/' .
                $this->autorizacaoexecucao->id . '/entrega/trp/' .
                request()->entrega_id . '/edit');
        }

        return redirect('autorizacaoexecucao/' . $this->contrato->id . '/' .
            $this->autorizacaoexecucao->id . '/entrega');
    }

    private function breadCrumb()
    {
        $linkVoltar = config('backpack.base.route_prefix') . '/autorizacaoexecucao/' . request()->contrato_id . '/' .
            request()->autorizacaoexecucao_id . '/entrega';
        $this->data['breadcrumbs'] = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
            'Ordens de Serviço / Fornecimento do Contrato ' . $this->contrato->numero =>
                backpack_url('autorizacaoexecucao/' . $this->contrato->id),
            'Entregas Ordem de Serviço / Fornecimento' => $linkVoltar,
            "Analisar Entrega" => false,
            // 'Voltar' => $linkVoltar,
        ];
    }
}
