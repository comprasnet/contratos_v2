<?php

namespace App\Http\Controllers\AutorizacaoExecucao;

use App\Http\Requests\AutorizacaoExecucaoRetificarRequest;
use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoHistoricoCrudTrait;
use App\Models\ArquivoGenerico;
use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoexecucaoHistorico;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoHistoricoRetificarService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\DB;

/**
 * Class AutorizacaoExecucaoRetificarCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AutorizacaoExecucaoRetificarCrudController extends CrudController
{
    use AutorizacaoexecucaoHistoricoCrudTrait;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->settingsSetup('retificar');

        $this->bloquearBotaoPadrao($this->crud, ['update', 'delete']);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->text_button_redirect_create = 'retificação';

        $this->cabecalho();

        $this->addColumnModelFunction('data_retificacao', 'Data da retificação', 'getDataRetificacao');
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
        $this->breadCrumb(true);
    }

    protected function setupShowOperation()
    {
        $autorizacaoExecucaoHistorico = AutorizacaoexecucaoHistorico::findOrFail(request()->id);

        $this->addColumnUsuarioResponsavel();

        if (count($autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('processo'))) {
            $this->addColumnTable(
                'processo',
                $this->fields['processo']['label'],
                $autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('processo')
            );
        }


        if (count($autorizacaoExecucaoHistorico->getAntesDepoisRelationshipColumnTable('tipo', 'descricao'))) {
            $this->addColumnTable(
                'tipo_id',
                $this->fields['tipo_id']['label'],
                $autorizacaoExecucaoHistorico->getAntesDepoisRelationshipColumnTable('tipo', 'descricao')
            );
        }


        if (count($autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('numero'))) {
            $this->addColumnTable(
                'numero',
                $this->fields['numero']['label'],
                $autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('numero')
            );
        }


        if (count($autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('data_assinatura'))) {
            $this->addColumnTable(
                'data_assinatura',
                $this->fields['data_assinatura']['label'],
                $autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('data_assinatura')
            );
        }


        if (count($autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('data_vigencia_inicio'))) {
            $this->addColumnTable(
                'data_vigencia_inicio',
                $this->fields['data_vigencia_inicio']['label'],
                $autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('data_vigencia_inicio')
            );
        }


        if (count($autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('data_vigencia_fim'))) {
            $this->addColumnTable(
                'data_vigencia_fim',
                $this->fields['data_vigencia_fim']['label'],
                $autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('data_vigencia_fim')
            );
        }


        if (count($autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('numero_sistema_origem'))) {
            $this->addColumnTable(
                'numero_sistema_origem',
                $this->fields['numero_sistema_origem']['label'],
                $autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('numero_sistema_origem')
            );
        }

        if (count($autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('url_arquivo'))) {
            $this->addColumnTable(
                'url_arquivo',
                'Arquivo - OS/F assinada',
                [
                    'Antes' => [$autorizacaoExecucaoHistorico->getViewLink('url_arquivo_antes')],
                    'Depois' => [$autorizacaoExecucaoHistorico->getViewLink('url_arquivo_depois')],
                ],
            );
        }

        $unidadesRequisitantesAntes = $autorizacaoExecucaoHistorico
            ->autorizacaoexecucaoUnidadeRequisitantesHistorico()
            ->where('tipo_historico', 'antes')
            ->get(['codigosiasg', 'nomeresumido'])
            ->map(function ($unidade) {
                return $unidade->codigosiasg . ' - ' . $unidade->nomeresumido;
            })
            ->reduce(function ($carry, $item) {
                if (!$carry) {
                    return $item . '<br />';
                }
                return $carry . '<br/>' . $item;
            });


        $unidadesRequisitantesDepois = $autorizacaoExecucaoHistorico
            ->autorizacaoexecucaoUnidadeRequisitantesHistorico()
            ->where('tipo_historico', 'depois')
            ->get(['codigosiasg', 'nomeresumido'])
            ->map(function ($unidade) {
                return $unidade->codigosiasg . ' - ' . $unidade->nomeresumido;
            })
            ->reduce(function ($carry, $item) {
                if (!$carry) {
                    return $item . '<br />';
                }
                return $carry . '<br/>' . $item;
            });

        $antesDepoisColumnTable = [];
        if (!empty($unidadesRequisitantesAntes) || !empty($unidadesRequisitantesDepois)) {
            $antesDepoisColumnTable = [
                'Antes' => [$unidadesRequisitantesAntes],
                'Depois' => [$unidadesRequisitantesDepois],
            ];
        }

        if (count($antesDepoisColumnTable)) {
            $this->addColumnTable(
                'unidaderequisitante_ids',
                $this->fields['unidaderequisitante_ids']['label'],
                $antesDepoisColumnTable
            );
        }

        $empenhosAntes = $autorizacaoExecucaoHistorico
            ->autorizacaoexecucaoEmpenhosHistorico()
            ->where('tipo_historico', 'antes')
            ->get(['numero', 'unidade_id'])
            ->map(function ($empenho) {
                $unidade = $empenho->unidade;
                if ($unidade) {
                    return $empenho->numero . ' - UG:' . $unidade->codigo . ' - ' . $unidade->nomeresumido;
                }
                return $empenho->numero;
            })
            ->reduce(function ($carry, $item) {
                if (!$carry) {
                    return $item . '<br />';
                }
                return $carry . '<br/>' . $item;
            });

        $empenhosDepois = $autorizacaoExecucaoHistorico
            ->autorizacaoexecucaoEmpenhosHistorico()
            ->where('tipo_historico', 'depois')
            ->get(['numero', 'unidade_id'])
            ->map(function ($empenho) {
                $unidade = $empenho->unidade;
                if ($unidade) {
                    return $empenho->numero . ' - UG:' . $unidade->codigo . ' - ' . $unidade->nomeresumido;
                }
                return $empenho->numero;
            })
            ->reduce(function ($carry, $item) {
                if (!$carry) {
                    return $item . '<br />';
                }
                return $carry . '<br/>' . $item;
            });


        $antesDepoisColumnTable = [];
        if (!empty($empenhosAntes) || !empty($empenhosDepois)) {
            $antesDepoisColumnTable = [
                'Antes' => [$empenhosAntes],
                'Depois' => [$empenhosDepois],
            ];
        }

        if ($antesDepoisColumnTable) {
            $this->addColumnTable(
                'empenhos',
                $this->fields['empenhos']['label'],
                $antesDepoisColumnTable
            );
        }

        $locaisExecucaoAntes = $autorizacaoExecucaoHistorico
            ->autorizacaoexecucaoLocaisExecucaoHistorico()
            ->where('tipo_historico', 'antes')
            ->get(['descricao'])
            ->map(function ($local) {
                return $local->descricao;
            })
            ->reduce(function ($carry, $item) {
                if (!$carry) {
                    return $item . '<br />';
                }
                return $carry . '<br/>' . $item;
            });


        $locaisExecucaoDepois = $autorizacaoExecucaoHistorico
            ->autorizacaoexecucaoLocaisExecucaoHistorico()
            ->where('tipo_historico', 'depois')
            ->get(['descricao'])
            ->map(function ($local) {
                return $local->descricao;
            })
            ->reduce(function ($carry, $item) {
                if (!$carry) {
                    return $item . '<br />';
                }
                return $carry . '<br/>' . $item;
            });

        $antesDepoisColumnTable = [];
        if (!empty($locaisExecucaoAntes) || !empty($locaisExecucaoDepois)) {
            $antesDepoisColumnTable = [
                'Antes' => [$locaisExecucaoAntes],
                'Depois' => [$locaisExecucaoDepois],
            ];
        }

        if (count($antesDepoisColumnTable)) {
            $this->addColumnTable(
                'contrato_local_execucao_ids',
                $this->fields['locais_execucao']['label'],
                $antesDepoisColumnTable
            );
        }

        if (count($autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('informacoes_complementares'))) {
            $this->addColumnTable(
                'informacoes_complementares',
                $this->fields['informacoes_complementares']['label'],
                $autorizacaoExecucaoHistorico->getAntesDepoisColumnTable('informacoes_complementares')
            );
        }

        $this->crud->addColumn([
            'name' => 'justificativa_motivo',
            'escaped' => false
        ]);

        if (!empty($autorizacaoExecucaoHistorico->getItensAntes()) ||
            !empty($autorizacaoExecucaoHistorico->getItensDepois())) {
            $this->addColumnItensAntesDepois();
        }
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');
        CRUD::setModel(AutorizacaoExecucao::class);
        $this->crud->getEntry(request()->autorizacaoexecucao_id);
        CRUD::setValidation(AutorizacaoExecucaoRetificarRequest::class);

        $this->cabecalho();
        $this->campos();

        $this->crud->addField([
            'name' => 'justificativa_motivo',
            'type' => 'textarea',
            'label' => 'Justificativa/Motivo da retificação',
            'required' => true
        ])->afterField('empenhos');

        $arquivo = $this->crud->entry->arquivo;

        $this->crud->addField([
            'name' => 'arquivo',
            'label' => 'Arquivo - OS/F assinada',
            'type' => 'upload_custom_generic',
            'wrapperAttributes' => [
                'class' => 'col-md-12'
            ],
            'required' => true,
            'attributes' => ['id' => 'arquivo'],
            'model' => ArquivoGenerico::class,
            'upload' => true,
            'subfields' => null,
            'entry' => $this->crud->entry,
            'colum_filter' => [
                'id' => $arquivo ? $arquivo->id : null,
            ]
        ])->afterField('informacoes_complementares');

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Retificar',
        ]);

        $this->breadCrumb();
    }

    public function store(AutorizacaoExecucaoRetificarRequest $request)
    {
        $autorizacaoexecucaoHistoricoRetificar = new AutorizacaoExecucaoHistoricoRetificarService(
            $this->autorizacaoexecucao
        );
        $camposRetificados = $autorizacaoexecucaoHistoricoRetificar->getCamposRetificados($request->validated());
        if (count($camposRetificados) === 0) {
            \Alert::add('error', 'Para retificar a OS/F é necessário alterar pelo menos um campo!')->flash();
            return redirect()->back();
        }

        DB::beginTransaction();

        try {
            $autorizacaoexecucaoHistoricoRetificar->create($camposRetificados, $request->justificativa_motivo);

            \Alert::add('success', 'Cadastro Realizado com sucesso!')->flash();

            DB::commit();
            return redirect('autorizacaoexecucao/' .
                request()->contrato_id . '/' . request()->autorizacaoexecucao_id . '/retificar');
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao realizar a retificação!')->flash();
            $this->inserirLogCustomizado('autorizacao_execucao', 'error', $e);
            return redirect()->back();
        }
    }
}
