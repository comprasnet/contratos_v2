<?php

namespace App\Http\Controllers\Contrato;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContratoLocalExecucaoRequest;
use App\Http\Traits\LogTrait;
use App\Services\Contrato\ContratoLocalExecucaoService;
use Illuminate\Support\Facades\DB;

class ContratoLocalExecucaoCrudController extends Controller
{
    use LogTrait;

    public function store(ContratoLocalExecucaoRequest $request)
    {
        DB::beginTransaction();
        try {
            $contratoLocalExecucaoService = new ContratoLocalExecucaoService();

            $contratoLocalExecucao = $contratoLocalExecucaoService->create(
                $request->validated(),
                $request->contrato_id
            );

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->inserirLogCustomizado('contrato_local_execucao', 'error', $e);
            return response('Ocorreu um erro interno', 500);
        }

        return response($contratoLocalExecucao, 200);
    }
}
