<?php

namespace App\Http\Controllers\Contrato;

use App\Http\Requests\ContratoPrepostoRequest;
use App\Http\Requests\UsuarioFornecedorContratoPrepostoRequest;
use App\Http\Requests\UsuarioFornecedorContratoPrepostoUpdateRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Models\CodigoItem;
use App\Models\Contrato;
use App\Models\Contratoarquivo;
use App\Models\Contratopreposto;
use App\Services\UsuarioFornecedor\UsuarioFornecedorContratoPrepostoService;
use App\Repositories\Contrato\ContratoResponsavelRepository;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/**
 * Class ContratoPrepostoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ContratoPrepostoCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;

    private $contrato;
    protected $prepostoService;

    public function __construct(UsuarioFornecedorContratoPrepostoService $prepostoService)
    {
        parent::__construct();
        $this->prepostoService = $prepostoService;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(Contratopreposto::class);
        CRUD::addClause('where', 'contrato_id', '=', request()->contrato_id);

        CRUD::setRoute(config('backpack.base.route_prefix') . '/contrato/' . request()->contrato_id . '/prepostos');
        CRUD::setSubheading('Criar', 'create');
        CRUD::setSubheading('Editar', 'edit');
        CRUD::setSubheading('Visualizar', 'index');

        $this->contrato = Contrato::findOrFail(request()->contrato_id);

        $responsavelRepository =new ContratoResponsavelRepository(request()->contrato_id);
        abort_if(
            !$responsavelRepository->isUsuarioFiscalContrato() && !$responsavelRepository->isUsuarioGestorContrato(),
            403,
            'Sem permissão para editar prepostos'
        );


        CRUD::setEntityNameStrings(
            "Prepostos",
            "Prepostos do Contrato {$this->contrato->numero}"
        );

        $this->crud->denyAccess('delete');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->breadCrumb();

        $this->crud->text_button_redirect_create = 'Preposto';

        if (config('app.app_amb') != 'Ambiente Produção') {
            $this->crud->addColumn([
                'name' => 'status_indicacao',
                'label' => 'Status Indicação',
                'type' => 'model_function',
                'function_name' => 'getStatusIndicacaoDescricaoCores',
                'limit' => 9999,
                'priority' => 1
            ]);
        }

        $this->crud->addColumn([
            'name' => 'cpf',
            'type' => 'text',
            'label' => 'CPF',
        ]);

        $this->crud->addColumn([
            'name' => 'nome',
            'type' => 'text',
            'label' => 'Nome',
        ]);

        $this->crud->addColumn([
            'name' => 'email',
            'type' => 'text',
            'label' => 'E-mail',
        ]);

        $this->crud->addColumn([
            'name' => 'telefonefixo',
            'type' => 'text',
            'label' => 'Telefone Fixo',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'celular',
            'type' => 'text',
            'label' => 'Celular',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'doc_formalizacao',
            'type' => 'text',
            'label' => 'Doc. Formalização',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'informacao_complementar',
            'type' => 'text',
            'label' => 'Informação Complementar',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'data_inicio',
            'type' => 'date',
            'label' => 'Data início',
        ]);

        $this->crud->addColumn([
            'name' => 'data_fim',
            'type' => 'date',
            'label' => 'Data início',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'situacao',
            'type' => 'boolean',
            'label' => 'Situação',
            'options' => [0 => 'Inativo', 1 => 'Ativo']
        ]);

        if (config('app.app_amb') != 'Ambiente Produção') {
            $this->crud->addButtonFromView(
                'line',
                'button_aceitar_indicacao_preposto',
                'button_aceitar_indicacao_preposto',
                'end'
            );

            $this->crud->addButtonFromView(
                'line',
                'button_recusar_indicacao_preposto',
                'button_recusar_indicacao_preposto',
                'end'
            );
        }

        $this->crud->enableExportButtons();
    }

    public function setupShowOperation()
    {
        $this->breadCrumb(true);

        $this->crud->addColumn([
            'name' => 'cpf',
            'type' => 'text',
            'label' => 'CPF',
        ]);

        $this->crud->addColumn([
            'name' => 'nome',
            'type' => 'text',
            'label' => 'Nome',
        ]);

        $this->crud->addColumn([
            'name' => 'email',
            'type' => 'text',
            'label' => 'E-mail',
        ]);

        $this->crud->addColumn([
            'name' => 'telefonefixo',
            'type' => 'text',
            'label' => 'Telefone Fixo',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'celular',
            'type' => 'text',
            'label' => 'Celular',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'doc_formalizacao',
            'type' => 'text',
            'label' => 'Doc. Formalização',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'informacao_complementar',
            'type' => 'text',
            'label' => 'Informação Complementar',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'data_inicio',
            'type' => 'date',
            'label' => 'Data início',
        ]);

        $this->crud->addColumn([
            'name' => 'data_fim',
            'type' => 'date',
            'label' => 'Data início',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'situacao',
            'type' => 'boolean',
            'label' => 'Situação',
            'options' => [0 => 'Inativo', 1 => 'Ativo']
        ]);

        $this->crud->addColumn([
            'name' => 'status_indicacao_descricao',
            'label' => 'Status Indicação',
            'type' => 'text'
        ]);

        // Montar as informações sobre o anexo
        $anexo = $this->crud->model->find($this->crud->getCurrentEntryId());
        $arraySaida = [];
        // Se existir o anexo, exibe para o usuário
        if (isset($anexo->arquivo)) {
            $arraySaida['Nome'][0] = $anexo->arquivo->nome;
            $arraySaida['Descrição'][0] = $anexo->arquivo->descricao;
            $arraySaida['Visualizar'][0] =
                '<a target="_blank" href="' . url("storage/{$anexo->arquivo->arquivos}") . '">
                <i class="fas fa-eye"></i>
            </a>';
        }

        $this->addColumnTable('anexo_alteracao', 'Anexo', $arraySaida);

        if (config('app.app_amb') != 'Ambiente Produção') {
            $this->crud->addButtonFromView(
                'line',
                'button_aceitar_indicacao_preposto',
                'button_aceitar_indicacao_preposto',
                'end'
            );
            $this->crud->addButtonFromView(
                'line',
                'button_recusar_indicacao_preposto',
                'button_recusar_indicacao_preposto',
                'end'
            );
        }
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->breadCrumb(true);

        $this->crud->setCreateContentClass('col-md-12');
        CRUD::setValidation(ContratoPrepostoRequest::class);

        $this->crud->addField([
            'name' => 'title_dados_preposto',
            'type' => 'custom_html',
            'value' => '<h2 class="h4 p-0 my-2">Dados Preposto</h2>',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $this->crud->addField([
            'name' => 'contrato_id',
            'label' => 'Contrato ID',
            'type' => 'hidden',
            'value' => request()->contrato_id
        ]);

        $this->crud->addField([
            'name' => 'cpf',
            'label' => 'CPF',
            'type' => 'cpf',
            'wrapperAttributes' => [
                'class' => 'col-md-4 mt-1'
            ],
            'required' => true,
        ]);

        $this->crud->addField([
            'name' => 'nome',
            'label' => 'Nome Completo',
            'type' => 'text',
            'attributes' => [
                'onkeyup' => 'maiuscula(this)'
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-8 mt-1'
            ],
            'required' => true,
        ]);

        $this->crud->addField([
            'name' => 'email',
            'label' => 'E-mail',
            'type' => 'text',
            'attributes' => [
                'onkeyup' => 'minusculo(this)'
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-4 mt-1'
            ],
            'required' => true,
        ]);

        $this->crud->addField([
            'name' => 'telefonefixo',
            'label' => 'Telefone Fixo',
            'type' => 'telefone',
            'wrapperAttributes' => [
                'class' => 'col-md-4 mt-1'
            ]
        ]);

        $this->crud->addField([
            'name' => 'celular',
            'label' => 'Celular',
            'type' => 'celular',
            'wrapperAttributes' => [
                'class' => 'col-md-4 mt-1'
            ]
        ]);

        $this->crud->addField([
            'name' => 'separator',
            'type' => 'custom_html',
            'value' => '<hr>',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $this->crud->addField([
            'name' => 'title_outros',
            'type' => 'custom_html',
            'value' => '<h2 class="h4 p-0 my-2">Outras Informações</h2>',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $this->crud->addField([
            'name' => 'doc_formalizacao',
            'type' => 'text',
            'label' => 'Doc. Formalização',
            'wrapper' => [
                'class' => 'form-group col-md-3',
            ],
        ]);

        $this->crud->addField([
            'name' => 'data_inicio',
            'type' => 'date',
            'label' => 'Data início',
            'wrapper' => [
                'class' => 'form-group col-md-3',
            ],
            'required' => true,
        ]);

        $this->crud->addField([
            'name' => 'data_fim',
            'type' => 'date',
            'label' => 'Data fim',
            'wrapper' => [
                'class' => 'form-group col-md-3',
            ],
        ]);

        $this->crud->addField([
            'name' => 'situacao',
            'type' => 'select2_from_array',
            'label' => 'Situação',
            'options' => [ 1 => 'Ativo', 0 => 'Inativo'],
            'default' => 0,
            'wrapper' => [
                'class' => 'form-group col-md-3',
            ],
        ]);

        $this->crud->addField([
            'name' => 'informacao_complementar',
            'type' => 'textarea',
            'label' => 'Informação Complementar',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $this->crud->addField([
            'name' => 'arquivos',
            'label' => 'Anexo',
            'type'  => 'upload_custom_generic',
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3'
            ],
            'accept' => 'pdf,doc,docx,xls,xlsx,jpg,jpeg,png,ppt,pptx',
            'nome_exibir' => 'descricao',
            'url_arquivo' => 'arquivos',
            'attributes' => ['id' => 'arquivos'],
            'model' => Contratoarquivo::class,
            'upload'    => true,
            'colum_filter' => ['contratopreposto_id' => Route::current()->parameters()['id'] ?? null]
        ]);


        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Adicionar Preposto',
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->breadCrumb(true, 'Editar');

        $this->crud->setUpdateContentClass('col-md-12');
        $this->setupCreateOperation();

        $this->crud->modifyField('cpf', [
            'attributes' => [
                'readonly' => 'readonly',
            ],
        ]);

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Atualizar Preposto',
        ]);
    }

    public function breadCrumb(bool $acao = false, $texto = 'Adicionar'): void
    {
        $breadcrumb = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
            'Prepostos do Contrato ' . $this->contrato->numero => false,
            'Lista' => false,
        ];

        if ($acao) {
            $breadcrumb = [
                trans('backpack::crud.admin') => backpack_url('dashboard'),
                'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
                'Prepostos do Contrato ' . $this->contrato->numero =>
                    backpack_url('contrato/' . request()->contrato_id . '/prepostos'),
                $texto => false,
                'Voltar' => backpack_url('contrato/' . request()->contrato_id . '/prepostos'),
            ];
        }

        $this->data['breadcrumbs'] = $breadcrumb;
    }

    public function aceitarIndicacaoPreposto(int $contratoId, int $contratoPrepostoId, string $statusIndicacaoDescres)
    {
        try {
            DB::beginTransaction();

            $this->prepostoService->statusAceitacaoIndicacaoPreposto(
                $contratoPrepostoId,
                $statusIndicacaoDescres,
                $contratoId
            );

            DB::commit();
            \Alert::add('success', 'Indicação do preposto atualizada com sucesso!')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error('Erro ao atualizar indicação do preposto: ' . $e->getMessage());
            \Alert::add('error', 'Erro ao atualizar a indicação do preposto.')->flash();
        }

        return redirect('contrato/' . $contratoId . '/prepostos');
    }

    public function store(UsuarioFornecedorContratoPrepostoRequest $request)
    {
        try {
            DB::beginTransaction();

            $dados = $request->validated();
            $dados['usuarioInterno'] = true;

            $contratoPreposto = $this->prepostoService->salvarAtualizarPreposto($dados);

            if ($request->hasFile('arquivos')) {
                $this->prepostoService->salvarArquivo(
                    $request->file('arquivos'),
                    $request->contrato_id,
                    $contratoPreposto->id
                );
            }

            DB::commit();

            \Alert::add('success', 'Preposto indicado com sucesso!')->flash();
        } catch (\Exception $e) {
            \Log::error($e);
            DB::rollBack();

            \Alert::add('error', 'Erro ao indicar preposto!')->flash();
            return redirect()->back()->withInput();
        }

        return redirect('contrato/' . request()->contrato_id . '/prepostos');
    }

    public function update(UsuarioFornecedorContratoPrepostoUpdateRequest $request)
    {
        try {
            DB::beginTransaction();

            $dados = $request->validated();
            $dados['usuarioInterno'] = true;

            $this->prepostoService->salvarAtualizarPreposto(
                $dados,
                true,
                $this->crud->getCurrentEntryId()
            );

            if (isset($request->arquivos_clear)) {
                $this->removerArquivoUpload(Contratoarquivo::class, $request->arquivos_clear, true);
            }

            if ($request->hasFile('arquivos')) {
                $this->prepostoService->salvarArquivo(
                    $request->file('arquivos'),
                    $request->contrato_id,
                    $this->crud->getCurrentEntryId()
                );
            }

            DB::commit();

            \Alert::add('success', 'Preposto atualizado com sucesso!')->flash();
        } catch (\Exception $e) {
            \Log::error($e);
            DB::rollBack();

            \Alert::add('error', 'Erro ao atualizar preposto!')->flash();
            return redirect()->back()->withInput();
        }

        return redirect('contrato/' . request()->contrato_id . '/prepostos');
    }
}
