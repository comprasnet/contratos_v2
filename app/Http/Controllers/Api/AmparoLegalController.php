<?php

namespace App\Http\Controllers\Api;

use App\Models\AmparoLegal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AmparoLegalController extends Controller
{

    public function amparolegalfiltercontratos(Request $request)
    {
        $search_term = $request->input('q');

        $options = AmparoLegal::select([
            DB::raw("amparo_legal.ato_normativo as campo_api_amparo")
        ])->groupby(
            'amparo_legal.ato_normativo'
        );

        if ($search_term) {
            $options = $options->where(function ($query) use ($search_term) {
                $query->where('amparo_legal.ato_normativo', 'ilike', '%' . strtoupper($search_term) . '%');
            })->where('codigo', '<>', '');
        }

        return $options->paginate(10);
    }
}
