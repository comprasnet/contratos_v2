<?php

namespace App\Http\Controllers\Api;

use App\Models\Unidade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class UnidadeController extends Controller
{
    public function unidadesfiltercontratos(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        $query = Unidade::select([
            'id',
            \DB::raw("codigo || ' - ' || nomeresumido as descricao")
        ]);

        if ($search_term) {
            $query->where(function ($q) use ($search_term) {
                $q->where('codigo', 'LIKE', '%' . strtoupper($search_term) . '%')
                    ->orWhere('nome', 'LIKE', '%' . strtoupper($search_term) . '%')
                    ->orWhere('nomeresumido', 'LIKE', '%' . strtoupper($search_term) . '%');
            });
        }

        $results = $query->paginate(10);

        return $results;
    }
}
