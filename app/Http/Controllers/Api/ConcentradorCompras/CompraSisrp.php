<?php

namespace App\Http\Controllers\Api\ConcentradorCompras;

use App\Api\Externa\ApiSiasg;
use App\Http\Traits\AmparoTrait;
use App\Http\Traits\Compra\CompraItemTrait;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\LogTrait;
use App\Jobs\CarregarUnidadeParticipanteCompraGestaoAtaJOB;
use App\Jobs\CarregarUnidadeParticipanteContratacaoNovoDCGestaoAtaJOB;
use App\Models\CodigoItem;
use App\Models\CompraItem;
use App\Models\Compras;
use App\Models\JobUser;
use App\Repositories\CodigoItemRepository;
use App\Repositories\Compra\CompraItemRepository;
use App\Repositories\Compra\CompraItemUnidadeRepository;
use App\Repositories\UnidadeRepository;
use App\Services\NovoDivulgacaoCompra\CompraItemUnidadeLocalEntregaService;
use App\Services\NovoDivulgacaoCompra\EnderecoService;
use App\Services\NovoDivulgacaoCompra\NovoDivulgacaoCompraService;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CompraSisrp extends NovoDivulgacaoCompraService
{
    use CompraTrait, DispatchesJobs, CompraItemTrait;
    use AmparoTrait;
    use LogTrait;
    
    private $origemConsulta = null;
    
    /**
     * @param string $numeroAnoCompra
     * @param int $idUasgGerenciadora
     * @param int $modalidadeCompra
     * @param int|null $idUasgUsuarioLogado
     * @param int|null $idUasgOrigemCompra
     * @return array|JsonResponse|mixed|object|\stdClass|null
     * @throws GuzzleException
     */
    public function getCompraLeiQuatozeUmTresTres(
        string $numeroAnoCompra,
        int $idUasgGerenciadora,
        int $modalidadeCompra,
        ?int $idUasgUsuarioLogado = null,
        ?int $idUasgOrigemCompra = null
    ) {

        # Validar se o número e ano da compra contem a barra do ano
        if (!str_contains($numeroAnoCompra, '/')) {
            return $this->mountObjectReturn(202, 'Número da Compra/Ano inválido');
        }

        # Validar se o ano da compra está correto
        if (str_contains($numeroAnoCompra, '/')) {
            $anoCompra = explode('/', $numeroAnoCompra)[1];
            # Se o ano da compra não for digitado com quatro dígitos
            if (strlen($anoCompra) < 4) {
                return $this->mountObjectReturn(202, 'Ano da compra inválido');
            }
        }

        $dataApiCompra = $this->getCompraPosteriorNovoDivulgacao(
            $numeroAnoCompra,
            $idUasgGerenciadora,
            $modalidadeCompra,
            $idUasgOrigemCompra
        );

        # Se não encontrar a compra a partir do ano de 2023
        if (empty($dataApiCompra)) {
            $dataApiCompra = $this->getCompraAnteriorNovoDivulgacao(
                $numeroAnoCompra,
                $idUasgGerenciadora,
                $modalidadeCompra,
                $idUasgOrigemCompra
            );
        }
        
        $arrayErroAPI = [404, 0, 422];
        if (in_array($dataApiCompra->codigoRetorno, $arrayErroAPI)) {
            return $this->mountObjectReturn(202, $dataApiCompra->messagem);
        }
        
        if (empty($dataApiCompra->data)) {
            return $dataApiCompra;
        }

        $amparosLegaisDerivadas14133 = $this->getDerivadas14133Formatado();

        if ($dataApiCompra->origemConsulta === 'base_de_dados') {
            $modelo = Compras::make($dataApiCompra->data);

            if ($modelo->tipoCompra->descricao === 'SISRP' && !in_array($modelo->lei, $amparosLegaisDerivadas14133)) {
                return $this->mountObjectReturn(202, "Compra não pode ser da lei {$modelo->lei}");
            }
            
            if ($modelo->tipoCompra->descricao === 'SISPP') {
                return $this->mountObjectReturn(202, "A compra informada não é do tipo SRP");
            }
            
            # Se o usuário tem a permissão para continuar na compra informada
            $unidade_autorizada_id = $this->verificarPermissaoUasgCompraSisrp(
                $modelo->unidadeSubrrogada->codigo,
                $idUasgGerenciadora
            );

            if (is_null($unidade_autorizada_id)) {
                $dataApiCompra = $this->mountObjectReturn(
                    202,
                    "Você não tem permissão para realizar ata para esta unidade!"
                );
            }

            return $dataApiCompra;
        }

        # Se a compra for da LEI 14.133 mas não for do tipo SISRP
        if (empty($dataApiCompra->data->linkSisrpCompleto)) {
            return $this->mountObjectReturn(202, "A compra informada não é do tipo SRP");
        }

        $dadosCompra = new Request();
        $dadosCompra->merge(['unidade_origem_id' => $idUasgOrigemCompra]);

        # Se o usuário tem a permissão para continuar na compra informada
        $unidade_autorizada_id = $this->verificaPermissaoUasgCompra($dataApiCompra, $dadosCompra, $idUasgUsuarioLogado);

        if (is_null($unidade_autorizada_id)) {
            $retornoErro = $this->montarRespostaAlertJs(
                202,
                "Você não tem permissão para realizar ata para esta unidade!",
                "warning"
            );
            return response()->json($retornoErro, $retornoErro->code);
        }

        # Se a compra informada for diferente da LEI 14.133
        if (!in_array($dataApiCompra->data->compraSispp->lei, $amparosLegaisDerivadas14133)) {
            return $this->mountObjectReturn(
                202,
                "Compra não pode ser da lei {$dataApiCompra->data->compraSispp->lei}"
            );
        }
        
        $codigoItemRepository = new CodigoItemRepository();
        $dadosModalidade = $codigoItemRepository->getCodigoItemPorId($modalidadeCompra);


        if ($dataApiCompra->origemConsulta === 'api_novo_divulgacao' &&
            $dataApiCompra->data->compraSispp->codigoModalidade != $dadosModalidade->descres) {
            return $this->mountObjectReturn(202, "Compra não encontrada");
        }


        # Se o usuário tem a permissão para continuar na compra informada
        $unidade_autorizada_id = $this->verificaPermissaoUasgCompraNovo(
            $dataApiCompra,
            $idUasgOrigemCompra,
            $idUasgUsuarioLogado
        );
        
        if (is_null($unidade_autorizada_id)) {
            return $this->mountObjectReturn(
                202,
                "Você não tem permissão para realizar ata para esta unidade!"
            );
        }

        return $dataApiCompra;
    }

    /**
     *
     *  Método responsável em buscar a compra na sequência
     *  1 - API SIASG
     *  2 - Base de dados
     *
     * @param string $numeroAnoCompra
     * @param int $idUasgGerenciadora
     * @param int $modalidadeCompra
     * @param int|null $idUasgOrigemCompra
     * @return mixed|object|\stdClass|null
     */
    private function getCompraAnteriorNovoDivulgacao(
        string $numeroAnoCompra,
        int $idUasgGerenciadora,
        int $modalidadeCompra,
        ?int $idUasgOrigemCompra = null
    ) {
        $dataApiCompra = null;
        $arrayNumeroCompra = explode('/', $numeroAnoCompra);
        $anoCompra = $arrayNumeroCompra[1];

        if ($anoCompra < 2023) {
            # Buscar a compra pela API do SIASG
            $dataApiCompra = $this->getCompraAPISiasg(
                $numeroAnoCompra,
                $idUasgGerenciadora,
                $modalidadeCompra,
                $idUasgOrigemCompra
            );

            # Se não encontrar a compra na API do SIASG
            if ($dataApiCompra->codigoRetorno == 204) {
                $dataApiCompra = $this->getDataCompraBase($idUasgOrigemCompra, $modalidadeCompra, $numeroAnoCompra);
            }

            # Se não encontrar a compra em nenhuma origem
            if ($dataApiCompra->codigoRetorno == 204) {
                unset($dataApiCompra->origemConsulta);
            }
        }

        return $dataApiCompra;
    }

    /**
     *
     * Método responsável em buscar a compra na sequência
     * 1 - API SIASG
     * 2 - API NOVO DIVULGAÇÃO
     * 3 - Base de dados
     *
     * @param string $numeroAnoCompra
     * @param int $idUasgGerenciadora
     * @param int $modalidadeCompra
     * @param int|null $idUasgOrigemCompra
     * @return array|mixed|object|\stdClass|null
     * @throws GuzzleException
     */
    private function getCompraPosteriorNovoDivulgacao(
        string $numeroAnoCompra,
        int $idUasgGerenciadora,
        int $modalidadeCompra,
        ?int $idUasgOrigemCompra = null
    ) {
        $dataApiCompra = null;
        $dataApiCompraSiasg = null;
        $dataApiCompraNdc = null;

        $arrayNumeroCompra = explode('/', $numeroAnoCompra);
        $anoCompra = $arrayNumeroCompra[1];
        
        # Ordem para buscar a compra = 1º Novo Divulgação 2º SIASG
        if ($anoCompra > 2023) {
            $dataApiCompraNdc =  $this->getCompraAPINovoDivulgacao(
                $arrayNumeroCompra[0],
                $anoCompra,
                $idUasgGerenciadora
            );

            if (!empty($dataApiCompraNdc)) {
                $modalidade = Codigoitem::find($modalidadeCompra);
            }
            
            $retornoSucessoNDC = in_array($dataApiCompraNdc->codigoRetorno, $this->arraySucesso);
            
            if (isset($dataApiCompraNdc->data->compraSispp) && $retornoSucessoNDC) {
                $modalidadeIgual = $dataApiCompraNdc->data->compraSispp->codigoModalidade == $modalidade->descres;
                
                if (!is_null($dataApiCompraNdc->data) && $modalidadeIgual) {
                    return $this->handleReturnApi($dataApiCompraNdc);
                }

                $dataApiCompraNdc->modalidade_igual = $modalidadeIgual;
            }

            $arrayCodigoErroBuscarSiasg = [204, 200];
            $arrayCodigoErroBuscarSiasg = array_merge($arrayCodigoErroBuscarSiasg, $this->arrayErro);

            $existeCodigoErroBuscarSiasg = in_array($dataApiCompraNdc->codigoRetorno, $arrayCodigoErroBuscarSiasg);
            $modalidadeDiferente = false;
            
            if (isset($dataApiCompraNdc->data->compraSispp)) {
                $modalidadeDiferente = $modalidade->descres !== $dataApiCompraNdc->data->compraSispp->codigoModalidade;
            }
            # Buscar a compra pela API do SIASG
            if ($existeCodigoErroBuscarSiasg || $modalidadeDiferente) {
                $dataApiCompraSiasg = $this->getCompraAPISiasg(
                    $numeroAnoCompra,
                    $idUasgGerenciadora,
                    $modalidadeCompra,
                    $idUasgOrigemCompra
                );
            }
        }

        # Ordem para buscar a compra = 1º SIASG 2º Novo Divulgação
        if ($anoCompra == 2022 || $anoCompra == 2023) {
            $dataApiCompraSiasg = $this->getCompraAPISiasg(
                $numeroAnoCompra,
                $idUasgGerenciadora,
                $modalidadeCompra,
                $idUasgOrigemCompra
            );

            if ($dataApiCompraSiasg->codigoRetorno == 204) {
                $dataApiCompraNdc =  $this->getCompraAPINovoDivulgacao(
                    $arrayNumeroCompra[0],
                    $anoCompra,
                    $idUasgGerenciadora
                );

                $retornoSucessoNDC = in_array($dataApiCompraNdc->codigoRetorno, $this->arraySucesso);

                if (isset($dataApiCompraNdc->data->compraSispp) && $retornoSucessoNDC) {
                    $modalidade = Codigoitem::find($modalidadeCompra);

                    $modalidadeIgual = $dataApiCompraNdc->data->compraSispp->codigoModalidade == $modalidade->descres;

                    if (!is_null($dataApiCompraNdc->data) && $modalidadeIgual) {
                        return $this->handleReturnApi($dataApiCompraNdc);
                    }

                    $dataApiCompraNdc->modalidade_igual = $modalidadeIgual;
                }
            }
        }

        $dataApiCompra = $this->montarRetornoCompra($dataApiCompraSiasg, $dataApiCompraNdc);

        # Se não encontrar a compra na API do novo divulgação de compra
        if ($dataApiCompra->codigoRetorno == 204) {
            $dataApiCompra = $this->getDataCompraBase($idUasgOrigemCompra, $modalidadeCompra, $numeroAnoCompra);
        }

        # Se não encontrar a compra em nenhuma origem
        if ($dataApiCompra->codigoRetorno == 204) {
            unset($dataApiCompra->origemConsulta);
        }

        return $this->handleReturnApi($dataApiCompra);
    }

    /**
     *
     * Método responsável em retornar a compra na base dados
     *
     * @param int $unidadeOrigemId
     * @param int $modalidadeId
     * @param string $numeroAno
     * @return mixed
     */
    private function getDataCompraBase(int $unidadeOrigemId, int $modalidadeId, string $numeroAno)
    {
        $dataCompra = Compras::join('codigoitens', function ($query) {
            $query->on('codigoitens.id', 'compras.tipo_compra_id')
                ->where("codigoitens.descricao", "SISRP");
        })
            ->where('unidade_origem_id', $unidadeOrigemId)
            ->where('modalidade_id', $modalidadeId)
            ->where('numero_ano', $numeroAno)
            ->select("compras.*")
            ->first();

        $retornoCompra = new \stdClass();
        $retornoCompra->codigoRetorno = 200;

        $this->origemConsulta = 'base_de_dados';

        if (empty($dataCompra)) {
            $retornoCompra->data = null;
            return $this->handleReturnApi($retornoCompra);
        }
        
        $retornoCompra->data = $dataCompra->getAttributes();
        
        return $this->handleReturnApi($retornoCompra);
    }

    /**
     *
     * Método responsável em recuperar a compra na API do Novo divulgação
     *
     * @param int $numeroCompra
     * @param int $anoCompra
     * @param $idUasgGerenciadora
     * @return array|mixed|object|null
     * @throws GuzzleException
     */
    private function getCompraAPINovoDivulgacao(int $numeroCompra, int $anoCompra, $idUasgGerenciadora)
    {
        $dadosUasgCompra = (new UnidadeRepository())->getUnidadePorId($idUasgGerenciadora);

        $dataContracao = $this->mountDataContratacao($numeroCompra, $anoCompra, $dadosUasgCompra->codigo);
        $this->origemConsulta = 'api_novo_divulgacao';
        
        $retornoAPI = (object) $this->getContratacao($dataContracao);

        if ($retornoAPI->codigoRetorno == 200) {
            $retornoAPI->data = (object) $retornoAPI->data;
            $retornoAPI->data->compraSispp = (object) $retornoAPI->data->compraSispp;
        }

        return $retornoAPI;
    }

    /**
     *
     * Método responsável em montar o request que será enviado para consultar a compra na API do SIASG
     *
     * @param string $numeroAnoCompra
     * @param int $idUasgGerenciadora
     * @param int $modalidadeCompra
     * @param int|null $idUasgOrigemCompra
     * @return Request
     */
    private function mountRequestConsultaCompraSiasg(
        string $numeroAnoCompra,
        int $idUasgGerenciadora,
        int $modalidadeCompra,
        ?int $idUasgOrigemCompra = null
    ): Request {
        $requestConsultaCompraSiasg = new Request();

        $arrayRequest = [
            'numero_ano' => $numeroAnoCompra,
            'unidade_origem_id' => $idUasgGerenciadora,
            'modalidade_id' => $modalidadeCompra,
            'numero_ano' => $numeroAnoCompra,
        ];
        $requestConsultaCompraSiasg->merge($arrayRequest);

        if (!empty($idUasgOrigemCompra)) {
            $requestConsultaCompraSiasg->merge(['unidade_origem_compra_id' => $idUasgOrigemCompra]);
        }

        return $requestConsultaCompraSiasg;
    }
    
    /**
     * @param string $numeroAnoCompra
     * @param int $idUasgGerenciadora
     * @param int $modalidadeCompra
     * @param int|null $idUasgOrigemCompra
     * @return object|\stdClass
     */
    private function getCompraAPISiasg(
        string $numeroAnoCompra,
        int $idUasgGerenciadora,
        int $modalidadeCompra,
        ?int $idUasgOrigemCompra = null
    ) {
        
        $requestConsultaCompraSiasg = $this->mountRequestConsultaCompraSiasg(
            $numeroAnoCompra,
            $idUasgGerenciadora,
            $modalidadeCompra,
            $idUasgOrigemCompra
        );

        $dataApi = $this->consultaCompraSiasg($requestConsultaCompraSiasg);
        $this->origemConsulta = 'api_siasg';

        return $this->handleReturnApi($dataApi);
    }

    /**
     *
     * Método responsável em tratar os retornos das APIs
     *
     * @param object $data
     * @return object
     */
    private function handleReturnApi(object $data): object
    {
        if ($data->codigoRetorno == 200 && empty($data->data)) {
            $data = $this->mountObjectReturn(204, $data->message);
        }

        $data->origemConsulta = $this->origemConsulta;

        return $data;
    }

    /**
     *
     * Método responsável em montar as informações para buscar na API do novo divulgação
     *
     * @param int $numeroCompra
     * @param int $anoCompra
     * @param string $uasgCompra
     * @return array{numeroCompra: int, anoCompra: int, uasgCompra: string}
     */
    private function mountDataContratacao(int $numeroCompra, int $anoCompra, string $uasgCompra): array
    {
        return [
            'numeroCompra' => $numeroCompra,
            'anoCompra' => $anoCompra,
            'uasgCompra' => $uasgCompra
        ];
    }

    private function mountObjectReturn(int $codigoRetorno, ?string $mensagem = null, $dataAPI = null)
    {
        $data = new \stdClass();
        $data->data = $dataAPI;
        $data->message = $mensagem;
        $data->codigoRetorno = $codigoRetorno;
        
        return $data;
    }

    private function mocarDataVazio()
    {
        return $this->mountObjectReturn(204);
    }
    
    /**
     * @param string|null $subrrogada
     * @param string|null $idUnidadeUsuarioLogado
     * @return string|null
     */
    private function verificarPermissaoUasgCompraSisrp(
        ?string $subrrogada,
        ?int $idUnidadeUsuarioLogado
    ): ?string {
        $unidadeAutorizadaId = null;
        
        if ($subrrogada != '000000') {
            $idUnidadeSubrogada = (new UnidadeRepository())->getUnidadePorCodigo($subrrogada)->id;

            ($idUnidadeSubrogada == $idUnidadeUsuarioLogado) ?
                $unidadeAutorizadaId = $idUnidadeUsuarioLogado : $unidadeAutorizadaId;
        }
        
        return $unidadeAutorizadaId;
    }
    
    public function gravarItemCompra(object $retornoAPI, Compras $compra, int $idUsuario, bool $novoGestaoAta = true)
    {
        #Inclui os itens gravados/atualizados para os participantes
        if ($retornoAPI->origemConsulta == 'api_siasg') {
            $this->gravaParametroItensdaCompraSISRP($retornoAPI, $compra);
            return;
        }

        #Inclui os itens gravados/atualizados para os participantes
        if ($retornoAPI->origemConsulta == 'api_novo_divulgacao') {
            $this->gravarItemCompraNovoDivulgacao(
                $compra,
                $retornoAPI->data->linkSisrpCompleto,
                $novoGestaoAta,
                $idUsuario
            );
        }
    }
    
    private function gravarItemCompraNovoDivulgacao(
        Compras $compra,
        array $urlItens,
        bool $novoGestaoAta,
        int $idUsuario
    ) {
        $modcompraItem = new CompraItem();
        $novoDivulgacaoService = new NovoDivulgacaoCompraService();

        # Percorrer todos os itens recuperados no serviço
        foreach ($urlItens as $urlItem) {
            $urlItem['linkSisrpCompleto'] =
                str_replace('sisrp?', 'item?', $urlItem['linkSisrpCompleto']);
            
            try {
                $itemFormatadoPadraoCompras = (object)
                $novoDivulgacaoService->getItemCompraNovoDivulgacao($urlItem['linkSisrpCompleto']);
            } catch (Exception $exception) {
                $mensagem = "Erro teste: ".
                $exception->getResponse()->getBody()->getContents();
                $novoException = new Exception($mensagem);
                $this->inserirLogCustomizado('novo_divulgacao_compra', 'error', $novoException);
                continue;
            }
            
            $catmatseritem = $this->gravaCatmatseritem($itemFormatadoPadraoCompras);
            
            $itemFormatadoPadraoCompras->numeroItem = $itemFormatadoPadraoCompras->numero;
            unset($itemFormatadoPadraoCompras->numero);
            
            $compraItem =
                $modcompraItem->updateOrCreateCompraItemSisrp(
                    $compra,
                    $catmatseritem,
                    $itemFormatadoPadraoCompras
                );
            
            $this->salvarFornecedorNdc($itemFormatadoPadraoCompras->resultados, $compraItem->id, $novoGestaoAta);
            
            $this->salvarLocalEntregaNdc(
                $itemFormatadoPadraoCompras->locaisEntregaPorUnidade,
                $compraItem,
                true,
                $urlItem['linkSisrpCompleto']
            );
            
            
            $dadosItemAPI = new \stdClass();
            $dadosItemAPI->data = new \stdClass();
            $dadosItemAPI->data->maximoAdesao = $itemFormatadoPadraoCompras->maximoAdesao;
            $dadosItemAPI->data->permiteCarona = $itemFormatadoPadraoCompras->permiteCarona;
            
            # Origem 1 = NDC
            $this->atualizarPermiteCaronaMaximoAdesaoItem($dadosItemAPI, $compraItem, 1);
            
            # Filtra somente as unidades participantes do item
            $unidadesParticipantes = array_filter(array_map(function ($unidade) {
                if ($unidade['tipoUasg'] === 'P') {
                    return $unidade;
                }
            }, $itemFormatadoPadraoCompras->dadosUasgItemCompra));
            
            if (empty($unidadesParticipantes)) {
                continue;
            }
            
            $this->salvarParticipanteItemNdc(
                $unidadesParticipantes,
                $compraItem,
                $itemFormatadoPadraoCompras->locaisEntregaPorUnidade,
                $idUsuario
            );
        }
    }
    
    public function criarJOBItemParticipanteGestaoAtaAPISiasg(
        Compras $compraOrigem,
        Collection $itensCompra,
        int $idUsuario
    ) {
        $compraItemUnidadeRepository = new CompraItemUnidadeRepository();
        $compraItemRepository = new CompraItemRepository();
        $urlCompraSisrp = $this->montarURLItemParticipante();
        $consultaCompra = new ApiSiasg();
        
        foreach ($itensCompra as $item) {
            # Monta os parâmetros necessários para buscar o item no serviço 4
            $parametrosApiCompraItem = $this->montarInformacoesConsultaItemSISRP($item);

            $dadosItemAPI = $this->retornarExecucaoCompraItem($parametrosApiCompraItem);

            if (!is_null($dadosItemAPI->data) && $dadosItemAPI->codigoRetorno == 200) {
                # Insere as unidades participates para poder ser processado no JOB
                $compraOrigem->dataApi = $dadosItemAPI->data->dadosUasgItemCompraSisrp;

                $compraOrigem->codigo_modalidade = $compraOrigem->modalidade->descres;
                $compraOrigem->codigo_unidade_origem = $compraOrigem->unidadeOrigem->codigo;

                $compraOrigem->numero_item = $item->numero;
                
                $itemGerenciadora = $compraItemUnidadeRepository->getItemUnidadePorItemSomenteCarona($item->id);
                
                # Se a quantidade for zero para a gerenciadora cadastrados um participante que tenha quantidade
                # para que seja exibido na lista de fornecedores e itens no cadastro da ata
                if ($itemGerenciadora->quantidade_autorizada == 0) {
                    $this->salvarItemParticipanteAta(
                        $compraOrigem,
                        $urlCompraSisrp,
                        $consultaCompra
                    );
                    
                    # Recuperar o item salvo
                    $itemSalvo = $compraItemRepository->getItemPorIdCompraNumeroItem(
                        $compraOrigem->id,
                        $compraOrigem->numero_item
                    );
                    
                    
                    if (!empty($itemSalvo)) {
                        $this->atualizarPermiteCaronaMaximoAdesaoItem($dadosItemAPI, $itemSalvo, $compraOrigem->origem);
                    }
                    continue;
                }

                # Recuera o nome da queue
                $nomeJob = config('arp.arp.nome_queue');

                # Cria o queue passando como parâmetro os dados da compra e as unidades participantes
                $job = (new CarregarUnidadeParticipanteCompraGestaoAtaJOB($compraOrigem->toArray()))
                    ->onQueue($nomeJob);
                # Recupera o ID do Job para popular a tabela intermediária responsável
                # em verificar se o item ainda está processado
                $jobId = $this->dispatch($job);

                # Monta os dados para inserir na tabela intermediária
                $insertJobUser = [
                    'user_id' => $idUsuario,
                    'job_id' => $jobId,
                    'job_user_type' => Compras::class,
                    'job_user_id' => $compraOrigem->id,
                    'compra_item_id' => $item->id
                ];

                JobUser::create($insertJobUser);

                # Informa o máximo que o item permite de adessão e se permite carona
                $this->atualizarPermiteCaronaMaximoAdesaoItem($dadosItemAPI, $item, $compraOrigem->origem);
            }
        }
    }
    public function verificaPermissaoUasgCompraNovo(
        object $dataApiCompra,
        int $idUasgOrigemCompra,
        int $idUasgUsuarioLogado
    ) {
        $unidade_autorizada_id = $idUasgUsuarioLogado;
        
        $tipoCompra = $dataApiCompra->data->compraSispp->tipoCompra;
        $subrrogada = $dataApiCompra->data->compraSispp->subrogada;

        if ($tipoCompra == config('api.siasg.SISPP')) {
            if ($subrrogada != '000000') {
                $unidadeOrigem = (new UnidadeRepository())->getUnidadePorId($idUasgOrigemCompra);
                if ($subrrogada == $unidadeOrigem->codigo) {
                    $unidade_autorizada_id = $idUasgUsuarioLogado;
                }
            }
        }
        
        return $unidade_autorizada_id;
    }
    
    public function atualizarPermiteCaronaMaximoAdesaoItem(object $dadosItemAPI, CompraItem $item, int $origem)
    {
        $item->maximo_adesao = $dadosItemAPI->data->maximoAdesao;
        $item->permite_carona = $dadosItemAPI->data->permiteCarona;
        
        if ($origem == 2) {
            $item->maximo_adesao_informado_compra = $item->maximo_adesao;
        }
        $item->save();
    }
    
    public function salvarFornecedorNdc(array $arrayFornecedor, int $compraItemId, bool $novoGestaoAta)
    {
        foreach ($arrayFornecedor as $key => $fornecedor) {
            $fornecedor = (object)$fornecedor;
            
            $dadosFornecedor = $this->retornaFornecedor($fornecedor);
            
            # Grava/Atualiza o item para a tabela de fornecedores (compra_item_fornecedor)
            $this->gravaCompraItemFornecedor(
                $compraItemId,
                $fornecedor,
                $dadosFornecedor,
                $fornecedor->percentualMaiorDesconto,
                $novoGestaoAta
            );
        }
    }
    
    public function salvarLocalEntregaNdc(
        array $locaisEntregaPorUnidade,
        CompraItem $compraItem,
        bool $novoGestaoAta,
        string $linkSisrpCompleto
    ) {
        $enderecoService = new EnderecoService();
        $compraItemUnidadeLocalEntregaService = new CompraItemUnidadeLocalEntregaService();
        $unidadeRepository = new UnidadeRepository();
        
        foreach ($locaisEntregaPorUnidade as $codigoUnidade => $localEntregaPorUnidade) {
            $localEntregaPorUnidade = (object)$localEntregaPorUnidade;
            
            # Se a unidade do item for Participante, não cadastramos os registros
            if ($localEntregaPorUnidade->tipoUasg === 'P') {
                continue;
            }
            
            $unidade = $unidadeRepository->getUnidadePorCodigo($codigoUnidade);
            
            $idCompraItemUnidade =  $this->gravaCompraItemUnidadeSisrp(
                $compraItem,
                $unidade->id,
                $localEntregaPorUnidade,
                null,
                null,
                $localEntregaPorUnidade->tipoUasg,
                $novoGestaoAta
            );
            
            $localEntregaPorUnidade->localEntrega = array_filter($localEntregaPorUnidade->localEntrega);
            
            foreach ($localEntregaPorUnidade->localEntrega as $local) {
                $local = (array) $local;
                
                $dadosEndereco =  $enderecoService->insertEndereco($local);
                
                $arrayError = [204, 500];
                if (in_array($dadosEndereco['code'], $arrayError)) {
                    $mensagem = "Endpoint {$linkSisrpCompleto}
                        apresentou o erro : {$dadosEndereco['error']}";
                    throw new \Exception($mensagem, $dadosEndereco['code']);
                }
                
                $arrayInsertLocalEntrega = [
                    'compra_item_unidade_id' => $idCompraItemUnidade,
                    'endereco_id' => $dadosEndereco['data']['id'],
                    'quantidade' => $local['quantidade'],
                    'endereco_id_novo_divulgacao' => $local['id']
                ];
                
                $compraItemUnidadeLocalEntregaService->insertLocalEntrega($arrayInsertLocalEntrega);
            }
        }
    }
    
    public function salvarParticipanteItemNdc(
        array $unidadesParticipantes,
        CompraItem $compraItem,
        array $localEntregaUnidade,
        int $usuarioId
    ) {
        $nomeJob = config('arp.arp.nome_queue_novo_divulgacao');
        
        foreach ($unidadesParticipantes as $unidade) {
            $localEntregaPorUnidadeJOB = new \stdClass();
            $localEntregaPorUnidadeJOB->localEntrega =$localEntregaUnidade[$unidade['uasg']];
            $localEntregaPorUnidadeJOB->unidadeParticipante = $unidade['uasg'];
            $localEntregaPorUnidadeJOB->compraItem = $compraItem;
            
            $job = (
            new CarregarUnidadeParticipanteContratacaoNovoDCGestaoAtaJOB(
                $localEntregaPorUnidadeJOB
            ))->onQueue($nomeJob);
            
            $jobId = $this->dispatch($job);
            
            # Montar os dados para inserir na tabela intermediária
            $insertJobUser = [
                'user_id' => $usuarioId,
                'job_id' => $jobId,
                'job_user_type' => Compras::class,
                'job_user_id' => $compraItem->compra_id,
                'compra_item_id' => $compraItem->id
            ];
            
            JobUser::create($insertJobUser);
        }
    }

    private function montarRetornoCompra(?object $dataApiCompraSiasg, ?object $dataApiCompraNdc)
    {
        $dataApiCompra = new \stdClass();

        if (is_null($dataApiCompraSiasg->data) && is_null($dataApiCompraNdc->data)) {
            $dataApiCompra->codigoRetorno = 200;
            $dataApiCompra->data = null;
            $message = "SIASG: {$dataApiCompraSiasg->messagem}. <br>";
            $message .= "NDC: {$dataApiCompraNdc->messagem}.";
            $dataApiCompra->message = $message;
        }

        if (!is_null($dataApiCompraSiasg->data)) {
            $dataApiCompra = $dataApiCompraSiasg;
        }

        if (isset($dataApiCompraNdc) && !is_null($dataApiCompraNdc->data) &&
            isset($dataApiCompraNdc->modalidade_igual) &&
            $dataApiCompraNdc->modalidade_igual
        ) {
            $dataApiCompra = $dataApiCompraNdc;
        }
        return $dataApiCompra;
    }
}
