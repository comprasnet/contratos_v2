<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\NovoDivulgacaoCompra\NovoDilvugacaoFornecedorUnidadeRequest;
use App\Http\Requests\Api\NovoDivulgacaoCompra\NovoDivulgacaoCompraItemRequest;
use App\Http\Requests\Api\NovoDivulgacaoCompra\NovoDivulgacaoCompraRequest;
use App\Http\Requests\Api\PNCPCompraRequest;
use App\Services\ContratacaoPNCPCompraService;
use App\Services\NovoDivulgacaoCompra\EnderecoService;
use App\Services\NovoDivulgacaoCompra\NovoDivulgacaoCompraService;
use App\Services\PNCPCompraService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;

class PNCPCompraController extends Controller
{
    private $pncpCompraService = null;

    public function __construct(ContratacaoPNCPCompraService $pncpCompraService)
    {
        $this->pncpCompraService = $pncpCompraService;
    }

    /**
     * Método responsável em retornar a contratação completa e formatada no padrão utilizado no Contratos
     * @param PNCPCompraRequest $request
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function getContratacao(PNCPCompraRequest $request): JsonResponse
    {
        $data = $request->all();

        $dadosCompra = $this->pncpCompraService->getContratacao($data);
        return response()->json($dadosCompra, $dadosCompra['codigoRetorno']);
    }

    /**
     * Método responsável em retornar o item formatado no padrão utilizado no Contratos
     * @param NovoDivulgacaoCompraItemRequest $request
     * @return JsonResponse
     */
    public function getItem(NovoDivulgacaoCompraItemRequest $request): JsonResponse
    {
        $urlItem = url()->full();

        $itemAPINovoDivulgacao = $this->pncpCompraService->getItemCompraNovoDivulgacao($urlItem);

        return response()->json($itemAPINovoDivulgacao);
    }
    
    /**
     * Método responsável em inserir e retornar as informação do fornecedor inseridos ou atualizados
     * @param NovoDilvugacaoFornecedorUnidadeRequest $request
     * @return JsonResponse
     */
    public function getFornecedorUnidade(NovoDilvugacaoFornecedorUnidadeRequest $request): JsonResponse
    {
        $enderecoService = new EnderecoService();
        
        $retornoEndereco = $enderecoService->insertEndereco($request->all());
        
        return response()->json($retornoEndereco, $retornoEndereco['code']);
    }
}
