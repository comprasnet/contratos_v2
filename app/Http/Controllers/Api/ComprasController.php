<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AtualizarCompraV1Request;
use App\Models\Compras;
use App\Services\CompraService;
use Exception;
use Illuminate\Support\Facades\DB;

class ComprasController extends Controller
{
    protected $compraService;
    public function __construct(CompraService $compraService)
    {
        $this->compraService = $compraService;
    }
    
    public function atualizarCompraV1(AtualizarCompraV1Request $request)
    {
        try {
            DB::beginTransaction();
            $compra = $this->compraService->inserirAtualizarCompraApi($request->all()['compra_id']);
            $this->compraService->inserirAtualizarItemCompra(
                $compra['compra'],
                $compra['request'],
                $request->all()['user_id'],
                true
            );
            DB::commit();

            return response()->json(['code' => 200, 'message' => 'Compra atualizada com sucesso!']);
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json([
                'code' => $exception->getCode(),
                'message' => 'Erro ao atualizar a compra!',
                'error' => $exception->getMessage()
            ]);
        }
    }
}
