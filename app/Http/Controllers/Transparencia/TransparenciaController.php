<?php

namespace App\Http\Controllers\Transparencia;

use App\Http\Traits\ImportContent;
use App\Models\Unidade;
use App\Services\Pncp\Arp\DashboardArpService;
use Illuminate\Routing\Controller;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;

class TransparenciaController extends Controller
{
    use ImportContent;

    protected $data = []; // the information we send to the view

    /**
     * Show the admin dashboard.
     *
     */
    public function index()
    {
        $this->data['title'] = 'Início'; // set the page title
        $this->data['breadcrumbs'] = [
            'Inicio'     => false,
//            trans('backpack::base.dashboard') => false,
        ];

        $arquivoJS = [
            'assets/js/blockui/jquery.blockUI.js',
            'packages/laravel-paginate/laravel-paginate.js',
            'assets/js/admin/dashboard.js'
        ];

        $this->importarScriptJs($arquivoJS);
        $this->importarScriptCss([
            'packages/backpack/base/css/dashboard/dashboard.css'
        ]);

        return view(backpack_view('transparencia.index'), $this->data);
    }

    /**
     * Obtém a lista de atas de registro de preço. Se estiver logado, filtra pela unidade
     *
     * @return string no formato Json com as atas
     */
    public static function listArpDashboard()
    {
        $arps = DashboardArpService::getRepositoryByRequest();
        return response()
            ->json(
                $arps->getArpsByFilter()
            );
    }
}
