<?php

namespace App\Http\Controllers\Transparencia;

use App\Models\Arp;
use App\Models\Orgao;
use App\Models\Unidade;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller;

class TransparenciaAtaPrecosController extends Controller
{
/*
    public function index(Request $request) {

        $filter['data_vigencia_inicio'] = $request->input('data_vigencia_inicio') ? $request->input('data_vigencia_inicio') : null;
        $filter['data_vigencia_fim'] = $request->input('data_vigencia_fim') ? $request->input('data_vigencia_fim') : null;
        $filter['modalidade_licitacao'] = $request->input('modalidade_licitacao') ? $request->input('modalidade_licitacao') : null;
        $filter['ano_compra'] = $request->input('ano_compra') ? $request->input('ano_compra') : null;
        $filter['numero_compra'] = $request->input('numero_compra') ? $request->input('numero_compra') : null;
        $filter['unidade_federacao'] = $request->input('unidade_federacao') ? $request->input('unidade_federacao') : null;
        $filter['orgaos_gerenciadores'] = $request->input('orgaos_gerenciadores') ? $request->input('orgaos_gerenciadores') : null;
        $filter['unidades_gerenciadoras'] = $request->input('unidades_gerenciadoras') ? $request->input('unidades_gerenciadoras') : null;
        $filter['orgaos_participantes'] = $request->input('orgaos_participantes') ? $request->input('orgaos_participantes') : null;
        $filter['unidades_participantes'] = $request->input('unidades_participantes') ? $request->input('unidades_participantes') : null;
        $filter['busca'] = $request->input('busca') ? $request->input('busca') : null;
        $filter['vigente'] = $request->input('vigente') ? $request->input('vigente') : null;
        $filter['nao_vigente'] = $request->input('nao_vigente') ? $request->input('nao_vigente') : null;

        $orgaos = Orgao::all();
        $unidades = Unidade::all();

        $data = $this->filterData($filter);

        return view('transparency.transparencia', compact('data', 'filter', 'orgaos', 'unidades'));

    }

    protected function filterData($filter) {
        $data = Arp::orderBy('created_at', 'ASC');

        if($filter['data_vigencia_inicio']) {
            $data->vigenciaInicial($filter['data_vigencia_inicio']);
        }

        if($filter['data_vigencia_fim']) {
            $data->vigenciaFinal($filter['data_vigencia_fim']);
        }

        if($filter['modalidade_licitacao']) {
            $data->modalidadeLicitacao($filter['modalidade_licitacao']);
        }

        if($filter['ano_compra']) {
            $data->anoCompra($filter['ano_compra']);
        }

        if($filter['numero_compra']) {
            $data->numeroCompra($filter['numero_compra']);
        }

        if($filter['vigente']) {
            $data->where('rascunho', false);
        }

        if($filter['nao_vigente']) {
            $data->where('rascunho', '!=', false);
        }

        if($filter['unidade_federacao'] || $filter['orgaos_gerenciadores'] || $filter['unidades_gerenciadoras'] || $filter['orgaos_participantes'] || $filter['unidades_participantes'] || $filter['busca']) {
            $data->join('compras', 'compras.id', 'arp.compra_id')
                ->join('compra_items', 'compra_items.compra_id', 'compras.id')
                ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', 'compra_items.id')
                ->join('unidades', 'unidades.id', 'compra_item_unidade.unidade_id')
                ->join('orgaos', 'orgaos.id', 'unidades.orgao_id');

            if($filter['unidade_federacao']) {
                $data->join('municipios', 'municipios.id', 'unidades.municipio_id')
                    ->join('estados', 'estados.id', 'municipios.estado_id')
                    ->where('estados.nome', 'like', '%'.$filter['unidade_federacao'].'%');
            }

            if($filter['orgaos_gerenciadores']) {
                $data->where('compra_item_unidade.tipo_uasg', 'G')
                    ->whereIn('orgaos.id', $filter['orgaos_gerenciadores']);
            }

            if($filter['unidades_gerenciadoras']) {
                $data->where('compra_item_unidade.tipo_uasg', 'G')
                    ->whereIn('unidades.id', $filter['unidades_gerenciadoras']);
            }

            if($filter['orgaos_participantes']) {
                $data->where('compra_item_unidade.tipo_uasg', 'P')
                    ->whereIn('orgaos.id', $filter['orgaos_participantes']);
            }

            if($filter['unidades_participantes']) {
                $data->where('compra_item_unidade.tipo_uasg', 'P')
                    ->whereIn('unidades.id', $filter['unidades_participantes']);
            }

            if($filter['busca']) {
                $data->where('compra_item_unidade.tipo_uasg', 'G')->where('unidades.nome', 'like', '%'.$filter['busca'].'%')
                    ->orWhere('compras.numero_ano', 'like', '%'.$filter['busca'].'%')
                    ->orWhere('orgaos.nome', 'like', '%'.$filter['busca'].'%');
            }

        }

        $data = $data->select([
                'arp.*'
            ])
            ->get();

        return $data;
    }
*/
}
