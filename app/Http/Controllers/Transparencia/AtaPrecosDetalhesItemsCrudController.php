<?php

namespace App\Http\Controllers\Transparencia;

use App\Http\Requests\CompraItemsRequest;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\CommonFields;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Models\Arp;
use App\Models\ArpItem;
use App\Models\CompraItem;
use App\Models\Compras;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

use function config;

class AtaPrecosDetalhesItemsCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \App\Http\Controllers\Operations\UpdateOperation;
    use \App\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    use CommonColumns;
    use BuscaCodigoItens;
    use CommonFields;
    use CompraTrait;
    use Formatador;
    use ImportContent;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $ata_id = Route::current()->parameter('id');
        $numero_item = Route::current()->parameter('numero_item');
        CRUD::setModel(\App\Models\Arp::class);
        $this->queryItem($ata_id, $numero_item);
        CRUD::setRoute(config('backpack.base.route_prefix') . "/transparencia/arpshow/itens/$numero_item/$ata_id");
        $this->crud->setShowView('vendor.backpack.crud.transparencia.ata_detalhamento.show');
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->allowAccess('show');

        //$this->exibirTituloPaginaMenu('Detalhamento do Item da Ata de Registro de Preços');

        $this->crud->setEntityNameStrings(
            'Exibe informações detalhadas do item',
            'Detalhamento do Item da Ata de Registro de Preços'
        );

        $this->data['breadcrumbs'] = [
            trans('Voltar') => "/transparencia/arpshow/$ata_id/show",
            trans('Voltar') => "/transparencia/arpshow/$ata_id/show",
            trans('Visualizar') => false
        ];
    }

    private function queryItem(int $ata_id, string $numero_item)
    {
        CRUD::setModel(\App\Models\Arp::class);
        CRUD::addClause(
            'select',
            'compra_items.numero',
            'catmatseritens.codigo_siasg',
            'compra_items.catmatseritem_id',
            'arp.vigencia_inicial',
            'arp.vigencia_final',
            'compra_item_fornecedor.quantidade_homologada_vencedor',
            'compra_item_fornecedor.quantidade_empenhada',
            'compra_item_fornecedor.valor_negociado',
            DB::raw(
                "CONCAT(' R$ ', TO_CHAR(compra_item_fornecedor.valor_unitario, '999G999D99'))
                AS valor_unitario_formatado"
            ),
            DB::raw(
                "CONCAT(' R$ ', TO_CHAR(compra_item_fornecedor.valor_negociado, '999G999D99'))
                AS valor_negociado_formatado"
            ),
            DB::raw('compra_items.descricaodetalhada as desc_detalhada'),
            // DB::raw(TO_CHAR('compra_item_fornecedor.valor_unitario as valor_unitario_formatado', '999G999D99')),
            DB::raw('codigoitens.descricao as tipo_item'),
            DB::raw('sum(compra_item_unidade.quantidade_saldo) AS quantidade_saldo'),
            DB::raw('sum(compra_item_unidade.quantidade_adquirir) AS quantidade_adquirir'),
            DB::raw('sum(quantidade_autorizada) AS quantidade_autorizada'),
            DB::raw('compra_items.descricaodetalhada as desc_resumida')
        );
        CRUD::addClause('join', 'arp_item', 'arp_item.arp_id', '=', 'arp.id');
        CRUD::addClause(
            'join',
            'compra_item_fornecedor',
            'arp_item.compra_item_fornecedor_id',
            '=',
            'compra_item_fornecedor.id'
        );
        CRUD::addClause('join', 'compra_items', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id');
        CRUD::addClause('join', 'compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compra_items.id');
        CRUD::addClause('join', 'unidades', 'unidades.id', '=', 'compra_item_unidade.unidade_id');
        CRUD::addClause('join', 'codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id');
        CRUD::addClause('join', 'catmatseritens', 'catmatseritens.id', '=', 'compra_items.catmatseritem_id');

        CRUD::addClause('where', 'arp.id', '=', $ata_id);
        CRUD::addClause('where', 'compra_items.numero', '=', $numero_item);
        CRUD::addClause(
            'groupby',
            'compra_items.numero',
            'codigoitens.descricao',
            'catmatseritens.codigo_siasg',
            'compra_items.descricaodetalhada',
            'compra_item_fornecedor.valor_unitario',
            'arp.vigencia_inicial',
            'arp.vigencia_final',
            'compra_item_fornecedor.valor_unitario',
            'compra_item_fornecedor.quantidade_homologada_vencedor',
            'compra_item_fornecedor.quantidade_empenhada',
            'compra_item_fornecedor.valor_negociado',
            'compra_items.descricaodetalhada',
            'compra_items.catmatseritem_id'
        );
    }

    protected function setupListOperation()
    {
        //$this->addColumnModelFunction('numero_ano', 'Número', 'getNumeroAno');
        $this->visualizarListShow();
        $this->crud->addButtonFromModelFunction('line', 'getViewButtonArp', 'getViewButtonArp', 'beginning');
        $this->crud->enableExportButtons();
        $this->crud->text_button_redirect_create = 'Ata';
    }

    private function visualizarListShow()
    {
        $this->addColumnModelFunction('unidade_origem', 'Unidade Gerenciadora', 'getUnidadeOrigem');
        $this->addColumnDate(true, true, true, true, 'vigencia_inicial', 'Vigência Inicial');
        $this->addColumnDate(true, true, true, true, 'vigencia_final', 'Vigência Final');
        $this->addColumnModelFunction('numero_compra', 'Número da Compra', 'getNumeroCompra');
        $this->addColumnModelFunction('fornecedor', 'Fornecedor', 'getFornecedorPorAta');
    }

    public function getItemPorAtaMinutaEmpenho($situacao, $numero_item, $idAta)
    {
        $arpItem = new  \App\Models\ArpItem();
        return $arpItem->getItemPorAtaMinutaEmpenho($situacao, $numero_item, $idAta);
    }

    protected function setupShowOperation()
    {
        $idAta = Route::current()->parameter("id");
        $ata = Arp::find($idAta);
        $this->crud->set('show.contentClass', 'col-md-12');
        $this->crud->set('show.setFromDb', false);

        /*************************************/
        /* DADOS DA ATA DE REGISTRO DE PREÇO */
        /*************************************/
        $numero_item = Route::current()->parameter('numero_item');
        $ata_id = Route::current()->parameter('id');

        $this->crud->addField([
            'name' => 'numero',
            'label' => 'Número da ata de registro de preços',
            'type'  => 'text',
            'value' => $ata->getNumeroAno(),
            'wrapperAttributes' => [
                'class' => 'col-md-4',
            ]
        ]);

        $this->crud->addField([
            'name' => 'unidade_gerenciadora',
            'label' => 'Unidade gerenciadora',
            'value' => $ata->unidades->codigo . '-' . $ata->unidades->nomeresumido,
            'wrapperAttributes' => [
                'class' => 'col-md-4'
            ]
        ]);

        $this->crud->addField([
            'name' => 'numero_compra',
            'label' => 'Número da compra/ Ano',
            'value' => $ata->getNumeroCompra(),
            'wrapperAttributes' => [
                'class' => 'col-md-4',
            ]
        ]);

        $this->crud->addField([
            'name' => 'modalidade',
            'label' => 'Modalidade da compra',
            'type'  => 'text',
            'value' => $ata->getModalidadeCompra(),
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-5',
            ]
        ]);

        $this->crud->addField([
            'name' => 'data_assinatura',
            'label' => 'Data da assinatura',
            'value' => Carbon::parse($ata->data_assinatura)->format('d/m/Y'),
            'tab' => 'testee',
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-5'
            ]
        ]);

        $this->crud->addField([
            'name' => 'vigencia_inicial',
            'label' => 'Vigência inicial',
            'type' => 'date',
            'value' => Carbon::parse($ata->vigencia_inicial)->format('d/m/Y'),
            'wrapperAttributes' => [
                'class' => 'col-md-4 areaDadosAta pt-5'
            ]
        ]);

        $this->crud->addField([
            'name' => 'vigencia_final',
            'label' => 'Vigência final',
            'type' => 'data',
            'value' => Carbon::parse($ata->vigencia_final)->format('d/m/Y'),
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-5'
            ]
        ]);

        $this->crud->addField([
            'name' => 'valor_total',
            'label' => 'Valor total',
            'type' => 'number',
            'value' => ' R$ ' . number_format($ata->getValorTotal(), 2, ',', '.'),
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-5 areaItemSelecionado'
            ]
        ]);

        /*************************/
        /* Tab: DETALHES DO ITEM */
        /*************************/
        CRUD::addColumn([
            'name'  => 'numero',
            'label' => 'Número do item',
            'type'  => 'text',
            'function_name' => function ($numero) {
                return $numero->numero;
            },
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInShow' => true,
            'visibleInExport' => true,
            'tab' => 'tab1'
        ]);

        CRUD::addColumn([
            'name'  => 'desc_detalhada',
            'limit' => 1000000,
            'label' => 'Descrição detalhada',
            'type'  => 'text',
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInShow' => true,
            'visibleInExport' => true,
            'tab' => 'tab1'
        ]);

        CRUD::addColumn([
            'name' => 'catmatseritem_id',
            'label' => 'Código do item',
            'type' => 'text',
            'visibleInTable' => false,
            'visibleInModal' => false,
            'visibleInShow' => true,
            'visibleInExport' => true,
            'tab' => 'tab1'
        ]);

        CRUD::addColumn([
            'name'  => 'tipo_item',
            'label' => 'Tipo do item ',
            'type'  => 'text',
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInShow' => true,
            'visibleInExport' => true,
            'tab' => 'tab1'
        ]);
        /*
        CRUD::addColumn([
            'name'  => 'valor_unitario_formatado',
            'label' => 'Valor Unitario ',
            'type'  => 'text',
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInShow' => true,
            'visibleInExport' => true,
            'tab' => 'tab1'
        ]);
        */
        CRUD::addColumn([
            'name'  => 'quantidade_homologada_vencedor',
            'label' => 'Quantidade homologada',
            'type'  => 'text',
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInShow' => true,
            'visibleInExport' => true,
            'tab' => 'tab1'
        ]);

        CRUD::addColumn([
            'name'  => 'vigencia_inicial',
            'label' => 'Vigência inicial ',
            'type'  => 'date',
            'function_name' => function ($vigencia_inicial) {
                return Carbon::parse($vigencia_inicial->vigencia_final)->format('d/m/Y');
            },
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInShow' => true,
            'visibleInExport' => true,
            'tab' => 'tab1'
        ]);

        CRUD::addColumn([
            'name'  => 'vigencia_final',
            'label' => 'Vigência final',
            'type'  => 'date',
            'function_name' => function ($vigencia_final) {
                return Carbon::parse($vigencia_final->vigencia_final)->format('d/m/Y');
            },
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInShow' => true,
            'visibleInExport' => true,
            'tab' => 'tab1'
        ]);

        /********************************************************/
        /* TABELA FORNECEDOR - Dentro da ficha Detalhes do Item */
        /********************************************************/
        $this->addColumnTable(
            'fornecedor_item',
            'Fornecedor',
            Arp::getFornecedorPorAtaItem($idAta, $numero_item),
            ['table' => false, 'modal' => false, 'show' => true, 'export' => true],
            'tab1'
        );

        /*************************/
        /* Tab: UNIDADES DO ITEM */
        /*************************/
        $unidadesParticipanteItens = Arp::getUnidadeParticipanteItens($idAta, $numero_item);
        $posicao = ['left', 'left', 'left', 'end', 'end'];

        $this->addColumnTableCentralizada(
            'unidade_participante',
            'Unidades',
            $unidadesParticipanteItens,
            ['table' => true, 'modal' => false, 'show' => true, 'export' => true],
            'tab2',
            $posicao
        );

        /*****************/
        /* Tab: EMPENHOS */
        /*****************/

        $posicao = ['left', 'left', 'end', 'left'];
        #$situacao = $this->retornaIdCodigoItem('Situações Minuta Empenho', 'EMPENHO EMITIDO');

        # Busca as informações para exibir na tabela com as unidades e totalizadores
        $totaisMinutaEmpenhoUnidade = Arp::getItemTotaisMinutaEmpenhoPorUnidade($idAta, $numero_item);
        # Busca as informações para exibir na tabela com os empenhos por unidade
        $minutasEmpenhoUnidades = Arp::getItemMinutasEmpenhoPorUnidade($idAta, $numero_item);

        # Se houver uma das tabelas é porque tem dados de empenho. Somente neste caso mostra as informações
        if ($totaisMinutaEmpenhoUnidade || $minutasEmpenhoUnidades) {
            $quantidadeTotalRegistradaAutorizada =
                Arp::getQuantidadeTotalRegistradaAutorizadaDeUmItem($idAta, $numero_item);

            # Exibe linha com a quantidade total registrada + autorizada
            CRUD::addColumn([
                'name' => 'quantidade_autorizada_empenho',
                'label' => 'Quantidade Registrada/Autorizada',
                'type' => 'text',
                'value' => $quantidadeTotalRegistradaAutorizada,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'tab' => 'tab4'
            ]);

            # Exibe linha com a quantidade total de saldo
            CRUD::addColumn([
                'name' => 'saldo_para_empenho',
                'label' => 'Saldo para Empenho',
                'type' => 'text',
                'value' => Arp::getQuantidadeTotalSaldoEmpenhoDeUmItem($idAta, $numero_item),
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'tab' => 'tab4',
            ]);

            # Tabela com as unidades e totalizadores
            $this->addColumnTable(
                'totais_por_unidade',
                'Totais por unidade',
                # Método que busca as unidades do item com totalizadores de empenho
                $totaisMinutaEmpenhoUnidade,
                ['table' => true, 'modal' => false, 'show' => true, 'export' => true],
                'tab4'
            );

            # Tabela com os empenhos por unidade
            $this->addColumnTable(
                'empenhos_por_unidade',
                'Empenhos por unidade',
                # Método que busca as unidades do item com totalizadores de empenho
                $minutasEmpenhoUnidades,
                ['table' => true, 'modal' => false, 'show' => true, 'export' => true],
                'tab4'
            );
        } else { # Se não houver tabelas com dados de empenho, exibe mensagem que não há informações
            CRUD::addColumn([
                'name' => 'nao_ha_informacoes_de_empenho',
                'type' => 'label',
                'value' => 'Não há informações de empenho',
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'tab' => 'tab4',
            ]);
        }


        /****************/
        /* Tab: ADESÕES */
        /****************/

        # Busca os totalizadores
        $totalizadoresAdesao = ArpItem::getTotalizadoresAdesao($idAta, $numero_item);

        # Faz um if para cada total, pois se não existir o valor, não cria um traço horizontal a mais na página
        if ($totalizadoresAdesao->maximo_adesao && $totalizadoresAdesao->maximo_adesao > 0) {
            CRUD::addColumn([
                'name' => 'quantidade_adesao',
                'label' => 'Qtd. limite para adesão',
                'type' => 'text',
                'value' => $totalizadoresAdesao->maximo_adesao,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'tab' => 'tab3'
            ]);
            
            CRUD::addColumn([
                'name' => 'maximo_adesao_informado_compra',
                'label' => 'Qtd. limite informado na compra',
                'type' => 'text',
                'value' => $totalizadoresAdesao->maximo_adesao_informado_compra,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'tab' => 'tab3'
            ]);
        }

        if ($totalizadoresAdesao->quantidade_disponivel_adesao) {
            CRUD::addColumn([
                'name' => 'quantidade_disponivel_adesao',
                'label' => 'Quantidade disponivel para adesão',
                'type' => 'text',
                'value' => $totalizadoresAdesao->quantidade_disponivel_adesao,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'tab' => 'tab3'
            ]);
        }

        if ($totalizadoresAdesao->quantidade_aguardando_analise) {
            CRUD::addColumn([
                'name' => 'quantidade_aguardando_analise',
                'label' => 'Quantidade aguardando análise',
                'type' => 'text',
                'value' => $totalizadoresAdesao->quantidade_aguardando_analise,
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'tab' => 'tab3'
            ]);
        }

        $aceitaAdesao = $totalizadoresAdesao->aceita_adesao ? $totalizadoresAdesao->aceita_adesao : 'Não';
        CRUD::addColumn([
            'name' => 'aceita_adesao',
            'label' => 'Aceita adesão',
            'type' => 'text',
            'value' => $aceitaAdesao,
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInShow' => true,
            'visibleInExport' => true,
            'tab' => 'tab3'
        ]);

        # Busca array com a lista de itens com adesões aprovadas e adiciona na página em forma de tabela
        $adesoesAprovadas = Arp::getAdesoesAprovadasDoItemPorUnidade($idAta, $numero_item);

        if ($adesoesAprovadas) {
            # Utilizado para exibir um título que identifica e separa visualmente a tabela de solicitações aprovadas
            CRUD::addColumn([
                'name' => 'titulo_solicitacoes_aprovadas',
                'label' => 'Solicitações aprovadas',
                'type' => 'text',
                'value' => '',
                'visibleInTable' => false,
                'visibleInModal' => false,
                'visibleInShow' => false,
                'visibleInExport' => false,
                'tab' => 'tab3'
            ]);

            $this->addColumnTable(
                'solicitacoes_aprovadas',
                'Solicitações aprovadas',
                $adesoesAprovadas,
                ['table' => true, 'modal' => false, 'show' => true, 'export' => true],
                'tab3'
            );
        }

        # Busca array com a lista de itens com adesões aguardando análise do item (enviadao para aprovação)
        # e adiciona em forma de tabela
        $adesoesAguardandoAnalise = Arp::getAdesoesAguardandoAnaliseDoItemPorUnidade($idAta, $numero_item);

        if ($adesoesAguardandoAnalise) {
            # Utilizado para exibir um título que identifica e separa visualmente a tabela de solicitações em análise
            CRUD::addColumn([
                'name' => 'titulo_solicitacoes_aguardando_analise',
                'label' => 'Solicitações aguardando análise',
                'type' => 'text',
                'value' => '',
                'visibleInTable' => false,
                'visibleInModal' => false,
                'visibleInShow' => false,
                'visibleInExport' => false,
                'tab' => 'tab3'
            ]);

            $this->addColumnTable(
                'solicitacoes_aguardando_analise',
                'Solicitações aguardando análise',
                $adesoesAguardandoAnalise,
                ['table' => false, 'modal' => false, 'show' => true, 'export' => true],
                'tab3'
            );
        }

        /******************/
        /* Tab: CONTRATOS */
        /******************/

        # Monta a tabela de contratos adicionando o link para acesso
        $colunasTratadas = null;
        $colunasProntas = Arp::getDetalhamentoContratosItens($idAta, $numero_item);

        if ($colunasProntas) {
            foreach ($colunasProntas['id'] as $key => $value) {
                # Monta o link de acesso
                $link = '<a href="' .
                    str_replace('appredirect/', 'transparencia/contratos/', env('URL_CONTRATO_VERSAO_UM')) .
                    $value .
                    '" target="_blank" style="font-size: 27px;" title="Clique aqui para visualizar o contrato">' .
                    trans('') .
                    '<i class="la la-external-link" aria-hidden="true" style="margin-right: -25px;"></i></a>';
                $colunasTratadas['Ação'][$key] = $link;
            }

            # Elimina a coluna 'id' pois não será exibida
            unset($colunasProntas['id']);

            $r = array_merge($colunasProntas, $colunasTratadas);

            $this->addColumnTable(
                'numero_instrumento',
                '',
                $r,
                ['table' => false, 'modal' => false, 'show' => true, 'export' => true],
                'tab5'
            );
        } else {
            CRUD::addColumn([
                'name' => 'nao_ha_contratos_relacionados',
                'type' => 'text',
                'value' => 'Não há contratos relacionados.',
                'visibleInTable' => true,
                'visibleInModal' => true,
                'visibleInShow' => true,
                'visibleInExport' => true,
                'tab' => 'tab5',
            ]);
        }
    }
}
