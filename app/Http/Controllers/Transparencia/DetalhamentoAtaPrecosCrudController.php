<?php

namespace App\Http\Controllers\Transparencia;

use App\Http\Controllers\Operations\DeleteOperation;
use App\Http\Controllers\Operations\UpdateOperation;
use App\Http\Requests\CodigoRequest;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\CommonFields;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Models\Arp;
use App\Models\ArpArquivo;
use App\Models\ArpItem;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;


use function config;

/**
 * Class CodigoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DetalhamentoAtaPrecosCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    use CommonColumns;
    use BuscaCodigoItens;
    use CommonFields;
    use CompraTrait;
    use Formatador;
    use ImportContent;



    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {

        $id = Route::current()->parameter('id');
        CRUD::setModel(\App\Models\Arp::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . "/transparencia/arp/$id/show");
        $this->exibirTituloPaginaMenu('Detalhamento da Ata de Registro de Preços');
        $this->crud->setShowView('vendor.backpack.crud.transparencia.ata_precos.show');

        $this->crud->setEntityNameStrings('Exibe informações detalhadas da ata', 'Detalhamento da Ata de Registro de Preços');



        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');

        $this->data['breadcrumbs'] = [
            trans('backpack::crud.admin') => url(
                config('backpack.base.route_prefix'),
                'transparencia'
            ),
            trans('Visualizar') => false
        ];
    }

    public function import()
    {
        // whatever you decide to do
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->addColumnModelFunction('numero_ano', 'Número', 'getNumeroAno');
        $this->visualizarListShow();

        $this->crud->addButtonFromModelFunction('line', 'getViewButtonArp', 'getViewButtonArp', 'beginning');

        $this->crud->enableExportButtons();
        $this->crud->text_button_redirect_create = 'Ata';
    }

    private function visualizarListShow()
    {
        $this->addColumnModelFunction('unidade_origem', 'Unidade Gerenciadora', 'getUnidadeOrigem');
        $this->addColumnDate(true, true, true, true, 'vigencia_inicial', 'Vigência Inicial');
        $this->addColumnDate(true, true, true, true, 'vigencia_final', 'Vigência Final');
        $this->addColumnModelFunction('numero_compra', 'Número da Compra', 'getNumeroCompra');
        $this->addColumnModelFunction('fornecedor', 'Fornecedor', 'getFornecedorPorAta');
    }

    protected function SetupShowOperation()
    {

        $this->crud->set('show.contentClass', 'col-md-12');

        $this->crud->set('show.setFromDb', false);

        $idAta = Route::current()->parameter("id");
        $ata = Arp::find($idAta);

        $atas = ArpItem::getUnidadeParticipantePorAta($idAta);

        $ataItem = ArpItem::getItemPorAta($idAta);
        $ataValor = ArpItem::getValorTotalAta($idAta);

        $ataArquivos = ArpArquivo::getArquivosPorAta($idAta);

        $this->crud->addField([
            'name' => 'numero',
            'label' => 'Número da ata de registro de preços',
            'type'  => 'text',
            'value' => $ata->getNumeroAno(),
            'wrapperAttributes' => [
                'class' => 'col-md-4',
            ]
        ]);
        $this->crud->addField([
            'name' => 'unidade_gerenciadora',
            'label' => 'Unidade gerenciadora',
            'value' => $ata->unidades->codigo . '-' . $ata->unidades->nomeresumido,
            'wrapperAttributes' => [
                'class' => 'col-md-4'
            ]
        ]);

        $this->crud->addField([
            'name' => 'link_ata',
            'label' => 'Link da ata no PNCP',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'col-md-4'
            ],
            'render_as_html' => true,
            'value' => '<a href="' . $ata->getLinkPNCP() .
                '" target="_blank" data-clipboard-text="' . $ata->getLinkPNCP() . '">' .
                Str::limit($ata->getLinkPNCP(), 35) .
                '</a>',
        ]);
        
        $linkReduzidoCompraAtaPncp = Str::limit($ata->getLinkCompraPncp(), 35);
        $this->crud->addField([
            'name' => 'link_compra_ata_pncp',
            'label' => 'Link da compra no PNCP',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-5'
            ],
            'render_as_html' => true,
            'value' => "<a href='{$ata->getLinkCompraPncp()}' target='_blank'>{$linkReduzidoCompraAtaPncp}</a>",
        ]);

        $this->crud->addField([
            'name' => 'numero_compra',
            'label' => 'Número da compra/ Ano',
            'value' => $ata->getNumeroCompra(),
            'wrapperAttributes' => [
                'class' => 'col-md-4  pt-5',
            ]
        ]);

        $this->crud->addField([
            'name' => 'modalidade',
            'label' => 'Modalidade da compra',
            'type'  => 'text',
            'value' => $ata->getModalidadeCompra(),
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-5',
            ]
        ]);


        $this->crud->addField([
            'name' => 'data_assinatura',
            'label' => 'Data da assinatura',
            'value' => Carbon::parse($ata->data_assinatura)->format('d/m/Y'),
            'tab' => 'testee',
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-5'
            ]
        ]);

        $this->crud->addField([
            'name' => 'vigencia_inicial',
            'label' => 'Vigência inicial',
            'type' => 'date',
            'value' => Carbon::parse($ata->vigencia_inicial)->format('d/m/Y'),
            'wrapperAttributes' => [
                'class' => 'col-md-4 areaDadosAta pt-5'
            ]
        ]);
        
        $this->crud->addField([
            'name' => 'vigencia_final',
            'label' => 'Vigência final',
            'type' => 'data',
            'value' => Carbon::parse($ata->vigencia_final)->format('d/m/Y'),
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-5'
            ]
        ]);

        $this->crud->addField([
            'name' => 'valor_total',
            'label' => 'Valor total',
            'type' => 'number',
            'value' => ' R$ ' . number_format($ata->getValorTotal(), 2, ',', '.'),
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-5 areaItemSelecionado'
            ]
        ]);
        
        $this->crud->addField([
            'name' => 'situacao',
            'label' => 'Status da ata',
            'value' => $ata->getSituacaoAtaAtiva(),
            'wrapperAttributes' => [
                'class' => 'col-md-4  pt-5',
            ]
        ]);


        # Monta a tabela simplificada de itens da ata
        $titulo = null;
        $colunaTabela = Arp::getItemAtaSimplificada($idAta);
        foreach ($colunaTabela['Número'] as $key => $item) {
            $url = url('/transparencia/arpshow/itens/'.$item.'/'.$idAta.'/show');
            $btnVisualizar = "<a class='br-button circle primary large mr-3 br-button-row-table' href='".$url." ' >".
                                "<i class='fas fa-eye'></i></a>";
                $titulo['Ação'][$key] = $btnVisualizar;
        }
        $r = array_merge($colunaTabela, $titulo);
        $this->crud->addField([
            'name' => 'numero_item',
            'label' => 'Itens',
            'type' => 'number',
            'value' => $r,
            'wrapperAttributes' => [
                'class' => 'col-md-12 pt-5'
            ]
        ]);

        # Monta a tabela de arquivos  da ata
        $colunasTratadas = null;
        $colunasProntas = Arp::getArquivosTransparencia($idAta);
        if ($colunasProntas) {
            foreach ($colunasProntas['Descrição'] as $key => $value) {
                # Monta o link para download
                $url = url('storage/' . ($colunasProntas['url'][$key]));
                $link = '<a href="' . $url .
                    '" target="_blank" title="Baixar arquivo"><i class="fa fa-download"></i></a>';
                $colunasTratadas['Ação'][$key] = $link;
            }

            # Elimina a coluna 'url' pois não será exibida
            unset($colunasProntas['url']);

            $r = array_merge($colunasProntas, $colunasTratadas);

            $this->crud->addField([
                'name' => 'arquivos_ata',
                'label' => 'Arquivos',
                'type' => 'number',
                'value' => $r,
                'wrapperAttributes' => [
                    'class' => 'col-md-12 pt-5'
                ]
            ]);
        }



        $this->addColumnModelFunction('numero_ano', 'Número', 'getNumeroAno');

        $this->visualizarListShow();
        $this->addColumnDate(true, true, true, true, 'data_assinatura', 'Data da Assinatura');
        $this->addColumnValorMonteraio('valor_total', 'Valor Total');

        $gestores = Arp::getGestorAta($idAta);
        $this->addColumnTable('gestor_ata', 'Gestores', $gestores);

        $autoridades = Arp::getAutoridadeSignataria($idAta);
        $this->addColumnTable('autoridade_signataria_ata', 'Autoridades', $autoridades);

        $this->addColumnModelFunction('modalidade_compra', 'Modalidade da Compra', 'getModalidadeCompra');

        $this->addColumnModelFunction('compra_centralizada', 'Compra centralizada', 'getCompraCentralizada');

        $unidadeParticipante = Arp::getUnidadeParticipante($idAta);
        $this->addColumnTable('unidade_participante', 'Unidade Participante', $unidadeParticipante);

        $fornecedores = Arp::getFornecedorCompra($idAta);
        $this->addColumnTable('fornecedor_compra', 'Fornecedores da Compra', $fornecedores);



        $item = Arp::getItemAta($idAta);

        /*$posicao = ['end','end','end'];
            $this->addColumnTableCentralizada(
                'item_ata',
                'Item da Ata',
                $item,
                ['table' => true, 'modal' => true, 'show' => true, 'export' => true],
                false,
                $posicao
            );
        */
        $this->addColumnTable('item_ata', 'Item da Ata', $item);

        $this->addColumnModelFunction('rascunho', 'Status', 'getStatus');

        $arp = $this->crud->getModel()->findOrFail($idAta);

        if (!$arp->rascunho) {
            $this->crud->denyAccess('update');
            $this->crud->denyAccess('delete');
        }
    }
}
