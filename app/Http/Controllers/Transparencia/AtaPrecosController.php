<?php

namespace App\Http\Controllers\Transparencia;

use App\Http\Controllers\Controller;
use App\Models\Estado;
use App\Models\Fornecedor;
use App\Models\Orgao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\ImportContent;
use App\Models\Arp;
use App\Models\ArpAutoridadeSignataria;
use App\Models\ArpGestor;
use App\Models\ArpItem;
use App\Models\ArpUnidades;
use App\Models\Autoridadesignataria;
use App\Models\CompraItemFornecedor;
use App\Models\Unidade;
use App\Models\User;
use Attribute;
use DataTables;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use App\Http\Traits\Transparencia\FilterDataTrait;
use Illuminate\Support\Facades\Cache;

class AtaPrecosController extends Controller
{
    use FilterDataTrait;

    public function getUnidades(Request $request)
    {
        $unidades = Unidade::orderBy('nome', 'ASC')->select([
            'id',
            DB::raw("codigo || ' - ' ||nomeresumido AS nome")
        ]);
        $query = $request->get('q');
        if ($query) {
            $unidades->where('nomeresumido', 'ILIKE', '%' . $query . '%')
                ->orWhere('codigo', 'ILIKE', '%' . $query . '%');
        }
        return ($unidades->take(20)->get()->toArray());
    }

    public function getOrgaos(Request $request)
    {
        $orgaos = Orgao::orderBy('nome', 'ASC')->select([
            'id',
            DB::raw("codigo || ' - ' ||nome AS nome")
        ]);
        $query = $request->get('q');
        if ($query) {
            $orgaos->where('nome', 'ILIKE', '%' . $query . '%')
                ->orWhere('codigo', 'ILIKE', '%' . $query . '%');
        }

        return ($orgaos->take(20)->get()->toArray());
    }

    public function getFornecedores(Request $request)
    {
        $fornecedores = Fornecedor::orderBy('nome', 'ASC')->select([
            'id',
            DB::raw("nome || ' - ' ||cpf_cnpj_idgener AS nome")
        ]);
        $query = $request->get('q');
        if ($query) {
            $fornecedores->where('nome', 'ILIKE', '%' . $query . '%')
                ->orWhere('cpf_cnpj_idgener', 'ILIKE', '%' . $query . '%')
                ->orWhere(DB::raw("regexp_replace(cpf_cnpj_idgener,
                 '\D','','g')"), 'ILIKE', DB::raw("regexp_replace('$query',
                  '\D','','g')"));
        }

        return ($fornecedores->take(20)->get()->toArray());
    }

    public function getEstados(Request $request)
    {
        $estados = Estado::orderBy('nome', 'ASC')->select(['id', 'nome']);
        $query = $request->get('q');
        if ($query) {
            $estados->where('nome', 'ILIKE', '%' . $query . '%');
        }

        return ($estados->take(20)->get()->toArray());
    }

    public function index(Request $request)
    {

        $filter['data_vigencia_inicio'] = $request->input('dataInicio') ?
            $request->input('dataInicio') : null;


        $filter['data_vigencia_fim'] = $request->input('dataFim') ?
            $request->input('dataFim') : null;
        $filter['modalidade_licitacao'] =
            $request->input('modalidadeLicitacao') ?
                $request->input('modalidadeLicitacao') : null;
        $filter['ano_compra'] = $request->input('anoCompra') ?
            $request->input('anoCompra') : null;
        $filter['numero_compra'] = $request->input('numeroCompra') ?
            $request->input('numeroCompra') : null;
        $filter['ano_ata'] = $request->input('anoAta') ?
            $request->input('anoAta') : null;
        $filter['numero_ata'] = $request->input('numeroAta') ?
            $request->input('numeroAta') : null;
        $filter['unidade_federacao'] = $request->input('unidadeFederacao') ?
            $request->input('unidadeFederacao') : null;
        $filter['orgaos_gerenciadores'] =
            $request->input('orgaosGerenciadores') ?
                $request->input('orgaosGerenciadores') : null;
        $filter['unidades_gerenciadoras'] =
            $request->input('unidadesGerenciadoras') ?
                $request->input('unidadesGerenciadoras') : null;
        $filter['orgaos_participantes'] =
            $request->input('orgaosParticipantes')
                ? $request->input('orgaosParticipantes') : null;
        $filter['unidades_participantes'] =
            $request->input('unidadesParticipantes') ?
                $request->input('unidadesParticipantes') : null;
        $filter['fornecedor_ata'] = $request->input('fornecedorAta') ?
            $request->input('fornecedorAta') : null;
        $filter['palavra_chave'] = $request->input('palavra_chave') ?
            $request->input('palavra_chave') : null;
        $filter['status'] = $request->input('status') ?
            $request->input('status') : null;
        $filter['titulo'] = 'Consultar Atas';
        $filter['subTitulo'] = 'Lista de Atas de Registro de Preços';
        if ($request->has('status')) {
            $orderBy = $request->input('order') ?
                $request->input('order') : 'recente';
        } else {
            $orderBy = $request->input('order') ?
                $request->input('order') : 'inicio';
        }



        $arps = $this->filterData($filter, $orderBy);


        $cacheKey = 'arps_' . md5(json_encode($filter) . $orderBy);


        if (Cache::has($cacheKey)) {
           // Log::info('Cache hit for key: ' . $cacheKey);
            $arps = Cache::get($cacheKey);
        }
        $arps = Cache::remember($cacheKey, now()->addMinutes(30), function () use ($filter, $orderBy) {
         // Log::info('Cache criado para chave: ');
            return $this->filterData($filter, $orderBy);
        });


        $basePath = str_replace(
            url(''),
            '',
            preg_replace(
                ['/\/transparencia\//', '/&?page=[0-9]+/'],
                '',
                $request->fullUrl()
            )
        );

        $arps->currentUrl = $basePath;
       // dd($arps);
//        return view(
//            'transparency.transparencia',
//            compact('arps', 'filter', 'orderBy')
//        );

        $content = view(
            'transparency.transparencia',
            compact(
                'arps',
                'filter',
                'orderBy'
            )
        )->render();

        return $content;
    }
}
