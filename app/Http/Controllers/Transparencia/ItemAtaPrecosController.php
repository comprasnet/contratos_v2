<?php

namespace App\Http\Controllers\Transparencia;

use App\Http\Controllers\Controller;
use App\Models\Estado;
use App\Models\Fornecedor;
use App\Models\Orgao;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\ImportContent;
use App\Models\Arp;
use App\Models\ArpAutoridadeSignataria;
use App\Models\ArpGestor;
use App\Models\ArpItem;
use App\Models\ArpUnidades;
use App\Models\Autoridadesignataria;
use App\Models\CompraItemFornecedor;
use App\Models\Unidade;
use App\Models\User;
use Attribute;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\DataTables;

class ItemAtaPrecosController extends Controller
{
    public function getUnidades(Request $request)
    {
        $unidades = Unidade::orderBy('nome', 'ASC')->select(['id', DB::raw("codigo || ' - ' ||nomeresumido AS nome")]);
        $query = $request->get('q');
        if ($query) {
            $unidades->where('nomeresumido', 'ILIKE', '%' . $query . '%')
                ->orWhere('codigo', 'ILIKE', '%' . $query . '%');
        }
        return ($unidades->take(20)->get()->toArray());
    }

    public function getOrgaos(Request $request)
    {
        $orgaos = Orgao::orderBy('nome', 'ASC')->select(['id',  DB::raw("codigo || ' - ' ||nome AS nome")]);
        $query = $request->get('q');
        if ($query) {
            $orgaos->where('nome', 'ILIKE', '%' . $query . '%')
                ->orWhere('codigo', 'ILIKE', '%' . $query . '%');
        }

        return ($orgaos->take(20)->get()->toArray());
    }
    public function getFornecedores(Request $request)
    {
        $fornecedores = Fornecedor::orderBy('nome', 'ASC')->select(['id', DB::raw("nome || ' - ' ||cpf_cnpj_idgener AS nome")]);
        $query = $request->get('q');
        if ($query) {
            $fornecedores->where('nome', 'ILIKE', '%' . $query . '%')
                ->orWhere('cpf_cnpj_idgener', 'ILIKE', '%' . $query . '%')
                ->orWhere(
                    DB::raw("regexp_replace(cpf_cnpj_idgener, '\D','','g')"),
                    'ILIKE',
                    DB::raw("regexp_replace('$query', '\D','','g')")
                );
        }

        return ($fornecedores->take(20)->get()->toArray());
    }

    public function getEstados(Request $request)
    {
        $estados = Estado::orderBy('nome', 'ASC')->select(['id', 'nome']);
        $query = $request->get('q');
        if ($query) {
            $estados->where('nome', 'ILIKE', '%' . $query . '%');
        }

        return ($estados->take(20)->get()->toArray());
    }

    public function index(Request $request)
    {
        if (empty(backpack_user())) {
            return redirect(backpack_url('login'));
        }
        $filter['titulo'] = 'Consultar Itens de Atas de Registro de Preços';
        $filter['subTitulo'] = 'Lista de itens de Atas de Registro de Preços';

        /*********************/
        /*** CAMPOS FILTRO ***/
        /*********************/
        $filter['data_vigencia_inicio'] = $request->input('dataInicio')
            ? $request->input('dataInicio') : null;
        $filter['data_vigencia_fim'] = $request->input('dataFim')
            ? $request->input('dataFim') : null;
        $filter['modalidade_licitacao'] = $request->input('modalidadeLicitacao')
            ? $request->input('modalidadeLicitacao') : null;
        $filter['ano_compra'] = $request->input('anoCompra')
            ? $request->input('anoCompra') : null;
        $filter['numero_compra'] = $request->input('numeroCompra')
            ? $request->input('numeroCompra') : null;
        $filter['ano_ata'] = $request->input('anoAta')
            ? $request->input('anoAta') : null;
        $filter['numero_ata'] = $request->input('numeroAta')
            ? $request->input('numeroAta') : null;
        $filter['descricao_item'] = $request->input('descricaoItem')
            ? $request->input('descricaoItem') : null;
        $filter['codigo_item'] = $request->input('codigoItem')
            ? $request->input('codigoItem') : null;
        $filter['unidade_federacao'] = $request->input('unidadeFederacao')
            ? $request->input('unidadeFederacao') : null;
        $filter['orgaos_gerenciadores'] = $request->input('orgaosGerenciadores')
            ? $request->input('orgaosGerenciadores') : null;
        $filter['unidades_gerenciadoras'] = $request->input('unidadesGerenciadoras')
            ? $request->input('unidadesGerenciadoras') : null;
        $filter['orgaos_participantes'] = $request->input('orgaosParticipantes')
            ? $request->input('orgaosParticipantes') : null;
        $filter['unidades_participantes'] = $request->input('unidadesParticipantes')
            ? $request->input('unidadesParticipantes') : null;
        $filter['fornecedor_ata'] = $request->input('fornecedorAta')
            ?
            $request->input('fornecedorAta')
            : null;
        $filter['palavra_chave'] = $request->input('palavra_chave')
            ? $request->input('palavra_chave') : null;
        $filter['status'] = $request->input('status')
            ? $request->input('status') : 'vigente';
        $filter['detalhesItem'] = $request->input('detalhesItem')
            ? $request->input('detalhesItem') : null;
        $filter['codigo_unidade'] = $request->input('codigoUnidade')
            ? $request->input('codigoUnidade') : null;
        $filter['modalidade_compra'] = $request->input('modalidadeCompra')
            ? $request->input('modalidadeCompra') : null;
        $filter['numero_item_compra'] = $request->input('numeroItemCompra')
            ? $request->input('numeroItemCompra') : null;
        $filter['esfera'] = $request->input('esfera')
            ? $request->input('esfera') : null;
        $filter['uf'] = $request->input('uf')
            ? $request->input('uf') : null;
        $filter['municipio'] = $request->input('municipio')
            ? $request->input('municipio') : null;
        $filter['codigo_pdm'] = $request->input('codigoPdm')
            ? str_pad($request->input('codigoPdm'), 5, "0", STR_PAD_LEFT) : null;
        $filter['descricao_pdm'] = $request->input('descricaoPdm')
            ? $request->input('descricaoPdm') : null;
        $filter['unidades'] = $request->input('unidades')
            ? $request->input('unidades') : null;

        /*****************/
        /*** Ordenação ***/
        /*****************/
        $orderBy = $request->input('order')
            ? $request->input('order') : 'recente';

        # Obtém as esferas da unidade para montar o select no filtro
        $esferas = (new Unidade())->getEsferasStatic();

        return view('transparency.transparencia_arp_itens', compact('filter', 'esferas', 'orderBy'));
    }

    public function ajax(Request $request)
    {

        # camposFiltro é a variável montada em ajax no arquivo transparencia_arp_itens.blade
        $camposFiltro = $request->input('camposFiltro');
        $filter = collect($camposFiltro)->pluck('value', 'name')->reject(function ($valor) {
            return $valor === null;
        });
        $search = $request->input('search');

        $userId = $request->input('user_id');
        $redisKey = "user:{$userId}:advanced_search";
        if (Redis::exists($redisKey)) {
            $nextAvailableTime = Redis::get($redisKey) + 2;

            if (time() < $nextAvailableTime) {
                $remainingTime = $nextAvailableTime - time();
                return response()->json(['error' => "Aguarde $remainingTime segundos antes de realizar uma nova busca."
                ], Response::HTTP_TOO_MANY_REQUESTS);
            }
        }
        Redis::set($redisKey, time());
        try {
            $query = ArpItem::select(
                'compra_items.descricaodetalhada',
                'estados.nome as unidade_federacao',
                'arp.id',
                DB::raw('compra_items.numero as numero_item_compra'),
                DB::raw('catmatpdms.codigo as codigo_pdm'),
                DB::raw('catmatpdms.descricao as descricao_pdm'),
                DB::raw("fornecedores.cpf_cnpj_idgener || ' - ' || fornecedores.nome as fornecedor"),
                DB::raw("unidades.codigo || ' - ' || unidades.nome as unidade_gerenciadora"),
                DB::raw(
                    "to_char(compra_item_fornecedor.quantidade_homologada_vencedor, 'FM999G999G999.0000')
                AS quantidade_registrada"
                ),
                //            DB::raw(
                //                "CASE
                //                    WHEN SUM(arp_solicitacao_item.quantidade_aprovada) IS NULL
                //                    THEN compra_items.maximo_adesao
                //                    ELSE (compra_items.maximo_adesao - SUM(arp_solicitacao_item.quantidade_aprovada))
                //                    END as saldo_adesao"
                //            ),

                DB::raw(
                    "COALESCE(
            compra_items.maximo_adesao - (
                SELECT SUM(arp_solicitacao_item.quantidade_aprovada)
                FROM arp_solicitacao_item
                WHERE arp_solicitacao_item.item_arp_fornecedor_id = arp_item.id
            ),
            compra_items.maximo_adesao
        ) AS saldo_adesao"
                ),
                DB::raw("arp.numero || '/' || arp.ano as numero"),
                'arp.vigencia_inicial',
                'arp.vigencia_final',
                'compra_items.catmatseritem_id',
                'unidades.nomeresumido',
                'orgaos.nome'
            )
                ->join('arp', 'arp_item.arp_id', '=', 'arp.id')
                ->join('arp_unidades', 'arp_item.id', '=', 'arp_unidades.arp_item_id')
                ->leftJoin('unidades', 'arp.unidade_origem_id', '=', 'unidades.id')
                ->leftJoin('municipios', 'unidades.municipio_id', '=', 'municipios.id')
                ->join('estados', 'municipios.estado_id', '=', 'estados.id')
                ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
                ->join('compra_items', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
                ->join('fornecedores', 'compra_item_fornecedor.fornecedor_id', '=', 'fornecedores.id')
                ->join('orgaos', 'orgaos.id', '=', 'unidades.orgao_id')
                ->join('catmatseritens', 'catmatseritens.id', '=', 'compra_items.catmatseritem_id')
                ->join('catmatpdms', 'catmatseritens.catmatpdm_id', '=', 'catmatpdms.id')
                // add de joins para melhor o filtro de palavra chave

                // ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', 'compra_items.id')
                // ->join('unidades', 'unidades.id', 'compra_item_unidade.unidade_id')
                // ->join('orgaos', 'orgaos.id', 'unidades.orgao_id')

                ->leftJoin('arp_solicitacao_item', 'arp_item.id', '=', 'arp_solicitacao_item.item_arp_fornecedor_id')
                ->join('compras', 'compra_items.compra_id', '=', 'compras.id')
                ->join('codigoitens', 'codigoitens.id', '=', 'compras.modalidade_id')
                ->whereNotExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('arp_item_historico')
                        ->join('arp_historico', 'arp_historico.id', '=', 'arp_item_historico.arp_historico_id')
                        ->join('arp_alteracao', function ($query) {
                            $query->on('arp_historico.arp_alteracao_id', 'arp_alteracao.id')
                                ->where('arp_alteracao.rascunho', false);
                        })
                        ->whereRaw('arp_item_historico.arp_item_id = arp_item.id')
                        ->where(function ($query) {
                            $query->where('arp_item_historico.item_cancelado', true)
                                ->orWhere('arp_item_historico.item_removido_retificacao', true);
                        });
                })
                ->where('arp.rascunho', false);


            if (!empty($filter['palavra_chave'])) {
                $query->where(function ($q) use ($filter) {
                    //$q->whereRaw('unaccent(lower(ci.descricaodetalhada)) L
                    //IKE unaccent(?)', ['%'.strtolower($filter['palavra_chave']).'%'])
                    //$q->whereRaw('unaccent(ci.descricaodetalhada) LIKE unaccent(?)',
                    // ['%'.$filter['palavra_chave'].'%'])
                    $q->whereRaw(
                        'unaccent(compra_items.descricaodetalhada) ILIKE unaccent(?)',
                        ['%' . $filter['palavra_chave'] . '%']
                    )
                        ->orWhereRaw(
                            'unaccent(lower(orgaos.nome)) LIKE unaccent(?)',
                            ['%' . strtolower($filter['palavra_chave']) . '%']
                        )
                        ->orWhereRaw(
                            'unaccent(lower(unidades.nomeresumido)) LIKE unaccent(?)',
                            ['%' . strtolower($filter['palavra_chave']) . '%']
                        )
                        ->orWhereRaw(
                            'unaccent(lower(unidades.codigo)) LIKE unaccent(?)',
                            ['%' . strtolower($filter['palavra_chave']) . '%']
                        )
                        ->orWhereRaw(
                            'CAST(compra_items.catmatseritem_id AS varchar) LIKE ?',
                            ['%' . $filter['palavra_chave'] . '%']
                        )
                        ->orWhereRaw(
                            'arp.numero || \'/\' || arp.ano ILIKE ?',
                            ['%' . $filter['palavra_chave'] . '%']
                        )
                        ->orWhereRaw(
                            'compras.numero_ano || \'/\' || compras.numero_ano ILIKE ?',
                            ['%' . $filter['palavra_chave'] . '%']
                        );
                });
            }
//
//        $query->groupBy(
//            'arp.numero',
//            'unidade_gerenciadora',
//            'compra_items.descricaodetalhada',
//            'estados.nome',
//            'fornecedor',
//            'compra_items.maximo_adesao',
//            'arp.vigencia_inicial',
//            'arp.vigencia_final',
//            'arp.id',
//            'numero_item_compra',
//            'compra_items.catmatseritem_id',
//            'unidades.nomeresumido',
//            'orgaos.nome',
//            'compra_item_fornecedor.quantidade_homologada_vencedor',
//            'codigo_pdm',
//            'descricao_pdm'
//        );

            foreach ($filter as $index => $item) {
                $this->setCondicao($query, $index, $item);
            }

            #dd($query->toSql(),  $query->getBindings());


            return DataTables::of($query)
                ->addColumn('acao', function ($row) {
                    $url = url(
                        '/transparencia/arpshow/itens/' . $row->numero_item_compra . '/' . $row->id . '/' . 'show'
                    );
                    return '<a href="' . $url . '" class=""><i class="fas fa-eye"></i></a>';
                })
                ->rawColumns(['acao'])
                ->filter(function ($query) use ($search) {
                    if (!is_null($search['value'])) {
                        $query->where(function ($query) use ($search) {
                            $query->whereRaw("unaccent(
                        compra_items.descricaodetalhada) ILIKE unaccent(
                        '%{$search['value']}%')")
                                ->orWhereRaw("unaccent(
                            lower(orgaos.nome)) ILIKE unaccent(
                            '%{$search['value']}%')")
                                ->orWhereRaw("unaccent(
                            lower(unidades.nomeresumido)) ILIKE unaccent(
                            '%{$search['value']}%')")
                                ->orWhereRaw("unaccent(
                            lower(unidades.codigo)) ILIKE unaccent(
                            '%{$search['value']}%')")
                                ->orWhereRaw("CAST(
                            compra_items.catmatseritem_id AS VARCHAR) ILIKE '%
                            {$search['value']}%'")
                                ->orWhereRaw("concat(
                            arp.numero, '/', arp.ano) 
                            ILIKE '%{$search['value']}%'")
                                ->orWhereRaw("concat(
                            compras.numero_ano, '/', compras.numero_ano) 
                            ILIKE '%{$search['value']}%'");
                        });
                    }
                })
                ->make(true);
            Redis::del($redisKey);
        } catch (\Exception $e) {
            Redis::del($redisKey);
            return response()->json(['error' => 'Ocorreu um erro ao processar sua busca.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Para cada campo a ser consultado, este método seleciona dinamicamente outro método
     * que montará uma condição específica para o campo na query
     *
     * @param object $query Armazena a query que está sendo montada para realizar a consulta no banco
     * @param string $index Identificador do campo a ser pesquisado
     * @param mixed $item Valor do campo que será pesquisado
     * @return void
     */
    private function setCondicao($query, string $index, $item)
    {
        $condicoes = [
            'status' => ['function' => 'Status', 'column' => 'arp.rascunho'],
            'numeroAta' => ['function' => 'Ilike', 'column' => 'arp.numero'],
            'dataInicio' => ['function' => 'Data', 'column' => 'arp.vigencia_inicial'],
            'dataFim' => ['function' => 'Data', 'column' => 'arp.vigencia_final'],
            'numeroCompra' => ['function' => 'Ilike', 'column' => 'compras.numero_ano'],
            'anoCompra' => ['function' => 'Ilike', 'column' => 'compras.numero_ano'],
            'descricaoItem' => ['function' => 'IlikeLower', 'column' => 'compra_items.descricaodetalhada'],
            'codigoItem' => ['function' => 'Ilike', 'column' => 'compra_items.catmatseritem_id'],
            'codigoUnidade' => ['function' => 'Equal', 'column' => 'unidades.codigo'],
            'modalidadeCompra' => ['function' => 'Equal', 'column' => 'codigoitens.descres'],
            'numeroItemCompra' => ['function' => 'Equal', 'column' => 'compra_items.numero'],
            'esfera' => ['function' => 'ILike', 'column' => 'unidades.esfera'],
            'municipio' => ['function' => 'ILike', 'column' => 'municipios.nome'],
            'fornecedor_ata' => ['function' => 'In', 'column' => 'fornecedores.id'],
            'uf' => [
                'function' => 'ILikeOr',
                'columns' => ['estados.sigla', 'estados.nome']
            ],
            'codigoPdm' => ['function' => 'Equal', 'column' => 'catmatpdms.codigo'],
            'descricaoPdm' => ['function' => 'ILikeLower', 'column' => 'catmatpdms.descricao'],
            'unidades' => ['function' => 'In', 'column' => 'unidades.id']
        ];

        if (isset($condicoes[$index])) {
            # Executa o método com nome montado dinamicamente setCondicao<índice 'function' do array>
            $function = "setCondicao{$condicoes[$index]['function']}";
            $this->$function($query, $condicoes[$index], $item);
        }
    }

    /**
     * Quando um só campo deve ser pesquisado em mais colunas utilizando 'or'
     *
     * @param Object  $query
     * @param array $columns
     * @param int $item
     * @return void
     */
    private function setCondicaoILikeOr($query, $columns, $item)
    {
        if (isset($item)) {
            $query->where(function ($query) use ($columns, $item) {
                foreach ($columns['columns'] as $column) {
                    $query->orWhereRaw("lower(unaccent({$column})) ILIKE lower(unaccent(?))", ["%{$item}%"]);
                }
            });
        }
    }

    private function setCondicaoEqual($query, $column, $item)
    {
        $query->where($column['column'], $item);
    }

    private function setCondicaoIlikeLower($query, $column, $item)
    {
        $query->whereRaw("lower(unaccent({$column['column']}))  like lower(unaccent(?))", ["%$item%"]);
        #$query->whereRaw("unaccent({$column['column']}) like unaccent(?)", ["%$item%"]);
    }

    private function setCondicaoIlike($query, $column, $item)
    {
        $query->where($column['column'], 'ilike', "%$item%");
    }

    private function setCondicaoData($query, $column, $item)
    {
        $date = DateTime::createFromFormat('d/m/Y', $item);
        $formatada = $date->format('Y-m-d');

        if (strpos($column['column'], 'inicial') !== false) {
            $query->where($column['column'], '>=', $formatada);
            return;
        }

        $query->where($column['column'], '<=', $formatada);
    }

    private function setCondicaoStatus($query, $column, $item)
    {
//        if (strpos($item, 'todos') !== false) {
//            return;
//        }

//        if (strpos($item, 'nao_vigente') !== false) {
//            $query->where('rascunho', '=', true);
//            return;
//        }
        $dataAtual = Carbon::now();
        if (strpos($item, 'nao_vigente') !== false) {
            return $query->where(function ($q) use ($dataAtual) {
                $q->where('vigencia_final', '<', $dataAtual)
                    ->orWhere('vigencia_inicial', '>', $dataAtual);
            });
        }

        if (strpos($item, 'vigente') !== false) {
            return $query->where(function ($q) use ($dataAtual) {
                $q->where('vigencia_final', '>=', $dataAtual)
                    ->where('vigencia_inicial', '<=', $dataAtual);
            });
        }


        //$query->where('arp.rascunho', '=', false);

        return ;
    }

    private function setCondicaoIn($query, $column, $item)
    {
        return $query->whereIn($column['column'], $item);
    }

    /**
     * Método executado pela rota utilizada pelo sistema de compras, para exibir os itens filtrados diretamente
     * para uma compra específica
     *
     * @param integer $codigo_unidade
     * @param string $modalidade_compra
     * @param string $numero_ano_compra
     * @param string|null  $numero_item_compra
     * @return void
     */
    public function listItensComBaseNaCompra(
        $codigo_unidade,
        $modalidade_compra,
        $numero_ano_compra,
        $numero_item_compra = null
    ) {
        return Redirect::route('transparencia.arp-item', [
            'codigoUnidade' => $codigo_unidade,
            'modalidadeCompra' => $modalidade_compra,
            'numeroCompra' => substr($numero_ano_compra, 0, 5),
            'anoCompra' => substr($numero_ano_compra, -4),
            'numeroItemCompra' => $numero_item_compra
        ]);
    }
}
