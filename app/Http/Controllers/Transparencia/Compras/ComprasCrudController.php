<?php

namespace App\Http\Controllers\Transparencia\Compras;

use App\Http\Requests\ComprasRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Models\CodigoItem;
use App\Models\Compras;
use App\Models\Unidade;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Traits\ImportContent;

/**
 * Class ComprasCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ComprasCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    use ImportContent;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {

        $this->crud->text_button_redirect_create = 'Compras';
        CRUD::setModel(\App\Models\Compras::class);

        CRUD::setRoute(config('backpack.base.route_prefix') . '/transparencia/compras');

        $this->exibirTituloPaginaMenu('Compras');

        CRUD::addClause('select', 'compras.*');
        CRUD::addClause('leftjoin', DB::raw('unidades as unidade_origem'), 'unidade_origem.id', '=', 'compras.unidade_origem_id');
        CRUD::addClause('leftjoin', DB::raw('unidades as unidade_subrrogada'), 'unidade_subrrogada.id', '=', 'compras.unidade_subrrogada_id');
        CRUD::addClause('leftjoin', DB::raw('unidades as uasg_beneficiaria'), 'uasg_beneficiaria.id', '=', 'compras.uasg_beneficiaria_id');
        CRUD::addClause('leftjoin', DB::raw('codigoitens as modalidade'), 'modalidade.id', '=', 'compras.modalidade_id');
        CRUD::addClause('leftjoin', DB::raw('codigoitens as tipo_compra'), 'tipo_compra.id', '=', 'compras.tipo_compra_id');

        $this->crud->addButtonFromView('line', 'compras', 'transparencia-compraitens', 'end');

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
    }

    protected function setupListOperation()
    {

        $this->crud->setOperationSetting('searchableTable', false);
        $this->crud->addFilter(
            [
            'name'        => 'unidade_origem_id',
            'type'        => 'select2_ajax',
            'label'       => 'Unidade Origem',
            'placeholder' => '',
            'minimum_input_length' => 0,
            'method'      => 'GET',
            'select_attribute' => 'nome_completo',
            'select_key' => 'id',
             'mensagemSelectError' =>  'Aguarde alguns segundos antes de realizar uma nova busca.',
            ],
            url('/transparencia/compras/ajax-unidades-options'),
            function ($value) {
                $this->crud->addClause('where', 'unidade_origem_id', $value);
            }
        );

        $this->crud->addFilter(
            [
            'name'        => 'unidade_subrrogada_id',
            'type'        => 'select2_ajax',
            'label'       => 'Unidade Sub-rogada',
            'placeholder' => '',
            'minimum_input_length' => 0,
            'method'      => 'GET',
            'select_attribute' => 'nome_completo',
            'select_key' => 'id',
            'mensagemSelectError' =>  'Aguarde alguns segundos antes de realizar uma nova busca.',
            ],
            url('/transparencia/compras/ajax-unidades-options'),
            function ($value) {
                $this->crud->addClause('where', 'unidade_subrrogada_id', $value);
            },
        );

        $this->crud->addFilter([
            'name'  => 'tipo_compra',
            'type'  => 'select2',
            'label' => 'Tipo de Compra'
        ], function () {
            return [
                'SISPP' => 'SISPP',
                'SISRP' => 'SISRP',
            ];
        }, function ($value) {
            $this->crud->addClause('where', 'tipo_compra.descricao', $value);
        });

        $this->crud->addFilter(
            [
            'name'        => 'modalidade_id',
            'type'        => 'select2_ajax',
            'label'       => 'Modalidade',
            'minimum_input_length' => 0,
            'placeholder' => '',
            'method'      => 'GET',
            'select_attribute' => 'descres_descricao',
            'select_key' => 'id',
            'mensagemSelectError' =>  'Aguarde alguns segundos antes de realizar uma nova busca.',
            ],
            url('/transparencia/compras/ajax-modalidade-options'),
            function ($value) {
                $this->crud->addClause('where', 'modalidade_id', $value);
            }
        );

        $this->crud->addFilter(
            [
            'name'        => 'lei',
            'type'        => 'select2_ajax',
            'label'       => 'Lei',
            'placeholder' => '',
            'minimum_input_length' => 0,
            'method'      => 'GET',
            'select_attribute' => 'lei',
            'select_key' => 'lei',
            'mensagemSelectError' =>  'Aguarde alguns segundos antes de realizar uma nova busca.',
            ],
            url('/transparencia/compras/ajax-compra-lei-options'),
            function ($value) {
                $this->crud->addClause('where', 'lei', $value);
            }
        );

        $this->setupShowOperation();

        //$this->crud->enableExportButtons();
    }

    public function modalidadeOptions(Request $request)
    {
        $userId = backpack_user()->id;
        $redisKey = "user:{$userId}:advanced_search";
        if (Redis::exists($redisKey)) {
            $nextAvailableTime = Redis::get($redisKey) + 2;
            if (time() < $nextAvailableTime) {
                $remainingTime = $nextAvailableTime - time();
                return response()->json(
                    ['error' =>
                    "Aguarde $remainingTime segundos antes de realizar uma nova busca."],
                    Response::HTTP_TOO_MANY_REQUESTS
                );
            }
        }
        Redis::set($redisKey, time());
        try {
            $term = $request->input('term');
            return CodigoItem::whereHas('codigo', function ($query) {
                $query->where('descricao', '=', 'Modalidade Licitação')
                    ->whereNull('deleted_at');
            })
                ->whereRaw('LENGTH(descres) <= 2')
                ->where('descricao', 'ilike', '%'.$term.'%')
                ->orderBy('codigoitens.descres')
                ->select(DB::raw("CONCAT(descres,' - ',descricao) AS descres_descricao"), 'id')
                ->pluck('descres_descricao', 'id');
            Redis::del($redisKey);
        } catch (\Exception $e) {
            Redis::del($redisKey);
            return response()->json(
                ['error' => 'Ocorreu um erro ao processar sua busca.'],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
    public function unidadesOptions(Request $request)
    {
        $userId = backpack_user()->id;
        $redisKey = "user:{$userId}:advanced_search";
        if (Redis::exists($redisKey)) {
            $nextAvailableTime = Redis::get($redisKey) + 2;
            if (time() < $nextAvailableTime) {
                $remainingTime = $nextAvailableTime - time();
                return response()->json(
                    ['error' =>
                    "Aguarde $remainingTime segundos antes de realizar uma nova busca."],
                    Response::HTTP_TOO_MANY_REQUESTS
                );
            }
        }
        Redis::set($redisKey, time());
        try {
            $term = $request->input('term');
            return Unidade::where('codigo', 'ilike', '%'.$term.'%')
                ->select([
                    'id',
                    DB::raw("CONCAT(codigo, ' - ', nome) AS nome_completo")
                ])
                ->get()
                ->take(10)
                ->pluck('nome_completo', 'id');

            Redis::del($redisKey);
        } catch (\Exception $e) {
            Redis::del($redisKey);
            return response()->json(
                ['error' => 'Ocorreu um erro ao processar sua busca.'],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
    public function compraLeiOptions(Request $request)
    {
        $userId = backpack_user()->id;
        $redisKey = "user:{$userId}:advanced_search";
        if (Redis::exists($redisKey)) {
            $nextAvailableTime = Redis::get($redisKey) + 2;
            if (time() < $nextAvailableTime) {
                $remainingTime = $nextAvailableTime - time();
                return response()->json(
                    ['error' =>
                    "Aguarde $remainingTime segundos antes de realizar uma nova busca."],
                    Response::HTTP_TOO_MANY_REQUESTS
                );
            }
        }
        Redis::set($redisKey, time());
        try {
            $term = $request->input('term');
            return Compras::where('lei', 'ilike', '%'.$term.'%')
                ->select([
                    'lei as nome',
                    'lei as id'
                ])
                ->distinct()
                ->pluck('nome', 'id');
            Redis::del($redisKey);
        } catch (\Exception $e) {
            Redis::del($redisKey);
            return response()->json(
                ['error' => 'Ocorreu um erro ao processar sua busca.'],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);

        $this->addColumnGetUnidade(
            true,
            true,
            true,
            true,
            'unidade_origem_id',
            'Unidade Origem',
            'getUnidadeOrigem',
            'unidade_origem'
        );

        $this->addColumnGetUnidade(
            true,
            true,
            true,
            true,
            'unidade_subrrogada_id',
            'Unidade Sub-rogada',
            'getUnidadeSubrrogada',
            'unidade_subrrogada'
        );

        $this->addColumnGetUnidade(
            false,
            true,
            true,
            true,
            'uasg_beneficiaria_id',
            'Unidade Beneficiária',
            'getUnidadeBeneficiaria',
            'uasg_beneficiaria'
        );

        $this->addColumnGetCodigoItens(
            true,
            true,
            true,
            true,
            'tipo_compra_id',
            'Tipo Compra',
            'getTipoCompra',
            'tipo_compra'
        );

        $this->addColumnGetCodigoItens(
            true,
            true,
            true,
            true,
            'modalidade_id',
            'Modalidade',
            'getModalidade',
            'modalidade'
        );

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'numero_ano',
            'Número/Ano'
        );

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'inciso',
            'Inciso'
        );

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'lei',
            'Lei'
        );

        $this->addColumnCreatedAt();
        $this->addColumnUpdatedAt();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ComprasRequest::class);

        CRUD::field('id');
        CRUD::field('unidade_origem_id');
        CRUD::field('unidade_subrrogada_id');
        CRUD::field('modalidade_id');
        CRUD::field('tipo_compra_id');
        CRUD::field('numero_ano');
        CRUD::field('inciso');
        CRUD::field('lei');
        CRUD::field('artigo');
        CRUD::field('created_at');
        CRUD::field('updated_at');
        CRUD::field('deleted_at');
        CRUD::field('uasg_beneficiaria_id');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Atualizar Compras',
        ]);

        $this->setupCreateOperation();
    }
}
