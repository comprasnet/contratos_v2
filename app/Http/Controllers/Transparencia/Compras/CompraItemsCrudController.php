<?php

namespace App\Http\Controllers\Transparencia\Compras;

use App\Http\Requests\CompraItemsRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Http\Traits\Compra\CompraItemTrait;
use App\Models\Compras;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Illuminate\Support\Facades\DB;

/**
 * Class CompraItemsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CompraItemsCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use CompraItemTrait;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $compra = Compras::find(\Route::current()->parameter('compra_id'));
        $this->crud->compra_item_id = \Route::current()->parameter('id');

        CRUD::setModel(\App\Models\CompraItem::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . "/transparencia/compras/$compra->id/itens");
        CRUD::setEntityNameStrings('Visualizar itens da compra', 'Itens da compra: '. $compra->unidadeOrigem->codigo . ' - ' . $compra->modalidade->descricao . ' | ' . $compra->numero_ano);

        CRUD::addClause('select', 'compra_items.*', 'compra_item_fornecedor.ata_vigencia_inicio AS vigencia_inicio', 'compra_item_fornecedor.ata_vigencia_fim AS vigencia_final');
        CRUD::addClause('leftjoin', DB::raw('compras'), 'compras.id', '=', 'compra_items.compra_id');
        CRUD::addClause('leftjoin', DB::raw('codigoitens as tipo_item'), 'tipo_item.id', '=', 'compra_items.tipo_item_id');
        CRUD::addClause('leftjoin', DB::raw('catmatseritens'), 'catmatseritens.id', '=', 'compra_items.catmatseritem_id');
        CRUD::addClause('leftjoin', DB::raw('compra_item_fornecedor'), 'compra_items.id', '=', 'compra_item_fornecedor.compra_item_id');

        CRUD::addClause('where', 'compra_id', '=', $compra->id);
        CRUD::addClause('where', 'compra_items.situacao', '=', true);
        CRUD::orderBy('numero', 'asc');


        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
        $this->crud->addButtonFromView('top', 'voltar', 'back.compras', 'end');
    }

    protected function setupListOperation()
    {



        $this->addColumnText(
            true,
            true,
            true,
            true,
            'numero',
            'Número'
        );

        $this->addColumnGetCodigoItens(
            true,
            true,
            true,
            true,
            'tipo_item_id',
            'Tipo Item',
            'getTipoItem',
            'tipo_item',
            '25'
        );

        CRUD::addColumn([
            'name' => 'descricao_completa',
            'label' => 'Descrição',
            'type' => 'model_function',
            'limit' => 9999,
            'function_name' => 'getDescricaoCompleta',
            'visibleInTable' => true
        ]);

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'qtd_total',
            'Qtd. Total'
        );
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.contentClass', 'col-md-12');

        $this->crud->set('show.setFromDb', false);

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'numero',
            'Número'
        );

        $this->addColumnGetCodigoItens(
            true,
            true,
            true,
            true,
            'tipo_item_id',
            'Tipo Item',
            'getTipoItem',
            'tipo_item',
            '25'
        );

        $this->addColumnGetCatMatSerItem(
            true,
            true,
            true,
            true,
            'catmatseritem_id',
            'Descrição',
            'getCatMatSerItem',
            'catmatseritens',
            '250'
        );

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'descricaodetalhada',
            'Descrição detalhada',
            '9999'
        );

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'qtd_total',
            'Qtd. Total'
        );

        $this->addColumnDate(
            true,
            true,
            true,
            true,
            'vigencia_inicio',
            'Vig. Início ARP'
        );

        $this->addColumnDate(
            true,
            true,
            true,
            true,
            'vigencia_final',
            'Vig. Fim ARP'
        );

        # Cria a tabela com a lista de Unidades Participantes
        $this->addColumnTable(
            'unidades_participantes',
            'Unidades Participantes',
            $this->getUnidadesParticipantesItem($this->crud->compra_item_id),
            ['table' => false, 'modal' => false, 'show' => true, 'export' => true]
        );

        # Cria a tabela com a lista de fornecedores homologados
        $this->addColumnTable(
            'fornecedores_homologados',
            'Fornecedores Homologados',
            $this->getFornecedoresHomologados($this->crud->compra_item_id),
            ['table' => false, 'modal' => false, 'show' => true, 'export' => true]
        );

        # Cria a tabela com a lista de fornecedores homologados
        $this->addColumnTable(
            'atas_registro_preco',
            'Atas de Registro de Preços',
            $this->getAtasDoItemDeCompra($this->crud->compra_item_id),
            ['table' => false, 'modal' => false, 'show' => true, 'export' => true]
        );

        $this->addColumnCreatedAt();
        $this->addColumnUpdatedAt();
    }

    protected function setupCreateOperation()
    {
        CRUD::setValidation(CompraItemsRequest::class);

        CRUD::field('id');
        CRUD::field('compra_id');
        CRUD::field('tipo_item_id');
        CRUD::field('catmatseritem_id');
        CRUD::field('descricaodetalhada');
        CRUD::field('qtd_total');
        CRUD::field('numero');
        CRUD::field('created_at');
        CRUD::field('updated_at');
        CRUD::field('deleted_at');
        CRUD::field('ata_vigencia_inicio');
        CRUD::field('ata_vigencia_fim');
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
