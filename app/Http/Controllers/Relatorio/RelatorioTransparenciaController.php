<?php

namespace App\Http\Controllers\Relatorio;

use App\Jobs\GenerateRelatorioArpPdfJob;
use App\Models\Arp;
use App\Models\ArpItem;
use App\Models\ArpPdf;
use App\Services\RelatorioArpPdfService;
use App\Services\RelatorioPdfService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Route;
use PDF;
use App\Http\Controllers\Controller;
use Dompdf\Dompdf;
//use App\Http\Traits\Transparencia\FilterDataTrait;
use Dompdf\FontMetrics;
use App\Repositories\Relatorio\ArpPdfRepository;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use GuzzleHttp\Client;

class RelatorioTransparenciaController extends Controller
{
   // use FilterDataTrait;

    public function visualizarPDF($id, $tipo = 'visualizar')
    {
        // Verifique se o PDF foi gerado para o ARP com o ID especificado
        $pdfRecord = ArpPdf::where('arp_id', $id)->first();
        if (!$pdfRecord || !$pdfRecord->gerado) {
            // O PDF não existe ou não foi gerado,
            // redirecione ou retorne uma mensagem de erro.
            return redirect()->back()->with(
                'error',
                'O PDF ainda não foi gerado.'
            );
        }
        $pdfPath = storage_path($pdfRecord->caminho_pdf);
        if ($tipo == 'visualizar') {
            return response()->file($pdfPath);
        }

        return response()->download($pdfPath, basename($pdfPath));
    }


    public function dispararJobPDF(
        $arpId,
        ArpPdfRepository $arpItemPdfRepository,
        RelatorioArpPdfService $relatorioArpPdfService,
        Request $request
    ) {
        try {
            // Verifique se o Recaptcha está habilitado
            $habilitarRecaptcha = env('HABILITAR_RECAPTCHA');
            $recaptchaResult = ['success' => true, 'score' => null];

            if ($habilitarRecaptcha) {
                $recaptchaResult = $this->verificarRecaptcha($request);

                // Verifique se o Recaptcha foi bem-sucedido e se a pontuação é aceitável
                if (!$recaptchaResult['success'] || !$this->verificarScoreRecaptcha($recaptchaResult['score'])) {
                    $retornoErro = $this->montarRespostaAlertJs(
                        202,
                        "Falha na verificação reCAPTCHA",
                        "warning"
                    );
                    return response()->json($retornoErro, $retornoErro->code);
                }
            }

            // Se o Recaptcha estiver desativado ou foi bem-sucedido
            if ($recaptchaResult['score'] === null && $habilitarRecaptcha) {
                return response()->json(['success' => false, 'message' => 'Pontuação do reCAPTCHA inválida']);
            }

            $nomeJobRelatorio = config('arp.arp.nome_queue_relatorio');
            $pdfRecord = $arpItemPdfRepository->getItemsByBuscaHistoricoDiaPdf($arpId);
            $pdfUrl = route('visualizar.pdf', ['id' => $arpId, 'tipo' => 'download']);

            if ($pdfRecord && $pdfRecord->gerado) {
                return response()->json([
                    'success' => true,
                    'pdfUrl' => $pdfUrl,
                    'score' => $habilitarRecaptcha ? $recaptchaResult['score'] : null
                ]);
            } else {
                $jobQueue = $this->dispararJobRelatorio($arpId, $arpItemPdfRepository, $nomeJobRelatorio);
                ArpPdf::where('arp_id', $arpId)->delete();
                Bus::dispatch($jobQueue);
                return response()->json([
                    'success' => true,
                    'pdfUrl' => $pdfUrl,
                    'score' => $habilitarRecaptcha ? $recaptchaResult['score'] : null
                ]);
            }
        } catch (\Exception $e) {
            \Log::info('Erro ao Gerar o Pdf: ' . $e->getMessage());
            return response()->json(['success' => false, 'score' => 0]);
        }
    }

    private function verificarRecaptcha(Request $request)
    {
        $recaptchaSecret = env('RECAPTCHA_SECRET_KEY');
        $recaptchaResponse = $request->input('g-recaptcha-response');

        $client = new Client();
        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            [
            'form_params' => [
                'secret' => $recaptchaSecret,
                'response' => $recaptchaResponse,
            ],
            ]
        );

        $responseData = json_decode($response->getBody());
        $score = $responseData->score;

        return [
            'success' => $responseData->success,
            'score' => $score,
        ];
    }

    private function verificarScoreRecaptcha($score)
    {

        return $score > env('SCORE_LIMIT_RECAPTCHA');
    }

    private function dispararJobRelatorio(
        $arpId,
        $arpItemPdfRepository,
        $nomeJobRelatorio
    ) {
        return (
            new GenerateRelatorioArpPdfJob($arpId, $arpItemPdfRepository)
        )->onQueue($nomeJobRelatorio);
    }

    public function checkStatusPDF($arpId)
    {
        $pdfRecord = ArpPdf::where('arp_id', $arpId)->first();
        if ($pdfRecord && $pdfRecord->gerado) {
            return response()->json(['gerado' => true]);
        } else {
            return response()->json(['gerado' => false]);
        }
    }
}
