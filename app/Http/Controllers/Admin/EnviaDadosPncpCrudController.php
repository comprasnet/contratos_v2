<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EnviaDadosPncpRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Models\Arp;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * Class EnviaDadosPncpCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EnviaDadosPncpCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        if (!backpack_user()->can('admin_pncpv2_visualizar')) {
            return abort(403);
        }
        
        CRUD::setModel(\App\Models\EnviaDadosPncp::class);
        CRUD::addClause('join', 'arp', 'arp.id', '=', 'envia_dados_pncp.pncpable_id');
        CRUD::addClause('join', 'codigoitens', 'codigoitens.id', '=', 'envia_dados_pncp.situacao');
        CRUD::addClause('join', 'codigoitens AS ci_ata', 'ci_ata.id', '=', 'arp.tipo_id');
        CRUD::addClause('join', 'unidades', 'unidades.id', '=', 'arp.unidade_origem_id');
        CRUD::addClause('where', 'pncpable_type', '=', Arp::class);
        CRUD::addclause(
            "select",
            "envia_dados_pncp.id",
            DB::raw("CONCAT(arp.numero,'/',arp.ano) as numero_ata"),
            "codigoitens.descricao as situacao_pnpcp",
            "ci_ata.descricao as situacao_ata",
            "unidades.codigo as unidade_gerenciadora_ata",
        );
        
        CRUD::addClause('orderby', 'envia_dados_pncp.created_at', 'DESC');
        
        CRUD::setRoute(config('backpack.base.route_prefix') . '/admin/envia-dados-pncp');
        
        $this->exibirTituloPaginaMenu('PNCP');
        $this->bloquearBotaoPadrao($this->crud, config('security.permission_block_crud.all'));
        $this->crud->addButtonFromView(
            'line',
            'button_pncp_historico',
            'button_pncp_historico',
            'end'
        );
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('unidade_gerenciadora_ata');
        CRUD::addColumn([
            'name' => 'numero_ata',
            'label' => 'Número da ata',
            'type' => 'text',
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInShow' => true,
            'visibleInExport' => true,
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhereRaw("CONCAT(arp.numero,'/',arp.ano) ilike '%$searchTerm%' ");
            }
        ]);
        CRUD::column('numero_ata');
        CRUD::addColumn([
            'name' => 'situacao_ata',
            'label' => 'Situação ata',
            'type' => 'text',
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInShow' => true,
            'visibleInExport' => true,
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhereRaw("ci_ata.descricao ilike '%$searchTerm%'");
            }
        ]);
        
        CRUD::addColumn([
            'name' => 'situacao_pnpcp',
            'label' => 'Situação Pncp',
            'type' => 'text',
            'visibleInTable' => true,
            'visibleInModal' => true,
            'visibleInShow' => true,
            'visibleInExport' => true,
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhereRaw("codigoitens.descricao ilike '%$searchTerm%' ");
            }
        ]);
        
        $this->crud->text_button_redirect_create = 'envia dados pncp';

        $this->filtros();
    }

    private function filtros()
    {
        $this->crud->addFilter(
            [
            'type'  => 'text',
            'name'  => 'filtro_unidade',
            'label' => 'Unidade'
            ],
            false,
            function ($value) {
                 $this->crud->addClause('where', 'unidades.codigo', 'ILIKE', "%$value%");
            }
        );
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');
        CRUD::setValidation(EnviaDadosPncpRequest::class);

        CRUD::field('id');
        CRUD::field('pncpable_type');
        CRUD::field('pncpable_id');
        CRUD::field('json_enviado_inclusao');
        CRUD::field('json_enviado_alteracao');
        CRUD::field('link_pncp');
        CRUD::field('situacao');
        CRUD::field('contrato_id');
        CRUD::field('sequencialPNCP');
        CRUD::field('tipo_contrato');
        CRUD::field('linkArquivoEmpenho');
        CRUD::field('retorno_pncp');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Criar EnviaDadosPncp',
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');
        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Atualizar EnviaDadosPncp',
        ]);
        $this->setupCreateOperation();
    }
}
