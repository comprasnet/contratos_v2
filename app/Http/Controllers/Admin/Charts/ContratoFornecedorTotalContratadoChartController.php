<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Http\Traits\Formatador;
use App\Services\UsuarioFornecedor\DashboardContratoService;

/**
 * Class ArpTotalChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ContratoFornecedorTotalContratadoChartController
{
    use Formatador;

    public function setup()
    {
        $contratos = DashboardContratoService::getRepositoryByRequest();

        $contratosTotalFornecedor = $contratos->calculaTotalContratadoPorFornecedor(
            session('fornecedor_id'),
            session('tipo_acesso') == 'Administrador'
        );

        return $contratosTotalFornecedor;
    }
}
