<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Http\Traits\Formatador;
use App\Services\UsuarioFornecedor\DashboardArpFornecedorService;

/**
 * Class ArpTotalChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ContratoFornecedorTotalContratadoAtasChartController
{
    use Formatador;

    public function setup()
    {
        $atas = DashboardArpFornecedorService::getRepositoryByRequest();

        $atasTotalFornecedor = $atas->retornaTotalContratadoAtasFornecedor(
            session('fornecedor_id'),
            session('tipo_acesso') == 'Administrador'
        );
//dd($contratosTotalFornecedor, 'sss');

        return $atasTotalFornecedor;
    }
}
