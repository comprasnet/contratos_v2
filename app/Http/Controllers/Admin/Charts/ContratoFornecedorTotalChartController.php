<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Http\Traits\Formatador;
use App\Services\UsuarioFornecedor\DashboardContratoService;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

/**
 * Class ArpTotalChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ContratoFornecedorTotalChartController extends ChartController
{
    use Formatador;

    public function setup()
    {
        $this->chart = new Chart();
        $contratos = DashboardContratoService::getRepositoryByRequest();

        $contratosUg = $contratos->retornaContratosPorUnidadeChart(
            session('fornecedor_id'),
            session('tipo_acesso') == 'Administrador'
        );

        $cores = $this->colors($contratosUg->count());

        $labels = $contratosUg->pluck('unidade_codigo')->toArray();

        $quantidade = $contratosUg->pluck('num_contratos')->toArray();

        $this->chart->dataset('CONTRATO', 'pie', $quantidade)
            ->backgroundColor(
                $cores
            );

        // OPTIONAL
        $this->chart->displayAxes(false);
        $this->chart->displayLegend(true);

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($labels);
    }
}
