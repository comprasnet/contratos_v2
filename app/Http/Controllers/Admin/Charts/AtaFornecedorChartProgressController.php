<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Services\UsuarioFornecedor\DashboardArpFornecedorService;
use Carbon\Carbon;

/**
 * Class ArpTotalChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AtaFornecedorChartProgressController
{
    public function setup()
    {
        $data = $this->montarDataProgress();
        
        return ['progress' => $data];
    }
    
    private function montarDataProgress()
    {
        $atas = DashboardArpFornecedorService::getRepositoryByRequest();
        
        $tipoAcesso = session('tipo_acesso') == 'Administrador';
        
        $qtdContrato30Dias =
            $atas->buscaQtdContratoPorPeriodoVencimento30Dias(session('fornecedor_id'), $tipoAcesso);

        $qtdContrato3090Dias =
            $atas->buscaQtdContratoPorPeriodoVencimento3090Dias(session('fornecedor_id'), $tipoAcesso);

        $qtdContrato90180Dias =
            $atas->buscaQtdContratoPorPeriodoVencimento90180Dias(session('fornecedor_id'), $tipoAcesso);
        
        return $this->formatarArrayData($qtdContrato30Dias, $qtdContrato3090Dias, $qtdContrato90180Dias);
    }
    
    private function formatarArrayData(int $qtdContrato30Dias, int $qtdContrato3090Dias, int $qtdContrato90180Dias)
    {
        $hoje = Carbon::now()->toDateString();
        $dataMenos30Dias = Carbon::now()->subDays(30)->toDateString();
        $dataMais30Dias = Carbon::now()->addDays(30)->toDateString();
        $dataMais90Dias = Carbon::now()->addDays(90)->toDateString();
        $dataMais180Dias = Carbon::now()->addDays(180)->toDateString();
        
        $urlContratoPrincipal ='/fornecedor/arp?redirect_vencimento=1&vigencia_final=%7B"from"%3A"';
        
        $urlContratoMenos30 = $urlContratoPrincipal.
            $dataMenos30Dias.'+00%3A00%3A00"%2C"to"%3A"'.$hoje.'+23%3A59%3A59"%7D';
        
        $urlContrato3090 = $urlContratoPrincipal.
            $dataMais30Dias.'+00%3A00%3A00"%2C"to"%3A"'.$dataMais90Dias.'+23%3A59%3A59"%7D';
        
        $urlContrato90180 = $urlContratoPrincipal.
            $dataMais90Dias.'+00%3A00%3A00"%2C"to"%3A"'.$dataMais180Dias.'+23%3A59%3A59"%7D';
        
        return [
            [
                'type'        => 'progress',
                'class'       => 'br-message danger',
                'style' => 'width: calc(90% + 36px) ;height: calc(100px + 5px); border-radius: 20px !important;',
                'value'       => $qtdContrato30Dias,
                'description' => 'A vencer em <br> - 30 dias',
                'progress'    => $qtdContrato30Dias,
                'hint'        => "{$qtdContrato30Dias} atas",
                'redirect'    => url($urlContratoMenos30),
                'wrapperClass'    => 'mt-3  mb-2',
            ],
            [
                'type'        => 'progress',
                'class'       => 'br-message warning',
                'style' => 'width: calc(90% + 36px) ;height: calc(100px + 5px); border-radius: 20px !important;',
                'value'       => $qtdContrato3090Dias,
                'description' => 'A vencer entre <br> 30 e 90 dias',
                'progress'    => $qtdContrato3090Dias,
                'hint'        => "{$qtdContrato3090Dias} atas",
                'redirect'    => url($urlContrato3090),
                'wrapperClass'    => 'mt-3  mb-2',
            ],
            [
                'type'        => 'progress',
                'class'       => 'br-message info',
                'style' => 'width: calc(90% + 25px);height: calc(100px + 5px); border-radius: 20px !important;',
                'value'       => $qtdContrato90180Dias,
                'description' => 'A vencer entre <br> 90 e 180 dias',
                'progress'    => $qtdContrato90180Dias,
                'hint'        => "{$qtdContrato90180Dias} atas",
                'redirect'    => url($urlContrato90180),
                'wrapperClass'    => 'mt-3  mb-2',
            ]
        ];
    }
}
