<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Services\UsuarioFornecedor\DashboardContratoService;
use Carbon\Carbon;

/**
 * Class ArpTotalChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ContratoFornecedorChartProgressController
{
    public function setup()
    {
        $data = $this->montarDataProgress();
        
        return ['progress' => $data];
    }
    
    private function montarDataProgress()
    {
        $contratos = DashboardContratoService::getRepositoryByRequest();
        
        $tipoAcesso = session('tipo_acesso') == 'Administrador';
        
        $qtdContrato30Dias =
            $contratos->buscaQtdContratoPorPeriodoVencimento30Dias(session('fornecedor_id'), $tipoAcesso);
        
        $qtdContrato3090Dias =
            $contratos->buscaQtdContratoPorPeriodoVencimento3090Dias(session('fornecedor_id'), $tipoAcesso);
        
        $qtdContrato90180Dias =
            $contratos->buscaQtdContratoPorPeriodoVencimento90180Dias(session('fornecedor_id'), $tipoAcesso);
        
        return $this->formatarArrayData($qtdContrato30Dias, $qtdContrato3090Dias, $qtdContrato90180Dias);
    }
    
    private function formatarArrayData(int $qtdContrato30Dias, int $qtdContrato3090Dias, int $qtdContrato90180Dias)
    {
        $dataMenos30Dias = Carbon::now()->subDays(30)->toDateString();
        $dataMais30Dias = Carbon::now()->addDays(30)->toDateString();
        $dataMais90Dias = Carbon::now()->addDays(90)->toDateString();
        $dataMais180Dias = Carbon::now()->addDays(180)->toDateString();
        $urlContratoPrincipal =
            '/fornecedor/contrato?redirect_vencimento=1&situacao=%5B"Ativo"%5D&vigencia_fim=%7B"from"%3A"';
        
        $urlContratoMenos30 = $urlContratoPrincipal.
            '2023-01-01+00%3A00%3A00"%2C"to"%3A"'.$dataMenos30Dias.'+23%3A59%3A59"%7D';
        
        $urlContrato3090 = $urlContratoPrincipal.
            $dataMais30Dias.'+00%3A00%3A00"%2C"to"%3A"'.$dataMais90Dias.'+23%3A59%3A59"%7D';
        
        $urlContrato90180 = $urlContratoPrincipal.
            $dataMais90Dias.'+00%3A00%3A00"%2C"to"%3A"'.$dataMais180Dias.'+23%3A59%3A59"%7D';
        
        return [
            [
                'type'        => 'progress',
                'class'       => 'br-message danger',
                'style' => 'width: calc(90% + 36px) ;height: calc(100px + 5px); border-radius: 20px !important;',
                'value'       => $qtdContrato30Dias,
                'description' => 'A vencer em <br> - 30 dias',
                'wrapperClass'    => 'mt-3  mb-2',
                'hint'        => "{$qtdContrato30Dias} contratos",
                'redirect'    => url($urlContratoMenos30),
                'progress'    => $qtdContrato30Dias,
            ],
            [
                'type'        => 'progress',
                'class'       => 'br-message warning mb-2',
                'style' => 'width: calc(90% + 36px) ;height: calc(100px + 5px); border-radius: 20px !important;',
                'value'       => $qtdContrato3090Dias,
                'description' => 'A vencer entre <br> 30 e 90 dias',
                'wrapperClass'    => 'mt-3  mb-2',
                'hint'        => "{$qtdContrato3090Dias} contratos",
                'redirect'    => url($urlContrato3090),
                'progress'    => $qtdContrato3090Dias,
            ],
            [
                'type'        => 'progress',
                'class'       => 'br-message info mb-2',
                'style' => 'width: calc(90% + 25px);height: calc(100px + 5px); border-radius: 20px !important;',
                'value'       => $qtdContrato90180Dias,
                'description' => 'A vencer entre <br> 90 e 180 dias',
                'wrapperClass'    => 'mt-3  mb-2',
                'hint'        => "{$qtdContrato90180Dias} contratos",
                'redirect'    => url($urlContrato90180),
                'progress'    => $qtdContrato90180Dias,
            ]
        ];
    }
}
