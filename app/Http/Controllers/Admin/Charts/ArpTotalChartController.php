<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Http\Traits\Formatador;
use App\Models\Arp;
use App\Services\Pncp\Arp\DashboardArpService;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use Carbon\Carbon;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/**
 * Class ArpTotalChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArpTotalChartController extends ChartController
{
    use Formatador;

    public function setup()
    {
        $this->chart = new Chart();
        $arps = DashboardArpService::getRepositoryByRequest();
        $arpsServicoValue = $arps->arpsTotalServico();
        $arpsMaterialValue = $arps->arpsTotalMaterial();

        $total = $arpsServicoValue + $arpsMaterialValue;
        $percentServico = 0;
        $percentMaterial = 0;
        if($total > 0) {
            $percentServico = number_format($arpsServicoValue * 100 / $total, 2);
            $percentMaterial = number_format($arpsMaterialValue * 100 / $total, 2);
        }
        $this->chart->dataset('ARP', 'pie', [$percentServico, $percentMaterial])
            ->backgroundColor([
                'rgb(20, 81, 180)',
                'rgb(173, 205, 255)',
            ]);

        // OPTIONAL
        $this->chart->displayAxes(false);
        $this->chart->displayLegend(true);

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels(['Serviço', 'Material']);
    }
}