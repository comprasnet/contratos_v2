<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Http\Traits\Formatador;
use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use App\Models\Arp;
use App\Services\UsuarioFornecedor\DashboardArpFornecedorService;
use App\Services\UsuarioFornecedor\UsuarioFornecedorService;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use Carbon\Carbon;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/**
 * Class ArpTotalChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArpFornecedorTotalChartController extends ChartController
{
    use UsuarioFornecedorTrait;
    use Formatador;

    public function setup()
    {
        $this->chart = new Chart();
        $atas = DashboardArpFornecedorService::getRepositoryByRequest();

        $atasUg = $atas->retornaAtasPorUnidadeChart(
            session('fornecedor_id'),
            session('tipo_acesso') == 'Administrador'
        );

        $cores = $this->colors($atasUg->count());
        $labels = $atasUg->pluck('unidade_codigo')->toArray();

        $quantidade = $atasUg->pluck('num_atas')->toArray();
        //dd($cores, $labels, $quantidade);

        $this->chart->dataset('ARP', 'pie', $quantidade)
            ->backgroundColor(
                $cores
            );

        // OPTIONAL
        $this->chart->displayAxes(false);
        $this->chart->displayLegend(true);

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($labels);
    }
}
