<?php

namespace App\Http\Controllers\Admin;

use App\Models\TermoAcesso;
use Backpack\CRUD\app\Http\Controllers\CrudController;

use Backpack\CRUD\CrudPanel;

/**
 * Class TermoAcessoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class TermoAcessoCrudController extends CrudController
{
    public function aceitaTermo()
    {
        $user = backpack_auth()->user();

        $verificaTermo = TermoAcesso::where('user_id', $user->id)->first();

        if (!$verificaTermo) {
            TermoAcesso::create(['user_id' => $user->id]);
        }
        
        session(['termoAcesso' => true]);


        return redirect(session('url.intended', '/'));
    }


    public function termoAceite()
    {
        return session('termoAcesso') ? redirect()->back() : view('backpack::termo_acesso.termo_aceite');
    }
}
