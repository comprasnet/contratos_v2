<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EnviaDadosPncpHistoricoRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Http\Traits\Pncp\CommonPncpTrait;
use App\Models\Arp;
use App\Models\ArpArquivo;
use App\Models\EnviaDadosPncpHistorico;
use App\Models\Orgao;
use App\Services\Pncp\Arp\InserirArquivoArpPncpService;
use App\Services\Pncp\PncpManagerService;
use App\Services\Pncp\Responses\PncpPostResponseService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use function config;
use App\Services\Pncp\Responses\PncpResponseService;
use Exception;

/**
 * Class EnviaDadosPncpHistoricoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EnviaDadosPncpHistoricoCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    use CommonPncpTrait;

    /**
     * @var PncpManagerService
     */
    private $pncpManagerService;

    public function __construct(PncpManagerService $pncpManagerService)
    {
        parent::__construct();
        $this->pncpManagerService = $pncpManagerService;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        if (!backpack_user()->can('admin_pncpv2_visualizar')) {
            return abort(403);
        }
        $id = request()->route('envia_dados_pncp_id');
        CRUD::setModel(\App\Models\EnviaDadosPncpHistorico::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . "/admin/envia-dados-pncp-historico/{$id}");
        CRUD::addClause('whereNotNull', 'status_code');
        CRUD::setEntityNameStrings('Admin PNCP', 'Admin PNCP');

        #clauses
        CRUD::addclause('withTrashed');
        CRUD::addclause(
            "select",
            "envia_dados_pncp_historico.*",
            DB::raw(
                "CASE 
            WHEN envia_dados_pncp_historico.pncpable_type = '" . Arp::class . "' THEN CONCAT(arp.numero, '/', arp.ano) 
            WHEN envia_dados_pncp_historico.pncpable_type = '" . ArpArquivo::class . "' THEN arp_arquivos.descricao 
            WHEN envia_dados_pncp_historico.pncpable_type = '" . Orgao::class . "' THEN orgaos.nome 
            ELSE NULL 
            END AS identificacao"
            ),
            DB::raw(
                "CASE 
            WHEN envia_dados_pncp_historico.pncpable_type = '" . Arp::class . "' THEN arp_unidade.codigo 
            WHEN envia_dados_pncp_historico.pncpable_type = '" . ArpArquivo::class . "' THEN arp_arquivo_unidade.codigo 
            WHEN envia_dados_pncp_historico.pncpable_type = '" . Orgao::class . "' THEN orgao_unidade.codigo 
            ELSE NULL 
            END AS unidade_identificacao"
            )
        );
        CRUD::addClause('leftjoin', 'arp', 'arp.id', '=', 'envia_dados_pncp_historico.pncpable_id');
        CRUD::addClause('leftjoin', 'arp_arquivos', 'arp_arquivos.id', '=', 'envia_dados_pncp_historico.pncpable_id');


        CRUD::addClause('leftjoin', 'arp as arp_para_arquivo', 'arp_para_arquivo.id', '=', 'arp_arquivos.arp_id');


        CRUD::addClause('leftjoin', 'orgaos', 'orgaos.id', '=', 'envia_dados_pncp_historico.pncpable_id');
        CRUD::addClause('leftjoin', 'unidades as arp_unidade', 'arp_unidade.id', '=', 'arp.unidade_origem_id');
        CRUD::addClause(
            'leftjoin',
            'unidades as arp_arquivo_unidade',
            'arp_arquivo_unidade.id',
            '=',
            'arp_para_arquivo.unidade_origem_id'
        );
        CRUD::addClause('leftjoin', 'unidades as orgao_unidade', 'orgao_unidade.id', '=', 'orgaos.id');
        
        CRUD::addClause('where', 'envia_dados_pncp_id', '=', $id);

        $this->exibirTituloPaginaMenu('Histórico PNCP');
        $this->bloquearBotaoPadrao($this->crud, config('security.permission_block_crud.all'));
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::enableDetailsRow();

        CRUD::addColumn([
            'name' => 'identificacao',
            'type' => 'text',
            'label' => 'Identificação',
            'visibleInTable' => true,
            'searchLogic' => function (Builder $query, $column, $searchTerm) {
                $query->where(
                    DB::raw(
                        "CASE WHEN envia_dados_pncp_historico.pncpable_type ='" .Arp::class. "'" .
                        "THEN CONCAT(arp.numero, '/', arp.ano) " .
                        "ELSE arp_arquivos.descricao END"
                    ),
                    'iLike',
                    '%' . $searchTerm . '%'
                );
            }
        ]);

        CRUD::addColumn([
            'name' => 'unidade_identificacao',
            'type' => 'text',
            'label' => 'Unidade',
            'visibleInTable' => true,
        ]);

        $this->addColumnModelFunction(
            'pncpable_type',
            'Tipo',
            'getTipoToListView',
        );

        CRUD::addColumn([
            'name' => 'log_name',
            'label' => 'Ação enviada',
            'visibleInTable' => true
        ]);

        CRUD::column('status_code');

        CRUD::addColumn([
            'name' => 'created_at',
            'format' => 'DD/MM/Y H:mm:ss',
            'type' => 'datetime',
            'label' => 'Data de envio',
            'visibleInTable' => true,
            'searchLogic' => function ($query, $column, $searchTerm) {
            },
        ]);

        $this->addFilters();

        $this->addButtonsInLine();

        
        $this->crud->text_button_redirect_create = 'envia dados pncp historico';
        
        $this->data['breadcrumbs'] = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Pncp' =>  backpack_url('admin/envia-dados-pncp'),
            "Histórico do Pncp" => false,
        ];
    }

    public function addButtonsInLine()
    {
        if (backpack_user()->hasRole('Administrador') || backpack_user()->can('admin_pncpv2_reenviar')) {
            $this->crud->addButtonFromView(
                'line',
                'reenviar_instrumento_para_pncp',
                'reenviar_instrumento_para_pncp',
                'end'
            );
        }
    }

    public function addFilters()
    {
        $this->crud->addFilter(
            [
                'type' => 'dropdown_pncp_filter',
                'name' => 'pncpable_type',
                'label' => 'Tipo'
            ],
            [
                0 => 'Arp',
                1 => 'ArpArquivo',
                2 => 'Todos',
            ],
            function ($value) {
                switch ($value) {
                    case 0:
                        $this->crud->addClause('where', 'pncpable_type', Arp::class);
                        break;
                    case 1:
                        $this->crud->addClause('where', 'pncpable_type', ArpArquivo::class);
                        break;
                    default:
                        return;
                }
            }
        );

        $this->crud->addFilter(
            [
                'type' => 'dropdown_pncp_filter',
                'name' => 'status_return',
                'label' => 'Status do Envio'
            ],
            [
                0 => 'Retorno com sucesso',
                1 => 'Retorno com Erro',
                2 => 'Todos',
            ],
            function ($value) {
                switch ($value) {
                    case 0:
                        $this->crud->addClause('whereIn', 'status_code', PncpResponseService::$successCode);
                        break;
                    case 1:
                        $this->crud->addClause('whereIn', 'status_code', PncpResponseService::$errorCode);
                        break;
                    default:
                        return;
                }
            }
        );

        $this->crud->addFilter(
            [
                'type' => 'dropdown_pncp_filter',
                'name' => 'log_name',
                'label' => 'Ação enviada'
            ],
            $this->getArrLoNameOptions(),
            function ($value) {
                if ($value !== 'Todos') {
                    $this->crud->addClause('where', 'log_name', $value);
                }
            }
        );

        $this->crud->addFilter(
            [
                'type' => 'dropdown_pncp_filter',
                'name' => 'status_code_return',
                'label' => 'Status Code'
            ],
            #mergeia os arrays com todos status code de PncpResponseService
            $this->getArrStatusCodeOptions(),
            function ($value) {
                if ($value !== 'Todos') {
                    $this->crud->addClause('where', 'status_code', $value);
                }
            }
        );
    }

    public function getArrLoNameOptions()
    {
        $arrWithKeys = array_keys(array_merge(config('api.pncp.endpoints'), ['Todos' => 'Todos']));
        #remove a chave todas_astas pois n é preciso no filtro
        $arrWithKeys = Arr::where($arrWithKeys, function ($value, $key) {
            return $value !== 'todas_atas';
        });

        return array_combine($arrWithKeys, $arrWithKeys);
    }

    public function getArrStatusCodeOptions()
    {
        $arrWithKeys = array_merge(PncpResponseService::$successCode, PncpResponseService::$errorCode, ['Todos']);
        return array_combine($arrWithKeys, $arrWithKeys);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');
        CRUD::setValidation(EnviaDadosPncpHistoricoRequest::class);

        CRUD::field('id');
        CRUD::field('envia_dados_pncp_id');
        CRUD::field('pncpable_type');
        CRUD::field('pncpable_id');
        CRUD::field('log_name');
        CRUD::field('status_code');
        CRUD::field('json_enviado_inclusao');
        CRUD::field('json_enviado_alteracao');
        CRUD::field('link_pncp');
        CRUD::field('situacao');
        CRUD::field('contrato_id');
        CRUD::field('sequencialPNCP');
        CRUD::field('tipo_contrato');
        CRUD::field('linkArquivoEmpenho');
        CRUD::field('retorno_pncp');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Criar EnviaDadosPncpHistorico',
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');
        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Atualizar EnviaDadosPncpHistorico',
        ]);
        $this->setupCreateOperation();
    }

    public function reSendToPNCP($enviarPncpHistoricoId)
    {
        #recupera entity por id
        $enviaPncpHistorico = EnviaDadosPncpHistorico::where('id', $enviarPncpHistoricoId)->first();

        #se não existir a entity então redireciona
        if (!$enviaPncpHistorico) {
            \Alert::error("Instrumento não existe")->flash();
            return redirect()->back();
        }
        #recupera request
        $requestToPncp =    $this->resolveRequestPncpByLogName($enviaPncpHistorico->log_name);
        #recupera response
        $responseToPncp =   $this->resolveResponsePncpByLogName($enviaPncpHistorico->log_name);
        #reenvia pra pncp
        try {
            $novoEnviaDadosPncpHistorico = $this->pncpManagerService->managePncp(
                $requestToPncp,
                $responseToPncp,
                $enviaPncpHistorico->enviaDadosPncp->pncpable
            );

            #exibe mensagem de retorno
            $this->showMessageResponse($novoEnviaDadosPncpHistorico, 'Instrumento enviado para o PNCP');
        } catch (Exception $e) {
            $this->showMessageResponse($enviaPncpHistorico, 'Erro ao enviar instrumento para o PNCP');
            Log::error('Erro ao reenviar requisição usando a tela do PNCP');
            Log::error($enviaPncpHistorico);
            Log::error($e);
            return redirect()->back();
        }

        $this->sendDocumentoToPncp($novoEnviaDadosPncpHistorico);

        return redirect()->back();
    }

    public function sendDocumentoToPncp(EnviaDadosPncpHistorico $novoEnviaDadosPncpHistorico)
    {
        $statusCode = (int)$novoEnviaDadosPncpHistorico->status_code;

        #caso tenha sucesso no envio
        if (in_array($statusCode, PncpResponseService::$successCode)) {
            #caso seja ata e acao enviada seja inserir_ata entao envia o arquivo da ata para o PNCP
            if ($novoEnviaDadosPncpHistorico->pncpable_type === Arp::class &&
                $novoEnviaDadosPncpHistorico->log_name === 'inserir_ata'
            ) {
                $arquivoAta = ArpArquivo::where('arp_id', $novoEnviaDadosPncpHistorico->pncpable_id)->get()->first();

                try {
                    $novoDocumentoEnviaDadosPncpHistorico = $this->pncpManagerService->managePncp(
                        new InserirArquivoArpPncpService(),
                        new PncpPostResponseService(),
                        $arquivoAta
                    );
                    #exibe mensagem de retorno
                    $this->showMessageResponse(
                        $novoDocumentoEnviaDadosPncpHistorico,
                        'Instrumento enviado para o PNCP'
                    );
                } catch (Exception $e) {
                    \Alert::error("Erro ao enviar documento parao PNCP")->flash();
                }
            }
        }
    }

    public function showMessageResponse(EnviaDadosPncpHistorico $enviaPncpHistorico, $message)
    {

        $statusCode = (int) $enviaPncpHistorico->status_code;

        if (in_array($statusCode, PncpResponseService::$successCode)) {
            \Alert::success($message)->flash();
        }

        if (in_array($statusCode, PncpResponseService::$errorCode)) {
            \Alert::warning($message)->flash();
        }
    }

    public function showDetailsRow($id)
    {
        $this->crud->hasAccessOrFail('list');

        $id = $this->crud->getCurrentEntryId() ?? $id;

        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;

        return view('crud::details_row.detail_row_envia_dados_pncp_historico', $this->data);
    }
}
