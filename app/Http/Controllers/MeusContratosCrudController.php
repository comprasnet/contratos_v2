<?php

namespace App\Http\Controllers;

use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CommonColumns;
use App\Models\AmparoLegal;
use App\Models\Contrato;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Library\Widget;
use Illuminate\Support\Facades\Config;

class MeusContratosCrudController extends CrudController
{
    use BuscaCodigoItens;
    use ListOperation;
    use CommonColumns;


    public function setup()
    {
        if (!backpack_user()->can('fiscalizacao_V2_acessar')) {
            return abort(403);
        }

        CRUD::setRoute(config('backpack.base.route_prefix') . '/meus-contratos');

        CRUD::setModel(Contrato::class);

        CRUD::addClause('select', 'contratos.*');
        CRUD::addClause('join', 'fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id');
        CRUD::addClause('join', 'unidades', 'unidades.id', '=', 'contratos.unidade_id');
        CRUD::addClause('whereHas', 'responsaveis', function ($query) {
            $query->whereHas('user', function ($query) {
                $query->where('id', '=', backpack_user()->id);
            })->where('situacao', '=', true);
        });
        CRUD::addClause('whereHas', 'historico', function ($query) {
            $query->whereHas('saldosItens', function ($query) {
                $query->whereHas('contratoItem', function ($query) {
                    $query->where('elaboracao', false);
                });
            });
        });


        CRUD::setEntityNameStrings('Lista Meus Contratos', 'Fiscalização e Gestão de Contratos');

        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');

        $this->crud->allowAccess('update');
        $this->crud->denyAccess([
            'create',
            'delete',
        ]);
    }

    protected function setupListOperation()
    {
        Widget::add()->to('before_content')->type('div')->class('row')->content([
            Widget::make(
                [
                    'type'         => 'subtitle_hint',
                    'subtitle'      => 'Meus Contratos',
                    'hint'      => 'Lista dos contratos em que o usuário logado figura como responsável. 
                        Caso não os reconheça, procure o Administrador da Unidade para atualização',
                ]
            ),
        ]);

        $this->filtros();

        $this->crud->addButtonFromView(
            'line',
            'button_autorizacaoexecucao',
            'button_autorizacaoexecucao',
            'beginning'
        );
        $this->crud->addButtonFromView(
            'line',
            'button_declaracaoopm',
            'button_declaracaoopm',
            'end'
        );

        $this->crud->addButtonFromView(
            'line',
            'show_in_v1',
            'show_in_v1',
            'end'
        );

        $this->crud->addButtonFromView(
            'line',
            'arp_link_pncp',
            'arp_link_pncp',
            'end'
        );

        $this->crud->addButtonFromView(
            'line',
            'more.meus_contratos',
            'more.meus_contratos',
            'end'
        );

        $this->crud->addColumn([
            'name' => 'receita_despesa',
            'label' => 'Receita / Despesa',
            'type' => 'model_function_raw',
            'function_name' => 'getReceitaDespesa',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'numero',
            'label' => 'Número do instrumento',
            'type' => 'text',
        ]);

        $this->crud->addColumn([
            'name' => 'codigo_sistema_externo',
            'label' => 'Código Sistema Externo',
            'type' => 'text',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'unidade_origem',
            'label' => 'Unidade gestora origem do contrato',
            'type' => 'model_function_raw',
            'function_name' => 'getUnidadeOrigem',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'unidade',
            'label' => 'Unidade gestora',
            'type' => 'model_function_raw',
            'function_name' => 'getUnidade',
        ]);

        $this->crud->addColumn([
            'name' => 'unidades_requisitantes',
            'label' => 'Unidades requisitantes',
            'type' => 'text',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'tipo',
            'label' => 'Tipo',
            'type' => 'model_function_raw',
            'function_name' => 'getTipo',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'subtipo',
            'label' => 'Subtipo',
            'type' => 'text',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'categoria',
            'label' => 'Categoria',
            'type' => 'model_function_raw',
            'function_name' => 'getCategoria',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'subcategoria',
            'label' => 'Subcategoria',
            'type' => 'model_function_raw',
            'function_name' => 'getSubCategoria',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'fornecedor',
            'label' => 'Fornecedor',
            'type' => 'model_function_raw',
            'function_name' => 'getFornecedor',
            'searchLogic' => function ($query, $column, $searchTerm) {
                if (!empty($searchTerm)) {
                    $query->orWhere('fornecedores.cpf_cnpj_idgener', 'LIKE', '%' . strtoupper($searchTerm) . '%')
                        ->orWhere('fornecedores.nome', 'LIKE', '%' . strtoupper($searchTerm) . '%');
                }
            }
        ]);

        $this->crud->addColumn([
            'name' => 'processo',
            'label' => 'Processo',
            'type' => 'text',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'objeto',
            'label' => 'Objeto',
            'type' => 'text',
            'limit' => 1000,
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'data_proposta_comercial',
            'label' => 'Data proposta comercial',
            'type' => 'date',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'info_complementar',
            'label' => 'Informações complementares',
            'type' => 'text',
            'limit' => 1000,
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'vigencia_inicio',
            'label' => 'Vig. início',
            'type' => 'date',
            'priority' => 1,
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'vigencia_fim',
            'label' => 'Vig. fim',
            'type' => 'date',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'valor_global',
            'label' => 'Valor global',
            'type' => 'model_function_raw',
            'function_name' => 'formatVlrGlobal',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'num_parcelas',
            'label' => 'Nª parcelas',
            'type' => 'text',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'valor_parcela',
            'label' => 'Valor parcela',
            'type' => 'model_function_raw',
            'function_name' => 'formatVlrParcela',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'valor_acumulado',
            'label' => 'Valor acumulado',
            'type' => 'model_function_raw',
            'function_name' => 'formatVlrAcumulado',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'total_despesas_acessorias',
            'label' => 'Total despesas acessórias',
            'type' => 'model_function_raw',
            'function_name' => 'formatTotalDespesasAcessorias',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'situacao',
            'label' => 'Situação',
            'type' => 'model_function_raw',
            'function_name' => 'getSituacao',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'prorrogavel_coluna',
            'label' => 'Prorrogável',
            'type' => 'text',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'modalidade',
            'label' => 'Modalidade da compra',
            'type' => 'model_function_raw',
            'function_name' => 'getModalidade',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'amparo_legal',
            'label' => 'Amparo legal',
            'type' => 'model_function_raw',
            'function_name' => 'retornaAmparo',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'numero_compra',
            'label' => 'Número da compra',
            'type' => 'text',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'unidadecompra',
            'label' => 'Unidade da compra',
            'type' => 'model_function_raw',
            'function_name' => 'getUnidadeCompra',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'created_at',
            'label' => 'Criado em',
            'type' => 'datetime',
            'visibleInTable' => false,
        ]);

        $this->crud->addColumn([
            'name' => 'updated_at',
            'label' => 'Atualizado em',
            'type' => 'datetime',
            'visibleInTable' => false,
        ]);

        $this->crud->enableExportButtons();
    }

    private function filtros()
    {
        $this->crud->addFilter(
            [
                'name' => 'receita_despesa',
                'label' => 'Receita / Despesa',
                'type' => 'select2'
            ],
            ['R' => 'Receita', 'D' => 'Despesa'],
            function ($value) {
                $this->crud->addClause('where', 'receita_despesa', $value);
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'tipo',
                'label' => 'Tipo',
                'type' => 'select2_multiple'
            ],
            $this->retornaArrayCodigosItens('Tipo de Contrato'),
            function ($value) {
                $this->crud->addClause('whereIn', 'contratos.tipo_id', json_decode($value));
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'categoria',
                'label' => 'Categoria',
                'type' => 'select2_multiple'
            ],
            $this->retornaArrayCodigosItens('Categoria Contrato'),
            function ($value) {
                $this->crud->addClause('whereIn', 'contratos.categoria_id', json_decode($value));
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'vigencia_inicio',
                'label' => 'Vig. início',
                'type' => 'date',
                'language' => 'pt-BR'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'vigencia_inicio', '>=', $value);
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'vigencia_fim',
                'label' => 'Vig. fim',
                'type' => 'date',
                'language' => 'pt-BR'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'vigencia_fim', '<=', $value);
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'valor_global',
                'type' => 'range',
                'label' => 'Valor Global',
                'label_from' => 'Vlr Mínimo',
                'label_to' => 'Vlr Máximo'
            ],
            false,
            function ($value) {
                $range = json_decode($value);

                if ($range->from) {
                    $this->crud->addClause('where', 'contratos.valor_global', '>=', (float)$range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'contratos.valor_global', '<=', (float)$range->to);
                }
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'valor_parcela',
                'type' => 'range',
                'label' => 'Valor Parcela',
                'label_from' => 'Vlr Mínimo',
                'label_to' => 'Vlr Máximo'
            ],
            false,
            function ($value) {
                $range = json_decode($value);

                if ($range->from) {
                    $this->crud->addClause('where', 'contratos.valor_parcela', '>=', (float)$range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'contratos.valor_parcela', '<=', (float)$range->to);
                }
            }
        );


        $this->crud->addFilter(
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'select2_multiple',
            ],
            [
                '1' => 'Ativo',
                'Inativo' => 'Inativo',
                'Rescindido' => 'Rescindido',
                'Encerrado' => 'Encerrado'
            ],
            function ($value) {
                #895

                $ativoInativo = str_replace(['[', '"', ']', 'Rescindido', 'Encerrado'], "", $value);
                $statusAtivoInativo = str_replace("Inativo", "0", $ativoInativo);
                $statusGerais = str_replace(['[', '"', ']', '1', 'Inativo'], "", $value);

                $arrayAtivoInativo = array_filter(explode(",", $statusAtivoInativo), function ($valor) {
                    return $valor !== "";
                });

                $arrayAtivoInativoEncode = json_encode($arrayAtivoInativo);
                $situacoesFiltradas = array_filter(explode(",", $statusGerais), function ($valor) {
                    return $valor !== "";
                });

                //Verifica se possui apenas o status inativo dentro do array e nenhuma outra situação
                if (count($arrayAtivoInativo) === 1 &&
                    $arrayAtivoInativo[0] == "0" &&
                    count($situacoesFiltradas) === 0
                ) {
                    $this->crud->addClause(
                        'whereIn',
                        'contratos.situacao',
                        json_decode($arrayAtivoInativoEncode)
                    );
                }
                //Verifica se possui apenas o status ativo dentro do array e nenhuma outra situação
                if (count($arrayAtivoInativo) === 1 &&
                    $arrayAtivoInativo[0] == "1" &&
                    count($situacoesFiltradas) === 0
                ) {
                    $this->crud->addClause(
                        'whereIn',
                        'contratos.situacao',
                        json_decode($arrayAtivoInativoEncode)
                    );
                }

                $this->crud->addClause(
                    'leftjoin',
                    'codigoitens as justificativa',
                    'justificativa.id',
                    '=',
                    'contratos.justificativa_contrato_inativo_id',
                );

                $situacoesFiltradasDecode = json_decode(json_encode($situacoesFiltradas), true);
                if ($situacoesFiltradasDecode) {
                    $this->crud->addClause(
                        'where',
                        function ($query) use (
                            $arrayAtivoInativoEncode,
                            $situacoesFiltradasDecode,
                            $arrayAtivoInativo
                        ) {
                            $query->orWhereIn('justificativa.descricao', $situacoesFiltradasDecode);
                            if (count($arrayAtivoInativo) > 0) {
                                $query->whereIn('contratos.situacao', json_decode($arrayAtivoInativoEncode));
                            }
                        }
                    );
                }
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'amparo_legal',
                'label' => 'Amparo legal',
                'type' => 'select2_ajax',
                'placeholder' => 'Selecione a Amparo Legal',
                'select_attribute' => 'ato_normativo',
                'select_key' => 'ato_normativo',
                'method'      => 'GET',
            ],
            url('amparo-legal/searchByAtoNormativo'),
            function ($value) {
                $options = AmparoLegal::select('amparo_legal_contrato.contrato_id')
                    ->join('amparo_legal_contrato', 'amparo_legal_contrato.amparo_legal_id', 'amparo_legal.id')
                    ->join('contratos', 'contratos.id', 'amparo_legal_contrato.contrato_id')
                    ->where('amparo_legal.ato_normativo', 'ilike', '%' . strtoupper($value) . '%')
                    ->groupby('amparo_legal_contrato.contrato_id')
                    ->get()
                    ->toArray();

                $this->crud->addClause('whereIn', 'contratos.id', $options);
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'modalidade',
                'label' => 'Modalidade da compra',
                'type' => 'select2_multiple',
            ],
            $this->retornaArrayCodigosItens('Modalidade Licitação'),
            function ($value) {
                $this->crud->addClause('whereIn', 'contratos.modalidade_id', json_decode($value));
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'numero_compra',
                'label' => 'Número da compra',
                'type' => 'text',
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'numero_compra', 'LIKE', "%$value%");
            }
        );


        $this->crud->addFilter(
            [
                'name' => 'unidadecompra',
                'label' => 'Unidade da compra',
                'type' => 'select2_ajax',
                'placeholder' => 'Selecione a Unidade da Compra',
                'select_attribute' => 'unidade_codigo_nomeresumido',
                'select_key' => 'id',
                'method'      => 'GET',
            ],
            url('/unidadeuasg'),
            function ($value) {
                $this->crud->addClause('where', 'unidadecompra_id', $value);
            }
        );
    }
}
