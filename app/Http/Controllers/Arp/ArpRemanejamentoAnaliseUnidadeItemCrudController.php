<?php

namespace App\Http\Controllers\Arp;

use App\Http\Requests\ArpRemanejamentoAnaliseUnidadeItemRequest;
use App\Http\Traits\Arp\ArpRemanejamentoTrait;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\CommonFields;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Http\Traits\LogTrait;
use App\Models\Arp;
use App\Models\ArpRemanejamento;
use App\Models\ArpRemanejamentoItem;
use App\Models\BackpackUser;
use App\Models\CodigoItem;
use App\Models\Compras;
use App\Repositories\Arp\ArpRemanejamentoItemRepository;
use App\Repositories\Arp\ArpRemanejamentoRepository;
use App\Repositories\CodigoItemRepository;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use DataTables;

/**
 * Class ArpRemanejamentoAnaliseUnidadeItemCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArpRemanejamentoAnaliseUnidadeItemCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    use ArpRemanejamentoTrait;
    use CommonFields;
    use ImportContent;
    use LogTrait;
    use BuscaCodigoItens;
    
    # Status da análise da Controller
    private $descricaoStatusAnalise = 'Aguardando aceitação da unidade participante';
    
    /**
     * Método responsável em as linhas do datatable
     */
    private function montarInformacaoDatatable(Collection $remanejamento)
    {
        $itemDatatable = array();
        foreach ($remanejamento as $key => $item) {
            $itemDatatable[$key]['situacao_remanejamento'] = $item->getSituacao();
            $itemDatatable[$key]['data_envio_solicitacao'] = $this->formatDatePtBR($item->data_envio_solicitacao);
            $itemDatatable[$key]['numero_solicitacao'] = $item->getNumeroSolicitacao();
            $itemDatatable[$key]['numero_ata'] = $item->getNumeroAta();
            $itemDatatable[$key]['numero_compra'] = $item->getNumeroCompra();
            $itemDatatable[$key]['modalidade_compra'] = $item->getModalidadeCompra();
            $itemDatatable[$key]['unidade_destino'] = $item->getUnidadeDestino();
            $itemDatatable[$key]['item_faltando_aprovar'] = $item->item_faltando_aprovar;
            $itemDatatable[$key]['id'] = $item->id;
        }
        
        return $itemDatatable;
    }
    
    public function remanejamentoGerenciadoraAta()
    {
        
        # Responsável em retornar os registros
        $remanejamento =
            $this->montarSQLAnaliseRemanejamento(
                $this->crud->model,
                $this->descricaoStatusAnalise,
                session('user_ug_id'),
                true
            );

        # Recuperar os remanejamentos que estão na situação Aguardando Aceitação da Unidade de Origem
        $itemDatatable = $this->montarInformacaoDatatable($remanejamento);

        
        return Datatables::of($itemDatatable)
            ->addIndexColumn()
            ->make(true);
    }
    
    public function remanejamentoGerenciadoraDonaItem()
    {
        # Responsável em retornar os registros
        $remanejamento =
            $this->montarSQLAnaliseRemanejamento(
                $this->crud->model,
                'Aguardando aceitação da unidade gerenciadora',
                session('user_ug_id')
            );
        
        # Recuperar os remanejamentos que estão na situação Aguardando Aceitação da Unidade de Origem
        $itemDatatable = $this->montarInformacaoDatatable($remanejamento);
        
        return Datatables::of($itemDatatable)
            ->addIndexColumn()
            ->make(true);
    }
    
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ArpRemanejamento::class);
        
        CRUD::setRoute(config('backpack.base.route_prefix') . '/arp/remanejamento/analisar');
        
        $this->exibirTituloPaginaMenu('Analisar solicitação de remanejamento');
        $this->bloquearBotaoPadrao($this->crud, ['create', 'delete']);
        $this->crud->enableExportButtons();
    }
    
    /**
     * Método responsável em montar as colunas necessárias do datatable
     */
    private function mountColumnDatatable()
    {
        $arrayColumn = array();
        
        $arrayColumn['data'] = [
            'situacao_remanejamento',
            'data_envio_solicitacao',
            'numero_solicitacao',
            'numero_ata',
            'numero_compra',
            'modalidade_compra',
            'unidade_destino'
        ];
        
        $arrayColumn['table_title'] = [
            'Situação',
            'Data do envio',
            'Nº da solicitação',
            'Nº da ata',
            'Nº da compra',
            'Modalidade',
            'Unidade destino'
        ];
        
        return $arrayColumn;
    }
    
    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->setListView('vendor.backpack.crud.arp.list');
        
        # Montar as colunas do datatable
        $colunasDatatable = $this->mountColumnDatatable();
        
        # Array para criar as tabelas na blade customizada
        $this->crud->one_or_more_table =
            [
                'quantidade_tabela' => 2,
                'id_table' =>
                    [
                        'tabela_gerenciadora_item',
                        'tabela_gerenciadora_ata'
                    ],
                'tab' =>
                    [
                        'Unidade participante',
                        'Unidade gerenciadora da ata'
                    ],
                'column' =>
                    [
                        $colunasDatatable,
                        $colunasDatatable
                    ],
                'route_tab' =>
                    [
                        config('backpack.base.route_prefix') . '/arp/remanejamento/analisar/gerenciadoradonaitem',
                        config('backpack.base.route_prefix') .
                        '/arp/remanejamento/analisar/remanejamentogerenciadoraata'
                    ],
                'route_custom_button' =>
                    [
                        null,
                        config('backpack.base.route_prefix') . '/arp/remanejamento/analisar/gerenciadoraata'
                    ],
                'permission_edit' =>
                    [
                        [
                            'Aguardando aceitação da unidade participante',
                            'Aguardando aceitação',
                            0
                        ],
                        [
                            'Aguardando aceitação da unidade gerenciadora',
                            'Aguardando aceitação',
                            0
                        ]
                    ]
            ];
        
        $this->crud->removeButton('update');
        
        $this->crud->addButtonFromView('line', 'button_edit_rascunho', 'button_edit_rascunho', 'end');
    }
    
    protected function setupShowOperation()
    {
        $this->crud->set('show.contentClass', 'col-md-12');
        
        $this->crud->set('show.setFromDb', false);
        
        # Exibe os campos para a listagem e show
        $this->camposShow();
        
        # Exibir o anexo e os itens, igual ao show na solicitação do remanejamento
        $this->exibirAnexoItemRemanejamento($this->crud);

        $remanejamento = $this->crud->getEntry($this->crud->getCurrentEntryId());
        
        $arrayBloqueioEdicao =
            ['Aguardando aceitação da unidade gerenciadora', 'Cancelada', 'Analisado pela unidade gerenciadora da ata'];
        if (in_array($remanejamento->getSituacao(), $arrayBloqueioEdicao)) {
            $this->crud->removeButton('update');
        }
    }
    
    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    // protected function setupCreateOperation()
    // {
    //     $this->crud->setCreateContentClass('col-md-12');
    //     CRUD::setValidation(ArpRemanejamentoAnaliseRequest::class);
    
    //     CRUD::setFromDb(); // fields
    
    //     /**
    //      * Fields can be defined using the fluent syntax or array syntax:
    //      * - CRUD::field('price')->type('number');
    //      * - CRUD::addField(['name' => 'price', 'type' => 'number']));
    //      */
    
    //     $this->crud->replaceSaveActions([
    //         'name' => 'save_action_one',
    //         'button_text' => 'Criar ArpRemanejamentoAnalise',
    //     ]);
    // }
    
    /**
     * Método responsável em montar o texto para o tooltip
     */
    // private function montarTextoTooltip(array $dadosItem, string $nomeInformacao)
    // {
    //     return "Informações sobre {$nomeInformacao} do item {$dadosItem['numero_item']} e
    //         unidade {$dadosItem['unidade_origem']}";
    // }
    
    /**
     * Método responsável em exibir a informação da na coluna Quantidade autorizada unidade origem
     */
    private function montarInformacaoCampoQuantidadeAutorizadaUnidadeOrigem(ArpRemanejamentoItem $item)
    {
        # Quando o item não foi analisado então o usuário pode lançar o valor
        if ($item->situacaoItem->descricao == $this->descricaoStatusAnalise) {
            $quantidadeAnalisadaUnidadeOrigem = $item->quantidade_solicitada;
            
            # Se o usuário inserir a quantidade para o item da unidade origem da ata
            if (!empty($item->quantidade_aprovada_participante)) {
                $quantidadeAnalisadaUnidadeOrigem = $item->quantidade_aprovada_participante;
            }
            
            if ($this->existeSaldoUnidadeParaRemanejar(
                $item->itemUnidade,
                $quantidadeAnalisadaUnidadeOrigem
            ) == false) {
                return 'Não existe saldo da unidade para remanejar';
            }
            
            # Recupera o modo que o usuário pode digitar a quantidade no campo
            $step = $this->retornarStepCampoQuantidade($item->itemUnidade->compraItens->tipoItem);
            
            $quantidadeMaximaAutorizada = $item->quantidade_solicitada;
            $quantidadoSaldoUnidade = $item->itemUnidade->quantidade_saldo;
            
            if ($quantidadoSaldoUnidade < $quantidadeMaximaAutorizada) {
                $quantidadeMaximaAutorizada = $quantidadoSaldoUnidade;
            }
            
            return $this->montarCampoHtmlQuantidade(
                $item->id,
                $quantidadeMaximaAutorizada,
                $step,
                $quantidadeAnalisadaUnidadeOrigem
            );
        }
        
        if ($this->existeSaldoUnidadeParaRemanejar(
            $item->itemUnidade,
            $item->quantidade_aprovada_participante
        ) == false
        ) {
            return 'Não existe saldo da unidade para remanejar';
        }
        
        return $this->formatValuePtBR($item->quantidade_aprovada_participante, 5);
    }
    
    /**
     * Método responsável em exibir os itens para o usuário analisar no remanejamento
     */
    private function exibirItemAnaliseRemanejamento(Collection $itemRemanejamento)
    {
        # Array responsável em montar o cabeçalho da ata
        $column = [
            // 'expandir' => 'Expandir',
            'informacoes_gerais' => 'Inform.',
            'situacao' => 'Situação',
            // 'dados_ata'=>'Dados ata',
            // 'dados_usuario_solicitante'=>'Dados solicitante',
            'numero_item' => 'Número',
            'descricao_item' => 'Descrição',
            // 'unidade_origem'=>'Unidade origem',
            'saldo_remanejamento' => 'Saldo disponível',
            'quantidade_solicitada' => 'Quant. solicitada',
            'quantidade_aprovada_unidade_origem' => 'Quant. autorizada',
            // 'quantidade_aprovada_unidade_gerenciadora_ata'=>'Quant. autorizada unidade gestora ata',
            'detail_row' =>
                [
                    'analise_item' => json_encode(['coldMd' => 3]),
                    'justificativa_analise' => json_encode(['coldMd' => 9])
                ]
        ];
        
        $itemFormatado = [];
        
        $codigoItemRepository = new CodigoItemRepository();
        
        # Recuperar os itens para o usuário marcar na coluna Análise do item
        $statusAnalise = $codigoItemRepository->recuperarStatusItemAnalise();
        
        # Percorre os itens do remanejanento
        foreach ($itemRemanejamento as $item) {
            $montarLinhaItem = array();
            
            $compraItem = $item->itemUnidade->compraItens;
            
            $montarLinhaItem['numero_item'] = $compraItem->numero;
            $montarLinhaItem['descricao_item'] = Str::limit(
                trim($compraItem->descricaodetalhada),
                50,
                '<i class="fas fa-info-circle"
                title="' . $compraItem->descricaodetalhada . '"></i>'
            );
            
            $compraItemUnidade = $item->itemUnidade;
            
            $montarLinhaItem['saldo_remanejamento'] = $this->formatValuePtBR($compraItemUnidade->quantidade_saldo, 0);
            
            
            $montarLinhaItem['quantidade_solicitada'] = $this->formatValuePtBR($item->quantidade_solicitada, 0);
            
            # Montar a informação na coluna Quantidade autorizada unidade origem
            $montarLinhaItem['quantidade_aprovada_unidade_origem'] =
                $this->montarInformacaoCampoQuantidadeAutorizadaUnidadeOrigem($item);
            $montarLinhaItem['id_unico'] = $item->id;
            
            if ($this->existeSaldoUnidadeParaRemanejar(
                $item->itemUnidade,
                $item->quantidade_solicitada
            ) == false) {
                $montarLinhaItem['informacoes_gerais'] = '';
                $montarLinhaItem['situacao'] = 'teste';
                $montarLinhaItem['detail_row'][$item->id] =
                    [
                        'analise_item' => '',
                        'justificativa_analise' => '',
                    ];
                $itemFormatado[] = $montarLinhaItem;
                continue;
            }
            
            $descricaoStatusAnalise = $item->situacaoAnaliseItem->descricao ?? 'Item não analisado';
            $montarLinhaItem['situacao'] = $this->exibirCorStatus($descricaoStatusAnalise);
            
            # Montar o botão para exibir as informações sobre a compra
            $montarLinhaItem['informacoes_gerais'] = $this->botaoExibirInformacoesGerais($item);
            
            
            $montarLinhaItem['detail_row'][$item->id] =
                [
                    'analise_item' => $this->montarRadioAnaliseItem(
                        $statusAnalise,
                        $item->id,
                        $item->status_analise_id,
                        $item->quantidade_solicitada
                    ),
                    'justificativa_analise' =>
                        $this->montarHtmlCampoJustificativa(
                            $item->id,
                            $item->justificativa_analise,
                            $descricaoStatusAnalise
                        ),
                ];
            $itemFormatado[] = $montarLinhaItem;
        }
        
        $this->addTableCustom(
            $column,
            'table_selecionar_item_generico',
            'Selecionar o fornecedor para exibir a informação',
            'col-md-12 pt-5',
            'areaItemAdesao',
            null,
            100,
            $itemFormatado,
            ' ',
            '[3,"ASC"]',
            'table_selecionar_item_generico',
            'table_selecionar_item_generico'
        );
    }
    
    /**
     * Método responsável em exibir as informações na tela da análise para o usuário
     */
    private function exibirDetalheInformacaoAnalise(ArpRemanejamento $remanejamento)
    {
        $this->exibirCamposLabelAnalise($this->crud, $remanejamento, $this->descricaoStatusAnalise);
    }
    
    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');
        $this->importarDatatableForm();
        $arquivoJS = [
            'assets/js/admin/forms/arp_remanejamento_analise.js',
            'packages/backpack/base/js/padrao_gov/jquery.mask.js'
        ];
        
        $this->importarScriptJs($arquivoJS);
        
        $remanejamento = $this->crud->getEntry($this->crud->getCurrentEntryId());
        
        $this->exibirDetalheInformacaoAnalise($remanejamento);
        
        $itemRemanejamentoRepository = new ArpRemanejamentoItemRepository();
        
        # Recupera os itens para serem analisados
        $itemAnalise =
            $itemRemanejamentoRepository->getStatusItensRemanejamento(
                $this->crud->getCurrentEntryId(),
                $this->descricaoStatusAnalise,
                session('user_ug_id')
            );
        
        # Método responsável em montar as informações para exibir na tabela de itens
        $this->exibirItemAnaliseRemanejamento($itemAnalise);
        
        # Criação da modal
        $this->addModalCustom('modalInformacoesDados', 'modalInformacoesDados', 'Informações');
        
        # Criação dos botões customizados
        $this->crud->button_custom = [
            [
                'button_text' => 'Salvar Rascunho',
                'button_id' => 'rascunho',
                'button_name_action' => 'rascunho',
                'button_value_action' => '1',
                'button_icon' => 'fas fa-edit',
                'button_tipo' => 'secondary'
            ],
            [
                'button_text' => 'Finalizar análise',
                'button_id' => 'adicionar',
                'button_name_action' => 'rascunho',
                'button_value_action' => '0',
                'button_icon' => 'fas fa-save',
                'button_tipo' => 'primary'
            ],
        ];
        
        # Para desabilitar o foco no primeiro campo da tela
        $this->crud->setAutoFocusOnFirstField(false);
    }
    
    public function update(ArpRemanejamentoAnaliseUnidadeItemRequest $request)
    {
        $dadosForm = $request->all();
        try {
            $itemRemanejamentoRepository = new ArpRemanejamentoItemRepository();
            $codigoItemRepository = new CodigoItemRepository();
            
            DB::beginTransaction();
            $mensagem = 'Item(ns) atualizado(s) com sucesso';
            $idStatusItemGerenciadoraItem = null;
            $statusAnaliseUnidadeGerenciadora = 'Aguardando aceitação da unidade gerenciadora';
            # Se for finalizar a análise, então recuperamos o ID para a próxima etapa e a mensagem de exibição no alerta
            if (!$dadosForm['rascunho']) {
                $mensagem = 'Item(ns) enviados para a análise da gerenciadora da ata com sucesso';
                $idStatusItemGerenciadoraItem =
                    $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', $statusAnaliseUnidadeGerenciadora);
            }
            
            # Recuperar os status para os itens analisados
            $statusItemAnalisado = $this->retornaIdsCodigoItem(
                'Tipo de Ata de Registro de Preços',
                ['Item Negado', 'Item Aceito Parcialmente', 'Item Aceito']
            );
            
            # Percorrer os itens filtrados dentro do Request
            foreach ($dadosForm['quantidade_analisada'] as $idItem => $quantidadeAnalisada) {
                # Recuperar o status da análise informada pelo usuário
                $statusAnalise = $dadosForm['status_id'][$idItem] ?? null;
                
                # Recuperar a justificativa da análise informada pelo usuário
                $motivoAnalise = $dadosForm['motivo_analise'][$idItem] ?? null;
                
                # Informar o id para atualizar o registro
                $campoCondicional = ['id' => $idItem];
                
                $camposAprovacaoAnalise = array();
                $camposAprovacaoAnalise['status_analise_id'] = $statusAnalise;
                $camposAprovacaoAnalise['justificativa_analise'] = $motivoAnalise;
                $camposAprovacaoAnalise['quantidade_aprovada_participante'] = $quantidadeAnalisada;
                
                # Se o usuário finalizar a análise, então incluímos  com a data da aprovação e o id do usuário
                # e se tiver marcado o status
                if (!$dadosForm['rascunho'] && !empty($camposAprovacaoAnalise['status_analise_id'])) {
                    $camposAprovacaoAnalise['id_usuario_aprovacao_participante'] = backpack_user()->id;
                    $camposAprovacaoAnalise['data_aprovacao_participante'] = Carbon::now()->format('Y-m-d H:i:s');
                    
                    $camposAprovacaoAnalise['situacao_id'] = $idStatusItemGerenciadoraItem;
                    
                    $codigoItem =
                        $codigoItemRepository->getCodigoItemUnico($camposAprovacaoAnalise['status_analise_id']);
                    # Se o item for marcado como negado, então assume esse ID
                    if ($codigoItem->descricao == 'Negar') {
                        $camposAprovacaoAnalise['situacao_id'] = $statusItemAnalisado['Item Negado'];
                    }
                }
                # Atualizar o registro do item do remanejamento
                $itemRemanejamentoRepository->updateRemanejamentoItem($campoCondicional, $camposAprovacaoAnalise);
            }
            
            $itensFaltandoAprovacao =
                $itemRemanejamentoRepository->getStatusItensRemanejamento(
                    $dadosForm['id'],
                    $this->descricaoStatusAnalise,
                    session('user_ug_id')
                );

            # Se não existir item, então passa para a próxima etapa
            if (!$dadosForm['rascunho'] && $itensFaltandoAprovacao->count() == 0) {
                $arpRemanejamentoRepository = new ArpRemanejamentoRepository();
                $campoUpdate = ['situacao_id' => $idStatusItemGerenciadoraItem];
                
                $itenNegado =
                    (new ArpRemanejamentoItemRepository())->getItemRemanejamentoNegado($dadosForm['id']);

                # Se todos os itens foram negados
                if ($itenNegado['totalItemNegado'] == $itenNegado['totalItensRemanejamento']) {
                    $idStatusRemanejamentoNegado =
                        $this->retornaIdCodigoItem(
                            'Tipo de Ata de Registro de Preços',
                            'Negado pela unidade participante'
                        );
                    $campoUpdate = ['situacao_id' => $idStatusRemanejamentoNegado];
                }
                
                $contagemItemPorStatus =
                    (new ArpRemanejamentoItemRepository())->getContagemItemRemanejamentoPorStatus(
                        $dadosForm['id'],
                        'Aguardando aceitação da unidade participante'
                    );
                
               # Se todos os itens foram analisados
                if ($contagemItemPorStatus['quantidadeItemFaltandoAnalisar'] === 0) {
                    $arpRemanejamentoRepository->updateRemanejamento($dadosForm['id'], $campoUpdate);
                }
            }
            
            DB::commit();
            
            \Alert::add('success', $mensagem)->flash();
            return redirect('arp/remanejamento/analisar');
        } catch (Exception $ex) {
            DB::rollBack();
            
            $this->inserirLogCustomizado('gestao_ata', 'error', $ex);
            \Alert::add('error', 'Erro ao realizar análise')->flash();
            
            return redirect('arp/remanejamento/analisar');
        }
    }
}
