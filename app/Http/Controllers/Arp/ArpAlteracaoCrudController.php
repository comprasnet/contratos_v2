<?php

namespace App\Http\Controllers\Arp;

use App\Http\Requests\AlteracaoArpRequest;
use App\Http\Traits\Arp\ArpAlteracaoTrait;
use App\Http\Traits\Arp\ArpItemHistoricoTrait;
use App\Http\Traits\Arp\ArpTrait;
use App\Http\Traits\Arp\ArpQuantidadeAutorizadaAlteracaoTrait;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Contrato\ContratoTrait;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Http\Traits\LogTrait;
use App\Http\Traits\UploadArquivoTrait;
use App\Http\Traits\Arp\ArpAlteracaoCancelamentoTrait;
use App\Http\Traits\Arp\ArpItemTrait;
use App\Jobs\AtualizarQuantitativoRenovadoArpJob;
use App\Models\Arp;
use App\Models\ArpAlteracao;
use App\Models\ArpArquivo;
use App\Models\ArpHistorico;
use App\Models\ArpItem;
use App\Models\ArpItemHistorico;
use App\Models\ArpItemHistoricoAlteracaoFornecedor;
use App\Models\CodigoItem;
use App\Models\Codigo;
use App\Models\CompraItemUnidade;
use App\Models\Contrato;
use App\Models\Fornecedor;
use App\Models\MinutaEmpenho;
use App\Models\Unidade;
use App\Repositories\Arp\ArpItemRepository;
use App\Repositories\Compra\CompraItemFornecedorRepository;
use App\Services\Arp\AdesaoService;
use App\Services\Arp\ArpAlteracaoService;
use App\Services\Arp\ArpRemanejamentoService;
use App\Services\Compra\CompraItemFornecedorService;
use App\Services\FornecedoresService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/**
 * Class ArpAlteracaoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArpAlteracaoCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    use ImportContent;
    use ArpTrait;
    use ArpAlteracaoTrait;
    use UploadArquivoTrait;
    use ArpItemHistoricoTrait;
    use ContratoTrait;
    use BuscaCodigoItens;
    use LogTrait;
    use ArpItemTrait;
    use ArpAlteracaoCancelamentoTrait;
    use ArpItemTrait;
    use ArpQuantidadeAutorizadaAlteracaoTrait;

    protected $arpItemRepository;
    protected $alteracaoService;

    public function __construct(ArpAlteracaoService $alteracaoService)
    {
        parent::__construct();
        $this->alteracaoService = $alteracaoService;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ArpAlteracao::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . 'arp/alteracaoarp/' . $this->returnIdArp());

        $this->exibirTituloPaginaMenu('Alteração de Ata de Registro de Preços');

        CRUD::addclause("where", "arp_id", $this->returnIdArp());
        CRUD::addclause("where", "sequencial", ">", 0);
        CRUD::addclause("orderBy", "sequencial", "DESC");

        $this->crud->addButtonFromModelFunction('line', 'getViewButtonArp', 'getViewButtonArp', 'beginning');

        $ata = Arp::find($this->returnIdArp());
        if ($ata->getSituacao() == 'Cancelada') {
            $this->crud->denyAccess('create');
            $this->crud->denyAccess('update');
            $this->crud->denyAccess('delete');
        }

        $this->data['breadcrumbs'] = $this->breadCrumb();
        return view('backpack::dashboard', $this->data);
    }

    private function returnIdArp()
    {
        return Route::current()->parameter("arp_id");
    }

    private function breadCrumb(bool $acaoVoltar = false)
    {
        $arp = Arp::find($this->returnIdArp());
        $bread = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Ata de Registro de Preços' =>  backpack_url('arp'),
            'Alteração de Ata de Registro de Preços' =>   false,
            $arp->getNumeroAno() =>   false
        ];

        return $bread;
    }

    private function recuperarIdTipoAlteracao(array $tipos)
    {
        return CodigoItem::where('descres', 'statushistorico')
            ->whereIn('descricao', $tipos)
            ->select('id')
            ->pluck('id')
            ->toArray();
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->text_button_redirect_create = 'alteração';
        $this->addColumnModelFunction('sequencial', 'Nº Alteração', 'getSequencial');
        $this->addColumnModelFunction('tipo_alteracao', 'Tipo de alteração', 'getTipoAlteracao');
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.contentClass', 'col-md-12');

        $this->crud->set('show.setFromDb', false);

        $this->setupListOperation();

        $id = Route::current()->parameter("id");
        $optionsAlteracao =
            ['Vigência', 'Valor(es) registrado(s)', 'Cancelamento de item(ns)', 'Informativo', 'Fornecedor'];
        
        $idAlteracao = $this->recuperarIdTipoAlteracao($optionsAlteracao);

        # Recupera todas as informações das alterações
        $atasHistorico = ArpAlteracao::join("arp_historico", "arp_alteracao.id", "=", "arp_historico.arp_alteracao_id")
            ->where("arp_alteracao.id", $id)
            ->whereIn("tipo_id", $idAlteracao)
            ->select("arp_historico.*")

            ->get();

        # Montar as informações sobre o anexo
        $arpAlteracao = $this->crud->model->find($this->crud->getCurrentEntryId());
        $arraySaida = [];

        # Se existir o anexo, exibe para o usuário
        if (isset($arpAlteracao->arquivo[0])) {
            $arraySaida['Nome'][0] = $arpAlteracao->arquivo[0]->nome;
            $arraySaida['Descrição'][0] = $arpAlteracao->arquivo[0]->descricao;
            $arraySaida['Visualizar'][0] =
                '<a target="_blank" href="' . url("storage/{$arpAlteracao->arquivo[0]->url}") . '">
                <i class="fas fa-eye"></i>
            </a>';
        }

        $this->addColumnTable('anexo_alteracao', 'Anexo', $arraySaida);

        foreach ($atasHistorico as $ataHistorico) {
            $data = [
                'name' => "data_assinatura_show",
                'label' => "Data assinatura da alteração",
                'type' => 'date',
                'format' => 'L',
                'value' => $ataHistorico->data_assinatura_alteracao_vigencia
            ];
            $this->crud->addColumn($data);

            # Se a alteração for do tipo de vigência, exibe no show
            if ($ataHistorico->status->descricao == 'Vigência') {
                $data = [
                    'name' => "data_assinatura_alteracao_inicio_vigencia",
                    'label' => "Data início vigência",
                    'type' => 'date',
                    'format' => 'L',
                    'value' => $ataHistorico->data_assinatura_alteracao_inicio_vigencia
                ];
                $this->crud->addColumn($data);

                $data = [
                    'name' => "data_assinatura_alteracao_fim_vigencia",
                    'label' => "Data fim vigência",
                    'type' => 'date',
                    'format' => 'L',
                    'value' => $ataHistorico->data_assinatura_alteracao_fim_vigencia
                ];
                $this->crud->addColumn($data);

                $unidadeLogada = Unidade::where('codigo', session('user_ug'))->first();
                # As esferas da unidade que precisam preencher o campo abaixo
                $esferaUnidadeExibirCampoQuantitativo = ['Estadual', 'Municipal'];

                // if (in_array($unidadeLogada->esfera, $esferaUnidadeExibirCampoQuantitativo))
                # Se a esfera da unidade do usuário logado estiver dentro do array, então precisa selecionar
                if ($unidadeLogada->sisg === false) {
                    $data = [
                        'name' => "quantitativo_renovado_vigencia",
                        'label' => "Quantitativo renovado na prorrogação de vigência",
                        'type' => 'text',
                        'value' => $ataHistorico->getQuantitativoRenovado()
                    ];
                    $this->crud->addColumn($data);
                    if ($ataHistorico->quantitativo_renovado_vigencia !== false) {
                        $data = [
                            'name' => "amparo_legal_renovacao_quantitativo",
                            'label' => "Amparo legal da renovação de quantitativo",
                            'type' => 'text',
                            'value' => $ataHistorico->amparo_legal_renovacao_quantitativo
                        ];
                        $this->crud->addColumn($data);
                    }
                }

                $this->crud->addColumn([
                    'name' => "justificativa_alteracao_vigencia",
                    'label' => "Justificativa/Motivo de alteração de vigência",
                    'type' => 'text',
                    'value' => $ataHistorico->justificativa_alteracao_vigencia
                ]);
            }

            # Se a alteração for do tipo de informativo, exibe no show
            if ($ataHistorico->status->descricao == 'Informativo') {
                $data = [
                    'name' => "objeto_alteracao",
                    'label' => "Objeto alteração",
                    'type' => 'text',
                    'value' => $ataHistorico->objeto_alteracao
                ];
                $this->crud->addColumn($data);
            }

            # Se a alteração for do tipo de valores registrados, exibe no show
            if ($ataHistorico->status->descricao == 'Valor(es) registrado(s)') {
                # Recupera os itens do item histórico

                $itensHistorico = ArpItemHistorico::where("arp_historico_id", $ataHistorico->id)
                    ->where(function ($query) {
                        $query->where("valor_alterado", true)
                            ->orWhereNotNull("arp_item_historico.percentual_maior_desconto");
                    })
                    ->orderBy('id', 'desc')

                    ->get();


                $itens = [];

                # Inicia o contador para informar na tabela para poder rendenrizar para o usuário
                $count = 0;

                # Percorre os itens recuperados que foram alterados
                foreach ($itensHistorico as $item) {
                    $fornecedor =
                        $item->item_arp->item_fornecedor_compra->fornecedor->getNomeFornecedorCompleto() .
                        "({$item->item_arp->item_fornecedor_compra->classificacao})";
                    $item->item_arp->item_fornecedor_compra->compraItens->numero =
                        "{$item->item_arp->item_fornecedor_compra->compraItens->numero}";
                    $item->item_arp->item_fornecedor_compra->compraItens->descricaodetalhada =
                        "{$item->item_arp->item_fornecedor_compra->compraItens->descricaodetalhada}";
                    $item->valor = "{$item->valor}";
                    $percentualMaiorDesconto = "{$item->percentual_maior_desconto}%";


                    $itens['Fornecedor_(Classificação)'][$count] = $fornecedor;
                    $itens['Número'][$count] = $item->item_arp->item_fornecedor_compra->compraItens->numero;
                    $itens['Descrição'][$count] =
                        $item->item_arp->item_fornecedor_compra->compraItens->descricaodetalhada;
                    // Verifica se é um valor ou percentualMaiorDesconto

                    if ($itensHistorico->isEmpty()) {
                        $itens = [];
                    } else {
                        // Se houver itens históricos, percorra o loop e construa o array $itens
                        $itens = [];
                        $mostrarColunaValorAtualizado = false;
                        $mostrarColunaDesconto = false;

                        foreach ($itensHistorico as $item) {
                            $fornecedor =
                                $item->item_arp->item_fornecedor_compra->fornecedor->getNomeFornecedorCompleto() .
                                "({$item->item_arp->item_fornecedor_compra->classificacao})";
                            $item->item_arp->item_fornecedor_compra->compraItens->numero =
                                "{$item->item_arp->item_fornecedor_compra->compraItens->numero}";
                            $item->item_arp->item_fornecedor_compra->compraItens->descricaodetalhada =
                                "{$item->item_arp->item_fornecedor_compra->compraItens->descricaodetalhada}";
                            $item->valor = "{$item->valor}";
                            $percentualMaiorDesconto = "{$item->percentual_maior_desconto}%";

                            $itens['Fornecedor_(Classificação)'][] = $fornecedor;
                            $itens['Número'][] = $item->item_arp->item_fornecedor_compra->compraItens->numero;
                            $itens['Descrição'][] =
                                $item->item_arp->item_fornecedor_compra->compraItens->descricaodetalhada;
                            // Verifica se é um valor ou percentualMaiorDesconto
                            if (is_numeric($item->valor)) {
                                $itens['Valor_atualizado'][] = "{$item->valor}";
                                $itens['Novo_desconto'][] = '';
                                $mostrarColunaValorAtualizado = true;
                            } else {
                                $itens['Valor_atualizado'][] = '';
                                $itens['Novo_desconto'][] = $percentualMaiorDesconto;
                                $mostrarColunaDesconto = true;
                            }
                        }
                        if (!$mostrarColunaValorAtualizado) {
                            unset($itens['Valor_atualizado']);
                        }
                        if (!$mostrarColunaDesconto) {
                            unset($itens['Novo_desconto']);
                        }
                    }

                    $count++;
                }
                $this->addColumnTable('valor_alterado', $ataHistorico->status->descricao, $itens);
            }
            
            # Se a exibiçao for para o cancelamento dos itens
            if ($ataHistorico->status->descricao == 'Cancelamento de item(ns)') {
                $itensHistorico = ArpItemHistorico::where("arp_historico_id", $ataHistorico->id)
                    ->where("item_cancelado", true)
                    ->get();

                $itens = [];
                foreach ($itensHistorico as $key => $item) {
                    $fornecedor =
                        $item->item_arp->item_fornecedor_compra->fornecedor->getNomeFornecedorCompleto() .
                        "({$item->item_arp->item_fornecedor_compra->classificacao})";
                    $itens['Fornecedor_(Classificação)'][$key] = $fornecedor;
                    $itens['Número'][$key] = $item->item_arp->item_fornecedor_compra->compraItens->numero;
                    $itens['Descrição'][$key] =
                        $item->item_arp->item_fornecedor_compra->compraItens->descricaodetalhada;
                    $itens['Valor_Unitário'][$key] =
                        $item->item_arp->item_fornecedor_compra->valor_unitario;
                }
                $this->addColumnTable('item_cancelado', $ataHistorico->status->descricao, $itens);

                $data = [
                    'name' => "justificativa_motivo_cancelamento",
                    'label' => "Justificativa/Motivo de cancelamento",
                    'type' => 'text',
                    'value' => $ataHistorico->justificativa_motivo_cancelamento
                ];
                $this->crud->addColumn($data);
            }
            
            if ($ataHistorico->status->descricao == 'Fornecedor') {
                $itensAlterados =
                    ArpItemHistoricoAlteracaoFornecedor::join('arp_historico', 'arp_historico.id', 'arp_historico_id')
                    ->where('arp_alteracao_id', $id)
                    ->whereNotNull('compra_item_fornecedor_destino_id')
                    ->select('arp_item_historico_alteracao_fornecedor.*')
                    ->get();
                
                $itens = [];
                $fornecedorService = new FornecedoresService();
                foreach ($itensAlterados as $item) {
                    $fornecedor =
                        $item->compraItemFornecedorOrigem->fornecedor->getNomeFornecedorCompleto() .
                        "({$item->compraItemFornecedorDestino->classificacao})";
                    $itens['Fornecedor_(Classificação)'][] = $fornecedor;
                    
                    $itens['Número'][] = $item->compraItemFornecedorOrigem->compraItens->numero;
                    
                    $itens['Descrição'][] =
                        $item->compraItemFornecedorOrigem->compraItens->descricaodetalhada;
                    
                    $itens['Novo CPF/CNPJ'][] = $item->novo_cnpj;
                    
                    $itens['Novo nome'][] =
                        $item->novo_nome ?? $fornecedorService->recuperarNomeFornecedorPorCpfCnpj($item->novo_cnpj);
                }
                
                $this->addColumnTable('fornecedor_alterado', $ataHistorico->status->descricao, $itens);
            }
        }

        $this->data['breadcrumbs'] = $this->breadCrumb(true);
        return view('backpack::dashboard', $this->data);
    }

    /**
     * Método responsável em exibir os tipos de alteração para o usuário selecionar
     */
    private function campoTipoAlteracao(bool $edicao = false, array $itemSelecionado = null)
    {
        #Todos os tipos de alteração
      

        # Recupera o ID do tipo de alteração de Vigência
        $idTipoAlteracao = $this->recuperarIdTipoAlteracao(['Vigência']);
      
        # Recupera se para a ARP tem um tipo de vigência
        $alteracaoVigencia = ArpHistorico::leftjoin(
            "arp_alteracao",
            "arp_historico.arp_alteracao_id",
            "=",
            "arp_alteracao.id"
        )
            ->where("rascunho", false)
            ->where("arp_alteracao.arp_id", $this->returnIdArp())
            ->whereIn("tipo_id", $idTipoAlteracao)
            ->exists();
        
        $optionsAlteracao =
            ['Vigência', 'Valor(es) registrado(s)', 'Cancelamento de item(ns)', 'Informativo', 'Fornecedor'];

        # Se já tiver sido cadastrado o tipo de Vigência, exibe a listagem abaixo
        if ($alteracaoVigencia) {
            array_shift($optionsAlteracao);
        }
        
        $this->crud->addField([   // Checklist
            'label'     => 'Tipo de alteração',
            'type'      => 'checklist',
            'name'      => 'tipo_id[]',
            'entity'    => 'status',
            'attribute' => 'descricao',
            'model'     => CodigoItem::class,
            'pivot'     => true,
            'options' => $optionsAlteracao,
            'columnfilter' => 'descricao',
            'required' => 'required',
            'attributes' => [
                'onclick' => 'exibirCampoFormulario(this)',
                'data-idArp' => $this->returnIdArp(),
                'data-url' => url('arp/alteracaoarp/' . $this->returnIdArp() . '/' . $this->crud->getCurrentEntryId())
            ],
            'wrapper' => [],
            'number_of_columns' => 5,
            'value' => $itemSelecionado
        ]);

        # Necessário para marcar na tela de edição
        $this->crud->addField([
            'name' => 'item_selecionado_hidden',
            'type' => 'hidden',
            'value' => json_encode($itemSelecionado)
        ]);
    }


    /**
     * Método responsável em exibir os dados comuns do tipo de alteração para o usuário
     */
    private function areaVigencia(bool $editar = false)
    {
        $dataAssinaturaAlteracaoVigencia = null;
        $descricaoAnexo = null;

        # Recupera as informações da ata principal
        $arp = Arp::find($this->returnIdArp());
        $unidadeLogada = Unidade::where('codigo', session('user_ug'))->first();

//        $dadosAlteracao = ArpAlteracao::where(
//            "arp_id", $this->crud->getCurrentEntryId()
//        );

        $arpHistoricoAmparo = ArpHistorico::where("arp_id", $this->returnIdArp())
            ->where("arp_alteracao_id", $this->crud->getCurrentEntryId())
            ->whereNotNull("quantitativo_renovado_vigencia")
            ->first();

        $amparoLegalAlteracao = $arpHistoricoAmparo->amparo_legal_renovacao_quantitativo ?? null;
        $quantitativoRenovadoAlteracao = $arpHistoricoAmparo->quantitativo_renovado_vigencia ?? null;


        # Converte a data para o carbon e posterga a vigência para 1 ano após a vigência inicial
        $dataFimVigenciaAlteracao = new Carbon($arp->vigencia_final);
        $dataLimiteVigencia = $dataFimVigenciaAlteracao->addDays(365);
        $dataFimVigenciaAlteracao = $dataLimiteVigencia;
        $justificativaAlteracaoVigencia = null;

        # Se a tela que o usuário acessar for a edição, entra no condicional
        if ($editar) {
            # Recupera a informação da data de assinatura informada
            $arpAlteracaoVigencia = ArpHistorico::where("arp_id", $this->returnIdArp())
                ->whereNotNull("arp_alteracao_id")
                ->orderby("id", "desc")
                ->first();

            # Se encontrar o registro no banco de dados, preenche com as informações comuns entre os tipos
            if (!empty($arpAlteracaoVigencia)) {
                $dataAssinaturaAlteracaoVigencia = $arpAlteracaoVigencia->data_assinatura_alteracao_vigencia;
                $dataFimVigenciaAlteracao = $arpAlteracaoVigencia->data_assinatura_alteracao_fim_vigencia;
                $quantitativoRenovadoAlteracao = $arpAlteracaoVigencia->quantitativo_renovado_vigencia;
                $amparoLegalAlteracao = $arpAlteracaoVigencia->amparo_legal_renovacao_quantitativo;
                $justificativaAlteracaoVigencia = $arpAlteracaoVigencia->justificativa_alteracao_vigencia;

                # Se for removido a data, retorna para a data padrão
                if (empty($dataFimVigenciaAlteracao)) {
                    $dataFimVigenciaAlteracao = $dataLimiteVigencia;
                }
            }

            # Recupera a descrição do anexo
            $arpArquivo = ArpArquivo::where("arp_alteracao_id", $this->crud->getCurrentEntryId())->first();
            $descricaoAnexo = $arpArquivo->descricao ?? null;
        }

        $this->crud->addField([
            'name' => 'data_assinatura_alteracao_vigencia',
            'label' => 'Data de assinatura da alteração',
            'type'  => 'date',
            'required' => 'required',
            'attributes' => [
                'required' => 'required'
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-3 pt-3'
            ],
            'value' => $dataAssinaturaAlteracaoVigencia
        ]);

        $this->crud->addField([
            'name' => 'arp_id',
            'type'  => 'hidden',
            'default' => $this->returnIdArp(),
            'attributes' => ['id' => 'arp_id']
        ]);

        $this->crud->addField([
            'name' => 'data_limite_vigencia',
            'type'  => 'hidden',
            'default' => $dataLimiteVigencia->toDateString(),
            'attributes' => ['id' => 'data_limite_vigencia']
        ]);

        $this->crud->addField([
            'name' => 'data_limite_vigencia_inicial',
            'type'  => 'hidden',
            'default' => $arp->vigencia_inicial,
            'attributes' => ['id' => 'data_limite_vigencia_inicial']
        ]);

        $this->crud->addField([
            'name' => 'arquivo_alteracao',
            'label' => 'Anexo da alteração',
            'type'  => 'upload_custom_generic',
            'required' => 'required',
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3'
            ],
            'attributes' => ['id' => 'arquivo_alteracao', 'required' => 'required'],
            'model' => ArpArquivo::class,
            'upload'    => true,
            'colum_filter' => ['arp_alteracao_id' => $this->crud->getCurrentEntryId()]
        ]);

        $this->crud->addField(
            [
                'name'      => 'descricao_anexo', // The db column name
                'label'     => 'Descrição do anexo', // Table column heading
                'type'      => 'textarea',
                'required' => 'required',
                'wrapperAttributes' => [
                    'class' => 'col-md-5 br-textarea pt-3'
                ],
                'attributes' => ['id' => 'descricao_anexo', 'required' => 'required'],
                'escaped' => false,
                'value' => $descricaoAnexo
            ]
        );

        $this->crud->addField(
            [
                'name'      => 'justificativa_alteracao_vigencia', // The db column name
                'label'     => 'Justificativa/Motivo de alteração de vigência', // Table column heading
                'type'      => 'textarea',
                'value' => $justificativaAlteracaoVigencia,
                'wrapperAttributes' => [
                    'class' => 'col-md-12 br-textarea pt-3 areaVigencia'
                ],
                'attributes' => [
                    'required' => 'required',
                    'id' => 'justificativa_alteracao_vigencia',
                    'maxlength'=>'250'
                ],
                'required' => true
            ]
        );

        $this->crud->addField([
            'name' => 'data_assinatura_alteracao_fim_vigencia',
            'label' => 'Data fim da vigência',
            'type'  => 'date',
            'value' => $dataFimVigenciaAlteracao,
            'wrapperAttributes' => [
                'class' => 'col-md-3 pt-3 areaVigencia'
            ],
            'attributes' => [
                'required' => 'required',
                'id' => 'data_assinatura_alteracao_fim_vigencia'
            ],
        ]);
        # As esferas da unidade que precisam preencher o campo abaixo
        $esferaUnidadeExibirCampoQuantitativo = ['Estadual', 'Municipal'];


        # Se a esfera da unidade do usuário logado estiver dentro do array, então precisa selecionar
        # o campo abaixo
       // if (in_array($unidadeLogada->esfera, $esferaUnidadeExibirCampoQuantitativo))

        if ($unidadeLogada->sisg === false) {
            $this->crud->addField(
                [   // radio
                   'name'        => 'quantitativo_renovado_vigencia', // the name of the db column
                   'label'            => 'Quantitativo renovado na prorrogação de vigência? 
                   <i class="fas fa-info-circle" title="
                     Essa opção só pode ser utilizada por estados e municípios que possuem 
                     regulamentação que permita a renovação de saldo. 
                     O decreto federal não permite essa opção para atas de órgãos federais"></i>',
                   'type'        => 'radio',
                   'options'     => [
                       // the key will be stored in the db, the value will be shown as label;
                       0 => "Não",
                       1 => "Sim"
                   ],
                   'default' => 0,
                   'wrapperAttributes' => [
                       'class' => 'col-md-4 pt-3 quantitativo_renovado_vigencia'
                   ],
                   'attributes' => [
                       'onchange' => 'analisarCampoQuantitativo(this)',
                       'id' => 'quantitativo_renovado_vigencia'
                   ],
                   'value' => $quantitativoRenovadoAlteracao,
                   // optional
                   'inline'      => 'd-inline-block',
                   //'required' => true
                ],
            );

            $this->crud->addField(
                [
                   'name'      => 'amparo_legal_renovacao_quantitativo',
                   'label'     => 'Amparo legal da renovação de saldo ',
                   'type'      => 'textarea',
                   'wrapperAttributes' => [
                       'class' => 'col-md-12 br-textarea pt-3 amparo_legal_renovacao_quantitativo'
                   ],
                   'attributes' => [
                       'required' => 'required',
                       'id' => 'amparo_legal_renovacao_quantitativo'
                   ],
                   'value' => $amparoLegalAlteracao,
                   'required' => true
                ]
            );
        }
    }

    private function areaValoresCancelamentoItens(
        bool $editar = false,
        array $itemSelecionado = null,
        ?int $idAlteracao = null
    ) {
        $styleValorUnitario = 'display : none;';
        $styleItemCancelado = 'display : none;';
        $itens = null;
        $itemValorAlterado = [];
        $itemCancelado = [];
        $cancelamento = null;

        # Se for na tela de edição, entra no IF
        if ($editar) {
            $itensAlteracao = $this->itemAlteracao($this->returnIdArp(), $this->crud->getCurrentEntryId())->toArray();
            foreach ($itensAlteracao as $itemAlteracao) {
                foreach ($itemSelecionado as $item) {
                    $tipoAlteracao = CodigoItem::find($item);
                    $nameInputValorUnitario = "novo_valor_unitario[{$itemAlteracao['id']}]";
                    $nameInputValorDesconto = "novo_valor_desconto[{$itemAlteracao['id']}]";

                    $itemAlteracao['novovalordesconto'] = $this->montarCampoValorDesconto(
                        'inputValorDesconto',
                        $nameInputValorDesconto,
                        $itemAlteracao['criterio_julgamento']
                    );

                    $itemAlteracao['novovalor'] = $this->montarCampoValorRegistrado(
                        'inputValorRegistrado',
                        $nameInputValorUnitario,
                        $itemAlteracao['criterio_julgamento']
                    );

                    #Se tiver marcado os valores alterados
                    if ($tipoAlteracao->descricao == 'Valor(es) registrado(s)') {
                        $styleValorUnitario = null;
                        $valorAlterado = ArpItemHistorico::join(
                            "arp_historico",
                            "arp_historico.id",
                            "=",
                            "arp_item_historico.arp_historico_id"
                        )
                            ->where("arp_alteracao_id", $idAlteracao)
                            ->where("arp_historico.tipo_id", $item)
                            ->where("arp_item_id", $itemAlteracao['id'])
                            ->where(function ($query) {
                                $query->where("valor_alterado", true)
                                    ->orWhereNotNull("arp_item_historico.percentual_maior_desconto");
                            })
                            ->select("arp_item_historico.valor", "arp_item_historico.percentual_maior_desconto")
                            ->first();

                        $valorLancado = null;
                        $valorPercentual = null;

                        if (!empty($valorAlterado)) {
                            $valorLancado = $valorAlterado->valor;
                            $valorPercentual =  $valorAlterado->percentual_maior_desconto;
                        }

                        $itemValorAlterado[$itemAlteracao['id']] = $valorLancado;

                        $itemAlteracao['novovalordesconto'] = $this->montarCampoValorDesconto(
                            'inputValorDesconto',
                            $nameInputValorDesconto,
                            $itemAlteracao['criterio_julgamento'],
                            $valorPercentual
                        );

                        $itemAlteracao['novovalor'] = $this->montarCampoValorRegistrado(
                            'inputValorRegistrado',
                            $nameInputValorUnitario,
                            $itemAlteracao['criterio_julgamento'],
                            $valorLancado
                        );
                    }

                    if (!isset($itemCancelado[$itemAlteracao['id']])) {
                        $itemAlteracao['itemcancelado'] = $this->montarCheckCancelarItem(
                            $itemAlteracao['id'],
                            'itemcanceladoclass',
                            $nameInputValorUnitario
                        );
                    }

                    #Se tiver item cancelado
                    if ($tipoAlteracao->descricao == 'Cancelamento de item(ns)') {
                        $styleItemCancelado = null;
                        $itemCanceladoHistorico = ArpItemHistorico::join(
                            "arp_historico",
                            "arp_historico.id",
                            "=",
                            "arp_item_historico.arp_historico_id"
                        )
                        ->where("arp_alteracao_id", $idAlteracao)
                        ->where("arp_historico.tipo_id", $item)
                        ->where("arp_item_id", $itemAlteracao['id'])
                        ->where("item_cancelado", true)
                        ->exists();

                        $itemChecked = '';
                        if ($itemCanceladoHistorico) {
                            $itemChecked = 'checked';
                        }

                        $itemCancelado[$itemAlteracao['id']] = true;
                        $itemAlteracao['itemcancelado'] =  $this->montarCheckCancelarItem(
                            $itemAlteracao['id'],
                            'itemcanceladoclass',
                            $nameInputValorUnitario,
                            $itemChecked
                        );
                    }
                }
                $itens[] = $itemAlteracao;
            }

            $this->crud->addField([
                'name' => 'item_valor_alterado',
                'type'  => 'hidden',
                'default' => json_encode($itemValorAlterado),
                'attributes' => ['id' => 'arp_id'],
                'fake' => true
            ]);

//            $this->crud->addField([
//                'name' => 'item_cancelado',
//                'type'  => 'hidden',
//                'default' => json_encode($itemCancelado),
//                'attributes' => ['id' => 'arp_id'],
//                'fake' => true
//            ]);

            $cancelamentoAlteracao = ArpHistorico::where("arp_id", $this->returnIdArp())
                ->where("arp_alteracao_id", $this->crud->getCurrentEntryId())
                ->whereNotNull("justificativa_motivo_cancelamento")
                ->first();

            $cancelamento = $cancelamentoAlteracao->justificativa_motivo_cancelamento ?? null;
        }

        $this->crud->addField(
            [
                'name'      => 'justificativa_motivo_cancelamento', // The db column name
                'label'     => 'Justificativa/Motivo de cancelamento', // Table column heading
                'type'      => 'textarea',
                'wrapperAttributes' => [
                    'class' => 'col-md-12 br-textarea pt-3 justificativa_motivo_cancelamento'
                ],
                'attributes' => [
                    'required' => 'required',
                    'id' => 'justificativa_motivo_cancelamento',
                    'maxlength'=>'250'
                ],
                'value' => $cancelamento,
                'required' => true
            ]
        );

        $column = [
            'nomefornecedor' => 'Fornecedor (Classificação)',
            'numero' => 'Número',
            'descricaodetalhada' => 'Descrição',
            'valor_unitario' => 'Valor unitário',
            'percentual_maior_desconto' => ['column' => 'Desconto anterior %', 'class' => 'percentual-antigo'],
            'novovalordesconto' => ['column' => 'Novo desconto %', 'class' => 'novovalordesconto'],
            'novovalor' =>
            ['column' => 'Novo valor unitário', 'class' => 'novovalor', 'style' => $styleValorUnitario],
            'itemcancelado' =>
            [
                'column' => 'Cancelar Item? <br> <input type="checkbox" class="selectAll">',
                'class' => 'itemcancelado',
                'style' => $styleItemCancelado
            ]
        ];


        $this->crud->addField([
            'name' => 'areaValoresRegistrados',
            'type' => 'table_selecionar_item_custom',
            'classArea' => 'areaValoresRegistrados',
            'idArea' => 'areaValoresRegistrados',
            'idTable' => 'tabelaValoresRegistrados',
            'label' => 'Itens da ata',
            'column' => $column,
            'value' => $itens,
            'datatable' => false
        ]);
    }

    private function areaObjetoAlteracao(bool $editar = false)
    {
        $objeto = null;

        if ($editar) {
            $objetoAlteracao = ArpHistorico::where("arp_id", $this->returnIdArp())
                ->where("arp_alteracao_id", $this->crud->getCurrentEntryId())
                ->whereNotNull("objeto_alteracao")
                ->first();
            if (!empty($objetoAlteracao)) {
                $objeto = $objetoAlteracao->objeto_alteracao;
            }
        }

        $this->crud->addField(
            [
                'name'      => 'objeto_alteracao', // The db column namesu
                'label'     => 'Objeto da alteração', // Table column heading
                'type'      => 'textarea',
                'wrapperAttributes' => [
                    'class' => 'col-md-12 br-textarea pt-3 areaInformativo'
                ],
                'escaped' => false,
                'value' => $objeto,
                'required' => true
            ]
        );
    }

    private function botoesBreadcrumbCustomizado()
    {
        $this->crud->button_custom = [
            [
                'button_text' => 'Salvar Rascunho', 'button_id' => 'rascunho',
                'button_name_action' => 'rascunho', 'button_value_action' => '1',
                'button_icon' => 'fas fa-edit', 'button_tipo' => 'secondary'
            ],
            [
                'button_text' => 'Criar Alteração', 'button_id' => 'adicionar',
                'button_name_action' => 'rascunho', 'button_value_action' => '0',
                'button_icon' => 'fas fa-save', 'button_tipo' => 'primary'
            ],
        ];

        $this->data['breadcrumbs'] = $this->breadCrumb(true);
        return view('backpack::dashboard', $this->data);
    }

    private function areaDadosAta()
    {
        $arpAlteracao = Arp::find($this->returnIdArp());

        $this->crud->addField(
            [
                'name'      => 'numero_ata', // The db column name
                'label'     => 'Número/Ano da Ata', // Table column heading
                'type'      => 'label',
                'wrapperAttributes' => [
                    'class' => 'col-md-4 pt-3 mb-3'
                ],
                'value' => $arpAlteracao->getNumeroAno()
            ]
        );

        $this->crud->addField(
            [
                'name'      => 'numero_ano_compra', // The db column name
                'label'     => 'Número/Ano da Compra', // Table column heading
                'type'      => 'label',
                'wrapperAttributes' => [
                    'class' => 'col-md-4 pt-3 mb-3'
                ],
                'value' => $arpAlteracao->compras->numero_ano
            ]
        );

        $this->crud->addField(
            [
                'name'      => 'modalidade_compra', // The db column name
                'label'     => 'Modalidade da Compra', // Table column heading
                'type'      => 'label',
                'wrapperAttributes' => [
                    'class' => 'col-md-4 pt-3 mb-3'
                ],
                'value' => $arpAlteracao->compras->getModalidade()
            ]
        );

        $this->crud->addField(
            [
                'name'      => 'data_assinatura', // The db column name
                'label'     => 'Data da Assinatura da Ata', // Table column heading
                'type'      => 'label',
                'date'      => true,
                'wrapperAttributes' => [
                    'class' => 'col-md-4 pt-3 mb-3'
                ],
                'value' => $arpAlteracao->data_assinatura
            ]
        );

        $this->crud->addField(
            [
                'name'      => 'data_inicio_vigencia', // The db column name
                'label'     => 'Data da Vigência inicial da Ata', // Table column heading
                'type'      => 'label',
                'date'      => true,
                'wrapperAttributes' => [
                    'class' => 'col-md-4 pt-3 mb-3'
                ],
                'value' => $arpAlteracao->vigencia_inicial
            ]
        );

        $this->crud->addField(
            [
                'name'      => 'data_fim_vigencia', // The db column name
                'label'     => 'Data da Vigência final da Ata', // Table column heading
                'type'      => 'label',
                'date'      => true,
                'wrapperAttributes' => [
                    'class' => 'col-md-4 pt-3 mb-3'
                ],
                'value' => $arpAlteracao->vigencia_final
            ]
        );
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');
        CRUD::setValidation(AlteracaoArpRequest::class);
        $this->importarDatatableForm();

        $arquivoJS = [
            'assets/js/admin/forms/arp_alteracao.js', 'assets/js/blockui/jquery.blockUI.js',
            'packages/backpack/base/js/padrao_gov/jquery.mask.js'
        ];

        $this->importarScriptJs($arquivoJS);

        $this->areaDadosAta();

        $this->campoTipoAlteracao();

        $this->areaVigencia();

        $this->areaValoresCancelamentoItens();

        $this->areaObjetoAlteracao();
        
        $this->areaFornecedorAlteracaoItens();
        
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */

        $this->botoesBreadcrumbCustomizado();
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');
        $arquivoJS = [
            'assets/js/admin/forms/arp_alteracao.js', 'assets/js/blockui/jquery.blockUI.js',
            'packages/backpack/base/js/padrao_gov/jquery.mask.js'
        ];

        $this->importarScriptJs($arquivoJS);

        $itensSelecionados = ArpHistorico::where("arp_id", $this->returnIdArp())
            ->where("arp_alteracao_id", $this->crud->getCurrentEntryId())
            ->select("tipo_id")
            ->pluck("tipo_id")
            ->toArray();

        # Área responsável em exibir as informações da ata
        $this->areaDadosAta();

        # Responsável em marcar os tipos de ações que o usuário marcou
        $this->campoTipoAlteracao(true, $itensSelecionados);

        # Responsável em exibir a data de vigência marcada
        $this->areaVigencia(true);

        # Responsável em montar a tabela com os valores lançados e os itens cancelados
        $this->areaValoresCancelamentoItens(true, $itensSelecionados, $this->crud->getCurrentEntryId());

        # Responsável em exibir o objeto de alteração
        $this->areaObjetoAlteracao(true);

        $this->botoesBreadcrumbCustomizado();
        
        $this->areaFornecedorAlteracaoItens(true, $itensSelecionados, $this->crud->getCurrentEntryId());
    }

    /**
     * Método responsável em recuperar os itens da ata que não foram cancelados
     */
    public function itemAlteracao($id, $arpAlteracaoId)
    {
        # Recupera os itens para exibição com a exceção dos itens cancelados
        return ArpItem::getItemAlteracaoPorAta($id, $arpAlteracaoId);
    }

    /**
     * Método responsável em exibir os itens quando selecionar o tipo de alteração
     * Valor(es) registrado(s) e Cancelamento de item(ns)
     */
    public function exibirItemTipoAlteracao($id)
    {
        $itensAta =  $this->itemAlteracao($id, $this->crud->getCurrentEntryId());
        $itemFormatado = array();
        # Percorre o resultado obtido para ajustar o texto
        foreach ($itensAta as $item) {
            # Formata o texto para exibir uma parte e o restante
            # ficará no ícone ao lado direito
            $item['descricaodetalhada'] = Str::limit(trim($item['descricaodetalhada']), 50, '
                                        <i class="fas fa-info-circle" title="' . $item['descricaodetalhada'] . '"></i>');
            $item['idata'] = $id;
            
            $item['item_alterado_fornecedor'] =
                $this->getItemExisteAlteracaoFornecedorCancelamento($id, $item->compra_item_fornecedor_id);
            
            $itemFormatado[] = $item;
        }

        return $itemFormatado;
    }

    /**
     * Método responsável em criar o registro com alteração de informativo
     */
    private function alterarInformativo(array $dadosFormulario, array $dadosHistoricoArp)
    {
        $dadosHistoricoArp['objeto_alteracao'] = $dadosFormulario['objeto_alteracao'];

        ArpHistorico::create($dadosHistoricoArp);
    }

    /**
     * Método responsável em alterar os valores dos itens
     */
    private function alterarValorRegistrado(array $dadosFormulario, array $dadosHistoricoArp)
    {
        # Criação do registro do item no histórico
        $idArpHistorico = ArpHistorico::create($dadosHistoricoArp)->id;

        if (!isset($dadosFormulario['novo_valor_unitario'])) {
            return $idArpHistorico;
        }

        $this->salvarItemAlteracaoValor(
            $dadosFormulario['novo_valor_unitario'],
            $dadosFormulario['data_assinatura_alteracao_vigencia'],
            $idArpHistorico,
            $dadosFormulario['rascunho'],
            $dadosFormulario['arp_id']
        );

        return $idArpHistorico;
    }

    private function alterarPercentualDesconto(array $dadosFormulario, array $dadosHistoricoArp, bool $rascunhoTela = false, int $idArpHistorico)
    {

        $itemInicial = ArpItem::where("arp_id", $dadosFormulario['arp_id'])->get();

        /*
        if (!$rascunhoTela) {
            # Criação do registro do item no histórico
            $idArpHistorico = ArpHistorico::create($dadosHistoricoArp)->id;
        }
        $idArpHistorico = $dadosFormulario['arp_historico_id']  ?? ArpHistorico::create($dadosHistoricoArp)->id;
        */

        if (!isset($dadosFormulario['novo_valor_desconto'])) {
            return;
        }
        # Remove os valores que não foram utilizados
        $arrayValorAlteradoDesc = array_filter($dadosFormulario['novo_valor_desconto']);

        foreach ($itemInicial as $item) {
            foreach ($arrayValorAlteradoDesc as $numeroItemAlterado => $valorItemAlteracaoDesconto) {
                # Percorre os valores informados pelo usuário ao preencher os valores
                $dadosItemDesconto = $item->toArray();
                $dadosItemDesconto['vigencia_inicial'] = $dadosFormulario['data_assinatura_alteracao_vigencia'];
                $dadosItemDesconto['arp_historico_id'] = $idArpHistorico;
                $dadosItemDesconto['item_cancelado'] = false;
                $dadosItemDesconto['arp_item_id'] = $item->id;
                $dadosItemDesconto['percentual_maior_desconto'] = str_replace(',', '.', $valorItemAlteracaoDesconto);

                # Se o numero do item selecionado for igual ao numero alterado, insere o valor informado e informa
                # que o valor do item está sendo atualizado
                if ($item->id == $numeroItemAlterado) {
                    # Se a ação não for um rascunho, atualiza a tabela fornecedor
                    if (!$dadosFormulario['rascunho']) {
                        $fornecedorItem = $item->item_fornecedor_compra;
                        $fornecedorItem->percentual_maior_desconto = str_replace(',', '.', $valorItemAlteracaoDesconto);
                        $fornecedorItem->save();
                    }
                    unset($dadosItemDesconto['arp_id']);

                    ArpItemHistorico::updateOrCreate(
                        ['arp_historico_id' => $dadosItemDesconto['arp_historico_id'], 'arp_item_id' => $dadosItemDesconto['arp_item_id']],
                        $dadosItemDesconto
                    );
                }
            }
        }
        
        $this->recalcularValorTotalAta($dadosFormulario['rascunho'], $dadosFormulario['arp_id']);
    }

    /**
     * Método responsável em retornar as informações da ata inicial na tabela Arp Histórico
     */
    private function retornarDadosAtaInicial(int $idArp)
    {
        # Recupera o id do status Ata Inicial
        $idStatusAtaInicial = $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', 'Ata Inicial');

        # Recupera as informações inicias da ata na tabela arp_historico
        $ataInicial = ArpHistorico::where("arp_id", $idArp)
            ->where("tipo_id", $idStatusAtaInicial)
            ->oldest()
            ->first();

        # Se não existir o registro, então retorna os dados da ata cadastrada inicialmente
        if (!empty($ataInicial)) {
            return $ataInicial->toArray();
        }

        # Busca as informações atuais da ata
        $arp = Arp::find($idArp);

        # Insere o ID da tabela ARP
        $arp->arp_id = $idArp;

        # Altera o status para a Ata Inicial
        $arp->tipo_id = $idStatusAtaInicial;

        # Converte o objeto em array para inserir na tabela Arp Histórico
        $dadosAtaInicial = $arp->toArray();

        return ArpHistorico::create($dadosAtaInicial)->toArray();
    }


    public function store(AlteracaoArpRequest $request)
    {
        DB::beginTransaction();
        $dados = $request->all();

        try {
            $dadosAtaInicial = $this->retornarDadosAtaInicial($dados['arp_id']);
            // ArpHistorico::where("arp_id", $dados['arp_id'])->oldest()->first()->toArray();

            # Prepara as informações para incluir na tabela arp alteração
            $dadosArpAlteracao = $this->montarDadosAlteracao($dados);

            # Recupera o Id da tabela arp para poder incluir na coluna na tabela arp histórico
            $dadosAtaInicial['arp_alteracao_id'] = $this->inserirArpAlteracao($dadosArpAlteracao);

            # Setar a informação da alteração id no request para salvar as informações no arquivo
            $request->request->set('arp_alteracao_id', $dadosAtaInicial['arp_alteracao_id']);

            if (isset($dados['tipo_id'])) {
                # Necessário ordenar em ordem crescente para que o a alteração do Fornecedor seja a última
                # alteração a ser realizada para que pegue todos os valores atualizados do item
                sort($dados['tipo_id']);
                foreach ($dados['tipo_id'] as $tipo) {
                    # Monta as informações necessários para salvar o histórico da ação
                    $dadosAtaInicial = $this->alteracaoService->montarInformacaoInicial(
                        $tipo,
                        $dados,
                        $dadosAtaInicial,
                        __FUNCTION__
                    );

                    #Para incluir na marcação que o usuário realizar
                    switch ($dadosAtaInicial['descricao_tipo']) {
                        case 'Vigência':
                            $this->alteracaoService->realizarAlteracaoVigencia(
                                null,
                                $tipo,
                                $dados,
                                $dadosAtaInicial
                            );
                            break;
                        case 'Informativo':
                            $this->alterarInformativo($dados, $dadosAtaInicial);
                            break;
                        case 'Valor(es) registrado(s)':
                            $idArpHistorico = $this->alterarValorRegistrado($dados, $dadosAtaInicial);
                            $this->alterarPercentualDesconto($dados, $dadosAtaInicial, true, $idArpHistorico);
                            break;
                        case 'Cancelamento de item(ns)':
                            $this->alteracaoService->realizarCancelamentoItem(
                                null,
                                $tipo,
                                $dados,
                                $dadosAtaInicial
                            );
                            break;
                        case 'Fornecedor':
                            $this->alterarFornecedor($dados, $dadosAtaInicial);
                            break;
                    }
                }
            }


            # Salvar o arquivo na tabela e na pasta
            $this->salvarArquivoArp(
                $request,
                'Alteração',
                'arp/alteracao/' . $request->arp_id . '/' . $request->arp_alteracao_id,
                'arquivo_alteracao'
            );

            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            $this->inserirLogCustomizado('gestao_ata', 'error', $ex);

            \Alert::add('error', 'Erro ao realizar a alteração!')->flash();
            return redirect()->back();
        }

        \Alert::add('success', 'Alteração realizada com sucesso')->flash();
        return redirect('/arp/alteracaoarp/' . $dados['arp_id']);
    }

    public function destroy($idArp, $idArpAlteracao)
    {
        DB::beginTransaction();
        try {
            # Recupera o registro para remover na parte física
            $arpAlteracao = ArpAlteracao::find($idArpAlteracao);
            foreach ($arpAlteracao->arquivo as $arquivo) {
                # Remove fisicamente o arquivo do servidor
                $this->removerArquivoUpload(ArpArquivo::class, $arquivo->id);
            }

            ArpHistorico::where("arp_alteracao_id", $arpAlteracao->id)->delete();

            # Deleta o registro
            $arpAlteracao->delete();

            DB::commit();
            return true;
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }
    }

    # Usado na tela de rascunho
    private function alterarValorRegistradoUpdate(array $dadosFormulario, ArpHistorico $dadosHistoricoArp)
    {
        if (!isset($dadosFormulario['novo_valor_unitario'])) {
            return;
        }
        $itemHistorico = $dadosHistoricoArp->item_historico;

        if (!empty($itemHistorico)) {
            foreach ($itemHistorico as $itemHistoricoDeletar) {
                $itemHistoricoDeletar->forceDelete();
            }
        }

        $this->salvarItemAlteracaoValor(
            $dadosFormulario['novo_valor_unitario'],
            $dadosFormulario['data_assinatura_alteracao_vigencia'],
            $dadosHistoricoArp->id,
            $dadosFormulario['rascunho'],
            $dadosFormulario['arp_id']
        );
    }

    # Usado na tela de rascunho
    private function alterarInformativoUpdate(array $dadosFormulario, ArpHistorico $dadosHistoricoArp)
    {
        $dadosHistoricoArp->objeto_alteracao = $dadosFormulario['objeto_alteracao'];
        # Remove a descrição para que não aconteça erro ao atualizar
        unset($dadosHistoricoArp['descricao_tipo']);
        $dadosHistoricoArp->save();
    }



    /**
     * Método responsável em montar as informações iniciais para o tipo do item cancelado
     */
    private function montarDadosInicialItemCancelado(?ArpItemHistorico $dadosItem)
    {
        $dadosInicial = [];

        $dadosInicial['arp_historico_id'] = $dadosItem->arp_historico_id ?? null;
        $dadosInicial['valor'] = $dadosItem->valor ?? 0;
        $dadosInicial['vigencia_inicial'] = $dadosItem->vigencia_inicial ?? null;
        $dadosInicial['vigencia_final'] = $dadosItem->vigencia_final ?? null;
        $dadosInicial['item_cancelado'] = $dadosItem->item_cancelado ?? true;

        return $dadosInicial;
    }

    # Deletar as informações que foram desmarcadas pelo usuário
    private function deletarInformacoesDesmarcadas(array $tipoInicial, array $dadosForm)
    {
        # Se nenhum item for marcado, todos os registros serão excluídos
        if (!isset($dadosForm['tipo_id'])) {
            ArpHistorico::where("arp_id", $dadosForm['arp_id'])
                ->whereIn("tipo_id", $tipoInicial)->forceDelete();

            return;
        }

        # Percorre os tipos marcados no primeiro cadastro
        foreach ($tipoInicial as $tipo) {
            # Se encontrar o tipo diferente enviado para o form, remove do banco de dados
            if (in_array($tipo, $dadosForm['tipo_id']) == false) {
                ArpHistorico::where("tipo_id", $tipo)
                    ->where("arp_alteracao_id", $dadosForm['id'])->forceDelete();
            }
        }
    }
    public function criarJobReset()
    {
        $ontem = now()->subDays(1)->format('Y-m-d');

        $dadosHistoricoArp = ArpHistorico::where('quantitativo_renovado_vigencia', true)
            ->whereDate('vigencia_final', '=', $ontem)
            ->get();


         //  ->whereDate('vigencia_final', '=', Carbon::now()->subDay())s

        foreach ($dadosHistoricoArp as $historicoArp) {
            $dadosFormulario = '';
            $nomeJobAlteracao = config('arp.arp.nome_queue_alteracao_arp_quantitativo');
            AtualizarQuantitativoRenovadoArpJob::dispatch($historicoArp, $dadosFormulario)
                        ->onQueue($nomeJobAlteracao);
        }
    }

    public function update(AlteracaoArpRequest $request)
    {
        DB::beginTransaction();
        $dados = $request->all();

        try {
            $mensagemAlerta = 'Rascunho salvo com sucesso!';

            # Atualiza a alteração com o novo sequencial
            if (!$dados['rascunho']) {
                $arpAlteracao = ArpAlteracao::find($dados['id']);
                $arpAlteracao->sequencial = $this->gerarSequencialArpAlteracao($dados['rascunho'], $dados['arp_id']);
                $arpAlteracao->rascunho = $dados['rascunho'];
                $arpAlteracao->save();
                $mensagemAlerta = 'Alteração realizada com sucesso!';
            }

            #Se tiver sido removido somente o arquivo
            $this->removerArquivoUpload(ArpArquivo::class, $dados['arquivo_alteracao_clear']);

            # Se for salvar ou substituir um arquivo
            $this->salvarArquivoArp(
                $request,
                'Alteração',
                'arp/alteracao/' . $request->arp_id . '/' . $request->id,
                'arquivo_alteracao'
            );

            # Remover as marcações que foram desmarcadas pelo usuário
            $this->deletarInformacoesDesmarcadas(json_decode($dados['item_selecionado_hidden'], true), $dados);

            # Percorre a marcação do usuário
            if (isset($dados['tipo_id'])) {
                $dadosAtaInicial = null;

                foreach ($dados['tipo_id'] as $tipo) {
                    # Recupera o registro do histórico para o item marcado
                    $arpHistorico = ArpHistorico::where("tipo_id", $tipo)->where("arp_alteracao_id", $dados['id'])
                        ->first();

                    # Se for uma marcação nova ele muda o nome da função para store
                    $nomeFuncao = __FUNCTION__;
                    if (empty($arpHistorico)) {
                        $nomeFuncao = 'store';
                    }

                    $dadosAtaInicial = $this->alteracaoService->montarInformacaoInicial(
                        $tipo,
                        $dados,
                        $arpHistorico,
                        $nomeFuncao
                    );

                    #Para atualizar ou incluir uma nova marcação
                    switch ($dadosAtaInicial['descricao_tipo']) {
                        case 'Vigência':
                            $arpHistorico = $this->alteracaoService->realizarAlteracaoVigencia(
                                $arpHistorico,
                                $tipo,
                                $dados,
                                $dadosAtaInicial
                            );
                            break;
                        case 'Informativo':
                            # Se for registro novo entra no IF
                            if (empty($arpHistorico)) {
                                $dadosAtaInicial =
                                    $this->alteracaoService->montarInformacoesUpdate($dados, $tipo, $dadosAtaInicial);
                                $this->alterarInformativo($dados, $dadosAtaInicial);
                            } else {
                                $this->alterarInformativoUpdate($dados, $dadosAtaInicial);
                            }
                            break;
                        case 'Valor(es) registrado(s)':
                            # Se for registro novo entra no IF
                            if (empty($arpHistorico)) {
                                $dadosAtaInicial =
                                    $this->alteracaoService->montarInformacoesUpdate($dados, $tipo, $dadosAtaInicial);
                                $idArpHistorico = $this->alterarValorRegistrado($dados, $dadosAtaInicial);
                                $this->alterarPercentualDesconto($dados, $dadosAtaInicial, true, $idArpHistorico);
                            } else {
                                $dadosAtaInicialArray =  $dadosAtaInicial->toArray();
                                $dados['arp_historico_id'] = $arpHistorico->id;
                                $this->alterarPercentualDesconto($dados, $dadosAtaInicialArray, true, $arpHistorico->id);
                                $this->alterarValorRegistradoUpdate($dados, $dadosAtaInicial);
                            }
                            break;

                        case 'Cancelamento de item(ns)':
                            $arpHistorico = $this->alteracaoService->realizarCancelamentoItem(
                                $arpHistorico,
                                $tipo,
                                $dados,
                                $dadosAtaInicial
                            );
                            break;
                        case 'Fornecedor':
                            $this->alterarFornecedor($dados, $dadosAtaInicial->toArray(), true);
                            break;
                    }

                    if (!$dados['rascunho']) {
                        $arpHistorico->data_assinatura_alteracao_vigencia =
                            $dados['data_assinatura_alteracao_vigencia'];

                        // Remover o atributo devido não existir o campo e foi necessário inserir para as validações
                        unset($arpHistorico['descricao_tipo']);
                        $arpHistorico->save();
                    }
                }
            }

            DB::commit();

            \Alert::add('success', $mensagemAlerta)->flash();
            return redirect('/arp/alteracaoarp/' . $dados['arp_id']);
        } catch (Exception $ex) {
            DB::rollBack();
            $this->inserirLogCustomizado('gestao_ata', 'error', $ex);
            \Alert::add('error', 'Erro ao realizar a alteração!')->flash();
            return redirect()->back();
        }
    }

    /**
     * Método responsável em montar a mensagem com os empenho do tipo contrato
     * que contem o item que seja cancelado ou com alteração de valor
     */
    private function montarMensagemMinutaTipoCompra(string $mensagem, array $empenhosPorUnidade)
    {
        # Mensagem de exibição para o usuário
        $mensagem = 'Existem minuta(s) de empenho do tipo compra vinculadas para esse item, sendo elas:<br>';
        foreach ($empenhosPorUnidade as $unidade => $empenhos) {
            # Inicia o contador de minutas em andamento
            $count = 0;
            # Insere a unidade na mensagem
            $mensagem .= "Unidade $unidade:<br> Empenho(s): ";
            foreach ($empenhos as $empenho) {
                if (empty($empenho)) {
                    $count++;
                    // $empenho = "EM ANDAMENTO";
                }
                $mensagem .= "$empenho, ";
            }

            # Remove a última vírgula
            $mensagem = substr($mensagem, 0, -2);

            # Se encontrar algum minuta de empenho, insere a quantidade
            # que está com o status em andamento
            if ($count > 0) {
                $count = $this->preencherCasasDireitaEsquerda($count);
                $mensagem .= " EM ANDAMENTO ({$count})";
            }

            # Pula para a próxima unidade
            $mensagem .= "<br>";
        }

        return $mensagem;
    }

    /**
     * Método responsável em montar a mensagem com os contratos
     * que contem o item que seja cancelado ou com alteração de valor
     */
    private function montarMensagemContrato(string $mensagem, array $contratoPorUnidade)
    {
        if (!empty($mensagem)) {
            $mensagem .= "<br>";
        }

        # Mensagem de exibição para o usuário
        $mensagem .= 'Existem contrato(s) vinculados para esse item, sendo eles:<br>';
        foreach ($contratoPorUnidade as $unidade => $contrato) {
            # Inicia o contador de minutas em andamento
            // $count = 0;
            # Insere a unidade na mensagem
            $mensagem .= "Unidade $unidade:<br> Contrato(s): ";
            foreach ($contrato as $empenho) {
                // if (empty($empenho)) {
                //     $count ++;
                //     // $empenho = "EM ANDAMENTO";
                // }
                $mensagem .= "$empenho, ";
            }

            # Remove a última vírgula
            $mensagem = substr($mensagem, 0, -2);

            // # Se encontrar algum minuta de empenho, insere a quantidade
            // # que está com o status em andamento
            // if ($count > 0) {
            //     $count = $this->preencherCasasDireitaEsquerda($count);
            //     $mensagem .= " EM ANDAMENTO ({$count})";
            // }

            # Pula para a próxima unidade
            $mensagem .= "<br>";
        }

        return $mensagem;
    }

    public function pesquisarEmpenho(Request $request)
    {
        $arpItemRepository = new ArpItemRepository();

        # Busca o compra_item_id
        $itemDeCompra = $arpItemRepository->getItemDeCompraDoItemDaAta($request->all()['arpItemId']);

        # Recupera os empenhos utilizados pelo item selecionado
        $minutasDeEmpenhoVinculadasAoItem =
            $arpItemRepository->getMinutasTipoCompraPorItem($itemDeCompra->compra_item_id);

        $mensagemMinutasEmpenho =
            $this->montarMensagemMinutasEmpenhoVinculadasAoItem($minutasDeEmpenhoVinculadasAoItem);

        # Recupera os itens que estão vinculados somente nos contratos
        $contratosVinculadosAoItem  = $arpItemRepository->getContratosPorItemFornecedor(
            $itemDeCompra->compra_item_id,
            $itemDeCompra->fornecedor_id
        );
        $mensagemContratos = $this->montarMensagemContratosVinculadosAoItem($contratosVinculadosAoItem);

        $mensagem = $mensagemMinutasEmpenho;

        if ($mensagemContratos) {
            $mensagem .= '<br>' . $mensagemContratos;
        }

        return $mensagem;
    }
    
    public function exibirFornecedorAlteracao(Request $request)
    {
        $dados = $request->all();
        $arpId = $dados['idArp'];
        
        $arpItemRepository = new ArpItemRepository();
        $arpAdesaoService = new AdesaoService();
        $arpRemanejamentoService = new ArpRemanejamentoService();
        
        $itensAta =  $this->itemAlteracao($arpId, $this->crud->getCurrentEntryId());
        $itemFormatado = array();
        # Percorre o resultado obtido para ajustar o texto
        foreach ($itensAta as $item) {
            # Formata o texto para exibir uma parte e o restante
            # ficará no ícone ao lado direito
            $item['descricaodetalhada'] = Str::limit(
                trim($item['descricaodetalhada']),
                50,
                '<i class="fas fa-info-circle" title="' . $item['descricaodetalhada'] . '"></i>'
            );
            
            $item['idata'] = $arpId;
            
            $item['urlpesquisarcnpj'] = url("arp/alteracaoarp/{$arpId}/pesquisarcnpjfornecedor");
            $item['novo_cnpj'] = null;
            $item['novo_nome'] = null;

            $minutaPorItem =
                $arpItemRepository->getMinutasTipoCompraPorItemEFornecedor($item->compra_item_id, $item->fornecedor_id);
            
            $contratoPorItem =
                $arpItemRepository->getContratosPorItemFornecedor(
                    $item->compra_item_id,
                    $item->compra_item_fornecedor_id
                );

            $item['item_utilizado_fornecedor'] = false;

            if ($minutaPorItem->count() > 0 || $contratoPorItem->count() > 0) {
                $item['item_utilizado_fornecedor'] = true;
                $itemFormatado[] = $item;
                continue;
            }

            $remanejamentos =
                $arpRemanejamentoService->getItemRemanejamentoPorCompraItemFornecedorId(
                    $item->compra_item_fornecedor_id
                );

            $item['item_adesao_remanejamento_pendente'] = false;

            if ($remanejamentos->count() > 0) {
                $item['item_utilizado_fornecedor'] = true;

                $item['item_adesao_remanejamento_pendente'] =
                    $arpRemanejamentoService->itemFornecedorPendenteAnaliseRemanejamento($remanejamentos);
                $itemFormatado[] = $item;
                continue;
            }

            $adesaoAtaPorItem = $arpAdesaoService->getSolicitacaoAdesaoAceitaPorItemAta($item['id']);

            if ($adesaoAtaPorItem->count() > 0) {
                $item['item_utilizado_fornecedor'] = true;

                $item['item_adesao_remanejamento_pendente'] =
                    $arpAdesaoService->itemFornecedorPendenteAnaliseAdesao($adesaoAtaPorItem);
                $itemFormatado[] = $item;
                continue;
            }

            $item['item_alterado_fornecedor'] =
                $this->getItemExisteAlteracaoFornecedor($arpId, $item->compra_item_fornecedor_id);
            
            if ($item['item_alterado_fornecedor']) {
                $itemFormatado[] = $item;
                continue;
            }

            if ($dados['idArpAlteracao'] > 0) {
                $itemHistoricoAlteracaoFornecedor  =
                    $this->recuperarDadosAlteracaoFornecedor(
                        $dados['idArpAlteracao'],
                        $item->compra_item_fornecedor_id
                    );

                $item['novo_cnpj'] = $itemHistoricoAlteracaoFornecedor->novo_cnpj ?? null;

                $item['novo_nome'] = $itemHistoricoAlteracaoFornecedor->novo_nome  ?? null;
                $item['fornecedor_novo'] = true;

                if (empty($item['novo_nome']) && !empty($item['novo_cnpj'])) {
                    $dadosFornecedor =
                        Fornecedor::where('cpf_cnpj_idgener', $itemHistoricoAlteracaoFornecedor->novo_cnpj)->first();
                    $item['novo_nome'] = $dadosFornecedor->nome;
                    $item['fornecedor_novo'] = false;
                }
            }
            
            $itemFormatado[] = $item;
        }
        
        return $itemFormatado;
    }
    
    public function pesquisarCnpjFornecedor(Request $request)
    {
        $fornecedorService = new FornecedoresService();
        $cnpj = $request->all()['cnpj'];
        $compraItemId = $request->all()['compraItemId'];
        $cnpjExisteItem = (new CompraItemFornecedorService())->fornecedorExisteItemPorCnpj($compraItemId, $cnpj);

        if ($cnpjExisteItem) {
            $mensagem = "Não pode realizar a alteração para o fornecedor de CPF/CNPJ {$cnpj}, devido existir no item";
            return response()->json(['sucesso' => false, 'mensagem' => $mensagem]);
        }

        $mensagem = $fornecedorService->recuperarNomeFornecedorPorCpfCnpj($cnpj) ?? '';

        return response()->json(['sucesso' => true, 'mensagem' => $mensagem]);
    }
    
    private function areaFornecedorAlteracaoItens()
    {
        $itens = null;
        
        $column = [
            'nomefornecedor' => 'Fornecedor (Classificação)',
            'numero' => 'Número',
            'descricaodetalhada' => 'Descrição',
            'cnpjfornecedornovo' => 'CPF/CNPJ',
            'nomefornecedornovo' => 'Nome'
        ];
        
        
        $this->crud->addField([
            'name' => 'areaAlteracaoFornecedor',
            'type' => 'table_selecionar_item_custom',
            'classArea' => 'areaAlteracaoFornecedor',
            'idArea' => 'areaAlteracaoFornecedor',
            'idTable' => 'tabelaAlteracaoFornecedor',
            'label' => 'Itens da ata para alteração de fornecedor',
            'column' => $column,
            'value' => $itens,
            'datatable' => false
        ]);
    }
    
    private function alterarFornecedor(array $dadosFormulario, array $dadosAtaInicial, bool $telaEditar = false)
    {
        if (!isset($dadosFormulario['item_alteracao_fornecedor'])) {
            return ;
        }

        if (!$telaEditar) {
            $idArpHistorico = ArpHistorico::create($dadosAtaInicial)->id;
        }
        
        if ($telaEditar) {
            $idArpHistorico = $dadosAtaInicial['id'];
        }
        
        # Remove os valores que não foram utilizados
        $arrayCnpjAlterado = array_filter($dadosFormulario['novo_cnpj_fornecedor']);
        $arrayNomeAlterado = array_filter($dadosFormulario['novo_nome_cnpj_fornecedor'] ?? []);
        
        $arpItemRepository = new ArpItemRepository();
        $fornecedorService = new FornecedoresService();

        foreach ($dadosFormulario['item_alteracao_fornecedor'] as $ItemAtaId) {
            $novoNome = $arrayNomeAlterado[$ItemAtaId] ?? null;
            $novoCpj = $arrayCnpjAlterado[$ItemAtaId] ?? null;
            
            $itemAtaOrigem = $arpItemRepository->getItemAtaUnico($ItemAtaId);
            $compraItemFornecedorOrigem = $itemAtaOrigem->item_fornecedor_compra;
            $fornecedorOrigem = $compraItemFornecedorOrigem->fornecedor;
            
            $dadosNovoFornecedor = null;

            # alteracao do nome
            if (empty($novoCpj) && !empty($novoNome) && !$dadosFormulario['rascunho']) {
                $dadosNovoFornecedor = $fornecedorOrigem->replicate();
                $dadosNovoFornecedor->nome = $novoNome;
                $dadosNovoFornecedor->save();
            }

            # alteração do cnpj
            if (!empty($novoCpj) && empty($novoNome) && !$dadosFormulario['rascunho']) {
                $dadosNovoFornecedor =  $fornecedorService->recuperarFornecedorPorCpfCnpj($novoCpj);
                if (empty($dadosNovoFornecedor)) {
                    $dadosNovoFornecedor = $fornecedorOrigem->replicate();
                    $dadosNovoFornecedor->cpf_cnpj_idgener = $novoCpj;
                    $dadosNovoFornecedor->save();
                }
            }
            
            # criação de um novo fornecedor
            if (!empty($novoCpj) && !empty($novoNome) && !$dadosFormulario['rascunho']) {
                $dadosNovoFornecedor = $fornecedorService->inserirFornecedorAlteracaoAta($novoCpj, $novoNome);
            }
            
            $compraItemFornecedorDestino = null;
            
            if (!$dadosFormulario['rascunho']) {
                $compraItemFornecedorDestino = $compraItemFornecedorOrigem->replicate();
                $compraItemFornecedorDestino->fornecedor_id = $dadosNovoFornecedor->id;
                $compraItemFornecedorDestino->ni_fornecedor = $dadosNovoFornecedor->cpf_cnpj_idgener;
                $compraItemFornecedorDestino->save();
                
                $compraItemFornecedorOrigem->situacao = false;
                $compraItemFornecedorOrigem->save();
                
                $novoItemAta = $itemAtaOrigem->replicate();
                $novoItemAta->compra_item_fornecedor_id = $compraItemFornecedorDestino->id;
                $novoItemAta->save();

                $situacaoAdesao = [];

                $itemAtaOrigem->itensAdesao->each(function ($item) use (&$situacaoAdesao) {
                    $situacaoAdesao[] = $item->adesao->situacao->descricao;
                });

                if (in_array('Cancelada pelo solicitante', $situacaoAdesao)) {
                    $itemAtaOrigem->delete();
                } else {
                    $itemAtaOrigem->forceDelete();
                }
            }
            
            ArpItemHistoricoAlteracaoFornecedor::updateOrCreate(
                [
                    'arp_historico_id' => $idArpHistorico,
                    'compra_item_fornecedor_origem_id' => $compraItemFornecedorOrigem->id,
                    'fornecedor_origem_id' => $fornecedorOrigem->id
                ],
                [
                    'compra_item_fornecedor_destino_id' => $compraItemFornecedorDestino->id ?? null,
                    'novo_cnpj' => $novoCpj,
                    'novo_nome' => $novoNome
                ]
            );
        }
    }
    
    private function recuperarDadosAlteracaoFornecedor(int $idArpAlteracao, int $compraItemFornecedorId)
    {
        return
            ArpItemHistoricoAlteracaoFornecedor::join(
                'arp_historico',
                'arp_historico_id',
                'arp_historico.id'
            )
                ->where('arp_alteracao_id', $idArpAlteracao)
                ->where('compra_item_fornecedor_origem_id', $compraItemFornecedorId)
                ->select('arp_item_historico_alteracao_fornecedor.*')
                ->first();
    }
    
    private function getItemExisteAlteracaoFornecedor(int $arpId, int $compraItemFornecedorId)
    {
        $itemHistoricoAlteracaoFornecedor = $this->queryBaseItemExisteAlteracaoFornecedor($arpId)
            ->where('compra_item_fornecedor_destino_id', $compraItemFornecedorId)
            ->count();
        
        if ($itemHistoricoAlteracaoFornecedor > 0) {
            return true;
        }
        
        return false;
    }
    
    private function getItemExisteAlteracaoFornecedorCancelamento(int $arpId, int $compraItemFornecedorId)
    {
        $itemHistoricoAlteracaoFornecedor = $this->queryBaseItemExisteAlteracaoFornecedor($arpId)
            ->where('compra_item_fornecedor_destino_id', $compraItemFornecedorId)
            ->count();
        
        if ($itemHistoricoAlteracaoFornecedor > 0) {
            return true;
        }
        
        return false;
    }
    
    private function queryBaseItemExisteAlteracaoFornecedor(int $arpId)
    {
        return $itemHistoricoAlteracaoFornecedor =
            ArpItemHistoricoAlteracaoFornecedor::join('arp_historico', 'arp_historico_id', 'arp_historico.id')
                ->join('codigoitens', function ($query) {
                    $query->on('arp_historico.tipo_id', 'codigoitens.id')
                        ->where('codigoitens.descricao', 'Fornecedor');
                })
                ->join('arp_alteracao', function ($query) {
                    $query->on('arp_alteracao.id', 'arp_alteracao_id')
                        ->where('arp_alteracao.rascunho', false);
                })
                ->where('arp_historico.arp_id', $arpId);
    }

    public function montarCampoValorDesconto(
        string $classInputValorDesconto,
        string $nameInputValorDesconto,
        ?string $criterioJulgamento,
        ?float $valorPercentual = null
    ) {
        $disabledCampo = ($criterioJulgamento === 'V' || $criterioJulgamento !== 'D' ? ' disabled' : '');

        $campoHtml = "<div class='br-input'>
                            <input  type='text'
                                    class='{$classInputValorDesconto}'
                                    name='{$nameInputValorDesconto}'
                                    data-criterio-julgamento='{$criterioJulgamento}'";

        if (!empty($valorPercentual)) {
            $campoHtml .= "value='{$valorPercentual}'";
        }

        $campoHtml .="{$disabledCampo} /> </div>";

        return $campoHtml;
    }

    public function montarCampoValorRegistrado(
        string $classInputValorDesconto,
        string $nameInputValorUnitario,
        ?string $criterioJulgamento,
        ?float $valorLancado = null
    ) {
        $disabledCampo = ($criterioJulgamento === 'D' ? ' disabled' : '');

        $campoHtml = "<div class='br-input'>
                                <input type='text'
                                class='{$classInputValorDesconto}'
                                name='{$nameInputValorUnitario}'
                                data-criterio-julgamento='{$criterioJulgamento}'";

        if (!empty($valorLancado)) {
            $campoHtml .= "value='{$valorLancado}'";
        }

        $campoHtml .="{$disabledCampo} /> </div>";

        return $campoHtml;
    }

    public function montarCheckCancelarItem(
        int $arpItemId,
        string $classInputItemCancelado,
        string $nameInputValorUnitario,
        ?string $itemChecked = null
    ) {
        $idCheckBoxItemCancelado = "chk_{$arpItemId}";

        $campoHtml = "<div class='br-checkbox'>
                        <input id='{$idCheckBoxItemCancelado}'
                        type='checkbox'
                        class='{$classInputItemCancelado}'
                        name='item_cancelado[{$arpItemId}]'
                        onclick='bloquearNovoValor(`$nameInputValorUnitario`, this)'
                        value='1'";

        if (!empty($itemChecked)) {
            $campoHtml .= $itemChecked;
        }

        $campoHtml .=' />';
        $campoHtml .= "<label for='{$idCheckBoxItemCancelado}'></label> </div>";

        return $campoHtml;
    }

    public function salvarItemAlteracaoValor(
        array $novoValorUnitario,
        ?string $dataAssinaturaAlteracaoVigencia,
        int $idArpHistorico,
        bool $rascunho,
        int $idAta
    ) {
        # Remove os valores que não foram utilizados
        $arrayValorAlterado = array_filter($novoValorUnitario);

        # Percorre os valores informados pelo usuário ao preencher os valores
        foreach ($arrayValorAlterado as $numeroItemAlterado => $valorItemAlteracao) {
            $item = ArpItem::find($numeroItemAlterado);

            $dadosItem = $item->toArray();
            $dadosItem['vigencia_inicial'] = $dataAssinaturaAlteracaoVigencia;
            $dadosItem['arp_historico_id'] = $idArpHistorico;
            $dadosItem['item_cancelado'] = false;
            $dadosItem['arp_item_id'] = $item->id;

            # Se o numero do item selecionado for igual ao numero alterado, insere o valor informado e informa
            # que o valor do item está sendo atualizado

            if ($item->id == $numeroItemAlterado) {
                $dadosItem['valor_alterado'] = true;
                $dadosItem['valor'] =
                    number_format(str_replace(",", ".", str_replace(".", "", $valorItemAlteracao)), 4, '.', '');

                # Se a ação não for um rascunho, atualiza a tabela fornecedor
                if (!$rascunho) {
                    # Altera a coluna valor_unitario na tabela compra_item_fornecedor
                    # para que reflita no valor do item na hora que for realizar
                    # uma minuta de empenho
                    $item->item_fornecedor_compra->valor_unitario = $dadosItem['valor'];
                    $item->item_fornecedor_compra->valor_negociado =
                        $dadosItem['valor'] *
                        $item->item_fornecedor_compra->quantidade_homologada_vencedor;
                    $item->item_fornecedor_compra->save();
                }
            }

            ArpItemHistorico::create($dadosItem);
        }

        $this->recalcularValorTotalAta($rascunho, $idAta);
    }



    private function recalcularValorTotalAta(bool $ataEmRascunho, int $idAta)
    {
        if (!$ataEmRascunho) {
            $arrayItensAta = ArpItem::where('arp_id', $idAta)->select('compra_item_fornecedor_id')
                                ->get()->pluck('compra_item_fornecedor_id')->toArray();

            $novoValorAta =
                (new CompraItemFornecedorRepository())->getValorTotalItemFornecedor($arrayItensAta);
            $arp = Arp::find($idAta);
            $arp->valor_total = $novoValorAta;
            $arp->save();
        }
    }
}
