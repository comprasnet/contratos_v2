<?php

namespace App\Http\Controllers\Arp;

use App\Http\Requests\ArpRemanejamentoAnaliseUnidadeItemRequest;
use App\Http\Traits\Arp\ArpRemanejamentoTrait;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\CommonFields;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Http\Traits\LogTrait;
use App\Models\Arp;
use App\Models\ArpHistorico;
use App\Models\ArpRemanejamento;
use App\Models\ArpRemanejamentoItem;
use App\Models\BackpackUser;
use App\Models\CodigoItem;
use App\Models\CompraItemMinutaEmpenho;
use App\Models\CompraItemUnidade;
use App\Models\Compras;
use App\Repositories\Arp\ArpRemanejamentoItemRepository;
use App\Repositories\Arp\ArpRemanejamentoRepository;
use App\Repositories\CodigoItemRepository;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

/**
 * Class ArpRemanejamentoAnaliseGerenciadoraAtaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArpRemanejamentoAnaliseGerenciadoraAtaCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    use ArpRemanejamentoTrait;
    use CommonFields;
    use ImportContent;
    use LogTrait;
    use BuscaCodigoItens;
    use CompraTrait;
    
    # Status da análise
    private $descricaoStatusAnalise = 'Aguardando aceitação da unidade gerenciadora';
    
    private $urlBase = null;
    
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->urlBase =
            config('backpack.base.route_prefix') . "/arp/remanejamento/analisar/gerenciadoraata";
        
        CRUD::setModel(\App\Models\ArpRemanejamento::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/arp/remanejamento/analisar');
        
        $this->exibirTituloPaginaMenu(
            'Analisar solicitação de remanejamento unidade gerenciadora da ata',
            null,
            false
        );
        
        $this->bloquearBotaoPadrao($this->crud, ['create', 'delete']);
    }
    
    protected function setupShowOperation()
    {
        $this->crud->set('show.contentClass', 'col-md-12');
        
        $this->crud->set('show.setFromDb', false);
        
        $this->camposShow();
        
        # Exibir o anexo e os itens, igual ao show na solicitação do remanejamento
        $this->exibirAnexoItemRemanejamento($this->crud);
        
        # Alterando a rota padrão para que o botão no edit fique com a URL correta
        $url = "{$this->urlBase}/{$this->crud->entry->id}/edit";
        $this->crud->url_button_custom = url($url);
        
        $this->crud->addButtonFromView('line', 'button_edit_rascunho', 'button_edit_rascunho', 'end');
        
        $remanejamento = $this->crud->getEntry($this->crud->getCurrentEntryId());
        
        $arraySituacaoPodeEditar = ['Aguardando aceitação da unidade gerenciadora', 'Aguardando aceitação'];
        if (!in_array($remanejamento->getSituacao(), $arraySituacaoPodeEditar)) {
            $this->crud->removeButton('update');
        }
    }
    
    /**
     * Método responsável em montar o texto para o tooltip
     */
    // private function montarTextoTooltip(array $dadosItem, string $nomeInformacao)
    // {
    //     return "Informações sobre {$nomeInformacao} do item {$dadosItem['numero_item']} e
    //         unidade {$dadosItem['unidade_origem']}";
    // }
    
    /**
     * Método responsável em exibir a informação da na coluna Quantidade autorizada unidade gestora ata
     */
    private function montarInformacaoCampoQuantidadeAutorizadaUnidadeGestoraAta(ArpRemanejamentoItem $item)
    {
        # Recuperar o valor máximo para autorizar, se não existir valor no campo quantidade_aprovada_participante
        # então a análise pulou a etapa da análise por parte da gerenciadora do item
        $quantidadeInicialGestoraAta = $item->quantidade_aprovada_participante ?? $item->quantidade_solicitada;
        
        if ($this->existeSaldoUnidadeParaRemanejar($item->itemUnidade, $quantidadeInicialGestoraAta) == false) {
            return 'Não existe saldo da unidade para remanejar';
        }
        
        # Recupera o modo que o usuário pode digitar a quantidade no campo
        $step = $this->retornarStepCampoQuantidade($item->itemUnidade->compraItens->tipoItem);
        
        $quantidadeAnalisadaGerenciadora = $quantidadeInicialGestoraAta;
        
        if (!empty($item->quantidade_aprovada_gerenciadora)) {
            $quantidadeAnalisadaGerenciadora = $item->quantidade_aprovada_gerenciadora;
        }

        $quantidadeSaldoItemUnidade = $item->itemUnidade->quantidade_saldo;
        
        if ($quantidadeSaldoItemUnidade < $quantidadeInicialGestoraAta) {
            $quantidadeInicialGestoraAta = $quantidadeSaldoItemUnidade;
        }
        
        $readonly = false;
        if (isset($item->situacaoAnaliseItem) && $item->situacaoAnaliseItem->descricao === 'Aceitar') {
            $readonly = true;
        }
        
        return $this->montarCampoHtmlQuantidade(
            $item->id,
            $quantidadeInicialGestoraAta,
            $step,
            $quantidadeAnalisadaGerenciadora,
            $readonly
        );
    }
    
    /**
     * Método responsável em exibir os itens para o usuário analisar no remanejamento
     */
    private function exibirItemAnaliseRemanejamento(Collection $itemRemanejamento)
    {
        # Array responsável em montar o cabeçalho da ata
        $column = [
            // 'expandir' => 'Expandir',
            'informacoes_gerais' => 'Inform.',
            'situacao' => 'Situação',
            // 'dados_ata'=>'Dados ata',
            // 'dados_usuario_solicitante'=>'Dados solicitante',
            'numero_item' => 'Número',
            'descricao_item' => 'Descrição',
            // 'unidade_origem'=>'Unidade origem',
            'saldo_remanejamento' => 'Saldo disponível',
            'quantidade_solicitada' => 'Quant. solicitada',
            'quantidade_aprovada_unidade_origem' => 'Quant. autorizada unidade origem',
            'quantidade_aprovada_unidade_gerenciadora_ata' => 'Quant. autorizada unidade gestora ata',
            'detail_row' =>
                [
                    'analise_item' => json_encode(['coldMd' => 3]),
                    'justificativa_analise' => json_encode(['coldMd' => 9])
                ]
        ];
        
        $itemFormatado = [];
        
        $codigoItemRepository = new CodigoItemRepository();
        
        # Recuperar os itens para o usuário marcar na coluna Análise do item
        $statusAnalise = $codigoItemRepository->recuperarStatusItemAnalise();
        
        # Percorre os itens do remanejanento
        foreach ($itemRemanejamento as $item) {
            $montarLinhaItem = array();
            
            $compraItem = $item->itemUnidade->compraItens;
            
            $montarLinhaItem['numero_item'] = $compraItem->numero;
            $montarLinhaItem['descricao_item'] = Str::limit(
                trim($compraItem->descricaodetalhada),
                50,
                '<i class="fas fa-info-circle"
                title="' . $compraItem->descricaodetalhada . '"></i>'
            );
            
            $compraItemUnidade = $item->itemUnidade;
            
            $montarLinhaItem['saldo_remanejamento'] = $this->formatValuePtBR($compraItemUnidade->quantidade_saldo, 0);
            
            
            $montarLinhaItem['quantidade_solicitada'] = $this->formatValuePtBR($item->quantidade_solicitada, 0);
            
            $quantidadeAprovadaUnidadeGerenciadoraItem = $item->quantidade_aprovada_participante ?
                $this->formatValuePtBR($item->quantidade_aprovada_participante, 5) : '';
            
            
            $montarLinhaItem['quantidade_aprovada_unidade_origem'] = $quantidadeAprovadaUnidadeGerenciadoraItem;
            
            # Montar a informação na coluna Quantidade autorizada unidade gestora ata
            $montarLinhaItem['quantidade_aprovada_unidade_gerenciadora_ata'] =
                $this->montarInformacaoCampoQuantidadeAutorizadaUnidadeGestoraAta($item);
            $montarLinhaItem['id_unico'] = $item->id;

            if ($this->existeSaldoUnidadeParaRemanejar(
                $item->itemUnidade,
                $item->quantidade_aprovada_participante
            ) == false) {
                $montarLinhaItem['informacoes_gerais'] = '';
                $montarLinhaItem['situacao'] = 'teste';
                $montarLinhaItem['detail_row'][$item->id] =
                    [
                        'analise_item' => '',
                        'justificativa_analise' => '',
                    ];
                $itemFormatado[] = $montarLinhaItem;
                continue;
            }

            $descricaoStatusAnalise = $item->situacaoAnaliseItem->descricao ?? 'Item não analisado';
            $montarLinhaItem['situacao'] = $this->exibirCorStatus($descricaoStatusAnalise);
            
            # Montar o botão para exibir as informações sobre a compra
            $montarLinhaItem['informacoes_gerais'] = $this->botaoExibirInformacoesGerais($item);
            
            if (empty($item->quantidade_aprovada_gerenciadora)) {
                $quantidadeAprovadaAnalise =
                    $item->quantidade_aprovada_participante ?? $item->quantidade_solicitada;
            }
            
            if (!empty($item->quantidade_aprovada_gerenciadora)) {
                $quantidadeAprovadaAnalise = $item->quantidade_aprovada_gerenciadora;
            }
            
            $montarLinhaItem['detail_row'][$item->id] =
                [
                    'analise_item' => $this->montarRadioAnaliseItem(
                        $statusAnalise,
                        $item->id,
                        $item->status_analise_id,
                        (int) $quantidadeAprovadaAnalise
                    ),
                    'justificativa_analise' =>
                        $this->montarHtmlCampoJustificativa(
                            $item->id,
                            $item->justificativa_analise,
                            $descricaoStatusAnalise
                        ),
                ];
            
            $itemFormatado[] = $montarLinhaItem;
        }
        
        $this->addTableCustom(
            $column,
            'table_selecionar_item_generico',
            'Selecionar o fornecedor para exibir a informação',
            'col-md-12 pt-5',
            'areaItemAdesao',
            null,
            100,
            $itemFormatado,
            ' ',
            '[3,"ASC"]',
            'table_selecionar_item_generico',
            'table_selecionar_item_generico'
        );
    }
    
    /**
     * Método responsável em exibir as informações na tela da análise para o usuário
     */
    private function exibirDetalheInformacaoAnalise(ArpRemanejamento $remanejamento)
    {
        $this->exibirCamposLabelAnalise(
            $this->crud,
            $remanejamento,
            'Aguardando aceitação da unidade gerenciadora da ata'
        );
    }
    
    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');
        $this->importarDatatableForm();
        $arquivoJS = [
            'assets/js/admin/forms/arp_remanejamento_analise.js',
            'packages/backpack/base/js/padrao_gov/jquery.mask.js'
        ];
        
        $this->importarScriptJs($arquivoJS);
        
        $remanejamento = $this->crud->getEntry($this->crud->getCurrentEntryId());
        
        $this->exibirDetalheInformacaoAnalise($remanejamento);
        
        $itemRemanejamentoRepository = new ArpRemanejamentoItemRepository();
        
        # Recupera os itens para serem analisados
        $itemAnalise =
            $itemRemanejamentoRepository->getStatusItensRemanejamento(
                $this->crud->getCurrentEntryId(),
                $this->descricaoStatusAnalise,
                session('user_ug_id'),
                true
            );
        
        # Método responsável em montar as informações para exibir na tabela de itens
        $this->exibirItemAnaliseRemanejamento($itemAnalise);
        
        # Criação da modal
        $this->addModalCustom('modalInformacoesDados', 'modalInformacoesDados', 'Informações');
        
        # Criação dos botões customizados
        $this->crud->button_custom = [
            [
                'button_text' => 'Salvar Rascunho',
                'button_id' => 'rascunho',
                'button_name_action' => 'rascunho',
                'button_value_action' => '1',
                'button_icon' => 'fas fa-edit',
                'button_tipo' => 'secondary'
            ],
            [
                'button_text' => 'Finalizar análise',
                'button_id' => 'adicionar',
                'button_name_action' => 'rascunho',
                'button_value_action' => '0',
                'button_icon' => 'fas fa-save',
                'button_tipo' => 'primary'
            ],
        ];
        
        # Para desabilitar o foco no primeiro campo da tela
        $this->crud->setAutoFocusOnFirstField(false);
        
        
        # Alterando a rota padrão para que o botão no edit fique com a URL correta
        $url = "{$this->urlBase}/{$this->crud->entry->id}";
        $this->crud->url_form_update_custom = url($url);
    }
    
    /**
     * Método responsável em alterar o saldo da unidade
     */
    private function alterarQuantidadeSaldoUnidade(
        ArpRemanejamentoItemRepository $itemRemanejamentoRepository,
        float $quantidadeAprovadaGerenciadora,
        int $idItemUnidade
    ) {

        # Recuperar o item do remanejamento
        $itemRemanejamento = $itemRemanejamentoRepository->getRemanejamentoItemUnico($idItemUnidade);
        # Recuperar o registro da tabela compra item unidade que vai ser retirado
        $itemUnidadeRetirar = $itemRemanejamento->itemUnidade;
        
        
        # Recuperar o registro da tabela compra item unidade que vai receber
        $itemUnidadeReceber = $itemRemanejamento->itemUnidadeReceber;

        # Reduzir o saldo da unidade que liberou a quantidade
        $itemUnidadeRetirar->quantidade_autorizada -= $quantidadeAprovadaGerenciadora;
        
        $itemUnidadeRetirar->save();

        $saldoUnidadeRetirar = $this->retornaSaldoAtualizado(
            $itemUnidadeRetirar->compra_item_id,
            $itemUnidadeRetirar->unidade_id,
            $itemUnidadeRetirar->id
        );

        $itemUnidadeRetirar->quantidade_saldo = (int)$saldoUnidadeRetirar->saldo;

        $itemUnidadeRetirar->save();

        # Atribuir o saldo para quem solicitou

        $itemUnidadeReceber->quantidade_autorizada += $quantidadeAprovadaGerenciadora;
        

        $itemUnidadeReceber->save();

        $saldoUnidadeReceber = $this->retornaSaldoAtualizado(
            $itemUnidadeReceber->compra_item_id,
            $itemUnidadeReceber->unidade_id,
            $itemUnidadeReceber->id
        );

        $itemUnidadeReceber->quantidade_saldo = $saldoUnidadeReceber->saldo;
        $itemUnidadeReceber->save();
    }
    
    public function update(ArpRemanejamentoAnaliseUnidadeItemRequest $request)
    {
        $dadosForm = $request->all();

        $itemRemanejamentoRepository = new ArpRemanejamentoItemRepository();
        
        try {
            DB::beginTransaction();
            $mensagem = 'Item(ns) salvo(s) com sucesso';
            $idStatusItemGerenciadoraItem = null;
            $statusAnaliseUnidadeGerenciadora = 'Analisado pela unidade gerenciadora da ata';


            # Se for finalizar a análise, então recuperamos o ID para finalizar a análise e mensagem exibição no alerta

            $mensagem = 'Item(ns) analisado(s) com sucesso';
            $idStatusItemGerenciadoraItem =
            $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', $statusAnaliseUnidadeGerenciadora);
            # Recuperar os status para os itens analisados
            $statusItemAnalisado = $this->retornaIdsCodigoItem(
                'Tipo de Ata de Registro de Preços',
                ['Item Negado', 'Item Aceito Parcialmente', 'Item Aceito']
            );
            
            $codigoItemRepository = new CodigoItemRepository();
            
            $existeItemAceito = false;
            $existeItemAceitoParcialmente = false;
            $existeItemNegado = false;

            # Percorrer os itens filtrados dentro do Request
            foreach ($dadosForm['quantidade_analisada'] as $idItem => $quantidadeAnalisada) {
                # Recuperar o status da análise informada pelo usuário
                $statusAnalise = $dadosForm['status_id'][$idItem] ?? null;
                
                # Recuperar a justificativa da análise informada pelo usuário
                $motivoAnalise = $dadosForm['motivo_analise'][$idItem] ?? null;
                
                # Informar o id para atualizar o registro
                $campoCondicional = ['id' => $idItem];
                
                $camposAprovacaoAnalise = array();
                $camposAprovacaoAnalise['status_analise_id'] = $statusAnalise;
                $camposAprovacaoAnalise['justificativa_analise'] = $motivoAnalise;
                $camposAprovacaoAnalise['quantidade_aprovada_gerenciadora'] = $quantidadeAnalisada;


                # Se o usuário finalizar a análise, então incluímos  com a data da aprovação e o id do usuário
                # e se tiver marcado o status
                if (!$dadosForm['rascunho'] && !empty($camposAprovacaoAnalise['status_analise_id'])) {
                    $camposAprovacaoAnalise['id_usuario_aprovacao_gerenciadora'] = backpack_user()->id;
                    $camposAprovacaoAnalise['data_aprovacao_gerenciadora'] = Carbon::now()->format('Y-m-d H:i:s');
                    
                    $codigoItem =
                        $codigoItemRepository->getCodigoItemUnico($camposAprovacaoAnalise['status_analise_id']);
                    $idStatusItemGerenciadora = null;
                    $idStatusRemanejamento = null;

                    switch ($codigoItem->descricao) {
                        case 'Aceitar':
                            $idStatusItemGerenciadora = $statusItemAnalisado['Item Aceito'];
                            $idStatusRemanejamento = $statusItemAnalisado['Item Aceito'];
                            $existeItemAceito = true;
                            break;
                        case 'Aceitar Parcialmente':
                            $idStatusItemGerenciadora = $statusItemAnalisado['Item Aceito Parcialmente'];
                            $idStatusRemanejamento = $statusItemAnalisado['Item Aceito Parcialmente'];
                            $existeItemAceitoParcialmente = true;
                            break;
                        case 'Negar':
                            $idStatusItemGerenciadora = $camposAprovacaoAnalise['status_analise_id'];
                            $idStatusRemanejamento = $statusItemAnalisado['Item Negado'];
                            $existeItemNegado = true;
                            break;
                    }
                    
                    $camposAprovacaoAnalise['status_analise_id'] = $idStatusItemGerenciadora;
                    $camposAprovacaoAnalise['situacao_id'] = $idStatusRemanejamento;

                    # Alterar a quantidade do saldo da unidade que liberou para quem solicitou
                    $this->alterarQuantidadeSaldoUnidade(
                        $itemRemanejamentoRepository,
                        $camposAprovacaoAnalise['quantidade_aprovada_gerenciadora'],
                        $idItem
                    );
                }
                # Atualizar o registro do item do remanejamento
                $itemRemanejamentoRepository->updateRemanejamentoItem($campoCondicional, $camposAprovacaoAnalise);
            }

            $itensFaltandoAprovacao =
                $itemRemanejamentoRepository->getStatusItensRemanejamento(
                    $dadosForm['id'],
                    $this->descricaoStatusAnalise,
                    session('user_ug_id')
                );

   
            if (!$dadosForm['rascunho'] && $itensFaltandoAprovacao->count() == 0) {
                $arpRemanejamentoRepository = new ArpRemanejamentoRepository();
                $campoUpdate = ['situacao_id' => $idStatusItemGerenciadoraItem];

                $contagemItemPorStatus =
                    (new ArpRemanejamentoItemRepository())->getContagemItemRemanejamentoPorStatus(
                        $dadosForm['id'],
                        'Aguardando aceitação da unidade gerenciadora'
                    );

                # Se todos os itens foram negados
                if ($contagemItemPorStatus['quantidadeNegar'] > 0 &&
                    $contagemItemPorStatus['quantidadeItemFaltandoAnalisar'] == 0 &&
                    $contagemItemPorStatus['quantidadeAceito'] == 0 &&
                    $contagemItemPorStatus['quantidadeAceitoParcial'] == 0
                ) {
                    $idStatusRemanejamentoNegado =
                        $this->retornaIdCodigoItem(
                            'Tipo de Ata de Registro de Preços',
                            'Negado pela unidade gerenciadora da ata'
                        );
                    $campoUpdate = ['situacao_id' => $idStatusRemanejamentoNegado];
                }

                // Se  todos os itens foram aceitos
                if ($contagemItemPorStatus['quantidadeNegar'] == 0 &&
                    $contagemItemPorStatus['quantidadeItemFaltandoAnalisar'] == 0 &&
                    $contagemItemPorStatus['quantidadeAceito'] > 0 &&
                    $contagemItemPorStatus['quantidadeAceitoParcial'] == 0
                ) {
                    $idStatusRemanejamentoNegado =
                        $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', 'Aceita');
                    $campoUpdate = ['situacao_id' => $idStatusRemanejamentoNegado];
                }

                // Se aceitarem o valor parcial, um item aceito
                if ($contagemItemPorStatus['quantidadeNegar'] == 0 &&
                    $contagemItemPorStatus['quantidadeItemFaltandoAnalisar'] == 0 &&
                    $contagemItemPorStatus['quantidadeAceito'] == 0 &&
                    $contagemItemPorStatus['quantidadeAceitoParcial'] > 0
                ) {
                    $idStatusRemanejamentoNegado =
                        $this->retornaIdCodigoItem(
                            'Tipo de Ata de Registro de Preços',
                            'Aceito Parcial'
                        );
                    $campoUpdate = ['situacao_id' => $idStatusRemanejamentoNegado];
                }

                if ($contagemItemPorStatus['quantidadeItemFaltandoAnalisar'] == 0) {
                    $arpRemanejamentoRepository->updateRemanejamento($dadosForm['id'], $campoUpdate);
                }
            }



            DB::commit();

            \Alert::add('success', $mensagem)->flash();
            return redirect('arp/remanejamento/analisar');
        } catch (Exception $ex) {
            DB::rollBack();
            $this->inserirLogCustomizado('gestao_ata', 'error', $ex);
            \Alert::add('error', 'Erro ao realizar análise')->flash();
            return redirect('arp/remanejamento/analisar');
        }
    }
}
