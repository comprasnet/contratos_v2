<?php

namespace App\Http\Controllers\Arp;

use App\Http\Controllers\Api\ConcentradorCompras\CompraSisrp;
use App\Http\Controllers\Operations\DeleteOperation;
use App\Http\Controllers\Operations\UpdateOperation;
use App\Http\Requests\ArpRequest as StoreRequest;
use App\Http\Requests\ArpRequest;
use App\Http\Requests\ArpUpdateRequest;
use App\Http\Traits\Arp\ArpItemHistoricoTrait;
use App\Http\Traits\Arp\ArpAlteracaoTrait;
use App\Http\Traits\Arp\ArpTrait;
use App\Http\Traits\Arp\ArpItemTrait;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\CommonFields;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\Formatador;
use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use App\Jobs\GenerateRelatorioArpPdfJob;
use App\Models\ArpArquivo;
use App\Models\ArpArquivoTipo;
use App\Models\ArpItemHistoricoAlteracaoFornecedor;
use App\Models\ArpPdf;
use App\Models\CompraItem;
use App\Models\Compras;
use App\Repositories\Compra\CompraItemFornecedorRepository;
use App\Repositories\Relatorio\ArpPdfRepository;
use App\Rules\ValidarItemAtaSolicitacao;
use App\Services\CompraService;
use App\Services\NovoDivulgacaoCompra\NovoDivulgacaoCompraService;
use App\Services\Pncp\Arp\ConsultarArquivoArpPncpService;
use App\Services\Pncp\PncpService;
use App\Services\RelatorioArpPdfService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;

use App\Http\Traits\ImportContent;
use App\Http\Traits\JobUserTrait;
use App\Http\Traits\LogTrait;
use App\Jobs\CarregarUnidadeParticipanteCompraGestaoAtaJOB;
use App\Models\Arp;
use App\Models\ArpAlteracao;
use App\Models\ArpAutoridadeSignataria;
use App\Models\ArpAutoridadeSignatariaHistorico;
use App\Models\ArpGestor;
use App\Models\ArpHistorico;
use App\Models\ArpItem;
use App\Models\ArpItemHistorico;
use App\Models\ArpUnidades;
use App\Models\Autoridadesignataria;
use App\Models\CompraItemFornecedor;
use App\Models\CompraItemUnidade;
use App\Models\FailedJob;
use App\Models\JobUser;
use App\Models\Unidade;
use App\Http\Traits\Pncp\ArpPncpTrait;
use App\Models\UnidadeConfiguracao;
use App\Models\User;
use App\Services\Pncp\Arp\ConsultarTodasArpPncpService;
use App\Services\Pncp\Arp\InserirArquivoArpPncpService;
use App\Services\Pncp\Arp\InsertArpPncpService;
use App\Services\Pncp\PncpManagerService;
use App\Services\Pncp\Responses\PncpGetArpUnicaResponseService;
use App\Services\Pncp\Responses\PncpPostResponseService;
use DataTables;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;
use App\Http\Traits\Unidade\ProcessoTrait as ProcessoControllerTrait;

/**
 * Class ArpCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArpCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use CommonColumns;
    use BuscaCodigoItens;
    use CommonFields;
    use CompraTrait;
    use Formatador;
    use ImportContent;
    use ArpTrait;
    use JobUserTrait;
    use ArpAlteracaoTrait;
    use LogTrait;
    use ArpPncpTrait;
    use ArpItemHistoricoTrait;
    use ArpItemTrait;
    use ProcessoControllerTrait;
    use UsuarioFornecedorTrait;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {

        // $this->crud->removeButton('update');
        //$this->crud->denyAccess(['update']);
       // $this->crud->denyAccess(['update_arp']);
       // $this->crud->denyAccess(['delete']);


        $this->permissionRule([
            'gestaodeatas_V2_acessar',
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);
        CRUD::setModel(\App\Models\Arp::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/arp');

        #CRUD::addClause('where', 'arp.unidade_origem_id', '=', session('user_ug_id'));

        # Busca todas atas que possuem vínculo com a unidade logada

        CRUD::addClause('select', [
            'arp.*',
            'compra_item_unidade.tipo_uasg',
        ]);

        CRUD::addClause('join', 'unidades', 'unidades.id', '=', 'arp.unidade_origem_id');
        CRUD::addClause('join', 'arp_item', 'arp_item.arp_id', '=', 'arp.id');
        CRUD::addClause(
            'join',
            'compra_item_fornecedor',
            'arp_item.compra_item_fornecedor_id',
            '=',
            'compra_item_fornecedor.id'
        );
        CRUD::addClause('leftjoin', 'compra_items', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id');
        CRUD::addClause('join', 'compras', 'compra_items.compra_id', '=', 'compras.id');
        CRUD::addClause('join', 'compra_item_unidade', 'compra_items.id', '=', 'compra_item_unidade.compra_item_id');
        CRUD::addClause('join', 'codigoitens', 'arp.tipo_id', '=', 'codigoitens.id');

        # Se a unidade logada for a gerenciadora ou
        # se a unidade logada tiver participação em algum item e a ata estiver ativa
        CRUD::addClause('where', function ($query) {
            $query->where(function ($subquery) {
                $subquery->where('arp.unidade_origem_id', session('user_ug_id'))
                    ->orWhere(function ($subquery) {
                        $subquery->where('compra_item_unidade.unidade_id', session('user_ug_id'))
                            ->where('codigoitens.descricao', 'Ata de Registro de Preços');
                    });
            });
        });

        CRUD::addClause('where', 'compra_item_unidade.unidade_id', session('user_ug_id'));
        CRUD::addClause('whereNull', 'compra_item_unidade.deleted_at');
        CRUD::addClause('whereNull', 'arp_item.deleted_at');
        CRUD::addClause('whereNull', 'compra_item_fornecedor.deleted_at');
        CRUD::addClause('whereNull', 'compra_items.deleted_at');
        CRUD::addClause('orderByDesc', 'arp.numero');
        CRUD::addClause('groupBy', ['arp.id', 'compra_item_unidade.tipo_uasg']);


        // dd(Str::replaceArray('?', $this->crud->query->getBindings(), $this->crud->query->toSql()));

        $this->exibirTituloPaginaMenu('Ata de Registro de Preços');
        $this->crud->addButtonFromModelFunction('line', 'getViewButtonAlterar', 'getViewButtonAlterar', 'end');

        $this->crud->addButtonFromModelFunction(
            'line',
            'getViewButtonRetificar',
            'getViewButtonRetificar',
            'end'
        );
        $this->crud->addButtonFromModelFunction(
            'line',
            'getViewButtonRelatarExecucaoDeAta',
            'getViewButtonRelatarExecucaoDeAta',
            'end'
        );
        $this->crud->addButtonFromModelFunction(
            'line',
            'getViewButtoRelatorioAta',
            'getViewButtoRelatorioAta',
            'end'
        );

        $this->crud->addButtonFromView(
            'line',
            'arp_link_pncp',
            'arp_link_pncp',
            'end'
        );
    }


    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->permissionRule(['gestaodeatas_V2_acessar']);

        $arquivoJS = [
            'assets/js/admin/forms/arp_list.js',
            'assets/js/blockui/jquery.blockUI.js',
            'packages/backpack/base/js/padrao_gov/jquery.mask.js'
        ];

        $this->filtros();
        $this->importarScriptJs($arquivoJS);

        CRUD::setSubheading('Listar', 'index');

        $this->addColumnModelFunction('status_arp', 'Situação', 'getSituacaoPersonalizada');

        $this->visualizarListShow();

        $this->crud->addButtonFromView(
            'line',
            'arp_arquivo_lista',
            'arp_arquivo_lista',
            'end'
        );

        $this->crud->enableExportButtons();
        $this->crud->prefix_text_button_redirect_create = 'Criar';
        $this->crud->text_button_redirect_create = 'Ata';
      //  $this->crud->addButtonFromModelFunction('line', 'getViewButtonArp', 'getViewButtonArp', 'end');
        $this->crud->addButton('line', 'update', 'view', 'crud::buttons.update_arp', 'beginning');
        $this->crud->addButton('line', 'delete', 'view', 'crud::buttons.delete_arp', 'end');
    }

    private function visualizarListShow()
    {
        $this->crud->setOperationSetting('persistentTable', false);

        $this->addColumnModelFunction('status_arp', 'Situação', 'getSituacaoPersonalizada');
        $this->addColumnModelFunction(
            'numero_ano',
            'Número/Ano da Ata',
            'getNumeroAno',
            ['table' => true, 'modal' => true, 'show' => true, 'export' => true],
            99999999999999999,
            "concat(arp.numero, '/' ,ano)"
        );
//        $this->addColumnModelFunction(
//            'unidade_origem',
//            'Unidade gerenciadora da ata',
//            'getUnidadeOrigem',
//        );

        $this->crud->addColumn([
            'name' => 'unidade_origem',
            'label' => 'Unidade gerenciadora da ata',
            'type' => 'model_function',
            'function_name' => 'getUnidadeOrigem',
        ]);
        $this->crud->addColumn([
            'name' => 'unidade_origem_compra',
            'label' => 'Tipo UASG',
            'type' => 'model_function',
            'function_name' => 'getUnidadeGerenciadora',
        ]);

//        $this->addColumnDate(
//            true,
//            true,
//            true,
//            true,
//            'vigencia_inicial',
//            'Vigência inicial'
//        );
//        $this->addColumnDate(
//            true,
//            true,
//            true,
//            true,
//            'vigencia_final',
//            'Vigência final'
//        );

        $this->crud->addColumn([
            'name' => 'vigencia_inicial',
            'label' => 'Vigência inicial',
            'type' => 'date'
        ]);
        $this->crud->addColumn([
            'name' => 'vigencia_final',
            'label' => 'Vigência final',
            'type' => 'date'
        ]);

        $this->addColumnModelFunction(
            'numero_compra',
            'Número da compra/Ano',
            'getNumeroCompra',
            ['table' => false, 'modal' => false, 'show' => true, 'export' => true],
            99999999999999999,
            "compras.numero_ano"
        );
        $this->crud->addColumn([
            'name' => 'unidade_origem_compra',
            'label' => 'Unidade origem da compra',
            'type' => 'model_function',
            'function_name' => 'getUnidadeOrigemCompra',
            'visibleInTable' => false
        ]);

    //    dd(Str::replaceArray('?', $this->crud->query->getBindings(), $this->crud->query->toSql()));
       // $this->addColumnModelFunction('fornecedor', 'Fornecedor', 'getFornecedorPorAta');
    }

    protected function setupShowOperation()
    {
        $this->permissionRule([
            'gestaodeatas_V2_acessar',
            'perfilfornecedor_consultar',
            'acessar_dashboard_usuario_fornecedor'
        ]);
        $this->crud->set('show.contentClass', 'col-md-12');

        $this->crud->set('show.setFromDb', false);

        $idAta = Route::current()->parameter("id");

        $this->addColumnModelFunction('numero_ano', 'Número', 'getNumeroAno');

        $this->visualizarListShow();

        CRUD::addColumn([
            'name' => 'numero_processo',
            'label' => 'Número do processo',
            'visibleInShow' => true,
            // 'visibleInExport' => $export
        ]);

        $this->addColumnDate(true, true, true, true, 'data_assinatura', 'Data da assinatura');
        $this->addColumnValorMonteraio('valor_total', 'Valor total');

        $autoridades = Arp::getAutoridadeSignataria($idAta);
        $this->addColumnTable('autoridade_signataria_ata', 'Autoridades', $autoridades);

        $this->addColumnModelFunction('modalidade_compra', 'Modalidade da compra', 'getModalidadeCompra');

        $this->addColumnModelFunction('compra_centralizada', 'Compra centralizada', 'getCompraCentralizada');

        $unidadeParticipante = Arp::getUnidadeParticipante($idAta);
        $this->addColumnTable('unidade_participante', 'Unidade participante', $unidadeParticipante);

        $item = Arp::getItemAta($idAta);
        $this->addColumnTable('item_ata', 'Item da ata', $item);

        $this->addColumnDateHour(
            true,
            true,
            true,
            true,
            'created_at',
            'Data da criação da ata'
        );

        $arp = $this->crud->getModel()->findOrFail($idAta);

        if (!$arp->rascunho) {
            $this->crud->denyAccess('update');
            $this->crud->denyAccess('delete');
        }
        
        $arquivoJS = [
            'assets/js/admin/forms/arp_list.js',
            'assets/js/blockui/jquery.blockUI.js'
        ];
        
        $this->importarScriptJs($arquivoJS);
    }

    private function campoCriarEditar(bool $exibir = true)
    {
        $this->crud->addField([
            'name' => 'numero',
            'label' => 'Número da ata',
            'type' => 'text',
            'attributes' => [
                'data-mask' => '00000',
                'required' => 'required'
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-4'
            ],
            'required' => true
        ]);

        $anoMaximo = Carbon::now()->format('Y');
        $anoMimino = config('arp.arp.ano_minimo_arp');

        $this->crud->addField([
            'name' => 'ano',
            'label' => 'Ano da ata',
            'type' => 'number',
            'attributes' => [
                'min' => $anoMimino,
                'max' => $anoMaximo,
                'required' => 'required'
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-4'
            ],
            'required' => true,
        ]);

        $classAreaDadosAta = '';
        if ($exibir) {
            $this->addAreaCustom('btn_validar_arp');
            $classAreaDadosAta = 'areaDadosAta';
        }


        $this->crud->addField(
            [
                'name' => 'objeto', // The db column name
                'label' => 'Objeto', // Table column heading
                'type' => 'textarea',
                'wrapperAttributes' => [
                    'class' => 'col-md-12 br-textarea ' . $classAreaDadosAta . ' pt-3'
                ],
                'attributes' => [
                    'required' => 'required'
                ],
                'escaped' => false,
                'required' => true,
            ]
        );

        $hoje = date('Y-m-d');

        $this->crud->addField([
            'name' => 'data_assinatura',
            'label' => 'Data de assinatura',
            'type' => 'date',
            'attributes' => [
                'required' => 'required',
                'max' => $hoje,
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-4 ' . $classAreaDadosAta . ' pt-3'
            ],
            'required' => true,
        ]);

        $this->crud->addField([
            'name' => 'vigencia_inicial',
            'label' => 'Data inicial de vigência',
            'type' => 'date',
            'attributes' => [
                'required' => 'required'
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-4 ' . $classAreaDadosAta . ' pt-3'
            ],
            'required' => true,
        ]);

        $this->crud->addField([
            'name' => 'vigencia_final',
            'label' => 'Data final de vigência',
            'type' => 'date',
            'attributes' => [
                'required' => 'required'
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-4 ' . $classAreaDadosAta . ' pt-3'
            ],
            'required' => true,
        ]);

        $attributesProcesso['required'] = 'required';
        $maskAttributes = $this->getProcessoMask();
        if (!empty($maskAttributes)) {
            $attributesProcesso['data-mask'] = $maskAttributes;
        }
        $this->crud->addField([
            'name' => 'numero_processo',
            'label' => 'Número do Processo',
            'type' => 'text',
            'attributes' => array_merge($attributesProcesso, ['maxlength' => 255]),
            'wrapperAttributes' => ['class' => 'col-md-4 ' . $classAreaDadosAta . ' pt-3'],
            'required' => true
        ]);
        // $this->montarSelectGestorAta('nome_arp', $exibir);
        $this->montarSelectAutoridadeSignataria('nome', $exibir);

        $this->urlValidacoesArp();
    }

    /**
     * Método responsável montar a URL para validar as ações via ajax
     */
    private function urlValidacoesArp()
    {
        # Monta a url para recuperar a compra
        $this->crud->addField([
            'name' => 'url_buscar_compra',
            'type' => 'hidden',
            'default' => url("arp/validarcompra"),
            'attributes' => ['id' => 'url_buscar_compra'],
            'fake' => true
        ]);

        # Monta a url para validar a ata digitada pelo usuário
        $this->crud->addField([
            'name' => 'url_buscar_arp',
            'type' => 'hidden',
            'default' => url("arp/validarnumeroata"),
            'attributes' => ['id' => 'url_buscar_arp'],
            'fake' => true
        ]);

        # Monta a url para validar a compra digitada pelo usuário
        $this->crud->addField([
            'name' => 'url_validar_compra_cadastro',
            'type' => 'hidden',
            'default' => url("arp/validarcompracadastro"),
            'attributes' => ['id' => 'url_validar_compra_cadastro'],
            'fake' => true
        ]);
    }

    private function adicionarArquivo(bool $exibir = true)
    {
        $arquivo_field = view('vendor.backpack.crud.fields.arquivo_field');
        $this->crud->addField([   // CustomHTML
            'name' => 'arquivo',
            'type' => 'custom_html',
            'value' => $arquivo_field,
            'upload' => true,
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3 arquivo-hidden'
            ],
        ]);
    }

    /**
     * Método responsável em atualizar o arquivo
     */
    private function updateArquivo(bool $exibir = true)
    {
        $idArp = \Route::current()->parameter('id');
        $arp = Arp::find($idArp);
        $tipoArquivo = ArpArquivoTipo::where('nome', 'Ata de Registro de Preços')->first();

        $arquivo = ArpArquivo::where('arp_id', $arp->id)
            ->where('tipo_id', $tipoArquivo->id)
            ->first();

        $arquivo_field = view('vendor.backpack.crud.fields.arquivo_field', compact('arquivo'));
        $this->crud->addField([   // CustomHTML
            'name' => 'arquivo',
            'type' => 'custom_html',
            'value' => $arquivo_field,
            'upload' => true,
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3'
            ],
        ]);
    }

    /**
     * Método responsável em montar o select para selecionar o gestor
     */
    private function montarSelectGestorAta($attribute, bool $exibir = true)
    {
        $classAreaDadosAta = '';
        if ($exibir) {
            $classAreaDadosAta = 'areaDadosAta';
        }
        $this->addFieldSelectMultiple(
            'Gestores da ata',
            'gestor_ata',
            'gestor',
            $attribute,
            "arp/gestores",
            ['class' => 'col-md-4 ' . $classAreaDadosAta . ' pt-3'],
            null,
            'user_id'
        );
    }

    /**
     * Método responsável em montar o select para selecionar a autoridade signatária
     */
    private function montarSelectAutoridadeSignataria($attribute, bool $exibir = true)
    {
        $classAreaDadosAta = '';
        if ($exibir) {
            $classAreaDadosAta = 'areaDadosAta';
        }
        $this->addFieldSelectMultiple(
            'Autoridade signatária',
            'autoridadesignataria_ata',
            'autoridade_signataria',
            $attribute,
            'arp/autoridadesignataria',
            ['class' => 'col-md-4 ' . $classAreaDadosAta . ' pt-3'],
            null,
            'autoridade_signataria_id'
        );
    }

    /**
     * Método responsável em exibir os campos da área dos dados básicos da ata
     * Tanto para a tela de criar como de editar
     */
    private function areaDadosAta()
    {
        $this->campoCriarEditar();
    }

    /**
     * Método responsável em exibir o campo da compra centralizada
     */
    private function campoCompraCentralizada(bool $exibir = true)
    {
        # Se for na tela de editar, remove a class para exibir ao carregar a página
        $classArea = '';
        if ($exibir) {
            $classArea = 'areaDadosCompra';
        }
        $this->crud->addField([
            'name' => 'compra_centralizada',
            'label' => 'Compra centralizada?',
            'type' => 'radio',
            'textTooltip' => 'Conforme Art. 2º, item VII do Decreto 11.462 de 2023:
            Compra centralizada se refere à compra ou contratação de bens, serviços ou obras,
            em que o órgão ou a entidade gerenciadora conduz os procedimentos para registro de preços destinado à
            execução descentralizada, mediante prévia indicação da demanda pelos órgãos ou pelas
            entidades participantes.',
            'default' => 0,
            'options' => [
                false => 'Não',
                true => 'Sim'
            ],
            'attributes' => [
                'required' => 'required'
            ],
            'inline' => 'd-inline-block',
            'wrapperAttributes' => [
                'class' => 'col-md-4 ' . $classArea . ' pt-3'
            ]
        ]);
    }

    /**
     * Método responsável em exibir os campos dos dados da compra na tela de edição
     */
    private function areaDadosCompraUpdate(Arp $arp)
    {
        $this->crud->addField([
            'name' => 'compra_id',
            'type' => 'hidden',
            'value' => $arp->compras->id,
        ]);

        $this->crud->addField([
            'name' => 'unidade_origem_id_fake',
            'label' => 'Unidade gerenciadora',
            'type' => 'label',
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->unidades->codigo . "-" . $arp->unidades->nomeresumido,
        ]);

        $this->crud->addField([
            'name' => 'unidade_origem_id',
            'type' => 'hidden',
            'default' => $arp->unidade_origem_id,
            'attributes' => ['id' => 'unidade_origem_id']
        ]);

        $this->crud->addField([
            'name' => 'unidade_origem_compra_fake',
            'label' => 'Unidade origem da compra',
            'type' => 'label',
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->getUnidadeOrigemCompra(),
        ]);

        $this->crud->addField([
            'name' => 'unidade_origem_compra_id',
            'type' => 'hidden',
            'attributes' => ['id' => 'unidade_origem_compra_id'],
            'default' => $arp->compras->unidade_origem_id,
        ]);

        $this->crud->addField([
            'name' => 'texto_numero_ano',
            'label' => 'Número da compra/Ano',
            'type' => 'label',
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->compras->numero_ano,
        ]);

        $this->crud->addField([
            'name' => 'numero_ano',
            'type' => 'hidden',
            'default' => $arp->compras->numero_ano,
            'attributes' => ['id' => 'numero_ano']
        ]);

        $this->crud->addField([
            'name' => 'modalidade_id',
            'type' => 'hidden',
            'default' => $arp->compras->modalidade_id,
            'attributes' => ['id' => 'modalidade_id']
        ]);

        $this->crud->addField([
            'name' => 'text_modalidade',
            'label' => 'Modalidade da compra',
            'type' => 'label',
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->compras->getModalidade(),
        ]);

        # Exibir o botão de consultar a compra
        $this->addAreaCustom(
            'btn_recuperar_compra_arp',
            null,
            null,
            null,
            null,
            'btn_recuperar_compra_arp',
            'Buscar compra:<br>Somente serão exibidos fornecedores que têm item(ns) pendente(s) de registro em ata'
        );
    }

    /**
     * Método responsável em exibir os dados da compra para a busca
     */
    private function areaDadosCompra()
    {
        $this->campoCompraCentralizada();

        $this->crud->addField([
            'name' => 'unidade_origem_id_fake',
            'label' => 'Unidade gerenciadora',
            'type' => 'text',
            'attributes' => ['readonly' => 'readonly'],
            'wrapperAttributes' => ['class' => 'col-md-4 areaDadosCompra pt-3'],
            'fake' => true,
            'default' => session('user_ug') . "-" . session('user_ug_nome_resumido')
        ]);

        $this->crud->addField([
            'name' => 'unidade_origem_id',
            'type' => 'hidden',
            'default' => session('user_ug_id'),
            'attributes' => ['id' => 'unidade_origem_id']
        ]);

        $this->crud->addField([
            'name' => 'unidade_origem_compra_id',
            'type' => 'hidden',
            'attributes' => ['id' => 'unidade_origem_compra_id'],
            'default' => session('user_ug_id')
        ]);

        $this->addFieldSelect(
            ' Unidade origem da compra',
            'unidade_origem_compra_fake',
            'unidades',
            'unidade_codigo_nomeresumido',
            "unidadeuasg",
            ['id' => 'unidade_origem_compra_fake', 'class' => 'col-md-4 areaDadosCompra pt-3'],
            null,
            true,
            true,
            session('user_ug_id')
        );

        $campos =
            [
                'name' => 'numero_ano',
                'label' => 'Número da compra/Ano',
                'type' => 'text',
                'attributes' => [
                    'data-mask' => '00000/0000',
                    'placeholder' => '12345/2022',
                    'required' => 'required'
                ],
                'wrapperAttributes' => [
                    'class' => 'col-md-4 areaDadosCompra pt-3 br-input'
                ],
                'required' => true,
            ];
        $this->crud->addField($campos);

        # Exibir somente algumas modalidades
        $modalidades = $this->retornaArrayCodigosItens('Modalidade Licitação', true);
        $optionsModalidades = ['Selecione a modalidade'];
        foreach ($modalidades as $key => $modalidade) {
            $modalidadesValida = ['03', '04', '05', '06', '07', '33', '44'];
            foreach ($modalidadesValida as $modalidadeValida) {
                if (strpos($modalidade, $modalidadeValida) !== false) {
                    $optionsModalidades[$key] = $modalidade;
                }
            }
        }

        $this->crud->addField([
            'name' => 'modalidade_id',
            'label' => "Modalidade da compra",
            'type' => 'select2_from_array',
            'options' => $optionsModalidades,
            'wrapperAttributes' => [
                'class' => 'col-md-4 areaDadosCompra pt-3',
                'id' => 'modalidade_id'
            ],
            'required' => true,
        ]);

        // $this->addAreaCustom('btn_recuperar_compra_arp','areaDadosCompra');
        $this->addAreaCustom(
            'btn_recuperar_compra_arp',
            'areaDadosCompra col-md-4 pt-6-form-br',
            null,
            null,
            null,
            'btn_recuperar_compra_arp',
            'Buscar compra:<br>Somente serão exibidos fornecedores que têm item(ns) pendente(s) de registro em ata'
        );
    }

    /**
     * Método responsável em exibir a modal no padrão GOVBR
     */
    private function modalUnidades()
    {
        $this->addModalCustom('modalArp', 'modalArp', 'Unidades');
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->permissionRule(['gestaodeatas_V2_acessar']);

        $this->crud->setCreateContentClass('col-md-12');

        $this->crud->setTitle('Criar Ata de Registro de Preços', 'create');

        $this->importarDatatableForm();

        $arquivoJS = [
            'assets/js/admin/forms/arp.js',
            'assets/js/admin/forms/arp_common.js',
            'assets/js/blockui/jquery.blockUI.js',
            'packages/backpack/base/js/padrao_gov/jquery.mask.js'
        ];

        $this->importarScriptJs($arquivoJS);

        $this->areaDadosAta();

        $this->areaDadosCompra();

        $this->addAreaCustom('table_fornecedor_compra_arp', null, 'areaFornecedor');
        $this->addAreaCustom('table_selecionar_item_compra_arp');
        $this->addAreaCustom('table_percentual_maior_desconto');
        // $this->addAreaCustom('table_detalhe_item');


//        $this->crud->addField([
//            'name' => 'valor_total',
//            'type' => 'hidden',
//        ]);

        $this->crud->addField([
            'name' => 'valor_total_fake',
            'label' => 'Valor total dos itens selecionados',
            'type' => 'text',
            'attributes' => ['readonly' => 'readonly'],
            'wrapperAttributes' => [
                'class' => 'col-md-4 areaItemSelecionado pt-3 br-input'
            ],
            'fake' => true
        ]);

        $this->modalUnidades();
        // $this->addAreaCustom('table_item_compra_selecionado_arp');

        $this->addAreaCustom('table_item_compra_selecionado_arp', 'areaItemSelecionado', 'areaItemSelecionado');

        $this->adicionarArquivo();

        $this->crud->button_custom = [
            [
                'button_text' => 'Salvar Rascunho',
                'button_id' => 'rascunho',
                'button_name_action' => 'rascunho',
                'button_value_action' => '1',
                'button_icon' => 'fas fa-edit',
                'button_tipo' => 'secondary'
            ],
            [
                'button_text' => 'Criar Ata',
                'button_id' => 'adicionar',
                'button_name_action' => 'rascunho',
                'button_value_action' => '0',
                'button_icon' => 'fas fa-save',
                'button_tipo' => 'primary'
            ],
        ];
    }

    /**
     * Método responsável em percorrer os gestores da ata selecionados
     */
    private function validarInsertGestorAta(array $dados, int $idArp)
    {
        if (!empty($dados['gestor_ata']) && count($dados['gestor_ata']) > 0) {
            foreach ($dados['gestor_ata'] as $gestor) {
                $this->inserirGestorAta($gestor, $idArp);
            }
        }
    }

    /**
     * Método responsável em inserir os gestores da ata
     */
    private function inserirGestorAta(int $gestor, int $idArp)
    {
        ArpGestor::create(['user_id' => $gestor, 'arp_id' => $idArp]);
    }

    /**
     * Método responsável em percorrer as autoridades signatárias para ata selecionada
     */
    private function validarInsertAutoridadeSignataria(array $dados, int $idArp)
    {
        if (!empty($dados['autoridadesignataria_ata']) && count($dados['autoridadesignataria_ata']) > 0) {
            foreach ($dados['autoridadesignataria_ata'] as $autoridadeSignataria) {
                $this->inserirAutoridadeSignatariaAta($autoridadeSignataria, $idArp, $dados);
            }
        }
    }

    /**
     * Método responsável em inserir a autoridade signatária da ata
     */
    private function inserirAutoridadeSignatariaAta(int $autoridadeSignataria, int $idArp, array $dados)
    {
        $autoridadeSignataria = ['autoridade_signataria_id' => $autoridadeSignataria, 'arp_id' => $idArp];
        ArpAutoridadeSignataria::create($autoridadeSignataria);

        # Se não for rascunho, salva no histórico
        if (!$dados['rascunho']) {
            $autoridadeSignataria['vigencia_inicial'] = $dados['vigencia_inicial'];
            $autoridadeSignataria['vigencia_final'] = $dados['vigencia_final'];
            ArpAutoridadeSignatariaHistorico::create($autoridadeSignataria);
        }
    }

    /**
     * Método responsável em recuperar o item na tabela compra_item_fornecedor
     */
    private function recuperarDadosItemSelecionado(string $item)
    {
        $item = str_replace("linha_", "", $item);
        return CompraItemFornecedor::find($item);
    }

    /**
     * Método responsável em inserir o item na ata
     */
    private function inserirItem(array $dados)
    {
        return ArpItem::create($dados)->id;
    }

    /**
     * Método responsável em inserir a unidade
     */
    private function inserirUnidades(array $dados)
    {
        return ArpUnidades::create($dados);
    }

    public function store(StoreRequest $request)
    {
        DB::beginTransaction();
        $dados = $this->formatarDataBanco(Arp::$datesGovBr, $request->all());

        # Se não tiver nenhum item selecionado, retorna para a tela de cadastro
        if (!isset($dados['id_item_fornecedor'])) {
            \Alert::add('error', 'Selecione ao menos um item para a ata.')->flash();
            return redirect()->back();
        }

        # Se não tiver nenhum arquivo selecionado, retorna para a tela de cadastro
        if (!$dados['rascunho'] && !$request->file('arquivo')) {
            \Alert::add('error', 'O campo Arquivo é obrigatório.')->flash();
            return redirect()->back()->withInput();
        }

        # Retorna os dados da compra
        $dadosCompra = $this->retornaDadosCompra($dados);

        # Retorna o tipo da ata, como Ata de Registro de Preços, entendendo que é a inicial
        $dados['compra_id'] = $dadosCompra->id;
        $descricaoCodigo = 'Tipo de Ata de Registro de Preços';

        # Inclusão do status Carregando Compra para caso aconteça erro,
        # possa enviar para o PNCP
        $dados['tipo_id'] = $this->retornaIdCodigoItem($descricaoCodigo, 'Em elaboração');

        # Calcular o valor total da ata com base nos itens selecionados
        $dados['valor_total'] =
            (new CompraItemFornecedorRepository())->getValorTotalItemFornecedor($dados['id_item_fornecedor']);

        try {
            $arp = Arp::create($dados);
            $idArp = $arp->id;
            #Inserir os Gestores da Ata
            $this->validarInsertGestorAta($dados, $idArp);

            #Inserir as Autoridades Signatárias
            $this->validarInsertAutoridadeSignataria($dados, $idArp);

            # Se existir o arquivo, insere na tabela
            if ($request->file('arquivo')) {
                $disk = "public";
                $destination_path = "arp/" . Carbon::now()->format('Y_m') . "/" . $idArp;
                $new_file_name = $request->file('arquivo')->hashName();
                $file_path = $request->file('arquivo')->storeAs($destination_path, $new_file_name, $disk);

                $anexos = new ArpArquivo();
                $anexos->arp_id = $idArp;
                $anexos->tipo_id = ArpArquivoTipo::where('nome', 'Ata de Registro de Preços')->first()->id;
                $anexos->descricao = 'Ata de Registro de Preços nº ' . $arp->numero;
                $anexos->nome = '-';
                $anexos->restrito = false;
                $anexos->url = $file_path;
                $anexos->user_id = Auth::id();
                # Método responsável em bloquear a Observer da Arp Arquivo
                # Para que não enviei as informações para o PNCP antes
                # do processamento da compra
                $anexos->unsetEventDispatcher();
                $anexos->save();
            }

            #Inserir os itens da Ata
            $itensCadastrados = [];
            if (isset($dados['id_item_fornecedor']) && count($dados['id_item_fornecedor']) > 0) {
                $itensCadastrados = array();
                # Percorre os itens selecionados
                foreach ($dados['id_item_fornecedor'] as $item) {
                    #Recupera as informações do item para inserção da tabela de arp item e de unidades
                    $compraItemFornecedor = $this->recuperarDadosItemSelecionado($item);

                    # Monta o array de dados para o insert do item
                    $arpItem = ['arp_id' => $idArp, 'compra_item_fornecedor_id' => $compraItemFornecedor->id];
                    $idItem = $this->inserirItem($arpItem);

                    # Recupera a informação na tabela compra_item_unidade para preenchimento
                    #das informações para a tabela unidades
                    $compraItemUnidade = $compraItemFornecedor->compraItens->compraItemUnidade[0];

                    $arpUnidade = [
                        'arp_item_id' => $idItem,
                        'unidade_id' => $compraItemUnidade->unidade_id,
                        'tipo_unidade' => $compraItemUnidade->tipo_uasg,
                        'quantidade_autorizada' => $compraItemUnidade->quantidade_autorizada,
                        'quantidade_saldo' => $compraItemUnidade->quantidade_saldo,
                        'valor_autorizado' => $compraItemFornecedor->valor_negociado
                    ];
                    $this->inserirUnidades($arpUnidade);

                    # Insere os itens cadastrados para incluir na arp_item
                    $itensCadastrados[] = $idItem;
                }
            }

            $dados['arp_id'] = $idArp;

            # Salvar as informações do histórico da Ata
            $this->salvarHistoricoArp($dados, $descricaoCodigo, $itensCadastrados);

            Validator::make($request->all(), [
                'id_item_fornecedor.*' => [new ValidarItemAtaSolicitacao($itensCadastrados)],
            ])->validated();

            DB::commit();

            $mensagem = 'Cadastro Realizado com sucesso!';

            if (!$dados['rascunho']) {
                $mensagem = 'Cadastro Realizado com sucesso!
                A ata será enviada automaticamente para o PNCP após a conclusão
                do carregamento dos dados da compra pelo sistema.';
            }

            \Alert::add('success', $mensagem)->flash();
            return redirect('arp');
        } catch (Exception $ex) {
            DB::rollBack();
            
            if (isset($ex->validator) && !empty($ex->validator->errors()->all())) {
                $allErrorMessages = $ex->validator->errors()->all();
                $mensagem = implode("<br>", $allErrorMessages);

                \Alert::add('error', $mensagem)->flash();
                return redirect()->back();
            }

            $firstExp = new MessageBag(['arparquivo_exception' => 'Falha ao enviar o arquivo']);

            $secondExp = new MessageBag(
                [
                    'other_exception' => $ex->getMessage()
                ]
            );

            $firstExp->merge($secondExp);

            \Alert::add('warning', $firstExp->all())->flash();

            $this->inserirLogCustomizado('gestao_ata', 'error', $ex);
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        # Inclusão do método forceDelete para que seja removido fisicamente
        # no banco de dados
        return Arp::find($id)->forceDelete();
    }

    # Query responsável por recuperar os itens que foram cadastrados na Ata
    private function recuperarItensCadastrados(int $idArp)
    {
        return ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("compra_items", "compra_item_fornecedor.compra_item_id", "=", "compra_items.id")
            ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'arp_item.id')
            ->join('unidades', 'unidades.id', '=', 'compra_item_unidade.unidade_id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id')
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->join(
                'catmatseritens',
                'catmatseritens.id',
                '=',
                'compra_items.catmatseritem_id'
            )
            ->where("arp_id", $idArp)
            ->select([
                "compra_items.numero",
                "codigoitens.descricao",
                "catmatseritens.codigo_siasg",
                "compra_item_fornecedor.valor_unitario",
                "compra_item_fornecedor.valor_negociado",
                "compra_items.criterio_julgamento",
                "compra_item_fornecedor.percentual_maior_desconto",
                "compra_items.maximo_adesao",
                "compra_items.permite_carona",
                DB::raw("compra_items.descricaodetalhada AS descricaosimplificada"),

                DB::raw("string_agg(unidades.codigo, ', ')  as codigouasg"),
                DB::raw("string_agg(compra_item_fornecedor.id::varchar, ', ')  as compraItemFornecedorId"),
                DB::raw("string_agg(quantidade_autorizada::varchar, ', ')  as quantidadeautorizadadetalhe"),
                DB::raw("string_agg(
                CASE
                    WHEN tipo_uasg = 'G' THEN 'Gerenciadora'
                    WHEN tipo_uasg = 'P' THEN 'Participante'
                    WHEN tipo_uasg = 'C' THEN 'Não participante' -- Carona
                    END , ',')  as tipouasgdetalhe"),
                DB::raw("TO_CHAR(compra_item_fornecedor.valor_unitario *
    ((100-compra_item_fornecedor.percentual_maior_desconto)/100), 'FM999999999.0000') as valor_unitario_desconto"),

                DB::raw("TO_CHAR(
        compra_item_fornecedor.valor_unitario * ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100),
        'FM999999999.0000'
    )::float * compra_item_fornecedor.quantidade_homologada_vencedor as valor_negociado_desconto"),
                DB::raw("sum(compra_item_unidade.quantidade_saldo) AS quantidade_saldo"),
                DB::raw("sum(compra_item_unidade.quantidade_adquirir) AS quantidade_adquirir"),
                DB::raw("sum(quantidade_autorizada) AS quantidadeautorizada"),
                DB::raw("sum(quantidade_homologada_vencedor) AS quantidadehomologadavencedor"),
                "fornecedores.cpf_cnpj_idgener",
                "fornecedores.nome AS nomefornecedor"
            ])
            ->groupBy(
                "compra_items.numero",
                "codigoitens.descricao",
                "catmatseritens.codigo_siasg",
                "compra_items.descricaodetalhada",
                "compra_item_fornecedor.valor_unitario",
                "compra_item_fornecedor.valor_negociado",
                "compra_items.criterio_julgamento",
                "compra_item_fornecedor.percentual_maior_desconto",
                "compra_item_fornecedor.valor_unitario",
                "compra_item_fornecedor.valor_negociado",
                "compra_items.maximo_adesao",
                "compra_items.permite_carona",
                "fornecedores.cpf_cnpj_idgener",
                "fornecedores.nome"
            )
            ->orderByRaw("codigoitens.descricao ASC, compra_items.numero ASC")
            ->get();
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->permissionRule(['gestaodeatas_V2_acessar']);

        $this->crud->setUpdateContentClass('col-md-12');

        $this->importarDatatableForm();


        $this->importarDatatableForm();

        $arquivoJS = [
            'assets/js/admin/forms/arp.js',
            'assets/js/admin/forms/arp_common.js',
            'assets/js/blockui/jquery.blockUI.js',
            'packages/backpack/base/js/padrao_gov/jquery.mask.js'
        ];

        $this->importarScriptJs($arquivoJS);

        $this->campoCriarEditar(false);
        $this->campoCompraCentralizada(false);

        $idArp = \Route::current()->parameter('id');
        $arp = Arp::find($idArp);
        $this->areaDadosCompraUpdate($arp);


        $this->addAreaCustom('table_fornecedor_compra_arp', null, 'areaFornecedor');
        $this->addAreaCustom('table_selecionar_item_compra_arp');
        $this->addAreaCustom('table_percentual_maior_desconto');
        // $this->addAreaCustom('table_detalhe_item');

        $this->modalUnidades();

        # Recupera os itens para exibir na tela de edição
        $itensCadastrados = ArpItem::itensAtaRascunho($idArp);

        $itensFormatado = [];
        $valorTotal = 0;
        $ndcService = new NovoDivulgacaoCompraService();
        
        foreach ($itensCadastrados as $itens) {
            $codigoSiasg = $itens->codigo_siasg;
            $idLinha = "linha_{$codigoSiasg}";

            $textoPermiteCarona = 'Não';

            if ($itens->permite_carona == 1) {
                $textoPermiteCarona = 'Sim';
            }

            # Campo Valor Total
            $valorNegociado = number_format($itens->valor_negociado, 2, ',', '.');
            # Campo Valor Unitário
            $valorUnitario = number_format($itens->valor_unitario, 4, ',', '.');

            # Variável responsável em receber o valor para calcular o total da ata
            $valorNegociadoTotal = $itens->valor_negociado;

            # Se existir desconto no item, então exibe o valor com o maior desconto
            if ($itens->percentual_maior_desconto > 0) {
                $valorNegociado = number_format($itens->valor_negociado_desconto, 2, ',', '.') . '
                <i class="fas fa-info-circle"
                title="Clique para maiores detalhes sobre o desconto"
                onclick="exibirDetalheMaiorDesconto(`' . number_format(
                    $itens->percentual_maior_desconto,
                    2,
                    ',',
                    '.'
                ) . '`,
                `' . $itens->numero . '`,
                `' . $itens->descricaosimplificada . '`,
                `' . number_format($itens->valor_unitario, 4, ',', '.') . '`,
                `' . number_format($itens->valor_unitario_desconto, 4, ',', '.') . '`,
                `' . number_format($itens->valor_negociado, 2, ',', '.') . '`,
                `' . number_format($itens->valor_negociado_desconto, 2, ',', '.') . '`)"></i>';

                $valorUnitario = number_format($itens->valor_unitario_desconto, 4, ',', '.');

                $valorNegociadoTotal = $itens->valor_negociado_desconto;
            }

            $itens->descricaosimplificada = Str::limit(
                trim($itens->descricaosimplificada),
                50,
                '<i class="fas fa-info-circle" title="' . $itens->descricaosimplificada . '"></i>'
            );
            
            $itens->maximo_adesao =
                number_format(
                    $ndcService->formataMaximoAdesao($itens->quantidade_homologada_vencedor, $itens->lei),
                    0,
                    ',',
                    '.'
                );
            
            $itens->maximo_adesao_informado_compra = number_format(
                $itens->maximo_adesao_informado_compra,
                0,
                ',',
                '.'
            );
            
            # Coluna Quantidade Registrada
            $itens->quantidadehomologadavencedor =
                number_format(
                    $itens->quantidade_homologada_vencedor,
                    5,
                    ',',
                    '.'
                ) .
                ' <i class="fas fa-info-circle"
                    title="Clique para maiores detalhes sobre a quantidade registrada"
                    onclick="exibirDetalheItem(' . $itens->compraId . ',' . backpack_user()->id . ',
                    ' . $itens->compraItemId . ',`' . url('arp/exibirdetalheitem') . '`)"></i>';

            # Coluna classificação do fornecedor
            $itens->classificacao = $itens->classificacao .
                '<i class="fas fa-info-circle"
                title="Clique para maiores detalhes sobre a classificação"
                onclick="exibirClassificacaoFornecedor(' . $itens->idCompraItemFornecedor . ',
                            `' . url('arp/exibirclassificacaofornecedor') . '`, `' . $itens->numero . '`)">
                </i>';
            
            $itensFormatado [] = "<tr id='{$idLinha}'>
                        <td>
                            <a class='br-button circle btn-danger large mr-3 br-button-row-table'
                                href='javascript:void(0)'
                                onclick='removerLinhaItemSelecionado(`{$idLinha}`, `{$itens->numero}`)'  >
                                    <i class='nav-icon la la-trash'></i>
                            </a>
                        </td>
                        <td>{$itens->cpf_cnpj_idgener}</td>
                        <td>{$itens->nomefornecedor}<br>({$itens->classificacao})</td>
                        <td>{$itens->numero}</td>
                        <td>{$itens->descricao}</td>
                        <td>{$codigoSiasg}</td>
                        <td>{$itens->descricaosimplificada}</td>
                        <td>{$itens->quantidadehomologadavencedor}</td>
                        <td>{$valorUnitario}</td>
                        <td>" . $valorNegociado . "</td>
                        <td style='text-align: right;'>{$itens->maximo_adesao}</td>
                        <td style='text-align: right;'>{$itens->maximo_adesao_informado_compra}</td>
                        <td style='text-align: right;'>
                            <input type='hidden' name='id_item_fornecedor[]'
                            value='{$itens->compraitemfornecedorid}'/>{$textoPermiteCarona}
                            </td>
                    </tr>";
            $valorTotal += $valorNegociadoTotal;
        }

        $this->crud->addField([
            'name' => 'valor_total',
            'type' => 'hidden',
            'value' => $valorTotal
        ]);

        $this->crud->addField([
            'name' => 'valor_total_fake',
            'label' => 'Valor total dos itens selecionados',
            'type' => 'text',
            'attributes' => ['readonly' => 'readonly'],
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3 br-input'
            ],
            'fake' => true,
            'value' => 'R$ ' . number_format($valorTotal, 4, ',', '.')
        ]);
        // dd($itensFormatado);
        $this->addAreaCustom('table_item_compra_selecionado_arp', null, null, $itensFormatado);

        $this->crud->addField([
            'name' => 'rascunho',
            'type' => 'hidden',
        ]);

        # Inclusão para buscar a compra no rascunho
        $this->crud->addField([
            'name' => 'modalidade_id',
            'type' => 'hidden',
            'attributes' => ['id' => 'modalidade_id'],
            'value' => $arp->compras->modalidade_id
        ]);

        # Inclusão para buscar a compra no rascunho
        $this->crud->addField([
            'name' => 'numero_ano',
            'type' => 'hidden',
            'value' => $arp->compras->numero_ano
        ]);

        $this->updateArquivo();

        $tipoArquivo = ArpArquivoTipo::where('nome', 'Ata de Registro de Preços')->first();

        $arquivo = ArpArquivo::where('arp_id', $arp->id)
            ->where('tipo_id', $tipoArquivo->id)
            ->first();

        $this->crud->addField([
            'name' => 'temArquivo',
            'type' => 'hidden',
            'value' => isset($arquivo) ? 1 : 0,
        ]);

        // $this->crud->replaceSaveActions([
        //     'name' => 'save_action_one',
        //     'button_text' => 'Atualizar Ata',
        // ]);

        $this->crud->button_custom = [
            [
                'button_text' => 'Salvar Rascunho',
                'button_id' => 'rascunho',
                'button_name_action' => 'rascunho',
                'button_value_action' => 1,
                'button_icon' => 'fas fa-edit',
                'button_tipo' => 'secondary'
            ],
            [
                'button_text' => 'Criar Ata',
                'button_id' => 'adicionar',
                'button_name_action' => 'rascunho',
                'button_value_action' => 0,
                'button_icon' => 'fas fa-save',
                'button_tipo' => 'primary'
            ],
        ];
    }

    private function deletarGestorAta(int $idAta)
    {
        ArpGestor::where("arp_id", $idAta)->delete();
    }

    private function deletarAutoridadeSignataria(int $idAta)
    {
        ArpAutoridadeSignataria::where("arp_id", $idAta)->delete();
    }

    public function update(ArpRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $this->formatarDataBanco(Arp::$datesGovBr, $request->all());
            $data['valor_total'] =
                (new CompraItemFornecedorRepository())->getValorTotalItemFornecedor($data['id_item_fornecedor']);

            $request->merge($data);

            $this->deletarGestorAta($data['id']);
            $this->validarInsertGestorAta($data, $data['id']);

            $this->deletarAutoridadeSignataria($data['id']);
            $this->validarInsertAutoridadeSignataria($data, $data['id']);
            ArpItem::where("arp_id", $data['id'])->forceDelete();


            if (!isset($data['id_item_fornecedor'])) {
                \Alert::add('error', 'Selecione ao menos um item para a ata.')->flash();
                return redirect()->back();
            }

            $itensCadastrados = array();
            if (isset($data['id_item_fornecedor']) && count($data['id_item_fornecedor']) > 0) {
                foreach ($data['id_item_fornecedor'] as $item) {
                    $compraItemFornecedor = $this->recuperarDadosItemSelecionado($item);
                    $arpItem = ['arp_id' => $data['id'], 'compra_item_fornecedor_id' => $compraItemFornecedor->id];
                    $idItem = $this->inserirItem($arpItem);
                    $compraItemUnidade = $compraItemFornecedor->compraItens->compraItemUnidade[0];

                    $arpUnidade = [
                        'arp_item_id' => $idItem,
                        'unidade_id' => $compraItemUnidade->unidade_id,
                        'tipo_unidade' => $compraItemUnidade->tipo_uasg,
                        'quantidade_autorizada' => $compraItemUnidade->quantidade_autorizada,
                        'quantidade_saldo' => $compraItemUnidade->quantidade_saldo,
                        'valor_autorizado' => $compraItemFornecedor->valor_negociado
                    ];
                    $this->inserirUnidades($arpUnidade);
                    $itensCadastrados[] = $idItem;
                }
            }

            if ($request->file('arquivo')) {
                # Se alterar o arquivo dentro do rascunho
                # guarda o evento do dispatcher para poder executar
                # logo em seguida da criação do registro na tabela
                # arp_arquivos
                $arpEventDispatcher = Arp::getEventDispatcher();

                $arquivoTipo = ArpArquivoTipo::where('nome', 'Ata de Registro de Preços')->first()->id;
                $anexos = ArpArquivo::where('arp_id', $data['id'])
                    ->where('tipo_id', $arquivoTipo)
                    ->first();
                //remove old file
                if ($data['rascunho'] && isset($anexos)) {
                    Storage::disk('public')->delete($anexos->url);
                }

                $disk = "public";
                $destination_path = "arp/" . Carbon::now()->format('Y_m') . "/" . $data['id'];
                $new_file_name = $request->file('arquivo')->hashName();
                $file_path = $request->file('arquivo')->storeAs($destination_path, $new_file_name, $disk);

                if (!isset($anexos)) {
                    $anexos = new ArpArquivo();
                    $anexos->arp_id = $data['id'];
                    $anexos->tipo_id = ArpArquivoTipo::where('nome', 'Ata de Registro de Preços')->first()->id;
                    $anexos->descricao = 'Ata de Registro de Preços nº ' . $data['numero'];
                    $anexos->nome = '-';
                    $anexos->restrito = false;
                    $anexos->user_id = Auth::id();
                }
                $anexos->url = $file_path;
                # Método responsável em bloquear a Observer da Arp Arquivo
                # Para que não enviei as informações para o PNCP antes
                # do processamento da compra
                $anexos->unsetEventDispatcher();
                $anexos->save();
                # Retorna o evento do dispatcher
                # para chamar a observer da ARP
                Arp::setEventDispatcher($arpEventDispatcher);
            }
            $descricaoCodigo = 'Tipo de Ata de Registro de Preços';
            $data['arp_id'] = $data['id'];

//            $this->salvarHistoricoArp($data, $descricaoCodigo, $itensCadastrados);
//            $descricaoCodigo = 'Tipo de Ata de Registro de Preços';
            $this->updateCrud($request, ['gestor_ata', 'autoridadesignataria_ata']);
            $data['arp_id'] = $data['id'];
            $data['compra_id'] = $data['compra_id'];

            # Salva o registro inicial na tabela de histórico
            $this->salvarHistoricoArp($data, $descricaoCodigo, $itensCadastrados);

            Validator::make($request->all(), [
                'id_item_fornecedor.*' => [new ValidarItemAtaSolicitacao($itensCadastrados, $data['arp_id'])],
            ])->validated();

            DB::commit();
            return redirect('arp');
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            
            if (isset($ex->validator) && !empty($ex->validator->errors()->all())) {
                $allErrorMessages = $ex->validator->errors()->all();
                $mensagem = implode("<br>", $allErrorMessages);
                
                \Alert::add('error', $mensagem)->flash();
                return redirect()->back();
            }
            
            \Alert::add('error', 'Erro ao atualizar a ata')->flash();
            return redirect()->back();
        }
    }

    /**
     * Método responsável em recuperar os itens para o fornecedor via AJAX
     */
    public function itemCompraFornecedor(Request $request)
    {
        if ($request->ajax()) {
            $itensAta = null;

            # Recupera todos os itens da ata quando for na tela de rascunho
            if (!empty(Route::current()->parameter("arp_id"))) {
                $itensAta = ArpItem::where(
                    "arp_id",
                    Route::current()->parameter("arp_id")
                )->get()->pluck('compra_item_fornecedor_id')->toArray();
            }

            # Recuperar os itens para o fornecedor selecionado
            $itens = CompraItem::itemCompraPorFornecedor(
                $request->input('numero_ano'),
                $request->input('idFornecedor'),
                $request->input('idUnidade'),
                $request->input('modalidadeId'),
                $itensAta
            );

            $arrayItemSelecao = array();
            $compraItemFornecedorRepository = new CompraItemFornecedorRepository();
            $ndcService = new NovoDivulgacaoCompraService();
            
            foreach ($itens as $key => $item) {
                $itemCompraItemFornecedor =
                    $compraItemFornecedorRepository->getItemFornecedor($item->compraItemFornecedorId);

                $itemHabilitadoSelecao = $this->itemValidoParaSelecionar($itemCompraItemFornecedor);

                if (!$itemHabilitadoSelecao) {
                    continue;
                }
                
                $item->maximo_adesao =
                    $ndcService->formataMaximoAdesao($item->quantidadehomologadavencedor, $item->lei);

                $arrayItemSelecao[$key] = $item->toArray();
            }

            # Reindexar o array
            $arrayItemSelecao = array_values($arrayItemSelecao);

            return Datatables::of($arrayItemSelecao)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '<input type="checkbox" class="select-checkbox  br-input"
                                    id = "' . $row['compraItemFornecedorId'] . '"
                                    onclick="itemSelecionado(`' . $row['compraItemFornecedorId'] . '`)">';
                    return $actionBtn;
                })
                ->addColumn('quantidadehomologadavencedor', function ($row) {
                    $row['quantidadehomologadavencedor'] = number_format(
                        $row['quantidadehomologadavencedor'],
                        5,
                        ',',
                        '.'
                    ) .
                        ' <i class="fas fa-info-circle"
                        title="Clique para maiores detalhes sobre a quantidade registrada"
                        onclick="exibirDetalheItem(' . $row['compraId'] . ',' . backpack_user()->id . ',
                        ' . $row['compraItemId'] . ',`' . url('arp/exibirdetalheitem') . '`)">
                        </i>';

                    return $row['quantidadehomologadavencedor'];
                })
                ->addColumn('descricaosimplificada', function ($row) {
                    $row['descricaosimplificada'] = Str::limit(
                        trim($row['descricaosimplificada']),
                        50,
                        '<i class="fas fa-info-circle" title="' . $row['descricaosimplificada'] . '"></i>'
                    );
                    return trim($row['descricaosimplificada']);
                })
                ->addColumn('classificacao', function ($row) {
                    return $row['classificacao'] .
                        ' <i class="fas fa-info-circle"
                        title="Clique para maiores detalhes sobre a classificação"
                        onclick="exibirClassificacaoFornecedor(' . $row['compraItemFornecedorId'] . ',
                                    `' . url('arp/exibirclassificacaofornecedor') . '`, `' . $row['numero'] . '`)">
                        </i>';
                })
                ->addColumn('valor_unitario', function ($row) {
                    if ($row['percentual_maior_desconto'] > 0) {
                        return number_format($row['valor_unitario_desconto'], 4, ',', '.');
                    }
                    return number_format($row['valor_unitario'], 4, ',', '.');
                })
                ->addColumn('maximo_adesao', function ($row) {
                    return number_format((int)$row['maximo_adesao'], 0, ',', '.');
                })
                ->addColumn('maximo_adesao_informado_compra', function ($row) {
                    return number_format((int)$row['maximo_adesao_informado_compra'], 0, ',', '.');
                })
                ->addColumn('valor_negociado', function ($row) {
                    $row['valor_negociado'] = number_format($row['valor_negociado'], 2, ',', '.');

                    $row['valor_unitario_desconto'] = number_format($row['valor_unitario_desconto'], 4, ',', '.');
                    $row['valor_negociado_desconto'] = number_format($row['valor_negociado_desconto'], 4, ',', '.');
                    $row['valor_unitario'] = number_format($row['valor_unitario'], 4, ',', '.');

                    # Se existir desconto no item, então exibe o valor com o maior desconto
                    if ($row['percentual_maior_desconto'] > 0) {
                        $row['valor_negociado'] = $row['valor_negociado_desconto'] . ' <i class="fas fa-info-circle"
                        title="Clique para maiores detalhes sobre o desconto"
                        onclick="exibirDetalheMaiorDesconto(`' . number_format(
                            $row['percentual_maior_desconto'],
                            2,
                            ',',
                            '.'
                        ) . '`,
                        `' . $row['numero'] . '`,
                        `' . $row['descricaosimplificada'] . '`,
                        `' . $row['valor_unitario'] . '`,
                        `' . $row['valor_unitario_desconto'] . '`,
                        `' . $row['valor_negociado'] . '`,
                        `' . $row['valor_negociado_desconto'] . '`)"></i>';
                    }

                    return $row['valor_negociado'];
                })
                ->rawColumns([
                    'action',
                    'valor_negociado',
                    'quantidadehomologadavencedor',
                    'descricaosimplificada',
                    'classificacao'
                ])
                ->make(true);
        }

        return null;
    }

    /**
     * Método responsável em recuperar a compra ao tentar cadastrar ou editar ata via AJAX
     */
    public function validarCompra(Request $dadosCompra)
    {
        $compraSisrp = new CompraSisrp();
        $compraService = new CompraService();
        
        $retornoSiasg = $compraService->validarCompraAta($dadosCompra, session('user_ug_id'));

        $arrayCodigoRetornoErro = [202, 204];

        # Se o erro for retornado pelo SIASG
        if (in_array($retornoSiasg->codigoRetorno, $arrayCodigoRetornoErro)) {
            $retornoErro = $this->montarRespostaAlertJs(
                $retornoSiasg->codigoRetorno,
                $retornoSiasg->message,
                "warning"
            );
            return $retornoErro;
        }

        if ($retornoSiasg->origemConsulta !== 'base_de_dados') {
            $this->montaParametrosCompra($retornoSiasg, $dadosCompra);
        }

        DB::beginTransaction();
        try {
            $dadosCompraFormatado = $compraService->inserirAtualizarCompraAta($retornoSiasg, $dadosCompra);
            $compraOrigem = $dadosCompraFormatado['compra'];
            
            $unidadeOrigem = [$compraOrigem->unidade_origem_id];
            $unidadeOrigemId = $compraOrigem->unidade_origem_id;

            $fonteRetorno = $this->formatarNomeFonteApiCompra($retornoSiasg->origemConsulta);
            $mensagemOrigemFormatada = "<br><br> Origem: {$fonteRetorno}";

            $compraService->inserirAtualizarItemCompra(
                $compraOrigem,
                $dadosCompraFormatado['request'],
                backpack_user()->id
            );

            DB::commit();
        } catch (Exception $exc) {
            DB::rollback();
            # Insere o log customizado
            $this->inserirLogCustomizado('gestao_ata', 'error', $exc);
            # Montar o erro pelo usuário
            $retornoErro = $this->montarRespostaAlertJs($exc->getCode(), $exc->getMessage(), "warning");

            return response()->json($retornoErro, $exc->getCode());
        }
        
        $itensCompra = CompraItem::where(['compra_id' => (int)$compraOrigem->id, 'situacao' => true])->get();


        if (count($itensCompra) == 0) {
            $retornoSiasg = [
                'type' => 'warning',
                'message' => "Compra sem itens homologados. {$mensagemOrigemFormatada}",
                'code' => 422
            ];
            return response()->json($retornoSiasg);
        }
        
        # Para concluir alguns jobs dos participantes para que seja exibido o fornecedor
        sleep(3);

        $unidadeOrigem = array_unique($unidadeOrigem);
        // $unidadeOrigemJson = json_encode($unidadeOrigem) ;
        $dadosCompra = $this->verificaCompraExiste($dadosCompra, $unidadeOrigem, true);

        $retornoSiasg = [
            'type' => 'success',
            'message' => 'Sua compra e os itens foram carregados,
                            mas as quantidades registradas por unidade ainda estão sendo processadas. <br>
                            Acompanhe esse processamento clicando no ícone do campo
                            "Quantidade registrada" de cada item. <br>
                            Observação: Somente os itens com situação homologada serão exibidos.'.
                            $mensagemOrigemFormatada,
            'code' => 200
        ];
        
        
        $fornecedores = array();

        # Recupera os fornecedores que serão exibidos para o usuário
        $dadosFornecedor = CompraItemFornecedor::fornecedorCompra($dadosCompra->all());

        $arrayFornecedor = array();
        foreach ($dadosFornecedor as $key => $fornecedor) {
            $fornecedorHabilitadoSelecao = $this->itemValidoParaSelecionar($fornecedor);

            # Se o item estiver selecionado para uma ata e não for cancelado em nenhum momento
            if (!$fornecedorHabilitadoSelecao) {
                continue;
            }

            $arrayFornecedor[$fornecedor->fornecedor_id]['id'] = $fornecedor->fornecedor_id;
            $arrayFornecedor[$fornecedor->fornecedor_id]['nome'] = $fornecedor->fornecedor->nome;
            $arrayFornecedor[$fornecedor->fornecedor_id]['cpf_cnpj_idgener'] =
                $fornecedor->fornecedor->cpf_cnpj_idgener;

            $iconeSicaf = "ban";
            # Se o fornecedor for ativo no SICAF, altera o ícone para válido
            if ($fornecedor->situacao_sicaf == 1) {
                $iconeSicaf = "check";
            }

            $arrayFornecedor[$fornecedor->fornecedor_id]['situacao_sicaf'] = "<i class='las la-{$iconeSicaf}'></i>";

            # Substituir a aspas simples pelo código asc
            $nomeFornecedorFormatado =
                str_replace("'", '&apos;', $arrayFornecedor[$fornecedor->fornecedor_id]['nome']);


            $arrayFornecedor[$fornecedor->fornecedor_id]['botaoitem'] =
                "<a class='br-button circle primary large mr-3 br-button-row-table'
                    href='javascript:void(0)'
                    onclick='exibirItemFornecedor(
                        {$fornecedor->fornecedor_id},
                    `{$nomeFornecedorFormatado}`
                    ,{$unidadeOrigemId})'
                    title='Clique para exibir os itens do fornecedor' >
                        <i class='fas fa-plus'></i>
                    </a>";
        }

        # Reindexar o array
        $arrayFornecedor = array_values($arrayFornecedor);

        $retornoSiasg['data'] = $arrayFornecedor;
        return response()->json($retornoSiasg);
    }

    public function gestores(Request $request)
    {
        $search_term = $request->input('q');

        if ($search_term) {
            $search_term = strtoupper($search_term);
            $results = User::where(
                'ugprimaria',
                session('user_ug_id')
            )->where(function ($query) use ($search_term) {
                $query->where('name', 'LIKE', "%{$search_term}%")
                    ->orWhere('cpf', 'LIKE', "%{$search_term}%");
            })
                ->select('id', DB::raw("CONCAT(name,' - ',cpf) AS nome_arp"))
                ->paginate(10);
        } else {
            $results = User::select('id', DB::raw("CONCAT(name,' - ',cpf) AS nome_arp"))->paginate(10);
        }

        return $results;
    }

    public function autoridadeSignataria(Request $request)
    {
        $search_term = $request->input('q');

        $results = AutoridadeSignataria::autoridadeSignatariaArp($search_term);

        return $results;
    }

    public function validarnumeroata(Request $request)
    {
        $numeroAta = $request->input('numeroAta');
        $anoAta = $request->input('anoAta');
        $unidadeGerenciadora = $request->input('unidadeGerenciadora');

        # Nega o retorno da função, pois se não existe, a Ata está válida para cadastro
        return !$this->ataJaExiste($numeroAta, $anoAta, $unidadeGerenciadora);
    }

    public function getUasgs(Request $request)
    {
        $uasgs = Unidade::orderBy('id', 'ASC')->select(['id', 'codigo', 'nome']);
        $query = $request->get('q');
        if ($query) {
            $uasgs->where('codigo', 'ILIKE', '%' . $query . '%')
                ->orWhere('nome', 'ILIKE', '%' . $query . '%');
        }
        return ($uasgs->take(20)->get()->toArray());
    }

    public function getSelectedUasgs(Request $request)
    {
        $uasgs = Unidade::whereIn('id', $request->items)
            ->select([
                'id',
                DB::raw("CONCAT(codigo, ' ', nome) AS text"),
            ]);
        return ($uasgs->take(20)->get()->toArray());
    }

    public function filterUasgs(Request $request)
    {

        $filtro['vigencia_inicial'] = $request->vigencia_inicial ? $request->vigencia_inicial : null;
        $filtro['vigencia_final'] = $request->vigencia_final ? $request->vigencia_final : null;
        $filtro['uasgs'] = $request->uasg ? $request->uasg : [];
        $filtro['numero_ano'] = $request->numero_ano ? $request->numero_ano : null;
        $filtro['gestor'] = $request->gestor ? $request->gestor : null;

        Session::put('filtro', $filtro);

        return redirect()->back();
    }


    private function montarDadosHistorico(array $dados)
    {
        $dadosHistorico = [];
        $dadosHistorico['arp_id'] = $dados['arp_id'];
        $dadosHistorico['rascunho'] = true;
        $dadosHistorico['sequencial'] = 0;

        return $dadosHistorico;
    }

    private function salvarHistoricoArp(array $dados, string $descricaoCodigo, ?array $itensCadastrados)
    {
        if (!$dados['rascunho']) {
            $dados['tipo_id'] = $this->retornaIdCodigoItem($descricaoCodigo, 'Ata Inicial');
            $dados['compra_centralizada'] = $dados['compra_centralizada'];

            // $dadosAlteracao = $this->montarDadosAlteracao($dados);
            // $idAlteracaoArp = ArpHistorico::create($dados)->id;

            // $dados['arp_alteracao_id'] = $idAlteracaoArp;

            # Cria o registro da ata para o histórico
            $idArpHistorico = ArpHistorico::create($dados)->id;

            # Insere os itens salvos na ata para o histórico
            $this->inserirItemArpHistorico(
                $itensCadastrados,
                $idArpHistorico,
                $dados['vigencia_inicial'],
                $dados['vigencia_final']
            );
            // # Percorre todos os itens cadastrados para incluir na arp_item_historico
            // foreach ($itensCadastrados as $itemHistorico) {
            //     $arpItemHistorico = new ArpItemHistorico();
            //     $arpItemHistorico->arp_historico_id =$idArpHistorico;
            //     $arpItemHistorico->arp_item_id = $itemHistorico;
            //     $arpItemHistorico->vigencia_inicial = $dados['vigencia_inicial'];
            //     $arpItemHistorico->vigencia_final = $dados['vigencia_final'];

            //     # Busca os itens a partir do histórico para
            //     # poder pegar as datas de vigência
            //     $arpItem = ArpItem::find($itemHistorico);

            //     # Recupera o valor unitário do item para salvar no histórico
            //     $arpItemHistorico->valor = $arpItem->item_fornecedor_compra->valor_unitario;

            //     $arpItemHistorico->save();
            // }
        }
    }

    # Método responsável em recuperar todos os fornecedores para o item selecionado via AJAX
    public function exibirClassificacaoFornecedor(Request $request)
    {
        $itemFornecedor = CompraItemFornecedor::find($request->all()['idItemFornecedor']);
        $fornecedores = [];

        # Recupera as informações sobre o fornecedor e ordena pela classificação
        $listaItemFornecedor = $itemFornecedor->compraItens->compraItemFornecedor()->orderBy("classificacao")->get();

        foreach ($listaItemFornecedor as $key => $itemFornecedor) {
            $fornecedores[$key]['cpf_cnpj_idgener'] = $itemFornecedor->fornecedor->cpf_cnpj_idgener;
            $fornecedores[$key]['nome'] = $itemFornecedor->fornecedor->nome;
            $fornecedores[$key]['classificacao'] = $itemFornecedor->classificacao;
            $fornecedores[$key]['quantidade_homologada_vencedor'] = number_format(
                $itemFornecedor->quantidade_homologada_vencedor,
                5,
                ',',
                '.'
            );
            $fornecedores[$key]['valor_unitario'] = number_format($itemFornecedor->valor_unitario, 4, ',', '.');
        }

        return $fornecedores;
    }


    # Método responsável em exibir o detalhe da coluna Quantidade Registrada do item
    public function exibirDetalheItem(Request $request)
    {
        $dados = $request->all();
        $retorno = array();

        # Recupera os jobs em processamento para a compra
        $compraProcessando = JobUser::where("job_user_type", Compras::class)
            ->where("job_user_id", $dados['compraId']);

        # Informa o retorno de sucesso
        $retorno['code'] = 200;

        # Clona a query para aplicar o filtro necessário
        $existeItemProcessado = clone $compraProcessando;

        # Recupera o item selecionado na tabela job_user para ver se o item já foi processado
        $itemProcessado = $existeItemProcessado
            ->where("compra_item_id", $dados['compraItemId'])
            ->first();
        # Recupera o total de itens na fila para processamento
        $totalItemFila = $compraProcessando->count();

        # Se existir o processamento do item, o usuário receberá uma mensagem de alerta
        # Se o item não existir na tabela job_user, porque já foi processado
        # então sai do condicional dos itens processados
        if ($totalItemFila > 0 && !empty($itemProcessado)) {
            # Informa o código do erro para exibir o alerta
            $retorno['code'] = 423;

            # Clona a query para aplicar o filtro necessário
            $startProcessamento = clone $compraProcessando;

            # Recupera o item que está sendo processado
            $compra = $startProcessamento->where("processando", true)->first();

            # Se ainda não tiver iniciado o processado, retorna para o usuário
            # a mensagem informando que ainda não foi iniciado
            if (empty($compra)) {
                $retorno['message'] = "A inclusão dos participantes ainda não foi iniciada.";
                return $retorno;
            }

            $totalItemFila -= 1;
            $percentualInclusao =
                (int)((1 - ($totalItemFila / $compra->compraItem->compra->item_compra->count())) * 100);
            $retorno['percentual'] = $percentualInclusao;
            $retorno['message'] = "A inclusão dos participantes dos itens está em processamento.
                                    O item atual em processamento é o {$compra->compraItem->numero}. Para os anteriores
                                    a informação já está disponível.";
            return $retorno;
        }

        # Recupera as unidades que pertence ao item
        $item = CompraItem::find($dados['compraItemId']);

        # Ordena as unidades com base no tipo
        $compraItemUnidade = $item->compraItemUnidade()
            ->where("situacao", true)
            ->orderByRaw("CASE
                                            WHEN compra_item_unidade.tipo_uasg = 'G' THEN 1
                                            WHEN compra_item_unidade.tipo_uasg = 'P' THEN 2
                                            WHEN compra_item_unidade.tipo_uasg = 'C' THEN 3
                                            ELSE 0 END")
            ->get();

        # Percorre as unidades encontradas montando um array para enviar no front
        foreach ($compraItemUnidade as $key => $itemUnidade) {
            $retorno['data'][$key] = [
                'unidade' => $itemUnidade->getCodigoUnidadeAttribute(),
                'tipo' => $itemUnidade->tipo_uasg,
                'quantidade' => number_format($itemUnidade->quantidade_autorizada, 4, ',', '.')
            ];
        }

        return $retorno;
    }

    # Método responsável em reprocessar a queue com falha
    public function jobReprocessaItemCompraParticipanteGestaoAta()
    {
        # Recupera o nome da queue
        $nomeJob = config('arp.arp.nome_queue');

        # Executa o comando para reprocessar a queue no todo
        Artisan::call("queue:retry --queue={$nomeJob}");
    }

    /**
     * Método responsável em enviar a Atas em background para o PNCP
     */
    public function jobEnviarAtaPNCP()
    {
        # Recupera o ID do status Carregando Compra
        $idCarregandoCompra = $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', 'Carregando Compra');

        # Recupera o ID do status Erro Publicar
        $idErroPublicar = $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', 'Erro Publicar');

        # Recupera o status Enviar PNCP
        $idStatusEnviarPncp = $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', 'Enviar PNCP');

        # Recupera o status Publicando PNCP
        $idStatusPublicandoPncp =
            $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', 'Publicando PNCP');

        # Recupera o status para ata vigente
        $idStatusAtaRegistroPreco =
            $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', 'Ata de Registro de Preços');

        # Responsável em recuperar todas as atas com o status Enviar PNCP e Erro Publicar
        $ataPorEnviarPNPCP = Arp::whereIn("tipo_id", [$idCarregandoCompra, $idErroPublicar])
            ->where("rascunho", false)
            ->get();
        # Inicia o processo de envio para o PNCP
        $pncpManagerService = new PncpManagerService();

        # Percorre as atas que estão com o status Carregando Compra
        foreach ($ataPorEnviarPNPCP as $ataPorEnviar) {
            # Insere a Model da ARP na sessão para poder alterar o status em
            # caso de falha
            session(['ataporenviarerro' => $ataPorEnviar]);

            # Recuperar os JOBS que estão sendo executados pela compra da ARP
            $jobAtivo = JobUser::where("job_user_type", Compras::class)
                ->where("job_user_id", $ataPorEnviar->compra_id)
                ->count();

            # Se não existir mais JOB para a importação dos participantes da compra
            # Altera para o status Enviar PNCP
            if ($jobAtivo == 0) {
                # Recupera as informações da Ata na tabela de histórico do PNCP
                $alreadyExistsInPncp = $this->getArpPNCP($ataPorEnviar);

                Log::debug($alreadyExistsInPncp->isEmpty());

                # Se o registro existir na tabela responsável pelo envio para o PNCP
                if (!$alreadyExistsInPncp->isEmpty()) {
                    Log::debug('linha 2238');
                    $this->ataExisteTabelaPNCP(
                        $ataPorEnviar,
                        $idStatusAtaRegistroPreco,
                        $idErroPublicar,
                        $pncpManagerService
                    );
                    continue;
                }

                # Verifica se o registro existe no PNCP, se não existir, enviar para o PNCP
                if ($alreadyExistsInPncp->isEmpty()) {
                    Log::debug('linha 2250');
                    $this->ataNaoExisteTabelaPNCP(
                        $pncpManagerService,
                        $ataPorEnviar,
                        $idStatusEnviarPncp,
                        $idStatusPublicandoPncp,
                        $idStatusAtaRegistroPreco,
                        $idErroPublicar
                    );
                }
            }
        }
    }

    private function ataNaoExisteTabelaPNCP(
        PncpManagerService $pncpManagerService,
        Arp                $ataPorEnviar,
        int                $idStatusEnviarPncp,
        int                $idStatusPublicandoPncp,
        int                $idStatusAtaRegistroPreco,
        int                $idErroPublicar
    ) {
        # Altera o status para o Enviar PNCP
        $this->alterarStatusAta($idStatusEnviarPncp, $ataPorEnviar);

        try {
            PncpService::addIfNotExists($ataPorEnviar->compras->cnpjOrgao);
        } catch (\Exception $e) {
            Log::error("Erro ao adicionar órgão: " . $e->getMessage());
        }

        # Inserção do try caso aconteça erro na ação do envio
        try {
            # Altera o status da Ata para Enviar PNCP
            $this->alterarStatusAta($idStatusPublicandoPncp, $ataPorEnviar);

            # Percorre todo os itens salvos na ata para incluir as datas de vigência inicial e final
            # que foram salvas no cadastro da ata
            $this->inserirVigenciaItem($ataPorEnviar);

            # Envia as informações para o PNCP
            $pncpManagerService->managePncp(
                new InsertArpPncpService(),
                new PncpPostResponseService(),
                $ataPorEnviar
            );

            # Envia o arquivo para o PNCP
            $this->publicarArquivoPNCP(
                $ataPorEnviar,
                $pncpManagerService,
                new InserirArquivoArpPncpService(),
                new PncpPostResponseService()
            );

            # Altera o status para Ata de Registro de Preço
            $this->alterarStatusAta($idStatusAtaRegistroPreco, $ataPorEnviar);
        } catch (Exception $ex) {
            Log::debug('linha 2308');
            Log::debug($ex->getMessage());
            # Se a ata já foi enviada para o PNCP e não existir
            # o registro no banco de dados, busca as informações atualizar
            # de forma manual
            if (str_contains(
                $ex->getMessage(),
                'Inclusão não permitida pois já existe'
            )) {
                try {
                    # Método responsável em verificar se a Ata existe no pncp
                    $pncpManagerService->managePncp(
                        new ConsultarTodasArpPncpService(),
                        new PncpGetArpUnicaResponseService(),
                        $ataPorEnviar
                    );

                    $arquivo = $this->recuperarArquivoAta($ataPorEnviar);
                    $arquivoSalvoPncp = (new ConsultarArquivoArpPncpService())->sendToPncp($arquivo);

                    if (!in_array($arquivoSalvoPncp->getStatusCode(), [200, 201])) {
                        throw new Exception(
                            $arquivoSalvoPncp->getBody()->getContents(),
                            $arquivoSalvoPncp->getStatusCode()
                        );
                    }

                    # Altera a vigência do item da compra na tabela compra_item
                    $this->inserirVigenciaItem($ataPorEnviar);

                    # Altera o status para Ata de Registro de Preço
                    $this->alterarStatusAta($idStatusAtaRegistroPreco, $ataPorEnviar);
                } catch (Exception $ex) {
                    try {
                        if (strpos($ex->getMessage(), 'Documento não encontrado') !== false) {
                            $this->publicarArquivoPncpJobEnviarAta(
                                $ataPorEnviar,
                                $pncpManagerService,
                                $idStatusAtaRegistroPreco
                            );
                        }

                        // Em alguns casos quando a ata não tem arquivo o PNCP está essa situação
                        if (empty($ex->getMessage()) && $ex->getCode() == 204) {
                            $this->publicarArquivoPncpJobEnviarAta(
                                $ataPorEnviar,
                                $pncpManagerService,
                                $idStatusAtaRegistroPreco
                            );
                        }
                    } catch (Exception $innerEx) {
                        $this->inserirLogCustomizado('pncp_ata', 'error', $innerEx);
                    }
                }

                return;
            }

            if (str_contains(
                $ex->getMessage(),
                'Serviço sobre ente'
            )) {
                PncpService::addOrgao($ataPorEnviar->compras->cnpjOrgao);
                Log::info("Tentando adicionar órgão na linha 2172: " . $ataPorEnviar->compras->cnpjOrgao);
            }

            $this->inserirLogCustomizado('pncp_ata', 'error', $ex);

            # Realiza a alteração do status
            $this->alterarStatusAta($idErroPublicar, $ataPorEnviar);
        }
    }

    /**
     * @param Arp $ataPorEnviar
     * @param int $idStatusAtaRegistroPreco
     * @param int $idErroPublicar
     * @return void
     */
    private function ataExisteTabelaPNCP(
        Arp $ataPorEnviar,
        int $idStatusAtaRegistroPreco,
        int $idErroPublicar,
        PncpManagerService $pncpManagerService
    ) {

        # Se existir o link PNCP, então está publicada
        if (!empty($ataPorEnviar->enviaDadosPncp->link_pncp)) {
            try {
                DB::beginTransaction();

                $arquivo = $this->recuperarArquivoAta($ataPorEnviar);

                $arquivoSalvoPncp = (new ConsultarArquivoArpPncpService())->sendToPncp($arquivo);

                if (!in_array($arquivoSalvoPncp->getStatusCode(), [200, 201])) {
                    new Exception($arquivoSalvoPncp->getBody()->getContents(), $arquivoSalvoPncp->getStatusCode());
                }

                $this->inserirVigenciaItem($ataPorEnviar);

                $this->alterarStatusAta($idStatusAtaRegistroPreco, $ataPorEnviar);

                DB::commit();
            } catch (Exception $ex) {
                Log::debug($ex->getCode());
                # Mensagem de retorno do PNCP quando não existir o arquivo no PNCP, então devemos enviar
                if ($ex->getCode() == 404) {
                    $retornoPncp = json_decode($ex->getResponse()->getBody()->getContents(), true);
                    Log::debug(json_encode($retornoPncp));
                    if (strpos($retornoPncp['message'], 'Documento não encontrado') !== false) {
                        Log::debug('linha 2383');
                        $this->publicarArquivoPncpJobEnviarAta(
                            $ataPorEnviar,
                            $pncpManagerService,
                            $idStatusAtaRegistroPreco
                        );
                    }
                    return;
                }
                Log::debug('linha 2399');
                DB::rollBack();
                $this->inserirLogCustomizado('pncp_ata', 'error', $ex);
                # Realiza a alteração do status
                $this->alterarStatusAta($idErroPublicar, $ataPorEnviar);
                Log::debug('linha 2404');
            }
        }

        # Se não existir o link do PNCP, mas
        if (empty($ataPorEnviar->enviaDadosPncp->link_pncp)) {
            try {
                DB::beginTransaction();
                # Método responsável em verificar se a Ata existe no pncp
                $pncpManagerService->managePncp(
                    new ConsultarTodasArpPncpService(),
                    new PncpGetArpUnicaResponseService(),
                    $ataPorEnviar
                );

                # Atualizar as informações do banco de dados
                $ataPorEnviar->refresh();

                # Se existir o link, porque as informações foram recuperadas e então enviamos o arquivo
                if (!empty($ataPorEnviar->enviaDadosPncp->link_pncp)) {
                    $this->publicarArquivoPncpJobEnviarAta(
                        $ataPorEnviar,
                        $pncpManagerService,
                        $idStatusAtaRegistroPreco
                    );
                }

                DB::commit();
            } catch (Exception $ex) {
                DB::rollBack();
                $this->inserirLogCustomizado('pncp_ata', 'error', $ex);
                # Realiza a alteração do status
                $this->alterarStatusAta($idErroPublicar, $ataPorEnviar);
            }
        }
    }

    /**
     * Método responsável em alterar o status da ata
     */
    private function alterarStatusAta(int $idStatus, Arp $ataPorEnviar)
    {
        $ataPorEnviar->tipo_id = $idStatus;
        $ataPorEnviar->unsetEventDispatcher();
        $ataPorEnviar->save();
    }

    /**
     * Método responsável em validar se a compra informada
     * existe no banco de dados
     */
    public function validarCompraCadastro(Request $dadosCompra)
    {
        # Recupera a compra na base de dados
        $dadosCompra = $this->retornaDadosCompra($dadosCompra->all());

        # Se não existir a compra, retorna falso para exibir
        # o alerta para o usuário
        if (empty($dadosCompra)) {
            return false;
        }

        # Se existir, pode continuar o cadastro
        return true;
    }

    public function visualizarArpPDF($id, $tipo = 'visualizar')
    {
        // Verifique se o PDF foi gerado para o ARP com o ID especificado
        $pdfRecord = ArpPdf::where('arp_id', $id)->first();
        if (!$pdfRecord || !$pdfRecord->gerado) {
            // O PDF não existe ou não foi gerado,
            // redirecione ou retorne uma mensagem de erro.
            return redirect()->back()->with(
                'error',
                'O PDF ainda não foi gerado.'
            );
        }
        $pdfPath = storage_path($pdfRecord->caminho_pdf);
        if ($tipo == 'visualizar') {
            return response()->file($pdfPath);
        }

        return response()->download($pdfPath, basename($pdfPath));
    }

    public function dispararJobArpPDF(
        $arpId,
        ArpPdfRepository $arpItemPdfRepository,
        RelatorioArpPdfService $relatorioArpPdfService,
        Request $request
    ) {
        try {
            $nomeJobRelatorio = config('arp.arp.nome_queue_relatorio');
            $pdfRecord = $arpItemPdfRepository->getItemsByBuscaHistoricoDiaPdf($arpId);

            $pdfUrl = route('visualizar.arp.pdf', ['id' => $arpId, 'tipo' => 'download']);


            if ($pdfRecord && $pdfRecord->gerado) {
                return response()->json([
                    'success' => true,
                    'pdfUrl' => $pdfUrl
                ]);
            } else {
                $jobQueue = $this->dispararJobArpRelatorio($arpId, $arpItemPdfRepository, $nomeJobRelatorio);
                ArpPdf::where('arp_id', $arpId)->delete();
                Bus::dispatch($jobQueue);
                return response()->json([
                    'success' => true,
                    'pdfUrl' => $pdfUrl
                ]);
            }
        } catch (\Exception $e) {
            \Log::info('Erro ao Gerar o Pdf');
            return response()->json(['success' => false]);
        }
    }


    private function dispararJobArpRelatorio(
        $arpId,
        $arpItemPdfRepository,
        $nomeJobRelatorio
    ) {
        return (
        new GenerateRelatorioArpPdfJob($arpId, $arpItemPdfRepository)
        )->onQueue($nomeJobRelatorio);
    }

    public function checkStatusArpPDF($arpId)
    {
        $pdfRecord = ArpPdf::where('arp_id', $arpId)->first();
        if ($pdfRecord && $pdfRecord->gerado) {
            return response()->json(['gerado' => true]);
        } else {
            return response()->json(['gerado' => false]);
        }
    }
    private function filtros()
    {

        $situacao = ['Ata de Registro de Preços' => 'Ativa',
                    // 'Encerrada' =>'Vg. Expirada',
                    'Cancelada'=>'Cancelada',
                    'Carregando Compra' => 'Carregando compra',
                    'Em elaboração' => 'Em elaboração',
                    'Erro Publicar' => 'Erro ao publicar',
                ];
        $this->crud->addFilter(
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'select2_multiple',
            ],
            $situacao,
            function ($value) {

                $decode_value = json_decode($value);
                if (!in_array("Encerrada", $decode_value)) {
                    $this->crud->addClause('whereIn', 'codigoitens.descricao', $decode_value);
                    if (in_array("Ata de Registro de Preços", $decode_value)) {
                        $date_now = Carbon::now()->toDateString();
                        $this->crud->query->whereRaw("'$date_now' between arp.vigencia_inicial and arp.vigencia_final");
                    }
                    return;
                }

                $date_now = Carbon::now()->toDateString();
                $this->crud->query->whereRaw("'$date_now' not between arp.vigencia_inicial and arp.vigencia_final");
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'numero_ano_ata',
                'label' => 'Número/Ano da Ata',
                'type' => 'text',
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', function ($query) use ($value) {
                    $query->where('arp.numero', 'LIKE', "%$value%")
                        ->orWhere('arp.ano', 'LIKE', "%$value%");
                });
            }
        );

        $this->crud->addFilter(
            [
            'name'        => 'unidade_origem_id',
            'type'        => 'select2_ajax',
            'label'       => 'Unidade Gerenciadora da Ata',
            'placeholder' => '',
            'minimum_input_length' => 0,
            'method'      => 'GET',
            'select_attribute' => 'nome_completo',
            'select_key' => 'id'
            ],
            url('/compras/ajax-unidades-options'),
            function ($value) {
                $this->crud->addClause('where', 'arp.unidade_origem_id', $value);
            }
        );

        $uasg = [
            'G' =>'Gerenciadora',
            'P' => 'Participante',
            'C'=>'Não participante',
            ];
        $this->crud->addFilter(
            [
                'name' => 'uasg',
                'label' => 'Tipo Uasg',
                'type' => 'select2_multiple',
            ],
            $uasg,
            function ($value) {
                $decode_value = json_decode($value);
                $this->crud->addClause('whereIn', 'compra_item_unidade.tipo_uasg', $decode_value);
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'vigencia_inicio',
                'label' => 'Vig. inicial',
                'type' => 'date',
                'language' => 'pt-BR'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'arp.vigencia_inicial', '>=', $value);
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'vigencia_fim',
                'label' => 'Vig. final',
                'type' => 'date',
                'language' => 'pt-BR'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'arp.vigencia_final', '<=', $value);
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'numero_compra',
                'label' => 'Número da compra',
                'type' => 'text',
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'compras.numero_ano', 'LIKE', "%$value%");
            }
        );
    }

    private function publicarArquivoPncpJobEnviarAta(
        Arp $ataPorEnviar,
        PncpManagerService $pncpManagerService,
        int $idStatusAtaRegistroPreco
    ) {
        $this->publicarArquivoPNCP(
            $ataPorEnviar,
            $pncpManagerService,
            new InserirArquivoArpPncpService(),
            new PncpPostResponseService()
        );

        $this->inserirVigenciaItem($ataPorEnviar);

        $this->alterarStatusAta($idStatusAtaRegistroPreco, $ataPorEnviar);
    }
}
