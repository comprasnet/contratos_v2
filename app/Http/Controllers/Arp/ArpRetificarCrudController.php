<?php

namespace App\Http\Controllers\Arp;

use App\Http\Controllers\Operations\DeleteOperation;
use App\Http\Controllers\Operations\UpdateOperation;
use App\Http\Requests\ArpAlteracaoRequest;
use App\Http\Requests\ArpRetificarRequest;
use App\Http\Traits\Arp\ArpItemHistoricoTrait;
use App\Http\Traits\Arp\ArpItemTrait;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\CommonFields;
use App\Http\Traits\Formatador;
use App\Http\Traits\LogTrait;
use App\Http\Traits\ImportContent;
use App\Http\Traits\Arp\ArpTrait;
use App\Models\Arp;
use App\Models\ArpAutoridadeSignataria;
use App\Models\ArpAutoridadeSignatariaHistorico;
use App\Models\ArpHistorico;
use App\Models\ArpItem;
use App\Models\ArpItemHistorico;
use App\Models\CodigoItem;
use App\Models\CompraItemFornecedor;
use App\Repositories\Arp\ArpItemRepository;
use App\Repositories\CodigoItemRepository;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Illuminate\Database\Events\StatementPrepared;

/**
 * Class ArpRetificarCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArpRetificarCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    use CommonFields;
    use BuscaCodigoItens;
    use ArpItemHistoricoTrait;
    use LogTrait;
    use ImportContent;
    use ArpTrait;
    use ImportContent;
    use ArpItemTrait;

    protected function setupShowOperation()
    {
        $this->crud->set('show.contentClass', 'col-md-12');

        CRUD::addColumn([
            'name' => 'numero_alteracao',
            'label' => 'Número retificado',
            'visibleInShow' => true
        ]);
        
        $this->addColumnModelFunction('numero_ano', 'Ano alteração', 'getAnoAlteracao');
        
        $this->addColumnDate(true, true, true, true, 'created_at', 'Data da Retificação');

        $this->addColumnDate(true, true, true, true, 'data_assinatura_alteracao', 'Data da assinatura retificada');
        $this->addColumnDate(true, true, true, true, 'vigencia_inicial_alteracao', 'Vigência inicial retificada');
        $this->addColumnDate(true, true, true, true, 'vigencia_final_alteracao', 'Vigência final retificada');

        CRUD::addColumn([
            'name' => 'objeto_alteracao',
            'label' => 'Objeto retificado',
            'visibleInShow' => true,
            'escaped' => false
        ]);
        $this->addColumnModelFunction('numero_ano', 'Compra centralizada retificada', 'getCompraCentralizadaAlteracao');

        $idArpHistorico = Route::current()->parameter("id");
        $idAta = Route::current()->parameter("arp_id");
        $autoridades = ArpHistorico::getAutoridadeSignataria($idArpHistorico);
        $this->addColumnTable('autoridade_signataria_ata', 'Autoridades', $autoridades);

        CRUD::addColumn([
            'name' => 'justificativa_motivo_cancelamento',
            'label' => 'Justificativa/Motivo da retificação',
            'visibleInShow' => true,
            'escaped' => false
        ]);

        $this->addColumnModelFunction('responsavel', 'Responsável pela retificação', 'getResponsavel');

        /****************************************************/
        /*** Listagem dos itens removidos por retificação ***/
        /****************************************************/
        $arpItemRepository = new ArpItemRepository();
        $itensRemovidosPorRetificacao = $arpItemRepository->getItensRemovidosEmRetificacaoPorAta($idArpHistorico);

        $itens = [];
        # Organiza o array no formato para o backpack montar a tabela
        foreach ($itensRemovidosPorRetificacao as $key => $item) {
            $itens['Fornecedor'][$key] = $item->fornecedor;
            $itens['Número'][$key] = $item->numero;
            $itens['Descrição'][$key] = $item->descricaodetalhada;
            $itens['Valor_Unitário'][$key] = $item->valor_unitario;
        }
        $this->addColumnTable('itens_removidos', 'Itens removidos por retificação', $itens);

        $this->crud->label_observacao_show = 'Somente as informações retificadas serão exibidas';
    }

    /**
     * Método responsável em montar o breadcrumd customizado
     */

    private function breadCrumb(bool $acaoVoltar = false)
    {
        $arp = Arp::find($this->returnIdArp());
        $bread = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Ata de Registro de Preços' => backpack_url('arp'),
            'Retificaçao Ata de Registro de Preços' => false,
            $arp->getNumeroAno() => false
        ];
        // if ($acaoVoltar) {
        //     $bread['Voltar'] = backpack_url('arp/retificar/' . $this->returnIdArp());
        //     return $bread;
        // }
        // $bread['Voltar'] = backpack_url('arp/');

        return $bread;
    }

    /**
     * Método responsável em retornar o id da ata na url
     */
    private function returnIdArp()
    {
        return Route::current()->parameter("arp_id");
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */

    protected $arp; // declaração da variável $arp como uma propriedade da classe

    public function setup()
    {
        $this->arp = Arp::find($this->returnIdArp()); // atribui o objeto Arp à propriedade $arp

        /*
        CRUD::setModel(\App\Models\ArpHistorico::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/arp/retificar/' . $this->returnIdArp());
        CRUD::addclause("join", "codigoitens", "codigoitens.id", "=", "arp_historico.tipo_id");
        CRUD::addclause("join", "codigos", "codigoitens.codigo_id", "=", "codigos.id");
        CRUD::addclause("where", "codigoitens.descricao", "=", "Retificar");
        CRUD::addclause("where", "codigos.descricao", "=", "Tipo de Ata de Registro de Preços");
        CRUD::addclause("where", "arp_historico.arp_id", "=", $this->returnIdArp());
        CRUD::addclause("select", "arp_historico.*");*/
        $this->crud->setModel(\App\Models\ArpHistorico::class);
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/arp/retificar/' . $this->returnIdArp());
        $this->crud->addClause("join", "codigoitens", "codigoitens.id", "=", "arp_historico.tipo_id");
        $this->crud->addClause("join", "codigos", "codigoitens.codigo_id", "=", "codigos.id");
        $this->crud->addClause("where", function ($query) {
            $query->where("codigoitens.descricao", "=", "Retificar")
                ->orWhere("codigoitens.descricao", "=", "Retificar - Excluir item")
                ->orWhere("codigoitens.descricao", "=", "Retificar - Dados da ata e exclusão de item");
        });
        $this->crud->addClause("where", "codigos.descricao", "=", "Tipo de Ata de Registro de Preços");
        $this->crud->addClause("where", "arp_historico.arp_id", "=", $this->returnIdArp());
        $this->crud->addClause("select", "arp_historico.*");

        $this->exibirTituloPaginaMenu('Retificar Ata de Registro de Preços');

        $acoesBloqueadas = ['update', 'delete'];

        if ($this->arp->tipo->descricao == 'Cancelada') {
            $acoesBloqueadas [] = 'create';
        }

        $this->bloquearBotaoPadrao($this->crud, $acoesBloqueadas);



        $this->data['breadcrumbs'] = $this->breadCrumb();
        return view('backpack::dashboard', $this->data);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        $this->crud->cabecalhoAta = true;

        $this->areaLabelSimplificada($this->arp);

        $this->crud->cabecalho = 'vendor.backpack.crud.retificarCabecalho';
        $this->addColumnModelFunction('data_retificacao', 'Data da retificação', 'getDataRetificacao');

        $this->crud->text_button_redirect_create = 'retificação';

        // Exibe o breadcrumb customizado
        $this->data['breadcrumbs'] = $this->breadCrumb(false);
    }

    /**
     * Método responsável em exibir os campos para o usuário
     * que são somente label
     */
    private function areaLabelSimplificada(Arp $arp)
    {
        $this->crud->addField([
            'name' => 'label_numero_ano', // The db column name
            'type' => 'label',
            'label' => 'Número/Ano', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->getNumeroAno()
        ]);

        $this->crud->addField([
            'name' => 'label_unidade_gerenciadora', // The db column name
            'type' => 'label',
            'label' => 'Unidade Gerenciadora', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->getUnidadeOrigem()
        ]);

        $this->crud->addField([
            'name' => 'label_numero_processo', // The db column name
            'type' => 'label',
            'label' => 'Número do Processo', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->numero_processo
        ]);

        $this->crud->addField([
            'name' => 'label_data_assinatura', // The db column name
            'type' => 'label',
            'label' => 'Data da Assinatura', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->data_assinatura,
            'date' => true
        ]);

        $this->crud->addField([
            'name' => 'label_data_vigencia_inicial', // The db column name
            'type' => 'label',
            'label' => 'Vigência Inicial', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->vigencia_inicial,
            'date' => true
        ]);

        $this->crud->addField([
            'name' => 'label_data_vigencia_final', // The db column name
            'type' => 'label',
            'label' => 'Vigência Final', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->vigencia_final,
            'date' => true
        ]);

        $this->crud->addField([
            'name' => 'label_data_valor_total', // The db column name
            'type' => 'label',
            'label' => 'Valor Total', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->valor_total,
            'money' => true
        ]);

        $this->crud->addField([
            'name' => 'label_numero_compra', // The db column name
            'type' => 'label',
            'label' => 'Número da compra/Ano', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->getNumeroCompra()
        ]);

        $this->crud->addField([
            'name' => 'label_modalidade_compra', // The db column name
            'type' => 'label',
            'label' => 'Modalidade da compra', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->getModalidadeCompra()
        ]);
    }


    private function areaLabel(Arp $arp)
    {
        $this->crud->addField([
            'name' => 'label_numero_ano', // The db column name
            'type' => 'label',
            'label' => 'Número/Ano', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->getNumeroAno()
        ]);

        $this->crud->addField([
            'name' => 'label_unidade_gerenciadora', // The db column name
            'type' => 'label',
            'label' => 'Unidade Gerenciadora', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->getUnidadeOrigem()
        ]);

        $this->crud->addField([
            'name' => 'label_numero_processo', // The db column name
            'type' => 'label',
            'label' => 'Número do Processo', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->numero_processo
        ]);

        $this->crud->addField([
            'name' => 'label_data_assinatura', // The db column name
            'type' => 'label',
            'label' => 'Data da Assinatura', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->data_assinatura,
            'date' => true
        ]);

        $this->crud->addField([
            'name' => 'label_data_vigencia_inicial', // The db column name
            'type' => 'label',
            'label' => 'Vigência Inicial', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->vigencia_inicial,
            'date' => true
        ]);

        $this->crud->addField([
            'name' => 'label_data_vigencia_final', // The db column name
            'type' => 'label',
            'label' => 'Vigência Final', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->vigencia_final,
            'date' => true
        ]);

        $this->crud->addField([
            'name' => 'label_data_valor_total', // The db column name
            'type' => 'label',
            'label' => 'Valor Total', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->valor_total,
            'money' => true
        ]);

        $this->crud->addField([
            'name' => 'label_numero_compra', // The db column name
            'type' => 'label',
            'label' => 'Número da compra/Ano', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->getNumeroCompra()
        ]);

        $this->crud->addField([
            'name' => 'label_modalidade_compra', // The db column name
            'type' => 'label',
            'label' => 'Modalidade da compra', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->getModalidadeCompra()
        ]);

        $this->crud->addField([
            'name' => 'label_objeto', // The db column name
            'type' => 'label',
            'label' => 'Objeto', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-8 pt-3'],
            'value' => $arp->objeto
        ]);

        $this->crud->addField([
            'name' => 'label_compra_centralizada', // The db column name
            'type' => 'label',
            'label' => 'Compra Centralizada', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $arp->getCompraCentralizada()
        ]);

        # Busca as autoridades signatarias vinculadas com a ata
        $autoridadesSignatarias = Arp::getAutoridadeSignataria($arp->id);
        # Armazena os dados em formato para exibição
        $autoridadesSignatariasFormatadas = [];
        # Define as colunas da tabela de exibição
        $column = [];
        # Organiza o array para passar ao backpack
        if ($autoridadesSignatarias) {
            $autoridadesParaExibicao = [];
            foreach ($autoridadesSignatarias["Nome"] as $key => $nome) {
                $cargo = $autoridadesSignatarias["Cargo"][$key];
                $autoridadesSignatariasFormatadas[] = [
                    "nome" => $nome,
                    "cargo" => $cargo
                ];
            }
            $column = ['nome' => 'Nome', 'cargo' => 'Cargo'];
        }

        $this->addTableCustom(
            $column,
            'table_selecionar_item_adesao_arp',
            'Itens da compra',
            'col-md-12 pt-5',
            'areaItemAdesao',
            null,
            90,
            $autoridadesSignatariasFormatadas,
            'Autoridade signatária'
        );
    }

    /**
     * Método responsável em exibir os campos para que o usuário possa
     * retificar
     */
    private function areaCampos()
    {
        $this->crud->addField(
            [
                'name' => 'objeto_alteracao', // The db column name
                'label' => 'Objeto', // Table column heading
                'type' => 'textarea',
                'wrapperAttributes' => [
                    'class' => 'col-md-12  pt-3'
                ]
            ]
        );

        $this->crud->addField([
            'name' => 'numero_alteracao',
            'label' => 'Número da ata',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3'
            ]
        ]);
        

        
        $this->crud->addField([
            'name' => 'ano',
            'label' => 'Ano da ata',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3'
            ]
        ]);

        $this->crud->addField([
            'name' => 'data_assinatura_alteracao',
            'type' => 'date',
            'label' => 'Data da Assinatura', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
        ]);

        $this->crud->addField([
            'name' => 'vigencia_inicial_alteracao',
            'type' => 'date',
            'label' => 'Vigência Inicial', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
        ]);

        $this->crud->addField([
            'name' => 'vigencia_final_alteracao',
            'type' => 'date',
            'label' => 'Vigência Final', // Table column heading
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
        ]);

        $this->crud->addField([
            'name' => 'compra_centralizada_alteracao',
            'label' => 'Compra Centralizada',
            'type' => 'radio',
            'options' => [
                false => 'Não',
                true => 'Sim'
            ],
            'inline' => 'd-inline-block',
            'wrapperAttributes' => [
                'class' => 'col-md-4  pt-3'
            ]
        ]);

        $this->addFieldSelectMultiple(
            'Autoridade signatária',
            'autoridadesignataria_ata',
            'autoridade_signataria',
            'nome',
            'arp/autoridadesignataria',
            ['class' => 'col-md-4  pt-3'],
            null,
            'autoridade_signataria_id',
            null,
            ArpAutoridadeSignatariaHistorico::class
        );
    }

    /**
     * Método responsável em montar os campos hidden para enviar no form
     */
    private function areaHidden(Arp $arp)
    {
        $this->crud->addField([
            'name' => 'arp_id', // The db column name
            'type' => 'hidden',
            'value' => $arp->id
        ]);

        $compraCentralizadaHidden = $arp->compra_centralizada;
        if (!$arp->compra_centralizada) {
            $compraCentralizadaHidden = "false";
        }
        $this->crud->addField([
            'name' => 'compra_centralizada', // The db column name
            'type' => 'hidden',
            'value' => $compraCentralizadaHidden
        ]);

        $this->crud->addField([
            'name' => 'objeto', // The db column name
            'type' => 'hidden',
            'value' => $arp->objeto
        ]);

        $idAtaRetificacao = $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', 'Retificar');

        $this->crud->addField([
            'name' => 'tipo_id', // The db column name
            'type' => 'hidden',
            'value' => $idAtaRetificacao
        ]);

        $this->crud->addField([
            'name' => 'data_assinatura', // The db column name
            'type' => 'hidden',
            'value' => $arp->data_assinatura
        ]);

        $this->crud->addField([
            'name' => 'vigencia_inicial', // The db column name
            'type' => 'hidden',
            'value' => $arp->vigencia_inicial
        ]);

        $this->crud->addField([
            'name' => 'vigencia_final', // The db column name
            'type' => 'hidden',
            'value' => $arp->vigencia_final
        ]);

        # Monta a url para validar a ata digitada pelo usuário
        $this->crud->addField([
            'name' => 'url_buscar_arp',
            'type' => 'hidden',
            'default' => url("arp/validarnumeroata"),
            'attributes' => ['id' => 'url_buscar_arp'],
            'fake' => true
        ]);

        $this->crud->addField([
            'name' => 'ano_ata',
            'type' => 'hidden',
            'value' => $arp->ano
        ]);

        $this->crud->addField([
            'name' => 'numero_ata',
            'type' => 'hidden',
            'value' => $arp->numero
        ]);

        $this->crud->addField([
            'name' => 'unidade_origem_id',
            'type' => 'hidden',
            'value' => $arp->unidade_origem_id
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');
        $arp = Arp::find($this->returnIdArp());

        $this->importarDatatableForm();

        $arquivoJS = [
            'assets/js/admin/forms/arp_retificar.js',
            'assets/js/admin/forms/arp_common.js',
            'assets/js/blockui/jquery.blockUI.js'
        ];

        $this->importarScriptJs($arquivoJS);

        # Montar a parte dos label preenchidos para o usuário
        $this->areaLabel($this->arp); // chama a função areaLabel passando $this->arp como argumento
        $this->areaLabelSimplificada($this->arp); // chama a função areaLabel passando $this->arp como argumento

        $this->crud->addField(
            [
                'name' => 'justificativa_motivo_cancelamento', // The db column name
                'label' => 'Justificativa/Motivo da retificação', // Table column heading
                'type' => 'textarea',
                'wrapperAttributes' => [
                    'class' => 'col-md-12  pt-3'
                ],
                'attributes' => [
                    'maxlength'=>'250'
                ],
                'required' => 'required'
            ]
        );

        $this->crud->addField([
            'name' => 'divider_texto', // The db column name
            'type' => 'br-divider',
            'prefix_text' => 'Preencha apenas os campos que deseja retificar',
            'texttooltip_prefix' => 'Ao clicar em Retificar, as informações serão publicadas no PNCP',
            'wrapperAttributes' => ['class' => 'col-md-12 pt-5'],
        ]);

        # Montar a parte dos campos para preenchimento do usuário
        $this->areaCampos();

        # Monta a tabela com os itens da ata
        $this->itensDaAta();

        #Insere os campos escondidos para análise no request
        $this->areaHidden($arp);

        # Monta visualização dos fornecedores do item
        $this->addAreaCustom('table_fornecedor_compra_arp', null, 'areaFornecedor');

        # Cria o elemento modalArp para exibir informações dentro deste componente
        $this->addModalCustom('modalArp', 'modalArp', 'Unidades');
        #Botões customizados
        $this->botoes();

        #BreadCrumb Customizado
        $this->data['breadcrumbs'] = $this->breadCrumb(true);

        # Como o form está na parte inferior e estava rolando o scroll para baixo
        # Criei a variável para exibir o alerta em baixo
        $this->crud->exibir_erro_end = true;
    }

    /**
     * Monta a exibição da tabela de itens da ata
     *
     * @return void
     */
    private function itensDaAta()
    {
        $arpItemRepository = new ArpItemRepository();
        # Recupera os itens para exibir na tela de edição
        $itensCadastrados = $arpItemRepository->getItensSemMovimentacaoPorAta($this->arp->id);

        $valorTotal = 0;
        $quantidadeDeItens = 0;
        $itemFormatado = [];
        foreach ($itensCadastrados as $item) {
            // Monta um ID para cada linha de item para poder manipulá-la em tela
            $idLinha = "linha_{$item->arp_item_id}";

            $textoPermiteCarona = 'Não';
            if ($item->permite_carona == 1) {
                $textoPermiteCarona = 'Sim';
            }

            # Campo Valor Total
            $valorNegociado = number_format($item->valor_negociado, 4, ',', '.');
            # Campo Valor Unitário


            $valorUnitario = number_format($item->valor_unitario, 4, ',', '.');
            # Variável responsável em receber o valor para calcular o total da ata
            $valorNegociadoTotal = $item->valor_negociado;

            # Se existir desconto no item, então exibe o valor com o maior desconto
            if ($item->percentual_maior_desconto > 0) {
                $valorNegociado = number_format($item->valor_negociado_desconto, 4, ',', '.') . '
                <i class="fas fa-info-circle"
                title="Clique para maiores detalhes sobre o desconto"
                onclick="exibirDetalheMaiorDesconto(`' . number_format(
                    $item->percentual_maior_desconto,
                    4,
                    ',',
                    '.'
                ) . '`,
                `' . $item->numero . '`,
                `' . $item->descricaodetalhada . '`,
                `' . number_format($item->valor_unitario, 4, ',', '.') . '`,
                `' . number_format($item->valor_unitario_desconto, 4, ',', '.') . '`,
                `' . number_format($item->valor_negociado, 4, ',', '.') . '`,
                `' . number_format($item->valor_negociado_desconto, 4, ',', '.') . '`)"></i>';

                $valorUnitario = number_format($item->valor_unitario_desconto, 4, ',', '.');

                $valorNegociadoTotal = $item->valor_negociado_desconto;
            }

            $item->descricaodetalhada = Str::limit(
                trim($item->descricaodetalhada),
                50,
                '<i class="fas fa-info-circle" title="' . $item->descricaodetalhada . '"></i>'
            );

            # Coluna Quantidade Registrada
            $item->quantidade_homologada_vencedor = number_format($item->quantidade_homologada_vencedor, 5, ',', '.') .
                ' <i class="fas fa-info-circle"
                title="Clique para maiores detalhes sobre a quantidade registrada"
                onclick="exibirDetalheItem(' . $item->compra_id . ',' . backpack_user()->id . ',
                ' . $item->compra_item_id . ',`' . url('arp/exibirdetalheitem') . '`)"></i>';

            # Coluna classificação do fornecedor
            $item->classificacao = $item->classificacao .
                '<i class="fas fa-info-circle"
                title="Clique para maiores detalhes sobre a classificação"
                onclick="exibirClassificacaoFornecedor(' . $item->fornecedor_id . ',`' .
                url('arp/exibirclassificacaofornecedor') . '`, `' . $item->numero . '`)">
                </i>';

            /*************************************************************/
            /*** Monta a linha html onde são exibidos os dados do item ***/
            /*************************************************************/
            $itemFormatado[] = "<tr id='{$idLinha}'>
                        <td>
                            <a class='br-button circle btn-danger large mr-3 br-button-row-table'
                                href='javascript:void(0)'
                                onclick='removerLinhaItemSelecionado(`{$this->arp->id}`, `{$item->arp_item_id}`)'>
                                    <i class='nav-icon la la-trash'></i>
                            </a>
                        </td>
                        <td>{$item->cpf_cnpj_idgener}</td>
                        <td>{$item->nomefornecedor}<br>({$item->classificacao})</td>
                        <td>{$item->numero}</td>
                        <td>{$item->descricao}</td>
                        <td>{$item->codigo_siasg}</td>
                        <td>{$item->descricaodetalhada}</td>
                        <td>{$item->quantidade_homologada_vencedor}</td>
                        <td>{$valorUnitario}</td>
                        <td>" . $valorNegociado . "</td>
                        <td style='text-align: right;'>{$item->maximo_adesao}</td>
                        <td style='text-align: right;'>{$item->maximo_adesao_informado_compra}</td>
                        <td style='text-align: right;'>
                            <input type='hidden' name='id_item_fornecedor[]'
                            value='{$item->arp_item_id}'/>{$textoPermiteCarona}
                        </td>
                    </tr>";
            $valorTotal += $valorNegociadoTotal;
            $quantidadeDeItens++;
        }

        $this->crud->addField([
            'name' => 'valor_total',
            'type' => 'hidden',
            'value' => $valorTotal
        ]);

        $this->crud->addField([
            'attributes' => ['id' => 'quantidade_de_itens'],
            'name' => 'quantidade_de_itens',
            'type' => 'hidden',
            'value' => $quantidadeDeItens
        ]);

        # Cria um campo hidden para armazenar os códigos dos itens a serem removidos
        $this->crud->addField([
            'attributes' => ['id' => 'itens_remover'],
            'name' => 'itens_remover',
            'type' => 'hidden',
            'value' => ''
        ]);

        if ($itemFormatado) {
            $this->addAreaCustom('table_item_compra_selecionado_arp', null, null, $itemFormatado);
        } else {
            $this->crud->addField([
                'name' => '',
                'type' => 'label',
                'value' => '<label style="padding-top: 40px;">Não há itens com possibilidade de retificação.</label>'
            ]);
        }
    }

    /**
     * Verifica se existem vínculos de empenhos e contratos com o item
     *
     * @param integer $itemId Id do item na tabela arp_item
     * @return string $mensagem Mensagem com os vínculos encontrados em formato json
     */
    public function verificarVinculosItem($arpId, $arpItemId)
    {
        $arpItemRepository = new ArpItemRepository();

        # Busca o compra_item_id
        $itemDeCompra = $arpItemRepository->getItemDeCompraDoItemDaAta($arpItemId);

        # Recupera os empenhos utilizados pelo item selecionado
        $minutasDeEmpenhoVinculadasAoItem =
            $arpItemRepository->getMinutasTipoCompraPorItem($itemDeCompra->compra_item_id);

        $mensagemMinutasEmpenho =
            $this->montarMensagemMinutasEmpenhoVinculadasAoItem($minutasDeEmpenhoVinculadasAoItem);

        # Recupera os itens que estão vinculados somente nos contratos
        $contratosVinculadosAoItem = $arpItemRepository->getContratosPorItemFornecedor(
            $itemDeCompra->compra_item_id,
            $itemDeCompra->fornecedor_id
        );
        $mensagemContratos = $this->montarMensagemContratosVinculadosAoItem($contratosVinculadosAoItem);

        # Verifica se o item possui adesões já aceitas (parcial ou
        $adesoesDoItem = $arpItemRepository->getAdesoesAceitasItem($arpItemId);
        $mensagemAdesoes = $this->montarMensagemAdesoesDoItem($adesoesDoItem);

        # Verifica se o item possui remanejamento
        $remanejamentosDoItem = $arpItemRepository->getRemanejamentosPorItem($arpItemId);
        $mensagemRemanejamentos = $this->montarMensagemRemanejamentosDoItem($remanejamentosDoItem);

        // Monta as mensagens
        $mensagem = $mensagemMinutasEmpenho;

        if ($mensagemContratos) {
            if ($mensagem) {
                $mensagem .= '<br>';
            }
            $mensagem .= $mensagemContratos;
        }

        if ($mensagemAdesoes) {
            if ($mensagem) {
                $mensagem .= '<br>';
            }
            $mensagem .= $mensagemAdesoes;
        }

        if ($mensagemRemanejamentos) {
            if ($mensagem) {
                $mensagem .= '<br>';
            }
            $mensagem .= $mensagemRemanejamentos;
        }

        return json_encode($mensagem);
    }

    /**
     * Método responsável em exibir os botões para o usuário no formulário
     */
    private function botoes()
    {
        $this->crud->button_custom = [
            [
                'button_text' => 'Retificar', 'button_id' => 'adicionar',
                'button_name_action' => 'rascunho',
                'button_value_action' => '',
                'onclick' => 'exibirAlertaEnvioCustomizado(`Salvando as informações e retificando no PNCP`)'
            ],
        ];
    }


    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');

        $this->setupCreateOperation();
    }

    /**
     * Método responsável em transcrever as informações que estão no banco de dados
     * com as informações que foram enviadas pelo form
     */
    private function addColumnRetificacao(array $arpAtual, array $arpRetificacao, array $colunaRetificacao = [])
    {
        if (empty($colunaRetificacao)) {
            # Array de colunas que serão alteradas
            $colunaRetificacao = [
                'objeto_alteracao', 'vigencia_inicial_alteracao',
                'vigencia_final_alteracao', 'data_assinatura_alteracao',
                'compra_centralizada_alteracao', 'rascunho', 'arp_id',
                'autoridadesignataria_ata', 'tipo_id',
                'justificativa_motivo_cancelamento',
                'numero_alteracao'
            ];
        }

        # Percorre as informações combinando com os valores do banco e form
        foreach ($colunaRetificacao as $coluna) {
            # Se não existir o valor no array, retorna nulo e então não será alterado
            $arpRetificacao[$coluna] = $arpRetificacao[$coluna] ?? null;
            $arpAtual[$coluna] = $arpRetificacao[$coluna];
        }
        
        return $arpAtual;
    }

    /**
     * Método responsável em alterar na tabela de autoridade signatária
     */
    private function atualizarTabelaPrincipalAutoridadeSignataria(array $dados)
    {
        # Se a ação do usuário for salvar sem ser rascunho, salva as alterações
        if (!$dados['rascunho']) {
            # Atualiza todas as autoridade signatárias para cancelados que são
            # diferentes do id do histórico atual
            ArpAutoridadeSignatariaHistorico::where("arp_id", $dados['arp_id'])
                ->where("arp_historico_id", "<>", $dados['arp_historico_id'])
                ->update(['autoridade_cancelada' => true]);

            # Deleta do banco de dados as autoridades signatárias vigentes
            ArpAutoridadeSignataria::where("arp_id", $dados['arp_id'])->delete();

            # Recupera as autoridades que não foram canceladas, para poder
            # inserir como as válidas
            $autoridadeValidas = ArpAutoridadeSignatariaHistorico::where("arp_id", $dados['arp_id'])
                ->where("arp_historico_id", "=", $dados['arp_historico_id'])
                ->where("autoridade_cancelada", false)
                ->get();

            # Insere o id da ata para salvar as novas autoridades signatárias
            $autoridadeSignatariaValida = ['arp_id' => $dados['arp_id']];

            # Percorre as informações recuperadas das autoridades ativas no
            # histórico para inserir como válidas
            foreach ($autoridadeValidas as $autoridadeValida) {
                $autoridadeSignatariaValida['autoridade_signataria_id'] = $autoridadeValida->autoridade_signataria_id;
                ArpAutoridadeSignataria::create($autoridadeSignatariaValida);
            }
        }
    }

    /**
     * Método responsável em inserir a autoridade signatária
     */
    private function inserirAutoridadeSignataria(array $dados)
    {
        # Inicia o array para a inserção
        $autoridadeSignataria = ['arp_historico_id' => $dados['arp_historico_id'], 'arp_id' => $dados['arp_id']];

        # Percorre para poder inserir as autoridades no histórico
        foreach ($dados['autoridadesignataria_ata'] as $autoridade) {
            $autoridadeSignataria['autoridade_signataria_id'] = $autoridade;
            $autoridadeSignataria['vigencia_inicial'] = $dados['vigencia_inicial_alteracao'];
            $autoridadeSignataria['vigencia_final'] = $dados['vigencia_final_alteracao'];
            ArpAutoridadeSignatariaHistorico::create($autoridadeSignataria);
        }

        # Atualiza as informações da autoridade signatária na tabela principal
        $this->atualizarTabelaPrincipalAutoridadeSignataria($dados);
    }

    /**
     * Método responsável em modificar as informações na tabela principal da ARP
     */
    private function modificarArpPrincipal(array $arpUnificada)
    {
        # Recupera as informações iniciais da ata
        $arpPrincipal = Arp::find($arpUnificada['arp_id']);

        # Se for retificado o número
        if ($arpPrincipal['numero'] != $arpUnificada['numero_alteracao']
            && !empty($arpUnificada['numero_alteracao'])
        ) {
            $arpPrincipal['numero'] = $arpUnificada['numero_alteracao'];
        }

        # Se for retificado a data de assinatura
        if ($arpPrincipal['data_assinatura'] != $arpUnificada['data_assinatura_alteracao']
            && !empty($arpUnificada['data_assinatura_alteracao'])
        ) {
            $arpPrincipal['data_assinatura'] = $arpUnificada['data_assinatura_alteracao'];
        }

        # Se for retificado a data inicial de vigência
        if ($arpPrincipal['vigencia_inicial'] != $arpUnificada['vigencia_inicial_alteracao']
            && !empty($arpUnificada['vigencia_inicial_alteracao'])
        ) {
            $arpPrincipal['vigencia_inicial'] = $arpUnificada['vigencia_inicial_alteracao'];
        }

        # Se for retificado a data final de vigência
        if ($arpPrincipal['vigencia_final'] != $arpUnificada['vigencia_final_alteracao']
            && !empty($arpUnificada['vigencia_final_alteracao'])
        ) {
            $arpPrincipal['vigencia_final'] = $arpUnificada['vigencia_final_alteracao'];
        }

        # Se for retificado o objeto
        if ($arpPrincipal['objeto'] != $arpUnificada['objeto_alteracao']
            && !empty($arpUnificada['objeto_alteracao'])
        ) {
            $arpPrincipal['objeto'] = $arpUnificada['objeto_alteracao'];
        }

        # Se for retificado a compra centralizada
        if ($arpPrincipal['compra_centralizada'] != $arpUnificada['compra_centralizada_alteracao']
            && $arpUnificada['compra_centralizada_alteracao'] != null
        ) {
            $arpPrincipal['compra_centralizada'] = $arpUnificada['compra_centralizada_alteracao'];
        }
        
        # Se for retificado a compra centralizada
        if ($arpPrincipal['ano'] != $arpUnificada['ano']
            && $arpUnificada['ano'] != null
        ) {
            $arpPrincipal['ano'] = $arpUnificada['ano'];
        }

        $arpPrincipal->save();

        # Se for retificado as datas de vigência da ata, entra no método
        # para poder alterar as datas do item
        if ($arpPrincipal->wasChanged(['vigencia_inicial', 'vigencia_final'])) {
            # Recupera todos os itens cadastrados para Ata
            $itemArp = ArpItem::where("arp_id", $arpPrincipal->id)->get();

            $itensCadastrados = array();
            foreach ($itemArp as $item) {
                # Recuperar as informações se o item foi cancelado
                $arrayItemCancelado = $item->itemHistorico->pluck('item_cancelado')->toArray();
                
                # Não pode atualizar a vigência para o item que sofreu cancelamento
                if (in_array(true, $arrayItemCancelado)) {
                    continue;
                }
                
                # Altera as datas de vigência do item

                $item->item_fornecedor_compra->ata_vigencia_inicio = $arpPrincipal->vigencia_inicial;
                $item->item_fornecedor_compra->ata_vigencia_fim = $arpPrincipal->vigencia_final;

                $item->item_fornecedor_compra->save();

                # Insere o id do item da tabela arp para inserir no histórico
                $itensCadastrados[] = $item->id;
            }

            $this->inserirItemArpHistorico(
                $itensCadastrados,
                $arpUnificada['arp_historico_id'],
                $arpPrincipal['vigencia_inicial'],
                $arpPrincipal['vigencia_final']
            );
        }
    }

    public function store(ArpRetificarRequest $request)
    {
        $dados = $request->all();

        $temDadosParaRetificar = false;
        $temItensParaRemover = false;

        # Se nenhum campo estiver marcado, retorna para a tela de cadastro
        if (empty($dados['objeto_alteracao']) &&
            empty($dados['data_assinatura_alteracao']) &&
            empty($dados['vigencia_inicial_alteracao']) &&
            empty($dados['vigencia_final_alteracao']) &&
            $dados['compra_centralizada_alteracao'] == null &&
            empty($dados['numero_alteracao']) &&
            (!is_array($dados['autoridadesignataria_ata']) || count($dados['autoridadesignataria_ata']) == 0) &&
            empty($dados['itens_remover']) &&
            empty($dados['ano'])
        ) {
            # Faz a verificação de itens separadamente para saber se a retificação é somente itens
            # ou se retifica alguma outra informação também.
            if (empty($dados['itens_remover'])) {
                \Alert::add('error', 'Preencha ao menos um item para retificar!')->flash();
                return redirect()->back();
            }
        } else {
            $temDadosParaRetificar = true;
        }
        # Armazena os IDs dos itens da ata a serem removidos em um array
        $idItensRemover = [];
        

        if ($dados['itens_remover']) {
            $idItensRemover = explode(',', $dados['itens_remover']);
            $temItensParaRemover = true;
        }

        # Verfica se está excluindo todos os itens. Neste caso, impede.
        if (count($idItensRemover) == ArpItem::getItemPorAta($dados['arp_id'])->count()) {
            \Alert::add('error', 'Não é possível remover todos os itens da ata!')->flash();
            return redirect()->back();
        }

        $arpAtual = Arp::find($dados['arp_id'])->toArray();
        $arpUnificada = $this->addColumnRetificacao($arpAtual, $dados);



        $arpUnificada['responsavel_acao_id'] = auth()->id();

        $codigoItemRepository = new CodigoItemRepository();

        # DEFINE O TIPO DE MOVIMENTO
        # Se tiver dados a retificar e itens a remover
        if ($temDadosParaRetificar && $temItensParaRemover) {
            $arpUnificada['tipo_id'] = $codigoItemRepository->getCodigoItemId(
                'Retificar - Dados da ata e exclusão de item',
                'statushistorico'
            );
        } elseif ($temItensParaRemover) { # Se a retificação é somente para excluir item
            $arpUnificada['tipo_id'] = $codigoItemRepository->getCodigoItemId(
                'Retificar - Excluir item',
                'statushistorico'
            );
        } # Se não for nenhuma das opções acima, será somente Retificar.
        # Este tipo já está na $arpUnificada['tipo_id'] vindo do form

        DB::beginTransaction();

        try {
            # Insere o histórico da ata

            $arpUnificada['arp_historico_id'] = ArpHistorico::create($arpUnificada)->id;
            # Se existir autoridade signatária selecionada, realiza o insert
            if (!empty($arpUnificada['autoridadesignataria_ata'])
                && count($arpUnificada['autoridadesignataria_ata']) > 0
            ) {
                $this->inserirAutoridadeSignataria($arpUnificada);
            }

            # Se houver itens marcados para remoção, executa o procedimento de exclusão
            if (count($idItensRemover)) {
                # Instancia o repository do item para utilizar as regras de acesso ao banco
                $arpItemRepository = new ArpItemRepository();

                $valorTotalDosItensRemovidos = 0;

                # Percorre cada item para fazer o tratamento de exclusão
                foreach ($idItensRemover as $arpItemId) {
                    // Verifica se os itens não tem vínculo
                    // O método retorna um json vazio (duas aspas) caso não tenha vínculo
                    if ($this->verificarVinculosItem($dados['arp_id'], $arpItemId) != '""') {
                        throw new Exception();
                    }

                    # Busca o valor negociado do item, considerando o desconto
                    $itemDeCompra = $arpItemRepository->getItemDeCompraDoItemDaAta($arpItemId);
                    $valorTotalDosItensRemovidos += $itemDeCompra->valor_negociado_desconto;

                    # Remove a vigência na tabela compra_item_fornecedor
                    $arpItemRepository->atualizarVigenciaItem($arpItemId);

                    # Prepara os dados do item para inserir no histórico
                    $itemHistorico = [
                        'vigencia_inicial' => null,
                        'vigencia_final' => null,
                        'arp_historico_id' => $arpUnificada['arp_historico_id'],
                        'item_removido_retificacao' => true,
                        'arp_item_id' => $arpItemId
                    ];

                    # Insere o item no histórico
                    ArpItemHistorico::create($itemHistorico);

                    # Exclui o item
                    $itemExcluir = ArpItem::find($arpItemId);
                    $itemExcluir->delete();
                }

                # Busca o valor total da ata para recalcular
                $valorTotalAta = Arp::find($dados['arp_id'])->valor_total;

                # Subtrai, do valor total da ata, o valor total dos itens removidos
                $valorTotalAta -= $valorTotalDosItensRemovidos;

                # Atualiza o valor total da ata
                Arp::find($dados['arp_id'])->update(['valor_total' => $valorTotalAta]);
            }



            # Se for finalizar para publicar no PNCP a retificação, entra no método

            if (!$arpUnificada['rascunho']) {
                $dados['arp_historico_id'] = $arpUnificada['arp_historico_id'];
                $arpUnificadaArpPrincipal = $this->addColumnRetificacaoArpPrincipal($arpAtual, $dados);
                $this->modificarArpPrincipal($arpUnificadaArpPrincipal);
            }

            \Alert::add('success', 'Cadastro Realizado com sucesso!')->flash();

            DB::commit();

            return redirect('/arp/retificar/' . $arpUnificada['arp_id']);
        } catch (Exception $ex) {
            DB::rollBack();
            #Log::error($ex);
            $this->inserirLogCustomizado('gestao_ata', 'error', $ex);
            \Alert::add('error', 'Erro ao realizar a retificação!')->flash();
            return redirect()->back();
        }
    }

    public function update(ArpRetificarRequest $request)
    {
        $dados = $request->all();
    }

    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');
        return $this->destroyCrud($id);
    }
    
    private function addColumnRetificacaoArpPrincipal(array $arpAtual, array $arpRetificacao)
    {
        # Array de colunas que serão alteradas
        $colunaRetificacao = [
            'objeto_alteracao', 'vigencia_inicial_alteracao',
            'vigencia_final_alteracao', 'data_assinatura_alteracao',
            'compra_centralizada_alteracao', 'rascunho', 'arp_id',
            'autoridadesignataria_ata', 'tipo_id',
            'justificativa_motivo_cancelamento',
            'numero_alteracao',
            'ano',
            'arp_historico_id'
        ];
        
        # Percorre as informações combinando com os valores do banco e form
        foreach ($colunaRetificacao as $coluna) {
            # Se não existir o valor no array, retorna nulo e então não será alterado
            $arpRetificacao[$coluna] = $arpRetificacao[$coluna] ?? null;
            $arpAtual[$coluna] = $arpRetificacao[$coluna];
        }
        
        return $arpAtual;
    }
}
