<?php

namespace App\Http\Controllers\Arp;

use App\Http\Controllers\Operations\DeleteOperation;
use App\Http\Requests\ArpItemRequest as StoreRequest;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\CommonFields;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Models\Arp;
use App\Models\ArpItem;
use App\Models\ArpUnidades;
use App\Models\CompraItemFornecedor;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/**
 * Class ArpItemCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArpItemCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    use CommonFields;
    use BuscaCodigoItens;
    use ImportContent;

    private function returnIdArp()
    {
        return Route::current()->parameter("arp_id");
    }

    private function breadCrumb()
    {
        return [
                    trans('backpack::crud.admin') => backpack_url('dashboard'),
                    'Ata de Registro de Preço' =>  backpack_url('arp'),
                    'Item da ata' =>   false,
                    // 'Voltar' => backpack_url('arp/item/'.$this->returnIdArp())
                ];
    }

    private function queryItem(bool $show = false, int $id)
    {
        CRUD::setModel(\App\Models\ArpItem::class);
        CRUD::addClause('join', "compra_item_fornecedor", "arp_item.compra_item_fornecedor_id", "=", "compra_item_fornecedor.id");
        CRUD::addClause('join', "fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id");
        CRUD::addClause('join', "compra_items", "compra_items.id", "=", "compra_item_fornecedor.compra_item_id");

        if ($show) {
            CRUD::addClause('where', 'arp_item.id', '=', $id);
        } else {
            CRUD::addClause('where', 'arp_item.arp_id', '=', $id);
        }

        CRUD::addClause('select', 'compra_items.numero', 'compra_items.descricaodetalhada', 'compra_items.maximo_adesao', DB::raw("CASE
                            WHEN compra_items.permite_carona = true THEN 'Sim'
                            WHEN compra_items.permite_carona = false THEN 'Não'
                            END AS aceita_adesao"), 'arp_item.id');
        CRUD::addClause('distinct', 'compra_items.numero');
        CRUD::addClause('orderby', 'compra_items.numero');
    }
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $idArp = $this->crud->getCurrentEntryId();
        CRUD::setRoute(config('backpack.base.route_prefix') .'/arp/item/'.$idArp);

        $this->queryItem(false, $idArp);
        $this->exibirTituloPaginaMenu('Item da Ata');
        $this->bloquearBotaoPadrao($this->crud, ['update']);
        
        $this->data['breadcrumbs'] = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Ata de Registro de Preço' =>  backpack_url('arp'),
            'Item da ata' =>  false,
        ];

        return view('backpack::dashboard', $this->data);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('numero');
        $this->addColumnModelFunction('descricaodetalhada', 'Descricao', 'getDescricaoDetalhada');
        CRUD::column('maximo_adesao');
        CRUD::column('aceita_adesao');

        $this->data['breadcrumbs'] =[
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Ata de Registro de Preço' =>  backpack_url('arp'),
            'Item da ata' =>   false,
            // 'Voltar' => backpack_url('arp')
        ];
        
        $this->crud->text_button_redirect_create = 'Item';
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    protected function setupShowOperation()
    {
        $this->data['breadcrumbs'] = $this->breadCrumb();

        $idItem = Route::current()->parameter("id");
        $this->queryItem(true, $idItem);

        CRUD::column('numero');
        CRUD::addColumn([
            'name' => 'descricaodetalhada', // the db column name (attribute name)
            'label' => "Descrição", // the human-readable label for it
            'type' => 'text',
            'limit' => 999999999999999999999999999999999999
         ]);
        CRUD::column('maximo_adesao');
        CRUD::column('aceita_adesao');

        
        return view('backpack::dashboard', $this->data);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->data['breadcrumbs'] = $this->breadCrumb();
        
        $this->crud->setCreateContentClass('col-md-12');

        $this->importarDatatableForm();

        $arquivoJS = ['assets/js/admin/forms/arp.js' , 'assets/js/blockui/jquery.blockUI.js', 'packages/backpack/base/js/padrao_gov/jquery.mask.js'];

        $this->importarScriptJs($arquivoJS);


        $unidadeParticipante = ArpItem::getUnidadeParticipante($this->returnIdArp());
        $arp = Arp::find($this->returnIdArp());
        $this->crud->addField([
            'name' => 'url_buscar_compra',
            'type'  => 'hidden',
            'default' => url("arp/validarcompra"),
            'attributes' => [ 'id'=>'url_buscar_compra' ],
            'fake' => true
        ]);
        
        $this->crud->addField([
            'name' => 'unidade_origem_id_fake',
            'label' => 'Unidade gerenciadora',
            'type'  => 'text',
            'attributes' => [ 'readonly'=>'readonly' ],
            'wrapperAttributes' => [ 'class' => 'col-md-4 pt-3' ],
            'fake'     => true,
            'default' => $arp->unidades->codigo."-".$arp->unidades->nomeresumido
        ]);

        $this->crud->addField([
            'name' => 'unidade_origem_id',
            'type'  => 'hidden',
            'default' => $arp->unidades->id,
            'attributes' => [ 'id'=>'unidade_origem_id' ]
        ]);

        $this->crud->addField([
            'name' => 'id_arp',
            'type'  => 'hidden',
            'default' => $this->returnIdArp(),
            'attributes' => [ 'id'=>'id_arp' ]
        ]);

        $campos =
        [
            'name'  => 'numero_ano',
            'label' => 'Número da compra',
            'type'  => 'text',
            'default' => $arp->compras->numero_ano,
            'attributes' => [
                'readonly'=> 'readonly'
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3 br-input'
            ],
            'fake'     => true,
        ];
        $this->crud->addField($campos);

        $modalidades =  $this->retornaArrayCodigosItens('Modalidade Licitação', true);

        $this->crud->addField([
            'name' => 'modalidade_id',
            'label' => "Modalidade licitação",
            'type' => 'select2_from_array',
            'default' => $arp->compras->modalidade->id,
            'options' => $modalidades,
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3',
            ],
        ]);

        $this->addAreaCustom('btn_recuperar_compra_arp', '', 'btn_recuperar_compra_arp');

        $this->addAreaCustom('table_fornecedor_compra_arp');
        $this->addAreaCustom('table_selecionar_item_compra_arp');
        $this->addAreaCustom('table_percentual_maior_desconto');
        $this->addModalCustom('modalArp', 'modalArp', 'Divisão da quantidade por unidade');


        // $this->crud->addField([
        //     'name' => 'valor_total',
        //     'label' => 'Valor total dos itens selecionados',
        //     'type'  => 'text',
        //     'attributes' => [ 'readonly'=>'readonly' ],
        //     'wrapperAttributes' => [
        //         'class' => 'col-md-4 areaItemSelecionado pt-3 br-input'
        //     ]
        // ]);

        $this->crud->addField([
            'name' => 'valor_total',
            'type'  => 'hidden',
        ]);

        $this->crud->addField([
            'name' => 'valor_total_fake',
            'label' => 'Valor total dos itens selecionados',
            'type'  => 'text',
            'attributes' => [ 'readonly'=>'readonly' ],
            'wrapperAttributes' => [
                'class' => 'col-md-4 areaItemSelecionado pt-3 br-input'
            ],
            'fake' => true
        ]);

        $this->addAreaCustom('table_item_compra_selecionado_arp');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Salvar Item',
        ]);

        return view('backpack::dashboard', $this->data);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Atualizar ArpItem',
        ]);
        $this->setupCreateOperation();
    }

    public function destroy($idAta, $idItem)
    {
        $itemAta = ArpItem::whereRaw("arp_item.id IN (SELECT arp_item.id
                                                        FROM arp_item 
                                                        INNER JOIN compra_item_fornecedor ON arp_item.compra_item_fornecedor_id = compra_item_fornecedor.id 
                                                        INNER JOIN compra_items ON compra_items.id = compra_item_fornecedor.compra_item_id 
                                                        WHERE  
                                                        arp_item.arp_id = {$idAta} AND
                                                        compra_items.numero IN (		
                                                                                    SELECT compra_items.numero
                                                                                            FROM arp_item 
                                                                                            INNER JOIN compra_item_fornecedor ON arp_item.compra_item_fornecedor_id = compra_item_fornecedor.id 
                                                                                            INNER JOIN compra_items ON compra_items.id = compra_item_fornecedor.compra_item_id 
                                                                                            WHERE arp_item.id = {$idItem} 
                                                                                    ) 
                                                    )") ->delete();
        return $this->crud->hasAccessOrFail('delete');
    }
    
    private function recuperarDadosItemSelecionado(string $item)
    {
        $item = str_replace("linha_", "", $item);
        return CompraItemFornecedor::find($item);
    }
    
    private function inserirItem(array $dados)
    {
        return ArpItem::create($dados)->id;
    }

    private function inserirUnidades(array $dados)
    {
        return ArpUnidades::create($dados);
    }

    public function store(StoreRequest $request)
    {
        $dados = $request->all();

        if (count($dados['id_item_fornecedor']) > 0) {
            foreach ($dados['id_item_fornecedor'] as $items) {
                $arrayItem = explode(',', $items);
                foreach ($arrayItem as $item) {
                    $compraItemFornecedor = $this->recuperarDadosItemSelecionado($item);
                    $arpItem = ['arp_id' => $dados['id_arp'], 'compra_item_fornecedor_id' => $compraItemFornecedor->id];
                    $idItem = $this->inserirItem($arpItem);
                    $compraItemUnidade = $compraItemFornecedor->compraItens->compraItemUnidade[0];

                    $arpUnidade = ['arp_item_id' => $idItem, 'unidade_id' => $compraItemUnidade->unidade_id, 'tipo_unidade' => $compraItemUnidade->tipo_uasg,
                                'quantidade_autorizada' => $compraItemUnidade->quantidade_autorizada, 'quantidade_saldo' => $compraItemUnidade->quantidade_saldo,
                                'valor_autorizado' => $compraItemFornecedor->valor_negociado];
                    $this->inserirUnidades($arpUnidade);
                }
            }
        }

        $arp = Arp::find($dados['id_arp']);
        $arp->valor_total = ArpItem::getValorTotalAta($dados['id_arp']);
        $arp->save();

        \Alert::add('success', 'Item Cadastro Realizado com sucesso!')->flash();
        return redirect()->back();
    }
}
