<?php

namespace App\Http\Controllers\Arp;

use App\Http\Requests\AdesaoItemRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Models\AdesaoItem;
use App\Models\ArpItem;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Illuminate\Support\Facades\Route;

/**
 * Class AdesaoItemCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AdesaoItemCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    
    private function returnIdArp()
    {
        return Route::current()->parameter("id_adesao");
    }

    private function breadCrumb()
    {
        return [
                    trans('backpack::crud.admin') => backpack_url('dashboard'),
                    'Adesão' =>  backpack_url('arp/adesao'),
                    'Item para adesão' =>   false,
                    // 'Voltar' => backpack_url('arp/adesao/item/'.$this->returnIdArp())
                ];
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\AdesaoItem::class);
        $idArp = $this->crud->getCurrentEntryId();
        CRUD::setRoute(config('backpack.base.route_prefix') . '/arp/adesao/item/'.$idArp);
        $this->exibirTituloPaginaMenu('Itens para adesão');
        $this->bloquearBotaoPadrao($this->crud, ['show']);

        $this->data['breadcrumbs'] = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Adesão' =>  backpack_url('arp/adesao'),
            'Item para adesão' =>  false,
        ];

        return view('backpack::dashboard', $this->data);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->addColumnModelFunction('numero', 'Número', 'getNumero');
        $this->addColumnModelFunction('descricao', 'Descrição', 'getDescricao', ['table' => true, 'modal' => true, 'show' => true, 'export' => true], 50);
        $this->addColumnQuantidade('quantidade_solicitada', 'Quantidade Solicitada', ['table' => true, 'modal' => true, 'show' => true, 'export' => true], 5);
        // $this->crud->column('quantidade_solicitada');
        $this->addColumnModelFunction('status', 'Status', 'getStatus');
        
        
        $this->crud->text_button_redirect_create = 'Item';
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');
        CRUD::setValidation(AdesaoItemRequest::class);
        $this->data['breadcrumbs'] = $this->breadCrumb();

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Criar AdesaoItem',
        ]);

        return view('backpack::dashboard', $this->data);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');
        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Atualizar AdesaoItem',
        ]);
        $this->setupCreateOperation();
    }
}
