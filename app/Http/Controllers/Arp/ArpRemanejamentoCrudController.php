<?php

namespace App\Http\Controllers\Arp;

use App\Http\Requests\ArpRemanejamentoRequest;
use App\Http\Traits\Arp\ArpRemanejamentoTrait;
use App\Http\Traits\Arp\ArpTrait;
use App\Http\Traits\ArquivoGenericoTrait;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\CommonFields;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Http\Traits\LogTrait;
use App\Models\ArpRemanejamento;
use App\Models\CompraItem;
use App\Models\CompraItemUnidade;
use App\Models\Unidade;
use App\Repositories\Arp\ArpRemanejamentoItemRepository;
use App\Repositories\Arp\ArpRemanejamentoRepository;
use App\Repositories\Arp\ArpRepositoy;
use App\Repositories\Compra\CompraItemUnidadeRepository;
use App\Services\Arp\ArpRemanejamentoService;
use App\Services\Arp\ArpService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

/**
 * Class ArpRemanejamentoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArpRemanejamentoCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    use CommonFields;
    use ImportContent;
    use ArpTrait;
    use ArquivoGenericoTrait;
    use LogTrait;
    use ArpRemanejamentoTrait;
    use CompraTrait;
    
    protected $arpService;
    
    public function __construct(ArpService $arpService)
    {
        parent::__construct();
        $this->arpService = $arpService;
    }
    
    /**
     * Método responsável em exibir os campos em comum para o list e show
     */
    private function campoComumListShow()
    {
        $this->addColumnModelFunction('numero_solicitacao', 'Nº da solicitação', 'getNumeroSolicitacao');
        $this->addColumnModelFunction('numero_ata', 'Nº da ata', 'getNumeroAta');
        $this->addColumnModelFunction('numero_compra', 'Nº da compra', 'getNumeroCompra');
        $this->addColumnModelFunction('modalidade_compra', 'Modalidade da compra', 'getModalidadeCompra');
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        if (empty(backpack_user()) || !backpack_user()->can('gestaodeatas_V2_acessar')) {
            return abort(403);
        }

        CRUD::setModel(\App\Models\ArpRemanejamento::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/arp/remanejamento');

        # Realizar o fitro pela unidade logado do usuário
        CRUD::addClause('where', 'unidade_solicitante_id', '=', session('user_ug_id'));
        CRUD::addClause('join', 'arp', 'arp.id', '=', 'arp_remanejamento.arp_id');
        CRUD::addClause('join', 'compras', 'arp.compra_id', '=', 'compras.id');
        CRUD::addClause('select', 'arp.*', 'compras.*', 'arp_remanejamento.*');

        $this->exibirTituloPaginaMenu('Remanejamentos de quantitativo', null);
        CRUD::setSubheading('Listar', 'index');

        # Exibir o botão para Cancelar o remanejamento
        $this->crud->addButtonFromModelFunction('line', 'getViewButtonCancelar', 'getViewButtonCancelar', 'end');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->campoComumListShow();
        
        $this->addColumnModelFunction('situacao', 'Situação', 'getSituacao');
        $this->addColumnCreatedAt(true, true, true, true);
        
        $this->crud->text_button_redirect_create = 'solicitação de remanejamento';

        $arquivoJS = ['assets/js/admin/forms/arp_remanejamento.js'];

        $this->importarScriptJs($arquivoJS);

        $this->filtrosDiversos();

        # Remover o search do datatable
        $this->crud->setOperationSetting('searchableTable', false);
    }

    private function filtrosDiversos()
    {
        CRUD::filter('sequencial')
            ->label('Nº da solicitação')
            ->type('text')
            ->whenActive(function ($value) {
                $this->crud->query
                    ->whereRaw(
                        "concat(
                            lpad(cast(arp_remanejamento.sequencial as varchar),5,'0'),
                            '/',
                            extract(YEAR FROM arp_remanejamento.created_at)
                        ) ILIKE  '%$value%'"
                    );
            })->apply();

        CRUD::filter('arp_id')
            ->label('Nº da ata')
            ->type('text')
            ->whenActive(function ($value) {
                $this->crud->query->whereRaw("concat(arp.numero,'/',arp.ano) ILIKE  '%$value%'");
            })->apply();

        CRUD::filter('compra_id')
            ->label('Nº da compra')
            ->type('text')
            ->whenActive(function ($value) {
                $this->crud->query->whereRaw("compras.numero_ano ILIKE  '%$value%'");
            })->apply();
    }

    /**
     * Método responsável em exibir o campo Número/Ano da Compra para o usuário
     * Na tela de solicitação, exibe o select2
     * Na tela de rascunho, exibe a label e o campo hidden para buscar os itens
     */
    private function exibirCampoNumeroAnoCompra(string $nomeTab, ?array $filtroRecuperado)
    {
        # Se o filtro for vazio, então o usuário está na tela de solicitação
        if (empty($filtroRecuperado)) {
            $this->addFieldSelect(
                'Número/Ano da Compra',
                'numero_compra',
                'compra',
                'numero_ano',
                'arp/remanejamento/numerocompraata',
                ['class' => 'col-md-4 pt-3'],
                $nomeTab,
                true,
                false,
                $filtroRecuperado['numero_compra'],
                null,
                null,
                ['disabled' => 'disabled', 'id' => 'numero_compra']
            );

            return;
        }

        # Recuperar a unidade gerenciadora da ata
        $unidadeGerenciadora = $this->crud->getEntry($this->crud->getCurrentEntryId());

        $this->crud->addField(
            [
                'name' => 'label_numero_ano_compra', // The db column name
                'type' => 'label',
                'label' => 'Número/Ano da Compra', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value' => $unidadeGerenciadora->arp->compras->numero_ano,
                'tab' => $nomeTab
            ]
        );
    }

    /**
     * Método responsável em exibir o campo Modalidade da Compra para o usuário
     * Na tela de solicitação, exibe o select2
     * Na tela de rascunho, exibe a label e o campo hidden para buscar os itens
     */
    public function exibirCampoModalidadeCompra(string $nomeTab, ?array $filtroRecuperado)
    {
        # Se o filtro for vazio, então o usuário está na tela de solicitação
        if (empty($filtroRecuperado)) {
            $this->addFieldSelect(
                'Modalidade da Compra',
                'modalidade_compra',
                'modalidade',
                'modalidade_compra',
                'arp/remanejamento/modalidadecompraata',
                ['class' => 'col-md-4 pt-3'],
                $nomeTab,
                true,
                false,
                $filtroRecuperado['modalidade_compra'],
                null,
                null,
                ['disabled' => 'disabled', 'id' => 'modalidade_compra']
            );

            return;
        }

        # Recuperar unidade gerenciadora da ata
        $unidadeGerenciadora = $this->crud->getEntry($this->crud->getCurrentEntryId());

        $this->crud->addField(
            [
                'name' => 'label_modalidade_compra', // The db column name
                'type' => 'label',
                'label' => 'Modalidade da Compra', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value' => $unidadeGerenciadora->arp->compras->getModalidade(),
                'tab' => $nomeTab
            ]
        );
    }

    /**
     * Método responsável em exibir o campo Unidade da Compra para o usuário
     * Na tela de solicitação, exibe o select2
     * Na tela de rascunho, exibe a label e o campo hidden para buscar os itens
     */
    private function exibirCampoUnidadeCompra(string $nomeTab, ?array $filtroRecuperado)
    {
        # Se o filtro for vazio, então o usuário está na tela de solicitação
        if (empty($filtroRecuperado)) {
            $this->addFieldSelect(
                'Unidade da Compra',
                'unidade_compra',
                'unidadeDestino',
                'unidade_codigo_nomeresumido',
                'arp/remanejamento/unidadecompraata',
                ['class' => 'col-md-4 pt-3'],
                $nomeTab,
                true,
                false,
                $filtroRecuperado['unidade_compra'],
                null,
                null,
                ['id' => 'unidade_compra']
            );
            return;
        }

        # Recuperar a unidade gerenciadora da ata
        $unidadeGerenciadora = $this->crud->getEntry($this->crud->getCurrentEntryId());

        $this->crud->addField(
            [
                'name' => 'label_unidade_compra', // The db column name
                'type' => 'label',
                'label' => 'Unidade da Compra', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value' => $unidadeGerenciadora->arp->compras->getUnidadeOrigem(),
                'tab' => $nomeTab
            ]
        );
    }

    /**
     * Método responsável em exibir os campos de filtro da compra
     */
    private function areaFiltroCompra(string $nomeTab, ?array $filtroRecuperado = null)
    {
        # Exibir o campo Unidade da Compra
        $this->exibirCampoUnidadeCompra($nomeTab, $filtroRecuperado);
        
        # Exibir o campo Número/Ano da Compra
        $this->exibirCampoNumeroAnoCompra($nomeTab, $filtroRecuperado);
        
        # Exibir o campo Modalidade da Compra
        $this->exibirCampoModalidadeCompra($nomeTab, $filtroRecuperado);
    }

    /**
     * Método responsável em exibir o campo Número da ata (Unidade gerenciadora) para o usuário
     * Na tela de solicitação, exibe o select2
     * Na tela de rascunho, exibe a label e o campo hidden para buscar os itens
     */
    private function exibirCampoNumeroAta(string $nomeTab, ?array $filtroRecuperado)
    {
        # Se o filtro for vazio, então o usuário está na tela de solicitação
        if (empty($filtroRecuperado)) {
            $this->addFieldSelect(
                'Número da ata (Unidade gerenciadora)',
                'numero_ata',
                'arp',
                'numero_ano_ata_remanejamento',
                'arp/remanejamento/numeroata',
                ['class' => 'col-md-4 pt-3'],
                $nomeTab,
                true,
                false,
                $filtroRecuperado['numero_ata']
            );
            return;
        }

        # Recuperar a unidade gerenciadora da ata
        $unidadeGerenciadora = $this->crud->getEntry($this->crud->getCurrentEntryId());

        # Recuperar o numero e ano da ata
        $numeroAnoAta = $unidadeGerenciadora->arp->getNumeroAno();

        # Recuperar a Unidade gerenciadora da ata
        $unidadeGerenciadoraAta = $unidadeGerenciadora->arp->getUnidadeOrigem();

        $this->crud->addField(
            [
                'name' => 'numero_ata', // The db column name
                'type' => 'label',
                'label' => 'Número da ata (Unidade gerenciadora)', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value' => "{$numeroAnoAta} ({$unidadeGerenciadoraAta})",
                'tab' => $nomeTab
            ]
        );
    }

    /**
     * Método responsável em exibir os demais campos do filtro
     */
    private function areaFiltroOutros(string $nomeTab, ?array $filtroRecuperado = null)
    {

        $this->exibirCampoNumeroAta($nomeTab, $filtroRecuperado);

        $unidadeUsuario = session('user_ug') . " - " . session('user_ug_nome_resumido');
        $this->crud->addField([
            'name' => 'unidade_solicitante_fake',
            'label' => 'Unidade Solicitante',
            'type' => 'label',
            'value' => $unidadeUsuario,
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3'
            ],
            'tab' => $nomeTab
        ]);
    }

    /**
     * Método responsável em enviar para a tela os campos hidden
     */
    private function areaCamposHidden()
    {
        # ID da unidade que o usuário está logado
        $this->crud->addField([
            'name' => 'unidade_solicitante',
            'type' => 'hidden',
            'value' => session('user_ug_id'),
        ]);

        # URL para buscar o item
//        $this->crud->addField([
//            'name' => 'url_buscar_item_remanejamento',
//            'type' => 'hidden',
//            'attributes' => ['id' => 'url_buscar_item_remanejamento'],
//            'value' => url('arp/remanejamento/buscaritemremanejamento'),
//        ]);
    }

    /**
     * Método responsável em importar o arquivo JS para o remanejamento
     */
    private function importarJSRemanejamento()
    {
        $this->importarDatatableForm();

        $arquivoJS = [
            'assets/js/admin/forms/arp_remanejamento.js',
            'assets/js/blockui/jquery.blockUI.js',
            'packages/backpack/base/js/padrao_gov/jquery.mask.js'
        ];

        $this->importarScriptJs($arquivoJS);
    }

    /**
     * Método responsável em exibir os campos para o usuário realizar
     * o filtro do item
     */
    private function areaFiltro($nomeTab, ?array $filtroAplicadoUsuario)
    {
        # Exibir os campos da compra para o filtro do usuário
        $this->areaFiltroCompra($nomeTab, $filtroAplicadoUsuario);

        # Exibir os demais campos do filtro
        $this->areaFiltroOutros($nomeTab, $filtroAplicadoUsuario);

        # Exibir os campos 'ocultos' no form
        $this->areaCamposHidden();

        # Importar o JS necessário para a tela
        $this->importarJSRemanejamento();

        # Se não tiver valor, então será na tela de cadastro
        if (empty($filtroAplicadoUsuario)) {
            # Exibe o botão para buscar os itens
            $this->addAreaCustom(
                'btn_recuperar_compra_arp',
                'areaDadosCompra col-md-4 pt-6-form-br',
                null,
                null,
                null,
                'btn_recuperar_item_remanejamento',
                'Buscar o item:<br>Somente serão exibidos itens com saldo para
                    remanejamento e de participante da compra e
                    de não participantes da compra (carona)',
                $nomeTab
            );
        }
    }

    /**
     * Método responsável em preencher as colunas da tabela para o usuário
     * lançar a quantidade que será remanejada
     */
    private function camposTabelaItemAdesao()
    {
        return [
            'numero_ata' => 'Nº da ata',
            // 'fornecedor' => 'Fornecedor',
            'unidade_origem' => 'Unidade origem',
            'numero_compra' => 'Nº compra',
            'tipo_uasg_extenso' => 'Tipo',
            'numero' => 'Nº do item',
            'descricaodetalhada' => 'Descrição do item',
            'quantidade_saldo' => 'Saldo para o remanejamento',
            'quantidadesolicitada' => 'Quant. solicitada',
        ];
    }

    /**
     * Método responsável em exibir a tabela para o usuário preencher o valor
     */
    private function areaItemRemanejamento(string $nomeTab)
    {
        # Recuperar as colunas da tabela
        $column = $this->camposTabelaItemAdesao();

        # Exibir a tabela na tela do usuário
//        $this->addTableCustom(
//            $column,
//            'table_selecionar_item_arp_remajenamento',
//            'Selecionar o fornecedor para exibir a informação',
//            'col-md-12 pt-5',
//            'areaItemAdesao',
//            $nomeTab,
//            90,
//            null,
//            'Preencha a quantidade para solicitar o remanejamento para o item',
//            "[4,'desc']"
//        );

        $this->crud->addField(
            [
                'name'  => 'table_ajax_item_remanejamento_arp',
                'type'  => 'table_ajax_item_remanejamento_arp',
                'idTable' => 'table_ajax_item_remanejamento_arp',
                'emptyTable' => 'Nenhum registro encontrato',
                'classArea' => 'col-md-12 pt-5',
                'idArea' => 'areaItemAdesao',
                'column' => $column,
                'tab' => $nomeTab,
                'pageLength' => 50,
                'urlAjax' => url('arp/remanejamento/buscaritemremanejamento'),
                'label_instrucao_tabela' => 'Preencha a quantidade para solicitar o remanejamento para o item'
            ]
        );

        # Campo necessário para verificar quantos arquivos existem
        # para que possa ser verificado na validação no backend
        $this->crud->addField(
            [
                'name' => 'qtd_ata_selecionada',
                'type' => 'hidden',
                'attributes' => ['id' => 'qtd_ata_selecionada'],
                'fake' => true
            ]
        );
    }

    private function tabItemRemanemajamento(string $nomeTab, ?array $filtroAplicadoUsuario = null)
    {
        # Exibir a área do filtro para o usuário preencher
        $this->areaFiltro($nomeTab, $filtroAplicadoUsuario);

        # Exibir os itens com base no filtro selecionado
        $this->areaItemRemanejamento($nomeTab);
    }

    /**
     * Método responsável em exibir a guia Checklist
     */
    private function tabCheckList(
        string $nomeTab,
        ?array $textoInformativoUsuarioArquivo = null,
        bool $tipoMultiple = true
    ) {
        $this->crud->addField(
            [   // radio
                'name' => 'remanejamento_entre_unidade_estado_df_municipio_distinto',
                'label' => 'O remanejamento está sendo feito entre
                                    unidades de estado, distrito federal ou
                                    município distintos?', // the input label
                'type' => 'radio',
                'options' => [
                    // the key will be stored in the db, the value will be shown as label;
                    0 => "Não",
                    1 => "Sim"
                ],
                'attributes' =>
                    ['onchange' => 'marcacaoCampoRemanejamentoEntreUnidadeEstadoDfMunicipioDistinto(this)'],
                'wrapperAttributes' => ['class' => 'col-md-6 pt-6'],
                // optional
                'inline' => 'd-inline-block', // show the radios all on the same line?
                'tab' => $nomeTab,
                'required' => true
            ]
        );

        $this->crud->addField(
            [   // radio
                'name' => 'fornecedor_de_acordo_novo_local',
                'label' => 'O(s) fornecedor(es) beneficiário(s) da ata de registro de preços está(ão) de
                acordo com o fornecimento no novo ente federativo?',
                'type' => 'radio',
                'options' => [
                    // the key will be stored in the db, the value will be shown as label;
                    0 => "Não",
                    1 => "Sim"
                ],
                // 'attributes' => [ 'required' => 'required' ],
                'wrapperAttributes' => ['class' => 'col-md-6 pt-6'],
                // optional
                'inline' => 'd-inline-block', // show the radios all on the same line?
                'tab' => $nomeTab,
            ]
        );

        # Conteúdo da tab Arquivo(s)
        $this->tabArquivo($nomeTab, $textoInformativoUsuarioArquivo, $tipoMultiple);
    }

    /**
     * Método responsável em exibir a guia dos arquivos para vincular a ata
     * que o usuário selecionar
     */

    private function tabArquivo(
        string $nomeTab,
        ?array $textoInformativoUsuarioArquivo = null,
        bool $tipoMultiple = true
    ) {
        $this->crud->addField(
            [   // radio
                'name' => 'arquivoRemanejamento',
                'upload' => true,
                // 'labelInformativo'       => 'Selecione o arquivo para o remanejamento(ata)',
                'type' => 'upload_file_generico',
                // 'attributes' => [ 'required' => 'required' ],
                'wrapperAttributes' => ['class' => 'col-md-12 pt-3'],
                'tab' => $nomeTab,
                'textoInformativoUsuarioArquivo' => $textoInformativoUsuarioArquivo,
                'multiple' => $tipoMultiple
            ]
        );
    }

    /**
     * Método responsável em exibir os botões para o usuário
     */
    private function botoesAcao()
    {
        $this->crud->button_guia = true;

        $this->crud->button_custom = [
            [
                'button_text' => 'Salvar rascunho',
                'button_id' => 'rascunho',
                'button_name_action' => 'rascunho',
                'button_value_action' => '1',
                'button_icon' => 'fas fa-edit',
                'button_tipo' => 'secondary'
            ],
            [
                'button_text' => 'Solicitar remanejamento',
                'button_id' => 'adicionar',
                'button_name_action' => 'rascunho',
                'button_value_action' => '0',
                'button_icon' => 'fas fa-save',
                'button_tipo' => 'primary'
            ],
        ];
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.contentClass', 'col-md-12');

        $this->crud->set('show.setFromDb', false);

        $this->campoComumListShow();
        $this->addColumnModelFunction('situacao', 'Situação', 'getSituacao');
        
        $remanejamento = $this->crud->getEntry($this->crud->getCurrentEntryId());
        
        if ($remanejamento->situacao->descricao === 'Aguardando aceitação da unidade gerenciadora') {
            $this->addColumnModelFunction(
                'unidade_gerenciadora_ata',
                'Unidade gerenciadora da ata',
                'getUnidadeGerenciadoraAta'
            );
        }
        
        $this->addColumnCreatedAt(true, true, true, true);
        
        $this->exibirAnexoItemRemanejamento($this->crud);
        
        if ($remanejamento->getSituacao() === 'Cancelada') {
            $this->crud->removeButton('update');
        }
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');
        CRUD::setValidation(ArpRemanejamentoRequest::class);

        # Conteúdo da tab dos itens para o usuário selecionar e remanejar
        $this->tabItemRemanemajamento('Item(ns) para remanejamento');

        # Conteúdo da tab checklist
        $this->tabCheckList('Dados e Arquivo(s)');

        # Conteúdo da tab Arquivo(s)
        // $this->tabArquivo('Arquivo(s)');

        $this->botoesAcao();

        CRUD::setEntityNameStrings(
            'Solicitação de remanejamento de quantitativo',
            'Solicitação de remanejamento de quantitativo'
        );
//        CRUD::setSubheading('Criar', 'create');
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');
        CRUD::setValidation(ArpRemanejamentoRequest::class);

        # Recuperar o filtro aplicado pelo usuário no REDIS
        $filtroRecuperado = $this->recuperarFiltroUsuarioRedis($this->crud->getCurrentEntryId());

        # Conteúdo da tab dos itens para o usuário selecionar e remanejar
        $this->tabItemRemanemajamento('Item(ns) para remanejamento', $filtroRecuperado);


        # Recuperar o arquivo vinculado ao remanejamento
        // $arquivo = $this->getArquivoGenerico($this->crud->getCurrentEntryId(), ArpRemanejamento::class);
        $arpRemanejamento = $this->crud->getEntry($this->crud->getCurrentEntryId());

        # Montar o array com os dados necessários para poder exibir o arquivo em tela
        # para o usuário
        $arrayTextoInformativoUsuarioArquivo =
            [
                'idUnico' => $arpRemanejamento->arp->id,
                'textoCampo' => "do remanejamento da ata {$arpRemanejamento->arp->getNumeroAno()}.<br>",
                'modelClass' => ArpRemanejamento::class,
                'textoUnico' => $arpRemanejamento->arp->getNumeroAno()
            ];

        # Conteúdo da tab checklist
        $this->tabCheckList('Dados e Arquivo', $arrayTextoInformativoUsuarioArquivo, false);

        # Conteúdo da tab Arquivo(s)
        // $this->tabArquivo('Arquivo(s)', $arrayTextoInformativoUsuarioArquivo, false);

        $this->botoesAcao();
    }

    /**
     * Método responsável em exibir as atas ativas quando digitado o valor
     * no campo Número da Ata
     */
    public function numeroAta(Request $request)
    {
        $search_term = $request->input('q');
        $arpRepository = new ArpRepositoy();

        # Recuperar as informações para exibir no select
        $results = $arpRepository->montarSelectAtaAtivaRemanejamento($search_term, "numero_ano_ata_remanejamento");

        return $results;
    }

    /**
     * Método responsável em exibir as compras vinculadas as atas
     * quando digitado o valor no campo Número/Ano da Compra
     */
    public function numeroCompraAta($idUnidadeCompra, Request $request)
    {
        $search_term = $request->input('q');

        # Recuperar as informações para exibir no select
        $results = $this->arpService->montarSelectNumeroCompraAta($search_term, $idUnidadeCompra);

        return $results;
    }

    /**
     * Método responsável em exibir a modalidade da compra vinculada as atas
     * quando digitado o valor no campo Modalidade da Compra
     */
    public function modalidadeCompraAta($idUnidadeCompra, Request $request)
    {
        $search_term = $request->input('q');

        # Recuperar as informações para exibir no select
        $results = $this->arpService->montarSelectModalidadeCompraAta($search_term, $idUnidadeCompra);

        return $results;
    }

    /**
     * Método responsável em exibir a unidade da compra vinculadas as atas
     * quando digitado o valor no campo Unidade da Compra
     */
    public function unidadeCompraAta(Request $request)
    {
        $search_term = $request->input('q');
        $arpRepository = new ArpRepositoy();

        # Recuperar as informações para exibir no select
        $results = $arpRepository->montarSelectUnidadeCompraAta($search_term);

        return $results;
    }

    /**
     * Método responsável em exibir os itens para o usuário
     * solicitar o remanejamento
     */
    public function buscarItemRemanejamento(Request $request)
    {
        $arpRepository = new ArpRepositoy();

        # Recuperar os itens do remanejamento para exibir ao usuário
        # conforme o filtro aplicado em tela
        $itens = $arpRepository->itensRemanejamento($request->all());

        $draw = $request->all()['draw'];

        return [
            'draw'            => (isset($draw) ? (int) $draw : 0),
            'recordsTotal'    => $itens['totalLinhas'],
            'recordsFiltered' => $itens['totalLinhas'],
            'data'            => $itens['data'],
        ];
    }

    /**
     * Método responsável em validar a quantidade que o usuário solicitou
     * no item
     */
    public function validarQuantitativoSolicitado(Request $request)
    {
        # Criar o array para retornar no AJAX
        $arrayRetorno = array();

        $dados = $request->all();

        # Variável responsável para controlar se o valor digitado pelo usuário pode ser solicitado
        $bloqueiaLancamento = false;
        $remanejamentoService = new ArpRemanejamentoService();
        $compraItemId = $dados['compraItemId'];
        $unidadeSolicitanteId = $dados['idUnidadeSolicitante'];

        $unidadeSolicitanteCarona =
            $remanejamentoService->unidadeSolicitanteCaronaItem($compraItemId, $unidadeSolicitanteId);

        if (!$unidadeSolicitanteCarona) {
            $arrayRetorno['bloqueiaLancamento'] = $bloqueiaLancamento;
            return response()->json($arrayRetorno);
        }

        $retornoValidacao = $remanejamentoService->usuarioPodeSolicitarQuantitativo(
            $compraItemId,
            $unidadeSolicitanteId,
            $dados['somatorioCampoItem']
        );

        # Se o valor dos 50% for menor que o digitado pelo usuário, então bloqueia o lançamento
        if (!$retornoValidacao['podeSolicitar']) {
            $bloqueiaLancamento = true;

            $quantidadeValidacao = $retornoValidacao['quantidadeSaldo'];
            $unidadeCeder = CompraItemUnidade::find($dados['idItemUnidadeCeder']);
            $itemSolicitado = Unidade::find($unidadeSolicitanteId);
            # Complementa o array com as informações para exibir na modal de erro
            $arrayRetorno['itemCompleto'] = $itemSolicitado->exibicaoCompletaUASG();
            $arrayRetorno['unidadeOrigemItem'] = $unidadeCeder->unidades->exibicaoCompletaUASG();
            $arrayRetorno['quantidadeValidacao'] = number_format($quantidadeValidacao, 5, ',', '.');
        }

        $arrayRetorno['bloqueiaLancamento'] = $bloqueiaLancamento;

        return response()->json($arrayRetorno);
    }

    /**
     * Método responsável em cancelar o remanejamento solicitado para o usuário
     */
    public function cancelarRemanejamento(Request $request)
    {
        try {
            DB::beginTransaction();
            # Recupera o id do remanejamento para cancelar
            $idRemanejamento = $request->idRemanejamento;
            $arpRemanejamentoRepository = new ArpRemanejamentoRepository();
            $compraItemUnidadeRepository = new CompraItemUnidadeRepository();
            # Recupera as informações do remanejamento
            $remanejamento = $arpRemanejamentoRepository->getRemanejamentoUnico($idRemanejamento);
            
            $itemSemSaldoParaEstorno = false;
            foreach ($remanejamento->itemRemanejamento as $item) {
                if (empty($item->situacaoItem)) {
                    continue;
                }
                
                # Cancelar os itens que não foram negados
                if ($item->situacaoItem->descricao != 'Item Negado') {
                    # Se o item ainda estiver aguardando análise da unidade gerenciadora do item ou ata
                    if ($item->situacaoItem->descricao == 'Aguardando aceitação da unidade participante' ||
                        $item->situacaoItem->descricao == 'Aguardando aceitação da unidade participante e gerenciadora'
                    ) {
                        $item->situacao_id = $this->getSituacao('Item Cancelado');
                        $item->save();
                    }

                    $arrayPodeAlterar = ['Item Aceito', 'Item Aceito Parcialmente'];

                    # Se o item estiver com a análise concluída
                    if (in_array($item->situacaoItem->descricao, $arrayPodeAlterar)) {
                        $quantidadeAprovadaGerenciadoraAta = $item->quantidade_aprovada_gerenciadora;
                        
                        $idItemUnidadeAtribuir = $item->itemUnidade;
                        
                        $itemUnidadeRetirar = $item->itemUnidadeReceber;
                        
                        $itemUnidadeRetirar->quantidade_autorizada -= $quantidadeAprovadaGerenciadoraAta;
                        $itemUnidadeRetirar->save();
                        
                        $saldoUnidadeReceber = $this->retornaSaldoAtualizado(
                            $itemUnidadeRetirar->compra_item_id,
                            $itemUnidadeRetirar->unidade_id,
                            $itemUnidadeRetirar->id
                        );

                        $itemUnidadeRetirar->quantidade_saldo = $saldoUnidadeReceber->saldo;

//                        # Se o saldo for negativo, então o item não pode ser cancelado.
                        if ($itemUnidadeRetirar->quantidade_saldo < 0) {
                            $itemSemSaldoParaEstorno = true;
                            continue;
                        }
                        
                        $itemUnidadeRetirar->save();
                        
                        $idItemUnidadeAtribuir->quantidade_autorizada += $quantidadeAprovadaGerenciadoraAta;

                        $idItemUnidadeAtribuir->save();
                        
                        $saldoAtribuirReceber = $this->retornaSaldoAtualizado(
                            $idItemUnidadeAtribuir->compra_item_id,
                            $idItemUnidadeAtribuir->unidade_id,
                            $idItemUnidadeAtribuir->id
                        );
                        
                        $idItemUnidadeAtribuir->quantidade_saldo = $saldoAtribuirReceber->saldo;

                        $idItemUnidadeAtribuir->save();
                        
                        $item->situacao_id = $this->getSituacao('Item Estornado');
                        $item->save();
                    }
                }
            }

            $idSituacaoCancelamentoRemanejamento = null;
            $mensagemAlerta = null;
            
            # Altera o status para Cancelado
            if ($itemSemSaldoParaEstorno) {
                $idSituacaoCancelamentoRemanejamento = $this->getSituacao('Cancelado parcialmente');
                $mensagemAlerta =
                    'O remanejamento não pode ser cancelado por completo devido ter item sem saldo para o estorno';
            }
            
            if (!$itemSemSaldoParaEstorno) {
                $idSituacaoCancelamentoRemanejamento = $this->getSituacao('Cancelada');
                $mensagemAlerta = 'Remanejamento cancelado com sucesso';
            }
            
            $remanejamento->situacao_id = $idSituacaoCancelamentoRemanejamento;
            
            $retornoAjax = [
                'sucesso' => $remanejamento->save(),
                'mensagem' => $mensagemAlerta
            ];
            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            $retornoAjax = [
                'sucesso' => false,
                'mensagem' => 'Erro ao realizar o cancelamento'
            ];
            $this->inserirLogCustomizado('gestao_ata', 'error', $exception);
        }
        
        # Retornar a mensagem conforme o saldo de todos os itens
        return $retornoAjax;
    }

    /**
     * Método responsável em montar as informações para inserir na tabela de
     * remanejamento
     */
    private function montarDataRemanejamento(
        array $dadosForm,
        int $arpId,
        ArpRemanejamentoRepository $arpRemanejamentoRepository,
        array $idSituacoes
    ) {
        $arrayData = array();

        // $arrayData['unidade_solicitante_id'] = $dadosForm['unidade_solicitante'];
        $arrayData['arp_id'] = $arpId;
        $arrayData['unidade_solicitante_id'] = session('user_ug_id');
        $arrayData['sequencial'] =
            $arpRemanejamentoRepository->getSequencial($dadosForm['rascunho'], $arrayData['unidade_solicitante_id']);
        $arrayData['remanejamento_entre_unidade_estado_df_municipio_distinto'] =
            $dadosForm['remanejamento_entre_unidade_estado_df_municipio_distinto'];
        $arrayData['fornecedor_de_acordo_novo_local'] =
            $dadosForm['fornecedor_de_acordo_novo_local'] ?? null;
        $arrayData['rascunho'] = $dadosForm['rascunho'];

        $arrayData['situacao_id'] = $idSituacoes['Em elaboração'];


        if (!$arrayData['rascunho']) {
            $arrayData['situacao_id'] = $idSituacoes['Aguardando aceitação da unidade participante'];
            $arrayData['data_envio_solicitacao'] = Carbon::now()->format('Y-m-d H:i:s');
        }


        return $arrayData;
    }

    /**
     * Método responsável em montar as informações para inserir na tabela de
     * remanejamento_itens
     */
    private function montarDataRemanejamentoItem(
        ArpRemanejamento $remanejamento,
        CompraItemUnidadeRepository $compraItemUnidadeRepository,
        float $quantidadeSolicitada,
        int $idItemUnidade,
        bool $rascunho,
        array $idSituacoes,
        ?int $idItemUnidadeReceber = null
    ) {

        $arrayData = array();

        $arrayData['remanejamento_id'] = $remanejamento->id;
        $arrayData['quantidade_solicitada'] = $quantidadeSolicitada;
        $arrayData['id_item_unidade'] = $idItemUnidade;
        $arrayData['id_usuario_solicitante'] = backpack_user()->id;

        if (!empty($idItemUnidadeReceber)) {
            $arrayData['id_item_unidade_receber'] = $idItemUnidadeReceber;
        }

        if (!empty($idItemUnidadeReceber)) {
            $arrayData['id_item_unidade_receber'] = $idItemUnidadeReceber;
        }

        if (!$rascunho) {
            # Se a unidade não for gerenciadora da ata
            $arrayData['situacao_id'] = $idSituacoes['Aguardando aceitação da unidade participante'];

            # Recuperar o id da unidade dona do item
            $idUnidadeGerenciadoraItem = $compraItemUnidadeRepository->getItemUnidade($arrayData['id_item_unidade']);

            # Recuperar o remanejamento cadastrado id da unidade gerenciadora da ata
            $idUnidadeGerenciadoraAta = $remanejamento->arp->unidade_origem_id;

            # Se a unidade for gerenciadora da ata, então alteramos para o status que a Gerenciadora da ata
            # vai analisar, pulando a etapa da unidade de origem (dona do item)
            if ($idUnidadeGerenciadoraItem->unidade_id == $idUnidadeGerenciadoraAta) {
                $arrayData['situacao_id'] = $idSituacoes['Aguardando aceitação da unidade gerenciadora'];
            }
        }

        return $arrayData;
    }

    /**
     * Método responsável em montar somente as informações com o filtro aplicado
     * pelo usuário
     */
    private function montarDataFiltroUsuario(array $dataForm)
    {
        $arrayFiltro = array();

        $arrayFiltro['numero_compra'] = $dataForm['numero_compra'] ?? null;
        $arrayFiltro['modalidade_compra'] = $dataForm['modalidade_compra'] ?? null;
        $arrayFiltro['unidade_compra'] = $dataForm['unidade_compra'] ?? null;
        $arrayFiltro['numero_ata'] = $dataForm['numero_ata'] ?? null;

        return $arrayFiltro;
    }

    /**
     * Método responsável em salvar o filtro aplicado pelo usuário
     * dentro do REDIS
     */
    private function salvarFiltroUsuarioRedis(array $filtroAplicado, int $idRemanejamento)
    {
        # Key único para salvar dentro do REDIS
        $key = "filtroAplicadoArpRemanejamento_{$idRemanejamento}";

        # Converter em JSON para salvar o filtro por ID do remanejamento
        $filtroAplicado = json_encode($filtroAplicado);

        # Salvar no REDIS
        Redis::set($key, $filtroAplicado);
    }

    /**
     * Método responsável em recuperar o filtro aplicado pelo usuário
     * dentro do REDIS
     */
    private function recuperarFiltroUsuarioRedis(int $idRemanejamento)
    {
        $key = "filtroAplicadoArpRemanejamento_{$idRemanejamento}";

        $filtroAplicado = Redis::get($key);

        return json_decode($filtroAplicado, true);
    }

    /**
     * Método responsável em atualizar o status do remanejamento
     */
    private function atualizarStatusRemanejamento(
        ArpRemanejamentoRepository $arpRemanejamentoRepository,
        Request $request,
        ArpRemanejamentoItemRepository $arpRemanejamentoItemRepository,
        array $idSituacoes
    ) {
        $mensagem = 'Remanejamento criado com sucesso';

        $camposUpdate = array();

        # Se a solicitação do usuário for solicitar direto, então altera o texto da mensagem
        if (!$request->rascunho) {
            $mensagem = 'Remanejamento solicitado com sucesso';
            
            $idStatusAceitacaoUnidadeOrigem = $idSituacoes['Aguardando aceitação'];
            
            
            # Recuperar todos os itens do remanejamento
            $todosItemRemanejamento =
                $arpRemanejamentoRepository->getRemanejamentoUnico($request->id)->itemRemanejamento;
            

            # Converter o resultado para o array
            $arrayStatusItem = $todosItemRemanejamento->pluck('situacao_id')->toArray();
            # Fazer uma contagem por status dentro do array
            $totalItemPorStatus = array_count_values($arrayStatusItem);
            
            # Somar todos os itens independente de status
            $totalItem = array_sum($totalItemPorStatus);
            
            # Recuperar o id do status Aguardando aceitação da unidade gerenciadora
            $idStatusAceitacaoUnidadeGerenciadora =
                $idSituacoes['Aguardando aceitação da unidade gerenciadora'];
            
            # Se todos os itens do remanejamento a unidade de origem da ata for analisar
            if (isset($totalItemPorStatus[$idStatusAceitacaoUnidadeGerenciadora]) &&
                $totalItemPorStatus[$idStatusAceitacaoUnidadeGerenciadora] == $totalItem
            ) {
                $idStatusAceitacaoUnidadeOrigem = $idStatusAceitacaoUnidadeGerenciadora;
            }
            
            $idStatusAceitacaoUnidadeGerenciadora =
                $idSituacoes['Aguardando aceitação da unidade participante'];
            
            # Se todos os itens do remanejamento a unidade de origem da ata for analisar
            if (isset($totalItemPorStatus[$idStatusAceitacaoUnidadeGerenciadora]) &&
                $totalItemPorStatus[$idStatusAceitacaoUnidadeGerenciadora] == $totalItem
            ) {
                $idStatusAceitacaoUnidadeOrigem = $idStatusAceitacaoUnidadeGerenciadora;
            }

            # Campos que serão atualizados ao solicitar o remanejamento
            $camposUpdate = [
                'situacao_id' => $idStatusAceitacaoUnidadeOrigem,
                'sequencial' => $arpRemanejamentoRepository->getSequencial($request->rascunho, session('user_ug_id')),
                'data_envio_solicitacao' => Carbon::now()->toDateTimeString(),
                'rascunho' => $request->rascunho
            ];
        }

        $camposUpdate['remanejamento_entre_unidade_estado_df_municipio_distinto'] =
            $request->remanejamento_entre_unidade_estado_df_municipio_distinto;
        $camposUpdate['fornecedor_de_acordo_novo_local'] =
            $request->fornecedor_de_acordo_novo_local;

        # Atualizar o registro do remanejamento
        $arpRemanejamentoRepository->updateRemanejamento($request->id, $camposUpdate);

        return $mensagem;
    }

    /**
     * Método responsável em retornar todos as situações do remanejamento
     */
    private function retornarSituacoesRemanejamento()
    {
        $situacoes = [
            'Em elaboração',
            'Aguardando aceitação da unidade participante',
            'Aguardando aceitação da unidade gerenciadora',
            'Aguardando aceitação'
        ];

        # Recuperar todas as situações para o remanejamento e itens
        return $this->getSituacoes($situacoes);
    }

    public function store(ArpRemanejamentoRequest $request)
    {
        DB::beginTransaction();
        $dadosForm = $request->all();

        try {
            # Retornar somente as informações que o usuário inseriu para os filtros
            $filtroAplicadoUsuario = $this->montarDataFiltroUsuario($dadosForm);

            $arpRemanejamentoRepository = new ArpRemanejamentoRepository();
            $arpRemanejamentoItemRepository = new ArpRemanejamentoItemRepository();
            $compraItemUnidadeRepository = new CompraItemUnidadeRepository();

            # Recuperar todas as situações para o remanejamento e itens
            $idSituacoes = $this->retornarSituacoesRemanejamento();

            // # Recuperar o id do tipo de arquivo Remenjamento
            $idTipoArquivoRemanejamento =
                $this->retornaIdCodigoItemPorDescres('arq_remanejamento', 'Tipo de Ata de Registro de Preços');

            # Mensagem de exibição para o usuário
            $mensagem = 'Solicitação de remanejamento adicionada com sucesso.<br>';

            $numeroAtaCadastrada = '';

            # Percorrer cada ata para criar a sua solicitação
            foreach ($dadosForm['id_ata'] as $arpId => $compraItemUnidade) {
                # Montar as informações para inserir na tabela arp_remanejamento
                $dataArpRemanejamento =
                    $this->montarDataRemanejamento(
                        $dadosForm,
                        $arpId,
                        $arpRemanejamentoRepository,
                        $idSituacoes
                    );

                # Inserir as informações para a criação do remanejamento
                # recuperando o ID para poder incluir nas tabelas de itens e arquivo
                $remanejamento = $arpRemanejamentoRepository->insertRemanejamento($dataArpRemanejamento);

                # Recupera o ID do remanejamento para inserir nos itens e arquivos
                $idRemanejamento = $remanejamento->id;
                foreach ($compraItemUnidade as $itemUnidade) {
                    $compraItemUnidadeCeder = $dadosForm['item_principal_solicitado'][$itemUnidade];
                    $quantidade = $dadosForm['quantidade_solicitada'][$itemUnidade];
                    $compraItemUnidadeReceber = $itemUnidade;
                    if ($itemUnidade == $compraItemUnidadeCeder) {
                        $compraItemUnidadeReceber = $compraItemUnidadeRepository->getItemUnidade($itemUnidade);
                        $compraItemUnidadeReceber = $compraItemUnidadeReceber->compraItens->compraItemUnidade
                            ->where("unidade_id", $dadosForm['unidade_solicitante'])
                            ->where('situacao', true)
                            ->first();
                        $compraItemUnidadeReceber = $compraItemUnidadeReceber->id;
                    }


                    $dataArpRemanejamentoItem = $this->montarDataRemanejamentoItem(
                        $remanejamento,
                        $compraItemUnidadeRepository,
                        $quantidade,
                        $compraItemUnidadeCeder,
                        $dadosForm['rascunho'],
                        $idSituacoes,
                        $compraItemUnidadeReceber
                    );

                        # Insere as informações na tabela arp_remanejamento_itens
                        $arpRemanejamentoItemRepository->insertRemanejamentoItem($dataArpRemanejamentoItem);
                }

                # Se existir o arquivo vinculado, então salva o registro
                if (!empty($request->file('arquivoRemanejamento')[$arpId])) {
                    # Inserir o arquivo para cada remanejamento de uma ata
                    $this->salvarArquivoFormatoGenerico(
                        $request->file('arquivoRemanejamento')[$arpId],
                        ArpRemanejamento::class,
                        $idRemanejamento,
                        $idTipoArquivoRemanejamento,
                        "arp/remanejamento/$idRemanejamento"
                    );
                }

                # Salvar o filtro do usuário para recuperar quando for rascunho
                if ($dadosForm['rascunho']) {
                    $mensagem = 'Remanejamento criado com sucesso.';
                    $this->salvarFiltroUsuarioRedis($filtroAplicadoUsuario, $idRemanejamento);
                }

                if (!$dadosForm['rascunho']) {
                    # Recuperar todos os itens do remanejamento
                    $todosItemRemanejamento =
                        $arpRemanejamentoItemRepository->getItemPorRemanejamento($idRemanejamento);

                    # Converter o resultado para o array
                    $arrayStatusItem = $todosItemRemanejamento->pluck('situacao_id')->toArray();
                    # Fazer uma contagem por status dentro do array
                    $totalItemPorStatus = array_count_values($arrayStatusItem);

                    # Somar todos os itens independente de status
                    $totalItem = array_sum($totalItemPorStatus);

                    # Recuperar o id do status Aguardando aceitação da unidade gerenciadora
                    $idStatusAceitacaoUnidadeGerenciadora =
                        $idSituacoes['Aguardando aceitação da unidade gerenciadora'];

                    # Se todos os itens do remanejamento a unidade de origem da ata for analisar
                    if (isset($totalItemPorStatus[$idStatusAceitacaoUnidadeGerenciadora]) &&
                        $totalItemPorStatus[$idStatusAceitacaoUnidadeGerenciadora] == $totalItem
                    ) {
                        $remanejamento->situacao_id = $idStatusAceitacaoUnidadeGerenciadora;
                        $remanejamento->save();
                        $numeroAtaCadastrada .= "Solicitação nº {$remanejamento->getNumeroSolicitacao()} ".
                                                "- Ata nº {$remanejamento->getNumeroAta()} <br>";
                        continue;
                    }

                    # Se existir ao menos um item para a gerenciadora do item e da ata
                    if ($totalItemPorStatus[$dataArpRemanejamento['situacao_id']] < $totalItem) {
                        $remanejamento->situacao_id =
                            $idSituacoes['Aguardando aceitação'];
                        $remanejamento->save();
                    }
                }
                
                $numeroAtaCadastrada .=
                    "Solicitação nº {$remanejamento->getNumeroSolicitacao()} - Ata nº {$remanejamento->getNumeroAta()}.
                    <br>";
            }
            
            $mensagem .= $numeroAtaCadastrada;
            
            \Alert::add('success', $mensagem)->flash();

            DB::commit();

            return redirect('arp/remanejamento');
        } catch (Exception $ex) {
            DB::rollBack();

            \Alert::add('error', 'Erro ao solicitar o remanejamento')->flash();

            # Inserir o erro no arquivo customizado gestao_ata
            $this->inserirLogCustomizado('gestao_ata', 'error', $ex);

            return redirect()->back();
        }
    }

    public function update(ArpRemanejamentoRequest $request)
    {
        DB::beginTransaction();
        try {
            $arpRemanejamentoItemRepository = new ArpRemanejamentoItemRepository();
            $arpRemanejamentoRepository = new ArpRemanejamentoRepository();
            $compraItemUnidadeRepository = new CompraItemUnidadeRepository();
            $remanejamento = $arpRemanejamentoRepository->getRemanejamentoUnico($request->id);

            # Recuperar todas as situações para o remanejamento e itens
            $idSituacoes = $this->retornarSituacoesRemanejamento();
            
            $remanejamento->itemRemanejamento->each->forceDelete();
            
            # Percorrer os itens digitados pelo usuário
            foreach ($request->quantidade_solicitada as $idItemUnidade => $quantidade) {
                # Montar o condicional do item para ver se vai atualizar ou incluir o registro
                $arrayCondicional = [
                    'remanejamento_id' => $remanejamento->id,
                    'id_item_unidade' => $idItemUnidade
                ];

                $compraItemUnidadeCeder = $request->item_principal_solicitado[$idItemUnidade];
                $quantidade = $request->quantidade_solicitada[$idItemUnidade];
                $compraItemUnidadeReceber = $idItemUnidade;
                if ($idItemUnidade == $compraItemUnidadeCeder) {
                    $compraItemUnidadeReceber = $compraItemUnidadeRepository->getItemUnidade($idItemUnidade);
                    $compraItemUnidadeReceber = $compraItemUnidadeReceber->compraItens->compraItemUnidade
                        ->where("unidade_id", $request->unidade_solicitante)
                        ->where('situacao', true)
                        ->first();
                    $compraItemUnidadeReceber = $compraItemUnidadeReceber->id;
                }

                # Recuperar os campos para a atualização ou inserção
                $camposUpdate = $this->montarDataRemanejamentoItem(
                    $remanejamento,
                    $compraItemUnidadeRepository,
                    $quantidade,
                    $compraItemUnidadeCeder,
                    $request->rascunho,
                    $idSituacoes,
                    $compraItemUnidadeReceber
                );

                # Atualizar/Inserir o item na tabela do arp_remanejamento_item
                $arpRemanejamentoItemRepository->updateRemanejamentoItem($arrayCondicional, $camposUpdate);
            }

            # Recuperar o id do tipo de arquivo Remenjamento
            $idTipoRemanejamento =
                $this->retornaIdCodigoItemPorDescres('arq_remanejamento', 'Tipo de Ata de Registro de Preços');

            # Montar o caminho que será salvo o arquivo, caso seja um arquivo sem ter nenhum sido inserido antes
            $pastaArquivo = "arp/remanejamento/{$remanejamento->id}";

            # Atualizar o arquivo com base na ação que o usuário realizou
            $this->atualizarArquivoGenericoFront(
                $request,
                'arquivoRemanejamento',
                ArpRemanejamento::class,
                $idTipoRemanejamento,
                $pastaArquivo
            );

            # Alterar o status do remanejamento salvo com base na ação do usuário
            $mensagem = $this->atualizarStatusRemanejamento(
                $arpRemanejamentoRepository,
                $request,
                $arpRemanejamentoItemRepository,
                $idSituacoes
            );

            \Alert::add('success', $mensagem)->flash();

            DB::commit();

            return redirect('arp/remanejamento');
        } catch (Exception $ex) {
            DB::rollBack();

            \Alert::add('error', 'Erro ao solicitar o remanejamento')->flash();

            # Inserir o erro no arquivo customizado gestao_ata
            $this->inserirLogCustomizado('gestao_ata', 'error', $ex);

            return redirect()->back();
        }
    }

    /**
     * Método responsável em deletar o registro
     */
    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        $arpRemanejamentoRepository = new ArpRemanejamentoRepository();

        # Inclusão do método forceDelete para que seja removido fisicamente
        # no banco de dados
        return $arpRemanejamentoRepository->deleteRemanejamento($id);
    }
}
