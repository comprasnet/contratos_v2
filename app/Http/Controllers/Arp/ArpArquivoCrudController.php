<?php

namespace App\Http\Controllers\Arp;

use App\Http\Requests\ArpArquivoRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Models\Arp;
use App\Models\ArpArquivo;
use App\Models\CodigoItem;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Traits\ImportContent;
use App\Models\ArpArquivoTipo;

/**
 * Class ArpArquivoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArpArquivoCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    use ImportContent;

    private $arp_id;
    private $arp;

    private function returnIdArp()
    {
        return Route::current()->parameter("arp_id");
    }

    private function breadCrumb(bool $acao = false, $texto = 'Adicionar')
    {
        $breadcrumb = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Ata de Registro de Preços' =>  backpack_url('arp'),
            'Arquivos' =>  false
        ];

        if ($acao) {
            $breadcrumb = [
                trans('backpack::crud.admin') => backpack_url('dashboard'),
                'Ata de Registro de Preço' =>  backpack_url('arp'),
                $texto . ' Arquivo Ata de Registro de Preços' =>  false,
                //'Voltar' =>  backpack_url('/arp/item/' . $this->returnIdArp() . '/arquivos'),
            ];
        }

        $this->data['breadcrumbs'] =  $breadcrumb;

        return view('backpack::dashboard', $this->data);
    }
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $arp_id = \Route::current()->parameter('arp_id');
        $this->arp = Arp::find($arp_id);

        CRUD::setModel(\App\Models\ArpArquivo::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . 'arp/item/' . $this->arp->id . '/arquivos');
        CRUD::setEntityNameStrings('arp arquivo', 'arp arquivos');

        // $this->crud->addClause('where', 'arp_arquivos.arp_id', '=', $this->arp->id);

        CRUD::addClause('join', DB::raw('arp'), 'arp.id', '=', 'arp_arquivos.arp_id');
        CRUD::addClause('join', "arp_arquivo_tipos", 'arp_arquivo_tipos.id', '=', 'arp_arquivos.tipo_id');

        CRUD::addClause('leftjoin', "arp_alteracao", 'arp_alteracao.id', '=', 'arp_arquivos.arp_alteracao_id');

        CRUD::addClause('where', "arp_arquivos.arp_id", '=', $this->arp->id);

        # Recupera os tipos de arquivos válidos de envio para o PNCP
        $tipoArquivoPNCP = config("arp.arp.tipo_arquivo_pncp");
        CRUD::addClause('whereIN', "arp_arquivo_tipos.nome", $tipoArquivoPNCP);

        # Recupera os arquivos das alterações que não estão em situação de rascunho
        $this->crud->query->whereRaw('(arp_arquivos.arp_alteracao_id is null or arp_alteracao.rascunho = false)');

        CRUD::addClause('select', ['arp_arquivos.*', 'arp.numero']);

        $this->exibirTituloPaginaMenu('Arquivos');

        $this->crud->addButtonFromView('line', 'arp_arquivo_restrito', 'arp_arquivo_restrito', 'beginning');

        $this->crud->addButtonFromView('line', 'arp_download_arquivo', 'arp_download_arquivo', 'final');

        $this->crud->denyAccess('show');
        $this->crud->denyAccess('update');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        # Armazena a ARP no CRUD para poder recuparar na view
        $this->crud->arp = $this->arp;

        $this->crud->setListView('vendor.backpack.crud.arp.list-arquivos-ata');

        /*
        $this->addColumnText(
            false,
            true,
            false,
            true,
            'numero',
            'Número da Ata'
        );*/

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'descricao',
            'Descrição'
        );

        CRUD::column('tipo_id');

        $this->addColumnDateHour(
            false,
            true,
            true,
            true,
            'created_at',
            'Incluído em'
        );

        CRUD::addColumn([
            'name' => 'user_id',
            'label' => 'Incluído por',
            //'type' => 'model_function',
            'limit' => 9999,
            'function_name' => 'getUser',
            'visibleInTable' => false,
            'visibleInModal' => false,
            'visibleInShow' => false,
            'visibleInExport' => true,
        ]);

        $this->crud->enableExportButtons();

        $this->crud->text_button_redirect_create = 'Arquivo';
        $this->breadCrumb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');
        $checkbox_field = view('vendor.backpack.crud.fields.restrito_checkbox');
        $arquivo_field = view('vendor.backpack.crud.fields.arquivo_field');
        
        
        $this->crud->addField([
            'name' => 'arp_id',
            'type'  => 'hidden',
            'value' => $this->arp->id
        ]);

        
        # Armazena a arp no crud para a view receber
        $this->crud->arp = $this->arp;
        $this->crud->addField([
            'name'      => '',
            'type'      => 'label',
            'value'     => view('vendor.backpack.crud.arp.ata-cabecalho-padrao', ['crud' => $this->crud])
        ]);

        $this->crud->addField([
            'name' => 'descricao',
            'label' => 'Descrição',
            'type'  => 'text',
            'required' => true,
            'wrapperAttributes' => [ 'class' => 'col-md-6' ],
        ]);
        $tipoArquivoPNCP = config("arp.arp.tipo_arquivo_pncp");

        $optionsTipoArquivo = ArpArquivoTipo::whereIn("nome", $tipoArquivoPNCP)
            ->select("nome", "id")
            ->get()
            ->pluck("nome", "id")
            ->toArray();

        // CRUD::field('tipo_id');
        $this->crud->addField([
            'name'        => 'tipo_id',
            'label'       => "Tipo",
            'type'        => 'select2_from_array',
            'options'     => $optionsTipoArquivo,
            'wrapperAttributes' => [ 'class' => 'col-md-6' ],
            'attributes' => [ 'id' => 'tipo_id' ],
        ]);

        $this->crud->addField([
            'name'  => 'restrito',
            'type'  => 'custom_html',
            'value' => $checkbox_field,
            'wrapperAttributes' => ['class' => 'col-md-12 restrito-wrapper'],
        ]);

        $this->crud->addField([
            'name'      => 'arquivo',
            'type'      => 'custom_html',
            'value'     => $arquivo_field,
            'upload'    => true,
            'wrapperAttributes' => ['class' => 'col-md-6'],
        ]);

        $tipoArp = ArpArquivoTipo::where('nome', 'Ata de Registro de Preços')->first();

        $arquivoTipoArp = ArpArquivo::where('arp_id', $this->arp->id)
            ->where('tipo_id', $tipoArp->id)
            ->first();

        if (isset($arquivoTipoArp)) {
            $alert_field = view('vendor.backpack.crud.fields.arp_arquivo_alert');
            $this->crud->addField([
                'name'      => 'alert',
                'type'      => 'custom_html',
                'value'     => $alert_field,
                'wrapperAttributes' => ['class' => 'col-md-6'],
            ]);
        }

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Criar Arquivo',
        ]);

        $this->breadCrumb(true);

        $this->importarScriptJs(['assets/js/admin/forms/arp_arquivo.js']);
        $this->importarScriptCss(['assets/css/arp_arquivo.css']);
    }

    public function store(ArpArquivoRequest $request)
    {
        // get variáveis para upload de arquivo
        if ($request->file('arquivo')) {
            $disk = "public";
            $arp = Arp::find($request->arp_id);
            $destination_path = "arp/" . Carbon::now()->format('Y_m') . "/" . $arp->id;

            DB::beginTransaction();

            try {
                $arquivoTipo = ArpArquivoTipo::where('nome', 'Ata de Registro de Preços')->first()->id;

                $arquivoCheck = ArpArquivo::where('arp_id', $arp->id)
                    ->where('tipo_id', $arquivoTipo)
                    ->first();

                if (isset($arquivoCheck) && $request->tipo_id == $arquivoTipo) {
                    $arquivoCheck->delete();
                    if ($arp->rascunho) {
                        Storage::disk('public')->delete($arquivoCheck->url);
                    }
                }

                //cria o nome
                $new_file_name = $request->file('arquivo')->hashName();
                //faz upload
                $file_path = $request->file('arquivo')->storeAs($destination_path, $new_file_name, $disk);

                //grava no banco
                $anexos = new ArpArquivo();
                $anexos->arp_id = $arp->id;
                $anexos->tipo_id = $request->tipo_id;
                $anexos->descricao = $request->descricao;
                $anexos->nome = '-';
                $anexos->restrito = isset($request->restrito);
                $anexos->url = $file_path;
                $anexos->user_id = Auth::id();
                $anexos->save();

                DB::commit();

                \Alert::success('Arquivo enviado com sucesso')->flash();
                return \Redirect::to($this->crud->route);
            } catch (\Exception $e) {
                DB::rollBack();

                $firstExp = new MessageBag(['arparquivo_exception' => 'Falha ao enviar o arquivo']);

                $secondExp = new MessageBag(
                    [
                        'other_exception' => $e->getMessage()
                    ]
                );
                $firstExp->merge($secondExp);

                \Alert::add('warning', $firstExp->all())->flash();

                return \Redirect::to($this->crud->route . '/create');
            }
        }

        \Alert::warning('Nenhum arquivo enviado')->flash();
        return \Redirect::to($this->crud->route . '/create');
    }

    public function destroy($arp_id, $id)
    {
        $arquivoDeletado = ArpArquivo::find($id);
        $tipoArp = ArpArquivoTipo::where('nome', 'Ata de Registro de Preços')->first();
        if (isset($arquivoDeletado) && $arquivoDeletado->tipo_id != $tipoArp->id) {
            $this->crud->hasAccessOrFail('delete');

            # Inclusão do método forceDelete para que seja removido fisicamente
            # no banco de dados
            return $arquivoDeletado->forceDelete($id);
        }

        if ($arquivoDeletado->tipo_id == $tipoArp->id) {
            return 2;
        }
    }

    public function alterarPrivacidade($arp_id, $arpArquivoId)
    {
        $arpArquivo = ArpArquivo::find($arpArquivoId);
        if ($arpArquivo) {
            $arpArquivo->restrito = !$arpArquivo->restrito;

            DB::beginTransaction();

            try {
                $arpArquivo->save();

                DB::commit();

                \Alert::success('Privacidade alterada com sucesso')->flash();

                return redirect()->back();
            } catch (\Exception $e) {
                DB::rollBack();
                Log::error('erro ao alterar o arquivo');
                Log::error($e);
                $firstExp = new MessageBag(['arparquivo_exception' => 'Falha ao alterar a privacidade do documento']);

                $secondExp = new MessageBag(
                    [
                        'other_exception' => $e->getMessage()
                    ]
                );
                $firstExp->merge($secondExp);

                \Alert::add('warning', $firstExp->all())->flash();

                return redirect()->back();
            }
        }
    }
}
