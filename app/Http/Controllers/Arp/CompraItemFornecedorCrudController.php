<?php

namespace App\Http\Controllers\Arp;

use App\Actions\DesabilitaAtualizaCompra;
use App\Actions\DesabilitaCompraItens;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Models\CompraItem;
use App\Models\Compras;
use App\Models\Fornecedor;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;

/**
 * Class CompraItemFornecedorCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CompraItemFornecedorCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $compraItem = CompraItem::find(\Route::current()->parameter('compra_item_id'));

        CRUD::setModel(\App\Models\CompraItemFornecedor::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/compras/'.$compraItem->compra_id.'/itens/'.$compraItem->id.'/compra-item-fornecedor');
        CRUD::setEntityNameStrings('fornecedor do item da compra', 'fornecedor do item da compra');

        $this->exibirTituloPaginaMenu('Fornecedor do item da compra');
        $this->bloquearBotaoPadrao($this->crud, config('security.permission_block_crud.compra-item-fornecedor'));

        $this->crud->denyAccess('list');
        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');

    }

    protected function setupShowOperation()
    {

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'classificacao',
            'Classificação'
        );

        CRUD::addColumn([
            'name' => 'fornecedor',
            'label' => 'Fornecedor',
            'type' => 'model_function',
            'limit' => 9999,
            'function_name' => 'getFornecedor',
            'visibleInTable' => true
        ]);

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'quantidade_homologada_vencedor',
            'Quantidade Homologada'
        );

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'situacao_sicaf',
            'Situação SICAF'
        );

        CRUD::addColumn([
            'name' => 'tipo_item',
            'label' => 'Tipo do Item',
            'type' => 'model_function',
            'limit' => 9999,
            'function_name' => 'getTipoItem',
            'visibleInTable' => true
        ]);

        CRUD::addColumn([
            'name' => 'valor_unitario',
            'label' => 'Valor Unitário',
            'type' => 'model_function',
            'limit' => 9999,
            'function_name' => 'getValorUnitario',
            'visibleInTable' => true
        ]);

        CRUD::addColumn([
            'name' => 'valor_negociado',
            'label' => 'Valor Negociado',
            'type' => 'model_function',
            'limit' => 9999,
            'function_name' => 'getValorNegociado',
            'visibleInTable' => true
        ]);

        CRUD::addColumn([
            'name' => 'qtd_empenhada',
            'label' => 'Quantidade Empenhada',
            'type' => 'model_function',
            'limit' => 9999,
            'function_name' => 'getQuantidadeEmpenhada',
            'visibleInTable' => true
        ]);

        $this->addColumnCompraItemFornecedorContratos(
            true,
        );

        $this->addColumnCompraItemFornecedorEmpenhos(
            true,
        );

        $this->addColumnCompraItemFornecedorEmpenhosDetalhados(
            true,
        );

    }
}
