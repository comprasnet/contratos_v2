<?php

namespace App\Http\Controllers\Arp;

use App\Http\Controllers\Operations\CreateOperation;
use App\Http\Controllers\Operations\UpdateOperation;
use App\Http\Requests\AdesaoRequest;
use App\Http\Requests\ArpAdesaoCancelamentoRequest;
use App\Http\Traits\Arp\ArpTrait;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\CommonFields;
use App\Http\Traits\CustomizeErro500Trait;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Http\Traits\UgPrimariaTrait;
use App\Models\Adesao;
use App\Models\AdesaoItem;
use App\Models\ArpSolicitacaoHistorico;
use App\Models\CompraItem;
use App\Models\CompraItemUnidade;
use App\Models\Contrato;
use App\Models\Unidade;
use App\Models\UnidadeConfiguracao;
use App\Repositories\Arp\ArpAdesaoItemRepository;
use App\Repositories\Arp\ArpAdesaoRepository;
use App\Repositories\Arp\ArpRepositoy;
use App\Repositories\OrgaoRepository;
use App\Services\Arp\AdesaoService;
use App\Rules\FornecedoresAdesaoDiferentesRule;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Prologue\Alerts\Facades\Alert;
use App\Http\Traits\Unidade\ProcessoTrait;
use Yajra\DataTables\DataTables;
use App\Notifications\NotificationFornecedor;

use App\Mail\NotificarPrepostoAdesao;

/**
 * Class AdesaoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AdesaoCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use CommonFields;
    use Formatador;
    use ImportContent;
    use BuscaCodigoItens;
    use ArpTrait;
    use ProcessoTrait;
    use UgPrimariaTrait;
    use CustomizeErro500Trait;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    protected $mp;
    
    protected $adesaoService;
    
    public function __construct(AdesaoService $adesaoService)
    {
        parent::__construct();
        $this->mp = config('arp.arp.resultado_lei');
        $this->adesaoService = $adesaoService;
    }
    public function setup()
    {
        if (!backpack_user()->can('gestaodeatas_V2_acessar')) {
            return abort(403);
        }

        CRUD::setModel(\App\Models\Adesao::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/arp/adesao');
        CRUD::addClause('where', 'arp_solicitacao.unidade_origem_id', '=', session('user_ug_id'));
        $this->exibirTituloPaginaMenu('Solicitar adesão', null, false);
    }

    private function camposListShow()
    {
        $this->addColumnModelFunction('numero_solicitacao', 'Nº Solicitação', 'getNumeroSolicitacao');
        $this->addColumnModelFunction('unidade_gerenciadora', 'Unidade Gerenciadora', 'getUnidadeGerenciadora');
        $this->addColumnModelFunction('numero_compra', 'Número da compra/ano', 'getNumeroCompra');
        $this->addColumnModelFunction('modalidade_compra', 'Modalidade da compra', 'getModalidade');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        $this->crud->prefix_text_button_redirect_create = 'Solicitar';
        $this->crud->text_button_redirect_create = 'adesão';
        $this->crud->addColumns([
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'model_function',
                'function_name' => 'getSituacaoPersonalizada',
                'limit' => 9999,
            ]
        ]);
        $this->camposListShow();
        $this->crud->addButtonFromModelFunction('line', 'getViewButtonAdesao', 'getViewButtonAdesao', 'beginning');
        $this->crud->addButtonFromView(
            'line',
            'arp_cancelar_adesao',
            'arp_cancelar_adesao',
            'end'
        );
        $arquivoJS = [
            'assets/js/admin/forms/arp_adesao_cancelamento.js'
        ];
        
        $this->importarScriptJs($arquivoJS);
        
        $this->crud->setListView('vendor.backpack.crud.arp.list-adesao');
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.contentClass', 'col-md-12');
        $this->camposListShow();
        $this->crud->addColumns([
            [
                'name' => 'situacao',
                'label' => 'Situação',
                'type' => 'model_function',
                'function_name' => 'getSituacao',
                'limit' => 9999,
            ]
        ]);
        $idAdesao = Route::current()->parameter("id");
        
        # Recuperar informações da adesão para exibir no show
        $adesao = Adesao::find($idAdesao);
        
        $situacaoAdesao = $adesao->getSituacao();
        
        $arraySituacaoCancelada = ['Cancelada pela gerenciadora', 'Cancelada pelo solicitante'];
        if (in_array($situacaoAdesao, $arraySituacaoCancelada)) {
            $this->crud->addColumn('justificativa_cancelamento');
            $this->addColumnModelFunction(
                'usuario_cancelamento',
                'Usuário responsável pelo cancelamento',
                'getUsuarioCancelamentoAdesao'
            );
            $this->addColumnModelFunction(
                'unidade_usuario_cancelamento',
                'Unidade responsável pelo cancelamento',
                'getUnidadeCancelamentoAdesao'
            );
            $this->addColumnDateHour(
                false,
                true,
                true,
                true,
                'data_cancelamento',
                'Data do cancelamento'
            );
        }

        $this->addColumnModelFunction('responsavel_adesao', 'Responsável', 'getResponsavelAdesao');

        $this->crud->addColumns([
            [
                'name' => 'texto_justificativa',
                'label' => 'Texto Justificativa',
                'type' => 'text',
                'limit' => 9999,
            ]
        ]);

        $arraySaida = [];
        # Se tiver um arquivo de justificativa, exibe para o usuário
        if (!empty($adesao->justificativa)) {
            $arrayArquivo = explode('-', $adesao->justificativa);
            $arraySaida['Nome'][0] = str_replace("_", " ", $arrayArquivo[1]);
            $arraySaida['Visualizar'][0] =
                '<a target="_blank" href="' . url("storage/$arrayArquivo[0]") . '"><i class="fas fa-eye"></i></a>';
        }

        $this->addColumnTable('justificativa_adesao', 'Anexo Justificativa', $arraySaida);

        $this->addColumnModelFunction(
            'demonstracao_valor',
            'Foi realizada demonstração de que os valores registrados estão <br>
                                        compatíveis com os valores praticados pelo mercado, <br>
                                        nos termos da Lei 14.133/2021
                                        (Art.23 e Art. 86, §2º, inc. II)?',
            'getDemonstracaoValor'
        );
        $demonstracao = Adesao::getDemonstracao($idAdesao);
        $this->addColumnTable('demonstracao_adesao', 'Anexo Demonstração', $demonstracao);

        $this->addColumnModelFunction(
            'aceitacao_valor',
            'Houve prévia consulta e aceitação do fornecedor, nos termos da Lei 14.133/2021 (Art. 86, §2º, inc. III)?',
            'getAceitacaoValor'
        );

        $aceitacao = Adesao::getAceitacao($idAdesao);
        $this->addColumnTable('aceitacao_adesao', 'Anexo Aceitação', $aceitacao);


        if (!empty($adesao->justificativa_item_isolado)) {
            $justItemIsolado = Adesao::getItemIsolado($idAdesao);
            $this->addColumnTable('aceitacao_justificativa_isolado', 'Anexo justificativa item isolado', $justItemIsolado);
        }

        if (!empty($adesao->texto_justificativa_item_isolado)) {
            $this->crud->addColumns([
                [
                    'name' => 'texto_justificativa_item_isolado',
                    'label' => 'Justificativa de item isolado pertencente a um grupo',
                    'type' => 'text',
                    'limit' => 9999,
                ]
            ]);
        }
        
        $this->addColumnModelFunction(
            'ata_enfrentando_impacto_decorrente_calamidade_publica',
            'Mostrar atas registradas para enfrentamento dos
                impactos decorrentes do estado de calamidade pública?',
            'getAtaEnfrentandoImpactoDecorrenteCalamidadePublica'
        );

        if ($adesao->aquisicao_emergencial_medicamento_material !== null) {
            $this->addColumnModelFunction(
                'aquisicao_emergencial_medicamento_material',
                'Aquisição emergencial de medicamentos e material de consumo médico-hospitalar?',
                'getAquisicaoEmergencialMedicamentoMaterial'
            );
        }

        if ($adesao->execucao_descentralizada_programa_projeto_federal !== null) {
            $this->addColumnModelFunction(
                'execucao_descentralizada_programa_projeto_federal',
                'A adesão é destinada à execução descentralizada de programa ou projeto federal'.
                ' com recursos financeiros provenientes de transferências voluntárias da União'.
                ' para Estados, municípios e Entidades da administração pública federal'.
                ' integrantes dos Orçamentos Fiscal e da Seguridade Social da União e a '.
                ' Organizações da Sociedade Civil (OSC)?',
                'getExecucaoDescentralizadaProgramaProjetoFederal'
            );

            if ($adesao->execucao_descentralizada_programa_projeto_federal) {
                $arrayArquivo =
                    explode('-', $adesao->anexo_comprovacao_execucao_descentralizada_programa_projeto_fed);
                $arraySaida['Nome'][0] = str_replace("_", " ", $arrayArquivo[1]);
                $arraySaida['Visualizar'][0] =
                    '<a target="_blank" href="' . url("storage/$arrayArquivo[0]") . '"><i class="fas fa-eye"></i></a>';

                $this->addColumnTable(
                    'anexo_comprovacao_execucao_descentralizada_programa_projeto_fed',
                    'Anexo comprovação',
                    $arraySaida
                );

                $this->crud->addColumns([
                    [
                        'name' => 'justificativa_anexo_comprovacao_execucao_descentralizada_progra',
                        'label' =>
                            'Justificativa da comprovação da execução descentralizada de programa ou projeto federal',
                        'type' => 'text',
                        'limit' => 9999,
                    ]
                ]);

                $this->addColumnModelFunction(
                    'ciente_analise_entre_projeto_federal',
                    'A unidade solicitante afirmou que se trata de uma execução descentralizada de programa ou'
                    .' projeto federal com recursos financeiros provenientes de transferências voluntárias da União'
                    .' para Estados, municípios e Entidades da administração pública federal integrantes dos'
                    .' Orçamentos Fiscal e da Seguridade Social da União e a Organizações da Sociedade Civil (OSC),'
                    .'conforme inciso I, § 2ºdo Art. 32 do Decreto nº 11.462 de 31/03/2023 e que a aceitação dessa'
                    .'solicitação de adesão não levará em consideração o saldo máximo de adesão do artigo citado',
                    'getCienteAnaliseEntreProjetoFederal'
                );
            }
        }


        $itens = AdesaoItem::getItemPorAdesao($idAdesao);
        
        $this->addColumnDateHour(
            false,
            true,
            true,
            true,
            'data_aprovacao_analise',
            'Data aprovação análise'
        );
        
        $this->addColumnTable('itens_adesao', 'Itens para adesão', $itens);

        # Bloqueia as funções abaixo se a adesão for ativa
        if (!$adesao->rascunho) {
            $this->crud->denyAccess(['update', 'delete']);
        }
    }

    /**
     * Método responsável em exibir os dados do item na tela de rascunho
     */
    private function areaItemAdesaoUpdate(array $jsonItem, $adesao, $nomeGuia = 'Item(ns) para adesão')
    {
        $this->crud->addField(
            [
                'name'      => 'label_item', // The db column name
                'type'      => 'label',
                'label'     => 'Unidade Gerenciadora', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value'     =>  $adesao->getUnidadeGerenciadora(),
                'tab' => $nomeGuia
            ]
        );

        $this->crud->addField(
            [
                'name'      => 'label_numero_compra', // The db column name
                'type'      => 'label',
                'label'     => 'Número da compra/Ano', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value'     =>  $adesao->getNumeroCompra(),
                'tab' => $nomeGuia
            ]
        );

        $this->crud->addField(
            [
                'name'      => 'label_modalidade', // The db column name
                'type'      => 'label',
                'label'     => 'Modalidade da compra', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value'     =>  $adesao->getModalidade(),
                'tab' => $nomeGuia
            ]
        );


        $this->crud->addField(
            [
                'name'      => 'label_modalidade', // The db column name
                'type'      => 'label',
                'label'     => 'Modalidade da compra', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value'     =>  $adesao->getModalidade(),
                'tab' => $nomeGuia
            ]
        );

        $this->crud->addField(
            [
                'name'      => 'label_unidade_origem', // The db column name
                'type'      => 'label',
                'label'     => 'Unidade solicitante', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value'     =>  session('user_ug') . "-" . session('user_ug_nome_resumido'),
                'tab' => $nomeGuia
            ]
        );

        $attributesProcesso['required'] = 'required';
        $maskAttributes = $this->getProcessoMask();
        if (!empty($maskAttributes)) {
            $attributesProcesso['data-mask'] = $maskAttributes;
        }

        $this->crud->addField([
            'name' => 'processo_adesao',
            'label' => 'Processo adesão',
            'type'  => 'text',
            'attributes' => array_merge($attributesProcesso, ['maxlength' => 255]),
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'tab' => $nomeGuia
        ]);
        
        $this->crud->addField([
            'name' => 'ata_enfrentando_impacto_decorrente_calamidade_publica',
            'label' => 'Mostrar atas registradas para enfrentamento dos
                impactos decorrentes do estado de calamidade pública?',
            'type'  => 'label',
            'attributes' => array_merge($attributesProcesso, ['maxlength' => 255]),
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'tab' => $nomeGuia,
            'value' => $adesao->getAtaEnfrentandoImpactoDecorrenteCalamidadePublica()
        ]);

        if ($adesao->execucao_descentralizada_programa_projeto_federal) {
            $this->crud->addField(
                [
                    'name'      => 'execucao_descentralizada_programa_projeto_federal_label',
                    'type'      => 'label',
                    'label'     => 'A adesão é destinada à execução descentralizada de programa ou projeto federal'.
                        ' com recursos financeiros provenientes de transferências voluntárias da União'.
                        ' para Estados, municípios e Entidades da administração pública federal'.
                        ' integrantes dos Orçamentos Fiscal e da Seguridade Social da União e a '.
                        ' Organizações da Sociedade Civil (OSC)?',
                    'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                    'value'     =>  $adesao->getExecucaoDescentralizadaProgramaProjetoFederal(),
                    'tab' => $nomeGuia
                ]
            );

            $this->crud->addField([
                'name' => 'execucao_descentralizada_programa_projeto_federal',
                'type'  => 'hidden',
                'default' => $adesao->execucao_descentralizada_programa_projeto_federal
            ]);
        }

        if ($adesao->aquisicao_emergencial_medicamento_material) {
            $this->crud->addField(
                [
                    'name'      => 'aquisicao_emergencial_medicamento_material_label',
                    'type'      => 'label',
                    'label'     => 'Aquisição emergencial de medicamentos e material de consumo médico-hospitalar?',
                    'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                    'value'     =>  $adesao->getAquisicaoEmergencialMedicamentoMaterial(),
                    'tab' => $nomeGuia
                ]
            );

            $this->crud->addField([
                'name' => 'aquisicao_emergencial_medicamento_material',
                'type'  => 'hidden',
                'default' => $adesao->aquisicao_emergencial_medicamento_material
            ]);
        }

        $this->areaExcecao($nomeGuia, true, $adesao);

        # Monta as colunas da tabela para exibir os itens
        $column = $this->adesaoService->camposTabelaItemAdesao();
        $this->addTableCustom(
            $column,
            'table_selecionar_item_adesao_arp',
            'Selecionar o fornecedor para exibir a informação',
            'col-md-12 pt-5',
            'areaItemAdesao',
            $nomeGuia,
            90,
            $jsonItem
        );
    }

    # Recuperar a máscara para a unidade que o usuário está logado na sessão
    private function configurarMascaraUnidade()
    {
        $unidadeConfiguracao = UnidadeConfiguracao::where("unidade_id", session('user_ug_id'))->first();
        $mascaraProcessoAdesao = null;
        $attributesProcesso['required'] = 'required';
        if (!empty($unidadeConfiguracao)) {
            $mascaraProcessoAdesao = str_replace("9", "0", $unidadeConfiguracao->padrao_processo_mascara);
            $attributesProcesso['data-mask'] = $mascaraProcessoAdesao;
        }

        return $attributesProcesso;
    }
    
    /**
     * Método responsável em montar os campos do item para adesão
     */
    private function areaItemAdesao($nomeGuia)
    {
        $this->addFieldSelect(
            'Unidade Gerenciadora',
            'unidade_gerenciadora_id',
            'unidades',
            'unidade_arp_adesao',
            'arp/adesao/unidade',
            ['class' => 'col-md-4', 'id' => 'unidade_gerenciadora_id'],
            $nomeGuia,
            false,
            false
        );

        $this->addFieldSelect(
            '<span class="label-text">Número da compra/Ano</span>',
            'compra_fake',
            'compra',
            'compra_arp_adesao',
            '',
            ['class' => 'col-md-4', 'id' => 'compra_fake'],
            $nomeGuia,
            true,
            false,
            null,
            'Informe obrigatoriamente a Unidade gerenciadora',
            'Informe obrigatoriamente a Unidade gerenciadora'
        );

        $customNoResults = 'Informe obrigatoriamente a Unidade gerenciadora e o Número da compra';
        $this->crud->addField([
            'name' => 'modalidade_fake',
            'label' => "Modalidade da compra",
            'type' => 'select2_from_array',
            'options' => [],
            'wrapperAttributes' => [
                'class' => 'col-md-4',
            ],
            'attributes' => ['id' => 'modalidade_fake'],
            'allows_null' => true,
            'tab' => $nomeGuia,
            'custom_no_results' => $customNoResults
        ]);
        $this->addFieldSelect(
            'Numero da ata/Ano',
            'arp_id',
            'Arp',
            'ata_arp_adesao',
            'arp/adesao/numeroAtaAdesao',
            ['class' => 'col-md-4 pt-3', 'id' => 'arp_id'],
            $nomeGuia,
            true,
            false,
            null,
            'Informe obrigatoriamente a Unidade gerenciadora',
            'Informe obrigatoriamente a Unidade gerenciadora'
        );

        $this->addFieldSelect(
            'Fornecedor',
            'fornecedor_id',
            'fornecedor',
            'fornecedor_arp_adesao',
            'arp/adesao/fornecedor',
            ['class' => 'col-md-4 pt-3', 'id' => 'fornecedor_id'],
            $nomeGuia,
            false,
            false
        );

        $this->crud->addField([
            'name' => 'unidade_origem_id_view_fake',
            'label' => 'Unidade solicitante',
            'type'  => 'text',
            'attributes' => ['readonly' => 'readonly'],
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'fake'     => true,
            'default' => session('user_ug') . "-" . session('user_ug_nome_resumido'),
            'tab' => $nomeGuia
        ]);

        CRUD::addField([

                'label'     => "Itens da ata",
                'type'      => 'select2_from_ajax',
                'name'      => 'arp_item', // the db column for the foreign key

                'entity'    => 'item',

                'model'     => "App\Models\ArpItem", // related model
                'attribute' => 'catmat_descricao', // foreign key attribute that is shown to user
                'data_source' => url("arp/adesao/itens"),
                'mensagemSelectError' =>  null,
                'mensagemSelect' =>  null,

                'wrapperAttributes' => ['class' => 'col-md-4 pt-3','id' => 'arp_item'],
                'tab' => $nomeGuia

        ]);

        $attributesProcesso['required'] = 'required';
        $maskAttributes = $this->getProcessoMask();
        if (!empty($maskAttributes)) {
            $attributesProcesso['data-mask'] = $maskAttributes;
        }

        $this->crud->addField([
            'name' => 'processo_adesao',
            'label' => 'Processo adesão',
            'type'  => 'text',
            'attributes' => array_merge($attributesProcesso, ['maxlength' => 255]),
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'tab' => $nomeGuia,
            'required' => true
        ]);
        
        $this->crud->addField(
            [   // radio
                'name'        => 'ata_enfrentando_impacto_decorrente_calamidade_publica', // the name of the db column
                'label'       => 'Mostrar atas registradas para enfrentamento dos
                impactos decorrentes do estado de calamidade pública?',
                'type'        => 'radio',
                'options'     => [
                    false => "Não",
                    true => "Sim"
                ],
                'default' => 0,
                'wrapperAttributes' => [ 'class' => 'col-md-4 pt-3'],
                // optional
                'inline'      => 'd-inline-block', // show the radios all on the same line?
                'tab' => $nomeGuia,
                'required' => true,
                'textTooltip' => 'Ao selecionar a opção "Sim" serão exibidas apenas as atas registradas com
                amparo legal decorrentes dos normativos de calamidade pública'
            ],
        );

        # Exibe essa área se a unidade estiver ligada ao orgão
        #  Ministério da saúde (36000)
        $this->areaExcecao($nomeGuia);

        $this->campoExecucaoDescentralizadaProgramaProjetoFederal($nomeGuia);

        # Responsável em exibir o botão para recuperar os itens
        $this->addAreaCustom(
            'btn_recuperar_compra_arp',
            'areaDadosCompra col-md-12',
            null,
            null,
            null,
            'btn_recuperar_compra_arp_adesao',
            'Buscar itens:<br>Somente serão exibidos itens de ata(s) de registro de preços vigentes,
            com quantidade disponível para adesão e que permitem adesão para a unidade solicitante.',
            $nomeGuia,
            'Buscar Itens'
        );

        $this->crud->limparAdesao = true; // btn para limpar todos os campos da adesao

        # Recupera as colunas para montar os itens para adesão
        $column = $this->adesaoService->camposTabelaItemAdesao();

        $this->crud->addField(
            [
                'name'  => 'table_ajax_item_adesao_arp',
                'type'  => 'table_ajax_item_adesao_arp',
                'idTable' => 'table_selecionar_item_adesao_arp',
                'emptyTable' => 'Selecionar o fornecedor para exibir a informação',
                'classArea' => 'col-md-12 pt-5',
                'idArea' => 'areaItemAdesao',
                'column' => $column,
                'tab' => $nomeGuia,
                'pageLength' => 50,
                'urlAjax' => url("arp/adesao/buscaritem"),
                'label_instrucao_tabela' => 'Preencha a quantidade para solicitar adesão para o item'
            ]
        );
    }

    /**
     * Método responsável em exibir os campos hidden na tela do usuário
     */
    private function camposHidden()
    {
        $this->crud->addField([
            'name' => 'unidade_origem_id',
            'type'  => 'hidden',
            'default' => session('user_ug_id'),
            'attributes' => ['id' => 'unidade_origem_id']
        ]);
        $this->crud->addField([
            'name' => 'status',
            'type'  => 'hidden',
            'default' => 1,
            'attributes' => ['id' => 'status']
        ]);

        $this->crud->addField([
            'name' => 'sequencial',
            'type'  => 'hidden',
            'attributes' => ['id' => 'sequencial']
        ]);

        $this->crud->addField([
            'name' => 'data_envio_analise',
            'type'  => 'hidden',
            'attributes' => ['id' => 'data_envio_analise']
        ]);


        $anoSequencial = Carbon::now()->format('Y');
        $this->crud->addField([
            'name' => 'ano',
            'type'  => 'hidden',
            'default' => $anoSequencial,
            'attributes' => ['id' => 'ano']
        ]);

        $this->crud->addField([
            'name' => 'rascunho',
            'type'  => 'hidden',
            'default' => 1,
            'attributes' => ['id' => 'rascunho']
        ]);

        $this->crud->addField([
            'name' => 'numero_compra',
            'type'  => 'hidden',
            'fake'     => true,
            'attributes' => ['id' => 'numero_compra']
        ]);
        $this->crud->addField([
            'name' => 'grupo_compra',
            'type'  => 'hidden',
            'fake'     => true,
            'attributes' => ['id' => 'grupo_compra']
        ]);

        $this->crud->addField([
            'name' => 'compra_id',
            'type'  => 'hidden',
            'attributes' => ['id' => 'compra_id'],
            'value' => 1
        ]);

        # Monta a URL para buscar os itens conforme os filtros aplicados
        $this->crud->addField([
            'name' => 'url_buscar_item_adesao',
            'type'  => 'hidden',
            'default' => url("arp/adesao/buscaritem"),
            'attributes' => ['id' => 'url_buscar_item_adesao'],
            'fake' => true
        ]);

        $this->crud->addField([
            'name' => 'responsavel_id',
            'type'  => 'hidden',
            'attributes' => ['id' => 'responsavel_id'],
            'value' => auth()->user()->id
        ]);

        # Monta a URL para verificar se a unidade gerenciadora pertence ao orgão Ministério da saúde (36000)
        $this->crud->addField([
            'name' => 'url_buscar_unidade_gerenciadora_excecao',
            'type'  => 'hidden',
            'default' => url("arp/adesao/unidadegerenciadoraexcecao"),
            'attributes' => ['id' => 'url_buscar_unidade_gerenciadora_excecao'],
            'fake' => true
        ]);

        $this->crud->addField([
            'name' => 'url_exibir_campo_execucao_descentralizada_programa_projeto_federal',
            'type'  => 'hidden',
            'default' => url("arp/adesao/exibircampoexecucaodescentralizadaprogramaprojetofederal"),
            'attributes' => ['id' => 'url_exibir_campo_execucao_descentralizada_programa_projeto_federal'],
            'fake' => true
        ]);
    }

    /**
     * Método responsável em montar a área de anexo para o usuário
     */
    private function areaAnexo(string $nomeGuia, Adesao $adesao = null)
    {
        $attributesProcesso['required'] = 'required';
        $maskAttributes = $this->getProcessoMask();
        if (!empty($maskAttributes)) {
            $attributesProcesso['data-mask'] = $maskAttributes;
        }

        $this->crud->addField([
            'name' => 'processo_adesao',
            'label' => 'Processo adesão',
            'type'  => 'text',
            'attributes' => array_merge($attributesProcesso, ['maxlength' => 255]),
            'wrapperAttributes' => ['class' => 'col-md-6 pt-3'],
            'tab' => $nomeGuia,
            'required' => true
        ]);
        $this->crud->addField([
            'name' => 'processo_adesao_fak',
            'type'  => 'hidden',
            'attributes' => ['id' => 'processo_adesao_fak'],
            'wrapperAttributes' => ['class' => 'col-md-6 pt-3'],
            'tab' => $nomeGuia,
        ]);

        $this->crud->addField(
            [
                'name'      => 'texto_justificativa', // The db column name
                'label'     => 'Justificativa da vantagem da adesão', // Table column heading
                'attributes' => ['required' => 'required'],
                'wrapperAttributes' => [
                    'class' => 'col-md-6 br-textarea pt-3'
                ],
                'escaped' => false,
                'tab' => $nomeGuia,
                'type' => 'textarea',
                'required' => true
            ]
        );

        $this->crud->addField(
            [   // Upload
                'name'      => 'justificativa',
                'label'     => 'Anexo justificativa',
                'type'      => 'upload_custom',
                'upload'    => true,
                'disk'      => 'local',
                'accept'    => 'application/pdf',
                'attributes' => ['id' => 'justificativa'],
                'wrapperAttributes' => ['class' => 'col-md-6 pt-3'],
                'tab' => $nomeGuia,
                'required' => true
            ]
        );

        $this->crud->addField(
            [   // radio
                'name'        => 'demonstracao_valores_registrados', // the name of the db column
                'label'       => 'Foi realizada demonstração de que os valores registrados estão compatíveis com
                                        os valores praticados pelo mercado, nos termos da Lei 14.133/2021
                                        (Art.23 e Art. 86, §2º, inc. II)?', // the input label
                'type'        => 'radio',
                'options'     => [
                    // the key will be stored in the db, the value will be shown as label;
                    0 => "Não",
                    1 => "Sim"
                ],
                'attributes' => ['required' => 'required'],

                'wrapperAttributes' => ['class' => 'col-md-6 pt-3'],
                // optional
                'inline'      => 'd-inline-block', // show the radios all on the same line?
                'tab' => $nomeGuia,
                'required' => true
            ]
        );

        $this->crud->addField(
            [   // Upload
                'name'      => 'demonstracao',
                'label'     => 'Anexo demonstração',
                'type'      => 'upload_multiple_custom',
                'upload'    => true,
                'disk'      => 'local',
                'accept'    => 'application/pdf',
                'attributes' => ['id' => 'demonstracao'],
                'wrapperAttributes' => ['class' => 'col-md-6 pt-3'],
                'tab' => $nomeGuia,
                'required' => true,
            ]
        );

        $this->crud->addField(
            [   // radio
                'name'        => 'consulta_aceitacao_fornecedor', // the name of the db column
                'label'       => 'Houve prévia consulta e aceitação do fornecedor,
                                    nos termos da  Lei 14.133/2021 '. '<br>' .'(Art. 86, §2º, inc. III)?',
                'type'        => 'radio',
                'options'     => [
                    // the key will be stored in the db, the value will be shown as label;
                    0 => "Não",
                    1 => "Sim"
                ],
                'default' => 0,
                'attributes' => [
                    'required' => 'required',
                    'id' =>     'consulta_aceitacao_fornecedor'
                ],
                'wrapperAttributes' => ['class' => 'col-md-6 pt-3'],
                // optional
                'inline'      => 'd-inline-block', // show the radios all on the same line?
                'tab' => $nomeGuia,
                'required' => true
            ],
        );

        $this->crud->addField(
            [   // Upload
                'name'      => 'aceitacao',
                'label'     => 'Anexo aceitação',
                'type'      => 'upload_multiple_custom',
                'upload'    => true,
                'attributes' => ['id' => 'aceitacao'],
                'disk'      => 'local',
                'accept'    => 'application/pdf',
                'wrapperAttributes' => ['class' => 'col-md-6 pt-3'],
                'tab' => $nomeGuia,
                'required' => true
            ]
        );

        $this->crud->addField([
            'name'      => 'texto_justificativa_item_isolado',
            'label'     => 'Justificativa de item isolado pertencente a um grupo',
            'wrapperAttributes' => [
                'class' => 'col-md-6 br-textarea pt-3 texto-justificativa-item-isolado'
            ],
            'escaped' => false,
            'tab' => $nomeGuia,
            'type' => 'textarea',
            'required' => true
        ]);

        $this->crud->addField([
            'name'      => 'justificativa_item_isolado',
            'label'     => 'Anexo justificativa item isolado',
            'type'      => 'upload_multiple_custom',
            'upload'    => true,
            'attributes' => ['id' => 'item_isolado'],
            'wrapperAttributes' => [
                'class' => 'col-md-6 pt-3 justificativa-item-isolado campo-upload'
            ],
            'disk'      => 'local',
            'accept'    => 'application/pdf',
            'tab' => $nomeGuia
        ]);

        if ($this->crud->getCurrentOperation() === 'create') {
            $this->camposExecucaoDescentralizadaProgramaProjetoFederal($nomeGuia);
        }

        if ($this->crud->getCurrentOperation() === 'update' &&
            !empty($adesao) &&
            $adesao->execucao_descentralizada_programa_projeto_federal) {
            $this->camposExecucaoDescentralizadaProgramaProjetoFederal($nomeGuia);
        }
    }

    /**
     * Método responsável em montar a área de exceção
     */
    private function areaExcecao(string $nomeGuia, bool $telaEdicao = false, $adesao = null)
    {
        $classOcultaCampo = 'invisible';
        $valorDefaultAquisicaoEmergencial = ['default' => 0];
        # Se for para a tela de rascunho
        if ($telaEdicao) {
            # Verifica se a unidade cadastrada da compra pertence ao orgão do Ministério da Saúde
            $unidadePertenceOrgaoMinisterioSaude = $this->unidadePertenceOrgao($adesao->compra->unidade_origem_id);

            # Se pertencer, retira a classe oculta para exibir o campo
            if ($unidadePertenceOrgaoMinisterioSaude) {
                $classOcultaCampo = '';
                $valorDefaultAquisicaoEmergencial = [];
            }
        }

        if (empty($this->crud->getCurrentEntryId())) {
            $this->crud->addField(
                [   // radio
                    'name'        => 'aquisicao_emergencial_medicamento_material', // the name of the db column
                    'label'       => 'Aquisição emergencial de medicamentos e material de consumo médico-hospitalar?',
                    'type'        => 'radio',
                    'options'     => [
                        // the key will be stored in the db, the value will be shown as label;
                        0 => "Não",
                        1 => "Sim"
                    ],
                    $valorDefaultAquisicaoEmergencial,
                    // 'default' => 0,
                    'wrapperAttributes' => [
                        'class' => 'col-md-4 pt-3 ' . $classOcultaCampo,
                        'id' => 'areaAquisicaoEmergencialMedicamento'
                    ],
                    'attributes' => [
                        'onchange' => 'analisarCampoQuantidadeExcecao(this)',
                        'data-excecao' => 'aquisicao_emergencial_medicamento_material'
                    ],
                    // optional
                    'inline'      => 'd-inline-block', // show the radios all on the same line?
                    'tab' => $nomeGuia,
                ],
            );
        }
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');
        $this->importarDatatableForm();
        $arquivoJS = [
            'assets/js/admin/forms/arp_adesao.js',
            'assets/js/admin/forms/arp_adesao_cancelamento.js',
            'assets/js/admin/forms/arp_common.js',
            'assets/js/blockui/jquery.blockUI.js',
            'packages/backpack/base/js/padrao_gov/jquery.mask.js'
        ];

        $this->importarScriptJs($arquivoJS);

        CRUD::setValidation(AdesaoRequest::class);

        $this->areaItemAdesao('Item(ns) para adesão');

        $this->camposHidden();

        $this->areaAnexo('Justificativa e anexos');

        $this->botoesAcao();
    }

    /**
     * Método responsável em exibir os botões para o usário
     */
    private function botoesAcao()
    {
        $this->crud->button_guia = true;

        $this->crud->button_custom = [
            [
                'button_text' => 'Salvar rascunho', 'button_id' => 'rascunho',
                'button_name_action' => 'rascunho', 'button_value_action' => '1',
                'button_icon' => 'fas fa-edit', 'button_tipo' => 'secondary'
            ],
            [
                'button_text' => 'Solicitar adesão', 'button_id' => 'adicionar',
                'button_name_action' => 'rascunho', 'button_value_action' => '0',
                'button_icon' => 'fas fa-save', 'button_tipo' => 'primary',
                'style' => 'display: inline-block;'
            ],
            [
                'button_text' => 'Enviar para o Fornecedor',
                'button_id' => 'enviar_fornecedor',
                'button_name_action' => 'enviar_fornecedor',
                'button_value_action' => '1',
                'button_icon' => 'fas fa-envelope',
                'button_tipo' => 'primary',
                'style' => 'display: none;'

            ]
        ];
        if (config('app.app_amb') == 'Ambiente Produção') {
            unset($this->crud->button_custom[2]);
        }
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    private function consultaGrupoCompraEdit(int $compraId, array $grupoCompraArray)
    {
        $resultGrupo = DB::table('compra_items')
            ->where('compra_id', $compraId)
            ->whereIn('grupo_compra', $grupoCompraArray)
            ->select(DB::raw('count(grupo_compra) as count, grupo_compra'))
            ->groupBy('grupo_compra')
            ->pluck('count', 'grupo_compra')
            ->toArray();

        return $resultGrupo;
    }
    
    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');
        $this->importarDatatableForm();
        $arquivoJS = [
            'assets/js/admin/forms/arp_adesao.js',
            'assets/js/admin/forms/arp_adesao_cancelamento.js',
            'assets/js/admin/forms/arp_common.js',
            'assets/js/blockui/jquery.blockUI.js',
            'packages/backpack/base/js/padrao_gov/jquery.mask.js'
        ];

        $this->importarScriptJs($arquivoJS);

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Atualizar Adesão',
        ]);

        $adesao = $this->crud->getCurrentEntry();

        # Recuperar os itens para a edição
        $itemDatatable = $this->queryBuscaItem(
            null,
            null,
            null,
            null,
            null,
            null,
            $adesao->execucao_descentralizada_programa_projeto_federal,
            $adesao->aquisicao_emergencial_medicamento_material,
            $this->crud->getCurrentEntryId(),
            null,
            null,
            null,
            $adesao->ata_enfrentando_impacto_decorrente_calamidade_publica
        );

        $grupoCompraArray = collect($itemDatatable)->pluck('grupo_compra')->countBy()->toArray();
        $grupoKey = array_keys($grupoCompraArray);
        $compraId = $adesao->compra_id;
        $grupoCompraEditArray = $this->consultaGrupoCompraEdit($compraId, $grupoKey);

        $itemFormatado = [];
        foreach ($itemDatatable['data'] as $todosItens) {
            foreach ($adesao->item as $itemSelecionado) {
                // Montar as informações preenchidas pelo usuário
                if ($itemSelecionado['item_arp_fornecedor_id'] == $todosItens['id']) {
                    $item['valorunitariocalculo'] = $todosItens['valorunitario'];
                    $todosItens = $this->montarCampoQuantidadeLancamento(
                        $todosItens,
                        $todosItens['max'],
                        $todosItens['step'],
                        #$this->formatValuePtBR($itemSelecionado['quantidade_solicitada'], 0)
                        $itemSelecionado['quantidade_solicitada']
                    );
                }
            }
            $itemFormatado[] = $todosItens;
        }

        foreach ($grupoCompraEditArray as $key => $grupoCompra) {
            $itemFormatado[count($itemFormatado) - 1]['valornegociado'] .=
                '<input
                type="hidden"
                name="count_grupo_' . $key . '"
                value="' . $grupoCompra . '"
                id="count_grupo_' . $key . '">';
        }

        # Montar a área de item da solicitação
        $this->areaItemAdesaoUpdate($itemFormatado, $adesao);

        # Método responsável em enviar todos os fields hidden para o FORM
        $this->camposHidden();

        $this->crud->addField([
            'name' => 'compra_id',
            'type'  => 'hidden',
            'attributes' => ['id' => 'compra_id'],
            'value' => $adesao->compra->id
        ]);
        $this->crud->addField([
            'name' => 'anexo_justificativa_adicionado',
            'type' => 'hidden',
            'attributes' => ['id' => 'anexo_justificativa_adicionado'],
        ]);

        # Exibe a guia de justificativa e anexos
        $this->areaAnexo('Justificativa e anexos', $adesao);

        $this->botoesAcao();

        $this->crud->addField([
            'name' => 'qtd_justificativa',
            'type'  => 'hidden',
            'attributes' => ['id' => 'qtd_justificativa'],
            'value' => strlen($adesao->justificativa)
        ]);

        $this->crud->addField([
            'name' => 'qtd_demonstracao',
            'type'  => 'hidden',
            'attributes' => ['id' => 'qtd_demonstracao'],
            'value' => count($adesao->demonstracao)
        ]);

        $this->crud->addField([
            'name' => 'qtd_aceitacao',
            'type'  => 'hidden',
            'attributes' => ['id' => 'qtd_aceitacao'],
            'value' => count($adesao->aceitacao)
        ]);
    }

    /**
     * Método responsável em exibir a unidade de origem  com base no filtro
     */
    public function unidade(Request $request)
    {
        $search_term = $request->input('q');

        if ($search_term) {
            $arpRepository = new ArpRepositoy();
            return $arpRepository->montarSelectUnidadeOrigemArp($search_term);
        }
        
        return [];
    }

    /**
     * Método responsável em exibir a compra com base no filtro
     */
    public function compra(Request $request)
    {
        $search_term = $request->input('q');
        $unidade_id = Route::current()->parameter("unidade_id");

        if ($search_term) {
            $arpAdesaoRepository = new ArpAdesaoRepository();

            return $arpAdesaoRepository->montarSelectNumeroCompraAdesao($search_term, $unidade_id);
        }

        return [];
    }

    /**
     * Método responsável em exibir a modalidade com base no filtro
     */
    public function modalidade(Request $request)
    {
        // $search_term = $request->input('q');
        $unidade_id = Route::current()->parameter("unidade_id");
        $numero_compra = Route::current()->parameter("numero_compra");
        $numero_compra = str_replace("-", "/", $numero_compra);

        $arpAdesaoRepository = new ArpAdesaoRepository();
        return $arpAdesaoRepository->montarSelectModalidadeCompraAdesao($unidade_id, $numero_compra);
    }

    /**
     * Método responsável em recuperar o número da ata com base no filtro
     * BKP - REGRA MUDADA PARA DEPENDÊNCIA DA UNIDADE GERENCIADORA
     */
//    public function numeroAtaAdesao(Request $request)
//    {
//        $search_term = $request->input('q');
//        $arpAdesaoRepository = new ArpAdesaoRepository();
//
//        return $arpAdesaoRepository->montarSelectNumeroAtaAdesao(
//            $search_term,
//            Route::current()->parameter("unidade_id"),
//            Route::current()->parameter("numero_compra"),
//            Route::current()->parameter("modalidade")
//        );
//
//
//    }

    public function numeroAtaAdesao(Request $request)
    {
        $search_term = $request->input('q');
        $unidade_id = Route::current()->parameter("unidade_id");

        if ($search_term) {
            $arpAdesaoRepository = new ArpAdesaoRepository();

            return $arpAdesaoRepository->montarSelectNumeroAtaAdesao(
                $search_term,
                $unidade_id,
                Route::current()->parameter("numero_compra"),
                Route::current()->parameter("modalidade")
            );
        }

        return [];
    }

    /**
     * Método responsável em retornar o fornecedor com base nos filtros
     */
    public function fornecedor(Request $request)
    {
        $search_term = $request->input('q');

        $arpAdesaoRepository = new ArpAdesaoRepository();

        return $arpAdesaoRepository->montarSelectFornecedorAdesao(
            $search_term,
            Route::current()->parameter("unidade_id"),
            Route::current()->parameter("numero_compra"),
            Route::current()->parameter("modalidade")
        );
    }

    public function itensAtaAdesao(Request $request)
    {
        $search_term = $request->input('q');
        $unidade_id = Route::current()->parameter("unidade_id");
        $numero_compra = Route::current()->parameter("numero_compra");
        $modalidade = Route::current()->parameter("modalidade");

        if ($search_term) {
            $arpAdesaoRepository = new ArpAdesaoRepository();

            return $arpAdesaoRepository->itensAdesao(
                $modalidade,
                $unidade_id,
                $numero_compra,
                null,
                null,
                null,
                null,
                null,
                null,
                session('user_ug_id'),
                $search_term
            );
        }
    }


    /**
     * Método responsável em recuperar os itens da ata com base
     * no filtro do número do item e id da ata
     */
    private function queryItemAprovado(array $filtro, bool $totalQuantidadeAprovada = true)
    {
        $arpAdesaoRepository = new ArpAdesaoRepository();
        return $arpAdesaoRepository->recuperarItemAprovadoAdesao($filtro, $totalQuantidadeAprovada);
    }

    # Recupera os itens aprovados para o usuário
    private function queryBuscaItem(
        ?int $modadelidadeId = null,
        ?int $unidadeOrigemId = null,
        ?string $numeroCompra = null,
        ?int $fornecedorId = null,
        ?string $numeroAta = null,
        ?string $itensAtaFiltro = null,
        ?bool $execucaoDescentralizadaProgramaProjetoFederal = null,
        ?bool $aquisicaoEmergencialMedicamentoMaterial = null,
        ?int $idSolicitacao = null,
        string $search = null,
        ?int $start = null,
        ?int $length = null,
        bool $ataEnfrentandoImpactoDecorrenteCalamidadePublica = false,
        array $orderBy = null
    ) {
        $arpAdesaoRepository = new ArpAdesaoRepository();

        # Recuperar os itens para que o usuário selecione
        $itens = $arpAdesaoRepository->recuperarItemSolicitarAdesao(
            $modadelidadeId,
            $unidadeOrigemId,
            $numeroCompra,
            $fornecedorId,
            $numeroAta,
            $itensAtaFiltro,
            $execucaoDescentralizadaProgramaProjetoFederal,
            $aquisicaoEmergencialMedicamentoMaterial,
            $idSolicitacao,
            session('user_ug_id'),
            $ataEnfrentandoImpactoDecorrenteCalamidadePublica
        );
        
        if (!empty($search)) {
            $itens->where(function ($query) use ($search) {
                $query->where('compra_items.descricaodetalhada', 'ilike', "%$search%")
                    ->orWhere('compra_items.numero', 'ilike', "%$search%")
                    ->orWhere('fornecedores.cpf_cnpj_idgener', 'ilike', "%$search%")
                    ->orWhere('fornecedores.nome', 'ilike', "%$search%");
            });
        }
        $totalLinhas = $itens->count();
        
        $itens = $itens->skip($start)->take($length);
        
        if (!empty($orderBy)) {
            $itens->orderByRaw("{$orderBy[0]['column']}  {$orderBy[0]['dir']}");
        }

        $itens = $itens->get();

        $itemDatatable = [];
        $step = 0;
        $countGrupoCompra = [];

        # Percorre os itens recuperados com base nos filtros

        foreach ($itens as $item) {
            $item = $item->toArray();
            # Recuperar o menor valor entre a quantidade máxima permitida do item com o dobro da
            # quantidade homologada para o fornecedor

            $item['maximoadesao'] = $this->adesaoService->calcularMaximoAdesaoPorUnidadeSolicitante(
                $item['fornecedor_id'],
                $item['maximo_adesao_inicial_item'],
                $item['quantidade_homologada_vencedor'],
                $arpAdesaoRepository,
                $item['compra_item_id'],
                session('user_ug_id'),
                $item['qtd_total'],
                $execucaoDescentralizadaProgramaProjetoFederal
            );

            $aquisicaoMedicamentoMaterialSelecionado =
                $this->adesaoService->aquisicaoMedicamentoMaterialSelecionado($aquisicaoEmergencialMedicamentoMaterial);
            $itemPodeSerSelecionadoExcecao =
                $this->adesaoService->usuarioPodeSelecionarItemExcecaoCarona(
                    $aquisicaoMedicamentoMaterialSelecionado,
                    $item['maximoadesao']
                );

            if (!$itemPodeSerSelecionadoExcecao) {
                continue;
            }

            # Caso seja aquisição de material hospitalar, então reduz somente da quantidade da unidade
            if ($aquisicaoMedicamentoMaterialSelecionado) {
                $item['maximoadesao'] = $this->adesaoService->calcularQuantidadeSaldoExcecao(
                    $item['maximo_adesao_inicial_item'],
                    $item['quantidade_homologada_vencedor'],
                    $arpAdesaoRepository,
                    $item['compra_item_id'],
                    session('user_ug_id')
                );

                if ($item['maximoadesao'] <= 0) {
                    continue;
                }
            }

            $countGrupoCompra["$item[grupo_compra]"] = isset($countGrupoCompra["$item[grupo_compra]"]) ?
                $countGrupoCompra["$item[grupo_compra]"] + 1 : 1;

            $item['descricaodetalhada'] = Str::limit(trim($item['descricaodetalhada']), 50, '
                                        <i class="fas fa-info-circle"
                                        title="' . $item['descricaodetalhada'] . '"></i>');

            # Se o tipo do item for material, aplica a regra de 50% do item e o usuário lança somente valor inteiro
            # o valor inicial sempre será o máximo de adesão salvo na tabela, sem nenhuma dedução
            $max =  $item['maximoadesao'];
            if ($item['descresitem'] == 'MATERIAL') {
                $step = 1;
                $max = ceil($max);
            }

            # Se for do tipo serviço, pode ser lançado valor decimal de até 5 dígitos
            if ($item['descresitem'] == 'SERVIÇO') {
                $step = 0.00001;
            }

            $item['step'] = $step;
            $item['max'] = $max;

            $item['quantidaderegistrada'] = number_format($item['quantidade_homologada_vencedor'], 4, ',', '.');

            $item['valorunitariocalculo'] = $item['valorunitario'];

            $item = $this->montarCampoQuantidadeLancamento($item, $item['max'], $item['step']);

            # Montar o checkbox quando o usuário digitar, ser marcado
            $item['botaoitem'] =
                '<input
                onclick="return false"
                id="chk_' . $item['id'] . '"
                type="checkbox"
                class="select-checkbox br-input"
                value="' . $item['id'] . '"
                data-unidade-id="' . $item['unidadeadesao'] . '"
                data-lei="' . $item['lei'] . '"
                data-compra-id="' . $item['compras_ids'] . '"
                data-numerocompras="' . $item['numerocompras'] . '"
                data-numero="' . $item['numero'] . '"
                data-id="' . $item['id'] . '"
                data-grupocompra="' . $item['grupo_compra'] . '"
                name="item_solicitacao_lancamento[]"
            >';

            $itemDatatable[] = $item;
        }

        foreach ($countGrupoCompra as $key => $grupoCompra) {
            $itemDatatable[count($itemDatatable) - 1]['valornegociado'] .=
                '<input
                type="hidden"
                name="count_grupo_' . $key . '"
                value="' . $grupoCompra . '"
                id="count_grupo_' . $key . '">';
        }

        return ['data' => $itemDatatable, 'totalLinhas' => $totalLinhas];
    }

    # Responsável por montar as linhas da tabela para os itens
    private function montarCampoQuantidadeLancamento(array $item, $max, $step, $value = null)
    {

        $valueInput = '';
        $checked = '';
        $valorTotalAdesao = 0;

        if ($value > 0) {
            $valueInput = $value;
            $checked = 'checked';
            $valorTotalAdesao = $valueInput * $item['valorunitariocalculo'];
        }

        $id = $item['id'];

        # Montar o checkbox do item conforme digitado
        $item['botaoitem'] =
            '<input
            onclick="return false;" id="chk_' . $item['id'] . '"
            type="checkbox"
            class="select-checkbox br-input"
            value="' . $item['id'] . '" ' . $checked . '
            data-unidade-id="' . $item['unidadeadesao'] . '"
            data-compra-id="' . $item['compras_ids'] . '"
            data-numero="' . $item['numero'] . '"
            data-numerocompras="' . $item['numerocompras'] . '"
            data-id="' . $item['id'] . '"
            data-grupocompra="' . $item['grupo_compra'] . '"
            name="item_solicitacao_lancamento[]"
        >';

        # Montar o campo da quantidade para o usuário digitar
        $item['quantidadesolicitada'] =
            "<div class='br-input'>
            <input
                type='number'
                class='quantidade_solicitada nao_fracionado'
                name='quantidade_solicitada[{$id}]'                
                id='quantidade_solicitada_{$id}'
                min = '0'
                step='{$step}'
                value='{$valueInput}'
                data-id='{$id}'
                data-maximocinquentaporcento='{$item['max']}'
                data-maximoadesao='{$item['maximoadesao']}'
                data-unidadeadesao='{$item['unidadeadesao']}'
                data-lei='{$item['lei']}'

                data-numerocompras='{$item['numerocompras']}'
                data-numero='{$item['numero']}'
                data-grupocompra ='{$item['grupo_compra']}'
                data-compra-id='{$item['compras_ids']}'
                data-compra-item-id='{$item['compra_item_id']}'
                data-compra-item-fornecedor-id='{$item['compra_item_fornecedor_id']}'
                data-fornecedor-id='{$item['fornecedor_id']}'
                data-quantidade-homologada-vencedor='{$item['quantidade_homologada_vencedor']}'
                onchange='quantidadeLancada(this)'
                style='width: 200px;'
            />
            <span style='display: none;' class='feedback warning' role='alert' id='feedback_{$id}'>
                <i class='fas fa-exclamation-triangle' aria-hidden='true'></i>Máximo {$max}
            </span>
            <input type='hidden' name='unidades_gerenciadoras[{$id}]' value='{$item['unidadeadesao']}'>
            <input type='hidden' name='compras_ids[{$id}]' value='{$item['compras_ids']}'>
          
        </div>";

        # Exibe o valor unitário formatado para o usuário
        $item['valorunitario'] =
            '<span id="valorunitario_' . $id . '">' .
            number_format($item['valorunitariocalculo'], 4, ',', '.') .
            '</span>';

        # Exibe o valor na coluna 'Valor total para Adesão'
        $item['valornegociado'] =
            '<span id="valornegociado_' . $id . '">R$ ' . number_format($valorTotalAdesao, 4, ',', '.') . '</span>';

        return $item;
    }

    /**
     * Método responsável em buscar os itens e exibir para o usuário
     * com base no filtro selecionado
     */
    public function buscarItem(Request $buscarItem)
    {
        $execucaoDescentralizadaProgramaProjetoFederal =
            $buscarItem->request->get("execucao_descentralizada_programa_projeto_federal");
        
        $aquisicaoEmergencialMedicamentoMaterial =
            $buscarItem->request->get("aquisicao_emergencial_medicamento_material");
        
        $search = $buscarItem->request->get('search')['value'];

        $itemDatatable = $this->queryBuscaItem(
            $buscarItem->request->get("modalidade_fake"),
            $buscarItem->request->get("unidade_gerenciadora_id"),
            $buscarItem->request->get("numero_compra"),
            $buscarItem->request->get("fornecedor_id"),
            $buscarItem->request->get("arp_id"),
            $buscarItem->request->get("arp_item"),
            $execucaoDescentralizadaProgramaProjetoFederal,
            $aquisicaoEmergencialMedicamentoMaterial,
            null,
            $search,
            $buscarItem->request->get('start'),
            $buscarItem->request->get('length'),
            $buscarItem->request->get('ata_enfrentando_impacto_decorrente_calamidade_publica'),
            $buscarItem->request->get('order')
        );
        
        $totalRegistros = $itemDatatable['totalLinhas'];
        
        $draw = $buscarItem->request->get('draw');
        
        return [
            'draw'            => (isset($draw) ? (int) $draw : 0),
            'recordsTotal'    => $totalRegistros,
            'recordsFiltered' => $totalRegistros,
            'data'            => $itemDatatable['data'],
        ];
    }

    /**
     * Método responsável em gerar o sequencial da adesão
     */
    private function gerarSequencial(array $data)
    {
        $arpAdesaoRepository = new ArpAdesaoRepository();
        return $arpAdesaoRepository->gerarSequencialArp($data);
    }

    /**
     * Método responsável em retornar o id do status da adesão
     */
    private function statusAdesao(bool $rascunho)
    {
        $descricaoCodigoItem = 'Enviada para aceitação';
        if ($rascunho) {
            $descricaoCodigoItem = 'Em elaboração';
        }

        return $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', $descricaoCodigoItem);
    }

    /**
     * Método responsável em inserir os itens que
     * foram solicitados adesão pelo usuário
     */
    private function inserirItemSolicitacao(array $data, int $idSolicitacao, $request)
    {
        # Verifica se existe valor lançado
        if (isset($data['quantidade_solicitada'])) {
            # Recupera o status para item não avaliado
            $statusIdItem = $this->retornaIdCodigoItem(
                'Tipo de Ata de Registro de Preços',
                'Item Não Avaliado'
            );
            $arpAdesaoItem = new ArpAdesaoItemRepository();
            $arrayRetorno = [];
            # Percorre os itens informados pelo usuário
            foreach ($data['quantidade_solicitada'] as $itemAta => $quantidade) {
                # Se a quantidade for vazia, pula a linha
                if (empty($quantidade)) {
                    continue;
                }

                $buscaCondicionalItens =  $arpAdesaoItem->getCondicional(
                    $idSolicitacao,
                    $itemAta,
                    $quantidade,
                    $statusIdItem
                );

                # Inserir o item na adesão
                $arpAdesaoItem->inserirItemAdesaoArp($buscaCondicionalItens);

                if ($request->enviar_fornecedor != 0) {
                    //
                    $dadosRetorno = $arpAdesaoItem->getCondicionalResult($buscaCondicionalItens);
                    $contratoFornecedor = $dadosRetorno->item_arp->item_fornecedor_compra->contratosFornecedor;
                    foreach ($contratoFornecedor as $contratos) {
                        foreach ($contratos->prepostos as $preposto) {
                            if (!empty($preposto->userCpf)) {
                                $preposto->userCpf->dadosRetorno = $dadosRetorno;
                                $arrayRetorno[$preposto->userCpf->id] = $preposto->userCpf;
                            }
                        }
                    }
                }
            }
            if (!empty($arrayRetorno)) {
                $email = [];
                $dadosRetornoEmail = null;
                foreach ($arrayRetorno as $user) {
                    $email[] = $user->email;
                    $dadosRetornoEmail = $user->dadosRetorno;
                    Notification::send($user, new NotificationFornecedor($user->dadosRetorno));
                }
                Mail::to($email)->send(new NotificarPrepostoAdesao($dadosRetornoEmail));
            }
        }
    }

    /**
     * Método responsável em inserir a data do envio da análise
     */
    private function incluirDataEnvioAnalise($request)
    {
        if (!$request->rascunho) {
            $request->merge(['data_envio_analise' => Carbon::now()->toDateTimeString()]);
        }
    }



    public function store(AdesaoRequest $request)
    {
        $data = $request->all();
        if ($data['rascunho'] == 1 && $data['enviar_fornecedor'] != 0) {
            $data['rascunho'] = 0;
            $request->request->set("rascunho", 0);
        }

        $idStatus = $this->adesaoService->retornarSituacaoAdesao($data['rascunho'], $data['enviar_fornecedor']);

        $request->request->set("status", $idStatus);

        if ($data['rascunho'] == 1 && $data['enviar_fornecedor'] != 0) {
            $data['rascunho'] = 0;
            $request->request->set("rascunho", 0);
        }


        $idStatus = $this->adesaoService->retornarSituacaoAdesao($data['rascunho'], $data['enviar_fornecedor']);

        $request->request->set("status", $idStatus);

        $sequencialSolicitacao = $this->gerarSequencial($data);
        $request->request->set("sequencial", $sequencialSolicitacao);

        DB::beginTransaction();
        try {
            $this->incluirDataEnvioAnalise($request);

            $this->storeCud($request, false);

            # Recupera o id da adesão
            $id = $this->crud->entry->id;

            # Insere os itens da solicitação
            $this->inserirItemSolicitacao($data, $id, $request);



            # Recupera o sequencial, cada situação gera um sequencial diferente
            $sequencialFormatado = $this->sequencialFormatado(
                $this->crud->entry->sequencial,
                $this->crud->entry->ano,
                $this->crud->entry->rascunho,
            );



            if (!$data['rascunho']) {
                $this->adesaoService->salvarHistorico($id, $idStatus);
            }

            DB::commit();

            $mensagem = 'Solicitação de adesão de nº ' . $sequencialFormatado . ' salva';

            if (!$data['rascunho']) {
                $mensagem =
                    'Solicitação de adesão de nº ' . $sequencialFormatado .
                    ' enviada para análise da unidade gerenciadora';
            }
            if ($data['enviar_fornecedor'] == 1) {
                $mensagem = 'Solicitação de anuência do pedido de adesão
                 nº ' . $sequencialFormatado . ' enviada para análise do fornecedor.';
            }
            Alert::success($mensagem)->flash();
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            \Alert::add('error', 'Erro ao gerar a solicitação!')->flash();
        }

        return redirect('arp/adesao')->with('alerts', Alert::all());
    }

    public function update(AdesaoRequest $request)
    {
        $data = $request->all();

        if ($data['rascunho'] == 1 && $data['enviar_fornecedor'] != 0) {
            $data['rascunho'] = 0;
            $request->request->set("rascunho", 0);
        }


        $idStatus = $this->adesaoService->retornarSituacaoAdesao($data['rascunho'], $data['enviar_fornecedor']);


        $request->request->set("status", $idStatus);

        if (!$data['rascunho']) {
            $sequencialSolicitacao = $this->gerarSequencial($data);
            $request->request->set("sequencial", $sequencialSolicitacao);
        }

        DB::beginTransaction();
        try {
            $this->incluirDataEnvioAnalise($request);
            # Atualiza o registro
            $this->updateCrud($request);

            # Remove todos os itens para incluir os novos
            AdesaoItem::where("arp_solicitacao_id", $data['id'])->forceDelete();
            # Insere os itens da solicitação
            $this->inserirItemSolicitacao($data, $data['id'], $request);


            if (!$data['rascunho']) {
                $this->adesaoService->salvarHistorico($data['id'], $idStatus);
            }
            DB::commit();
        } catch (Exception $ex) {
            Log::info($ex);
            DB::rollBack();
            Log::error($ex);
            \Alert::add('error', 'Erro ao gerar a solicitação!')->flash();
        }



        $mensagem = 'Solicitação de adesão enviada para análise da unidade gerenciadora';

        if ($data['enviar_fornecedor'] == 1) {
            $mensagem = 'Solicitação de anuência do pedido de adesão
                enviada para análise do fornecedor.';
        }
        Alert::success($mensagem)->flash();

        # Se o item for igual os itens de enviada para aceitação, retorna para a tela inicial da adesao
        $idAvalicao =  $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', 'Enviada para aceitação');
        if ($idAvalicao == $idStatus) {
            return redirect('/arp/adesao');
        }

        return redirect('arp/adesao')->with('alerts', Alert::all());
    }

    /**
     * Método responsável em verificar se a unidade gerenciadora selecionada
     * pelo usuário pertence ao orgão do Ministério da saúde (36000)
     */
    public function unidadeGerenciadoraExcecao(int $id_unidade_gerenciadora)
    {
        return $this->unidadePertenceOrgao($id_unidade_gerenciadora);
    }

    /**
     * Método responsável em retornar se a unidade gerenciadora pertence ao
     * orgão do Ministério da saúde (36000)
     */
    private function unidadePertenceOrgao(int $idUnidade)
    {
        $orgaoRepository = new OrgaoRepository();

        # Recupera se a unidade selecionada pertence ao orgão do Ministério da saúde (36000)
        return $orgaoRepository->unidadePertenceOrgao('36000', $idUnidade);
    }

    /**
     * Método responsável em excluir o registro
     */
    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        # Inclusão do método forceDelete para que seja removido fisicamente
        # no banco de dados
        return Adesao::find($id)->forceDelete();
    }
    
    public function cancelarAdesao(ArpAdesaoCancelamentoRequest $request)
    {
        $UsuarioId = auth()->user()->id;
        $UnidadeId = session('user_ug_id');
        $data = $request->all();
        $arpAdesaoService = new AdesaoService();
        
        return $arpAdesaoService->cancelarAdesao(
            $data['adesaoIdCancelamento'],
            $UsuarioId,
            $UnidadeId,
            $data['justificativaCancelamento']
        );
    }
    
    public function formatarInformacoesDatatable(
        Collection $itemDatatable,
        ?bool $execucaoDescentralizadaProgramaProjetoFederal,
        ?bool $aquisicaoEmergencialMedicamentoMaterial
    ) {
        $arrayLinhas = array();

        foreach ($itemDatatable as $row) {
            # Recuperar o menor valor entre a quantidade máxima permitida do item com o dobro da
            # quantidade homologada para o fornecedor
            $quantidadeMenorAnalise =
                min($row->maximo_adesao_inicial_item, $row->quantidade_homologada_vencedor * 2);
            
            $arpAdesaoRepository = new ArpAdesaoRepository();
            
            $queryBaseTotalQuantidadeAprovada =
                $arpAdesaoRepository->queryBaseTotalQuantidadeAprovada($row->compra_item_id, $row->fornecedor_id);
            
            
            $row->quantidade_total_aprovada_adesao = $queryBaseTotalQuantidadeAprovada->sum('quantidade_aprovada');
            
            $row->quantidade_total_aprovada_adesao_por_unidade = $queryBaseTotalQuantidadeAprovada->
            where('arp_solicitacao.unidade_origem_id', session('user_ug_id'))->sum('quantidade_aprovada');
            
            # Subtrai a menor quantidade entre a homologada vencedor com a quantidade aprovada do item na adesão
            $quantidadeSaldoFornecedorAceitoAdesao =
                $quantidadeMenorAnalise - $row->quantidade_total_aprovada_adesao;

            # Recuperar o menor valor da unidade logada para que pegue sempre 50% do máximo de adesão do item e a
            # quantidade que foi homologada para o fornecedor
            $quantidadeMenorUnidadeLogada =
                min($row->maximo_adesao_inicial_item * 0.5, $row->quantidade_homologada_vencedor * 0.5)
                - $row->quantidade_total_aprovada_adesao;
            
            # Recupera a menor quantidade entre os 50% permitidos para a unidade e o saldo que foi aprovado
            $quantidadeMinimaAnalise = min($quantidadeMenorUnidadeLogada, $quantidadeSaldoFornecedorAceitoAdesao);

            $aquisicaoMedicamentoMaterialSelecionado =
                ($execucaoDescentralizadaProgramaProjetoFederal || $aquisicaoEmergencialMedicamentoMaterial);
            
            # Caso seja aquisição de material hospitalar, então reduz somente da quantidade da unidade
            if ($aquisicaoMedicamentoMaterialSelecionado) {
                $quantidadeSaldoFornecedorAceitoAdesao =
                    ceil($quantidadeMenorUnidadeLogada - $row->quantidade_total_aprovada_adesao_por_unidade);
                
                $quantidadeMinimaAnalise =
                    min($quantidadeMenorUnidadeLogada, $quantidadeSaldoFornecedorAceitoAdesao);
            }
            
            # arrendodar para cima a quantidade
            $row->maximoadesao = ceil($quantidadeMinimaAnalise);
            
            # Se o tipo do item for material, aplica a regra de 50% do item e o usuário lança somente valor inteiro
            # o valor inicial sempre será o máximo de adesão salvo na tabela, sem nenhuma dedução
            $max =  $row->maximoadesao;
            if ($row->descresitem == 'MATERIAL') {
                $step = 1;
                $max = ceil($max);
            }
            
            # Se for do tipo serviço, pode ser lançado valor decimal de até 5 dígitos
            if ($row->descresitem == 'SERVIÇO') {
                $step = 0.00001;
            }
            
            $row->max = $row->maximoadesao;
            $row->step = $step;
            
            $row->valorunitariocalculo = $row->valorunitario;
            $row->quantidaderegistrada =
                number_format($row->quantidade_homologada_vencedor, 4, ',', '.');
            $arrayLinhas[] = (object)$row->toArray();
        }

        return $arrayLinhas;
    }

    public function validarQuantitativoSolicitacaoCarona(Request $request)
    {
        $data = $request->all();

        $unidadeSolicitanteId = session('user_ug_id');

        $itemSelecionado = CompraItem::find($data['compraItemId']);

        $arpAdesaoRepository = new ArpAdesaoRepository();

        $quantidadeHomologadaVencedorTodosItens = $itemSelecionado->qtd_total * 0.5;

        $maximoAdesao = $this->adesaoService->calcularMaximoAdesaoPorUnidadeSolicitante(
            $data['fornecedorId'],
            $itemSelecionado->maximo_adesao,
            $quantidadeHomologadaVencedorTodosItens,
            $arpAdesaoRepository,
            $data['compraItemId'],
            $unidadeSolicitanteId,
            $itemSelecionado->qtd_total,
            $data['unidadePertenceEsfera']
        );

        if (!is_numeric($data['quantidadeTotalItem'])) {
            $errorCode = $this->errorCode();
            $mensagem = $this->mensagemUsuario($errorCode);

            $exceptionCustomizado =  new Exception("A non-numeric value encountered");

            $this->inserirLogCustomize500($errorCode, $exceptionCustomizado);

            Log::error($data);

            return $this->montarRespostaAlertJs(
                500,
                $mensagem,
                "error"
            );
        }

        $maximoAdesao -= $data['quantidadeTotalItem'];

        $aquisicaoMedicamentoMaterialSelecionado =
            $this->adesaoService->aquisicaoMedicamentoMaterialSelecionado($data['aquisicaoEmergencialMedicamento']);
        
        $itemPodeSerSelecionadoExcecao =
            $this->adesaoService->usuarioPodeSelecionarItemExcecaoValidacaoCarona(
                $aquisicaoMedicamentoMaterialSelecionado,
                $maximoAdesao
            );

        if (!$itemPodeSerSelecionadoExcecao) {
            return $this->montarRespostaAlertJs(
                500,
                "O limite estabelecido pelo art. 32, I, do Decreto 11.462/2023 foi "
                ."atingido pela unidade.",
                "error"
            );
        }

        if ($aquisicaoMedicamentoMaterialSelecionado) {
            $maximoAdesao = $this->adesaoService->calcularQuantidadeSaldoExcecao(
                $itemSelecionado->maximo_adesao,
                $data['quantidadeHomologadaVencedor'],
                $arpAdesaoRepository,
                $data['compraItemFornecedorId'],
                $unidadeSolicitanteId
            );

            $maximoAdesao -= $data['quantidadeTotalItem'];

            if ($maximoAdesao < 0) {
                return $this->montarRespostaAlertJs(
                    500,
                    "O limite estabelecido pelo art. 32, I, do Decreto 11.462/2023 foi "
                    ."atingido pela unidade.",
                    "error"
                );
            }
        }

        return $this->montarRespostaAlertJs(
            200,
            '',
            "success"
        );
    }

    public function exibirCampoExecucaoDescentralizadaProgramaProjetoFederal(Request $request)
    {
        $retorno = $this->adesaoService->exibirCampoExecucaoDescentralizadaProgramaProjetoFederal(
            session('user_ug_id'),
            $request->unidade_gerenciadora_compra_id
        );

        return response()->json($retorno);
    }

    public function campoExecucaoDescentralizadaProgramaProjetoFederal(string $nomeGuia, bool $telaEdicao = false)
    {
        $classOcultaCampo = 'd-none';
        $valorDefaultAquisicaoEmergencial = ['default' => 0];
        # Se for para a tela de rascunho
//        if ($telaEdicao) {
//            # Verifica se a unidade cadastrada da compra pertence ao orgão do Ministério da Saúde
//            $unidadePertenceOrgaoMinisterioSaude = $this->unidadePertenceOrgao($adesao->compra->unidade_origem_id);
//
//            # Se pertencer, retira a classe oculta para exibir o campo
//            if ($unidadePertenceOrgaoMinisterioSaude) {
//                $classOcultaCampo = '';
//                $valorDefaultAquisicaoEmergencial = [];
//            }
//        }

        if (empty($this->crud->getCurrentEntryId())) {
            $this->crud->addField(
                [   // radio
                    'name'        => 'execucao_descentralizada_programa_projeto_federal',
                    'label'       => 'A adesão é destinada à execução descentralizada de programa ou projeto federal'.
                                     ' com recursos financeiros provenientes de transferências voluntárias da União'.
                                     ' para Estados, municípios e Entidades da administração pública federal'.
                                     ' integrantes dos Orçamentos Fiscal e da Seguridade Social da União e a'.
                                     ' Organizações da Sociedade Civil (OSC)?',
                    'type'        => 'radio',
                    'options'     => [
                        0 => "Não",
                        1 => "Sim"
                    ],
                    $valorDefaultAquisicaoEmergencial,
                    'wrapperAttributes' => [
                        'class' => 'col-md-4 pt-3 ' . $classOcultaCampo,
                        'id' => 'execucao_descentralizada_programa_projeto_federal'
                    ],
                    'attributes' => [
                        'onchange' => 'exibirCampoAnexoComprovacao(this)'
                    ],
                    'inline'      => 'd-inline-block', // show the radios all on the same line?
                    'tab' => $nomeGuia,
                    'required' => true
                ],
            );
        }
    }

    public function camposExecucaoDescentralizadaProgramaProjetoFederal(string $nomeGuia)
    {
        $this->crud->addField([
            'name'      => 'marcacao_usuario_execucao_descentralizada_programa_projeto_federal',
            'label'     => 'A adesão é destinada à execução descentralizada de programa ou projeto federal'.
                ' com recursos financeiros provenientes de transferências voluntárias da União'.
                ' para Estados, municípios e Entidades da administração pública federal'.
                ' integrantes dos Orçamentos Fiscal e da Seguridade Social da União e a '.
                ' Organizações da Sociedade Civil (OSC)?',
            'wrapperAttributes' => [
                'class' => 'col-md-11 pt-3 areaExecucaoDescentralizadaProgramaProjetoFederal',
                'id' => 'marcacao_usuario_execucao_descentralizada_programa_projeto_federal'],
            'value' => '',
            'tab' => $nomeGuia,
            'type' => 'label'
        ]);

        $this->crud->addField([
            'name'      => 'justificativa_anexo_comprovacao_execucao_descentralizada_progra',
            'label'     => 'Justificativa da comprovação da execução descentralizada de programa ou projeto federal',
            'wrapperAttributes' => [
                'class' => 'col-md-6 br-textarea pt-3 areaExecucaoDescentralizadaProgramaProjetoFederal',
                'id' => 'justificativa_anexo_comprovacao_execucao_descentralizada_progra'
            ],
            'escaped' => false,
            'tab' => $nomeGuia,
            'type' => 'textarea',
            'required' => true
        ]);

        $this->crud->addField(
            [
                'name'      => 'anexo_comprovacao_execucao_descentralizada_programa_projeto_fed',
                'label'     => 'Anexo Comprovação',
                'type'      => 'upload_custom',
                'upload'    => true,
                'disk'      => 'local',
                'accept'    => 'application/pdf',
                'wrapperAttributes' => ['class' => 'col-md-6 pt-3 areaExecucaoDescentralizadaProgramaProjetoFederal'],
                'tab' => $nomeGuia,
                'required' => true
            ]
        );
    }
}
