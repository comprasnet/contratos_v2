<?php

namespace App\Http\Controllers\Arp;

use App\Http\Requests\AnalisarAdesaoRequest;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\CommonFields;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Http\Traits\UgPrimariaTrait;
use App\Models\Adesao;
use App\Models\AdesaoItem;
use App\Models\ArpItem;
use App\Models\ArpItemHistorico;
use App\Models\CodigoItem;
use App\Models\CompraItemUnidade;
use App\Models\Compras;
use App\Repositories\Arp\ArpAdesaoItemRepository;
use App\Repositories\Arp\ArpAdesaoRepository;
use App\Repositories\Compra\CompraItemUnidadeRepository;
use App\Services\Arp\AdesaoService;
use App\Services\Arp\ArpItemHistoricoService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\Widget;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/**
 * Class AnalisarAdesaoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AnalisarAdesaoCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    use CommonFields;
    use BuscaCodigoItens;
    use ImportContent;
    use UgPrimariaTrait;
    use CompraTrait;
    
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    protected $mp;
    
    protected $adesaoService;
    
    protected $arpItemHistoricoService;
    
    public function __construct(AdesaoService $adesaoService, ArpItemHistoricoService $arpItemHistoricoService)
    {
        parent::__construct();
        $this->mp = config('arp.arp.resultado_lei');
        $this->adesaoService = $adesaoService;
        $this->arpItemHistoricoService = $arpItemHistoricoService;
    }
    public function setup()
    {
        if (!backpack_user()->can('gestaodeatas_V2_acessar')) {
            return abort(403);
        }


        CRUD::setModel(\App\Models\Adesao::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/arp/adesao/analisar');
        CRUD::addclause("where", "rascunho", false);
        CRUD::addclause("where", "unidade_gerenciadora_id", session('user_ug_id'));

        $this->exibirTituloPaginaMenu('Analisar solicitação de adesão', null, false);
        $this->bloquearBotaoPadrao($this->crud, ['create', 'delete']);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->camposListShow();
        $this->crud->removeButton('update');

        $this->crud->addButtonFromView('line', 'button_edit_custom', 'button_edit_custom', 'end');

        $this->crud->addButtonFromView(
            'line',
            'arp_cancelar_adesao_unidade_gerenciadora',
            'arp_cancelar_adesao_unidade_gerenciadora',
            'end'
        );
        $arquivoJS = [
            'assets/js/admin/forms/arp_adesao_cancelamento.js'
        ];

        $this->importarScriptJs($arquivoJS);

        $this->crud->text_button_redirect_create = 'analisar adesao';

        $this->crud->setListView('vendor.backpack.crud.arp.list-adesao');
    }

    private function camposListShow()
    {
        $this->addColumnModelFunction('numero_solicitacao', 'Nº Solicitação', 'getNumeroSolicitacao');
        $this->addColumnModelFunction('unidade_gerenciadora', 'Unidade Gerenciadora', 'getUnidadeGerenciadora');
        $this->addColumnModelFunction('unidade_solicitacao', 'Unidade Solicitante', 'getUnidadeSolicitante');
        $this->addColumnModelFunction('numero_compra_solicitacao', 'Número da compra', 'getNumeroCompra');
        $this->addColumnModelFunction('modalidade_solicitacao', 'Modalidade', 'getModalidade');
        $this->addColumnModelFunction('situacao_solicitacao', 'Situação', 'getSituacao');
    }


    protected function setupShowOperation()
    {
        $this->camposListShow();

        $this->addColumnModelFunction('numero_ata_solicitacao', 'Número da ata', 'getNumeroGera');
        $this->addColumnModelFunction('solicitante', 'Solicitante', 'getResponsavelAdesao');

        $idAdesao = Route::current()->parameter("id");
        $itens = AdesaoItem::getItemAnalisadoPorAdesao($idAdesao);

        # Recuperar informações da adesão para exibir no show
        $adesao = Adesao::find($idAdesao);

        $situacaoAdesao = $adesao->getSituacao();

        $arraySituacaoCancelada = ['Cancelada pela gerenciadora', 'Cancelada pelo solicitante'];
        if (in_array($situacaoAdesao, $arraySituacaoCancelada)) {
            $this->crud->addColumn('justificativa_cancelamento');
            $this->addColumnModelFunction(
                'usuario_cancelamento',
                'Usuário responsável pelo cancelamento',
                'getUsuarioCancelamentoAdesao'
            );
            $this->addColumnModelFunction(
                'unidade_usuario_cancelamento',
                'Unidade responsável pelo cancelamento',
                'getUnidadeCancelamentoAdesao'
            );
            $this->addColumnDateHour(
                false,
                true,
                true,
                true,
                'data_cancelamento',
                'Data do cancelamento'
            );
        }

        $arraySaida = [];
        if (!empty($adesao->justificativa)) {
            $arrayArquivo = explode('-', $adesao->justificativa);
            $arraySaida['Nome'][0] = str_replace("_", " ", $arrayArquivo[1]);
            $arraySaida['Visualizar'][0] =
                '<a target="_blank" href="' . url("storage/$arrayArquivo[0]") . '"><i class="fas fa-eye"></i></a>';
        }


        $this->addColumnTable('justificativa_adesao', 'Anexo Justificativa', $arraySaida);

        $demonstracao = Adesao::getDemonstracao($idAdesao);
        $this->addColumnTable('demonstracao_adesao', 'Anexo Demonstração', $demonstracao);

        $anuenciaFornecedor = Adesao::getAnuenciaFornecedor($idAdesao);
        $aceitacao = Adesao::getAceitacao($idAdesao);
        if ($anuenciaFornecedor == null) {
            $this->addColumnTable('aceitacao_adesao', 'Anexo Aceitação', $aceitacao);
        } else {
            $this->addColumnTable(
                'anuencia_fornecedor',
                'Anexo Anuência Fornecedor',
                Adesao::getArrayAnuenciaFornecedor($idAdesao)
            );
        }

        if (!empty($adesao->justificativa_item_isolado)) {
            $justItemIsolado = Adesao::getItemIsolado($idAdesao);
            $this->addColumnTable(
                'aceitacao_justificativa_isolado',
                'Anexo justificativa item isolado',
                $justItemIsolado
            );
        }

        $this->addColumnTable('demonstracao_adesao', 'Anexo Demonstração', $demonstracao);
        if ($adesao->execucao_descentralizada_programa_projeto_federal !== null) {
            $this->addColumnModelFunction(
                'execucao_descentralizada_programa_projeto_federal',
                'A adesão é destinada à execução descentralizada de programa ou projeto federal'.
                ' com recursos financeiros provenientes de transferências voluntárias da União'.
                ' para Estados, municípios e Entidades da administração pública federal'.
                ' integrantes dos Orçamentos Fiscal e da Seguridade Social da União e a '.
                ' Organizações da Sociedade Civil (OSC)?',
                'getExecucaoDescentralizadaProgramaProjetoFederal'
            );

            if ($adesao->execucao_descentralizada_programa_projeto_federal) {
                $arrayArquivo =
                    explode('-', $adesao->anexo_comprovacao_execucao_descentralizada_programa_projeto_fed);
                $arraySaida['Nome'][0] = str_replace("_", " ", $arrayArquivo[1]);
                $arraySaida['Visualizar'][0] =
                    '<a target="_blank" href="' . url("storage/$arrayArquivo[0]") . '"><i class="fas fa-eye"></i></a>';

                $this->addColumnTable(
                    'anexo_comprovacao_execucao_descentralizada_programa_projeto_fed',
                    'Anexo comprovação',
                    $arraySaida
                );

                $this->crud->addColumns([
                    [
                        'name' => 'justificativa_anexo_comprovacao_execucao_descentralizada_progra',
                        'label' =>
                            'Justificativa da comprovação da execução descentralizada de programa ou projeto federal',
                        'type' => 'text',
                        'limit' => 9999,
                    ]
                ]);

                $this->addColumnModelFunction(
                    'ciente_analise_entre_projeto_federal',
                    'A unidade solicitante afirmou que se trata de uma execução descentralizada de programa ou'
                        .' projeto federal com recursos financeiros provenientes de transferências voluntárias da União'
                        .' para Estados, municípios e Entidades da administração pública federal integrantes dos'
                        .' Orçamentos Fiscal e da Seguridade Social da União e a Organizações da Sociedade Civil (OSC),'
                        .'conforme inciso I, § 2ºdo Art. 32 do Decreto nº 11.462 de 31/03/2023 e que a aceitação dessa'
                        .'solicitação de adesão não levará em consideração o saldo máximo de adesão do artigo citado',
                    'getCienteAnaliseEntreProjetoFederal'
                );
            }
        }

        if ($adesao->aquisicao_emergencial_medicamento_material !== null) {
            $this->addColumnModelFunction(
                'aquisicao_emergencial_medicamento_material',
                'Aquisição emergencial de medicamentos e material de consumo médico-hospitalar?',
                'getAquisicaoEmergencialMedicamentoMaterial'
            );
        }
        
        $this->addColumnModelFunction(
            'ata_enfrentando_impacto_decorrente_calamidade_publica',
            'Mostrar atas registradas para enfrentamento dos
                impactos decorrentes do estado de calamidade pública?',
            'getAtaEnfrentandoImpactoDecorrenteCalamidadePublica'
        );

        if ($adesao->aquisicao_emergencial_medicamento_material !== null) {
            $this->addColumnModelFunction(
                'aquisicao_emergencial_medicamento_material',
                'Aquisição emergencial de medicamentos e material de consumo médico-hospitalar?',
                'getAquisicaoEmergencialMedicamentoMaterial'
            );
        }

        $this->addColumnDateHour(
            false,
            true,
            true,
            true,
            'data_aprovacao_analise',
            'Data aprovação análise'
        );

        $this->addColumnTable('itens_adesao', 'Itens para adesão', $itens);
        
        # Bloqueia as funções abaixo se a adesão for ativa
        if (!$adesao->rascunho) {
            $this->crud->denyAccess(['update', 'delete']);
        }
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Criar AnalisarAdesao',
        ]);
    }

    /**
     * Função responsável em converter o retorno do campo para a saída textual
     */
    private function converterBoleanTexto(?bool $opcao)
    {
        if ($opcao === null) {
            return 'Opção não selecionada';
        }

        if ($opcao) {
            return 'Sim';
        }

        return 'Não';
    }

    /**
     * Responsável em montar a seleção do status e a quantidade que o usuário pode informar
     */
    private function estruturaCampoQuantidadeAprovada(bool $somenteAceitarParcial = false, float $maximoAdesao = 0)
    {
        $opcoesExibir = ['Aceitar', 'Negar', 'Aceitar Parcialmente'];


        if ($somenteAceitarParcial) {
            $opcoesExibir = ['Negar', 'Aceitar Parcialmente'];
        }

        if ($maximoAdesao == 0) {
            $opcoesExibir = ['Negar'];
        }

        # Recupera os status para a análise da adesão
        $codigo_item = CodigoItem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Ata de Registro de Preços')
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->whereIn('descricao', $opcoesExibir)
            ->orderByRaw("descricao ='Aceitar Parcialmente', descricao = 'Negar', descricao = 'Aceitar'")
            ->pluck('id', 'descricao')
            ->toArray();
        $campo = '<div class="br-input">';

        foreach ($codigo_item as $label => $id) {
            $idCampo = "status_id[:id]_{$id}";
            $campo .= '<div class="d-inline-block mr-5 pt-2">
                            <div class="br-radio">
                                <input
                                id="' . $idCampo . '" 
                                type="radio" 
                                name="status_id[:id]" 
                                value="' . $id . '" :check_' . $id . '
                                onchange="alterarEstadoJustificativa(`' . $label . '`, `:justificativaMotivacao`, :id)"
                                style="width: 25px;height: 20px;"/>
                                <label class="analisar_adesao_radio" for="' . $idCampo . '"></label>
                                <label>' . $label . '</label>
                            </div>
                            <style> 
                                label.analisar_adesao_radio::before{margin-top: 5px;}
                                label.analisar_adesao_radio::after{margin-top: 5px;}
                            </style>
                      </div>';
        }

        $campo .= '<input
                    type="number"
                    name="quantidade_aprovada[:id]"
                    class="nao_fracionado"
                    min = ":min"
                    max=":max"
                    step=":step"
                    value=":value"
                    style="width: 200px;"
                    :required
                    readonly/>
                </div>';

        return $campo;
    }

    /**
     * Método responsável em recuperar os itens ou o total da quantidade aprovada do item
     */
    private function queryItemAprovado(int $itemId, string $campoFiltro, bool $totalQuantidadeAprovada = true)
    {
        $totalItemAprovado = Adesao::join("codigoitens AS ci1", "arp_solicitacao.status", "=", "ci1.id")
            ->join("codigos AS c1", "ci1.codigo_id", "=", "c1.id")
            ->join(
                "arp_solicitacao_item",
                "arp_solicitacao.id",
                "=",
                "arp_solicitacao_item.arp_solicitacao_id"
            )
            ->join("codigoitens", "arp_solicitacao_item.status_id", "=", "codigoitens.id")
            ->join("codigos", "codigoitens.codigo_id", "=", "codigos.id")
            ->join("arp_item", "arp_item.id", "=", "arp_solicitacao_item.item_arp_fornecedor_id")
            ->join(
                "compra_item_fornecedor",
                "arp_item.compra_item_fornecedor_id",
                "=",
                "compra_item_fornecedor.id"
            )
            ->whereiN("ci1.descricao", ["Aceita", "Aceito Parcial"])
            ->where("c1.descricao", "Tipo de Ata de Registro de Preços")
            ->where($campoFiltro, $itemId)
            ->whereIN("codigoitens.descricao", ["Aceitar", "Aceitar Parcialmente"])
            ->where("codigos.descricao", "Tipo de Ata de Registro de Preços");

        if ($totalQuantidadeAprovada) {
            return $totalItemAprovado->sum('quantidade_aprovada');
        }

        return $totalItemAprovado->get();
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');
        $adesao = Adesao::join('compras', 'compras.id', '=', 'arp_solicitacao.compra_id')
        ->where('arp_solicitacao.id', $this->crud->getCurrentEntryId())
            ->select('arp_solicitacao.*', 'compras.lei')
        ->first();

        if (!in_array($adesao->situacao->descricao, [
            'Enviada para aceitação',
            'Aceita Parcialmente pelo Fornecedor',
            'Aceita pelo Fornecedor'
        ])) {
            \Alert::add('error', 'Somente adesão com situação enviada para aceitação pode ser analisada')->flash();
            \Redirect::to('/arp/adesao/analisar')->send();
            abort(200);
        }

        $arquivoJS = [
            'assets/js/admin/forms/arp_common.js',
            'assets/js/admin/forms/arp_analisar_adesao.js'
        ];

        $this->importarScriptJs($arquivoJS);

        $this->crud->addField(
            [
                'name' => 'label_unidade_solicitante', // The db column name
                'type' => 'label',
                'label' => 'Unidade Solicitante', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value' => $adesao->getUnidadeSolicitante(),
            ]
        );

        $this->crud->addField(
            [
                'name' => 'label_numero_solicitacao', // The db column name
                'type' => 'label',
                'label' => 'Número da solicitação', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-3 pt-3'],
                'value' => $adesao->getNumeroSolicitacao(),
            ]
        );
        // Se for preciso copiar, descomentar e tentar consertar o layout
        // $myCustomRadioButton = view("vendor.backpack.crud.fields.copy",
        //['text' => $adesao->responsavel->email, 'textTooltip' => 'Copiar o e-mail do responsável'])
        //->render();
        $this->crud->addField(
            [
                'name' => 'label_situacao', // The db column name
                'type' => 'label',
                'label' => 'Responsável pela solicitação', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value' => $adesao->responsavel->name . " - " . $adesao->responsavel->email
            ]
        );

        $this->crud->addField(
            [
                'name' => 'texto_justificativa', // The db column name
                'type' => 'label',
                'label' => 'Justificativa', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-8 pt-3'],
                'value' => $adesao->texto_justificativa,
            ]
        );

        $arrayJustificativa = explode('-', $adesao->justificativa);
        $anexoJustificativa = '<ul>';
        $anexoJustificativa .=
            "<li><a target='_blank' href='" . url("storage/$arrayJustificativa[0]") . "'>" .
            $arrayJustificativa[1] .
            "</a></li>";
        $anexoJustificativa .= '</ul>';

        $this->crud->addField(
            [
                'name' => 'justificativa_texto_html', // The db column name
                'type' => 'label',
                'label' => 'Anexo Justificativa', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value' => $anexoJustificativa
            ]
        );

        $this->crud->addField(
            [
                'name' => 'texto_demonstracao_valor', // The db column name
                'type' => 'label',
                'label' => 'Foi realizada demonstração de que os valores registrados estão
                compatíveis com os valores praticados pelo mercado, <br>
                nos termos da Lei 14.133/2021
                (Art.23 e Art. 86, §2º, inc. II)?', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-8 pt-3'],
                'value' => $this->converterBoleanTexto($adesao->demonstracao_valores_registrados),
            ]
        );

        $anexoDemonstracao = '<ul>';
        foreach ($adesao->demonstracao as $arquivo) {
            $arrayArquivo = explode('-', $arquivo);
            $anexoDemonstracao .=
                "<li><a target='_blank' href='" . url("storage/$arrayArquivo[0]") . "'>" .
                $arrayArquivo[1] .
                "</a></li>";
        }
        $anexoDemonstracao .= '</ul>';
        $this->crud->addField(
            [
                'name' => 'anexo_demonstracao_valor', // The db column name
                'type' => 'label',
                'label' => 'Anexo Demonstração', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value' => $anexoDemonstracao
                //'',
            ]
        );


        $this->crud->addField(
            [
                'name' => 'texto_aceitacao_valor', // The db column name
                'type' => 'label',
                'label' => 'Houve prévia consulta e aceitação do fornecedor,
                nos termos da Lei 14.133/2021 (Art. 86, §2º, inc. III)?',
                'wrapperAttributes' => ['class' => 'col-md-8 pt-3'],
                'value' => $this->converterBoleanTexto($adesao->consulta_aceitacao_fornecedor),
            ]
        );

        $anexoAceitacao = '<ul>';

        $arquivos = !empty($adesao->aceitacao) ? $adesao->aceitacao : [$adesao->anuencia_fornecedor];
        $label = !empty($adesao->aceitacao) ? 'Anexo Aceitação' : 'Anexo Anuência Fornecedor';

        foreach ($arquivos as $arquivo) {
            $arrayArquivo = explode('-', $arquivo);
            $anexoAceitacao .=
                "<li><a target='_blank' href='" . url("storage/$arrayArquivo[0]") . "'>" .
                $arrayArquivo[1] .
                "</a></li>";
        }
        $anexoAceitacao .= '</ul>';

        $this->crud->addField([
            'name' => 'anexo_aceitacao_valor',
            'type' => 'label',
            'label' => $label,
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'value' => $anexoAceitacao
        ]);


        if (!empty($adesao->texto_justificativa_item_isolado)) {
            $this->crud->addField(
                [
                    'name' => 'texto_justificativa_item_isolado', // The db column name
                    'type' => 'label',
                    'label' => 'Justificativa de item isolado pertencente a um grupo', // Table column heading
                    'wrapperAttributes' => ['class' => 'col-md-8 br-textarea pt-3 texto-justificativa-item-isolado'],
                    'value' => $adesao->texto_justificativa_item_isolado,
                ]
            );
        }

        if (!empty($adesao->justificativa_item_isolado)) {
            $justItem = '<ul>';
            foreach ($adesao->justificativa_item_isolado as $arquivo) {
                $arrayArquivo = explode('-', $arquivo);
                $justItem .=
                    "<li><a target='_blank' href='" . url("storage/$arrayArquivo[0]") . "'>" . $arrayArquivo[1] . "</a></li>";
            }
            $justItem .= '</ul>';
            $this->crud->addField(
                [
                    'name' => 'aceitacao_justificativa_isolado_valor', // The db column name
                    'type' => 'label',
                    'label' => 'Anexo justificativa item isolado', // Table column heading
                    'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                    'value' => $justItem
                ]
            );
        }

        if ($adesao->execucao_descentralizada_programa_projeto_federal !== null) {
            $this->crud->addField(
                [
                    'name' => 'execucao_descentralizada_programa_projeto_federal', // The db column name
                    'type' => 'label',
                    'label'     => 'A adesão é destinada à execução descentralizada de programa ou projeto federal'.
                        ' com recursos financeiros provenientes de transferências voluntárias da União'.
                        ' para Estados, municípios e Entidades da administração pública federal'.
                        ' integrantes dos Orçamentos Fiscal e da Seguridade Social da União e a '.
                        ' Organizações da Sociedade Civil (OSC)?',
                    'wrapperAttributes' => ['class' => 'col-md-8 pt-3'],
                    'value' =>
                        $this->converterBoleanTexto($adesao->execucao_descentralizada_programa_projeto_federal),
                ]
            );

            if ($adesao->execucao_descentralizada_programa_projeto_federal) {
                $arrayAnexoComprovacaoExecucaoDescentralizadaProgramaProjetoFederal =
                    explode('-', $adesao->anexo_comprovacao_execucao_descentralizada_programa_projeto_fed);

                $anexoAnexoComprovacaoExecucaoDescentralizadaProgramaProjetoFederal = '<ul>';
                $url = url("storage/$arrayAnexoComprovacaoExecucaoDescentralizadaProgramaProjetoFederal[0]");
                $anexoAnexoComprovacaoExecucaoDescentralizadaProgramaProjetoFederal .=
                    "<li><a target='_blank' href='" .$url. "'>" .
                    $arrayJustificativa[1] .
                    "</a></li>";
                $anexoAnexoComprovacaoExecucaoDescentralizadaProgramaProjetoFederal .= '</ul>';

                $this->crud->addField(
                    [
                        'name' => 'anexo_comprovacao_execucao_descentralizada_programa_projeto_fed',
                        'type' => 'label',
                        'label' => 'Anexo comprovação', // Table column heading
                        'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                        'value' => $anexoAnexoComprovacaoExecucaoDescentralizadaProgramaProjetoFederal
                    ]
                );

                $this->crud->addField(
                    [
                        'name' => 'justificativa_anexo_comprovacao_execucao_descentralizada_progra',
                        'type' => 'label',
                        'label' =>
                            'Justificativa da comprovação da execução descentralizada de programa ou projeto federal',
                        'wrapperAttributes' => ['class' => 'col-md-12 pt-3'],
                        'value' => $adesao->justificativa_anexo_comprovacao_execucao_descentralizada_progra,
                    ]
                );

                Widget::add([
                    'type'     => 'view',
                    'view'     =>
                        'vendor.backpack.base.widgets.modal-execucao-descentralizada-programa-projeto-federal',
                    'id' => 'modalexecucaodescentralizadaprogramaprojetofedal',
                ]);
            }
        }

        if ($adesao->aquisicao_emergencial_medicamento_material !== null) {
            $this->crud->addField(
                [
                    'name' => 'aquisicao_emergencial_medicamento_material', // The db column name
                    'type' => 'label',
                    'label' => 'Aquisição emergencial de medicamentos e material de consumo médico-hospitalar?',
                    'wrapperAttributes' => ['class' => 'col-md-6 pt-3'],
                    'value' => $this->converterBoleanTexto($adesao->aquisicao_emergencial_medicamento_material),
                ]
            );
        }
        
        $this->crud->addField(
            [
                'name' => 'ata_enfrentando_impacto_decorrente_calamidade_publica', // The db column name
                'type' => 'label',
                'label' => 'Mostrar atas registradas para enfrentamento dos
                impactos decorrentes do estado de calamidade pública?',
                'wrapperAttributes' => ['class' => 'col-md-6 pt-3'],
                'value' => $this->converterBoleanTexto($adesao->ata_enfrentando_impacto_decorrente_calamidade_publica),
            ]
        );

        $itensAnalise = $this->montarItemAnalise($adesao);

        $this->crud->addField(
            [
                'name' => 'quantidade_item_analisado', // The db column name
                'type' => 'label',
                'label' => 'Item(ns) analisado(s)', // Table column heading
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'value' => $itensAnalise['quantidadeAnalisado'] . "/" . $itensAnalise['quantidadeTotalItem']
            ]
        );

        $column = $this->adesaoService->camposTabelaAnalisarItemAdesao(
            null,
            false,
            $adesao->item
        );

        $this->addTableCustom(
            $column,
            'table_selecionar_item_adesao_arp',
            'Selecionar o fornecedor para exibir a informação',
            'col-md-12 pt-5',
            'areaItemAdesao',
            null,
            100,
            $itensAnalise['item'],
            'Preencha o quantidade para analisar o item'
        );

        $this->crud->button_custom = [
            [
                'button_text' => 'Salvar Rascunho', 'button_id' => 'rascunho',
                'button_name_action' => 'rascunho', 'button_value_action' => '1',
                'button_icon' => 'fas fa-edit', 'button_tipo' => 'secondary'
            ],
            [
                'button_text' => 'Finalizar análise', 'button_id' => 'adicionar',
                'button_name_action' => 'rascunho', 'button_value_action' => '0',
                'button_icon' => 'fas fa-save', 'button_tipo' => 'primary'
            ],
        ];
    }

    public function update(AnalisarAdesaoRequest $request)
    {
        $data = $request->all();

        # Se não tiver nenhum item preenchido, retorna para a tela do usuário
        if (!isset($data['status_id'])) {
            \Alert::add('error', 'Preencha ao menos um item da análise')->flash();
            return redirect()->back();
        }

        $todosItensAnalisado = true;
        $todosItensAnalisadosAceito = 0;
        $todosItensAnalisadosNegado = 0;
        $todosItensAnalisadosAceitoParcial = 0;
        $todosItens = 0;

        DB::beginTransaction();
        $adesaoItemRepository = new ArpAdesaoItemRepository();
        $compraItemUnidade = new CompraItemUnidadeRepository();
        $adesaoRepository = new ArpAdesaoRepository();

        try {
            # Percorre todos os status informados na tabela para atualizar as informações no banco de dados
            foreach ($data['status_id'] as $idItemAdesao => $statusId) {
                # Recupera o item da adesão
                $itemAdesao = $adesaoItemRepository->getItemAdesaoUnico($idItemAdesao);

                # Recupera o status marcado pelo usuário
                $descricaoStatus = CodigoItem::find($statusId);

                # Monta as informações necessário para a inserção com base no status selecionado
                switch ($descricaoStatus->descricao) {
                    case 'Aceitar':
                        $quantidadeHomologadaVencedor =
                            $itemAdesao->item_arp->item_fornecedor_compra->quantidade_homologada_vencedor;
                        $compraItemId = $itemAdesao->item_arp->item_fornecedor_compra->id;

                        $itemAdesao->quantidade_aprovada =
                            $itemAdesao->quantidade_anuencia_fornecedor
                            ?? $itemAdesao->quantidade_solicitada;

                        $itemAdesao->motivo_analise = null;

                        $todosItensAnalisadosAceito++;
                        break;
                    case 'Negar':
                        $itemAdesao->quantidade_aprovada = 0;
                        $itemAdesao->motivo_analise = $data['motivo_analise'][$idItemAdesao];
                        $todosItensAnalisadosNegado++;
                        break;
                    case 'Aceitar Parcialmente':
                        $itemAdesao->quantidade_aprovada = $data['quantidade_aprovada'][$idItemAdesao];
                        $itemAdesao->motivo_analise = $data['motivo_analise'][$idItemAdesao];
                        $todosItensAnalisadosAceitoParcial++;
                        break;
                }

                # Se alguma quantidade for fracionada, não permitir continuar
                if ($this->temValorFracionado($itemAdesao->quantidade_aprovada)) {
                    \Alert::add(
                        'error',
                        'Não é permitido fazer a aprovação de uma quantidade fracionada.'
                    )->flash();
                    return redirect()->back();
                }

                $itemAdesao->responsavel_aprovacao_id = backpack_user()->id;
                $itemAdesao->status_id = $statusId;
                $itemAdesao->save();

                # Se ainda exisitir item não avaliado, seta na variável
                # para não mudar o status da adesão de forma automática
                if ($descricaoStatus->descricao == 'Item Não Avaliado') {
                    $todosItensAnalisado = false;
                }

                $todosItens++;
            }

            # Recupera o total dos itens
            $totalItens = $adesaoItemRepository->getTotalItemAdesao($data['id']);

            # Se todos os itens não foram analisados, salva como rascunho
            if ($totalItens != count($data['status_id'])) {
                \Alert::add('success', 'Rascunho salvo com sucesso')->flash();
                return redirect()->back();
            }

            # Se todos os itens foram analisados e foi clicado para finalizar a ata, entra no IF
            if ($todosItensAnalisado && !$data['rascunho']) {
                # Se todos os itens foram aceitos
                if ($todosItens == $todosItensAnalisadosAceito) {
                    $statusAnaliseFinalizada =
                        $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', 'Aceita');
                }

                # Se todos os itens foram negados
                if ($todosItens == $todosItensAnalisadosNegado) {
                    $statusAnaliseFinalizada =
                        $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', 'Negada');
                }

                # Se existe algum item parcial
                if ($todosItensAnalisadosAceitoParcial > 0 || ($todosItensAnalisadosAceito > 0
                        && $todosItensAnalisadosNegado > 0)) {
                    $statusAnaliseFinalizada =
                        $this->retornaIdCodigoItem(
                            'Tipo de Ata de Registro de Preços',
                            'Aceito Parcial'
                        );
                }

                # Recupera a adesão para alterar o status conforme o que o usuário escreveu
                $analise = $adesaoRepository->getAdesaoUnico($data['id']);
                $analise->status = $statusAnaliseFinalizada;
                $analise->data_aprovacao_analise = Carbon::now()->toDateTimeString();

                if ($analise->execucao_descentralizada_programa_projeto_federal) {
                    $analise->ciente_analise_entre_projeto_federal = true;
                }

                $analise->save();

                # Recupera o item para poder incluir a solicitante como carona na tabela compra_item_unidade
                $itemAdesao = $adesaoItemRepository->getItemAdesaoSolicitada($data['id']);
                $idUnidadeCarona = $analise->unidade_origem_id;
                foreach ($itemAdesao as $item) {
                    $itemUnidadeCarona =
                        $compraItemUnidade->getItemUnidadeCaronaAdesao(
                            $item->compra_item_id,
                            $idUnidadeCarona,
                            $item->fornecedor_id
                        );

                    # Se existir, então acrescenta com a quantidade autorizada existente
                    if (!empty($itemUnidadeCarona)) {
                        $itemUnidadeCarona->quantidade_autorizada += $item->quantidade_aprovada;
                        $itemUnidadeCarona->quantidade_adquirir += $item->quantidade_aprovada;

                        $itemUnidadeCarona->save();

                        $itemUnidadeCarona->quantidade_saldo = $this->retornaSaldoAtualizado(
                            $itemUnidadeCarona->compra_item_id,
                            $itemUnidadeCarona->unidade_id,
                            $itemUnidadeCarona->id
                        )->saldo;

                        $itemUnidadeCarona->save();
                        continue;
                    }

                    $itemUnidade = new CompraItemUnidade();

                    $itemUnidade->compra_item_id = $item->compra_item_id;
                    $itemUnidade->fornecedor_id = $item->fornecedor_id;
                    $itemUnidade->unidade_id = $idUnidadeCarona;
                    $itemUnidade->quantidade_autorizada = $item->quantidade_aprovada;
                    $itemUnidade->tipo_uasg = "C";
                    $itemUnidade->quantidade_adquirir = $item->quantidade_aprovada;
                    $itemUnidade->quantidade_adquirida = 0;
                    $itemUnidade->novo_gestao_ata = true;
                    $itemUnidade->save();


                    $itemUnidade->quantidade_saldo = $this->retornaSaldoAtualizado(
                        $itemUnidade->compra_item_id,
                        $itemUnidade->unidade_id,
                        $itemUnidade->id
                    )->saldo;

                    $itemUnidade->save();
                }

                $this->adesaoService->salvarHistorico($data['id'], $statusAnaliseFinalizada);

                DB::commit();

                \Alert::add('success', 'Adesão finalizada com sucesso')->flash();
                return redirect('/arp/adesao/analisar');
            }

            DB::commit();

            \Alert::add('success', 'Rascunho salvo com sucesso')->flash();
            return redirect()->back();
        } catch (Exception $ex) {
            DB::rollBack();
            Log::info($ex);
            \Alert::add('error', "Erro ao salvar a solicitação")->flash();
            return redirect()->back();
        }
    }

    private function montarItemAnalise(Adesao $adesao)
    {
        $itemFormatado = array();

        $quantidadeAnalisado = 0;
        $quantidadeTotalItem = 0;
        $item = [];

        $execucaoDescentralizadaProgramaProjetoFederal = $adesao->execucao_descentralizada_programa_projeto_federal;
        $aquisicaoEmergencialMedicamentoMaterial = $adesao->aquisicao_emergencial_medicamento_material;

        $unidadeUsuarioPertenceCentralCompras = $this->unidadeUsuarioPertenceCentralCompras();
        $arpAdesaoRepository = new ArpAdesaoRepository();

        foreach ($adesao->item as $itemSelecionado) {
            $compraItem = $itemSelecionado->item_arp->item_fornecedor_compra->compraItens;
            $compraItemFornecedor = $itemSelecionado->item_arp->item_fornecedor_compra;

            $max = ($compraItem->maximo_adesao * 0.5);
            if ($compraItem->tipoItem->descres == 'MATERIAL') {
                $step = 1;
                $max = ceil($max);
            }

            if ($compraItem->tipoItem->descres == 'SERVIÇO') {
                $step = 0.00001;
            }

            $item['numeroata'] = $itemSelecionado->item_arp->arp->getNumeroAno();
            $item['nomefornecedor'] = $compraItemFornecedor->fornecedor->getNomeFornecedorCompleto();
            $item['numero'] = $compraItem->numero;
            $item['descricaodetalhada'] =
                Str::limit(
                    trim($compraItem->descricaodetalhada),
                    50,
                    '<i class="fas fa-info-circle"
                    title="' . $compraItem->descricaodetalhada . '">
                    </i>'
                );

            $quantidadeHomologadaVencedor = $compraItemFornecedor->quantidade_homologada_vencedor;
            $qtdPermitidoFornecedor = $quantidadeHomologadaVencedor * 2;

            # Informa a quantidade registrada para o item
            $item['quantidaderegistrada'] =
                number_format($quantidadeHomologadaVencedor, 4, ',', '.');

            $item['valorunitario'] = $compraItemFornecedor->getValorUnitarioFinal();

            $item['vigencia'] =
                (new Carbon($compraItem->ata_vigencia_inicio))->format('d/m/y') .
                "<br>-<br>" .
                (new Carbon($compraItem->ata_vigencia_fim))->format('d/m/y');

            $itemCanceladoRemovido =
                $this->arpItemHistoricoService->itemAtaCanceladoRemovido($itemSelecionado->item_arp->id);

            if ($itemCanceladoRemovido) {
                $item['statusitem'] = 'Item e/ou ata estão cancelados. Não é possível aceitar adesão.';
                $item['maximoadesao'] = 0;
                $item['quantidadesolicitada'] = 0;
                $item['quantidadeaprovada'] =
                    "<input type='hidden' name='quantidade_aprovada[{$itemSelecionado->id}]' value='0'>
                    Item e/ou ata estão cancelados. Não é possível aceitar adesão.";
                $item['motivoanalise'] = 'Item e/ou ata estão cancelados. Não é possível aceitar adesão.';
                $itemFormatado[] = $item;
                continue;
            }

            $quantidadeHomologadaVencedorTodosItens = $compraItem->qtd_total * 0.5;

            $item['maximoadesao'] = $this->adesaoService->calcularMaximoAdesaoPorUnidadeSolicitante(
                $compraItemFornecedor->fornecedor_id,
                $compraItem->maximo_adesao,
                $quantidadeHomologadaVencedorTodosItens,
                $arpAdesaoRepository,
                $compraItemFornecedor->compra_item_id,
                $itemSelecionado->adesao->unidade_origem_id,
                $compraItem->qtd_total,
                $execucaoDescentralizadaProgramaProjetoFederal
            );

            $item['quantidadesolicitada'] = $itemSelecionado->quantidade_solicitada;
            $item['quantidadeaprovadafornecedor'] = $itemSelecionado->quantidade_anuencia_fornecedor;

            $somenteAceitarParcial = false;

            $resultado = false;

            if (preg_match($this->mp, $adesao->lei, $matches)) {
                $resultado = $matches[0];
            }

            if ($unidadeUsuarioPertenceCentralCompras && $resultado !== false) {
                $somenteAceitarParcial = false;
            } elseif ($item['maximoadesao'] < $item['quantidadesolicitada']) {
                $somenteAceitarParcial = true;
            }


            if ($execucaoDescentralizadaProgramaProjetoFederal || $aquisicaoEmergencialMedicamentoMaterial) {
                $somenteAceitarParcial = false;
            }

            # a cada registro, será montado as opções de análise.
            $estruturaCampoQuantidadeAprovada =
                $this->estruturaCampoQuantidadeAprovada($somenteAceitarParcial, $item['maximoadesao']);

            $item['quantidadeaprovada'] = str_replace(':id', $itemSelecionado->id, $estruturaCampoQuantidadeAprovada);
            $item['quantidadeaprovada'] =
                str_replace(":justificativaMotivacao", $item['numero'], $item['quantidadeaprovada']);

            $item['quantidadeaprovada'] = str_replace(":step", $step, $item['quantidadeaprovada']);
            $item['quantidadeaprovada'] =
                str_replace(":check_" . $itemSelecionado->status_id, 'checked', $item['quantidadeaprovada']);

            $item['quantidadeaprovada'] =
                str_replace(":value", $itemSelecionado->quantidade_aprovada, $item['quantidadeaprovada']);

            $item = $this->maximoAdesaoCentralDeCompras(
                $unidadeUsuarioPertenceCentralCompras,
                $resultado,
                $execucaoDescentralizadaProgramaProjetoFederal,
                $aquisicaoEmergencialMedicamentoMaterial,
                $item,
                $itemSelecionado,
                $qtdPermitidoFornecedor
            );

            $textoStatus = $itemSelecionado->status->descricao;

            # Se o item for aceito parcialmente, desbloqueia o campo para o usuário digitar
            if ($textoStatus == 'Aceitar Parcialmente') {
                $item['quantidadeaprovada'] = str_replace("readonly", '', $item['quantidadeaprovada']);
            }

            # Converte o status e altera a cor do círculo
            $corStatus = '';
            switch ($textoStatus) {
                case 'Item Não Avaliado':
                    $corStatus = 'bg-black';
                    break;
                case 'Aceitar':
                    $corStatus = 'bg-success';
                    $textoStatus = 'Aceito';
                    break;
                case 'Negar':
                    $corStatus = 'bg-danger';
                    $textoStatus = 'Negado';
                    break;
                case 'Aceitar Parcialmente':
                    $corStatus = 'bg-warning';
                    $textoStatus = 'Aceito Parcialmente';
                    break;
            }

            # Responsável em exibir o status para o usuário
            $item['statusitem'] = "
            <div class='d-flex align-items-center'><span class='br-tag status {$corStatus} large'></span></div>
            <div class='br-tooltip' role='tooltip' info='info' place='top'>
            <span class='subtext'>{$textoStatus}</span></div>";

            $disabled = 'disabled';
            $required = '';

            # Se a marcação do usuário for diferente de 'Aceitar' e o item for analisado, entra no IF
            if ($textoStatus != 'Aceito' && $textoStatus != 'Item Não Avaliado') {
                $disabled = '';
                $required = 'required';
            }

            # Responsável em criar o campo de justificativa/motivação
            $item['motivoanalise'] =
                '<div class="br-textarea">
            <textarea name="motivo_analise[' . $itemSelecionado->id . ']" id="textarea-' . $itemSelecionado->id . '"
            placeholder="Descreva o justificativa/motivo"
            style="height: 83px;"
            ' . $disabled . ' ' . $required . '>' . $itemSelecionado->motivo_analise . '</textarea>
            <div class="text-base mt-1"><span class="characters"><strong>0</strong> caracteres digitados</span></div>
            </div>
            ';

            if ($this->unidadeUsuarioPertenceCentralCompras()) {
                unset($item['maximoadesao']);
            }

            $item['motivoanalisefornecedor'] = '
            <div class="br-textarea">
                <textarea
                    placeholder="Descreva a justificativa do fornecedor"
                    style="height: 83px;"
                    disabled
                >' . htmlspecialchars($itemSelecionado->motivo_justificativa_fornecedor) . '</textarea>
                <div class="text-base mt-1"></div>
            </div>';

            # Insere os campos para montar a tabela para o usuário preencher
            $itemFormatado[] = $item;

            # Realiza a contagem dos itens que foram analisados
            if ($itemSelecionado->status->descricao != 'Item Não Avaliado') {
                $quantidadeAnalisado++;
            }

            # Realiza a contagem total dos itens
            $quantidadeTotalItem++;
        }
        $arrayDadosFormatados = array();
        $arrayDadosFormatados['item'] = $itemFormatado;
        $arrayDadosFormatados['quantidadeAnalisado'] = $quantidadeAnalisado;
        $arrayDadosFormatados['quantidadeTotalItem'] = $quantidadeTotalItem;


        return $arrayDadosFormatados;
    }

    private function maximoAdesaoCentralDeCompras(
        bool $unidadeUsuarioPertenceCentralCompras,
        bool $resultado,
        ?bool $execucaoDescentralizadaProgramaProjetoFederal,
        ?bool $aquisicaoEmergencialMedicamentoMaterial,
        array $item,
        object $itemSelecionado,
        float $qtdPermitidoFornecedor
    ) {
        if ($unidadeUsuarioPertenceCentralCompras && $resultado !== false) {
            # Se um dos dois campos estiverem marcados como 'SIM', então a pessoa responsável em analisar
            # pode lançar qualquer valor
            if ($execucaoDescentralizadaProgramaProjetoFederal || $aquisicaoEmergencialMedicamentoMaterial) {
                $item['quantidadeaprovada'] =
                    str_replace(":max", $itemSelecionado->quantidade_aprovada, $item['quantidadeaprovada']);
                $item['maximoadesao'] = $qtdPermitidoFornecedor;
            }

            $maximoAceitoParcialmente = $item['quantidadesolicitada'] - 0.1;

            # Se a quantidade solicitada for menor o máximo deve ser o máximo de adesão,
            # se não, vai ser a quantidade solicitada
            if ($item['maximoadesao'] < $item['quantidadesolicitada']) {
                $maximoAceitoParcialmente = $item['maximoadesao'];

                $item['quantidadeaprovada'] =
                    str_replace(":max", $maximoAceitoParcialmente, $item['quantidadeaprovada']);
            }

            if ($item['maximoadesao'] > $item['quantidadesolicitada']) {
                $item['quantidadeaprovada'] =
                    str_replace(":max", $maximoAceitoParcialmente, $item['quantidadeaprovada']);
            }

            # Se a quantidade aprovada for maior que zero, inclui o valor deixa o campo obrigatório
            if ($itemSelecionado->quantidade_aprovada > 0) {
                $item['quantidadeaprovada'] =
                    str_replace(":value", $itemSelecionado->quantidade_aprovada, $item['quantidadeaprovada']);
                $item['quantidadeaprovada'] = str_replace(":required", 'required', $item['quantidadeaprovada']);
            }
        }

        return $item;
    }
}
