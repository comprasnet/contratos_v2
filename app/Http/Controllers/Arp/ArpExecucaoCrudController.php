<?php

namespace App\Http\Controllers\Arp;

use App\Http\Requests\ArpExecucaoRequest;
use App\Http\Traits\Arp\ArpItemTrait;
use App\Http\Traits\Arp\ArpTrait;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\CommonFields;
use App\Http\Traits\Formatador;
use App\Models\Arp;
use App\Models\ArpExecucao;
use App\Models\ArpHistorico;
use App\Models\ArpItemHistorico;
use App\Repositories\Arp\ArpHistoricoRepository;
use App\Repositories\Arp\ArpItemRepository;
use App\Repositories\CodigoItemRepository;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use App\Http\Traits\ImportContent;
use Illuminate\Http\Request;

/**
 * Class ArpExecucaoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArpExecucaoCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    use ArpTrait;
    use ArpItemTrait;
    use ImportContent;
    use CommonFields;
    use Formatador;

    private function returnIdArp()
    {
        return Route::current()->parameter("arp_id");
    }

    private $unidadeOrigemDaAtaId;

    private function breadCrumb(bool $acaoVoltar = false)
    {
        $arp = Arp::find($this->returnIdArp());
        $bread = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Ata de Registro de Preços' => backpack_url('arp'),
            'Execução de Ata de Registro de Preços' => false,
            $arp->getNumeroAno() => false
        ];
        // if ($acaoVoltar) {
        //     $bread['Voltar'] = backpack_url('arp/execucao/' . $this->returnIdArp());
        // }

        return $bread;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ArpItem::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . 'arp/execucao/' . $this->returnIdArp());
        /*CRUD::setEntityNameStrings('arp execucao', 'arp execucaos');*/

        $this->exibirTituloPaginaMenu('Relatar execução da ata');

        CRUD::addClause("where", "arp_id", $this->returnIdArp());

        #dd(Str::replaceArray('?', $this->crud->query->getBindings(), $this->crud->query->toSql()));
        $this->unidadeOrigemDaAtaId = Arp::select('unidade_origem_id')
            ->where('id', $this->returnIdArp())
            ->first()->unidade_origem_id;

        $this->data['breadcrumbs'] = $this->breadCrumb();
        return view('backpack::dashboard', $this->data);
        #$this->bloquearBotaoPadrao($this->crud, config('security.permission_block_crud.all'));
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        #$this->crud->text_button_redirect_create = 'arp execucao';
        #$this->addColumnText('id');
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('col-md-12');
        #CRUD::setValidation(ArpExecucaoRequest::class);

        $arquivoJS = [
            'assets/js/admin/forms/arp_common.js',
            'assets/js/admin/forms/arp_execucao.js',
            'assets/js/blockui/jquery.blockUI.js'
        ];
        $this->importarScriptJs($arquivoJS);

        # Exibe o cabeçalho com dados principais da ata
        $this->areaDadosAta();

        $this->crud->addField(
            [
                'name' => 'hint', // The db column name
                'label' => 'Atenção', // Table column heading
                'type' => 'label',
                'wrapperAttributes' => [
                    'class' => 'col-md-12 pt-3 mb-3'
                ],
                'value' => 'Relatório de Execução da Ata em cumprimento ao art. 82, §§1º e 2º da Lei 14.133/2021, '.
                    'devendo ser justificadas as aquisições de item(ns) isolado(s) pertencente(s) a um grupo '.
                    'durante a vigência da ata e/ou a não execução de todos os itens do grupo '.
                    'ao final da vigência da ata, ficando o usuário que salvou a informação responsável '.
                    'pelos dados informados.'
            ]
        );

        #
        $this->crud->addField([
            'name' => 'arp_id',
            'type' => 'hidden',
            'value' => $this->returnIdArp()
        ]);


        # Monta a tabela com os itens da ata
        $this->itensDaAta();

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */

        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Salvar',
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');
        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Atualizar ArpExecucao',
        ]);
        $this->setupCreateOperation();
    }

    private function reorganizaDados($dados)
    {
        $dadosReorganizados = [];
        // Percorre as chaves do array $dados
        foreach ($dados['grupo'] as $chave => $valor) {
            // Adiciona os valores correspondentes de cada subarray ao novo array
            $dadosReorganizados[$chave] = [
                'grupo' => $valor,

                'numero_item' => $dados['numero_item'][$chave] ?? null,

                'quantidade_empenhada' => isset($dados['quantidade_empenhada'][$chave]) ?
                    str_replace(',', '.', $dados['quantidade_empenhada'][$chave]) : 0,

                'unidade_participa' => isset($dados['unidade_participa'][$chave]) ?
                    (bool)$dados['unidade_participa'][$chave] : null,

                'texto_justificativa_item_isolado' => $dados['texto_justificativa_item_isolado'][$chave] ?? null,
            ];
        }
        return $dadosReorganizados;
    }

    /**
     * Função para executar a validação para o front end
     * É chamada em arp_execucao.js
     *
     * @param Request $request
     * @return false|string
     */
    public function validaFormularioAjax(Request $request)
    {
        $dados = $this->reorganizaDados($request->all());
        $erros = $this->validaRegrasDaExecucao($dados);

        $status = ['status' => 'success'];

        if ($erros) {
            $status = ['status'=>'error', 'message' => $erros];
        }

        return response()->json($status);
    }

    /**
     * Função para executar a validação das regras
     * @param $dados
     * @return string
     */
    private function validaRegrasDaExecucao($dados)
    {
        $retornoNaoParcicipa = '';
        $retornoQuantidadeEmpenhada = '';
        #$haSomenteItensSemGrupo = true;

        foreach ($dados as $compraItemId => $item) {
            # Não verifica se não tem grupo
            if (!$this->itemPertenceGrupo($item['grupo'])) {
                continue;
            }
            #$haSomenteItensSemGrupo = false;

            # Se a unidade não participa do item, precisa ter justificativa
            if (!$item['unidade_participa']) {
                if (empty($item['texto_justificativa_item_isolado'])) {
                    $retornoNaoParcicipa .=
                        "Item {$item['numero_item']} - A unidade não participa deste item do grupo {$item['grupo']}. " .
                        "Justifique a aquisição de item isolado pertencente a um grupo, durante a vigência da Ata.<br>";
                }
            } else {
                # Se a unidade participa, mas não tem valor informado nem justificativa
                if ($item['quantidade_empenhada'] == 0 && empty($item['texto_justificativa_item_isolado'])) {
                    $retornoQuantidadeEmpenhada .=
                        "Item {$item['numero_item']} - Este item do grupo {$item['grupo']} não foi empenhado. " .
                        "Justifique a não execução de todos os itens agrupados no grupo ".
                        "ou informe a quantidade empenhada de todos os itens.<br>";
                }
            }
        }

        /*if ($haSomenteItensSemGrupo) {
            return 'Não há itens para relatar a execução.';
        }*/
        return $retornoNaoParcicipa . $retornoQuantidadeEmpenhada;
    }

    /**
     * Busca o último histórico da execução pela unidade para trazer os itens para a tela
     *
     * @param $arpId
     * @param $unidadeId
     * @return mixed
     */
    public function buscaUltimoHistoricoDaExecucao($arpId, $unidadeId)
    {
        $idArpHistorico = ArpHistorico::join('codigoitens', 'codigoitens.id', '=', 'arp_historico.tipo_id')
            ->where('arp_id', $arpId)
            ->where('unidade_executora_id', $unidadeId)
            ->where('codigoitens.descricao', ArpExecucao::RELATAR_EXECUCAO_ATA)
            ->latest('arp_historico.id')
            ->pluck('arp_historico.id')
            ->first();

        return ArpItemHistorico::where('arp_historico_id', $idArpHistorico)->get();
    }

    public function store(ArpExecucaoRequest $request)
    {
        $dados = $this->reorganizaDados($request->all());
        $arpId = $this->returnIdArp();
        # Se não passar nas regras, devolve mensagem e para a execução
        if ($errors = $this->validaRegrasDaExecucao($dados)) {
            \Alert::add('error', $errors)->flash();
            return redirect("/arp/execucao/{$arpId}/create");
        }

        $arpHistoricoRepository = new ArpHistoricoRepository();
        $codigoItemRepository = new CodigoItemRepository();

        # Busca o id da codigoitens referente à execução da ata
        $tipoId = $codigoItemRepository->getCodigoItemId('Relatar execução da ata');

        # Prepara o histórico da ata
        $arpUltimoStatus = $arpHistoricoRepository->getUltimoEstadoDaAta($arpId, session('user_ug_id'));
        $arpUltimoStatus['tipo_id'] = $tipoId;

        try {
            DB::beginTransaction();

            # Grava o histórico da ata para então registrar a execução dos itens
            $idArpHistorico = ArpHistorico::create($arpUltimoStatus);

            # Gravação dos itens
            foreach ($dados as $arpItemId => $item) {
                $arpItemHistorico = [
                    'arp_historico_id' => $idArpHistorico->id,
                    'execucao_quantidade_empenhada' => $item['quantidade_empenhada'],
                    'justificativa' => $item['texto_justificativa_item_isolado'],
                    'user_id' => Auth::user()->id,
                    'arp_item_id' => $arpItemId
                ];

                ArpItemHistorico::create($arpItemHistorico);
            }

            DB::commit();

            \Alert::add('success', 'Informações gravadas com sucesso.')->flash();
            return redirect("/arp");
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::add('error', $e->getMessage())->flash();
            return redirect("/arp/execucao/{$arpId}/create");
        }
    }

    /**
     * Monta a exibição da tabela de itens da ata
     *
     * @return void
     */
    private function itensDaAta()
    {
        $arpId = $this->returnIdArp();

        # Busca os dados no histórico. Se existir, exibe as últimas informações para o usuário
        $ultimoHistorico = $this->buscaUltimoHistoricoDaExecucao($arpId, session('user_ug_id'));

        $historicoExecucao = [];
        foreach ($ultimoHistorico as $item) {
            $historicoExecucao[$item->arp_item_id] = $item;
        }

        # Recupera os itens de compra referentes aos itens da ata que está sendo visualizada
        $itensDaAta = $this->getItensParaExecucaoPorUmaUnidade($this->returnIdArp(), session('user_ug_id'));

        /*** Calcula as quantidades de itens por grupo de forma geral e também a quantidade que a unidade participa ***/
        $quantidadeDeItensPorGrupo = $this->calculaQuantidadeDeItensPorGrupo($itensDaAta);
        $quantidadeDeItensPorGrupoUnidadeParticipa =
            $this->calculaQuantidadeDeItensPorGrupoParaUmaUnidade($itensDaAta);

        $linhaItem = null;

        foreach ($itensDaAta as $item) {
            $grupoDescricao = '-';
            $justificativaStatus = 'disabled';
            $item->justificativa = '';

            # Se o item existir no histórico, utiliza os dados do histórico
            if (isset($historicoExecucao[$item->id])) {
                $item->quantidade_empenhada = $historicoExecucao[$item->id]->execucao_quantidade_empenhada;
                $item->justificativa = $historicoExecucao[$item->id]->justificativa;
            }

            # Tratamentos quando o item pertence a um grupo
            if ($this->itemPertenceGrupo($item->grupo_compra)) {
                # Atribui a descrição do grupo
                $grupoDescricao = $item->grupo_compra;

                # Se a Unidade não participa de todos os itens ou se não tem nada empenhado no item,
                # habilita o campo para justificar o motivo
                if (($quantidadeDeItensPorGrupo[$item->grupo_compra] !=
                        $quantidadeDeItensPorGrupoUnidadeParticipa[$item->grupo_compra]
                    )
                    || !$item->quantidade_empenhada
                ) {
                    $justificativaStatus = '';
                }
            } else { # Se o item não pertence a um grupo
                # Se a quantidade for zero, permite justificar também, mesmo sendo item isolado
                if ($item->quantidade_empenhada == 0) {
                    $justificativaStatus = '';
                }
            }

            $quantidadeEmpenhadaStatusDoCampo = '';
            $valueUnidadeParticipa = '1';

            # Tratamentos para quando uma unidade não é participante de um item
            if (!$item->unidade_participa) {
                # Desabilita o campo de quantidade empenhada
                $quantidadeEmpenhadaStatusDoCampo = 'disabled';
                # Exibe o valor como zero
                $valueUnidadeParticipa = '0';
            }

            $linhaItem[] = '
                <tr
                id="linha_' . $item->id . '" 
                grupo="' . $item->grupo_compra . '" 
                item_id="' . $item->id . '" 
                unidade_participa="' . $valueUnidadeParticipa . '">
                
                    <td>' . $item->nome_fornecedor . '</td>
                    <td>' . $item->numero . '</td>
                    <td>' . $item->descricaodetalhada . '</td>
                    <td>' . $grupoDescricao . '</td>
                    <td>' .
                Arp::getQuantidadeTotalRegistradaAutorizadaDeUmItem($this->returnIdArp(), $item->numero) . ' 
                    </td>
                    <td>
                        <div class="br-input">
                            <input type="text"
                                name="quantidade_empenhada[' . $item->id . ']"
                                value="' . $this->formatQuantidade($item->quantidade_empenhada) . '"
                                ' . $quantidadeEmpenhadaStatusDoCampo . '
                                class="inputValorNumericoFracionado5Casas quantidade_empenhada"
                                style="min-width: 120px;"                                
                                />
                        </div>                                                  
                    </td>
                    <td>
                    <div class="br-textarea">
                        <textarea 
                        id="texto_justificativa_item_isolado_' . $item->id . '"
                        name="texto_justificativa_item_isolado[' . $item->id . ']"
                        class="col-md-12 br-textarea texto-justificativa-item-isolado_' . $item->id . '"
                        style="min-width: 250px"
                        ' . $justificativaStatus . '
                        >' . $item->justificativa . '</textarea>
                        </div>                        
                    </td>
                </tr>
                <input type="hidden" name="numero_item[' . $item->id . ']" value="' . $item->numero . '" />
                
                <input type="hidden" 
                name="unidade_participa[' . $item->id . ']" 
                value="' . $valueUnidadeParticipa . '" />
                
                <input type="hidden" name="grupo[' . $item->id . ']" value="' . $item->grupo_compra . '" />
                ';
        }

        if ($linhaItem) {
            $this->addAreaCustom('table_item_arp_registro_execucao', null, null, $linhaItem);
            return;
        }

        $this->crud->addField([
            'name' => '',
            'type' => 'label',
            'value' => '<label style="padding-top: 40px;">Não foram encontrados itens para esta ata.</label>'
        ]);
    }
}
