<?php

namespace App\Http\Controllers\Arp;

use App\Http\Requests\ComprasRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Models\CodigoItem;
use App\Models\Compras;
use App\Models\Unidade;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function config;

/**
 * Class ComprasCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ComprasCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $dadosUsuarioRedis = $this->recuperarDadosUsuarioRedis(auth()->id());
        $uasg_session = session('user_ug_id');

        if (!$uasg_session) {
            $uasg_session = $dadosUsuarioRedis['user_ug_id'];
        }

        $this->crud->text_button_redirect_create = 'Compras';
        CRUD::setModel(\App\Models\Compras::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/compras');

        $this->exibirTituloPaginaMenu('Compras');
        $this->bloquearBotaoPadrao($this->crud, config('security.permission_block_crud.compras'));

        CRUD::addClause('select', 'compras.*');
        CRUD::addClause('leftjoin', DB::raw('unidades as unidade_origem'), 'unidade_origem.id', '=', 'compras.unidade_origem_id');
        CRUD::addClause('leftjoin', DB::raw('unidades as unidade_subrrogada'), 'unidade_subrrogada.id', '=', 'compras.unidade_subrrogada_id');
        CRUD::addClause('leftjoin', DB::raw('unidades as uasg_beneficiaria'), 'uasg_beneficiaria.id', '=', 'compras.uasg_beneficiaria_id');
        CRUD::addClause('leftjoin', DB::raw('codigoitens as modalidade'), 'modalidade.id', '=', 'compras.modalidade_id');
        CRUD::addClause('leftjoin', DB::raw('codigoitens as tipo_compra'), 'tipo_compra.id', '=', 'compras.tipo_compra_id');
//        CRUD::addClause('where', 'unidade_origem_id', '=', $uasg_session);

        $this->crud->addButtonFromView('line', 'compras', 'compraitens', 'end');
        // $this->crud->addButtonFromView('line', 'compras', 'morecontrato', 'end');

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');
    }

    protected function setupListOperation()
    {

        $this->crud->addFilter(
            [
            'name'        => 'unidade_origem_id',
            'type'        => 'select2_ajax',
            'label'       => 'Unidade Origem',
            'placeholder' => '',
            'minimum_input_length' => 0,
            'method'      => 'GET',
            'select_attribute' => 'nome_completo',
            'select_key' => 'id'
            ],
            url('/compras/ajax-unidades-options'),
            function ($value) {
                $this->crud->addClause('where', 'unidade_origem_id', $value);
            }
        );

        $this->crud->addFilter(
            [
            'name'        => 'unidade_subrrogada_id',
            'type'        => 'select2_ajax',
            'label'       => 'Unidade Sub-rogada',
            'placeholder' => '',
            'minimum_input_length' => 0,
            'method'      => 'GET',
            'select_attribute' => 'nome_completo',
            'select_key' => 'id'
            ],
            url('/compras/ajax-unidades-options'),
            function ($value) {
                $this->crud->addClause('where', 'unidade_subrrogada_id', $value);
            }
        );

        $this->crud->addFilter([
            'name'  => 'tipo_compra',
            'type'  => 'select2',
            'label' => 'Tipo de Compra'
        ], function () {
            return [
                'SISPP' => 'SISPP',
                'SISRP' => 'SISRP',
            ];
        }, function ($value) {
            $this->crud->addClause('where', 'tipo_compra.descricao', $value);
        });

        $this->crud->addFilter(
            [
            'name'        => 'modalidade_id',
            'type'        => 'select2_ajax',
            'label'       => 'Modalidade',
            'minimum_input_length' => 0,
            'placeholder' => '',
            'method'      => 'GET',
            'select_attribute' => 'descres_descricao',
            'select_key' => 'id'
            ],
            url('/compras/ajax-modalidade-options'),
            function ($value) {
                $this->crud->addClause('where', 'modalidade_id', $value);
            }
        );

        $this->crud->addFilter(
            [
            'name'        => 'lei',
            'type'        => 'select2_ajax',
            'label'       => 'Lei',
            'placeholder' => '',
            'minimum_input_length' => 0,
            'method'      => 'GET',
            'select_attribute' => 'lei',
            'select_key' => 'lei'
            ],
            url('/compras/ajax-compra-lei-options'),
            function ($value) {
                $this->crud->addClause('where', 'lei', $value);
            }
        );

        $this->setupShowOperation();

        $this->crud->enableExportButtons();
    }

    public function modalidadeOptions(Request $request)
    {
        $term = $request->input('term');
        return CodigoItem::whereHas('codigo', function ($query) {
                    $query->where('descricao', '=', 'Modalidade Licitação')
                        ->whereNull('deleted_at');
        })
                    ->whereRaw('LENGTH(descres) <= 2')
                    ->where('descricao', 'ilike', '%'.$term.'%')
                    ->orderBy('codigoitens.descres')
                    ->select(DB::raw("CONCAT(descres,' - ',descricao) AS descres_descricao"), 'id')
                    ->pluck('descres_descricao', 'id');
    }

    public function unidadesOptions(Request $request)
    {
        $term = $request->input('term');
        return Unidade::where('codigo', 'ilike', '%'.$term.'%')
            ->select([
                'id',
                DB::raw("CONCAT(codigo, ' - ', nome) AS nome_completo")
            ])
            ->get()
            ->take(10)
            ->pluck('nome_completo', 'id');
    }

    public function compraLeiOptions(Request $request)
    {
        $term = $request->input('term');
        return Compras::where('lei', 'ilike', '%'.$term.'%')
            ->select([
                'lei as nome',
                'lei as id'
            ])
            ->distinct()
            ->pluck('nome', 'id');
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);

        $this->addColumnGetUnidade(
            true,
            true,
            true,
            true,
            'unidade_origem_id',
            'Unidade Origem',
            'getUnidadeOrigem',
            'unidade_origem'
        );

        $this->addColumnGetUnidade(
            true,
            true,
            true,
            true,
            'unidade_subrrogada_id',
            'Unidade Sub-rogada',
            'getUnidadeSubrrogada',
            'unidade_subrrogada'
        );

        $this->addColumnGetUnidade(
            false,
            true,
            true,
            true,
            'uasg_beneficiaria_id',
            'Unidade Beneficiária',
            'getUnidadeBeneficiaria',
            'uasg_beneficiaria'
        );

        $this->addColumnGetCodigoItens(
            true,
            true,
            true,
            true,
            'tipo_compra_id',
            'Tipo Compra',
            'getTipoCompra',
            'tipo_compra'
        );

        $this->addColumnGetCodigoItens(
            true,
            true,
            true,
            true,
            'modalidade_id',
            'Modalidade',
            'getModalidade',
            'modalidade'
        );

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'numero_ano',
            'Número/Ano'
        );

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'inciso',
            'Inciso'
        );

        $this->addColumnText(
            true,
            true,
            true,
            true,
            'lei',
            'Lei'
        );

        $this->addColumnCreatedAt();
        $this->addColumnUpdatedAt();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ComprasRequest::class);

        CRUD::field('id');
        CRUD::field('unidade_origem_id');
        CRUD::field('unidade_subrrogada_id');
        CRUD::field('modalidade_id');
        CRUD::field('tipo_compra_id');
        CRUD::field('numero_ano');
        CRUD::field('inciso');
        CRUD::field('lei');
        CRUD::field('artigo');
        CRUD::field('created_at');
        CRUD::field('updated_at');
        CRUD::field('deleted_at');
        CRUD::field('uasg_beneficiaria_id');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Atualizar Compras',
        ]);

        $this->setupCreateOperation();
    }
}
