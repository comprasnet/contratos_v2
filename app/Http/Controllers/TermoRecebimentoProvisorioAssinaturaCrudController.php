<?php

namespace App\Http\Controllers;

use App\Http\Requests\TermoRecebimentoProvisorioAssinaturaRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Models\Contrato;
use App\Models\TermoRecebimentoProvisorio;
use App\Services\ModelSignatario\ModelDTO;
use App\Services\ModelSignatario\SignatarioDTO;
use App\Services\ModelSignatario\TermoRecebimentoProvisorioAssinador;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class TermoRecebimentoProvisorioAssinaturaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TermoRecebimentoProvisorioAssinaturaCrudController extends CrudController
{
    use CreateOperation;
    use CommonColumns;
    use Formatador;
    use ImportContent;

    private $trp;
    private $trpAssinador;
    private $contrato;
    private $arquivoGenerico;
    private $signatario;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(TermoRecebimentoProvisorio::class);
        $route = config('backpack.base.route_prefix') . '/trp/' . request()->contrato_id;
        if (request()->has('entrega_id')) {
            $route .= '/' . request()->entrega_id;
        }
        $route .= '/' . request()->id . '/assinatura';
        CRUD::setRoute($route);

        $this->contrato = Contrato::findOrFail(request()->contrato_id);
        $this->exibirTituloPaginaMenu(
            'Contrato ' . $this->contrato->numero . ' - ' . $this->contrato->unidade->codigo,
            'Termos Recebimento Provisório',
            false
        );

        CRUD::setSubheading('Assinar', 'create');

        $this->trp = $this->crud->getEntry(request()->id);
        $this->trpAssinador = new TermoRecebimentoProvisorioAssinador(new ModelDTO($this->trp));
        $this->arquivoGenerico = $this->trp->arquivo;
        $this->signatario = $this->trp->getSignatarioByUsuario();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        abort_if(
            !in_array($this->trp->situacao->descres, ['trp_status_2', 'trp_status_3']),
            403,
            'Não autorizado para assinar TRP.'
        );

        $this->crud->setCreateContentClass('col-md-12');

        $this->importarScriptJs([
            'assets/js/admin/forms/autorizacaoExecucao/submit_assinatura.js',
        ]);

        $this->crud->setOperationSetting('showCancelButton', false);

        $this->crud->addField([
            'name' => 'pdf_preview',
            'type' => 'custom_html',
            'value' => '
                <iframe class="pdf" style="width: 100%; aspect-ratio: 4 / 3; height: 100%"
                    src="' . Storage::url($this->arquivoGenerico->url) . '?temp=' . time() . '"
                    width="800" height="500">
                </iframe>',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        if ($this->signatario) {
            $this->crud->addField([
                'name' => 'pagina_assinatura',
                'label' => 'Página da assinatura',
                'type' => 'label',
                'wrapper' => [
                    'class' => 'pagina_assinatura d-none',
                ],
                'value' => $this->signatario->pivot->pagina_assinatura
            ]);

            $this->crud->addField([
                'name' => 'posicao_x_assinatura',
                'label' => 'Posição X da assinatura',
                'type' => 'label',
                'wrapper' => [
                    'class' => 'posicao_x_assinatura d-none',
                ],
                'value' => $this->signatario->pivot->posicao_x_assinatura
            ]);

            $this->crud->addField([
                'name' => 'posicao_y_assinatura',
                'label' => 'Posição Y da assinatura',
                'type' => 'label',
                'wrapper' => [
                    'class' => 'posicao_y_assinatura d-none',
                ],
                'value' => $this->signatario->pivot->posicao_y_assinatura
            ]);
        }

        $linkVoltar = config('backpack.base.route_prefix') . '/trp/' . request()->contrato_id;
        if (request()->has('entrega_id')) {
            $linkVoltar .= '/' . request()->entrega_id;
        }

        $this->crud->button_custom = [];
        $this->crud->button_custom[] = [
            'button_text' => 'Voltar', 'button_id' => 'voltar',
            'button_name_action' => 'voltar', 'button_value_action' => '0',
            'button_icon' => 'fas fa-arrow-left', 'button_tipo' => 'secondary',
            'onclick' =>  "bloquearSubmit = true;window.location.href='$linkVoltar'"
        ];
        if ($this->trpAssinador->verifyUsuarioCanAssinar()) {
            if ($this->trp->situacao->descres === 'trp_status_2') {
                $this->crud->button_custom[] = [
                    'button_text' => 'Ajustar', 'button_id' => 'ajustar_trp',
                    'button_name_action' => 'ajustar_trp', 'button_value_action' => '1',
                    'button_tipo' => 'warning'
                ];
            }

            $this->crud->button_custom[] = [
                'button_text' => 'Assinar', 'button_id' => 'assinar',
                'button_name_action' => 'recusar_assinatura', 'button_value_action' => '0',
                'button_tipo' => 'primary',
            ];
        }

        $this->data['breadcrumbs'] = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
            'Ordens de Serviço / Fornecimento do Contrato ' . $this->contrato->numero =>
                backpack_url('autorizacaoexecucao/' . $this->contrato->id),
            'Termo de Recebimento Provisório' => $linkVoltar,
            'Assinar' => false,
        ];
    }

    public function assinar()
    {
        try {
            abort_if(
                !strstr(session()->get('_previous.url'), route('assinagov.token')) ||
                !$this->trpAssinador->verifyUsuarioCanAssinar(),
                403
            );

            DB::beginTransaction();

            $this->trpAssinador->assinar(new SignatarioDTO($this->signatario));

            $pathArquivoExplodido = explode('.', $this->arquivoGenerico->url);

            Storage::disk('public')->move($this->arquivoGenerico->url, $this->arquivoGenerico->url . '.old');
            Storage::disk('public')->move(
                $pathArquivoExplodido[0] . '-assinado.' . $pathArquivoExplodido[1],
                $this->arquivoGenerico->url
            );
            Storage::disk('public')->delete($this->arquivoGenerico->url . '.old');

            DB::commit();
            \Alert::add('success', 'Termo de Recebimento Provisório assinado com sucesso!')->flash();
        } catch (Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao assinar o TRP!')->flash();
            $this->inserirLogCustomizado('trp', 'error', $e);
        }

        return redirect($this->crud->route . '/create');
    }
}
