<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Transparencia\TransparenciaController;
use App\Http\Traits\Formatador;
use App\Http\Traits\UgPrimariaTrait;
use App\Models\Unidade;
use App\Repositories\ArpsDashboardRepository;
use App\Repositories\UnidadeRepository;
use App\Services\Pncp\Arp\DashboardArpService;
use Illuminate\Routing\Controller;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    use \App\Http\Traits\ImportContent;
    use Formatador;
    use UgPrimariaTrait;
    
    protected $data = []; // the information we send to the view

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(backpack_middleware());
    }

    /**
     * Show the admin dashboard.
     *
     */
    public function inicio()
    {
        $this->data['title'] = 'Início'; // set the page title
        $this->data['breadcrumbs'] = [
            'Inicio'     => false,
            //            trans('backpack::base.dashboard') => false,
        ];

        $arquivoJS = [
            'assets/js/blockui/jquery.blockUI.js',
            'packages/laravel-paginate/laravel-paginate.js',
            'assets/js/admin/dashboard.js'
        ];


        $this->importarScriptCss([
            'packages/backpack/base/css/dashboard/dashboard.css'
        ]);
        $this->importarScriptJs($arquivoJS);
        $this->data['unidade'] = ArpsDashboardRepository::unidadeSession();

        return view(backpack_view('dashboard'), $this->data);
    }

    /**
     * Redirect to the dashboard.
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function redirect()
    {
        // The '/admin' route is not to be used as a page, because it breaks the menu's active state.
        return redirect(backpack_url('inicio'));
    }
    
    
    public function autenticarUsuarioAutomatico()
    {
        if (!empty(session()->all()['_token'])) {
            $idUsuario =  base64_encode(auth()->id());
            $urlCompleta = \Request::getRequestUri();
            $url = explode("/", $urlCompleta)[2];
            
            return Redirect::to(env('URL_CONTRATO_VERSAO_UM')."{$idUsuario}/{$url}");
        }
    }

    public function alterarUasgUsuario(Request $request)
    {
        $codigoUnidade =  $request->all()['novauasgusuario'];
        $novaUasg = (new UnidadeRepository())->getUnidadePorCodigo($codigoUnidade);
        
        #usag invalida
        if (empty($novaUasg)) {
            $retorno = ['code' => 203];
            $retorno['mensagem'] = 'Unidade inválida';

            return response()->json($retorno);
        }
        
        $novaUnidadeId = $novaUasg->id;
        
        #verifica se é ugprimaria ou secundaria quando n for admin ou admin sup
        if (!backpack_user()->hasRole('Administrador') && !backpack_user()->hasRole('Administrador Suporte')&&
            !backpack_user()->hasRole('Desenvolvedor')) {
            if (!backpack_user()->havePermissionUg($novaUnidadeId)) {
                $retorno = ['code' => 204];
                $retorno['mensagem'] = 'Unidade não permitida';

                return response()->json($retorno);
            }
        }

        $this->inserirRemoverInformacaoUsuarioSessao(
            true,
            $novaUasg->codigo,
            $novaUnidadeId,
            $novaUasg->orgao_id,
            $novaUasg->sisg,
            $novaUasg->nomeresumido
        );
        
        $retorno['code'] = 200;
        $retorno['mensagem'] = "Uasg alterada para {$novaUasg->codigo}";

        $dadosUsuario = backpack_user()->toArray();
        $dadosUsuario['ugprimaria'] = $novaUnidadeId;

        $this->salvarDadosUsuarioRedis($dadosUsuario);
        
        \Alert::add('success', 'Unidade alterada com sucesso!')->flash();

        return response()->json($retorno);
    }

    /**
     * Rota chamada pela página inicial da área logada que busca a lista de atas de registro de preço
     * Utiliza o método da Transparência para unificar a rotina
     * e por não precisar de autenticação para exibir estas informações
     *
     * @return string no formato Json com as atas de registro de preço
     */
    public function listArpDashboard()
    {
        # Utiliza a chamada da lista através do Transparência pois a lista não precisa de autenticação
        return TransparenciaController::listArpDashboard();
    }

    public function getUnidadeSession()
    {
        $arpsRepository = ArpsDashboardRepository::createByRequest();
        return response()->json($arpsRepository->unidadeSession());
    }
    
    public function infoContratos()
    {
        $ambiente = config('app.app_amb');

        if ($ambiente === 'Ambiente Produção') {
            abort('403');
        }
        
        if (!backpack_user()->can('log_visualizar')) {
            abort('403');
        }
        
        phpinfo();
    }
}
