<?php

namespace App\Http\Controllers;

use App\Http\Traits\Formatador;
use App\Models\Municipio;
use App\Services\EstaleiroCEPService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BuscaEnderecoController extends Controller
{
    use Formatador;

    public function buscaCep(Request $request)
    {
        $cep = $this->removeMascaraCPF($request->get('cep'));
        $cpf = $this->removeMascaraCPF(backpack_user()->cpf);

        try {
            $estaleiroCEP = new EstaleiroCEPService($cpf, $cep);
            return response($estaleiroCEP->consultaCep(), 200);
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 404:
                    Log::info('cep não encontrado ' . $e->getMessage());
                    return response()->json(['error' => 'Cep não encontrado.'], 404);
                case 403:
                    Log::info('Erro Consulta CEP ' . $e->getMessage());
                    return response()->json(['error' => $e->getCode()], 403);
            }
        }
    }

    public function buscaMunicipio(Request $request)
    {
        $search_term = $request->input('q');

        $options = Municipio::query();

        if ($search_term) {
            $estado = null;

            if (strpos($search_term, ' - ') !== false) {
                list($cidade, $estado) = explode(' - ', $search_term, 2);
            } else {
                $cidade = $search_term;
            }

            return $options->selectRaw("municipios.id,CONCAT(municipios.nome, ' - ', estados.sigla) as nome")
                ->join('estados', 'estados.id', '=', 'municipios.estado_id')
                ->where('municipios.nome', 'ilike', '%' . mb_convert_case($cidade, MB_CASE_TITLE, "UTF-8") . '%')
                ->when($estado, function ($query) use ($estado) {
                    return $query->where('estados.sigla', 'ilike', '%' . strtoupper($estado) . '%');
                })
                ->orderBy('municipios.nome')
                ->paginate(10);
        }

        return $options
            ->selectRaw("municipios.id,CONCAT(municipios.nome, ' - ', estados.sigla) as nome")
            ->join('estados', 'estados.id', '=', 'municipios.estado_id')
            ->paginate(10);
    }
}
