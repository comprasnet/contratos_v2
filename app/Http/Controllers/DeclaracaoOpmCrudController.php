<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeclaracaoOpmRequest;
use App\Http\Requests\DeclaracaoOpmCancelamentoRequest;
use App\Http\Traits\Autorizacaoexecucao\FiscalizacaoCabecalhoTrait;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\DateFormat;
use App\Models\Adesao;
use App\Models\AdesaoItem;
use App\Models\AutorizacaoExecucao;
use App\Models\CodigoItem;
use App\Models\Contrato;
use App\Models\Contratoarquivo;
use App\Models\ContratoOpm;
use App\Models\Contratoresponsavel;
use App\Repositories\Contrato\ContratoPrepostoRepository;
use App\Repositories\Contrato\ContratoResponsavelRepository;
use App\Repositories\Relatorio\DeclaracaoOpmPdfRepository;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use App\Services\RelatorioPdfService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Operations\CreateOperation;
use App\Http\Traits\CommonFields;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Http\Traits\UgPrimariaTrait;
use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

/**
 * Class AdesaoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */

class DeclaracaoOpmCrudController extends CrudController
{
    use FiscalizacaoCabecalhoTrait;
    use CommonColumns;
    use DateFormat;
    use UpdateOperation;

    use ListOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonFields;
    use Formatador;
    use ImportContent;


    private $autorizacaoExecucaoService;
    private $declaracaoOpmPdfRepository;

    public function __construct(
        AutorizacaoExecucaoService $autorizacaoExecucaoService,
        DeclaracaoOpmPdfRepository $declaracaoOpmPdfRepository
    ) {
        parent::__construct();
        $this->autorizacaoExecucaoService = $autorizacaoExecucaoService;
        $this->declaracaoOpmPdfRepository = $declaracaoOpmPdfRepository;
    }


    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        $this->contrato = $this->getContrato();

        $this->importarScriptJs(['assets/js/declaracaoopm/functions.js']);


        CRUD::setModel(\App\Models\ContratoOpm::class);

        CRUD::setRoute(config('backpack.base.route_prefix') . "/declaracaoopm/{$this->contrato->id}");

        CRUD::setEntityNameStrings(
            " Declaração do Organismo de Políticas para Mulheres (OPM)",
            "Declaração OPM do Contrato nº {$this->contrato->numero}"
        );
        CRUD::setSubheading('Criar', 'create');
       // CRUD::setSubheading('Editar', 'edit');
        CRUD::setSubheading('Visualizar', 'index');

        CRUD::addClause('select', 'contrato_opm.*');
        CRUD::addClause('where', 'contrato_opm.contrato_id', '=', $this->contrato->id);

        $this->crud->addButtonFromModelFunction('line', 'getViewButtonDeclaracao', 'getViewButtonDeclaracao', 'end');


        //$this->crud->addButtonFromModelFunction('line', 'getViewButtonAlterar', 'getViewButtonAlterar', 'end');
    }

    public function getPrepostosResponsaveisContrato(): array
    {
        $contratoResponsavel = new ContratoResponsavelRepository($this->contrato->id);
        $responsaveis = $contratoResponsavel->getResponsaveisContrato();

        return $responsaveis->pluck('user.name', 'id')->toArray();
    }
    public function getPrepostoResponsavelContratoPdf($contratoResponsavelId): string
    {
        $contratoResponsavel = ContratoResponsavel::find($contratoResponsavelId);
        return $contratoResponsavel ? $contratoResponsavel->user->name : 'N/A';
    }

    protected function setupListOperation()
    {
        $this->crud->setOperationSetting('persistentTable', false);
        $this->breadCrumb();

      //  $this->addColumnModelFunction('situacao', 'Situação', 'getSituacao');



//        $this->addColumnDate(
//            true,
//            true,
//            true,
//            true,
//            'data_assinatura',
//            'Data da declaração'
//        );

        CRUD::addColumn([
            'name' => 'data_assinatura',
            'format' => 'DD/MM/Y',
            'type' => 'datetime',
            'label' => 'Data da declaração',
        ]);


        $this->crud->addColumn([
            'name' => 'sequencial',
            'label' => 'Número da Declaração',
            'type' => 'model_function',
            'function_name' => 'getFormataSequencial',
         //   'visibleInTable' => true,
        ]);

        $this->crud->addColumn([
            'name' => 'quantidade_familiar',
            'label' => 'Quantidade de vagas para mulheres em situação de violência',
            'type' => 'number',
            'decimals' => 0,
            'thousands_sep' => '',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'quantidade_contratadas',
            'label' => 'Quantidade de mulheres contratadas',
            'type' => 'number',
            'decimals' => 0,
            'thousands_sep' => '',
            'visibleInTable' => false
        ]);

        $this->crud->addColumn([
            'name' => 'percentual_minimo',
            'label' => 'A empresa contratada está cumprindo o percentual mínimo de 8%',
            'type' => 'boolean',
            'options' => [0 => 'Não', 1 => 'Sim'],
            'visibleInTable' => false
        ]);
        $this->crud->addColumn([
            'name' => 'responsavel_contrato',
            'type' => 'model_function',
            'label' => 'Responsável do Contrato',
            'function_name' => 'getResponsavelContrato',
            'limit' => 100,
            'visibleInTable' => false
        ]);



//        $this->crud->addColumn([
//            'name' => 'sequencial',
//            'type' => 'text',
//            'label' => 'Número/Ano'
//        ]);


//        $this->addColumnDate(
//            true,
//            true,
//            true,
//            true,
//            'created_at',
//            'Criado em'
//        );

//        $this->crud->addButtonFromView(
//            'line',
//            'declaracao_opm_cancelar',
//            'declaracao_opm_cancelar',
//            'end'
//        );
        $this->crud->addButtonFromView('line', 'delete_opm', 'delete_opm', 'end');

        $this->crud->enableExportButtons();
        $this->crud->setListView('vendor.backpack.crud.list-declaracao');
    }

    public function destroy($contrato_id, $id)
    {
        try {
            DB::beginTransaction();

            $dados = ContratoOpm::find($id);

            if (!$dados) {
                return response()->json(['error' => 'Registro não encontrado'], 404);
            }

            $ultimoArquivo = Contratoarquivo::where('contrato_id', $contrato_id)
                ->where('contrato_opm_id', $id)
                ->orderBy('created_at', 'desc')
                ->get();

//            if ($dados->rascunho) {
//                if ($ultimoArquivo) {
//                    $ultimoArquivo->forceDelete();
//                }
//                $dados->forceDelete();
//
//            }
//            $dados->delete(); // Exclusão lógica se não for rascunho
//            $ultimoArquivo->delete();

            if ($dados->rascunho) {
                if ($ultimoArquivo) {
                    foreach ($ultimoArquivo as $arquivo) {
                        $arquivo->forceDelete();
                    }
                }
                $dados->forceDelete();
            } else {
                // Exclusão lógica se não for rascunho
                $dados->delete();
                if ($ultimoArquivo) {
                    foreach ($ultimoArquivo as $arquivo) {
                        $arquivo->delete();
                    }
                }
            }

            DB::commit();
            return response()->json(1); // Retornar resposta de sucesso
        } catch (\Exception $e) {
            DB::rollBack();
            $this->inserirLogCustomizado('', 'error', $e);
            return response()->json(['error' => 'Erro ao excluir registros'], 500);
        }
    }



    public function getProximoNumero(int $contrato_id)
    {
        $ano = Carbon::now()->format('Y');
        $ultimoNumero = ContratoOpm::where('contrato_id', $contrato_id)
            ->where('sequencial', 'like', "%/$ano")
            ->orderBy('sequencial', 'desc')
            ->first('sequencial');
        $result = "00001/$ano";
        if ($ultimoNumero) {
            $numero = explode('/', $ultimoNumero->sequencial);
            $nextNumber = str_pad($numero[0] + 1, 5, '0', STR_PAD_LEFT);

            $result = $nextNumber . '/' . $numero[1];
        }

        return $result;
    }
    private function areaCampos()
    {
        $this->crud->addField([
            'name' => 'separator',
            'type' => 'custom_html',
            'value' => '<hr>',
            'wrapper' => [
                'class' => 'form-group col-md-12',
            ],
        ]);

        $this->crud->addField([
            'name' => 'quantidade_familiar',
            'label' => 'Quantidade de vagas para mulheres em situação de violência doméstica e familiar',
            'class' => 'd-inline',
            'type' => 'number',
            'required' => true,
            'attributes' => ['id' => 'quantidade_familiar'],
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3'
            ]


        ]);
        $this->crud->addField([
            'name' => 'quantidade_contratadas',
            'label' => 'Quantidade de mulheres em situação de violência doméstica e familiar contratadas',
            'class' => 'd-inline',
            'type' => 'number',
            'required' => true,
            'attributes' => ['id' => 'quantidade_contratadas'],
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3'
            ]
        ]);


        $this->crud->addField([

            'name' => 'percentual_minimo',
            'label' => 'A empresa contratada está cumprindo o percentual mínimo de 8% ' .
                        'definido no Decreto nº 11.430/2023?',
            'type' => 'radio',
            'attributes' => [
                'required' => 'required',
                'id' => 'percentual_minimo'
            ],
            'options' => [1 => 'Sim', 0 =>'Não'],
            'default' => 0,
            'inline' => 'd-inline-block',
            'wrapperAttributes' => [
                'class' => 'col-md-4  pt-3'
            ],
            'required' => true,
        ]);
        $this->crud->addField([
            'name' => 'data_assinatura_opm',
            'label' => 'Data da Declaração',
            'type'  => 'date',
            'required' => 'required',
            'attributes' => [
                'required' => 'required',
                'id' => 'data_assinatura_opm'
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3'
            ],
        ]);


        $this->crud->addField([
            'name' => 'sequencial',
            'label' => 'Número da Declaração',
            'type' => 'text',
            'attributes' => [
//                'pattern' => '^\d{5}/\d{4}$',
                'maxlength' => 30,
                'id' => 'sequencial-input',
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3 mt-1'
            ],
            'default' => $this->getProximoNumero($this->contrato->id),
            'required' => true,
        ]);


        $this->crud->addField([
            'name' => 'contrato_responsavel_id',
            'type' =>  'select2_from_array',
           'options' => $this->getPrepostosResponsaveisContrato(),
            'label' => 'Responsável do Contrato',
            'attributes' => [
                'id' => 'contrato_responsavel_id'
            ],
            'wrapper' => [
                'class' => 'col-md-4 pt-3 mt-1',
            ],
            'required' => true,
        ]);

        $this->crud->addField(
            [   // Upload
                'name'      => 'anexo_emitida_orgao',
                'label'     => 'Anexo da declaração emitida pelo órgão',
                'type'      => 'upload_custom',
                'upload'    => true,
                'disk'      => 'local',
                'accept'    => 'application/pdf',
                'attributes' => ['id' => 'anexo_emitida_orgao'],
                'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
                'required' => true
            ]
        );
    }

    protected function setupShowOperation()
    {


        $this->crud->addColumn([
            'name' => 'contrato',
            'type' => 'model_function_raw',
            'label' => 'Contrato',
            'function_name' => 'getContrato',
        ]);
        $this->crud->addColumn([
            'name' => 'fornecedor',
            'type' => 'model_function_raw',
            'label' => 'Fornecedor',
            'function_name' => 'getFornecedor',
        ]);

        $this->addColumnModelFunction(
            'sequencial',
            'Número da declaração',
            'getFormataSequencial'
        );
        $this->crud->addColumn([
            'name' => 'responsavel_contrato',
            'type' => 'model_function',
            'label' => 'Responsável do Contrato',
            'function_name' => 'getResponsavelContrato',
            'limit' => 100
        ]);

        $this->addColumnDate(
            true,
            true,
            true,
            true,
            'created_at',
            'Declaração criada em'
        );
//        $this->addColumnModelFunction('situacao', 'Situação', 'getSituacao');


        $this->crud->addColumn([
            'name' => 'quantidade_familiar',
            'label' => 'Quantidade de vagas para mulheres em situação de violência doméstica e familiar',
            'type' => 'number',
            'decimals' => 0,
            'thousands_sep' => '',
        ]);

        $this->crud->addColumn([
            'name' => 'quantidade_contratadas',
            'label' => 'Quantidade de mulheres em situação de violência doméstica e familiar contratadas',
            'type' => 'number',
            'decimals' => 0,
            'thousands_sep' => '',
        ]);

        $this->crud->addColumn([
            'name' => 'percentual_minimo',
            'label' => 'A empresa contratada está cumprindo o percentual mínimo de 8% ' .
                        'definido no Decreto nº 11.430/2023',
            'type' => 'boolean',
            'options' => [0 => 'Não', 1 => 'Sim'],
        ]);
        $this->crud->addColumn([
            'name' => 'rascunho',
            'label' => 'Rascunho',
            'type' => 'boolean',
            'options' => [0 => 'Não', 1 => 'Sim'],
        ]);

        $id = Route::current()->parameter("id");

        # Recuperar informações da adesão para exibir no show
        $dados = ContratoOpm::find($id);



       // $arpAlteracao = $this->crud->model->find($this->crud->getCurrentEntryId());
        $arpAlteracao = Contratoarquivo::where('contrato_id', $dados->contrato_id)
            ->where('contrato_opm_id', $id)
            ->first();

        $arraySaida = [];

// Se existir o anexo, exibe para o usuário
        if ($arpAlteracao && !empty($arpAlteracao->arquivos)) {
            $arraySaida['Descrição'][0] = $arpAlteracao->descricao;
            $arraySaida['Visualizar'][0] = '<a target="_blank" href="' . url("storage/{$arpAlteracao->arquivos}") . '">
        <i class="fas fa-eye"></i>
    </a>';
        }

        $this->addColumnTable('anexo_alteracao', 'Anexo', $arraySaida);


        $situacaoOpm = $dados->getSituacao();
        $arraySituacaoCancelada = ['Cancelada'];

        if (in_array($situacaoOpm, $arraySituacaoCancelada)) {
            $this->crud->addColumn('justificativa_cancelamento');
            $this->addColumnModelFunction(
                'usuario_cancelamento',
                'Usuário responsável pelo cancelamento',
                'getUsuarioCancelamento'
            );

            $this->addColumnDate(
                false,
                true,
                true,
                true,
                'data_cancelamento',
                'Data do cancelamento'
            );
        }
        $this->addColumnModelFunction(
            'usuario_id',
            'Usuário que criou a declaração',
            'getUsuario'
        );

        $this->addColumnDate(
            false,
            true,
            true,
            true,
            'data_assinatura',
            'Data da declaração'
        );
    }


    protected function setupCreateOperation()
    {
        $this->crud->setCreateContentClass('show.contentClass', 'col-md-12');
        $this->crud->setUpdateContentClass('show.contentClass', 'col-md-12');
        $this->crud->setValidation(DeclaracaoOpmRequest::class);



        $autorizacaoexecucao = null;
        $id = Route::current()->parameter('id');

        if ($id) {
            $autorizacaoexecucao = AutorizacaoExecucao::find($id);
        }

        if (!$autorizacaoexecucao) {
            $autorizacaoexecucao = new AutorizacaoExecucao();
        }



        $this->autorizacaoExecucaoService->setContrato($this->contrato);
        $this->autorizacaoExecucaoService->setAutorizacaoExecucao($autorizacaoexecucao);
        $this->fields = $this->autorizacaoExecucaoService->getFieldLabels();




        $this->crud->button_custom = [
            [
                'button_text' => 'Salvar Rascunho',
                'button_id' => 'rascunho',
                'button_name_action' => 'rascunho',
                'button_value_action' => 1,
                'button_icon' => 'fas fa-edit',
                'button_tipo' => 'secondary'
            ],
            [
                'button_text' => 'Criar Declaração',
                'button_id' => 'adicionar',
                'button_name_action' => 'rascunho',
                'button_value_action' => 0,
                'button_icon' => 'fas fa-save',
                'button_tipo' => 'primary'
            ],
        ];


        $this->cabecalho();
        # Montar a parte dos campos para preenchimento do usuário
        $this->areaCampos();


        $this->breadCrumb(true);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

        $this->crud->setValidation(DeclaracaoOpmRequest::class);
        $id = Route::current()->parameter('id');
        $contratoOpm = ContratoOpm::findOrFail($id);

//
        $anexo = Contratoarquivo::where('contrato_id', $contratoOpm->contrato_id)
            ->where('contrato_opm_id', $id)
            ->first();


        $this->crud->addField([
            'name' => 'anexo_emitida_orgao',
            'label' => 'Anexo da declaração emitida pelo órgão',
            'type' => 'upload_custom_generic',
            'wrapperAttributes' => ['class' => 'col-md-4 pt-3'],
            'attributes' => [
                'id' => 'anexo_emitida_orgao',
                'data-url' => $anexo ? url('storage/' . $anexo->arquivos) : '',
            ],
            'model' => Contratoarquivo::class,
            'upload' => true,
            'subfields' => null,
            'url_arquivo' => 'arquivos',
            'nome_exibir' => 'descricao',
            'colum_filter' => [
                'id' => $anexo ? $anexo->id : null,
            ]
        ]);


        $this->crud->addField([
            'name' => 'data_assinatura_opm',
            'label' => 'Data de declaração',
            'type'  => 'date',
            'value' => $contratoOpm->data_assinatura
                        ? Carbon::parse($contratoOpm->data_assinatura)
                            ->format('Y-m-d')
                        : '',
            'required' => 'required',
            'attributes' => [
                'required' => 'required'
            ],
            'wrapperAttributes' => [
                'class' => 'col-md-4 pt-3'
            ],
        ]);





        $this->breadCrumb(true, 'Editar');
    }

    private function processarAnexoEmitidaOrgao($request, $contratoOpm)
    {
        $file = $request->file('anexo_emitida_orgao');

        // Extrair nome e extensão do arquivo original
        $filenameWithExtension = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        // Definir o nome do arquivo
        $filename = 'Declaracao_n_' . $contratoOpm->sequencial
            . '_do_Contrato_' . $this->contrato->numero . '.' . $extension;

        // Remover caracteres inválidos do nome do arquivo
        $filename = str_replace('/', '_', $filename);

        // Definir o caminho do diretório onde o arquivo será salvo
        $folder = 'app/public/declaracao_pdf/anexos/' .
            date('Y') . '/' . date('m');
        $folderFullPath = storage_path($folder);

        // Verificar se o diretório existe, se não, criar
        if (!is_dir($folderFullPath)) {
            mkdir($folderFullPath, 0777, true);
        }

        // Salvar o arquivo na storage com o nome definido
        $file->move($folderFullPath, $filename);

        // Definir o caminho relativo para salvar no banco de dados
        $path = 'declaracao_pdf/anexos/' . date('Y') .
            '/' . date('m') . '/' . $filename;

        // Salvar registro no banco de dados
        $descricao = 'Declaração nº'.' ' . $contratoOpm->sequencial.' ' .
            ' '.'do Contrato'. ' ' .$this->contrato->numero;
        $situacao = CodigoItem::where('descres', 'DECL_OPM')->first();

        $anexoRecord = new Contratoarquivo();
        $anexoRecord->contrato_id = $this->contrato->id;
        $anexoRecord->contrato_opm_id  = $contratoOpm->id;
        $anexoRecord->descricao = $descricao;
        $anexoRecord->tipo = $situacao->id;
        $anexoRecord->restrito = false;
        $anexoRecord->envio_v2 = true;
        $anexoRecord->rascunho = $contratoOpm->rascunho;
        $anexoRecord->arquivos = $path;
        $anexoRecord->save();
    }
    public function store(DeclaracaoOpmRequest $request)
    {
        try {
            DB::beginTransaction();
            $isRascunho = $request->input('rascunho') == 1;

            $unidadeId = session('user_ug_id');
            $Usuario = auth()->user()->id;

            $contratoOpm = new ContratoOpm();



            $contratoOpm->contrato_id = $this->contrato->id;
            $contratoOpm->quantidade_familiar = $request->input('quantidade_familiar');
            $contratoOpm->quantidade_contratadas = $request->input('quantidade_contratadas');
            $contratoOpm->percentual_minimo = $request->input('percentual_minimo');
            $contratoOpm->percentual_minimo = $request->input('percentual_minimo');
            $contratoOpm->contrato_responsavel_id = $request->input('contrato_responsavel_id');
            $contratoOpm->unidade_id = $unidadeId;
            $contratoOpm->usuario_id = $Usuario;

            $dataAssinaturaInput = $request->input('data_assinatura_opm');

            $contratoOpm->data_assinatura = $dataAssinaturaInput !== null
                ? Carbon::createFromFormat('d/m/Y', $dataAssinaturaInput)->toDateString()
                : null;


            ## Processar e validar o campo sequencial
            $contratoOpm->sequencial = $request->input('sequencial');

            $contratoOpm->rascunho = $isRascunho;
            $situacao = CodigoItem::where('descres', 'opm_ativa')->first();
            if ($contratoOpm->rascunho) {
                $situacao = CodigoItem::where('descres', 'opm_elaboracao')->first();
            }

            $contratoOpm->situacao_id = $situacao->id;
            $contratoOpm->save();


            if ($request->hasFile('anexo_emitida_orgao')) {
                $this->processarAnexoEmitidaOrgao($request, $contratoOpm);
            }
            DB::commit();

//            if (!$contratoOpm->rascunho) {
//                $this->gerarPDF($this->contrato->id, $contratoOpm->id);
//            }

            \Alert::add('success', 'Declaração gerada com sucesso!')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao cadastrar')->flash();
            $this->inserirLogCustomizado('', 'error', $e);
        }

        return redirect('declaracaoopm/' . $this->contrato->id);
    }

    public function update(DeclaracaoOpmRequest $request, $contratoId, $id)
    {

        try {
            DB::beginTransaction();


            $isRascunho = $request->input('rascunho') == 1;

            ## Verificar se já existe uma solicitação ativa para o contrato
//            $solicitacaoAtiva = ContratoOpm::where('contrato_id', $this->contrato->id)
//                ->whereHas('situacao', function ($query) {
//                    $query->where('descres', 'opm_ativa');
//                })
//                ->first();
//
//            ## Se existir uma solicitação ativa e a nova for ativa também, revogar a anterior
//            if ($solicitacaoAtiva && !$isRascunho) {
//                $situacaoRevogada = CodigoItem::where('descres', 'opm_revogada')->first();
//
//                if ($situacaoRevogada) {
//                    $solicitacaoAtiva->situacao_id = $situacaoRevogada->id;
//                    $solicitacaoAtiva->save();
//
//                    $this->gerarPDF($this->contrato->id, $solicitacaoAtiva->id);
//                }
//            }

            $contratoOpm = ContratoOpm::findOrFail($id);
            $unidadeId = session('user_ug_id');
            $Usuario = auth()->user()->id;

            $contratoOpm->contrato_id = $this->contrato->id;
            $contratoOpm->quantidade_familiar = $request->input('quantidade_familiar');
            $contratoOpm->quantidade_contratadas = $request->input('quantidade_contratadas');
            $contratoOpm->percentual_minimo = $request->input('percentual_minimo');
            $dataAssinaturaInput = $request->input('data_assinatura_opm');
            $contratoOpm->unidade_id = $unidadeId;
            $contratoOpm->usuario_id = $Usuario;


            $contratoOpm->data_assinatura = $dataAssinaturaInput !== null
                ? Carbon::createFromFormat('d/m/Y', $dataAssinaturaInput)->toDateString()
                : null;

            $contratoOpm->sequencial = $request->input('sequencial');
            $contratoOpm->contrato_responsavel_id = $request->input('contrato_responsavel_id');

            $contratoOpm->rascunho = $isRascunho;
            $situacao = CodigoItem::where('descres', 'opm_ativa')->first();
            if ($contratoOpm->rascunho) {
                $situacao = CodigoItem::where('descres', 'opm_elaboracao')->first();
            }
            $contratoOpm->situacao_id = $situacao->id;
            // Salva as alterações
            $contratoOpm->save();

            $contratoArquivo = Contratoarquivo::where('contrato_id', $this->contrato->id)
                ->where('contrato_opm_id', $id)
                ->first();
            if ($contratoArquivo) {
                $contratoArquivo->rascunho = $isRascunho;
                $contratoArquivo->save();
            }

            if ($request->hasFile('anexo_emitida_orgao')) {
                $this->processarAnexoEmitidaOrgao($request, $contratoOpm);
            }

            DB::commit();
//
//            if (!$isRascunho) {
//                $this->gerarPDF($this->contrato->id, $contratoOpm->id);
//            }



            \Alert::add('success', 'Atualização realizada com sucesso!')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao cadastrar declaracao OPM!')->flash();
            $this->inserirLogCustomizado('', 'error', $e);
        }

        return redirect('declaracaoopm/' . $contratoOpm->contrato_id);
    }


    public function cancelarDeclaracao(DeclaracaoOpmCancelamentoRequest $request)
    {
        try {
            DB::beginTransaction();
            $opmId = $request->input('opmId');
            $UsuarioId = auth()->user()->id;

            $justificativaCancelamento = $request->input('justificativaCancelamento');

            $contratoOpm = ContratoOpm::findOrFail($opmId);

            // Atualizar os campos
            $contratoOpm->data_cancelamento = now();
            $contratoOpm->justificativa_cancelamento = $justificativaCancelamento;
            $contratoOpm->usuario_id_cancelamento = $UsuarioId;

            // Atualizar a situação para 'opm_cancelada'
            $situacaoCancelada = CodigoItem::where('descres', 'opm_cancelada')->first();
            if ($situacaoCancelada) {
                $contratoOpm->situacao_id = $situacaoCancelada->id;
            }

            $contratoOpm->save();

            DB::commit();


            $this->processarAnexoEmitidaOrgao($request, $contratoOpm);

//            $this->gerarPDF($this->contrato->id, $contratoOpm->id);

            return $this->montarRespostaAlertJs(200, 'Declaração cancelada com sucesso', "success");
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao cancelar a solicitação: ' . $e->getMessage())->flash();
       //     $this->inserirLogCustomizado('declaracao_opm', 'error', $e);
        }

        return redirect('declaracaoopm/' . $this->contrato->id);
    }

    public function buscarPDF($contrato_id, $id)
    {


        $anexo = Contratoarquivo::where('contrato_id', $contrato_id)
            ->where('contrato_opm_id', $id)
            ->first();

        $contratoOpm = ContratoOpm::find($id);

        $pdfPath = $anexo->arquivos;

        $filename = 'Declaracao_n_' . str_replace('/', '-', $contratoOpm->sequencial) .
            '_do_Contrato_' . str_replace('/', '-', $this->contrato->numero) . '.pdf';

        // Definir o nome do arquivo


        //$filename = 'Declaração OPM nº' . $this->contrato->id . '_' .$id. '.pdf';

      //  $pdfPath = 'declaracao_pdf/anexos/' . date('Y') . '/' . date('m') . '/' . $filename;


        if (Storage::disk('public')->exists($pdfPath)) {
            return Storage::disk('public')->download($pdfPath, $filename);
        } else {
            \Alert::add('error', 'Erro no download do pdf')->flash();
            return redirect()->back();
        }
    }


    public function gerarPDF($contrato_id, $id)
    {

        $items = $this->declaracaoOpmPdfRepository->getCabecalhoContratoPdf($contrato_id, $id);
        $dadosOpm = $this->declaracaoOpmPdfRepository->getContratoOpm($contrato_id, $id);

        $responsavel = $this->getPrepostoResponsavelContratoPdf($dadosOpm->contrato_responsavel_id);

        $dompdf = RelatorioPdfService::create();
        $logo = 'data:image/png;base64,' . base64_encode(
            file_get_contents(public_path('img/logo_relatorio.png'))
        );



        $html = view('relatorio.declaracao-opm-pdf', [
            'titulo' => 'Declaração do Organismo de Políticas para Mulheres (OPM)',
            'subtitulo' => 'Contrato nº ' . $this->contrato->numero,
            'unidadeGerenciadora' => 'Unidade Gestora Atual ' .
                $this->contrato->unidade->codigosiasg . ' - ' .
                $this->contrato->unidade->nome,
            'logo' => $logo,
            'items' => $items,
            'dadosOpm' => $dadosOpm,
            'responsavel' => $responsavel,


        ])->render();

        $dompdf->loadHtml($html);
        $dompdf->render();

        $font = $dompdf->getFontMetrics()->getFont(
            "helvetica",
            "normal"
        );
        $canvas = $dompdf->getCanvas();

        $canvas->page_text(
            35,
            820,
            "Contrato nº {$this->contrato->numero} - " .
            "{$this->contrato->unidade->codigosiasg}",
            $font,
            10,
        );

        $canvas->page_text(
            565,
            820,
            "{PAGE_NUM}/{PAGE_COUNT}",
            $font,
            10,
        );
        ## Definir caminho do arquivo
        $pdfPath = 'Relatorio-opm'.'-'.$this->contrato->id . '.pdf';
        $folder = 'app/public/declaracao_pdf/pdf/' . date('Y-m');
        $folderFullPath = storage_path($folder);


        if (!is_dir($folderFullPath)) {
            mkdir($folderFullPath, 0777, true);
        }

        $pdfFullPath = $folderFullPath . '/' . $pdfPath;
        $descricao = 'Declaração OPM nº' . ' ' . $this->contrato->numero;

        ## Salvar registro no banco de dados
        $situacao = CodigoItem::where('descres', 'DECL_OPM')->first();

        $pdfRecord = new Contratoarquivo();
        $pdfRecord->contrato_id = $this->contrato->id;
        $pdfRecord->descricao = $descricao;
        $pdfRecord->tipo = $situacao->id;
        $pdfRecord->restrito = false;
        $pdfRecord->envio_v2 = true;

        $filename = 'Declaração OPM nº' . $this->contrato->id . '_' .$dadosOpm->id . '.pdf';

        $path = 'declaracao_pdf/pdf/' .
                Carbon::now()->format('Y') . '/' .
                Carbon::now()->format('m') . '/' .
                $filename;

            Storage::disk('public')->put($path, $dompdf->output());

        $pdfRecord->arquivos = $path;
        $pdfRecord->save();

         return Storage::disk('public')->download($path, $filename);

        //return $dompdf->stream('', array("Attachment" => false),$pdfUrl);
    }


    private function breadCrumb(bool $acao = false, $texto = 'Adicionar')
    {
        $contrato = $this->getContrato();


        $breadcrumb = [
            trans('backpack::crud.admin') => backpack_url('dashboard'),
            'Fiscalização e Gestão de Contratos' => backpack_url('meus-contratos'),
            'Declaração (OPM) com o n ' . $contrato->numero => false,
            'Lista' => false,
        ];

        if ($acao) {
            $breadcrumb = [
                trans('backpack::crud.admin') => backpack_url('dashboard'),
                'Declaração do Organismo de Políticas para Mulheres ' => backpack_url('meus-contratos'),
                'Declaração OPM do Contrato nº ' . $contrato->numero => false,
                $texto => false,
                // 'Voltar' => backpack_url('declaracaoopm/' . $contrato->id),
            ];
        }

        $this->data['breadcrumbs'] = $breadcrumb;
    }
}
