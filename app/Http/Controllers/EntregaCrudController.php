<?php

namespace App\Http\Controllers;

use App\Http\Requests\EntregaRequest;
use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoCrudTrait;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\EntregaTrpTrd\EntregaTrpTrdSetupRootTrait;
use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoExecucaoEntrega;
use App\Models\CodigoItem;
use App\Models\Entrega;
use App\Repositories\Contrato\ContratoParametroRepository;
use App\Repositories\Contrato\ContratoResponsavelRepository;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use App\Services\EntregaTrpTrd\EntregaService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Library\Widget;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class EntregaCrudController extends CrudController
{

    use EntregaTrpTrdSetupRootTrait;

    public function setup()
    {
        $this->setupRoot(Entrega::class);

        $route = config('backpack.base.route_prefix') . '/entrega/' . request()->contrato_id;
        if (request()->autorizacaoexecucao_id) {
            $route .= '/' . request()->autorizacaoexecucao_id;
            CRUD::addClause('select', 'entregas.*');
            CRUD::addClause('join', 'entrega_autorizacaoexecucao', function ($join) {
                $join->on('entrega_autorizacaoexecucao.entrega_id', 'entregas.id')
                    ->where(
                        'entrega_autorizacaoexecucao.autorizacaoexecucao_id',
                        '=',
                        request()->autorizacaoexecucao_id
                    );
            });
        }

        CRUD::setRoute($route);

        CRUD::setSubheading('', 'create');
        CRUD::setSubheading('', 'edit');
        CRUD::setSubheading('', 'index');

        $this->crud->addButtonFromModelFunction(
            'line',
            'getViewButtons',
            'getViewButtons',
        );

        CRUD::addButtonFromView('line', 'duplicar', 'duplicar', 'beginning');
        CRUD::modifyButton('duplicar', ['title' => 'Duplicar Entrega']);

        $this->exibirTituloPaginaMenu(
            'Contrato ' . $this->contrato->numero . ' - ' . $this->contrato->unidade->codigo,
            'Entregas',
            false
        );
    }

    protected function setupShowOperation()
    {
        $this->crud->addColumn([
            'name' => 'numero',
            'label' => 'Número Entrega',
            'type' => 'text',
            'function_name' => 'numero'
        ]);

        $this->crud->addColumn([
            'name' => 'situacao',
            'label' => 'Situação',
            'type' => 'model_function',
            'function_name' => 'getSituacao',
            'limit' => 9999,
        ]);

        $this->crud->addColumn([
            'name' => 'valor_total',
            'label' => 'Valor Total Entrega',
            'type' => 'model_function',
            'function_name' => 'getValorTotalFormatado'
        ]);

        $this->crud->addColumn([
            'name' => 'data_entrega',
            'label' => 'Data Entrega',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_inicio',
            'label' => 'Data início',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_fim',
            'label' => 'Data fim',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_prevista_trp',
            'label' => 'Data prevista para o recebimento provisório',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_prevista_trd',
            'label' => 'Data prevista para o recebimento definitivo',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'locais_execucao_entrega',
            'type' => 'model_function_raw',
            'label' => 'Locais de Execução',
            'function_name' => 'getLocaisExecucao',
        ]);

        $this->crud->addColumn([
            'name' => 'mes_ano_referencia',
            'label' => 'Mês/Ano de Referência',
            'type' => 'model_function',
            'function_name' => 'getMesAnoReferencia'
        ]);

        $this->addColumnText(
            false,
            true,
            true,
            true,
            'informacoes_complementares',
            'Informações Complementares'
        );

        $this->crud->addColumn([
            'name' => 'itens',
            'type' => 'model_function_raw',
            'label' => 'Itens',
            'function_name' => 'getTableItensView',
        ]);

        $this->crud->addColumn([
            'name' => 'anexos',
            'type' => 'model_function_raw',
            'label' => 'Anexos',
            'function_name' => 'getAnexosViewLink',
        ]);

        $this->breadcrumb(true, 'Visualizar');
    }

    protected function setupListOperation()
    {
        $this->crud->addButton('top', 'create', 'view', 'crud::buttons.button_criar_entrega');

        $this->setupListOperationRoot();

        // FILTERS
        $this->crud->addFilter(
            [
                'type'  => 'select2',
                'name'  => 'situacao_id',
                'label' => 'Situação',
            ],
            $this->retornaArrayCodigosItens('Situação Entrega'),
            function ($value) {
                $this->crud->addClause('where', 'situacao_id', '=', "$value");
            }
        );

        $this->crud->addFilter(
            [
                'name'  => 'data_entrega',
                'type'  => 'date_range',
                'label' => 'Data Entrega'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $fromDate = Carbon::parse($dates->from);
                $toDate = Carbon::parse($dates->to)->endOfDay();

                $this->crud->addClause('where', 'data_entrega', '>=', $fromDate);
                $this->crud->addClause('where', 'data_entrega', '<=', $toDate);
            }
        );

        $this->crud->addFilter(
            [
                'type' => 'date',
                'name' => 'data_inicio',
                'label' => 'Data Início',
                'language' => 'pt-BR'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'data_inicio', '>=', $value);
            }
        );

        $this->crud->addFilter(
            [
                'type' => 'date',
                'name' => 'data_fim',
                'label' => 'Data Fim',
                'language' => 'pt-BR'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'data_fim', '<=', $value);
            }
        );

        $this->crud->addFilter(
            [
                'type' => 'date_range',
                'name' => 'data_prevista_trp',
                'label' => 'Previsão para o TRP',
                'language' => 'pt-BR'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $fromDate = Carbon::parse($dates->from);
                $toDate = Carbon::parse($dates->to)->endOfDay();

                $this->crud->addClause('where', 'data_prevista_trp', '>=', $fromDate);
                $this->crud->addClause('where', 'data_prevista_trp', '<=', $toDate);
            }
        );

        $this->crud->addFilter(
            [
                'type' => 'date_range',
                'name' => 'data_prevista_trd',
                'label' => 'Previsão para o TRP',
                'language' => 'pt-BR'
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $fromDate = Carbon::parse($dates->from);
                $toDate = Carbon::parse($dates->to)->endOfDay();

                $this->crud->addClause('where', 'data_prevista_trd', '>=', $fromDate);
                $this->crud->addClause('where', 'data_prevista_trd', '<=', $toDate);
            }
        );

        // COLUMNS

        $this->crud->addColumn([
            'name' => 'mes_ano_referencia',
            'label' => 'Mês/Ano Referência',
            'type' => 'model_function',
            'function_name' => 'getMesAnoReferencia',
        ]);

        $this->crud->addColumn([
            'name' => 'data_inicio',
            'label' => 'Data Início',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_fim',
            'label' => 'Data fim',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_entrega',
            'label' => 'Data Entrega',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_prevista_trp',
            'label' => 'Previsão para o TRP',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'data_prevista_trd',
            'label' => 'Previsão para o TRD',
            'type' => 'date'
        ]);
    }

    public function setupCreateOperation()
    {
        $this->setupCreateOperationRoot();

        $this->crud->external = request()->external ?? 0;
        if (request()->id) {
            $entrega = $this->crud->getEntry(request()->id);
            $this->crud->external = $entrega->originado_sistema_externo;
        }

        $this->crud->addField([
            'name' => 'vinculos_entrega',
            'type' => 'custom_html',
            'value' => view(
                'crud::autorizacaoexecucao.vinculo.adicionar-vinculos-entrega',
                [
                    'crud' => $this->crud,
                    'opcoes_vinculos' => AutorizacaoExecucao::where('contrato_id', $this->contrato->id)
                        ->whereIn(
                            'situacao_id',
                            CodigoItem::whereIn('descres', ['ae_status_2', 'ae_status_8'])->pluck('id')
                        )
                        ->orderBy('numero')
                        ->get(),
                    'vinculos' => empty(request()->id) ? null : $entrega->autorizacoes->pluck('id')->toArray()
                ]
            ),
        ]);

        $this->crud->addField([
            'name' => 'originado_sistema_externo',
            'type' => 'hidden',
            'label' => 'Originado Sistema Externo',
            'value' => (int) $this->crud->external,
            'attributes' => ['readonly' => 'readonly'],
        ]);

        $this->crud->addField([
            'name' => 'informacoes_complementares',
            'type' => 'tinymce',
            'label' => 'Informações Complementares',
            'options' => [
                'language' => 'pt_BR',
                'menubar' => false,
                'plugins' => [
                    'print autolink lists link table',
                ],
                'toolbar' => 'print | undo redo | copy cut paste |' .
                    ' bold italic underline strikethrough superscript subscript | 
                    backcolor forecolor link unlink blockquote | alignleft aligncenter alignright alignjustify | 
                    bullist numlist outdent indent | hr table removeformat',
            ],
        ]);

        $this->crud->addField([
            'name' => 'locais_execucao',
            'label' => 'Locais de Execução para Entrega',
            'type' => 'select2_from_array',
            'attribute' => 'descricao',
            'required' => false,
            'model' => 'App\Models\ContratoLocalExecucao',
            'value' => empty(request()->id) ? null : $entrega->locaisExecucao()->pluck('id')->toArray(),
            'options' => $this->contrato->locaisExecucao()->pluck('descricao', 'id')->toArray(),
            'allows_multiple' => true,
            'wrapperAttributes' => ['class' => 'form-group col-md-12 mt-1'],
        ]);

        $this->crud->addField([
            'name' => 'mes_ano_referencia',
            'label' => 'Mês/Ano de Referência',
            'type' => 'date_month',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4 mt-1'
            ],
            'required' => true,
        ]);

        $this->crud->addField([
            'name' => 'data_inicio',
            'label' => 'Período ínicio da entrega',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4 mt-1'
            ],
            'required' => true,
        ]);

        $this->crud->addField([
            'name' => 'data_fim',
            'label' => 'Período fim da entrega',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4 mt-1'
            ],
            'required' => true,
        ]);

        $dataPrazoRecebimentoProvisorio = null;
        $dataPrazoRecebimentoDefinitivo = null;
        if (!$this->crud->external) {
            $contratoParametroRepository = new ContratoParametroRepository(request()->contrato_id);
            $dataPrazoRecebimentoProvisorio = $contratoParametroRepository->getDataPrazoRecebimentoProvisorio();
            $dataPrazoRecebimentoDefinitivo = $contratoParametroRepository->getDataPrazoRecebimentoDefinitivo();
        }

        $this->crud->addField([
            'name' => 'data_entrega',
            'label' =>  'Data efetiva da entrega',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4 mt-1'
            ],
            'required' => true,
            'value' => $entrega->data_entrega ?? date('Y-m-d')
        ]);

        $this->crud->addField([
            'name' => 'data_prevista_trp',
            'label' =>  'Data prevista para o recebimento provisório',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4 mt-1'
            ],
            'required' => !$this->crud->external,
            'value' => $entrega->data_prevista_trp ?? $dataPrazoRecebimentoProvisorio
        ]);

        $this->crud->addField([
            'name' => 'data_prevista_trd',
            'label' =>  'Data prevista para recebimento definitivo',
            'type' => 'date',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4 mt-1'
            ],
            'required' => !$this->crud->external,
            'value' => $entrega->data_prevista_trd ?? $dataPrazoRecebimentoDefinitivo
        ]);

        $codigoItem = CodigoItem::where('descres', 'ae_entrega_arquivo')->first();

        $entrega = request()->id ? Entrega::find(request()->id) : new Entrega();
        $anexos = $entrega->anexos()->where('tipo_id', $codigoItem->id)->get();

        $this->crud->addField([
            'name' => 'anexos',
            'label' => 'Anexos - Entrega',
            'type' => 'upload_anexos_autorizacaoexecucao',
            'anexos' => $anexos->toArray(),
            'accept' => '*',
        ]);

        $buttonText = $this->crud->external ? 'Informar Recebimento' : 'Comunicar Recebimento';
        $this->crud->button_custom = [
            ['button_text' => 'Salvar Rascunho', 'button_id' => 'rascunho',
                'button_name_action' => 'rascunho', 'button_value_action' => '1',
                'button_icon' => 'fas fa-edit', 'button_tipo' => 'secondary'],
            ['button_text' => $buttonText, 'button_id' => 'informar',
                'button_name_action' => 'rascunho', 'button_value_action' => '0',
                'button_icon' => 'fas fa-save', 'button_tipo' => 'primary']
        ];

        $this->breadcrumb(true);
    }

    public function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('show.contentClass', 'col-md-12');

        $this->setupCreateOperation();

        $this->breadCrumb(true, 'Editar');
    }

    public function store(EntregaRequest $request)
    {
        try {
            DB::beginTransaction();
            $entregaService = new EntregaService();
            $entregaService->create($request->validated(), $request->contrato_id);
            DB::commit();
            \Alert::add('success', 'Entrega criada com sucesso!')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->inserirLogCustomizado('entrega', 'error', $e);
            \Alert::add('error', 'Erro ao criar a entrega!')->flash();
        }

        return redirect($this->crud->getRoute());
    }

    public function update(EntregaRequest $request)
    {
        try {
            DB::beginTransaction();
            $entregaService = new EntregaService();
            $entregaService->update($request->validated(), $request->id);
            DB::commit();
            \Alert::add('success', 'Entrega alterada com sucesso!')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->inserirLogCustomizado('entrega', 'error', $e);
            \Alert::add('error', 'Erro ao alterar a entrega!')->flash();
        }

        return redirect($this->crud->getRoute());
    }

    public function duplicar(Request $request)
    {
        try {
            DB::beginTransaction();
            $entregaService = new EntregaService();
            $entregaService->duplicar($request->entrega_id);

            DB::commit();

            \Alert::add('success', 'Duplicação realizada com sucesso!')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            \Alert::add('error', 'Erro ao duplicar a entrega!')->flash();
            $this->inserirLogCustomizado('entrega', 'error', $e);
        }

        return redirect()->back();
    }
}
