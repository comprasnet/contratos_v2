<?php

namespace App\Http\Controllers;

use App\Models\AmparoLegal;
use App\Repositories\AmparoLegalRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AmparoLegalCrudController extends Controller
{
    public function searchByAtoNormativo(Request $request)
    {
        $search_term = $request->input('q');

        $amparoLegalRepository = new AmparoLegalRepository();

        return $amparoLegalRepository->getPaginateByAtoNormativo($search_term);
    }
}
