<?php

namespace App\Http\Controllers;

use App\Services\AssinaPdfService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AssinaGovController extends Controller
{
    private $client;
    private $host;
    private $response_type;
    private $client_id;
    private $scope;
    private $redirect_uri;
    private $nonce;
    private $state;
    private $secret;
    private $api_assinatura;

    public function __construct(Request $request)
    {
        $this->client         = new Client();
        $this->host           = config('assinagov.host');
        $this->response_type  = 'code';
        $this->client_id      = config('assinagov.client_id');
        $this->scope          = 'sign';
        $this->redirect_uri   = config('app.url') . '/assinagov';
        $this->nonce          = Str::random(12);
        $this->state          = Str::random(13);
        $this->secret         = config('assinagov.secret');
        $this->api_assinatura = config('assinagov.api_assinatura');
    }

    public function autorizacao(Request $request)
    {
        if (session()->has('fornecedor_id')) {
            $this->redirect_uri = config('app.url') . '/fornecedor/assinagov';
            if (!session()->has('assinagov')) {
                $urlAnterior = request()->headers->get('Referer');
                $arrayUrl = explode('/', $urlAnterior);
                $contrato_id = $arrayUrl[5];
                $autorizacaoexecucao_id = $arrayUrl[6];


                $request->request->set('redirectSuccess', config('app.url')
                    . "/fornecedor/autorizacaoexecucao/{$contrato_id}/{$autorizacaoexecucao_id}/assinatura/assinar");
            }
        }

        $url = $this->host
            . '/authorize?response_type=' . $this->response_type
            . '&client_id=' . $this->client_id
            . '&scope=' . $this->scope
            . '&redirect_uri=' . urlencode($this->redirect_uri . '/token')
            . '&nonce=' . $this->nonce
            . '&state=' . $this->state;

        if (!session()->has('assinagov')) {
            Log::info('AssinaGovController: assinagov na sessão');
            session()->put([
                'assinagov' => [
                    'fornecedor' => session()->has('fornecedor_id') ? true : false,
                    'pathArquivo' => $request->get('pathArquivo'),
                    'pagina_assinatura' => $request->get('pagina_assinatura'),
                    'posicao_x_assinatura' => $request->get('posicao_x_assinatura'),
                    'posicao_y_assinatura' => $request->get('posicao_y_assinatura'),
                    'redirectError' => $request->get('redirectError') ?? session()->get('_previous.url'),
                    'redirectSuccess' => $request->get('redirectSuccess') ?? session()->get('_previous.url'),
                    //tem que ter o parametro na rota para assinar e não oldessa redirect success
                ]
            ]);
            session()->remove('error_nivel');
            session()->remove('error_msg');

            return redirect()->route('acessogov.autorizacao');
        } elseif (session()->has('error_msg')) {
            Log::info('AssinaGovController: retorna erro de nível');
            \Alert::error(session()->get('error_msg'))->flash();
            session()->remove('error_nivel');
            session()->remove('error_msg');
            $redirectError = session()->get('assinagov.redirectError');
            session()->remove('assinagov');
            return redirect()->to($redirectError);
        }

        Log::info('AssinaGovController: segue com assinatura');

        return Redirect::away($url);
    }

    public function token(Request $request)
    {
        abort_unless(session()->get('assinagov'), 400, 'Assinatura não encontrada, por favor tente novamente.');

        try {
            throw_if($request->get('error'), new \Exception($request->get('error')));

            // Obter access token a partir do código de autorização
            $accessToken = $this->getAccessToken($request->get('code'));
            // Download do certificado público
            $this->getCertificadoPublico($accessToken);

            $assinaturaPdfService = new AssinaPdfService(session()->get('assinagov.pathArquivo'));

            $conteudoByteRange = $assinaturaPdfService->getConteudoByteRange();

            $hash = hash('sha256', $conteudoByteRange, true);
            $assinaturaBase64 = $this->getAssinaturaBase64($hash, $accessToken);

            $conteudoComAssinatura = $assinaturaPdfService->addAssinaturaDigital($assinaturaBase64);

            $pathArquivoExplodido = explode('.', session()->get('assinagov.pathArquivo'));
            Storage::disk('public')->put(
                $pathArquivoExplodido[0] . '-assinado.' . $pathArquivoExplodido[1],
                $conteudoComAssinatura
            );
        } catch (GuzzleException $e) {
            Log::error($e);
            if ($e->getCode() === 403) {
                \Alert::error('Cidadão não possui a identidade (Prata ou Ouro) necessária para uso da assinatura' .
                    ' eletrônica digital.')->flash();
            } else {
                \Alert::error('Ocorreu um erro ao se comunicar com o acesso.gov, tente novamente mais tarde.')->flash();
            }
            return redirect()->to(session()->get('assinagov.redirectError'));
        } catch (\Exception $e) {
            Log::error($e);
            \Alert::error('Ocorreu um erro ao se comunicar com o acesso.gov, tente novamente mais tarde.')->flash();
            return redirect()->to(session()->get('assinagov.redirectError'));
        }

        $redirectToSuccess = redirect()->to(session()->get('assinagov.redirectSuccess'));
        session()->remove('assinagov');

        return $redirectToSuccess;
    }

    private function getAccessToken(string $code)
    {
        $response = $this->client->request(
            'POST',
            $this->host . '/token',
            [
                'form_params' => [
                    'grant_type' => 'authorization_code',
                    'code' => $code,
                    'client_id' => $this->client_id,
                    'client_secret' => $this->secret,
                    'redirect_uri' => $this->redirect_uri . '/token'
                ]
            ]
        );

        $result = json_decode($response->getBody()->getContents());
        return $result->access_token;
    }

    private function getCertificadoPublico(string $accessToken)
    {
        $response = $this->client->request(
            'GET',
            $this->api_assinatura . '/certificadoPublico',
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $accessToken
                ]
            ]
        );

        return $response->getBody()->getContents();
    }

    private function getAssinaturaBase64($hash256ByteRange, $accessToken)
    {
        $response = $this->client->request('POST', $this->api_assinatura . '/assinarPKCS7', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $accessToken
            ],
            'body' => json_encode(array('hashBase64' => base64_encode($hash256ByteRange)))
        ]);

        return $response->getBody()->getContents();
    }
}
