<?php

namespace App\Http\Controllers;

use App\Http\Requests\TermoRecebimentoProvisorioDefinitivoRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\EntregaTrpTrd\EntregaTrpTrdSetupRootTrait;
use App\Models\CodigoItem;
use App\Models\TermoRecebimentoDefinitivo;
use App\Models\TermoRecebimentoProvisorio;
use App\Repositories\Contrato\ContratoResponsavelRepository;
use App\Services\EntregaTrpTrd\TermoRecebimentoDefinitivoService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TermoRecebimentoDefinitivoCrudController extends CrudController
{
    use EntregaTrpTrdSetupRootTrait;
    use CommonColumns;

    public function setup()
    {
        $this->setupRoot(TermoRecebimentoDefinitivo::class);
        $route = config('backpack.base.route_prefix') . '/trd/' . request()->contrato_id;
        if (request()->trp_id) {
            $route .= '/' . request()->trp_id;
            CRUD::addClause('select', 'termos_recebimento_definitivo.*');
            CRUD::addClause('join', 'termo_recebimento_definitivo_termo_recebimento_provisorio', function ($join) {
                $join->on(
                    'termo_recebimento_definitivo_termo_recebimento_provisorio.termo_recebimento_definitivo_id',
                    'termos_recebimento_definitivo.id'
                )->where(
                    'termo_recebimento_definitivo_termo_recebimento_provisorio.termo_recebimento_provisorio_id',
                    '=',
                    request()->trp_id
                );
            });
        }

        CRUD::setRoute($route);

        $this->crud->addButtonFromModelFunction(
            'line',
            'getViewButtons',
            'getViewButtons',
        );

        $this->crud->addButtonFromView(
            'line',
            'button_assinar_termo_recebimento_definitivo',
            'button_assinar_termo_recebimento_definitivo',
            'end'
        );

        $this->crud->addButtonFromView(
            'line',
            'button_pdf_termo_recebimento_definitivo',
            'button_pdf_termo_recebimento_definitivo',
            'end'
        );

        $this->exibirTituloPaginaMenu(
            'Contrato ' . $this->contrato->numero . ' - ' . $this->contrato->unidade->codigo,
            'Termos Recebimento Definitivo',
            false
        );
    }

    protected function setupListOperation()
    {
        $this->crud->prefix_text_button_redirect_create = 'Elaborar Novo';
        $this->crud->text_button_redirect_create = 'TRD';

        // FILTERS
        $this->crud->addFilter(
            [
                'type'  => 'select2',
                'name'  => 'situacao_id',
                'label' => 'Situação',
            ],
            $this->retornaArrayCodigosItens('Situação Termo de Recebimento Definitivo'),
            function ($value) {
                $this->crud->addClause('where', 'situacao_id', '=', "$value");
            }
        );

        $this->setupListOperationRoot();

        $this->crud->addColumn([
            'name' => 'status_assinaturas_signatarios',
            'type' => 'model_function',
            'label' => 'Status Assinaturas dos Signatários',
            'function_name' => 'getViewStatusAssinaturasSignatarios',
            'limit' => 999999999
        ]);
    }

    protected function setupShowOperation()
    {
        $this->crud->addColumn([
            'name' => 'numero',
            'label' => 'Número',
            'type' => 'text',
            'function_name' => 'numero'
        ]);

        $this->crud->addColumn([
            'name' => 'osfs',
            'type' => 'model_function',
            'label' => 'OS/Fs',
            'function_name' => 'getRedirectAutorizacoesComponent',
            'limit' => 99999999999999999,
        ]);

        $this->crud->addColumn([
            'name' => 'arquivo',
            'type' => 'model_function_raw',
            'label' => 'Arquivo TRD',
            'function_name' => 'getArquivoViewLink',
        ]);

        $this->crud->addColumn([
            'name' => 'situacao',
            'label' => 'Situação',
            'type' => 'model_function',
            'function_name' => 'getSituacao',
            'limit' => 9999,
        ]);

        $this->crud->addColumn([
            'name' => 'itens',
            'type' => 'model_function_raw',
            'label' => 'Itens',
            'function_name' => 'getTableItensView',
        ]);

        $this->crud->addColumn([
            'name' => 'valor_total',
            'label' => 'Valor Total',
            'type' => 'model_function',
            'function_name' => 'getValorTotalFormatado',
        ]);

        $this->crud->addColumn([
            'name' => 'instrumento_cobranca_label',
            'label' => 'Inclui instrumento de cobrança?',
            'type' => 'label',
        ]);

        $this->addColumnText(
            false,
            true,
            true,
            true,
            'introducao',
            'Introdução',
        );

        $this->addColumnText(
            false,
            true,
            true,
            true,
            'informacoes_complementares',
            'Infomações Complementares',
        );

        $this->breadcrumb(true, 'Visualizar');
    }

    protected function setupCreateOperation()
    {
        $this->importarScriptJs(['assets/js/admin/forms/autorizacaoExecucao/submit_assinatura.js']);
        $this->setupCreateOperationRoot();

        $this->crud->external = request()->external ?? 0;
        if (request()->id) {
            $trp = $this->crud->getEntry(request()->id);
            $this->crud->external = $trp->originado_sistema_externo;
        }

        $this->crud->addField([
            'name' => 'introducao',
            'type' => 'tinymce',
            'label' => 'Introdução',
            'required' => true,
            'options' => [
                'language' => 'pt_BR',
                'menubar' => false,
                'plugins' => [
                    'print autolink lists link table',
                ],
                'toolbar' => 'print | undo redo | copy cut paste |' .
                    ' bold italic underline strikethrough superscript subscript | 
                    backcolor forecolor link unlink blockquote | alignleft aligncenter alignright alignjustify | 
                    bullist numlist outdent indent | hr table removeformat',
            ],
        ]);

        $this->crud->addField([
            'name' => 'vinculos_trp',
            'type' => 'custom_html',
            'value' => view(
                'crud::autorizacaoexecucao.vinculo.adicionar-vinculos-trd',
                [
                    'crud' => $this->crud,
                    'opcoes_vinculos' => TermoRecebimentoProvisorio::with(
                        'itens',
                        'itens.entregaItem.entrega.locaisExecucao'
                    )->where('contrato_id', $this->contrato->id)
                        ->where('situacao_id', CodigoItem::where('descres', 'trp_status_3')->first()->id)
                        ->orderBy('numero')
                        ->get(),
                    'vinculos' => empty(request()->id) ? null : null //$trp->autorizacoes->pluck('id')->toArray()
                ]
            ),
        ]);

        $this->crud->addField([
            'name' => 'informacoes_complementares',
            'type' => 'tinymce',
            'label' => 'Informações Complementares',
            'options' => [
                'language' => 'pt_BR',
                'menubar' => false,
                'plugins' => [
                    'print autolink lists link table',
                ],
                'toolbar' => 'print | undo redo | copy cut paste |' .
                    ' bold italic underline strikethrough superscript subscript | 
                    backcolor forecolor link unlink blockquote | alignleft aligncenter alignright alignjustify | 
                    bullist numlist outdent indent | hr table removeformat',
            ],
        ]);

        $this->crud->addField([
            'name' => 'incluir_instrumento_cobranca',
            'label' => 'Deseja incluir no Termo de Recebimento Definitivo a Autorização para Emissão do Instrumento de 
                Cobrança?',
            'type' => 'radio',
            'default' => 0,
            'options' => [0 => 'Não', 1 => 'Sim'],
            'required' => true,
            'attributes' => [
                'required' => 'required'
            ],
            'inline' => 'd-inline-block'
        ]);

        $contratoResponsavel = new ContratoResponsavelRepository(request()->contrato_id);

        $this->crud->addField([
            'type' => 'modal_select_signatarios_trd',
            'name' => 'select_prepostos_responsaveis[]',
            'responsaveis' => $contratoResponsavel->getResponsaveisContrato(),
        ]);

        $this->crud->button_custom = [
            ['button_text' => 'Salvar Rascunho', 'button_id' => 'rascunho',
                'button_name_action' => 'rascunho', 'button_value_action' => '1',
                'button_icon' => 'fas fa-edit', 'button_tipo' => 'secondary'],
            ['button_text' => 'Gerar', 'button_id' => 'assinatura',
                'button_name_action' => 'rascunho', 'button_value_action' => '0',
                'button_icon' => 'fas fa-save', 'button_tipo' => 'primary']
        ];

        $this->breadcrumb(true);
    }

    protected function setupUpdateOperation()
    {
        $this->crud->setUpdateContentClass('col-md-12');
        $this->crud->replaceSaveActions([
            'name' => 'save_action_one',
            'button_text' => 'Atualizar TermoRecebimentoProvisorio',
        ]);
        $this->setupCreateOperation();

        $this->breadcrumb(true, 'Editar');
    }


    public function store(TermoRecebimentoProvisorioDefinitivoRequest $request)
    {
        try {
            DB::beginTransaction();
            $trdService = new TermoRecebimentoDefinitivoService();
            $trdService->create($request->validated(), $request->contrato_id);
            DB::commit();
            \Alert::add('success', 'Termo de Recebimento Definitivo criado com sucesso!')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->inserirLogCustomizado('trp', 'error', $e);
            \Alert::add('error', 'Erro ao criar a Termo de Recebimento Definitivo!')->flash();
        }

        return redirect($this->crud->getRoute());
    }

    public function update(TermoRecebimentoProvisorioDefinitivoRequest $request)
    {
        try {
            DB::beginTransaction();
            $trdService = new TermoRecebimentoDefinitivoService();
            $trdService->update($request->validated(), $request->id);
            DB::commit();
            \Alert::add('success', 'Termo de Recebimento Definitivo alterado com sucesso!')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->inserirLogCustomizado('trp', 'error', $e);
            \Alert::add('error', 'Erro ao alterar a Termo de Recebimento Definitivo!')->flash();
        }

        return redirect($this->crud->getRoute());
    }

    public function ajustar(Request $request)
    {
        abort_if(
            $this->crud->getEntry($request->id)->situacao->descres !== 'trd_status_2',
            403,
            'Operação não autorizada!'
        );


        try {
            DB::beginTransaction();
            $trdService = new TermoRecebimentoDefinitivoService();
            $trdService->ajustar($request->id);
            DB::commit();
            \Alert::add('success', 'Termo de Recebimento Definitivo retornado para em elaboração')->flash();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->inserirLogCustomizado('trd', 'error', $e);
            \Alert::add('error', 'Erro ao retornar o TRd para em elaboração!')->flash();
            return redirect()->back();
        }

        return redirect($this->crud->getRoute() . '/' . $request->id . '/edit');
    }
}
