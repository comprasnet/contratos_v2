<?php

namespace App\Http\Controllers;

use App\Http\Requests\NotificacoesRequest;
use App\Http\Traits\CommonColumns;
use App\Http\Traits\Formatador;
use App\Http\Traits\ImportContent;
use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use App\Models\Notificacoes;
use App\Services\UsuarioFornecedor\UsuarioFornecedorService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/**
 * Class NotificacoesFornecedorCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class NotificacoesFornecedorCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CommonColumns;
    use Formatador;
    use ImportContent;
    use UsuarioFornecedorTrait;
    
    protected $usuarioFornecedorService;
    
    public function __construct(UsuarioFornecedorService $usuarioFornecedorService)
    {
        parent::__construct();
        
        $this->usuarioFornecedorService = $usuarioFornecedorService;
    }
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Notificacoes::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . 'fornecedor/notificacao');

        $this->exibirTituloPaginaMenu('Notificacões');
        $this->bloquearBotaoPadrao($this->crud, ['create', 'update']);
        CRUD::addClause('where', 'notifiable_id', backpack_user()->id);
        CRUD::addClause('orderby', 'read_at', 'desc');

        $this->crud->addButtonFromView(
            'line',
            'alterar_situacao_notificacao',
            'alterar_situacao_notificacao',
            'end'
        );
        $administradorFornec = $this->usuarioFornecedorService->verificaAdministradorOuPrespostoFornecedorLogado(
            backpack_user(),
            session('fornecedor_id')
        );

        //$this->crud->setListView('vendor.backpack.crud.usuario-fornecedor.list');
        $this->crud->setListView('vendor.backpack.crud.usuario-fornecedor.list-custom-left-top-button');
        $this->crud->setShowView('vendor.backpack.crud.usuario-fornecedor.show');

        $this->bredcrumbs(
            $administradorFornec,
            'Notificações',
            null,
            url("fornecedor/inicio/" . strtolower(session()->get('tipo_acesso'))),
            true
        );
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addButtonFromView(
            'top',
            'status_solicitacao_adesao_fornec',
            'button_status_solicitacao_adesao_fornec',
            'end'
        );

        $this->crud->addColumn([
            'name' => 'texto',
            'label' => 'Notificação',
            'type' => 'model_function',
            'function_name' => 'getTextoAreaNotificacao',
            'limit' => 99999999999999999999999999999
        ]);
        
        $this->crud->addColumn([
            'name' => 'situacao',
            'label' => 'Situação',
            'type' => 'model_function',
            'function_name' => 'getSituacaoNotificacao',
        ]);
        
        $this->crud->text_button_redirect_create = 'notificacoes';
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }
    
    public function acessarNotificao($id)
    {
        $notificacao = Notificacoes::find($id);

        if (empty($notificacao)) {
            \Alert::add('warning', 'A notificação foi removida da base de dados')->flash();

            return redirect()->back();
        }
        
        $notificacao->markAsRead();
        
        return redirect($notificacao->data['data']['url']);
    }
    
    public function alterarSituacao(Request $request)
    {
        $data = $request->all();

        $notificacao = Notificacoes::find($data['id']);
        $mensagem = '';

        if (!$data['situacaoAtual']) {
            $notificacao->markAsRead();
            $mensagem = 'Situação alterada para lido';
        }
        
        if ($data['situacaoAtual']) {
            $notificacao->markAsUnread();
            $mensagem = 'Situação alterada para não lido';
        }
        
        $codigoSucesso = 200;
        $retorno = $this->montarRespostaAlertJs($codigoSucesso, $mensagem, "success");
        
        return response()->json($retorno, $codigoSucesso);
    }
    
    protected function show($id)
    {
        $id = Route::current()->parameter("id");

        $notificacao = Notificacoes::find($id);

        if (empty($notificacao)) {
            \Alert::add('warning', 'A notificação foi removida da base de dados')->flash();
            
            return redirect()->back();
        }

        $notificacao->markAsRead();

        return redirect($notificacao->data['data']['url']);
    }


    public function alterarSituacaoGeral()
    {
        $mensagem = '';
        $codigoSucesso = 200;

        try {
            DB::beginTransaction();

            $notificacoes = Notificacoes::whereNull('read_at')
                ->where('notifiable_id', backpack_user()->id)
                ->orderBy('id', 'desc')
                ->limit(100)
                ->get();

            if ($notificacoes->isEmpty()) {
                $mensagem = 'Todas as notificações já estão marcadas como lidas';
                \Alert::add('warning', $mensagem)->flash();
                return redirect('fornecedor/notificacao/');
            }

            foreach ($notificacoes as $notificacao) {
                $notificacao->markAsRead();
                // $notificacao->update(['read_at' => now()->format('Y-m-d H:i:s')]);
            }

            $mensagem = 'As notificações foram marcadas como lidas.';

            DB::commit();

            \Alert::add('success', $mensagem)->flash();
            return redirect('fornecedor/notificacao/');
        } catch (\Exception $e) {
            DB::rollBack();
            Log::info($e);
            $mensagem = 'Não foi possível alterar os status das notificações';
            \Alert::add('error', $mensagem)->flash();
            return redirect('fornecedor/notificacao/');
        }
    }
}
