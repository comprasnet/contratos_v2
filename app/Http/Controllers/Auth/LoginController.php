<?php

namespace App\Http\Controllers\Auth;

use App\Http\Traits\UsuarioTrait;
use App\Library\Auth\AuthenticatesUsers;
use App\Http\Traits\Formatador;
use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use App\Services\UsuarioFornecedor\UsuarioFornecedorService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent;
use App\Http\Traits\TracksUserLogin;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    use Formatador;
    use UsuarioFornecedorTrait;
    use TracksUserLogin;
    use UsuarioTrait;

    protected $data = []; // the information we send to the view

    protected $usuarioFornecedorService;

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers {
        logout as defaultLogout;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UsuarioFornecedorService $usuarioFornecedorService)
    {

        $guard = backpack_guard_name();

        $this->middleware("guest:$guard", ['except' => 'logout']);

        // ----------------------------------
        // Use the admin prefix in all routes
        // ----------------------------------

        // If not logged in redirect here.
        $this->loginPath = property_exists($this, 'loginPath') ? $this->loginPath
            : backpack_url('login');

        // Redirect here after successful login.
        $this->redirectTo = property_exists($this, 'redirectTo') ? $this->redirectTo
            : backpack_url('dashboard');

        // Redirect here after logout.
        $this->redirectAfterLogout = property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout
            : backpack_url('login');

        $this->usuarioFornecedorService = $usuarioFornecedorService;
    }

    /**
     * Return custom username for authentication.
     *
     * @return string
     */
    public function username()
    {
        return backpack_authentication_column();
    }

    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        return redirect($this->redirectAfterLogout);
    }

    /**
     * Get the guard to be used during logout.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return backpack_auth();
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        //Ocultar erro por nível conta GovBR
        session()->put('error_nivel', false);

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            if ($this->verificarUsuarioTemApenasPerfilFornecedor()) {
                $this->logout($request);
                return $request->wantsJson()
                    ? new Response('', 204)
                    : redirect('/login')->withErrors(['erro_usuario_compras' => 'Usuários do perfil Fornecedor só' .
                        ' poderão ser logados através do compras.gov.br.']);
            }

            $usuarioPerfilAdministrador = $this->usuarioPertenceGrupoAdministradorDesenvolvedor();

            if (!$usuarioPerfilAdministrador) {
                auth()->logout();

                $urlGovBr = route('acessogov.autorizacao');
                $rota = $this->retornarNomeRotaLoginUsuario(true);

                return redirect($rota)->withErrors(
                    [
                        'erro_usuario_compras' =>
                            "Entre com seu <a href='{$urlGovBr}'>gov.br</a>"
                    ]
                );
            }

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Log the user out and redirect him to specific location.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $perfilApenasFornecedor = $this->verificarUsuarioTemApenasPerfilFornecedor();

        $idUsuario  = auth()->id();
        if ($idUsuario != null) {
            $this->removerDadosUsuarioRedis($idUsuario);
        }

        // Se sou usuário do contratos e usuário fornecedor no compras
        if ($this->usuarioFornecedorService->verificarSeUserTemUgprimaria(backpack_user())) {
            $this->usuarioFornecedorService->mudaValorAcessoComprasFornecedor(backpack_user());
        }

        if (!empty(Cache::get(session()->get('chave_responseUsuarioCompras')))) {
            Cache::forget(session()->get('chave_responseUsuarioCompras'));
        }

        // Faz o logout do usuário
        $this->guard()->logout();
        // Garantindo que o Laravel auth também seja deslogado
        auth()->logout();

        if (session('logged_in') == 'gov') {
            $url = config('acessogov.host') . '/logout'
            . '?post_logout_redirect_uri=' . config('app.url') . '/';
            
            Session::flush();

            if (!empty(Cache::get(session()->get('chave_responseUsuarioCompras')))) {
                Cache::forget(session()->get('chave_responseUsuarioCompras'));
            }

            return Redirect::away($url);
        }

        // Limpa a sessão para todos os casos
        Session::flush();


        //se tem apenas perfil fornecedor, redirecione para compras.gov
        if ($perfilApenasFornecedor) {
            return Redirect::away('https://www.comprasnet.gov.br/seguro/loginPortal.asp');
        }

        // Redireciona para o local padrão - tela login v2
        return redirect($this->redirectAfterLogout);
    }
    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param  mixed  $user
     * @return void
     */
    protected function authenticated(Request $request, $user)
    {
        // Chama o método da trait para rastrear o login
        $this->trackLogin($user);
    }
}
