<?php

namespace App\Http\Middleware;

use App\Http\Traits\UgPrimariaTrait;
use Closure;
use Illuminate\Support\Facades\Redirect;

class UgprimariaMiddleware
{
    use UgPrimariaTrait;
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->setarUnidadeUsuario();

        return $next($request);
    }
}
