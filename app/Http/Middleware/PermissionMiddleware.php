<?php

namespace App\Http\Middleware;

use App\Http\Traits\UgPrimariaTrait;
use App\Http\Traits\UsuarioTrait;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PermissionMiddleware
{
    use UgPrimariaTrait;
    use UsuarioTrait;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, string $permissions)
    {
        if (!backpack_user()) {
            $rota = $this->retornarRotaLoginUsuario();
            return redirect($rota);
        }

        $permissionList = explode('|', $permissions);
        $hasPermission = false;

        foreach ($permissionList as $permission) {
            if (backpack_user()->can($permission)) {
                $hasPermission = true;
                break;
            }
        }

        if (!$hasPermission) {
            $fromv1 = url()->previous().'appredirect/';

            if ($fromv1 === env('URL_CONTRATO_VERSAO_UM')) {
                return Redirect::to('transparencia/arp');
            }

            if (in_array('V2_acessar', $permissionList)) {
                auth()->logout();
            }

            abort(403, config('app.erro_permissao'));
        }

        $this->setarUnidadeUsuario();

        return $next($request);
    }
}
