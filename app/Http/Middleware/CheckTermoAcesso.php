<?php

namespace App\Http\Middleware;

use App\Models\TermoAcesso;
use Carbon\Carbon;
use Closure;

class CheckTermoAcesso
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $dataAcessoSomenteGov = config('acessogov.data_acesso_somente_gov');

        $dataLimite = Carbon::parse($dataAcessoSomenteGov);

        if ($dataLimite->lessThanOrEqualTo(Carbon::now())) {
            return $next($request);
        }

        if (auth()->check()) {
            session(['url.intended' => url()->full()]);
            // Caso o usuário seja do tipo 'gov'
            if (session('logged_in') == 'gov') {
                $verificaTermo = TermoAcesso::where('user_id', backpack_auth()->user()->id)->first();
                return $verificaTermo ? $next($request) : redirect()->to('/termo-acesso');
            }
            return session('termoAcesso') ? $next($request) : redirect()->to('/termo-acesso');
        }

        return $next($request);
    }
}
