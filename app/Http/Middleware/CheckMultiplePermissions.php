<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckMultiplePermissions
{
    protected $permissions;


    public function __construct()
    {
        // Configurar as permissões aqui ou através de outro método
        $this->permissions = ['V2_acessar', 'V2_acessar_desenvolvedor'];
    }

    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        foreach ($this->permissions as $permission) {
            if ($user->can($permission)) {
                return $next($request);
            }
        }

        abort(403, 'Unauthorized');
    }
}
