<?php

namespace App\Http\Middleware;

use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use function PHPUnit\Framework\stringContains;

class BlockPermissionFornecedorMiddleware
{
    use UsuarioFornecedorTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        #realiza a requisição da rota
        $response = $next($request);

        $proibeFornecedor = $this->verificarUsuarioTemApenasPerfilFornecedor();

        // todo observar se a lógica está correta com a permissão do fornecedor
        if (strpos($request->getPathInfo(), '/fornecedor') !== false) {
            return $response;
        }

        if ($proibeFornecedor) {
            abort('403', config('app.erro_permissao'));
            //return redirect()->back();
        }

        return $response;
    }
}
