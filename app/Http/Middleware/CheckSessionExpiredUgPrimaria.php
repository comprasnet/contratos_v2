<?php

namespace App\Http\Middleware;

use App\Http\Traits\UgPrimariaTrait;
use App\Http\Traits\UsuarioTrait;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckSessionExpiredUgPrimaria
{
    use UgPrimariaTrait;
    use UsuarioTrait;

    public function handle($request, Closure $next)
    {
        // Verificar se o usuário está autenticado e a sessão está ativa
        if (backpack_user()) {
            $sessionLifetime = config('session.lifetime');
            $lastActivity = session('last_activity') ?? now();
            $diffInMinutes = now()->diffInMinutes($lastActivity);

            if ($diffInMinutes >= $sessionLifetime) {
                $rota = $this->retornarRotaLoginUsuario();
                backpack_user()->logout();

                return redirect()->route($rota)->withErrors(['message' => 'Session expired.']);
            }

            # Incluir as informações da unidade para o usuário
            $this->setarUnidadeUsuario();
        }
        
        return $next($request);
    }
}
