<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckFornecedorAdministrador
{
    public function handle(Request $request, Closure $next, ...$permission)
    {
        if (!backpack_user()) {
            return redirect()->route('backpack.auth.login');
        }

        foreach ($permission as $permissao) {
            if (backpack_user()->can($permissao)) {
                return $next($request);
            }
        }

        abort('403', config('app.erro_permissao'));
    }
}
