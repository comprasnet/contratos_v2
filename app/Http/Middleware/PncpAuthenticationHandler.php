<?php

namespace App\Http\Middleware;

use App\Services\Pncp\PncpService;
use Illuminate\Http\Response;
use Psr\Http\Message\RequestInterface;
use App\Http\Traits\ExternalServices;

class PncpAuthenticationHandler
{
    use ExternalServices;

    public function __invoke(callable $handler)
    {
        return function (RequestInterface $request, array $options) use ($handler) {

            if (!PncpService::tokenIsValid()) {
                $this->refreshToken();
            }

            $token = PncpService::getToken();

            $request = $request->withAddedHeader('Authorization', $token);

            return $handler($request, $options);
        };
    }

    private function refreshToken()
    {
        $usuarioPncp = PncpService::getUsuarioPncp();

        $response = $this->makeRequest(
            'POST',
            config('api.pncp.base_uri'),
            'v1/usuarios/login',
            null,
            ['Content-Type' => 'application/json'],
            json_encode(
                ['login' => $usuarioPncp->login, 'senha' => $usuarioPncp->senha]
            ),
            []
        );

        if (Response::HTTP_OK === $response->getStatusCode()) {
            PncpService::setToken($response->getHeader('authorization')[0]);
        }
    }
}
