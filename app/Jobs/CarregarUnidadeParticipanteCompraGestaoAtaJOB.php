<?php

namespace App\Jobs;

use App\Api\Externa\ApiSiasg;
use App\Http\Traits\Compra\CompraItemTrait;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\ExternalServices;
use App\Http\Traits\LogTrait;
use App\Models\Compras;
use App\Models\FailedJob;
use App\Models\JobUser;
use App\Models\Unidade;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Support\Facades\Log;
use Throwable;

class CarregarUnidadeParticipanteCompraGestaoAtaJOB implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use CompraTrait;
    use LogTrait;
    use ExternalServices;
    use CompraItemTrait;

    private $dadosCompraParticipantes;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dadosCompraParticipantes)
    {
        $this->dadosCompraParticipantes = $dadosCompraParticipantes;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        # Altera o status do Job na tabela job_user informando que o item está sendo processado
        $this->alterarStatus($this->job->getJobId(), true);

        DB::beginTransaction();
        try {
            $urlCompraSisrp = $this->montarURLItemParticipante();
            
            # Inicia a utlização para consumir a API do SIASG
            $consultaCompra = new ApiSiasg();

            $this->salvarItemParticipanteAta($this->dadosCompraParticipantes, $urlCompraSisrp, $consultaCompra);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();

            # Se acontecer erro, insere no arquivo de log gestao_ata
            $this->inserirLogCustomizado('gestao_ata', 'error', $ex);

            # Em caso de falha, será inserido o registro na tabela failed_job
            $this->inserirRegistroFailedJob($ex);
        }
    }

    /**
     * Método responsável em alterar o status do item para processando
     */
    private function alterarStatus(int $idStatus, bool $status)
    {
        # Recupera o registro do JOB na tabela intermediária
        $jobUser = JobUser::where("job_id", $idStatus)->first();

        # Se não for encontrado o job, sai do método
        if (empty($jobUser)) {
            return ;
        }

        # Altera o status para processando
        $jobUser->processando = $status;
        $jobUser->save();
    }

    /**
     * Método responsável em inserir o registro na tabela failed_job
     * Para poder realizar o reprocessamento a cada 30 minutos  no JOB jobReprocessaItemCompraParticipanteGestaoAta
     */
    private function inserirRegistroFailedJob(Exception $ex)
    {
        # Inserir registro na tabela job_user com ID do failed JOB
        DB::beginTransaction();
        try {
            # Monta o array para o insert, recuperando as informações do JOB
            $arrayInsertFailedJob = ['uuid' => $this->job->uuid(), 'connection' => $this->job->getConnectionName(),
            'queue' => $this->job->getQueue(), 'payload' => json_encode($this->job->payload()), 'exception' => $ex  ];

            # Insere na tabela failed_job
            FailedJob::updateOrCreate($arrayInsertFailedJob);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();

            # Se acontecer erro, insere no arquivo de log gestao_ata
            $this->inserirLogCustomizado('gestao_ata', 'error', $ex);
        }
    }
}
