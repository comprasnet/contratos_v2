<?php

namespace App\Jobs;

use App\Services\RelatorioArpPdfService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Repositories\Relatorio\ArpPdfRepository;

class GenerateRelatorioArpPdfJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $arpId;
    private $arpItemPdfRepository;
    public function __construct($arpId, ArpPdfRepository $arpItemPdfRepository)
    {
        set_time_limit(300);
        $this->arpId = $arpId;
        $this->arpItemPdfRepository = $arpItemPdfRepository;
    }

    public function handle()
    {
        (new RelatorioArpPdfService())->generatePDF(
            $this->arpId,
            $this->arpItemPdfRepository
        );
    }
}
