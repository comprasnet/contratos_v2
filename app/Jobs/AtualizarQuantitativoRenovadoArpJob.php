<?php

namespace App\Jobs;

use App\Models\ArpHistorico;
use App\Http\Traits\Arp\ArpQuantidadeAutorizadaAlteracaoTrait;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AtualizarQuantitativoRenovadoArpJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ArpQuantidadeAutorizadaAlteracaoTrait;

   // protected $arpHistoricoId;

    protected $dadosHistoricoArp;
    protected $dadosFormulario;
    //protected $arpId;


    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct($dadosHistoricoArp, $dadosFormulario)
    {
        $this->dadosHistoricoArp = $dadosHistoricoArp;
        $this->dadosFormulario = $dadosFormulario;
    }


    public function handle()
    {
       // $vigenciaFinal = Carbon::parse($this->dadosHistoricoArp['vigencia_final'])->addDay();

        // Verifica se a data atual é maior ou igual à vigenciaFinal + 1 dia
        //if (Carbon::now()->greaterThanOrEqualTo($vigenciaFinal)) {
            // Executa o método para atualizar a quantidade autorizada
            $this->atualizarQuantidadeAutorizadaAlteracao($this->dadosHistoricoArp, $this->dadosFormulario);
        //}
    }
}
