<?php

namespace App\Repositories;

use App\Models\Municipio;

class MunicipiosRepository extends Municipio
{
    
    /**
     * @param string $uf
     * @param string $municipio
     * @return mixed
     */
    public function getMunicipioPorUF(string $uf, string $municipio)
    {
        return $this->select(['municipios.*',  'estados.sigla'])
            ->join('estados', function ($join) use ($uf) {
                $join->on('municipios.estado_id', '=', 'estados.id')
                    ->where('estados.sigla', '=', $uf);
            })
            ->where('municipios.nome', 'ilike', "%{$municipio}%")
            ->whereNull('municipios.deleted_at')
            ->whereNull('estados.deleted_at')
            ->first();
    }

    public function getMunicipioPorCodigo(?string $codigo)
    {
        if (!empty($codigo)) {
            return $this->join('estados', 'estados.id', '=', 'municipios.estado_id')
                ->whereRaw("codigo_ibge::text LIKE '$codigo%'")
                ->select(['municipios.*',  'estados.sigla'])
                ->first();
        }
        return null;
    }

    public function getMunicipio(?string $codigo, string $uf = null, string $municipio = null)
    {
        return $this->getMunicipioPorCodigo($codigo) ?? $this->getMunicipioPorUF($uf, $municipio);
    }
}
