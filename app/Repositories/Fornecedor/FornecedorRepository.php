<?php

namespace App\Repositories\Fornecedor;

use App\Models\BackpackUser;
use App\Models\Fornecedor;
use App\Models\User;
use Illuminate\Database\QueryException;
use phpDocumentor\Reflection\Types\Boolean;

class FornecedorRepository
{
    /**
     * Verifica se um fornecedor existe com o CPF/CNPJ e tipo fornecidos.
     *
     * @param string $cpf_cnpj_idgener O CPF, CNPJ ou ID do fornecedor.
     * @param string $tipo_fornecedor O tipo do fornecedor (Pessoa Física, Jurídica ou Estrangeiro).
     * @return \App\Models\Fornecedor|null Retorna uma instância de Fornecedor se encontrado, ou null caso contrário.
     */
    public function verificarFornecedorRepository(string $cpf_cnpj_idgener): ?Fornecedor
    {
        //dd($cpf_cnpj_idgener);
        return Fornecedor::where('cpf_cnpj_idgener', $cpf_cnpj_idgener)
            ->first();
    }

    /**
     * Verifica se há um vínculo entre um fornecedor e um usuário.
     *
     * @param \App\Models\Fornecedor $fornecedor O fornecedor a ser verificado.
     * @param \App\Models\User $user O usuário a ser verificado.
     * @return bool Retorna true se houver um vínculo entre o fornecedor e o usuário, do contrário false.
     */
    public function verificarVinculoUsuarioFornecedorRepository(Fornecedor $fornecedor, User $user): bool
    {
        //verificar aqui, parece que não tá achando o vinculo
        if ($fornecedor->users()->where('user_id', $user->id)->exists()) {
            return true;
        }
        return false;
    }

    /**
     * Vincula um usuário a um fornecedor.
     *
     * @param \App\Models\Fornecedor $fornecedor O fornecedor.
     * @param \App\Models\User $user O usuário a ser vinculado.
     * @return void
     */
    public function vincularUsuarioFornecedorRepository(
        Fornecedor $fornecedor,
        User $user,
        bool $administrador = false
    ): void {
        //$fornecedor->users()->attach($user->id);
        $fornecedor->users()->attach($user->id, [
            'administrador_fornecedor' => $administrador,
        ]);
    }

    /**
     * Cadastra ou atualiza um fornecedor.
     *
     * @param array $dados Os dados do fornecedor.
     *   - 'cpf_cnpj_idgener' string O CPF, CNPJ ou ID do fornecedor.
     *   - 'tipo_fornecedor' string O tipo do fornecedor (Pessoa Física, Jurídica ou Estrangeiro).
     *   - 'nome' string O nome do fornecedor.
     * @return \App\Models\Fornecedor|null Retorna uma instância de Fornecedor
     *   representando o fornecedor cadastrado ou atualizado, ou null em caso de falha.
     */
    public function cadastrarAtualizarFornecedorRepository(array $dados): ?Fornecedor
    {
        try {
            // Tenta encontrar o fornecedor
            $fornecedorExistente = Fornecedor::where('cpf_cnpj_idgener', $dados['cpf_cnpj_idgener'])
                ->where('tipo_fornecedor', $dados['tipo_fornecedor'])
                ->first();

            // Se não existe, cria
            if ($fornecedorExistente === null) {
                return Fornecedor::create([
                    'cpf_cnpj_idgener' => $dados['cpf_cnpj_idgener'],
                    'tipo_fornecedor' => $dados['tipo_fornecedor'],
                    'nome' => $dados['nome']
                ]);
            }

            // Se encontrado, atualiza os dados do fornecedor
            $fornecedorExistente->update($dados);

            return $fornecedorExistente;
        } catch (\Throwable $e) {
            // Log or handle the exception as needed

            return null;
        }
    }
    
    public function recuperarFornecedorPorCpfCnpj(string $cpfCnpjIdgener): ?Fornecedor
    {
        return Fornecedor::where('cpf_cnpj_idgener', $cpfCnpjIdgener)->first();
    }
    
    public function inserirFornecedor(array $dados): ?Fornecedor
    {
        return Fornecedor::create($dados);
    }
}
