<?php

namespace App\Repositories;

use App\Models\Catmatpdms;

class CatmatPdmRepository
{
    /**
     * @param $pdmCode
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public static function getPDMByCode($pdmCode)
    {
        $query = Catmatpdms::query()
            ->where('codigo', $pdmCode);
        return $query->first();
    }
}
