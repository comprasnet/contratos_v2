<?php

namespace App\Repositories\EntregaTrpTrd;

use App\Models\CodigoItem;
use App\Models\EntregaItem;

class EntregaItemRepository
{
    public function getQuantidadeTotalConsumida(int $itemableId, string $itemableType, int $entregaId = 0)
    {
        $situacoes = CodigoItem::whereIn('descres', [
            'entrega_status_2', // analise
            'entrega_status_3', // execucao
            'entrega_status_4', // concluída
        ])->pluck('id');

        return EntregaItem::whereHas('entrega', function ($query) use ($situacoes, $entregaId) {
            $query->whereIn('situacao_id', $situacoes)->where('id', '<>', $entregaId);
        })->where('itemable_id', $itemableId)
            ->where('itemable_type', $itemableType)
            ->sum('quantidade_informada');
    }
}
