<?php

namespace App\Repositories\EntregaTrpTrd;

use App\Models\CodigoItem;
use App\Models\TermoRecebimentoDefinitivoItem;
use App\Models\TermoRecebimentoProvisorioItem;

class TermoRecebimentoDefinitivoItemRepository
{
    public function getQuantidadeTotalConsumida(int $itemableId, string $itemableType, int $trpItemId, int $trdId = 0)
    {
        $situacoes = CodigoItem::whereIn('descres', [
            'trd_status_3', // execucao
            'trd_status_4', // aguardando instrumento de cobrança
            'trd_status_5', // concluída
        ])->pluck('id');

        return TermoRecebimentoDefinitivoItem::whereHas('trd', function ($query) use ($situacoes, $trdId) {
            $query->whereIn('situacao_id', $situacoes)->where('id', '<>', $trdId);
        })->where('itemable_id', $itemableId)
            ->where('itemable_type', $itemableType)
            ->where('termo_recebimento_provisorio_item_id', $trpItemId)
            ->sum('quantidade_informada');
    }
}
