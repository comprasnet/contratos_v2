<?php

namespace App\Repositories\EntregaTrpTrd;

use App\Models\CodigoItem;
use App\Models\TermoRecebimentoProvisorioItem;

class TermoRecebimentoProvisorioItemRepository
{
    public function getQuantidadeTotalConsumida(
        int $itemableId,
        string $itemableType,
        int $entregaItemId,
        int $trpId = 0
    ) {
        $situacoes = CodigoItem::whereIn('descres', [
            'trp_status_3', // execucao
            'trp_status_4', // concluída
        ])->pluck('id');

        return TermoRecebimentoProvisorioItem::whereHas('trp', function ($query) use ($situacoes, $trpId) {
            $query->whereIn('situacao_id', $situacoes)->where('id', '<>', $trpId);
        })->where('itemable_id', $itemableId)
            ->where('itemable_type', $itemableType)
            ->where('entrega_item_id', $entregaItemId)
            ->sum('quantidade_informada');
    }
}
