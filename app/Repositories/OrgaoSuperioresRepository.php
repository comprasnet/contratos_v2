<?php

namespace App\Repositories;

use App\Models\OrgaoSuperior;

class OrgaoSuperioresRepository extends OrgaoSuperior
{
    public function salvarAtualizarOrgaoSuperior(array $data)
    {
        return $this->updateOrCreate($data);
    }
    
    public function getOrgaoSuperior($codigo)
    {
        return $this->where("codigo", $codigo)
            ->where("situacao", true)
            ->first();
    }
}
