<?php

namespace App\Repositories\Arp;

use App\Http\Traits\Formatador;
use App\Models\Arp;
use App\Models\ArpItem;
use App\Models\CompraItemUnidade;
use App\Services\Arp\ArpRemanejamentoService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ArpRepositoy extends Arp
{
    use Formatador;

    /**
     * Método responsável em recuperar as unidades de origem vinculadas as atas
     */
    public function montarSelectUnidadeOrigemArp(string $textoDigitado)
    {
        return $this->join("unidades", "unidade_origem_id", "=", "unidades.id")
            ->where("rascunho", false)
            ->where(function ($query) use ($textoDigitado) {
                $query->where('codigo', 'ILIKE', "%{$textoDigitado}%")
                    ->orWhere('nomeresumido', 'ILIKE', "%{$textoDigitado}%");
            })
            ->select('unidades.id', DB::raw("CONCAT(codigo,' - ',nomeresumido) AS unidade_arp_adesao"))
            ->groupBy("codigo", "nomeresumido", "unidades.id")
            ->paginate(10);
    }

    /**
     * Método responsável em recuperar todas as atas ativas do sistema
     */
    public function getAllAtaAtiva()
    {
        return $this->where("rascunho", false)->get();
    }

    /**
     * Método responsável em exibir as atas vigentes no select
     */
    public function montarSelectAtaAtivaRemanejamento(string $textoDigitado, string $nomeCampo)
    {
        $concat = "CONCAT(arp.numero, '/', arp.ano, ' (', unidades.codigo, ' - ', unidades.nomeresumido ,')' )";

        $ataAtiva = $this->join("unidades", "unidades.id", "=", "unidade_origem_id")
            ->where("rascunho", false)
            ->where(function ($query) use ($textoDigitado) {
                $query->whereRaw('arp.numero || \'/\' || arp.ano ILIKE ?', ["%{$textoDigitado}%"])
                    ->orWhere('unidades.codigo', 'ILIKE', "%{$textoDigitado}%")
                    ->orWhere('unidades.nomeresumido', 'ILIKE', "%{$textoDigitado}%");
            })
            ->select(
                'arp.id',
                'arp.numero',
                'arp.ano',
                'unidades.codigo',
                'unidades.nomeresumido',
                DB::raw(" {$concat} AS {$nomeCampo}")
            )
            ->paginate(10);
        return $ataAtiva;
    }

    /**
     * Método responsável em exibir no select as compras vinculadas as atas
     */
    public function montarSelectNumeroCompraAta(string $textoDigitado, int $idUnidadeCompra)
    {
        $compra = $this->join("compras", "compras.id", "=", "arp.compra_id")
            ->where("compras.unidade_origem_id", $idUnidadeCompra)
            ->where("rascunho", false)
            ->where("compras.numero_ano", "ILIKE", "%{$textoDigitado}%")
            ->select("compras.id", "compras.numero_ano AS numero_ano")
            ->groupBy("compras.id", "compras.numero_ano");

        return $compra;
    }

    /**
     * Método responsável em exibir no select a modalidade da
     * compra vinculadas as atas
     */
    public function montarSelectModalidadeCompraAta(string $textoDigitado, int $idUnidadeCompra)
    {
        $compra = $this->join("compras", "compras.id", "=", "arp.compra_id")
            ->join("codigoitens", "codigoitens.id", "=", "compras.modalidade_id")
            ->where("compras.unidade_origem_id", $idUnidadeCompra)
            ->where("rascunho", false)
            ->where(function ($query) use ($textoDigitado) {
                $query->where('codigoitens.descres', "ILIKE", "%{$textoDigitado}%")
                    ->orWhere('codigoitens.descricao', "ILIKE", "%{$textoDigitado}%");
            })
            ->select(
                "codigoitens.id",
                DB::raw("CONCAT(codigoitens.descres, '-', codigoitens.descricao) AS modalidade_compra")
            )
            ->groupBy(
                "codigoitens.id",
                "codigoitens.descres",
                "codigoitens.descricao"
            );

        return $compra;
    }

    /**
     * Método responsável em exibir no select a unidade da compra vinculada a ata
     */
    public function montarSelectUnidadeCompraAta(string $textoDigitado)
    {
        $compra = $this->join("compras", "compras.id", "=", "arp.compra_id")
            ->join("unidades", "unidades.id", "=", "arp.unidade_origem_id")
            ->where("rascunho", false)
            ->where(function ($query) use ($textoDigitado) {
                $query->where('unidades.codigo', "ILIKE", "%{$textoDigitado}%")
                    ->orWhere('unidades.nomeresumido', "ILIKE", "%{$textoDigitado}%");
            })
            ->select(
                "unidades.id",
                DB::raw("CONCAT(unidades.codigo, '-', unidades.nomeresumido) AS unidade_codigo_nomeresumido")
            )
            ->groupBy(
                "unidades.id",
                "unidades.codigo",
                "unidades.nomeresumido"
            )
            ->paginate(10);

        return $compra;
    }

    /**
     * Método responsável em recuperar os itens para o remanejamento
     */
    public function itensRemanejamento(array $dadosFiltro)
    {
        # Realiza o JOIN com a tabela arp Item para recuperar
        # os itens de todas as atas
        $arpItem = $this->queryBaseItemRemanejamento();

        # Não pode exibir os itens que da unidade solicitante
        $arpItem->where("compra_item_unidade.unidade_id", '<>', session('user_ug_id'));

        # Filtro Número/Ano da Compra
        if (!empty($dadosFiltro['numeroAnoCompra'])) {
            $arpItem->where("arp.compra_id", $dadosFiltro['numeroAnoCompra']);
        }

        # Filtro Modalidade da Compra
        if (!empty($dadosFiltro['modalidadeCompra'])) {
            $arpItem->where("compras.modalidade_id", $dadosFiltro['modalidadeCompra']);
        }

        # Filtro Unidade da Compra
        if (!empty($dadosFiltro['unidadeCompra'])) {
            $arpItem->where("compras.unidade_origem_id", $dadosFiltro['unidadeCompra']);
        }

        # Filtro Número da ata
        if (!empty($dadosFiltro['numeroAta'])) {
            $arpItem->where("arp.id", $dadosFiltro['numeroAta']);
        }

        # Quando a busca for realizada pela tela de rascunho, filtrar somente
        # pela ata do item que o usuário selecionou
        if (!empty($dadosFiltro['idRemanejamento'])) {
            $arpRemanejamentoRepository = new ArpRemanejamentoRepository();

            $remanejamentoRecuperado =
                $arpRemanejamentoRepository->getRemanejamentoUnico($dadosFiltro['idRemanejamento']);

            $arpItem->where("arp.id", $remanejamentoRecuperado->arp_id);
        }

        $search = $dadosFiltro['search']['value'];
        if (!empty($search)) {
            $arpItem->where(function ($query) use ($search) {
                $query->where('compra_items.descricaodetalhada', 'ilike', "%$search%")
                    ->orWhere('compra_items.numero', 'ilike', "%$search%")
                    ->orWhere('unidades.codigo', 'ilike', "%$search%")
                    ->orWhere('unidades.nomeresumido', 'ilike', "%$search%")
                    ->orWhere('compra_item_unidade.quantidade_saldo', 'ilike', "%$search%");
            });
        }

        $arpItem = $arpItem->groupBy(
            'arp.numero',
            'arp.ano',
            'unidades.codigo',
            'unidades.nomeresumido',
            'compra_item_unidade.id',
            'compras.numero_ano',
            'compra_items.numero',
            'compra_items.descricaodetalhada',
            'compra_item_unidade.quantidade_saldo',
            'compra_item_unidade.tipo_uasg',
            'arp.id',
            'compra_item_unidade.compra_item_id',
            'cgi.descres'
        );

        # Recuperar as colunas
        $arpItem = $arpItem
            ->select(
                DB::raw("CONCAT(arp.numero,'/',arp.ano) as numero_ata"),
                DB::raw("CONCAT(unidades.codigo,' - ',unidades.nomeresumido) as unidade_origem"),
                "compra_item_unidade.id as id_item_unidade",
                "compras.numero_ano as numero_compra",
                "compra_items.numero",
                "compra_items.descricaodetalhada",
                "compra_item_unidade.quantidade_saldo",
                "compra_item_unidade.quantidade_autorizada",
                "cgi.descres as descresitem",
                "compra_item_unidade.tipo_uasg",
                "arp.id as idata",
                "compra_item_unidade.compra_item_id"
            );

        $idsToRemove = [];
        $remanejamentoService = new ArpRemanejamentoService();
        # Recuperar as atas em que a unidade que vai solicitar é carona para a ata
        foreach ($arpItem->get() as $item) {
            $itemPodeSolicitar = $remanejamentoService->usuarioPodeSolicitarQuantitativo(
                $item->compra_item_id,
                session('user_ug_id'),
                0
            );

            if (!$itemPodeSolicitar['podeSolicitar']) {
                continue;
            }

            $compraItemUnidadeParticipanteCarona = CompraItemUnidade::where('compra_item_id', $item->compra_item_id)
                ->whereIn('tipo_uasg', ['P', 'C'])
                ->where('unidade_id', session('user_ug_id'))
                ->get();

            // Loop através dos itens participantes/carona
            foreach ($compraItemUnidadeParticipanteCarona as $itemParticipanteCarona) {
                if ($itemParticipanteCarona->tipo_uasg === 'C') {
                    $itemAta = ArpItem::join(
                        'compra_item_fornecedor',
                        'compra_item_fornecedor.id',
                        'compra_item_fornecedor_id'
                    )
                        ->where('compra_item_id', $itemParticipanteCarona->compra_item_id)
                        ->where('fornecedor_id', $itemParticipanteCarona->fornecedor_id)
                        ->where('arp_id', $item->idata)
                        ->exists();

                    if ($itemAta == false) {
                        // Adicionar o ID do item ao array de IDs a serem removidos
                        $idsToRemove[] = $item->idata;
                    }
                }
            }
        }

        $idsToRemove = array_unique($idsToRemove);

        $arpItem = $arpItem->whereNotIn('arp.id', $idsToRemove);

        $totalLinhas = $arpItem->get()->count();

        $arpItem = $arpItem->skip($dadosFiltro['start'])->take($dadosFiltro['length']);

        $orderBy = $dadosFiltro['order'];
        if (!empty($orderBy)) {
            $colunaOrdenacao = $orderBy[0]['column'] + 1;

            $arpItem->orderByRaw("{$colunaOrdenacao}  {$orderBy[0]['dir']}");
        }

        $arpItem = $arpItem->get()->toArray();

        # Cria o array para retornar as informações no front
        $itemFormatado = array();
        $arpRemanejamentoItemRepository = new ArpRemanejamentoItemRepository();
        $remanejamentoService = new ArpRemanejamentoService();

        # Percorre os itens encontrados na query
        foreach ($arpItem as $item) {
            $unidadeSolicitanteCarona = $remanejamentoService->unidadeSolicitanteCaronaItem(
                $item['compra_item_id'],
                session('user_ug_id')
            );

            if ($unidadeSolicitanteCarona) {
                $retornoValidacao = $remanejamentoService->usuarioPodeSolicitarQuantitativo(
                    $item['compra_item_id'],
                    session('user_ug_id'),
                    0,
                    true
                );
                if (!$retornoValidacao['podeSolicitar']) {
                    continue;
                }
            }

            # Traduz o valor recuperado no banco para o por extenso
            $item['tipo_uasg_extenso'] = $this->tipoUasgConvertido($item['tipo_uasg']);

            # Reduz a descrição do item para cinquenta caracteres e inclui o ícone
            # para poder ver a descrição completa
            $item['descricaodetalhada'] = Str::limit(trim($item['descricaodetalhada']), 50, '
                                            <i
                                                class="fas fa-info-circle"
                                                title="' . $item['descricaodetalhada'] . '"
                                            ></i>');

            # O valor máximo para o lançamento do usuário
            $item['quantidade_saldo'] = min($item['quantidade_saldo'], $item['quantidade_autorizada']) ;
            $max = $item['quantidade_saldo'];

            # Insere o método de inserção apertando o botão dentro do input number
            # no campo quantidade solicitada
            $item['step'] = 1;

            $idItemUnidade = $item['id_item_unidade'];
            $numeroAta = $item['numero_ata'];

            $idAta = $item['idata'];

            # Inserir a unidade do usuário logado para poder verificar o máximo
            # que o usuário poderá solicitar
            $item['unidade_solicitante_id'] = session('user_ug_id');

            # Montar a URL principal para complementar com o id da unidade e do item unidade
            $item['url'] = url('arp/remanejamento/validarquantitativosolicitado');
            $valueRascunho = '';

            if (!empty($dadosFiltro['idRemanejamento'])) {
                $itemSalvoRascunho = $arpRemanejamentoItemRepository->getItemRemanejamentoPorUnidade(
                    $dadosFiltro['idRemanejamento'],
                    $idItemUnidade
                );

                $valueRascunho = $itemSalvoRascunho->quantidade_solicitada ?? '';
            }

            $item['valor_rascunho'] = $valueRascunho;

            # Monta o campo do lançamento do quantitativo para o remanejamento
            $item['quantidadesolicitada'] =
                "
 <div class='br-input'>
                 <input
                     type='hidden'
                     id='ata_selecionada_[{$idItemUnidade}]'
                     value='${numeroAta}'
                 >
                 <input
                     type='hidden'
                     name='id_ata[{$idItemUnidade}]'
                     value='{$idAta}'
                 >
                 <input
                     type='number'
                     class='quantidade_solicitada quant_item_{$item['compra_item_id']}'
                     id='quantidade_solicitada_{$idItemUnidade}_{$idAta}'
                     name='quantidade_solicitada[{$idItemUnidade}]'
                     min='0'
                     max='{$max}'
                     step='{$item['step']}'
                     data-iditemunidade='{$idItemUnidade}'
                     data-numeroata='{$numeroAta}'
                     data-idata='{$idAta}'
                     data-unidadesolicitanteid = '{$item['unidade_solicitante_id']}'
                     data-urlvalidarlancamento = '{$item['url']}'
                     data-compraitemid = '{$item['compra_item_id']}'
                     data-itemprincipal = '{$idItemUnidade}'
                     value='{$item['valor_rascunho']}'
                 />
                 <span
                     style='display: none;'
                     class='feedback warning'
                     role='alert'
                     id='feedback_{$idItemUnidade}'
                 >
                     <i
                         class='fas fa-exclamation-triangle'
                         aria-hidden='true'
                     ></i>Máximo ${max}
                 </span>
             </div>";

            $itemUnidadeCarona =  CompraItemUnidade::where('compra_item_id', $item['compra_item_id'])
                ->where('tipo_uasg', 'C')
                ->where('unidade_id', $item['unidade_solicitante_id'])
                ->where('situacao', true)
                ->get();

            $item['itemFornecedorCarona'] = array();
            
            if ($unidadeSolicitanteCarona) {
                $item['quantidadesolicitada'] ="<a href='javascript: void(0)'
                                                    class='details-link br-button circle
                                                            primary br-button-form
                                                            br-button-cor-icone'
                                                    title='Informar o fornecedor do item remanejado'>
                                                            <i class='fas fa-plus'></i></a>";

                $itemUnidadeCarona =  CompraItemUnidade::where('compra_item_id', $item['compra_item_id'])
                    ->where('tipo_uasg', 'C')
                    ->where('unidade_id', $item['unidade_solicitante_id'])
                    ->where('situacao', true)
                    ->get();

                foreach ($itemUnidadeCarona as $key => $dadosCarona) {
                    $itemSalvoRascunho = $arpRemanejamentoItemRepository->getItemRemanejamentoPorUnidadeCarona(
                        $dadosFiltro['idRemanejamento'] ?? 0,
                        $idItemUnidade,
                        $dadosCarona->id
                    );
//                    dd($itemSalvoRascunho);
                    $item['itemFornecedorCarona'][$key] = [
                        'compra_item_unidade_id' => $dadosCarona->id,
                        'cpf_cnpj_idgener' => $dadosCarona->fornecedor->cpf_cnpj_idgener,
                        'nome' => $dadosCarona->fornecedor->nome,
                        'valor_rascunho' => $itemSalvoRascunho->quantidade_solicitada ?? '',
                        'idata' => $item['idata'],
                    ];
                }
            }

            $item['quantidade_saldo_sem_formatar'] = $item['quantidade_saldo'];

            $item['quantidade_saldo'] =
                number_format($item['quantidade_saldo'], 5, ',', '.');

            $itemFormatado[] = $item;
        }

        if (count($itemFormatado) == 0) {
            $totalLinhas = 0;
        }

        return ['data' => $itemFormatado, 'totalLinhas' => $totalLinhas];
    }

    public function queryBaseItemRemanejamento()
    {
        $arpItem = $this->join("arp_item", function ($query) {
            $query->on("arp_item.arp_id", "=", "arp.id")
                ->whereNull("arp_item.deleted_at")
                ->where("rascunho", false);
        })
            ->join("compra_item_fornecedor", function ($query) {
                $query->on(
                    "compra_item_fornecedor.id",
                    "=",
                    "arp_item.compra_item_fornecedor_id"
                )
                    ->where("compra_item_fornecedor.situacao", true);
            })
            ->join("compra_items", function ($query) {
                $query->on(
                    "compra_items.id",
                    "=",
                    "compra_item_fornecedor.compra_item_id"
                )->where("compra_items.situacao", true);
            })
            ->join("compra_item_unidade", function ($query) {
                $query->on(
                    "compra_items.id",
                    "=",
                    "compra_item_unidade.compra_item_id"
                )->where("compra_item_unidade.situacao", true);
            })
            ->join(
                "unidades",
                "unidades.id",
                "=",
                "compra_item_unidade.unidade_id"
            )
            ->join(
                "fornecedores",
                "fornecedores.id",
                "=",
                "compra_item_fornecedor.fornecedor_id"
            )
            ->join(
                "compras",
                "compras.id",
                "=",
                "arp.compra_id"
            )->join(
                'codigoitens as cgi',
                'cgi.id',
                '=',
                'compra_items.tipo_item_id'
            );

        # Recupera somente os itens que tem saldo para a unidade
        $arpItem->where("compra_item_unidade.quantidade_saldo", ">", 0);
        $arpItem->where("compra_item_unidade.quantidade_autorizada", ">", 0);

        # Recupera somente os itens da Unidade Participante e Gerenciadora
        $arpItem->whereIN("compra_items.id", function ($query) {
            $query->from('compra_item_unidade as ciu1')
                ->whereColumn('ciu1.compra_item_id', 'compra_item_unidade.compra_item_id')
                ->where('ciu1.unidade_id', session('user_ug_id'))
                ->whereIN("compra_item_unidade.tipo_uasg", ['G', 'P'])
                ->select('ciu1.compra_item_id');
        });


        $arpItem->whereNotExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('arp_item_historico')
                ->join(
                    'arp_historico',
                    'arp_historico.id',
                    '=',
                    'arp_item_historico.arp_historico_id'
                )
                ->join('arp_alteracao', function ($query) {
                    $query->on('arp_historico.arp_alteracao_id', 'arp_alteracao.id')
                        ->where('arp_alteracao.rascunho', false);
                })
                ->whereRaw('arp_item_historico.arp_item_id = arp_item.id')
                ->where(function ($query) {
                    $query->where('arp_item_historico.item_cancelado', true)
                        ->orWhere('arp_item_historico.item_removido_retificacao', true);
                });
        });

        return $arpItem;
    }
}
