<?php

namespace App\Repositories\Arp;

use App\Models\ArpItem;
use App\Models\CompraItemFornecedor;
use App\Models\Contrato;
use App\Models\MinutaEmpenho;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ArpItemRepository extends ArpItem
{
    /**
     * Obtém as minutas de empenho de um item
     *
     * @param int $itemId
     * @return Object Lista com as minutas de empenho do item
     */
    public function getMinutasTipoCompraPorItem($itemId)
    {
        return $this->getMinutasTipoCompraPorItemBase($itemId)
            ->where(function ($query) {
                $query->where('codigoitens.descricao', 'EMPENHO EMITIDO')
                    ->orWhere('codigoitens.descricao', 'EM ANDAMENTO')
                    ->orWhere('codigoitens.descricao', 'ERRO');
            })->get();
    }


    /**
     * Obtém os contratos relacionados a um item, agrupando por fornecedor
     *
     * @param integer $compraItemId id do item na tabela compra_item
     * @param integer $compraItemFornecedorId id do fornecedor
     * @return Object Collection com a lista de contratos
     */
    public function getContratosPorItemFornecedor(int $compraItemId, int $compraItemFornecedorId)
    {
        return $this->getContratosPorItemFornecedorBase($compraItemId)
            ->where('compra_item_fornecedor.id', $compraItemFornecedorId)->get();
    }


    /**
     * Verifica se um item de ata tem remanejamento, independente da aprovação, ou seja,
     * se tiver solicitação, considera que houve movimento.
     *
     * @param integer $arpItemId ID do item da ata (arp_item.id)
     * @return Object Collection com a lista de solicitações/remanejamentos do item
     */
    public function getRemanejamentosPorItem(int $arpItemId)
    {
        return ArpItem::select([
            DB::raw("unidades.codigo || ' - ' || unidades.nomeresumido unidade"),
            'arp_remanejamento_itens.quantidade_solicitada',
            DB::raw(
                "CASE
                    WHEN tipo_uasg = 'G'
                    THEN COALESCE(CAST(arp_remanejamento_itens.quantidade_aprovada_gerenciadora AS VARCHAR), '-')
                    ELSE COALESCE(CAST(arp_remanejamento_itens.quantidade_aprovada_participante AS VARCHAR), '-')
                END quantidade_aprovada"
            )
        ])
            ->join('compra_item_fornecedor', function ($join) {
                $join->on('compra_item_fornecedor.id', '=', 'arp_item.compra_item_fornecedor_id')
                    ->where('compra_item_fornecedor.situacao', true);
            })
            ->join('compra_items', function ($join) {
                $join->on('compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')
                    ->where('compra_items.situacao', true);
            })
            ->join('compra_item_unidade', function ($join) {
                $join->on('compra_item_unidade.compra_item_id', '=', 'compra_items.id')
                    ->where('compra_item_unidade.situacao', true);
            })
            ->join('arp_remanejamento_itens', 'arp_remanejamento_itens.id_item_unidade', '=', 'compra_item_unidade.id')
            ->join('unidades', 'compra_item_unidade.unidade_id', '=', 'unidades.id')
            ->where('arp_item.id', $arpItemId)
            #->dd()
            ->get();
    }

    /**
     * Atualiza as datas de vigência do item na tabela compra_item_fornecedor
     *
     * @param integer $arpItemId id do item na tabela arp_item
     * @param string|null $ataVigenciaInicio data de vigência inicial
     * @param string|null $ataVigenciaFim data de vigência final
     * @return void
     */
    public function atualizarVigenciaItem(
        int    $arpItemId,
        string $ataVigenciaInicio = null,
        string $ataVigenciaFim = null
    ) {
        CompraItemFornecedor::join(
            'arp_item',
            'compra_item_fornecedor.id',
            '=',
            'arp_item.compra_item_fornecedor_id'
        )
            ->where('arp_item.id', $arpItemId)
            ->whereNull('arp_item.deleted_at')
            ->update([
                'compra_item_fornecedor.ata_vigencia_inicio' => $ataVigenciaInicio,
                'compra_item_fornecedor.ata_vigencia_fim' => $ataVigenciaFim
            ]);
    }

    /**
     * Retorna dados da tabela compra_item_fornecedor referentes a um item de ata
     *
     * @param integer $arpItemId id do item na tabela arp_item
     * @return Object Collection com dados da tabela compra_item_fornecedor
     */
    public function getItemDeCompraDoItemDaAta(int $arpItemId)
    {
        return ArpItem::select(
            'compra_item_fornecedor.compra_item_id',
            'compra_item_fornecedor.fornecedor_id',
            'compra_item_fornecedor.valor_negociado',
            DB::raw("TO_CHAR(compra_item_fornecedor.valor_unitario *
    ((100-compra_item_fornecedor.percentual_maior_desconto)/100), 'FM999999999.0000') as valor_unitario_desconto"),
            DB::raw("TO_CHAR(
        compra_item_fornecedor.valor_unitario * ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100),
        'FM999999999.0000'
    )::float * compra_item_fornecedor.quantidade_homologada_vencedor as valor_negociado_desconto")
        )
            ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
            ->where('arp_item.id', $arpItemId)
            ->first();
    }

    /**
     * Retorna uma query que busca as adesões de um item de ata
     *
     * @param int $arpItemId ID do item de ata
     * @return \Illuminate\Database\Query\Builder Query para trazer adesões
     */
    private function getAdesoesQuery(int $arpItemId)
    {
        return ArpItem::select(
            DB::raw("unidades.codigo || ' - ' || unidades.nomeresumido unidade"),
            'compra_items.maximo_adesao',
            'arp_solicitacao_item.quantidade_aprovada',
            'arp_solicitacao_item.quantidade_solicitada'
        )
            ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
            ->join('compra_items', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
            ->join('arp_solicitacao_item', 'arp_item.id', '=', 'arp_solicitacao_item.item_arp_fornecedor_id')
            ->join('arp_solicitacao', function ($join) {
                $join->on('arp_solicitacao_item.arp_solicitacao_id', '=', 'arp_solicitacao.id')
                    ->where('arp_solicitacao.rascunho', '=', false);
            })
            ->join('unidades', 'arp_solicitacao.unidade_origem_id', '=', 'unidades.id')
            ->where('arp_item.id', $arpItemId);
    }

    /**
     * Obtém a relação de adesões de um item, agrupados por unidade
     *
     * @param integer $arpItemId id do item na tabela arp_item
     * @return Object Collection com a relação de adesões do item
     */
    public function getAdesoesItem(int $arpItemId)
    {
        return $this->getAdesoesQuery($arpItemId)->get();
    }

    /**
     * @param int $arpItemId id do item na tabela arp_item
     * @return Object Relação das adesões negadas de um item
     */
    public function getAdesoesNegadasItem(int $arpItemId)
    {
        return $this->getAdesoesQuery($arpItemId)
            # Se a quantidade aprovada for zero, a solicitação foi negada por completo
            ->where('arp_solicitacao_item.quantidade_aprovada', '=', '0')
            # Verifica se não é nula para garantir que tenha sido feito o processo de análise
            ->whereNotNull('arp_solicitacao_item.quantidade_aprovada')
            ->get();
    }

    /**
     * @param int $arpItemId id do item na tabela arp_item
     * @return Object Relação das adesões aceitas (total ou parcial) de um item
     */
    public function getAdesoesAceitasItem(int $arpItemId)
    {
        return $this->getAdesoesQuery($arpItemId)
            # Traz somente as solicitações que tiveram alguma aprovação (total ou parcial)
            ->where('arp_solicitacao_item.quantidade_aprovada', '>', 0)
            #->dd()
            ->get();
    }

    /**
     * Método que monta a parte do SELECT com os dados principais dos itens.
     *
     * Este método é utilizado por outros métodos para construir as consultas SELECT
     * que trabalham com os mesmos dados. Ele retorna uma instância do construtor de
     * consultas Eloquent, permitindo que outros métodos adicionem condições, joins, etc.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function selectDadosPrincipaisItens()
    {
        return self::query()
            ->select(
                'compra_items.permite_carona',
                'arp_item.id as arp_item_id',
                'compra_items.compra_id',
                'compra_items.id as compra_item_id',
                'compra_items.numero',
                'compra_items.descricaodetalhada',
                'compra_items.maximo_adesao',
                'fornecedores.cpf_cnpj_idgener',
                'compra_item_fornecedor.id as fornecedor_id',
                'fornecedores.nome AS nomefornecedor',
                'compra_item_fornecedor.valor_unitario',
                'compra_item_fornecedor.valor_negociado',
                DB::raw("TO_CHAR(compra_item_fornecedor.valor_unitario *
    ((100-compra_item_fornecedor.percentual_maior_desconto)/100), 'FM999999999.0000') as valor_unitario_desconto"),
                DB::raw("TO_CHAR(
        compra_item_fornecedor.valor_unitario * ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100),
        'FM999999999.0000'
    )::float * compra_item_fornecedor.quantidade_homologada_vencedor as valor_negociado_desconto"),
                'compra_item_fornecedor.classificacao',
                'compra_item_fornecedor.quantidade_homologada_vencedor',
                'compra_item_fornecedor.percentual_maior_desconto',
                'catmatseritens.codigo_siasg',
                'codigoitens.descricao'
            );
    }


    /**
     * Quando uma query precisa dos valores de desconto de um item,
     * pode utilizar este método para juntar estes valores na consulta
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function selectValoresDosItens()
    {
        return self::query()
            ->select(
                DB::raw("TO_CHAR(compra_item_fornecedor.valor_unitario *
    ((100-compra_item_fornecedor.percentual_maior_desconto)/100), 'FM999999999.0000') as valor_unitario_desconto"),
                DB::raw("TO_CHAR(
        compra_item_fornecedor.valor_unitario * ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100),
        'FM999999999.0000'
    )::float * compra_item_fornecedor.quantidade_homologada_vencedor as valor_negociado_desconto")
            );
    }

    /**
     * Obtém todos os itens de uma ata
     *
     * @param integer $idAta
     * @return Object Collection com os itens de uma ata
     */
    public function getItensDeUmaAta(int $idAta)
    {
        return self::selectDadosPrincipaisItens()
            ->addSelect(self::selectValoresDosItens()->getBindings())
            ->addSelect('compra_item_minuta_empenho.quantidade as quantidade_empenhada')
            ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
            ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
            ->join('compra_items', 'compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')
            ->leftJoin(
                'compra_item_minuta_empenho',
                'compra_item_minuta_empenho.compra_item_id',
                '=',
                'compra_item_fornecedor.compra_item_id'
            )
            ->join('codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id')
            ->join('catmatseritens', 'catmatseritens.id', '=', 'compra_items.catmatseritem_id')
            ->where('arp_item.arp_id', $idAta)
            ->orderBy('compra_items.numero')
            #->dd()
            ->get();
    }


    /**
     * Busca os itens de uma ata agrupado por unidade
     *
     * @param int $idAta
     * @return mixed
     */
    public function getItensDeUmaAtaAgrupadosPorItemDaCompraEPorUnidade(int $idAta, int $unidadeId)
    {
        return ArpItem::select(
            'arp_item.id',
            'compra_items.numero',
            'compra_items.descricaodetalhada',
            'compra_items.grupo_compra',
            DB::raw("CONCAT(fornecedores.cpf_cnpj_idgener,'-',fornecedores.nome) nome_fornecedor"),
            DB::raw("(SELECT SUM(compra_item_minuta_empenho.quantidade)
                              FROM compra_item_unidade
                              LEFT JOIN minutaempenhos ON minutaempenhos.unidade_id = compra_item_unidade.unidade_id
                                 AND minutaempenhos.fornecedor_compra_id = compra_item_fornecedor.fornecedor_id
                                 AND minutaempenhos.compra_id = compra_items.compra_id
                                 AND minutaempenhos.situacao_id = 217
                              LEFT JOIN compra_item_minuta_empenho ON 
                              minutaempenhos.id = compra_item_minuta_empenho.minutaempenho_id
                                 AND compra_items.id = compra_item_minuta_empenho.compra_item_id
                              LEFT JOIN minutaempenhos_remessa ON 
                              compra_item_minuta_empenho.minutaempenhos_remessa_id = minutaempenhos_remessa.id
                                 AND (
                                    (minutaempenhos_remessa.remessa = 0 AND minutaempenhos_remessa.situacao_id = 215)
                                    OR (minutaempenhos_remessa.remessa > 0 AND minutaempenhos_remessa.situacao_id = 217)
                                 )
                              LEFT JOIN codigoitens ON codigoitens.id = minutaempenhos.tipo_empenhopor_id
                                 AND codigoitens.descricao = 'Compra'
                              INNER JOIN unidades ON unidades.id = compra_item_unidade.unidade_id
                              WHERE compra_items.id = compra_item_unidade.compra_item_id
                                 AND compra_item_unidade.situacao = true
                             ) AS quantidade_empenhada"),
            DB::raw("EXISTS( SELECT 1 FROM compra_item_unidade 
                                    WHERE compra_item_unidade.compra_item_id = compra_items.id and
                                               compra_item_unidade.unidade_id ={$unidadeId}) unidade_participa")
        )
            ->join('compra_item_fornecedor', function ($join) {
                $join->on('compra_item_fornecedor.id', '=', 'arp_item.compra_item_fornecedor_id')
                    ->where('compra_item_fornecedor.situacao', '=', true);
            })
            ->join('compra_items', function ($join) {
                $join->on('compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')
                    ->where('compra_items.situacao', '=', true);
            })
            ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
            ->where('arp_id', $idAta)
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('arp_item_historico')
                    ->join('arp_historico', 'arp_historico.id', '=', 'arp_item_historico.arp_historico_id')
                    ->leftjoin('arp_alteracao', function ($query) {
                        $query->on('arp_historico.arp_alteracao_id', 'arp_alteracao.id')
                            ->where('arp_alteracao.rascunho', false);
                    })
                    ->whereRaw('arp_item_historico.arp_item_id = arp_item.id')
                    ->where(function ($query) {
                        $query->where('arp_item_historico.item_cancelado', true)
                            ->orWhere('arp_item_historico.item_removido_retificacao', true);
                    });
            })
            ->orderBy('compra_items.grupo_compra', 'desc')
            ->orderBy('compra_items.numero')
            ->get();
    }

    /**
     * Obtém todos os itens de uma ata que não tiverem movimentação
     * Devido ao fato de, quando uma ata for alterada, seus itens são armazenados no histórico mesmo que não houve
     * movimentação no mesmo, foi utilizada a lógica abaixo para buscar os itens que não tiveram uma movimentação
     * real, mas estão no histórico.
     *
     * 1. Itens que tenham:
     * 1.1. Somente o registro de 'Ata Inicial'
     * 1.2. Registros posteriores (diferente de Ata Inicial) mas todas as informações de histórico estejam nulas/falsas
     *      Necessário, pois um item em que a Ata tenha outro item cancelado,
     *      vai ser registrado no histórico do item devido à alteração da ata
     * 2. Itens que tenham
     * 2.1. Registros posteriores diferentes de Ata Inicial com qualquer informação
     *      Significa que houve informação alterada
     * 2.2. Registros posteriores diferentes de Retificar
     *      Ao fazer uma retificação da ata envolvendo data de vigência,
     *      todos os itens são registrados no histórico com essa alteração de data, mas neste caso,
     *      o item ainda pode ser removido numa próxima retificação
     *
     * @param integer $idAta
     * @return Object Collection com os itens
     */
    public function getItensSemMovimentacaoPorAta(int $idAta)
    {
        return self::selectDadosPrincipaisItens()
            ->addSelect(self::selectValoresDosItens()->getBindings())
            ->addSelect("compra_items.maximo_adesao_informado_compra")
            ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
            ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
            ->join('compra_items', 'compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id')
            ->join('catmatseritens', 'catmatseritens.id', '=', 'compra_items.catmatseritem_id')
            ->where('arp_item.arp_id', $idAta)
            ->where(function ($query) {
                $query->where(function ($subquery) {
                    $subquery->whereIn('arp_item.id', function ($subsubquery) {
                        $subsubquery->select('arp_item_historico.arp_item_id')
                            ->from('arp_item_historico')
                            ->join('arp_historico', 'arp_historico.id', '=', 'arp_item_historico.arp_historico_id')
                            ->join('codigoitens', 'codigoitens.id', '=', 'arp_historico.tipo_id')
                            ->where('codigoitens.descricao', '<>', 'Ata Inicial')
                            ->where('arp_item_historico.item_cancelado', false)
                            ->whereNull('arp_item_historico.valor')
                            #->whereNull('arp_item_historico.vigencia_inicial')
                            ->whereNull('arp_item_historico.vigencia_final')
                            ->where('arp_item_historico.valor_alterado', false)
                            ->whereNull('arp_item_historico.percentual_maior_desconto')
                            ->where('arp_item_historico.item_removido_retificacao', false);
                    })->whereNotIn('arp_item.id', function ($subsubquery) {
                        $subsubquery->select('arp_item_historico.arp_item_id')
                            ->from('arp_item_historico')
                            ->join('arp_historico', 'arp_historico.id', '=', 'arp_item_historico.arp_historico_id')
                            ->join('codigoitens', 'codigoitens.id', '=', 'arp_historico.tipo_id')
                            ->where('codigoitens.descricao', '<>', 'Ata Inicial')
                            # Um item que está em 'Retificar' é porque somente a Ata foi retificada
                            ->where('codigoitens.descricao', '<>', 'Retificar')
                            ->where(function ($subsubsubquery) {
                                $subsubsubquery->where('arp_item_historico.item_cancelado', true)
                                    ->orWhereNotNull('arp_item_historico.valor')
                                    #->orWhereNotNull('arp_item_historico.vigencia_inicial')
                                    ->orWhereNotNull('arp_item_historico.vigencia_final')
                                    ->orWhere('arp_item_historico.valor_alterado', true)
                                    ->orWhereNotNull('arp_item_historico.percentual_maior_desconto')
                                    ->orWhere('arp_item_historico.item_removido_retificacao', true);
                            });
                    });
                })->orWhereIn('arp_item.id', function ($subquery) {
                    $subquery->select('arp_item_historico.arp_item_id')
                        ->from('arp_item_historico')
                        ->groupBy('arp_item_historico.arp_item_id')
                        ->havingRaw('COUNT(*) = 1');
                })
                    ->orWhereNotExists(function ($subquery) {
                        $subquery->select('arp_item_historico.id')
                            ->from('arp_item_historico')
                            ->whereColumn('arp_item_historico.arp_item_id', 'arp_item.id');
                    });
            })
            ->orderBy('compra_items.numero')
            #->dd()
            ->get();
    }

    /**
     * Obtém a lista dos itens removidos em retificação de uma ata
     *
     * @param integer $idAta
     * @return Object Colletcion com a lista de itens
     */
    public function getItensRemovidosEmRetificacaoPorAta(int $idArpHistorico)
    {
        return ArpItem::withTrashed()
            ->select(
                DB::raw("fornecedores.cpf_cnpj_idgener || ' - ' || fornecedores.nome fornecedor"),
                'compra_items.numero',
                'compra_items.descricaodetalhada',
                'compra_item_fornecedor.valor_unitario'
            )
            ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
            ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
            ->join('compra_items', 'compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')
            ->join('arp_item_historico', function ($join) {
                $join->on('arp_item_historico.arp_item_id', '=', 'arp_item.id')
                    ->where('arp_item_historico.item_removido_retificacao', true);
            })
            ->join('arp_historico', 'arp_item_historico.arp_historico_id', '=', 'arp_historico.id')
            #->where('arp_item.arp_id', $idAta)
            ->where('arp_historico.id', $idArpHistorico)
            #->dd()
            ->get();
    }
    
    public function getMinutasTipoCompraPorItemBase(int $itemId)
    {
        return MinutaEmpenho::join(
            "compra_item_minuta_empenho",
            "minutaempenhos.id",
            "=",
            "compra_item_minuta_empenho.minutaempenho_id"
        )
            ->join("codigoitens", "codigoitens.id", "=", "minutaempenhos.situacao_id")
            ->join("codigos", "codigos.id", "=", "codigoitens.codigo_id")
            ->join("unidades", "unidades.id", "=", "minutaempenhos.unidade_id")
            ->where('codigos.descricao', 'Situações Minuta Empenho')
            ->where("compra_item_minuta_empenho.compra_item_id", $itemId);
    }
    
    public function getContratosPorItemFornecedorBase(int $compraItemId)
    {
        return Contrato::select([
            'contratos.numero',
            'contratos.data_assinatura',
            'contratos.vigencia_inicio',
            'contratos.vigencia_fim',
            'contratoitens.*',
            'codigoitens.descres',
            'unidades.codigo',
            'unidades.nomeresumido'
        ])
            ->join('contratoitens', function ($join) {
                $join->on('contratoitens.contrato_id', '=', 'contratos.id')
                    ->whereNull('contratoitens.deleted_at');
            })
            ->join('compras', function ($join) {
                $join->on('contratos.unidadecompra_id', '=', 'compras.unidade_origem_id')
                    ->on('contratos.modalidade_id', '=', 'compras.modalidade_id')
                    ->on('contratos.licitacao_numero', '=', 'compras.numero_ano');
            })
            ->join('compra_items', function ($join) {
                $join->on('compra_items.compra_id', '=', 'compras.id')
                    ->where('compra_items.situacao', true)
                    ->whereColumn('compra_items.numero', '=', 'contratoitens.numero_item_compra');
            })
            ->join('compra_item_fornecedor', function ($join) {
                $join->on('compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
                    ->where('compra_item_fornecedor.situacao', true)
                    ->whereColumn('compra_item_fornecedor.fornecedor_id', '=', 'contratos.fornecedor_id');
            })
            ->join('unidades', 'contratos.unidade_id', '=', 'unidades.id')
            ->join('codigoitens', 'codigoitens.id', 'contratoitens.tipo_id')
            ->where('compra_items.id', $compraItemId);
    }
    
    public function getMinutasTipoCompraPorItemConsumiuSaldoUnidade(
        int $compraItemId,
        int $idUnidade,
        int $compraItemFornecedorId
    ) {
        $minutaEmpenhoBase = $this->getMinutasTipoCompraPorItemBase($compraItemId);
        $minutaEmpenhoBase->join('compra_items', 'compra_item_minuta_empenho.compra_item_id', 'compra_items.id');
        $minutaEmpenhoBase->join('compra_item_unidade', 'compra_items.id', 'compra_item_unidade.compra_item_id');
        
        $minutaEmpenhoBase->where("minutaempenhos.unidade_id", $idUnidade);
        $minutaEmpenhoBase->where("compra_item_minuta_empenho.compra_item_fornecedor_id", $compraItemFornecedorId);
        
        $minutaEmpenhoBase->selectRaw(
            "distinct compra_item_unidade.quantidade_autorizada != compra_item_unidade.quantidade_saldo
            as saldo_consumido"
        );
        
        return $minutaEmpenhoBase->get();
    }
    
    public function getContratosPorItemFornecedorUnidadeOrigemContrato(
        int $compraItemId,
        int $compraItemFornecedorId,
        int $idUnidade
    ) {
        $contratosBase =  $this->getContratosPorItemFornecedorBase($compraItemId);
        
        return $contratosBase->where('contratos.fornecedor_id', $compraItemFornecedorId)
            ->where('contratos.unidadeorigem_id', $idUnidade)->get();
    }
    
    public function getItemAtaUnico(int $id)
    {
        return ArpItem::find($id);
    }

    public function getMinutasTipoCompraPorItemEFornecedor(int $itemId, int $fornecedorId)
    {
        return $this->getMinutasTipoCompraPorItemBase($itemId)
            ->where('minutaempenhos.fornecedor_compra_id', $fornecedorId)
            ->where(function ($query) {
                $query->where('codigoitens.descricao', 'EMPENHO EMITIDO')
                    ->orWhere('codigoitens.descricao', 'EM ANDAMENTO')
                    ->orWhere('codigoitens.descricao', 'ERRO');
            })->get();
    }
}
