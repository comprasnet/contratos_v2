<?php

namespace App\Repositories\Arp;

use App\Models\ArpAlteracao;

class ArpAlteracaoRepository extends ArpAlteracao
{

    /**
     * Método responsável em retornar todas as informações das alterações
     */
    public function retornarInformacoesAlteracao(int $idAlteracao, array $tipoAlteracao)
    {
        return $this->join("arp_historico", "arp_alteracao.id", "=", "arp_historico.arp_alteracao_id")
                    ->where("arp_alteracao.id", $idAlteracao)
                    ->whereIn("tipo_id", $tipoAlteracao)
                    ->select("arp_historico.*")
                    ->get();
    }
}
