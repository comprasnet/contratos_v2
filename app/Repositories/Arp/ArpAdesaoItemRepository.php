<?php

namespace App\Repositories\Arp;

use App\Http\Traits\Formatador;
use App\Models\AdesaoItem;

class ArpAdesaoItemRepository extends AdesaoItem
{
    use Formatador;

    /**
     * Método responsável em inserir o item para adesão
     */
    public function inserirItemAdesaoArp(array $condicionalInsercao)
    {
        $valorResultado = $condicionalInsercao;
        $responsavel =  auth()->user()->id;
        $valorResultado['responsavel_id'] = $responsavel;

        # Campos necessários para saber se o registro será atualizado ou incluído
        //$condicionalInsercao = $this->getCondicional($idSolicitacao, $itemAta, $quantidade, $statusIdItem);
        AdesaoItem::updateOrInsert($condicionalInsercao, $valorResultado);
    }

    public function getItemAdesaoSolicitada(int $idSolicitacao)
    {
        return $this->join("codigoitens", "arp_solicitacao_item.status_id", "=", "codigoitens.id")
            ->join("codigos", "codigoitens.codigo_id", "=", "codigos.id")
            ->join(
                "arp_item",
                "arp_item.id",
                "=",
                "arp_solicitacao_item.item_arp_fornecedor_id"
            )
            ->join(
                "compra_item_fornecedor",
                "compra_item_fornecedor.id",
                "=",
                "arp_item.compra_item_fornecedor_id"
            )
            ->join(
                "compra_items",
                "compra_items.id",
                "=",
                "compra_item_fornecedor.compra_item_id"
            )
            ->whereIN("codigoitens.descricao", ["Aceitar", "Aceitar Parcialmente"])
            ->where("codigos.descricao", "Tipo de Ata de Registro de Preços")
            ->where("arp_solicitacao_id", $idSolicitacao)
            ->selectRaw(
                "compra_item_id, fornecedor_id,
                SUM(arp_solicitacao_item.quantidade_aprovada) AS quantidade_aprovada,
                compra_items.numero"
            )
            ->groupBy(
                "compra_item_id",
                "fornecedor_id",
                "quantidade_aprovada",
                "compra_items.numero"
            )
            ->get();
    }

    public function getTotalItemAdesao(int $idSolicitacao)
    {
        return $this->where("arp_solicitacao_id", $idSolicitacao)->count();
    }

    public function getItemAdesaoUnico(int $idItemAdesao)
    {
        return $this->find($idItemAdesao);
    }

    public function getSolicitacaoAdesaoAceitaPorItemAta(int $idItemAta)
    {
        return  $this->join('arp_solicitacao', function ($join) {
            $join->on('arp_solicitacao.id', '=', 'arp_solicitacao_item.arp_solicitacao_id')
                ->where('arp_solicitacao.rascunho', false);
        })
            ->join('codigoitens', 'arp_solicitacao.status', '=', 'codigoitens.id')
            ->join('codigoitens as ciItem', 'ciItem.id', '=', 'arp_solicitacao_item.status_id')
            ->whereIn('codigoitens.descricao', ['Aceito Parcial', 'Aceita', 'Enviada para aceitação'])
            ->whereIn('ciItem.descricao', ['Aceitar Parcialmente', 'Aceitar', 'Item Não Avaliado'])
            ->where('arp_solicitacao_item.item_arp_fornecedor_id', $idItemAta)
            ->select('arp_solicitacao_item.*')
            ->get();
    }

    /**
     * @param int $idSolicitacao
     * @param $itemAta
     * @param $quantidade
     * @param $statusIdItem
     * @return array
     */
    public function getCondicional(int $idSolicitacao, $itemAta, $quantidade, $statusIdItem): array
    {
        $condicionalInsercao = [
            'arp_solicitacao_id' => $idSolicitacao,
            'item_arp_fornecedor_id' => $itemAta,
            'quantidade_solicitada' => $quantidade, 'status_id' => $statusIdItem
        ];
        return $condicionalInsercao;
    }
    public function getCondicionalResult(array $condicionalInsercao)
    {
        return AdesaoItem::where($condicionalInsercao)->first();
    }
}
