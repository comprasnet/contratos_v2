<?php

namespace App\Repositories\Arp;

use App\Models\Arp;
use App\Models\ArpHistorico;

class ArpHistoricoRepository extends ArpHistorico
{
    /**
     * Método responsável em retornar a quantidade de alteração que a ata sofreu
     * pelo tipo de alteração
     */
    public function quantidadeHistoricoPorTipoAlteracao(int $idArp, array $idTipoAlteracao)
    {
        return $this->where("arp_id", $idArp)
            ->whereIn("tipo_id", $idTipoAlteracao)
            ->count();
    }

    /**
     * Método responsável em retornar a quantidade de alteração do tipo Vigência
     */
    public function quantidadeAlteracaoTipoVigencia(int $idArp, $idTipoAlteracao)
    {
        return $this->leftjoin(
            "arp_alteracao",
            "arp_historico.arp_alteracao_id",
            "=",
            "arp_alteracao.id"
        )
            ->where("rascunho", false)
            ->where("arp_alteracao.arp_id", $idArp)
            ->whereIn("tipo_id", $idTipoAlteracao)
            ->count();
    }

    /**
     * Método responsável em retornar as informações do histórico para um alteração
     */
    public function retornarDadosAlteracao(int $idArp)
    {
        return $this->where("arp_id", $idArp)
            ->whereNotNull("arp_alteracao_id")
            ->orderby("id", "desc")
            ->first();
    }

    /**
     * Método responsável em retornar as informações do objeto da alteração
     */
    public function retornarObjetoAlteracao(int $idArp, int $idArpAlteracao)
    {
        return $this->where("arp_id", $idArp)
            ->where("arp_alteracao_id", $idArpAlteracao)
            ->whereNotNull("objeto_alteracao")
            ->first();
    }

    public function getUltimoEstadoDaAta(int $idArp, $unidadeExecutoraId)
    {
        # Recupera as informações inicias da ata na tabela arp_historico
        # Busca o histórico feito pela unidade executora
        $ataInicial = ArpHistorico::where("arp_id", $idArp)
            ->where("unidade_executora_id", $unidadeExecutoraId)
            ->orderBy("id", "desc")
            ->first();

        # Se existir histórico, retorna
        if (!empty($ataInicial)) {
            return $ataInicial->toArray();
        }

        # Busca as informações atuais da ata
        $arp = Arp::find($idArp);

        # Converte o objeto em array para inserir na tabela Arp Histórico
        $arpArray = $arp->toArray();
        $arpArray['arp_id'] = $idArp;
        $arpArray['unidade_executora_id'] = $unidadeExecutoraId;
        return $arpArray;
    }
}
