<?php

namespace App\Repositories\Arp;

use App\Models\ArpItemHistorico;

class ArpItemHistoricoRepository extends ArpItemHistorico
{
    /**
     * Método responsável em retornar o histórico do item que tiveram os valores alterados
     */
    public function retornarItensValoresAlteradosHistorico(int $ataHistoricoId)
    {
        return $this->where("arp_historico_id", $ataHistoricoId)
                    ->where("valor_alterado", true)->get();
    }

    /**
     * Método responsável em retornar o histórico do item que forma cancelados
     */
    public function retornarItensCanceladosHistorico(int $ataHistoricoId)
    {
        return $this->where("arp_historico_id", $ataHistoricoId)
                    ->where("item_cancelado", true)
                    ->get();
    }

    /**
     * Método responsável em retornar as informações dos itens por alteração de valores
     */
    public function retornarItensValoresAlteradosPorAlteracao(
        int $idAlteracao,
        int $tipoId,
        int $arpItemId
    ) {
        return $this->join(
            "arp_historico",
            "arp_historico.id",
            "=",
            "arp_item_historico.arp_historico_id"
        )
        ->where("arp_alteracao_id", $idAlteracao)
        ->where("arp_historico.tipo_id", $tipoId)
        ->where("arp_item_id", $arpItemId)
        ->where("valor_alterado", true)
        ->select("arp_item_historico.valor")
        ->first();
    }

    /**
     * Método responsável em retornar as informações dos itens por cancelamento do item
     */
    public function retornarItensCanceladoPorAlteracao(
        int $idAlteracao,
        int $tipoId,
        int $arpItemId
    ) {
        return $this->join(
            "arp_historico",
            "arp_historico.id",
            "=",
            "arp_item_historico.arp_historico_id"
        )
        ->where("arp_alteracao_id", $idAlteracao)
        ->where("arp_historico.tipo_id", $tipoId)
        ->where("arp_item_id", $arpItemId)
        ->where("item_cancelado", true)
        ->first();
    }

    /**
     * Método responsável em retornar os itens que estão cancelados
     */
    public function retornarItensNaoCancelados()
    {
        return $this->join(
            "arp_historico",
            "arp_historico.id",
            "=",
            "arp_item_historico.arp_historico_id"
        )
        ->join("arp_alteracao", "arp_alteracao.id", "=", "arp_historico.arp_alteracao_id")
        ->join("codigoitens", "codigoitens.id", "=", "arp_historico.tipo_id")
        ->where("codigoitens.descricao", "Cancelamento de item(ns)")
        ->where("item_cancelado", true)
        ->where("arp_alteracao.rascunho", false)
        ->get();
    }
}
