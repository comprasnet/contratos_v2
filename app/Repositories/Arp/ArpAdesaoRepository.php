<?php

namespace App\Repositories\Arp;

use App\Models\Adesao;
use App\Models\Arp;
use App\Models\ArpItem;
use App\Models\Unidade;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ArpAdesaoRepository extends Adesao
{
    /**
     * Método responsável em montar o select do numéro da compra
     */
    public function montarSelectNumeroCompraAdesao(string $numeroAno, int $unidadeGerenciadora)
    {
        return Arp::join("compras", "arp.compra_id", "=", "compras.id")
        ->where("arp.rascunho", false)
        ->where("arp.unidade_origem_id", $unidadeGerenciadora)
        ->where("compras.numero_ano", "ilike", "%$numeroAno%")
        ->select("compras.id", "compras.numero_ano AS numero_ano_adesao_arp")
        ->groupBy("compras.id", "compras.numero_ano")
        ->paginate(10);
    }

    public function montarSelectNumeroAtaAdesao(
        string $numeroArp,
        int $unidadeGerenciadora,
        ?string $numeroCompra,
        ?int $idModalidade
    ) {
        return  Arp::join("compras", "arp.compra_id", "=", "compras.id")
            ->join("codigoitens", "compras.modalidade_id", "=", "codigoitens.id")
            ->join("arp_item", "arp.id", "=", "arp_item.arp_id")
            ->join("compra_item_fornecedor", "arp_item.compra_item_fornecedor_id", "=", "compra_item_fornecedor.id")
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->join("compra_items", "compra_items.id", "=", "compra_item_fornecedor.compra_item_id")
            ->where("arp.rascunho", false)
            ->where("arp.unidade_origem_id", $unidadeGerenciadora)
            ->where(function ($query) use ($numeroArp) {
                $query->whereRaw('arp.numero || \'/\' || arp.ano ILIKE ?', ["%{$numeroArp}%"]);
            })
            //->select("arp.id", DB::raw("CONCAT(arp.numero, '/', arp.ano) AS ata_arp_adesao"))
            ->select("arp.id", "arp.numero", "arp.ano", DB::raw("CONCAT(arp.numero, '/', arp.ano) AS ata_arp_adesao"))
            ->groupBy("arp.id", "arp.numero", "arp.ano")
            ->paginate(10);


        if ($numeroCompra) {
            $numeroCompra = str_replace("-", "/", $numeroCompra);
            $query->where("compras.numero_ano", $numeroCompra);
        }

        if ($idModalidade) {
            $query->where("codigoitens.id", $idModalidade);
        }

      //  return  $query->get()->toArray();
    }

    /**
     * Método responsável em montar o select para exibir o campo Modalidade da Compra
     */
    public function montarSelectModalidadeCompraAdesao(int $unidadeGerenciadora, $numeroCompra)
    {
        # Recupera o número da Compra e formata para o padrão salvo no banco de dados
        $numeroCompra = str_replace("-", "/", $numeroCompra);

        return Arp::join("compras", "arp.compra_id", "=", "compras.id")
        ->join("codigoitens", "compras.modalidade_id", "=", "codigoitens.id")
        ->where("arp.rascunho", false)
        ->where("arp.unidade_origem_id", $unidadeGerenciadora)
        ->where("compras.numero_ano", $numeroCompra)
        ->select(
            "codigoitens.id",
            DB::raw("CONCAT(codigoitens.descres, ' - ', codigoitens.descricao   ) AS text")
        )
        ->groupBy("codigoitens.id", "codigoitens.descres", "codigoitens.descricao")
        ->get()
        ->toArray();
    }

    /**
     * Método responsável em montar o select para exibir no campo Número da ata
     * BKP - REGRA FOI MUDADA, PARA UTILIZAR A UNIDADE JUNTO
     */
//    public function montarSelectNumeroAtaAdesao(
//        string $numeroArp,
//        ?int $unidadeGerenciadora,
//        ?string $numeroCompra,
//        ?int $idModalidade
//    ) {
//        $query = Arp::join("compras", "arp.compra_id", "=", "compras.id")
//            ->join("codigoitens", "compras.modalidade_id", "=", "codigoitens.id")
//            ->join("arp_item", "arp.id", "=", "arp_item.arp_id")
//            ->join("compra_item_fornecedor", "arp_item.compra_item_fornecedor_id", "=", "compra_item_fornecedor.id")
//            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
//            ->join("compra_items", "compra_items.id", "=", "compra_item_fornecedor.compra_item_id")
//            ->where("arp.rascunho", false)
//            ->where(function ($query) use ($numeroArp) {
//                $query->whereRaw('arp.numero || \'/\' || arp.ano ILIKE ?', ["%{$numeroArp}%"]);
//            })
//            //->select("arp.id", DB::raw("CONCAT(arp.numero, '/', arp.ano) AS ata_arp_adesao"))
//            ->select("arp.id", "arp.numero", "arp.ano", DB::raw("CONCAT(arp.numero, '/', arp.ano) AS ata_arp_adesao"))
//            ->groupBy("arp.id", "arp.numero", "arp.ano");
//
//        if ($unidadeGerenciadora) {
//            $query->where("arp.unidade_origem_id", $unidadeGerenciadora);
//        }
//
//        if ($numeroCompra) {
//            $numeroCompra = str_replace("-", "/", $numeroCompra);
//            $query->where("compras.numero_ano", $numeroCompra);
//        }
//
//        if ($idModalidade) {
//            $query->where("codigoitens.id", $idModalidade);
//        }
//
//        return  $query->get()->toArray();
//    }

    /**
     * Método responsável em montar o select para exibir no campo Fornecedor
     */
    public function montarSelectFornecedorAdesao(
        ?string $dadosFornecedor,
        ?int $unidadeGerenciadora,
        ?string $numeroCompra,
        ?int $idModalidade
    ) {
        if (empty($dadosFornecedor)) {
            return [];
        }

        $query = Arp::join("compras", "arp.compra_id", "=", "compras.id")
        ->join("codigoitens", "compras.modalidade_id", "=", "codigoitens.id")
        ->join("arp_item", "arp.id", "=", "arp_item.arp_id")
        ->join("compra_item_fornecedor", "arp_item.compra_item_fornecedor_id", "=", "compra_item_fornecedor.id")
        ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
        ->join("compra_items", "compra_items.id", "=", "compra_item_fornecedor.compra_item_id")
        ->where("arp.rascunho", false)
        ->where(function ($query) use ($dadosFornecedor) {
            $query->whereRaw("unaccent(lower(fornecedores.nome)) LIKE unaccent(lower('%{$dadosFornecedor}%'))")
                //->orWhereRaw("REGEXP_REPLACE(unaccent(fornecedores.cpf_cnpj_idgener), '[^0-9./-]', '')
                //LIKE CONCAT('%', REGEXP_REPLACE(unaccent('{$search_term}'), '[^0-9./-]', ''), '%')");
                ->orWhereRaw(
                    "translate(unaccent(fornecedores.cpf_cnpj_idgener), '.-/','') LIKE
                    translate(unaccent('%{$dadosFornecedor}%'), '.-/','')"
                );
        })

        ->select(
            "fornecedores.id",
            DB::raw("CONCAT(fornecedores.cpf_cnpj_idgener, ' - ', fornecedores.nome) AS fornecedor_arp_adesao")
        )
        ->groupBy("fornecedores.id", "fornecedores.cpf_cnpj_idgener", "fornecedores.nome");
        if ($unidadeGerenciadora) {
            $query->where("arp.unidade_origem_id", $unidadeGerenciadora);
        }

        if ($numeroCompra) {
            $numeroCompra = str_replace("-", "/", $numeroCompra);
            $query->where("compras.numero_ano", $numeroCompra);
        }

        if ($idModalidade) {
            $query->where("codigoitens.id", $idModalidade);
        }

        return  $query->get()->toArray();
    }

    /**
     * Método responsável em exibir os itens aprovados para solicitar na adesão
     */
    public function recuperarItemAprovadoAdesao(array $filtro, bool $totalQuantidadeAprovada)
    {
        $totalItemAprovado = $this->join("codigoitens AS ci1", "arp_solicitacao.status", "=", "ci1.id")
            ->join("codigos AS c1", "ci1.codigo_id", "=", "c1.id")
            ->join("arp_solicitacao_item", "arp_solicitacao.id", "=", "arp_solicitacao_item.arp_solicitacao_id")
            ->join("codigoitens", "arp_solicitacao_item.status_id", "=", "codigoitens.id")
            ->join("codigos", "codigoitens.codigo_id", "=", "codigos.id")
            ->join("arp_item", "arp_item.id", "=", "arp_solicitacao_item.item_arp_fornecedor_id")
            ->join("compra_item_fornecedor", "arp_item.compra_item_fornecedor_id", "=", "compra_item_fornecedor.id")
            ->join("compra_items", "compra_items.id", "=", "compra_item_fornecedor.compra_item_id")
            // ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            // ->join("compra_item_unidade",
            //"compra_item_unidade.compra_item_id", "=", "compra_item_fornecedor.compra_item_id")
            ->whereiN("ci1.descricao", ["Aceita", "Aceito Parcial"])
            ->where("c1.descricao", "Tipo de Ata de Registro de Preços")

            ->where($filtro)
            ->whereIN("codigoitens.descricao", ["Aceitar", "Aceitar Parcialmente"])
            ->where("codigos.descricao", "Tipo de Ata de Registro de Preços");

        if ($totalQuantidadeAprovada) {
            return $totalItemAprovado->sum('quantidade_aprovada');
        }

        return $totalItemAprovado->get();
    }

    public function itensAdesao(
        ?int $modadelidadeId = null,
        ?int $unidadeOrigemId = null,
        ?string $numeroCompra = null,
        ?int $fornecedorId = null,
        ?string $numeroAta = null,
        ?string $itensAtaFiltro = null,
        bool $execucaoDescentralizadaProgramaProjetoFederal = null,
        bool $aquisicaoEmergencialMedicamentoMaterial = null,
        ?int $idSolicitacao = null,
        int $idUnidadeUsuarioLogado = null,
        string $seach
    ) {
        $itens = $this->recuperaItem(
            $modadelidadeId,
            $unidadeOrigemId,
            $numeroCompra,
            $fornecedorId,
            $numeroAta,
            $itensAtaFiltro,
            $execucaoDescentralizadaProgramaProjetoFederal,
            $aquisicaoEmergencialMedicamentoMaterial,
            $idSolicitacao,
            $idUnidadeUsuarioLogado
        );


        $itens->where(function ($query) use ($seach) {
            $query->where('compra_items.descricaodetalhada', 'ilike', "%$seach%")
                ->orWhere('compra_items.catmatseritem_id', 'ilike', "%$seach%");
        });

        return  $itens->get()->toArray();
    }

    public function recuperaItem(
        ?int $modadelidadeId = null,
        ?int $unidadeOrigemId = null,
        ?string $numeroCompra = null,
        ?int $fornecedorId = null,
        ?string $numeroAta = null,
        ?int $itensAtaFiltro = null,
        bool $execucaoDescentralizadaProgramaProjetoFederal = null,
        bool $aquisicaoEmergencialMedicamentoMaterial = null,
        ?int $idSolicitacao = null,
        int $idUnidadeUsuarioLogado = null,
        bool $ataEnfrentandoImpactoDecorrenteCalamidadePublica = false
    ) {
        $itens = ArpItem::join(
            "compra_item_fornecedor",
            function ($query) {
                $query->on(
                    "compra_item_fornecedor.id",
                    "arp_item.compra_item_fornecedor_id"
                )
                    ->whereNull('arp_item.deleted_at')
                    # Exibe somente os itens que estão vigentes
                    ->whereNotNull('compra_item_fornecedor.ata_vigencia_fim');
            }
        );
        
        if (!empty($idSolicitacao)) {
            $itens->join("arp_solicitacao_item", "arp_solicitacao_item.item_arp_fornecedor_id", "=", "arp_item.id")
                ->where("arp_solicitacao_item.arp_solicitacao_id", $idSolicitacao);
        }
        
        $itens->join("compra_items", function ($query) {
                $query->on("compra_items.id", "compra_item_fornecedor.compra_item_id")
                    ->whereNull('compra_items.deleted_at')
                    # Exibe somente os itens que possam receber carona de outras unidades
                    ->where("compra_items.permite_carona", true);
        })
        ->join('codigoitens as cgi', 'cgi.id', '=', 'compra_items.tipo_item_id')
        ->join("compras", "compras.id", "=", "compra_items.compra_id")
        ->join("arp", function ($query) {
            $query->on("arp.id", "arp_item.arp_id")
                ->where("arp.rascunho", false);
        })
        ->join("unidades", "unidades.id", "=", "arp.unidade_origem_id")
        ->join('codigoitens', 'compras.modalidade_id', '=', 'codigoitens.id')
        ->join("fornecedores", "compra_item_fornecedor.fornecedor_id", "=", "fornecedores.id")
        
        ->whereRaw(
            '? between compra_item_fornecedor.ata_vigencia_inicio and compra_item_fornecedor.ata_vigencia_fim',
            Carbon::now()->toDateString()
        );
        
        # Exibe somente se a unidade não for unidade sub-rogada
        $itens = $itens->where(function ($query) use ($idUnidadeUsuarioLogado) {
            $query->whereNull("compras.unidade_subrrogada_id")
                ->orWhere("compras.unidade_subrrogada_id", "<>", $idUnidadeUsuarioLogado);
        });
        
        if (!$ataEnfrentandoImpactoDecorrenteCalamidadePublica) {
            # Recuperar as informações da unidade logado do usuário
            $dadosUnidadeLogada = Unidade::find($idUnidadeUsuarioLogado);
            
            if (empty($dadosUnidadeLogada->esfera) ||
                $dadosUnidadeLogada->esfera === 'Estadual') {
                $itens->whereIn("unidades.esfera", ['Estadual', 'Federal']);
            }
            
            if ($dadosUnidadeLogada->esfera === 'Municipal') {
                $itens->whereIn("unidades.esfera", ['Estadual', 'Federal', 'Municipal']);
            }
            
            if ($dadosUnidadeLogada->esfera === 'Federal') {
                $itens->where("unidades.esfera", $dadosUnidadeLogada->esfera);
            }
        }
        
        # Aplica o filtro para que não possa ultrapassar o dobro que está liberado para o fornecedor
        $queryQuantidadeAprovadaFiltro = "
            (select count(compra_item_unidade.tipo_uasg)
             from compras c1
                      join compra_items ci1 on c1.id = ci1.compra_id
                      join compra_item_unidade on ci1.id = compra_item_unidade.compra_item_id
             where ci1.id = compra_items.id
               and compra_item_unidade.tipo_uasg in ('G','P')
               and compra_item_unidade.unidade_id = {$idUnidadeUsuarioLogado}
               and compra_item_unidade.situacao = true) = 0";
        $itens->havingRaw($queryQuantidadeAprovadaFiltro);
        
        # Filtro se o fornecedor for selecionado
        if (!is_null($fornecedorId)) {
            $itens->where("compra_item_fornecedor.fornecedor_id", $fornecedorId);
        }
        
        # Filtro se a unidade de origem for selecionada
        if (!is_null($unidadeOrigemId)) {
            $itens->where('arp.unidade_origem_id', $unidadeOrigemId);
        }
        
        # Filtro se o número da ata for selecionado
        if (!is_null($numeroAta)) {
            $numeroAta = str_replace("-", "/", $numeroAta);
            $itens->where("arp.id", $numeroAta);
        }
        
        
        if (!is_null($itensAtaFiltro)) {
            $itens->where(function ($query) use ($itensAtaFiltro) {
                $query->Where("arp_item.id", $itensAtaFiltro);
            });
        }
        
        # Filtro se a modalidade da compra for selecionada
        if (!is_null($modadelidadeId)) {
            $itens->where("compras.modalidade_id", $modadelidadeId);
        }
        
        # Filtro se a compra for selecionada
        if (!is_null($numeroCompra)) {
            $itens->where("compras.id", $numeroCompra);
        }
        
        if (!$ataEnfrentandoImpactoDecorrenteCalamidadePublica) {
            $itens->where("compras.lei", "<>", "MP1221");
        }

        if ($ataEnfrentandoImpactoDecorrenteCalamidadePublica) {
            $itens->where("compras.lei", "MP1221");
        }
        
        $itens = $itens->whereNotExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('arp_item_historico')
                ->join('arp_historico', 'arp_historico.id', '=', 'arp_item_historico.arp_historico_id')
                ->join('arp_alteracao', function ($query) {
                    $query->on('arp_historico.arp_alteracao_id', 'arp_alteracao.id')
                        ->where('arp_alteracao.rascunho', false);
                })
                ->whereRaw('arp_item_historico.arp_item_id = arp_item.id')
                ->where(function ($query) {
                    $query->where('arp_item_historico.item_cancelado', true)
                        ->orWhere('arp_item_historico.item_removido_retificacao', true);
                });
        });
        
        # Campos do select
        $itens = $itens->select(
            "arp_item.id",
            "compras.numero_ano as numerocompras",
            "compra_items.descricaodetalhada",
            "compra_items.catmatseritem_id AS codigo_item",
            DB::raw("CASE 
                    WHEN compra_item_fornecedor.percentual_maior_desconto IS NULL 
                         OR compra_item_fornecedor.percentual_maior_desconto = 0 
                    THEN compra_item_fornecedor.valor_unitario::text
                    ELSE TO_CHAR(compra_item_fornecedor.valor_unitario *
                        ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100), 'FM999999999.0000')
                END AS valorunitario"),
            "compra_items.compra_id AS compras_ids",
            "compra_items.numero",
            "compras.unidade_subrrogada_id",
            //            DB::raw("STRING_AGG(compra_item_unidade.unidade_id::varchar,',') AS codigouasg"),
            DB::raw("CONCAT(arp.numero,'/',arp.ano) as numeroata"),
            DB::raw("CONCAT(compra_items.catmatseritem_id,' - ',compra_items.descricaodetalhada) as catmat_descricao"),
            DB::raw("CONCAT(unidades.codigo,' - ',unidades.nomeresumido) as unidadeadesao"),
            DB::raw("
                        CONCAT(
                            to_char(compra_item_fornecedor.ata_vigencia_inicio, 'DD/MM/YYYY'), ' - ',
                            to_char(compra_item_fornecedor.ata_vigencia_fim, 'DD/MM/YYYY')
                            ) as vigencia"),
            DB::raw("CASE WHEN compra_items.grupo_compra IS NULL OR
             compra_items.grupo_compra = '00000' OR
              compra_items.grupo_compra = '' THEN
               '-' ELSE compra_items.grupo_compra END as grupo_compra_tabela"),
            "compra_items.maximo_adesao as maximo_adesao_inicial_item",
            "cgi.descres as descresitem",
            "compra_item_fornecedor.quantidade_homologada_vencedor",
            "codigoitens.descricao as modalidadeadesao",
            "compra_items.grupo_compra as grupo_compra",
            DB::raw(
                "CONCAT(fornecedores.cpf_cnpj_idgener, ' - ',fornecedores.nome,
                CASE
                    WHEN compra_item_fornecedor.classificacao <> ''
                    THEN CONCAT(' (', compra_item_fornecedor.classificacao, ')') ELSE '' END) as nomefornecedor"
            ),
            "compra_item_fornecedor.id as compraitemfornecedorid",
            "compras.id as compraid",
            "compra_item_fornecedor.compra_item_id",
            "compra_item_fornecedor.fornecedor_id",
            "unidades.codigo as codigo_unidade_gerenciadora",
            'codigoitens.descres',
            'cgi.descres as descresitem',
            "unidades.codigo as codigo_unidade_gerenciadora",
            "compras.lei",
            "compra_item_fornecedor.id as compra_item_fornecedor_id",
            "compra_items.qtd_total"
        )
            ->groupby(
                "compra_items.numero",
                "compras.lei",
                "compra_items.descricaodetalhada",
                "compra_items.catmatseritem_id",
                "compra_item_fornecedor.valor_unitario",
                "cgi.descres",
                "compras.unidade_subrrogada_id",
                "arp.numero",
                "arp.ano",
                "unidades.codigo",
                "unidades.nomeresumido",
                "codigoitens.descricao",
                "compra_items.ata_vigencia_inicio",
                "compra_items.ata_vigencia_fim",
                "compra_items.maximo_adesao",
                "compras.numero_ano",
                "fornecedores.cpf_cnpj_idgener",
                "fornecedores.nome",
                "compra_items.id",
                "compra_item_fornecedor.id",
                "compras.id",
                "compra_item_fornecedor.quantidade_homologada_vencedor",
                "compra_items.grupo_compra",
                "compra_item_fornecedor.compra_item_id",
                "arp_item.id",
                "codigoitens.descres"
            )
            ->orderBy("numeroata", "desc");
        
        return $itens;
    }

    /**
     * Método responsável em buscar os itens para que o usuário seleciona para adesão
     */
    public function recuperarItemSolicitarAdesao(
        ?int $modadelidadeId = null,
        ?int $unidadeOrigemId = null,
        ?string $numeroCompra = null,
        ?int $fornecedorId = null,
        ?string $numeroAta = null,
        ?string $itensAtaFiltro = null,
        bool $execucaoDescentralizadaProgramaProjetoFederal = null,
        bool $aquisicaoEmergencialMedicamentoMaterial = null,
        ?int $idSolicitacao = null,
        int $idUnidadeUsuarioLogado = null,
        bool $ataEnfrentandoImpactoDecorrenteCalamidadePublica = false
    ) {
        # Join e condicionais para recuperar o item
        return $this->recuperaItem(
            $modadelidadeId,
            $unidadeOrigemId,
            $numeroCompra,
            $fornecedorId,
            $numeroAta,
            $itensAtaFiltro,
            $execucaoDescentralizadaProgramaProjetoFederal,
            $aquisicaoEmergencialMedicamentoMaterial,
            $idSolicitacao,
            $idUnidadeUsuarioLogado,
            $ataEnfrentandoImpactoDecorrenteCalamidadePublica
        );
//        ->orderBy("numeroata", "desc")
//         ->dd()
//        ->get();
//        ->toArray();

//        return $itens;
    }

    /**
     * Método responsável em gerar o sequencial para a adesão
     */
    public function gerarSequencialArp(array $dadosForm)
    {
        $sequencialSolicitacao = $this->where("unidade_origem_id", $dadosForm['unidade_origem_id'])
            ->where("ano", $dadosForm['ano'])
            ->where("rascunho", $dadosForm['rascunho'])
            ->max('sequencial');

        if (empty($sequencialSolicitacao)) {
            return 1;
        }

        return $sequencialSolicitacao + 1;
    }
    
    public function getAdesaoUnico(int $idAdesao)
    {
        return $this->find($idAdesao);
    }
    
    public function queryBaseTotalQuantidadeAprovada(int $compraItemId, int $fornecedorId)
    {
        return $this->queryBaseRecuperarItemAceitoAdesao()
            ->where('cif1.compra_item_id', $compraItemId)
            ->where('cif1.fornecedor_id', $fornecedorId);
    }

    public function queryBaseRecuperarItemAceitoAdesao()
    {
        return $this->join('arp_solicitacao_item', function ($query) {
            $query->on('arp_solicitacao.id', 'arp_solicitacao_item.arp_solicitacao_id')
                ->where('arp_solicitacao.rascunho', false)
                ->whereNull('arp_solicitacao.deleted_at');
        })
            ->join('codigoitens', function ($query) {
                $query->on('arp_solicitacao_item.status_id', 'codigoitens.id')
                    ->whereIn('codigoitens.descricao', ['Aceitar', 'Aceitar Parcialmente']);
            })
            ->join('codigoitens as ciAdesao', function ($query) {
                $query->on('arp_solicitacao.status', 'ciAdesao.id')
                    ->whereIn('ciAdesao.descricao', ['Aceita', 'Aceito Parcial']);
            })
            ->join('codigos', function ($query) {
                $query->on('codigoitens.codigo_id', 'codigos.id')
                    ->where('codigos.descricao', 'Tipo de Ata de Registro de Preços');
            })
            ->join('arp_item', 'arp_item.id', '=', 'arp_solicitacao_item.item_arp_fornecedor_id')
            ->join('compra_item_fornecedor as cif1', function ($join) {
                $join->on('arp_item.compra_item_fornecedor_id', '=', 'cif1.id')
                    ->where('cif1.situacao', true);
            });
    }

    public function queryBaseTotalQuantidadeAprovadaPorItemCompra(int $compraItemId)
    {
        return $this->queryBaseRecuperarItemAceitoAdesao()
            ->where('cif1.compra_item_id', $compraItemId);
    }
}
