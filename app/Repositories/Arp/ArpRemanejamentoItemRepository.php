<?php

namespace App\Repositories\Arp;

use App\Models\ArpRemanejamentoItem;
use Illuminate\Support\Facades\DB;

class ArpRemanejamentoItemRepository extends ArpRemanejamentoItem
{
    /**
     * Método responsável em inserir os registros na tabela arp_remanejamento_itens
    */
    public function insertRemanejamentoItem(array $dados)
    {
        return $this->create($dados)->id;
    }

    /**
     * Buscar o(s) item(ns) a partir de um remanejamento criado
     */
    public function getItemPorRemanejamento(int $idRemanejamento)
    {
        return $this->where("remanejamento_id", $idRemanejamento)->get();
    }

    /**
     * Método responsável em atualizar ou inserir os valores dos itens
     */
    public function updateRemanejamentoItem(array $condicional, ?array $campos)
    {
        # Se não enviar os campos opcionais, então atualiza as informações das colunas do condicional
        if (empty($campos)) {
            return $this->updateOrCreate($condicional);
        }

        return $this->updateOrCreate($condicional, $campos);
    }

    /**
     * Método responsável em contar todos os status dos itens no remanejamento
     */
    public function getStatusItensRemanejamento(
        int $idRemanejamento,
        string $status,
        int $idUnidadeAnalise,
        bool $gerenciadoraAta = false
    ) {
        $itens = $this->whereHas('situacaoItem', function ($query) use ($status, $idRemanejamento) {
            $query->where("codigoitens.descres", "status_remanejamento")
                ->where("codigoitens.descricao", $status)
                ->where("arp_remanejamento_itens.remanejamento_id", $idRemanejamento);
        });
        if (!$gerenciadoraAta) {
            $itens = $itens->whereHas('itemUnidade', function ($query) use ($idUnidadeAnalise) {
                $query->where("compra_item_unidade.unidade_id", $idUnidadeAnalise);
            });
        }
//                join("codigoitens", "codigoitens.id", "arp_remanejamento_itens.situacao_id")
//                ->where("codigoitens.descres", "status_remanejamento")
//                ->where("codigoitens.descricao", $status)
//                ->where("arp_remanejamento_itens.remanejamento_id", $idRemanejamento);

        # Exibir somente os itens que não foram negados
        if ($gerenciadoraAta) {
            $itens->leftjoin("codigoitens as c1", "c1.id", "arp_remanejamento_itens.status_analise_id")
                ->where(function ($query) {
                    $query->whereNull("arp_remanejamento_itens.data_aprovacao_gerenciadora")
                        ->orWhere("c1.descricao", "<>", "Negar");
                });
        }


        return $itens->select("arp_remanejamento_itens.*")->get();
    }

    /**
     * Método responsável em retornar o item do remanejamento único
    */
    public function getRemanejamentoItemUnico(int $idRemanejamentoItem)
    {
        return $this->find($idRemanejamentoItem);
    }

    /**
     * Método responsável em retornar todos os itens por status
     */
    public function getItemRemanejamentoPorStatusAprovado(int $idRemanejamento, int $idUnidadeUsuarioAnalise)
    {
        return $this->join('codigoitens', 'codigoitens.id', 'arp_remanejamento_itens.status_analise_id')
                ->join('compra_item_unidade', 'compra_item_unidade.id', 'arp_remanejamento_itens.id_item_unidade')
                ->where('compra_item_unidade.unidade_id', $idUnidadeUsuarioAnalise)
                ->where('remanejamento_id', $idRemanejamento)
                ->whereNotNull('arp_remanejamento_itens.id_usuario_aprovacao_gerenciadora')
                ->select(DB::raw('codigoitens.descricao, count(codigoitens.descricao) as quantidade'))
                ->groupBy('codigoitens.descricao')
                ->get()
                ->pluck('quantidade', 'descricao')
                ->toArray();
    }
    
    /**
     * @param int $idUnidade
     * @return mixed
     */
    public function getItemRemanejamentoAprovadoPorUnidade(int $idUnidade, int $idCompraItemUnidade)
    {
        return $this->whereHas('itemUnidade', function ($query) use ($idUnidade, $idCompraItemUnidade) {
            $query->where("compra_item_unidade.unidade_id", $idUnidade)
                ->where("compra_item_unidade.id", $idCompraItemUnidade)
                ->whereNotNull("id_usuario_aprovacao_gerenciadora");
        })->first();
    }
    
    public function getItemRemanejamentoNegado(int $idRemanejamento)
    {
        $totalItemNegado = $this->whereHas('situacaoAnaliseItem', function ($query) {
            $query->where('descricao', 'Negar');
        })
        ->where("remanejamento_id", $idRemanejamento)
        ->count();
        
        $totalItensRemanejamento = count($this->getItemPorRemanejamento($idRemanejamento));
        
        return [
            'totalItemNegado' => $totalItemNegado,
            'totalItensRemanejamento' => $totalItensRemanejamento
        ];
    }
    
    public function getContagemItemRemanejamentoPorStatus(int $idRemanejamento, string $situacaoAnalise)
    {
        $itemPorStatus = $this->leftjoin('codigoitens', 'codigoitens.id', 'arp_remanejamento_itens.status_analise_id')
            ->join('codigoitens as c1', 'c1.id', 'arp_remanejamento_itens.situacao_id')
            ->where("remanejamento_id", $idRemanejamento)
            ->where('c1.descricao', $situacaoAnalise)
            ->selectRaw("codigoitens.descricao, COUNT(*) as quantidade")
            ->groupBy('codigoitens.descricao')
            ->get()
            ->toArray();

        $itemAguardandoAnaliseUnidadeParticipante =
            array_values(array_filter($itemPorStatus, function ($value) {
                if (empty($value['descricao'])) {
                    return $value;
                }
                return null;
            }));
        
        $quantidadeItemFaltandoAnalisar = $itemAguardandoAnaliseUnidadeParticipante[0]['quantidade'] ?? 0;
        
        $itemAceito =
            array_values(array_filter($itemPorStatus, function ($value) {
                if ($value['descricao'] === 'Item Aceito') {
                    return $value;
                }
                return null;
            }));
        
        $quantidadeAceito = $itemAceito[0]['quantidade'] ?? 0;
        
        $itemNegar =
            array_values(array_filter($itemPorStatus, function ($value) {
                if ($value['descricao'] === 'Negar') {
                    return $value;
                }
                return null;
            }));

        $quantidadeNegar = $itemNegar[0]['quantidade'] ?? 0;
        
        $itemAceitoParcialmente =
            array_values(array_filter($itemPorStatus, function ($value) {
                if ($value['descricao'] === 'Aceitar Parcialmente') {
                    return $value;
                }
                return null;
            }));

        $quantidadeAceitoParcial = $itemAceitoParcialmente[0]['quantidade'] ?? 0;
        
        return [
            'quantidadeItemFaltandoAnalisar' => $quantidadeItemFaltandoAnalisar,
            'quantidadeAceito' => $quantidadeAceito,
            'quantidadeNegar' => $quantidadeNegar,
            'quantidadeAceitoParcial' => $quantidadeAceitoParcial,
        ];
    }

    public function getItemRemanejamentoPorUnidade(int $idRemanejamento, int $idItemUnidade)
    {
        return $this->where('remanejamento_id', $idRemanejamento)
            ->where('id_item_unidade', $idItemUnidade)
            ->first();
    }

    public function getItemRemanejamentoPorUnidadeCarona(
        int $idRemanejamento,
        int $idItemUnidadeCeder,
        int $idItemUnidadeReceber
    ) {
        return $this->where('remanejamento_id', $idRemanejamento)
            ->where('id_item_unidade', $idItemUnidadeCeder)
            ->where('id_item_unidade_receber', $idItemUnidadeReceber)
            ->first();
    }
}
