<?php

namespace App\Repositories\Arp;

use App\Models\ArpRemanejamento;

class ArpRemanejamentoRepository extends ArpRemanejamento
{
    /**
     * Método responsável em retornar o Item da Ata a partir do fornecedor
     */
    public function getSequencial(bool $rascunho, int $idUnidadeSolicitante)
    {
        $sequencialAtual = $this->where("rascunho", $rascunho)
            ->where("unidade_solicitante_id", $idUnidadeSolicitante)
            ->whereRaw("date_part('year', created_at) = date_part('year', CURRENT_DATE)")
            ->max('sequencial');

        # Se ainda não tiver sequencial, retorna como o primeiro
        if (empty($sequencialAtual)) {
            return 1;
        }

        # Se encontrar, incrementa com o atual
        return $sequencialAtual + 1;
    }

    /**
     * Método responsável em inserir os registros na tabela arp_remanejamento
     */
    public function insertRemanejamento(array $dados)
    {
        return $this->create($dados);
    }

    /**
     * Método responsável em retornar o registro do remanejamento
     */
    public function getRemanejamentoUnico(int $id)
    {
        return $this->find($id);
    }

    /**
     * Método responsável em atualizar o status do remanejamento
     */
    public function updateRemanejamento(int $idRemanejamento, array $campos)
    {
        return $this->where("id", $idRemanejamento)->update($campos);
    }

    /**
     * Método responsável em deletar o registro fisicamente no banco de dados
     */
    public function deleteRemanejamento(int $idRemanejamento)
    {
        return $this->find($idRemanejamento)->forceDelete();
    }
    
    public function quantidadeRemanejamentoAceitoPorAdesaoQuantitativo(int $idSolicitacaoAdesao)
    {
        return $this->join('arp_remanejamento_itens as ari', 'arp_remanejamento.id', 'ari.remanejamento_id')
            ->join('codigoitens as ci', function ($join) {
                $join->on('ci.id', 'arp_remanejamento.situacao_id')
                    ->where('ci.descricao', 'Analisado pela unidade gerenciadora da ata')
                    ->where('ci.descres', 'status_remanejamento')
                    ->whereNotNull('id_usuario_aprovacao_gerenciadora');
            })
            ->join('codigoitens as ciItemRemanejamento', 'ciItemRemanejamento.id', 'ari.situacao_id')
            ->join('codigos', function ($join) {
                $join->on('ciItemRemanejamento.codigo_id', 'codigos.id')
                ->where('codigos.descricao', 'Tipo de Ata de Registro de Preços');
            })
            ->join('arp', 'arp_remanejamento.arp_id', 'arp.id')
            ->join('codigoitens as ciAta', function ($join) {
                $join->on('ciAta.id', 'arp.tipo_id')->where('ciAta.descricao', '=', 'Ata de Registro de Preços');
            })
            ->join('arp_item', 'arp.id', '=', 'arp_item.arp_id')
            ->join('arp_solicitacao_item', 'arp_item.id', '=', 'arp_solicitacao_item.item_arp_fornecedor_id')
            ->join('codigoitens as ciItemAdesao', function ($join) {
                $join->on('ciItemAdesao.id', '=', 'arp_solicitacao_item.status_id')
                ->whereIn('ciItemAdesao.descricao', ['Aceitar', 'Aceitar Parcialmente']);
            })
            ->where('arp_solicitacao_item.arp_solicitacao_id', $idSolicitacaoAdesao)
            ->count();
    }

    public function getItemRemanejamentoPorCompraItemFornecedorId(int $compraItemFornecedorId)
    {
        return ArpRemanejamento::join(
            'arp_remanejamento_itens',
            'arp_remanejamento.id',
            'arp_remanejamento_itens.remanejamento_id'
        )
            ->join('compra_item_unidade', 'compra_item_unidade.id', 'arp_remanejamento_itens.id_item_unidade')
            ->join('compra_items', 'compra_item_unidade.compra_item_id', 'compra_items.id')
            ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', 'compra_items.id')
            ->join('codigoitens', 'arp_remanejamento.situacao_id', '=', 'codigoitens.id')
            ->join(
                'codigoitens as ciItemRemanejamento',
                'ciItemRemanejamento.id',
                'arp_remanejamento_itens.situacao_id'
            )
            ->whereIn(
                'codigoitens.descricao',
                [
                    'Analisado pela unidade gerenciadora da ata',
                    'Aguardando aceitação da unidade gerenciadora',
                    'Aguardando aceitação da unidade participante',
                    'Aguardando aceitação'
                ]
            )
            ->where('compra_item_fornecedor.id', $compraItemFornecedorId)
            ->select('arp_remanejamento.*')
            ->get();
    }
}
