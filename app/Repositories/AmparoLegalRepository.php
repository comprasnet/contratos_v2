<?php

namespace App\Repositories;

use App\Models\AmparoLegal;
use Illuminate\Support\Facades\DB;

class AmparoLegalRepository
{
    private $model = AmparoLegal::class;

    public function query()
    {
        return $this->model::query();
    }

    public function getPaginateByAtoNormativo(string $ato_normativo)
    {
        $options = $this->query()->select([
            DB::raw("amparo_legal.ato_normativo as ato_normativo")
        ])->groupby(
            'amparo_legal.ato_normativo'
        );

        if ($ato_normativo) {
            $options = $options->where(function ($query) use ($ato_normativo) {
                $query->where('amparo_legal.ato_normativo', 'ilike', '%' . strtoupper($ato_normativo) . '%');
            })->where('codigo', '<>', '');
        }

        return $options->paginate(10);
    }

    public function retornaOpcoesFiltroAmaparoLegalContratoRepository(string $values)
    {
        return AmparoLegal::select('amparo_legal_contrato.contrato_id')
            ->join('amparo_legal_contrato', 'amparo_legal_contrato.amparo_legal_id', 'amparo_legal.id')
            ->join('contratos', 'contratos.id', 'amparo_legal_contrato.contrato_id')
            ->where('amparo_legal.ato_normativo', 'ilike', '%' . strtoupper($values) . '%')
            ->groupby('amparo_legal_contrato.contrato_id')
            ->get()->toArray();
    }
}
