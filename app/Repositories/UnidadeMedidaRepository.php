<?php

namespace App\Repositories;

use App\Models\Unidade;
use App\Models\UnidadeMedida;

class UnidadeMedidaRepository
{
    const TYPE_MATERIAL = 'material';
    const TYPE_SERVICO = 'servico';

    public static function getmoveByTypeCode($type, $code)
    {
        $query = UnidadeMedida::query()
            ->where('tipo', '=', $type);

        if ($type == self::TYPE_MATERIAL) {
            $query->where('codigo_pdm', $code);
        }
        if ($type == self::TYPE_SERVICO) {
            $query->where('codigo_siasg', $code);
        }

        return $query->get();
    }

    public static function createByCode($type, $code, $itens = [])
    {
        if (count($itens) > 0) {
            foreach ($itens as $item) {
                $sigla = $item->siglaUnidadeMedida;
                $nome = $item->nomeUnidadeMedida;


                if ($type == self::TYPE_MATERIAL) {
                    $sigla = $item->siglaUnidadeFornecimento;
                    $nome = $item->nomeUnidadeFornecimento;
                }

                $data = [
                    'tipo' => $type,
                    'sigla' => $sigla,
                    'nome' => $nome,
                ];
                if ($type == self::TYPE_SERVICO) {
                    $data['codigo_siasg'] = $code;
                }
                if ($type == self::TYPE_MATERIAL) {
                    $data['codigo_pdm'] = $code;
                }
                $unidadeMedidaExsists = self::getByCodeMaterialOrService($code, $type);

                if (is_null($unidadeMedidaExsists)) {
                    UnidadeMedida::create($data);
                }
            }
        }
    }

    private static function getByCodeMaterialOrService($code, $type)
    {
        $query = UnidadeMedida::query();
        if ($type === self::TYPE_MATERIAL) {
            return $query->where('codigo_pdm', $code)->first();
        }
        return $query->where('codigo_siasg', $code)->first();
    }

    public static function getByTypeAndCode($type, $code)
    {
        $query = UnidadeMedida::where('tipo', $type);
        if ($type == self::TYPE_MATERIAL) {
            $query->where('codigo_pdm', $code);
        }
        if ($type == self::TYPE_SERVICO) {
            $query->where('codigo_siasg', $code);
        }
        $arrUnidades = $query->get()->toArray();
        $arrData = [];
        foreach ($arrUnidades as $item) {
            $arrData[$item['sigla']] = $item;
        }

        return array_values($arrData);
    }
}
