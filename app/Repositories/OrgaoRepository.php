<?php

namespace App\Repositories;

use App\Models\Orgao;

class OrgaoRepository extends Orgao
{

    /**
     * Método responsável em verificar se a unidade pertence ao orgão
     */
    public function unidadePertenceOrgao(string $codigoOrgao, int $idUnidade)
    {
        $pertenceAoOrgao = $this->join('unidades', 'orgaos.id', "=", "unidades.orgao_id")
        ->where("orgaos.codigo", $codigoOrgao)
        ->where("unidades.id", $idUnidade)
        ->count();

        # Se a unidade pertencer, então retorna verdadeiro
        if ($pertenceAoOrgao >0) {
            return true;
        }

        return false;
    }
    
    public function getOrgaoPorCodigo(string $codigo)
    {
        return $this->where("codigo", $codigo)
            ->where("situacao", true)
            ->first();
    }
    
    public function salvarAtualizarOrgao(array $dados)
    {
        return $this->updateOrCreate($dados);
    }
}
