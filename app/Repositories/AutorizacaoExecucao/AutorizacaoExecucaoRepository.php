<?php

namespace App\Repositories\AutorizacaoExecucao;

use App\Http\Traits\ArquivoGenericoTrait;
use App\Http\Traits\DateFormat;
use App\Http\Traits\Formatador;
use App\Http\Traits\NumeroTrait;
use App\Models\AutorizacaoExecucao;
use App\Models\CodigoItem;
use App\Models\Contrato;
use App\Models\Contratohistorico;
use App\Models\Contratoitem;
use App\Models\Saldohistoricoitem;
use App\Repositories\ArquivoGenerico\ArquivoGenericoRepository;
use App\Services\ContratoItemService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class AutorizacaoExecucaoRepository
{
    use DateFormat;
    use ArquivoGenericoTrait;
    use Formatador;
    use NumeroTrait;

    public function getContratoItensByHistoricos($historicos)
    {
        $historicosItens = [];
        foreach ($historicos as $id => $historico) {
            $historicosItens[$id] = $this->getContratoItemSaldoHistorico($id);
        }
        return $historicosItens;
    }

    public function getContratoItemSaldoHistorico(
        int $contratohistoricoId,
        $vigienciaFim = null
    ) {

        $query = Saldohistoricoitem::select('saldohistoricoitens.*')
            ->where('saldohistoricoitens.id', $contratohistoricoId);

        $result = $query->first();
        $unidadesMedida = ContratoItemService::getUnidades($result->contratoItem);

        $item = $result->contratoItem->getCatmatseritem(true, true);

        $numeroParcelas = $result->contratoItem->contrato->num_parcelas;

        // se vigência fim for indeterminada
        if ($result->contratoItem->contrato->vigencia_fim == null) {
            if (!$vigienciaFim) {
                $vigienciaFim = Carbon::now();
            } else {
                $vigienciaFim = Carbon::createFromFormat('d/m/Y', $vigienciaFim);
            }

            $inicioExercicio = Carbon::createFromFormat(
                'Y-m-d',
                $result->contratoItem->contrato->vigencia_inicio
            );
            $fimExercicio = Carbon::create($inicioExercicio->year, 12, 31);

            // calcula o número de parcelas até o fim do exercicio
            $numeroParcelas = $fimExercicio->diffInMonths($inicioExercicio);
            // adiciona 12 parcelas para cada ano subsequente ao fim do exercicio
            $numeroParcelas += (12 * abs($vigienciaFim->year - $inicioExercicio->year));
        }

        return [
            'tipo' => $result->contratoItem->getTipo(),
            'contratoitem_id' => $result->contratoItem->id,
            'descricao' => $result->contratoItem->descricao_complementar ??
                $result->contratoItem->getCatmatseritem(true, false),
            'catmatser_item' => $item,
//            'quantidade' => $result->quantidade * $numeroParcelas,
            'quantidade_item' => $result->quantidade,
            'numero_parcelas' => $numeroParcelas,
            'valor_unitario' => $result->valorunitario,
            'numero_item_compra' => $result->numero_item_compra,
            'unidades_medida' => $unidadesMedida
        ];
    }

    public function getSaldoHistoricoItemByPeriodo(
        int $itemId,
        string $vigenciaInicio,
        string $vigenciaFim
    ): array {
        $contratoId = Contratoitem::find($itemId)->contrato_id;

        // agrupa os periodos de vigência do contrato pelo seu histórico
        $periodosContrato = Contratohistorico::select('vigencia_fim')
            ->selectRaw('MIN(vigencia_inicio) AS vigencia_inicio')
            ->selectRaw('MIN(data_inicio_novo_valor) AS vigencia_inicio_novo_valor')
            ->where('contrato_id', $contratoId)
            ->groupBy('vigencia_fim')
            ->get('vigencia_fim', 'vigencia_inicio', 'vigiencia_inicio_novo_valor');

        $periodoVigenciaDe = null;
        $periodoVigenciaAte = null;

        // pega a data de inicio/fim de todas as vigencias
        foreach ($periodosContrato as $key => $periodo) {
            if ($vigenciaInicio >= $periodo['vigencia_fim']) {
                $periodoVigenciaDe = $periodosContrato[$key + 1]['vigencia_fim'];
            }
            if ($vigenciaFim <= $periodo['vigencia_fim']) {
                $periodoVigenciaAte = $periodo['vigencia_fim'];
                break;
            }
        }

        // filtra os períodos baseado nas datas de inicio/fim
        $periodosSelecionados = $periodosContrato->filter(
            function ($item) use ($periodoVigenciaDe, $periodoVigenciaAte) {
                return $item['vigencia_fim'] >= $periodoVigenciaDe && $item['vigencia_fim'] <= $periodoVigenciaAte;
            }
        );

        $saldoHistoricoItem = [];
        $periodosSelecionados->each(function ($item) use ($contratoId, $itemId, &$saldoHistoricoItem) {

            $saldoItem = Saldohistoricoitem::join('contratohistorico', function ($join) use ($contratoId, $item) {
                $join->on('contratohistorico.id', 'saldohistoricoitens.saldoable_id')
                    ->where('saldohistoricoitens.saldoable_type', Contratohistorico::class)
                    ->where('contratohistorico.contrato_id', $contratoId)
                    ->where('contratohistorico.vigencia_fim', $item['vigencia_fim']);
            })
            ->where('contratoitem_id', $itemId)
            ->orderBy('saldohistoricoitens.id')
            ->get(['contratohistorico.*', 'saldohistoricoitens.*']);

            $saldoItemOrigem = $saldoItem->first();
            $saldoItemAtualizado = $saldoItem->last();
            $unidadesMedida = ContratoItemService::getUnidades($saldoItemOrigem->contratoItem);

            $numeroParcelas = $saldoItemAtualizado->periodicidade ?? 1;

            $vigenciaInicio = $item['vigencia_inicio_novo_valor'] ?? $item['vigencia_inicio'];
            $periodoFormatado = "De: " . $this->retornaDataAPartirDeCampo($vigenciaInicio) .
                " Até: " . $this->retornaDataAPartirDeCampo($item['vigencia_fim']);

            $saldoHistoricoItem[] = [
                'periodo' => $periodoFormatado,
                'periodo_vigencia_inicio' => $vigenciaInicio,
                'periodo_vigencia_fim' => $item['vigencia_fim'],
                'contratohistorico_id' => $saldoItemAtualizado->saldoable_id,
                'saldohistoricoitens_id' => $saldoItemOrigem->id,
                'tipo' => $saldoItemOrigem->contratoItem->getTipo(),
                'descricao' => $saldoItemOrigem->contratoItem->descricao_complementar ??
                    $saldoItemOrigem->contratoItem->getCatmatseritem(true, false),
                'catmatser_item' => $saldoItemOrigem->contratoItem->getCatmatseritem(true, true),
                'quantidade_item' => $saldoItemAtualizado->quantidade, // atualizados
                'numero_parcelas' => $numeroParcelas, // atualiuzados
                'valor_unitario' => $saldoItemAtualizado->valorunitario, // atualizados
                'numero_item_compra' => $saldoItemOrigem->numero_item_compra,
                'unidades_medida' => $unidadesMedida
            ];
        });

        return $saldoHistoricoItem;
    }

    public function getSaldoHistoricoItem(
        int $contratoHistoricoId,
        int $saldohistoricoitensId
    ): array {
        // 1 - saldo historico item origem - saldohistoricoitens_id
        $saldoItemOrigem = Saldohistoricoitem::find($saldohistoricoitensId);
        $unidadesMedida = ContratoItemService::getUnidades($saldoItemOrigem->contratoItem);

        // 2 - saldo historico item atualizado - contratohistorico_id + contratoitem_id
        $saldoItemAtualizado = Saldohistoricoitem::where('saldoable_type', Contratohistorico::class)
            ->where('saldoable_id', $contratoHistoricoId)
            ->where('contratoitem_id', $saldoItemOrigem->contratoitem_id)
            ->orderBy('id')
            ->first();

        $numeroParcelas = $saldoItemAtualizado->periodicidade ?? 1;

        // 3 - período de vigência - contratohistorico_id -> vigencia_fim + contrato_id -> first()
        $contratoHistoricoFim = Contratohistorico::find($contratoHistoricoId); // vigencia_fim
        $contratoHistoricoInicio = Contratohistorico::where('contrato_id', $contratoHistoricoFim->contrato_id)
            ->where('vigencia_fim', $contratoHistoricoFim->vigencia_fim)
            ->first(['data_inicio_novo_valor', 'vigencia_inicio']); // vigencia_inicio

        $vigenciaInicio = $contratoHistoricoInicio->data_inicio_novo_valor ?? $contratoHistoricoInicio->vigencia_inicio;
        $periodo = "De: " . $this->retornaDataAPartirDeCampo($vigenciaInicio) .
            " Até: " . $this->retornaDataAPartirDeCampo($contratoHistoricoFim->vigencia_fim);

        return [
            "periodo" => $periodo,
            "contratohistorico_id" => $saldoItemAtualizado->saldoable_id,
            'saldohistoricoitens_id' => $saldoItemOrigem->id,
            'tipo' => $saldoItemOrigem->contratoItem->getTipo(),
            "descricao" => $saldoItemOrigem->contratoItem->descricao_complementar ??
                $saldoItemOrigem->contratoItem->getCatmatseritem(true, false),
            "catmatser_item" => $saldoItemOrigem->contratoItem->getCatmatseritem(true, true),
            "quantidade_item" => $saldoItemAtualizado->quantidade,
            "numero_parcelas" => $numeroParcelas,
            "valor_unitario" => $saldoItemAtualizado->valorunitario,
            'numero_item_compra' => $saldoItemOrigem->numero_item_compra,
            "unidades_medida" => $unidadesMedida
        ];
    }

    public function getUsedSaldoByItemBySaldoHistoricoItemId($id, $autorizacaoExecucaoId = null)
    {
        $situacaoIds = CodigoItem::whereIn('descres', ['ae_status_2', 'ae_status_4', 'ae_status_5'])->pluck('id');

        return AutorizacaoExecucao::select(
            'ai.saldohistoricoitens_id',
            'autorizacaoexecucoes.data_vigencia_inicio',
            'autorizacaoexecucoes.data_vigencia_fim'
        )
            ->selectRaw('STRING_AGG(ai.id::VARCHAR, \',\') as ids')
            ->selectRaw('SUM(ai.quantidade * ai.parcela) as quantidade_total')
            ->selectRaw('si.valorunitario as valor')
            ->selectRaw('coditens.descres as tipo')
            ->selectRaw('(si.valorunitario * SUM(ai.quantidade * ai.parcela)) as total')
            ->join('autorizacaoexecucao_itens as ai', 'ai.autorizacaoexecucoes_id', '=', 'autorizacaoexecucoes.id')
            ->join('contratohistorico as ci', 'ci.id', '=', 'ai.contratohistorico_id')
            ->join('saldohistoricoitens as si', 'si.id', '=', 'ai.saldohistoricoitens_id')
            ->join('contratoitens as citens', 'citens.id', '=', 'si.contratoitem_id')
            ->join('codigoitens as coditens', 'coditens.id', '=', 'citens.tipo_id')
            ->whereIn('situacao_id', $situacaoIds)
            ->whereNull('autorizacaoexecucoes.deleted_at')
            ->whereNull('ai.deleted_at')
            ->where('ci.contrato_id', $id)
            ->where('autorizacaoexecucoes.id', '<>', $autorizacaoExecucaoId)
            ->groupBy('ai.saldohistoricoitens_id', 'si.valorunitario', 'autorizacaoexecucoes.id', 'coditens.id')
            ->get();
    }

    public function create(Contrato $contrato, array $data): AutorizacaoExecucao
    {
        $descres = $data['rascunho'] ? 'ae_status_1' : 'ae_status_2';
        if (isset($data['enviar_assinatura']) && $data['enviar_assinatura']) {
            $descres = 'ae_status_5';
        }

        $situacao = CodigoItem::where('descres', $descres)->first();

        return AutorizacaoExecucao::create(
            array_merge(
                [
                    'contrato_id' => $contrato->id,
                    'fornecedor_id' => $contrato->fornecedor_id,
                    'situacao_id' => $situacao->id,
                ],
                $this->dataTransform($data)
            )
        );
    }

    public function update(AutorizacaoExecucao $autorizacaoExecucao, array $data): bool
    {
        $descres = $data['rascunho'] ? 'ae_status_1' : 'ae_status_2';
        if (isset($data['enviar_assinatura']) && $data['enviar_assinatura']) {
            $descres = 'ae_status_5';
        } elseif (!in_array($autorizacaoExecucao->situacao->descres, ['ae_status_1', 'ae_status_2', 'ae_status_5'])) {
            $descres = $autorizacaoExecucao->situacao->descres;
        }

        $situacao = CodigoItem::where('descres', $descres)->first();

        return $autorizacaoExecucao->update(
            array_merge(
                [
                    'situacao_id' => $situacao->id,
                ],
                $this->dataTransform($data)
            )
        );
    }

    public function duplicar(AutorizacaoExecucao $autorizacaoExecucao)
    {
        $aeDuplicada = $autorizacaoExecucao->replicate(['id'])->fill([
            'numero' => $this->getProximoNumero(AutorizacaoExecucao::class, $autorizacaoExecucao->contrato_id),
            'situacao_id' => CodigoItem::where('descres', 'ae_status_1')->first()->id
        ]);

        $aeDuplicada->save();

        $autorizacaoExecucao
            ->autorizacaoexecucaoUnidadeRequisitantes
            ->each(function ($autorizacaoexecucaoUnidadeRequisitante) use ($aeDuplicada) {
                $this->replicate(
                    $autorizacaoexecucaoUnidadeRequisitante,
                    $aeDuplicada->autorizacaoexecucaoUnidadeRequisitantes()
                );
            });

        $autorizacaoExecucao
            ->autorizacaoexecucaoEmpenhos
            ->each(function ($autorizacaoExecucaoEmpenho) use ($aeDuplicada) {
                $this->replicate(
                    $autorizacaoExecucaoEmpenho,
                    $aeDuplicada->autorizacaoexecucaoEmpenhos()
                );
            });


        $autorizacaoExecucao
            ->autorizacaoexecucaoItens
            ->each(function ($autorizacaoExecucaoItem) use ($aeDuplicada) {
                $this->replicate(
                    $autorizacaoExecucaoItem,
                    $aeDuplicada->autorizacaoexecucaoItens()
                );
            });
    }

    public function salvarArquivo(AutorizacaoExecucao $autorizacaoExecucao, UploadedFile $file)
    {
        $this->salvarArquivoFormatoGenerico(
            $file,
            AutorizacaoExecucao::class,
            $autorizacaoExecucao->id,
            CodigoItem::where('descres', 'ae_arquivo_assinado')->first()->id,
            "autorizacaoexecucao",
        );
    }

    public function salvarAnexo(AutorizacaoExecucao $autorizacaoExecucao, array $anexoData)
    {
        $urlDestino = $this->moveAnexoTemporario($anexoData['url_arquivo_anexo']);

        $autorizacaoExecucao->arquivo()->create([
            'nome' => $anexoData['nome_arquivo_anexo'],
            'descricao' => $anexoData['descricao_arquivo_anexo'] ?? '',
            'url' => $urlDestino,
            'tipo_id' => CodigoItem::where('descres', 'ae_arquivo_anexo')->first()->id,
            'restrito' => false,
        ]);
    }

    public function excluiAnexo(array $anexoData)
    {
        if (empty($anexoData['id'])) {
            $this->deleteAnexoTemporario($anexoData['url_arquivo_anexo']);
        } else {
            $this->excluiArquivo($anexoData['id'], false);
        }
    }

    public function excluiArquivo(int $id, $idIsArquivoable = true)
    {
        $arquivoGenerico = new ArquivoGenericoRepository();

        if ($idIsArquivoable) {
            $anexo = $arquivoGenerico->getArquivoGenerico($id, AutorizacaoExecucao::class);
        } else {
            $anexo = $arquivoGenerico->getArquivoGenericoUnico($id);
        }

        if ($anexo) {
            $this->deletarArquivoFiscamente($arquivoGenerico, $anexo->id);
            $anexo->delete();
        }
    }

    private function dataTransform(array $data): array
    {
        return [
            'processo' => $data['processo'] ?? null,
            'numero_sistema_origem' => $data['numero_sistema_origem'] ?? null,
            'tipo_id' => $data['tipo_id'],
            'numero' => $data['numero'] ?? null,
            'data_assinatura' => $data['data_assinatura'] ?? null,
            'data_vigencia_inicio' => $data['data_vigencia_inicio'] ?? null,
            'data_vigencia_fim' => $data['data_vigencia_fim'] ?? null,
            'informacoes_complementares' => $data['informacoes_complementares'] ?? null,
            'rascunho' => $data['rascunho'],
            'originado_sistema_externo' => $data['originado_sistema_externo']
        ];
    }

    /**
     * @param $autorizacaoExecucaoUnidadeRequisitante
     * @param  AutorizacaoExecucao  $aeDuplicada
     * @return void
     */
    private function replicate(Model $modelOrigem, HasMany $relacionamentoDestino): void
    {
        $dadosDuplicados = $modelOrigem->replicate([
            'autorizacaoexecucoes_id',
            'deleted_at',
            'created_at',
            'updated_at',
        ]);

        $relacionamentoDestino->save($dadosDuplicados);
    }

    private function moveAnexoTemporario(string $urlArquivoAnexo): string
    {
        $date = Carbon::now();
        $nomeArquivoTemporario = last(explode('/', $urlArquivoAnexo));
        $urlDestino = 'autorizacaoexecucao/' .
            $date->format('Y') . '/' .
            $date->format('m') . '/' .
            $nomeArquivoTemporario;

        throw_unless(
            Storage::disk('public')->move($urlArquivoAnexo, $urlDestino),
            500,
            'Não foi possivel mover o anexo temporário'
        );

        return $urlDestino;
    }

    private function deleteAnexoTemporario(string $urlArquivoAnexo): void
    {
        throw_unless(
            Storage::disk('public')->delete($urlArquivoAnexo),
            500,
            'Não foi possivel deletar o anexo temporário'
        );
    }
}
