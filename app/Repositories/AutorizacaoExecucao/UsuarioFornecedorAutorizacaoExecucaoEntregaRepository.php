<?php

namespace App\Repositories\AutorizacaoExecucao;

use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoExecucaoEntrega;
use App\Models\AutorizacaoExecucaoEntregaItens;
use App\Models\AutorizacaoexecucaoItens;
use App\Models\CodigoItem;

class UsuarioFornecedorAutorizacaoExecucaoEntregaRepository
{
    /**
     * Retorna uma instância de AutorizacaoExecucao com base no ID da entrega.
     *
     * @param int $entregaId O ID da entrega.
     * @return AutorizacaoExecucao|null Retorna uma instância de AutorizacaoExecucao
     * se encontrada, ou null se não encontrada.
     */
    public function retornaOrdemServicoRepository(int $entregaId): ?AutorizacaoExecucao
    {
        return AutorizacaoExecucao::find($entregaId);
    }
    public function getSequencialRepository(int $entregaId)
    {
        return $this->retornaOrdemServicoRepository($entregaId)->entregas()
                ->max('sequencial') + 1;
    }

    public function retornaQuantidadeJaExecutadaOsfRepository(int $osfId, int $itemId, int $entregaId = null)
    {
        $sitacaoEntrega = CodigoItem::whereIn('descres', [
            'ae_entrega_status_1',
            'ae_entrega_status_3',
            'ae_entrega_status_9'
        ])->pluck('id');

        return AutorizacaoExecucaoEntregaItens::whereHas(
            'entrega',
            function ($query) use ($osfId, $sitacaoEntrega, $entregaId) {
                $query->where('autorizacaoexecucao_id', $osfId)
                ->whereNotIn('situacao_id', $sitacaoEntrega)
                ->where('id', '!=', $entregaId ?? 0);
            }
        )
            ->where('autorizacao_execucao_itens_id', $itemId)
            ->sum('quantidade_informada');
    }

    public function retornaTotaisEmAnaliseAteTRP(int $aeItemId, int $entregaItemId = null)
    {
        $sitacoesEmAnaliseAteTRP = CodigoItem::whereIn(
            'descres',
            [
                'ae_entrega_status_1',
                'ae_entrega_status_2',
                'ae_entrega_status_4',
                'ae_entrega_status_6',
                'ae_entrega_status_10'
            ]
        )->pluck('id');

        return AutorizacaoExecucaoEntregaItens::selectRaw(
            'SUM(quantidade_informada) as quantidade_total, SUM(valor_glosa) as valor_glosa_total'
        )->whereHas('entrega', function ($query) use ($sitacoesEmAnaliseAteTRP) {
            $query->whereIn('situacao_id', $sitacoesEmAnaliseAteTRP);
        })->where('autorizacao_execucao_itens_id', $aeItemId)
            ->where('id', '<>', $entregaItemId ?? 0)
            ->first();
    }

    public function retornaTotaisEmAvaliacaoAteTRD(int $aeItemId, int $entregaItemId = null)
    {
        $sitacoesEmAnaliseAteTRP = CodigoItem::whereIn(
            'descres',
            [
                'ae_entrega_status_8',
                'ae_entrega_status_11',
            ]
        )->pluck('id');

        return AutorizacaoExecucaoEntregaItens::selectRaw(
            'SUM(quantidade_informada) as quantidade_total, SUM(valor_glosa) as valor_glosa_total'
        )->whereHas('entrega', function ($query) use ($sitacoesEmAnaliseAteTRP) {
            $query->whereIn('situacao_id', $sitacoesEmAnaliseAteTRP);
        })->where('autorizacao_execucao_itens_id', $aeItemId)
            ->where('id', '<>', $entregaItemId ?? 0)
            ->first();
    }

    public function retornaTotaisExecutadosPosTRD(int $aeItemId, int $entregaItemId = null)
    {
        $sitacaoId = CodigoItem::whereIn('descres', ['ae_entrega_status_7', 'ae_entrega_status_12'])->pluck('id');

        return AutorizacaoExecucaoEntregaItens::selectRaw(
            'SUM(quantidade_informada) as quantidade_total, SUM(valor_glosa) as valor_glosa_total'
        )->whereHas('entrega', function ($query) use ($sitacaoId) {
            $query->whereIn('situacao_id', $sitacaoId);
        })->where('autorizacao_execucao_itens_id', $aeItemId)
            ->where('id', '<>', $entregaItemId ?? 0)
            ->first();
    }

    public function retornaValorTotaldaOs(int $osfId)
    {
        return AutorizacaoExecucaoItens::where('autorizacaoexecucoes_id', $osfId)
            ->get()
            ->sum(function ($item) {
                return $item->quantidade * str_replace(['.', ','], ['', '.'], $item->valor_unitario);
            });
    }
}
