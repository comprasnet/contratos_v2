<?php

namespace App\Repositories\AutorizacaoExecucao;

use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoexecucaoHistorico;
use App\Services\ArquivoGenericoService;

class AutorizacaoExecucaoHistoricoRepository
{
    /**
     * @var AutorizacaoExecucao
     */
    private $autorizacaoExecucao;

    public function __construct(AutorizacaoExecucao $autorizacaoExecucao)
    {
        $this->autorizacaoExecucao = $autorizacaoExecucao;
    }

    public function deletar(int $id)
    {
        $arquivoGenerico = new ArquivoGenericoService();
        $arquivoGenerico->delete($id, AutorizacaoexecucaoHistorico::class);

        return AutorizacaoexecucaoHistorico::find($id)->delete();
    }

    /**
     * @param $tipoHistoricoId
     * @param $rascunho
     * @return int|mixed
     */
    public function getSequencial(int $tipoHistoricoId, bool $rascunho)
    {
        return $this->autorizacaoExecucao->autorizacaoexecucaoHistorico()
                ->where('tipo_historico_id', $tipoHistoricoId)
                ->where('rascunho', $rascunho)
                ->max('sequencial') + 1;
    }
}
