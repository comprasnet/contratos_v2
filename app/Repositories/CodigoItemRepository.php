<?php

namespace App\Repositories;

use App\Http\Traits\BuscaCodigoItens;
use App\Models\CodigoItem;

class CodigoItemRepository extends CodigoItem
{
    use BuscaCodigoItens;

    /**
     * Método responsável em retornar os IDs pela descrição do código
     */
    public function recuperarIdDescricao(array $tipos)
    {
        return $this->where('descres', 'statushistorico')
            ->whereIn('descricao', $tipos)
            ->select('id')
            ->pluck('id')
            ->toArray();
    }

    /**
     * Método responsável em retornar os status para análise dos itens
     */
    public function recuperarStatusItemAnalise(
        array $status = ['Aceitar', 'Negar', 'Aceitar Parcialmente'],
        string $orderBy = "descricao ='Aceitar Parcialmente', descricao = 'Negar', descricao = 'Aceitar'"
    ) {
        return CodigoItem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Ata de Registro de Preços')
                ->whereNull('deleted_at');
        })
            ->whereNull('deleted_at')
            ->whereIn('descricao', $status)
            ->orderByRaw($orderBy)
            ->pluck('id', 'descricao')
            ->toArray();
    }

    /**
     * Método responsável em retornar o registro único para o codigo item
     */
    public function getCodigoItemUnico(int $idCodigoItem)
    {
        return $this->find($idCodigoItem);
    }

    /**
     * Obtém o ID de codigoitens baseado na descrição e descres (caso este também seja informado)
     *
     * @param string $descricao
     * @param string|null $descres
     * @return integer|null
     */
    public function getCodigoItemId(string $descricao, string $descres = null): ?int
    {
        $query = CodigoItem::select('id')->where('descricao', $descricao);

        # Caso seja informado o valor de descres também, adiciona na busca
        if ($descres !== null) {
            $query->where('descres', $descres);
        }

        $codigoItem = $query->first();

        return $codigoItem ? $codigoItem->id : null;
    }
    
    public function getCodigoItemPorId(int $id)
    {
        return $this->find($id);
    }

    /**
     * @return mixed
     */
    public function retornaTiposRepository()
    {
        return Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Tipo de Contrato');
        })
            ->whereNotIn('descricao', config('app.tipo_contrato_not_in'))
            ->orderBy('descricao')
            ->pluck('descricao', 'id')
            ->toArray();
    }

    public function retornaCategoriasRepository()
    {
        return Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Categoria Contrato');
        })
            ->where('descricao', '<>', 'A definir')
            ->orderBy('descricao')
            ->pluck('descricao', 'id')
            ->toArray();
    }

    public function retornaModalidadeCompraRepository()
    {
        return Codigoitem::whereHas('codigo', function ($query) {
            $query->where('descricao', '=', 'Modalidade Licitação');
        })
            ->where('codigoitens.visivel', true)
            ->orderBy('codigoitens.descricao')
            ->pluck('codigoitens.descricao', 'codigoitens.id')
            ->toArray();
    }
}
