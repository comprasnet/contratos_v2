<?php

namespace App\Repositories\Contrato;

use App\Models\Contratoresponsavel;
use Illuminate\Database\Eloquent\Collection;

class ContratoResponsavelRepository
{
    private $contrato_id;

    public function __construct(int $contrato_id)
    {
        $this->contrato_id = $contrato_id;
    }

    private function query()
    {
        return Contratoresponsavel::query()
            ->where('contrato_id', $this->contrato_id)
            ->where('situacao', true);
    }

    public function getResponsaveisContrato()
    {
        return $this->query()->with('user')->get();
    }

    public function getWithFuncoesExcept(array $descres = [], array $columns = ['*']): Collection
    {
        return $this->query()->whereHas('funcao', function ($query) use ($descres) {
            $query->whereNotIn('descres', $descres);
        })
        ->with('funcao')
        ->get($columns);
    }

    public function getFiscaisContrato()
    {
        return $this->query()
            ->whereHas('funcao', function ($query) {
                $query->where('descricao', 'ilike', 'Fiscal%');
            })
            ->get();
    }

    public function isUsuarioResponsavel()
    {
        return $this->query()->where('user_id', backpack_user()->id)->exists();
    }

    public function isUsuarioFiscalContrato()
    {
        return $this->query()
            ->where('user_id', backpack_user()->id)
            ->whereHas('funcao', function ($query) {
                $query->where('descricao', 'ilike', 'Fiscal%');
            })
            ->exists();
    }

    public function isUsuarioGestorContrato()
    {
        return $this->query()
            ->where('user_id', backpack_user()->id)
            ->whereHas('funcao', function ($query) {
                $query->where('descricao', 'ilike', 'Gestor%');
            })
            ->exists();
    }
}
