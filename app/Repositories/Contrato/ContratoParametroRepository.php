<?php

namespace App\Repositories\Contrato;

use App\Http\Traits\Formatador;
use App\Models\ContratoParametro;
use App\Models\Contratopreposto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class ContratoParametroRepository
{
    use Formatador;

    private $contrato_id;

    public function __construct(int $contrato_id)
    {
        $this->contrato_id = $contrato_id;
    }

    public function query()
    {
        return ContratoParametro::query()->where('contrato_id', $this->contrato_id);
    }

    public function getDataPrazoRecebimentoProvisorio(): ?string
    {
        $parametro = $this->query()->first(['pz_recebimento_provisorio', 'tipo_recebimento_provisorio']);

        if (!$parametro) {
            return null;
        }

        if ($parametro->tipo_recebimento_provisorio === 'Úteis') {
            return $this->verificaDataDiaUtil(
                Carbon::now()
                    ->addWeekdays($parametro->pz_recebimento_provisorio)
                    ->toDateString()
            );
        }
        return Carbon::now()->addDays($parametro->pz_recebimento_provisorio)->toDateString();
    }

    public function getDataPrazoRecebimentoDefinitivo(): ?string
    {
        $parametro = $this->query()->first(['pz_recebimento_definitivo', 'tipo_recebimento_definitivo']);

        if (!$parametro) {
            return null;
        }

        if ($parametro->tipo_recebimento_definitivo === 'Úteis') {
            return $this->verificaDataDiaUtil(
                Carbon::parse($this->getDataPrazoRecebimentoProvisorio())
                ->addWeekdays($parametro->pz_recebimento_definitivo)
                ->toDateString()
            );
        }
        return Carbon::parse($this->getDataPrazoRecebimentoProvisorio())
            ->addDays($parametro->pz_recebimento_definitivo)
            ->toDateString();
    }
}
