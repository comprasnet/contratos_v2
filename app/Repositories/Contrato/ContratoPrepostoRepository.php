<?php

namespace App\Repositories\Contrato;

use App\Models\Contratopreposto;
use Illuminate\Database\Eloquent\Collection;

class ContratoPrepostoRepository
{
    private $contrato_id;

    public function __construct(int $contrato_id)
    {
        $this->contrato_id = $contrato_id;
    }

    public function query()
    {
        return Contratopreposto::query()->where('contrato_id', $this->contrato_id);
    }

    public function getBySituacao(bool $situacao = true, array $columns = ['*']): Collection
    {
        return $this->query()->where('situacao', $situacao)->get($columns);
    }
}
