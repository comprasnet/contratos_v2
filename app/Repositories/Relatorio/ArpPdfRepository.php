<?php

namespace App\Repositories\Relatorio;

use App\Models\Arp;
use App\Models\ArpAlteracao;
use App\Models\ArpPdf;
use App\Models\CodigoItem;
use App\Models\Contrato;
use Illuminate\Support\Facades\DB;
use App\Models\ArpItem;
use App\Models\CompraItem;
use Illuminate\Support\Facades\Route;

class ArpPdfRepository
{
    public function getItemPdf($arpId)
    {
        return ArpItem::select(
            DB::raw('DISTINCT ON ("compra_items"."numero") 
        CASE 
            WHEN compra_items.permite_carona = true THEN \'Sim\'
            WHEN compra_items.permite_carona = false THEN \'Não\'
        END AS aceita_adesao'),
            'compra_items.numero',
            'compra_items.descricaodetalhada',
            'compra_items.maximo_adesao',
            'compra_items.maximo_adesao_informado_compra',
            'fornecedores.cpf_cnpj_idgener',
            'fornecedores.nome as nomefornecedor',
            'compra_items.catmatseritem_id as codigo_item',
            'codigoitens.descricao as tipo_item',
            'compra_item_fornecedor.quantidade_homologada_vencedor
             as quantidade_homologada'
        )
            ->leftJoin(
                'compra_item_fornecedor',
                'arp_item.compra_item_fornecedor_id',
                '=',
                'compra_item_fornecedor.id'
            )
            ->leftJoin(
                'fornecedores',
                'fornecedores.id',
                '=',
                'compra_item_fornecedor.fornecedor_id'
            )
            ->leftJoin(
                'compra_items',
                'compra_items.id',
                '=',
                'compra_item_fornecedor.compra_item_id'
            )
            ->leftJoin(
                'codigoitens',
                'codigoitens.id',
                '=',
                'compra_items.tipo_item_id'
            )
            ->where('arp_item.arp_id', $arpId)
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('arp_item_historico')
                    ->join(
                        'arp_historico',
                        'arp_historico.id',
                        '=',
                        'arp_item_historico.arp_historico_id'
                    )
                    ->join('arp_alteracao', function ($query) {
                        $query->on('arp_historico.arp_alteracao_id', 'arp_alteracao.id')
                            ->where('arp_alteracao.rascunho', false);
                    })
                    ->whereRaw('arp_item_historico.arp_item_id = arp_item.id')
                    ->where(function ($query) {
                        $query->where('arp_item_historico.item_cancelado', true)
                            ->orWhere('arp_item_historico.item_removido_retificacao', true);
                    });
            })
            //  ->whereNull('arp_item.deleted_at')
            ->orderBy('compra_items.numero', 'asc')
            ->get();
    }

    public function getArpPdfData($arpId)
    {
        return Arp::select(
            'arp.id',
            'arp.updated_at',
            'arp.vigencia_inicial as arp_vigencia_inicial',
            'arp.vigencia_final as arp_vigencia_final',
            DB::raw('MAX(CONCAT("arp"."numero", \'/\',
             "arp"."ano")) as arp_numero'),
            'unidades.codigosiasg as cod',
            'orgaos.nome as orgao_nome',
            DB::raw('MAX(CONCAT("fornecedores"."cpf_cnpj_idgener",
             \' - \', "fornecedores"."nome")) as fornecedor_nome'),
            DB::raw('MAX(CONCAT("unidades"."codigosiasg",
             \' - \', "unidades"."nomeresumido")) as unidade_gerenciadora'),
            DB::raw('MAX(CONCAT("unidades"."codigosiasg",
             \' - \', "unidades"."nome")) as unidade_gerenciadora_titulo'),
            DB::raw('MAX("unidades"."nome") as unidade_id'),
            'arp.objeto',
            'arp.data_assinatura'
        )
            ->leftJoin('compras', 'compras.id', '=', 'arp.compra_id')
            ->leftJoin(
                'compra_items',
                'compra_items.compra_id',
                '=',
                'compras.id'
            )
            ->leftJoin(
                'compra_item_unidade',
                'compra_item_unidade.compra_item_id',
                '=',
                'compra_items.id'
            )
            ->leftJoin(
                'unidades',
                'unidades.id',
                '=',
                'compra_item_unidade.unidade_id'
            )
            ->leftJoin('orgaos', 'orgaos.id', '=', 'unidades.orgao_id')
            ->leftJoin('arp_item', 'arp_item.arp_id', '=', 'arp.id')
            ->leftJoin(
                'compra_item_fornecedor',
                'compra_item_fornecedor.id',
                '=',
                'arp_item.compra_item_fornecedor_id'
            )
            ->leftJoin(
                'fornecedores',
                'fornecedores.id',
                '=',
                'compra_item_fornecedor.fornecedor_id'
            )
            ->leftJoin(
                'codigoitens',
                'codigoitens.id',
                '=',
                'compras.modalidade_id'
            )
            ->leftJoin(
                'municipios',
                'municipios.id',
                '=',
                'unidades.municipio_id'
            )
            ->leftJoin(
                'estados',
                'estados.id',
                '=',
                'municipios.estado_id'
            )
            ->where('arp.id', $arpId)
            ->where('compra_item_unidade.tipo_uasg', 'G')
            ->where('arp.rascunho', false)
            // ->whereNull('arp.deleted_at')
            ->groupBy(
                'arp.id',
                'arp.updated_at',
                'orgao_nome',
                'arp.objeto',
                'arp.vigencia_inicial',
                'arp.vigencia_final',
                'unidades.codigosiasg',
                'arp.data_assinatura'
            )
            ->orderBy('arp.created_at', 'asc')
            ->first();
    }

    public function getItemsByNumero($arpId)
    {
           return  ArpItem::select(
               'compra_items.numero',
               'compra_items.descricaodetalhada',
               'compra_items.maximo_adesao',
               'fornecedores.nome as nomefornecedor',
               'compra_items.catmatseritem_id as codigo_item',
               'compra_items.catmatseritem_id as codigo_item',
               'codigoitens.descricao as tipo_item',
               'compra_item_fornecedor.quantidade_homologada_vencedor
             as quantidade_homologada',
               'compra_item_fornecedor.ata_vigencia_inicio',
               'compra_item_fornecedor.ata_vigencia_fim'
           )
            ->leftJoin(
                'compra_item_fornecedor',
                'arp_item.compra_item_fornecedor_id',
                '=',
                'compra_item_fornecedor.id'
            )
            ->leftJoin(
                'fornecedores',
                'fornecedores.id',
                '=',
                'compra_item_fornecedor.fornecedor_id'
            )
            ->leftJoin(
                'compra_items',
                'compra_items.id',
                '=',
                'compra_item_fornecedor.compra_item_id'
            )
            ->leftJoin(
                'codigoitens',
                'codigoitens.id',
                '=',
                'compra_items.tipo_item_id'
            )
            ->where('arp_item.arp_id', $arpId)
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('arp_item_historico')
                    ->join('arp_historico', 'arp_historico.id', '=', 'arp_item_historico.arp_historico_id')
                    ->whereRaw('arp_item_historico.arp_item_id = arp_item.id')
                    ->where('arp_item_historico.item_cancelado', true);
            })
            //->whereNull('arp_item.deleted_at')
            ///    ->where('compra_items.numero', $numero)
            ->orderBy('compra_items.numero', 'asc')
            ->get();
    }
    public function getFornecedorItens($arpId)
    {
        return ArpItem::select(
            'compra_item_fornecedor.quantidade_homologada_vencedor',
            DB::raw("CASE 
                    WHEN compra_item_fornecedor.percentual_maior_desconto IS NULL 
                         OR compra_item_fornecedor.percentual_maior_desconto = 0 
                    THEN compra_item_fornecedor.valor_unitario::text
                    ELSE TO_CHAR(compra_item_fornecedor.valor_unitario *
                        ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100), 'FM999999999.0000')
                END AS valor_unitario"),
            'compra_item_fornecedor.classificacao',
            'fornecedores.cpf_cnpj_idgener',
            'fornecedores.nome',
            'compra_items.numero'
        )
            ->leftJoin(
                'compra_item_fornecedor',
                'arp_item.compra_item_fornecedor_id',
                '=',
                'compra_item_fornecedor.id'
            )
            ->leftJoin(
                'fornecedores',
                'fornecedores.id',
                '=',
                'compra_item_fornecedor.fornecedor_id'
            )
            ->leftJoin(
                'compra_items',
                'compra_items.id',
                '=',
                'compra_item_fornecedor.compra_item_id'
            )
            ->where('arp_item.arp_id', $arpId)
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('arp_item_historico')
                    ->join(
                        'arp_historico',
                        'arp_historico.id',
                        '=',
                        'arp_item_historico.arp_historico_id'
                    )
                    ->whereRaw('arp_item_historico.arp_item_id = arp_item.id')
                    ->where('arp_item_historico.item_cancelado', true);
            })

            ->groupBy(
                'fornecedores.cpf_cnpj_idgener',
                'fornecedores.nome',
                'compra_item_fornecedor.quantidade_homologada_vencedor',
                'compra_item_fornecedor.valor_unitario',
                'compra_item_fornecedor.classificacao',
                'compra_items.numero',
                'compra_item_fornecedor.percentual_maior_desconto'
            )
            ->orderBy('compra_item_fornecedor.classificacao', 'asc')
            ->get();
    }


    public static function getItemsByUnidadeParticipantePorAtaItem($arpId)
    {
        return ArpItem::select(
            DB::raw(
                "CASE compra_item_unidade.tipo_uasg 
                    WHEN 'G' THEN 'Gerenciadora'
                    WHEN 'P' THEN 'Participante'
                    WHEN 'C' THEN 'Não participante' -- Carona
                END as tipo_uasg"
            ),
            "unidades.codigo",
            "unidades.nomeresumido",
            "compra_items.numero",
            "compra_item_unidade.quantidade_autorizada",
            # Se o saldo for negativo, retorna zero
            DB::raw(
                "CASE
                    WHEN COALESCE(SUM(compra_item_minuta_empenho.quantidade),
                     0) <= 0
                    THEN compra_item_unidade.quantidade_autorizada
                    ELSE GREATEST(
                            compra_item_unidade.quantidade_autorizada - 
                            COALESCE(SUM(compra_item_minuta_empenho.quantidade),
                             0), 0)
                END AS quantidade_saldo"
            )
        ) # fim da cláusula 'select'

        ->join('compra_item_fornecedor', function ($join) {
            $join->on(
                'compra_item_fornecedor.id',
                '=',
                'arp_item.compra_item_fornecedor_id'
            )
                ->where('compra_item_fornecedor.situacao', '=', true);
        })
            ->join(
                'compra_items',
                function ($join) {
                    $join->on(
                        'compra_items.id',
                        '=',
                        'compra_item_fornecedor.compra_item_id'
                    )
                        ->where('compra_items.situacao', '=', true);
                }
            )
            ->join('compra_item_unidade', function ($join) {
                $join->on(
                    'compra_item_unidade.compra_item_id',
                    '=',
                    'compra_items.id'
                )
                    #->on("compra_item_unidade.unidade_id", "=",
                    # "minutaempenhos.unidade_id")
                    ->where('compra_item_unidade.situacao', '=', true)
                    ->where('compra_item_unidade.tipo_uasg', '<>', 'C');
            })
            ->leftJoin("minutaempenhos", function ($join) {
                $join->on(
                    "minutaempenhos.unidade_id",
                    "=",
                    "compra_item_unidade.unidade_id"
                )
                    ->on(
                        "minutaempenhos.fornecedor_compra_id",
                        "=",
                        "compra_item_fornecedor.fornecedor_id"
                    )
                    ->on(
                        "minutaempenhos.compra_id",
                        "=",
                        "compra_items.compra_id"
                    )
                    ->where('minutaempenhos.situacao_id', '=', 217);
            })
            ->leftJoin("compra_item_minuta_empenho", function ($join) {
                $join->on(
                    "minutaempenhos.id",
                    "=",
                    "compra_item_minuta_empenho.minutaempenho_id"
                )
                    ->on(
                        "compra_items.id",
                        "=",
                        "compra_item_minuta_empenho.compra_item_id"
                    );
            })
            ->leftJoin(
                'minutaempenhos_remessa',
                function ($join) {
                    $join->on(
                        'compra_item_minuta_empenho.minutaempenhos_remessa_id',
                        '=',
                        'minutaempenhos_remessa.id'
                    )
                        ->where(function ($query) {
                            $query->where(function ($subquery) {
                                $subquery->where(
                                    'minutaempenhos_remessa.remessa',
                                    0
                                )
                                    ->where(
                                        'minutaempenhos_remessa.situacao_id',
                                        215
                                    );
                            })
                                ->orWhere(function ($subquery) {
                                    $subquery->where(
                                        'minutaempenhos_remessa.remessa',
                                        '>',
                                        0
                                    )
                                        ->where(
                                            'minutaempenhos_remessa.situacao_id',
                                            217
                                        );
                                });
                        });
                }
            )

            # Traz somente os empenhos que forem de compra
            ->leftJoin('codigoitens', function ($join) {
                $join->on(
                    'codigoitens.id',
                    '=',
                    'minutaempenhos.tipo_empenhopor_id'
                )
                    ->where('codigoitens.descricao', '=', 'Compra');
            })
            ->join(
                'unidades',
                'unidades.id',
                '=',
                'compra_item_unidade.unidade_id'
            )
            ->where('arp_id', '=', $arpId)
            //->where('compra_items.numero', '=', $numero_item)
            ->groupBy(
                'unidades.codigo',
                'compra_item_unidade.tipo_uasg',
                'unidades.nomeresumido',
                "compra_items.numero",
                'compra_item_unidade.quantidade_autorizada'
            )
            ->get();
    }

    /*
    public function getUnidadeItens($arpId)
    {

        return ArpItem::select(
            DB::raw(
                "CASE compra_item_unidade.tipo_uasg
                    WHEN 'G' THEN 'Gerenciadora'
                    WHEN 'P' THEN 'Participante'
                    WHEN 'C' THEN 'Carona'
                END as tipo_uasg"
            ),
            DB::raw("CONCAT(unidades.codigo, ' - ', unidades.nomeresumido)
            AS codigo_nomeresumido"),
            "unidades.codigo",
            "unidades.nomeresumido",
            "compra_items.numero",
            "compra_item_unidade.quantidade_autorizada",
            DB::raw(
                "CASE
                    WHEN compra_item_unidade.quantidade_saldo >= 0
                    THEN compra_item_unidade.quantidade_saldo
                    ELSE '0.00000'
                END quantidade_saldo"
            )
        )
            ->Join('compra_item_fornecedor', function ($join) {
                $join->on(
                    'compra_item_fornecedor.id',
                    '=',
                    'arp_item.compra_item_fornecedor_id'
                )
                    ->where('compra_item_fornecedor.situacao', '=', true);
            })
            ->Join(
                'compra_items',
                function ($join) {
                    $join->on(
                        'compra_items.id',
                        '=',
                        'compra_item_fornecedor.compra_item_id'
                    )
                        ->where('compra_items.situacao', '=', true);
                }
            )
            ->Join('compra_item_unidade', function ($join) {
                $join->on(
                    'compra_item_unidade.compra_item_id',
                    '=',
                    'compra_items.id'
                )
                    ->where('compra_item_unidade.situacao', '=', true)
                    ->where('compra_item_unidade.tipo_uasg', '<>', 'C');
            })
            ->Join(
                'unidades',
                'unidades.id',
                '=',
                'compra_item_unidade.unidade_id'
            )
            ->where('arp_id', '=', $arpId)
            ->groupBy(
                'unidades.codigo',
                'compra_item_unidade.tipo_uasg',
                'unidades.nomeresumido',
                "compra_items.numero",
                'compra_item_unidade.quantidade_autorizada',
                "compra_item_unidade.quantidade_saldo"
            )
            ->get();
    }
    */
    public function getItemsByTotalizadoresAdesao($arpId)
    {
        return ArpItem::select(
            'compra_items.maximo_adesao',
            'compra_items.numero',
            DB::raw('CASE WHEN compra_items.maximo_adesao < 0 THEN 0 ELSE
             compra_items.maximo_adesao END as maximo_adesao'),
            DB::raw('CASE WHEN compra_items.maximo_adesao - COALESCE(
            SUM(arp_solicitacao_item.quantidade_aprovada), 0)
             < 0 THEN 0 ELSE compra_items.maximo_adesao - COALESCE(
             SUM(arp_solicitacao_item.quantidade_aprovada), 0) END
              as quantidade_disponivel_adesao'),
            DB::raw(
                'SUM(
            CASE WHEN arp_solicitacao_item.quantidade_aprovada IS NULL AND
             arp_solicitacao.rascunho = false
            THEN arp_solicitacao_item.quantidade_solicitada ELSE 0 END)
             as quantidade_aguardando_analise'
            ),
            DB::raw(
                "CASE
            WHEN compra_items.permite_carona = true THEN 'Sim'
            WHEN compra_items.permite_carona = false THEN 'Não'
        END as aceita_adesao"
            )
        )
            ->leftJoin(
                'compra_item_fornecedor',
                'compra_item_fornecedor.id',
                '=',
                'arp_item.compra_item_fornecedor_id'
            )
            ->leftJoin(
                'compra_items',
                'compra_items.id',
                '=',
                'compra_item_fornecedor.compra_item_id'
            )
            ->leftJoin('arp', 'arp.id', '=', 'arp_item.arp_id')
            ->leftJoin(
                'arp_solicitacao_item',
                'arp_item.id',
                '=',
                'arp_solicitacao_item.item_arp_fornecedor_id'
            )
            ->leftJoin('arp_solicitacao', function ($join) use ($arpId) {
                $join->on(
                    'arp_solicitacao_item.arp_solicitacao_id',
                    '=',
                    'arp_solicitacao.id'
                )
                    ->where('arp_solicitacao.rascunho', '=', false);
            })
            ->where('arp.id', $arpId)
            ->groupBy(
                'compra_items.maximo_adesao',
                'compra_items.numero',
                'aceita_adesao'
            )
            ->orderBy('compra_items.numero', 'asc')
            ->get();
    }


    public function getItemsByAdesoesAprovadas($arpId)
    {
        return ArpItem::select(
            DB::raw(
                'unidades.codigo || \'-\' || unidades.nomeresumido as 
                unidade_solicitacao'
            ),
            'compra_items.maximo_adesao',
            'arp_solicitacao_item.quantidade_aprovada',
            'compra_items.numero'
        )
            ->addSelect(
                DB::raw(
                    'arp_solicitacao_item.quantidade_solicitada,
            arp_solicitacao.created_at as data_hora_solicitacao'
                )
            )
            ->leftJoin('arp', 'arp.id', '=', 'arp_item.arp_id')
            ->leftJoin(
                'compra_item_fornecedor',
                'arp_item.compra_item_fornecedor_id',
                '=',
                'compra_item_fornecedor.id'
            )
            ->leftJoin(
                'compra_items',
                'compra_item_fornecedor.compra_item_id',
                '=',
                'compra_items.id'
            )
            ->leftJoin(
                'arp_solicitacao_item',
                'arp_item.id',
                '=',
                'arp_solicitacao_item.item_arp_fornecedor_id'
            )
            ->leftJoin('arp_solicitacao', function ($join) {
                $join->on(
                    'arp_solicitacao_item.arp_solicitacao_id',
                    '=',
                    'arp_solicitacao.id'
                )
                    ->where('arp_solicitacao.rascunho', '=', false);
            })
            ->leftJoin(
                'unidades',
                'arp_solicitacao.unidade_origem_id',
                '=',
                'unidades.id'
            )
            ->where('arp.id', $arpId)
            ->whereNotNull('arp_solicitacao_item.quantidade_aprovada')
            ->where('arp_solicitacao_item.quantidade_aprovada', '>', 0)
            ->orderBy('compra_items.numero', 'asc')
            ->get();
    }
    public function getItemsByAdesoesAnalise($arpId, $status = null)
    {

        $query = Arp::selectRaw(
            "unidades.codigo || '-' || unidades.nomeresumido AS
             unidade_solicitacao, compra_items.numero,
              arp_solicitacao_item.quantidade_aprovada,
               compra_items.maximo_adesao,
               arp_solicitacao.data_aprovacao_analise"
        )
            ->Join('arp_item', 'arp.id', '=', 'arp_item.arp_id')
            ->Join(
                'compra_item_fornecedor',
                'arp_item.compra_item_fornecedor_id',
                '=',
                'compra_item_fornecedor.id'
            )
            ->Join(
                'compra_items',
                'compra_item_fornecedor.compra_item_id',
                '=',
                'compra_items.id'
            )
            ->Join(
                'arp_solicitacao_item',
                'arp_item.id',
                '=',
                'arp_solicitacao_item.item_arp_fornecedor_id'
            )
            ->Join('arp_solicitacao', function ($join) {
                $join->on(
                    'arp_solicitacao_item.arp_solicitacao_id',
                    '=',
                    'arp_solicitacao.id'
                )
                    ->where('arp_solicitacao.rascunho', '=', false);
            })
            ->Join(
                'unidades',
                'arp_solicitacao.unidade_origem_id',
                '=',
                'unidades.id'
            )
            ->where('arp.id', '=', $arpId);

        switch ($status) {
            case 'aguardando_analise':
                $query->addSelect(
                    DB::raw(
                        'arp_solicitacao_item.quantidade_solicitada,
                    arp_solicitacao.created_at as data_hora_solicitacao'
                    )
                )
                    ->whereNull('arp_solicitacao_item.quantidade_aprovada')
                    ->orderBy('arp_solicitacao.created_at', 'asc');
                break;
            case 'analisada':
                $query->addSelect('arp_solicitacao_item.quantidade_aprovada')
                    ->whereNotNull('arp_solicitacao_item.quantidade_aprovada')
                    ->where('arp_solicitacao_item.quantidade_aprovada', '>', 0);
                break;
            case 'negada':
                $query->addSelect('arp_solicitacao_item.quantidade_solicitada')
                    ->where(
                        'arp_solicitacao_item.quantidade_aprovada',
                        '=',
                        '0'
                    )
                    ->whereNotNull('arp_solicitacao_item.quantidade_aprovada');
                break;
        }
        // dd($query->toSql());

        return collect($query->get());
    }



    public function getItemsByTotalRegistrada($arpId)
    {
        $subQuery = ArpItem::selectRaw(
            'MAX(compra_item_unidade.quantidade_autorizada) as
             quantidade_autorizada'
        )
            ->join('compra_item_fornecedor', function ($join) {
                $join->on(
                    'compra_item_fornecedor.id',
                    '=',
                    'arp_item.compra_item_fornecedor_id'
                )
                    ->where('compra_item_fornecedor.situacao', true);
            })
            ->join('compra_items', function ($join) {
                $join->on(
                    'compra_items.id',
                    '=',
                    'compra_item_fornecedor.compra_item_id'
                )
                    ->where('compra_items.situacao', true);
            })
            ->join('compra_item_unidade', function ($join) {
                $join->on(
                    'compra_item_unidade.compra_item_id',
                    '=',
                    'compra_items.id'
                )
                    ->where('compra_item_unidade.situacao', true)
                    ->where('compra_item_unidade.tipo_uasg', '<>', 'C');
            })

            ->where('arp_item.arp_id', $arpId)
            ->whereNull('arp_item.deleted_at')
            ->groupBy('compra_item_unidade.unidade_id');

        return DB::table(DB::raw("({$subQuery->toSql()}) as
         subquery"))
            ->mergeBindings($subQuery->getQuery())
            ->selectRaw('SUM(subquery.quantidade_autorizada) as
             quantidade_total_registrada')
            ->first();
    }
    public static function getItemsByTotalAprovadaParaAdesao($arpId)
    {
        return ArpItem::select(
            DB::raw("sum(arp_solicitacao_item.quantidade_aprovada)
             quantidade_total_aprovada_adesao")
        )
            ->join(
                'compra_item_fornecedor',
                'arp_item.compra_item_fornecedor_id',
                '=',
                'compra_item_fornecedor.id'
            )
            ->join(
                'compra_items',
                'compra_item_fornecedor.compra_item_id',
                '=',
                'compra_items.id'
            )
            ->join(
                'arp_solicitacao_item',
                'arp_item.id',
                '=',
                'arp_solicitacao_item.item_arp_fornecedor_id'
            )
            ->join('arp_solicitacao', function ($join) {
                $join->on(
                    'arp_solicitacao_item.arp_solicitacao_id',
                    '=',
                    'arp_solicitacao.id'
                )
                    ->where('arp_solicitacao.rascunho', '=', false);
            })
            ->join(
                'unidades',
                'arp_solicitacao.unidade_origem_id',
                '=',
                'unidades.id'
            )
            ->where('arp_id', '=', $arpId)
            ->whereNotNull('arp_solicitacao_item.quantidade_aprovada')
            ->where('arp_solicitacao_item.quantidade_aprovada', '>', 0)
            //->dd()
            ->first();
    }



    public function getItemsByTotalRegistradaAprovadaSomada($arpId)
    {
        // Obtenha o resultado da primeira consulta
        $quantidadeTotalRegistrada = $this->getItemsByTotalRegistrada($arpId);

        // Obtenha o resultado da segunda consulta
        $quantidadetotalAprovadaParaAdesao =
            $this->getItemsByTotalAprovadaParaAdesao($arpId);

        // Some os resultados
        $somaResultados =
            $quantidadeTotalRegistrada->quantidade_total_registrada +
            $quantidadetotalAprovadaParaAdesao->quantidade_total_aprovada_adesao;

        return $somaResultados;
    }


    public function getItemsByTotaisMinutaEmpenho($arpId, $valorRetorno = '*')
    {
        switch ($valorRetorno) {
            case 'quantidade_total_empenhada':
                $query = ArpItem::select(
                    DB::raw("sum(compra_item_minuta_empenho.quantidade) as
                     quantidade_total_empenhada")
                );
                break;
            case '*':
                $query = ArpItem::select(
                    DB::raw("(unidades.codigo || ' - ' ||
                     unidades.nomeresumido) as unidade"),
                    DB::raw(
                        "CASE compra_item_unidade.tipo_uasg 
                            WHEN 'G' THEN 'Gerenciadora'
                            WHEN 'P' THEN 'Participante'
                            WHEN 'C' THEN 'Não participante' -- Carona
                        END as tipo_uasg"
                    ),
                    'compra_items.numero',
                    DB::raw("compra_item_unidade.quantidade_autorizada"),
                    DB::raw("sum(compra_item_minuta_empenho.quantidade)
                     as quantidade_empenhada"),
                    DB::raw(
                        "CASE
                            WHEN 
                            compra_item_unidade.quantidade_autorizada
                             - sum(compra_item_minuta_empenho.quantidade) < 0
                            THEN 0
                            ELSE compra_item_unidade.quantidade_autorizada 
                            - sum(compra_item_minuta_empenho.quantidade)
                        END as saldo_para_empenho"
                    )
                )->groupBy(
                    "unidades.codigo",
                    "unidades.nomeresumido",
                    "compra_item_unidade.quantidade_autorizada",
                    "compra_item_unidade.quantidade_saldo",
                    'compra_items.numero',
                    "tipo_uasg"
                );
                break;
        }

        return $query->join("arp", "arp.id", "=", "arp_item.arp_id")
            ->join(
                "arp_unidades",
                "arp_item.id",
                "=",
                "arp_unidades.arp_item_id"
            )
            ->join(
                "compra_item_fornecedor",
                "arp_item.compra_item_fornecedor_id",
                "=",
                "compra_item_fornecedor.id"
            )
            ->join(
                "compra_items",
                "compra_item_fornecedor.compra_item_id",
                "=",
                "compra_items.id"
            )
            ->join("compras", "compra_items.compra_id", "=", "compras.id")
            ->join(
                "compra_item_minuta_empenho",
                "compra_items.id",
                "=",
                "compra_item_minuta_empenho.compra_item_id"
            )

            ->join("minutaempenhos", function ($join) {
                $join->on(
                    "compra_item_minuta_empenho.minutaempenho_id",
                    "=",
                    "minutaempenhos.id"
                )
                    ->on(
                        "minutaempenhos.fornecedor_compra_id",
                        "=",
                        "compra_item_fornecedor.fornecedor_id"
                    );
            })
            ->join("compra_item_unidade", function ($join) {
                $join->on(
                    "compra_items.id",
                    "=",
                    "compra_item_unidade.compra_item_id"
                )
                    ->on(
                        "compra_item_unidade.unidade_id",
                        "=",
                        "minutaempenhos.unidade_id"
                    )
                    ->where(
                        "compra_item_unidade.situacao",
                        "=",
                        true
                    );
            })
            ->join(
                "unidades",
                "unidades.id",
                "=",
                "minutaempenhos.unidade_id"
            )
            ->join(
                "minutaempenhos_remessa",
                "compra_item_minuta_empenho.minutaempenhos_remessa_id",
                "=",
                "minutaempenhos_remessa.id"
            )

            ->whereNull("arp_item.deleted_at")
            ->where("arp.id", $arpId)
            //->where('compra_items.numero', '=', $numero_item)
            ->whereRaw("
                    minutaempenhos.situacao_id = 217
                        and (
                        (minutaempenhos_remessa.remessa = 0 and 
                        minutaempenhos_remessa.situacao_id = 215)
                        or
                        (minutaempenhos_remessa.remessa > 0 AND
                         minutaempenhos_remessa.situacao_id = 217)
                    )")

            ->get();
    }

    public static function getItemsByMinutasEmpenhoPorUnidade($arpId)
    {
        return ArpItem::select(
            "minutaempenhos.mensagem_siafi",
            DB::raw("(unidades.codigo || ' - ' ||
             unidades.nomeresumido) as unidade"),
            "minutaempenhos.data_emissao",
            'compra_items.numero',
            DB::raw("(fornecedores.cpf_cnpj_idgener || ' - ' ||
            fornecedores.nome) as fornecedor"),
            DB::raw(
                "SUM(CASE
                        WHEN compra_item_minuta_empenho.operacao_id = 220
                        THEN compra_item_minuta_empenho.quantidade 
                        ELSE 0.00000 END
                    ) AS quantidade_inclusao"
            ),
            DB::raw(
                "SUM(CASE
                        WHEN compra_item_minuta_empenho.operacao_id = 221
                        THEN compra_item_minuta_empenho.quantidade 
                        ELSE 0.00000 END
                    ) AS quantidade_reforco"
            ),
            DB::raw(
                "SUM(CASE
                        WHEN compra_item_minuta_empenho.operacao_id = 222
                        THEN compra_item_minuta_empenho.quantidade 
                        ELSE 0.00000 END
                    ) AS quantidade_anulacao"
            ),
            DB::raw(
                "(SUM(CASE 
                    WHEN compra_item_minuta_empenho.operacao_id = 220
                    THEN compra_item_minuta_empenho.quantidade 
                    ELSE 0.00000 END
                ) +
                SUM(CASE
                    WHEN compra_item_minuta_empenho.operacao_id = 221
                    THEN compra_item_minuta_empenho.quantidade 
                    ELSE 0.00000 END
                ) + 
                SUM(CASE
                    WHEN compra_item_minuta_empenho.operacao_id = 222
                    THEN compra_item_minuta_empenho.quantidade 
                    ELSE 0.00000 END
                )) AS quantidade_empenhada"
            ),
            DB::raw(
                "SUM(compra_item_minuta_empenho.valor) as valor"
            )
        )
            ->join("arp", "arp.id", "=", "arp_item.arp_id")
            ->join(
                "arp_unidades",
                "arp_item.id",
                "=",
                "arp_unidades.arp_item_id"
            )
            ->join(
                "compra_item_fornecedor",
                "arp_item.compra_item_fornecedor_id",
                "=",
                "compra_item_fornecedor.id"
            )
            ->join(
                "fornecedores",
                "fornecedores.id",
                "=",
                "compra_item_fornecedor.fornecedor_id"
            )
            ->join(
                "compra_items",
                "compra_item_fornecedor.compra_item_id",
                "=",
                "compra_items.id"
            )
            ->join(
                "compras",
                "compra_items.compra_id",
                "=",
                "compras.id"
            )
            ->join(
                "compra_item_minuta_empenho",
                "compra_items.id",
                "=",
                "compra_item_minuta_empenho.compra_item_id"
            )

            ->join("minutaempenhos", function ($join) {
                $join->on(
                    "compra_item_minuta_empenho.minutaempenho_id",
                    "=",
                    "minutaempenhos.id"
                )
                    ->on(
                        "minutaempenhos.fornecedor_compra_id",
                        "=",
                        "compra_item_fornecedor.fornecedor_id"
                    );
            })
            ->join("compra_item_unidade", function ($join) {
                $join->on(
                    "compra_items.id",
                    "=",
                    "compra_item_unidade.compra_item_id"
                )
                    ->on(
                        "compra_item_unidade.unidade_id",
                        "=",
                        "minutaempenhos.unidade_id"
                    )
                    ->where(
                        "compra_item_unidade.situacao",
                        "=",
                        true
                    );
            })
            ->join(
                "unidades",
                "unidades.id",
                "=",
                "minutaempenhos.unidade_id"
            )
            ->join(
                "minutaempenhos_remessa",
                "compra_item_minuta_empenho.minutaempenhos_remessa_id",
                "=",
                "minutaempenhos_remessa.id"
            )

            ->whereNull("arp_item.deleted_at")
            ->where("arp.id", $arpId)
            // ->where('compra_items.numero', '=', $numero_item)
            ->whereRaw("
                minutaempenhos.situacao_id = 217
                    and (
                    (minutaempenhos_remessa.remessa = 0 and
                     minutaempenhos_remessa.situacao_id = 215)
                    or
                    (minutaempenhos_remessa.remessa > 0 AND 
                    minutaempenhos_remessa.situacao_id = 217)
                )")
            ->groupBy(
                "minutaempenhos.mensagem_siafi",
                "unidade",
                "minutaempenhos.data_emissao",
                'compra_items.numero',
                "fornecedor"
            )
            ->orderBy("minutaempenhos.data_emissao")

            ->get();
    }
    public function getItemsByTotalRegistradaSaldoSomada($arpId)
    {
        $quantidadeTotalRegistradaAutorizada =
            $this->getItemsByTotalRegistradaAprovadaSomada($arpId);

        $quantidadeTotalEmpenhada = $this->getItemsByTotaisMinutaEmpenho(
            $arpId,
            'quantidade_total_empenhada'
        );
        $somaResultadosSaldo = $quantidadeTotalRegistradaAutorizada -
            $quantidadeTotalEmpenhada[0]->quantidade_total_empenhada;
        return $somaResultadosSaldo;
    }
    public static function getItemsByContratosItem($arpId)
    {
        return Contrato::select(
            'contratos.id',
            'contratos.numero',
            'contratoitens.numero_item_compra',
            DB::raw("unidades.codigo || ' - ' || 
            unidades.nomeresumido unidade"),
            DB::raw("fornecedores.cpf_cnpj_idgener || ' - ' ||
            fornecedores.nome as fornecedor_contrato"),
            DB::raw("sum(contratoitens.quantidade) quantidade_contratada")
        )
            ->join('contratoitens', function ($join) {
                $join->on('contratoitens.contrato_id', '=', 'contratos.id')
                    ->whereNull('contratoitens.deleted_at');
            })
            ->join('compras', function ($join) {
                $join->on(
                    'contratos.unidadecompra_id',
                    '=',
                    'compras.unidade_origem_id'
                )
                    ->on(
                        'contratos.modalidade_id',
                        '=',
                        'compras.modalidade_id'
                    )
                    ->on(
                        'contratos.licitacao_numero',
                        '=',
                        'compras.numero_ano'
                    );
            })
            ->join('compra_items', function ($join) {
                $join->on('compra_items.compra_id', '=', 'compras.id')
                    ->where('compra_items.situacao', true)
                    ->whereColumn(
                        'compra_items.numero',
                        '=',
                        'contratoitens.numero_item_compra'
                    );
            })
            ->join('compra_item_fornecedor', function ($join) {
                $join->on(
                    'compra_item_fornecedor.compra_item_id',
                    '=',
                    'compra_items.id'
                )
                    ->where('compra_item_fornecedor.situacao', true)
                    ->whereColumn(
                        'compra_item_fornecedor.fornecedor_id',
                        '=',
                        'contratos.fornecedor_id'
                    );
            })
            ->join(
                'arp_item',
                'arp_item.compra_item_fornecedor_id',
                '=',
                'compra_item_fornecedor.id'
            )

            ->join(
                'fornecedores',
                'fornecedores.id',
                '=',
                'contratos.fornecedor_id'
            )
            ->join(
                'unidades',
                'contratos.unidade_id',
                '=',
                'unidades.id'
            )
            ->join(
                'codigoitens',
                'codigoitens.id',
                'contratoitens.tipo_id'
            )
            ->where('arp_item.arp_id', $arpId)
            ->groupBy(
                "contratos.id",
                "contratos.numero",
                'contratoitens.numero_item_compra',
                "unidade",
                "fornecedor_contrato"
            )
          ->get();
    }

    public static function getItemsByBuscaHistoricoDiaPdf($arpId)
    {

        return ArpPdf::where('arp_id', $arpId)
            ->where('created_at', '>', date('Y-m-d H:i:s', strtotime('-1 day')))
            ->first();
    }

    private function recuperarIdTipoAlteracao(array $tipos)
    {
        return CodigoItem::where('descres', 'statushistorico')
            ->whereIn('descricao', $tipos)
            ->select('id')
            ->pluck('id')
            ->toArray();
    }
    public function getItemsByBuscaArpAlteracaoPdf($arpId)
    {
        $optionsAlteracao = ['Vigência', 'Valor(es) registrado(s)',
            'Cancelamento de item(ns)', 'Informativo'];
        $idAlteracao = $this->recuperarIdTipoAlteracao($optionsAlteracao);

        # Recupera todas as informações das alterações
        $atasHistorico = ArpAlteracao::join(
            "arp_historico",
            "arp_alteracao.id",
            "=",
            "arp_historico.arp_alteracao_id"
        )
            ->where("arp_alteracao.arp_id", $arpId)
            ->where("arp_alteracao.rascunho", false)
            ->whereIn("tipo_id", $idAlteracao)
            ->select([
                'arp_historico.id',
                'arp_historico.arp_id',
                'arp_historico.tipo_id',
                //'arp_historico.numero',
                //'arp_historico.ano',
                //'arp_historico.data_assinatura',
                //'arp_historico.vigencia_inicial',
                //'arp_historico.vigencia_final',
                'arp_historico.justificativa_motivo_cancelamento',
                'arp_historico.objeto_alteracao'
              //  'arp_historico.data_assinatura_alteracao_vigencia',
               // 'arp_historico.data_assinatura_alteracao_inicio_vigencia',
                //'arp_historico.data_assinatura_alteracao_fim_vigencia',
                //'arp_historico.quantitativo_renovado_vigencia',
                //'arp_historico.amparo_legal_renovacao_quantitativo'
            ])
             ->get();
        return $atasHistorico;
    }
}
