<?php

namespace App\Repositories\Relatorio;

use App\Models\ContratoOpm;
use App\Models\Contrato;
use App\Models\Contratoresponsavel;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Route;

class DeclaracaoOpmPdfRepository
{
    public function getContratoOpm($contrato_id, $id)
    {
        return ContratoOpm::where('id', $id)
            ->where('contrato_id', $contrato_id)
            ->with('contrato')
            ->firstOrFail();
    }

    public function getCabecalhoContratoPdf($contrato_id, $id)
    {
       // $contratoOpm = $this->getContratoOpm($contrato_id, $id);

        $contrato = Contrato::find($contrato_id);
        $item = new \stdClass();

        $item->numero_contrato = $contrato->numero;
        $item->fornecedor = "{$contrato->fornecedor->cpf_cnpj_idgener} - {$contrato->fornecedor->nome}";
        $item->contratante = $this->formataCnpj($contrato->unidadeorigem->orgao->cnpj) .
                                " - {$contrato->unidadeorigem->orgao->nome}";
        $item->objeto = isset($contrato->objeto) ? $contrato->objeto : '';
        $item->vigencia_inicial = (new \DateTime($contrato->vigencia_inicio))->format('d/m/Y');
        $item->vigencia_final = $contrato->vigencia_fim ?
            (new \DateTime($contrato->vigencia_fim))->format('d/m/Y') : 'Indeterminado';
        $item->amparo_legal = $contrato->retornaAmparo();
        $item->processo = $contrato->processo;

        // Prepostos
        $prepostos = [];
        foreach ($contrato->prepostos as $preposto) {
            if ($preposto->situacao) {
                $prepostos[] = $preposto->nome . ' - ' . $preposto->email;
            }
        }
        $item->prepostos = implode('<br>', $prepostos);

        // Gestores
        $gestores = [];
        foreach ($contrato->responsaveis as $responsavel) {
            if (in_array($responsavel->funcao->descricao, ['Gestor Substituto', 'Gestor'])) {
                $gestores[] = $responsavel->user->name . ' - ' .
                                $responsavel->funcao->descricao . ' (' .
                                $responsavel->portaria . ') - ' .
                                $responsavel->user->email;
            }
        }
        $item->gestores = implode('<br>', $gestores);


        return $item;
    }

    private function formataCnpj($cnpj)
    {
        // Formatação do CNPJ
        return preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj);
    }
}
