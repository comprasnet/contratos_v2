<?php

namespace App\Repositories\Compra;

use App\Models\ArpItem;
use App\Models\CompraItemUnidade;

class CompraItemUnidadeRepository extends CompraItemUnidade
{
    /**
     * Método base para filtrar o compra tem unidade pelo item
     */
    public function getItemUnidadeBase(int $compraItemId)
    {
        return $this->where("compra_item_id", $compraItemId)
                    ->where("situacao", true);
    }
    /**
     * Método responsável em retornar as informações de um item da unidade
     */
    public function getItemUnidade(int $idItemUnidade)
    {
        return $this->find($idItemUnidade);
    }

    /**
     * Método responsável em retornar todos as unidades de um determinado item
     */
    public function getItemUnidadePorItem(int $compraItemId)
    {
        $itemUnidadeBase = $this->getItemUnidadeBase($compraItemId);
        return $itemUnidadeBase->get();
    }

    /**
     * Método responsável em retornar os itens com base no tipo da uasg
     */
    public function getItemUnidadePorTipoUasg(int $compraItemId, array $tipoUasg = ['G', 'P', 'C'])
    {
        $itemUnidadeBase = $this->getItemUnidadeBase($compraItemId);
        return $itemUnidadeBase->whereIn("tipo_uasg", $tipoUasg);
    }
    
    public function getItemUnidadePorItemSomenteCarona(int $idCompraItem, bool $situacao = true)
    {
        return $this->where("tipo_uasg", "G")
            ->where("compra_item_id", $idCompraItem)
            ->where("situacao", $situacao)
            ->first();
    }
    
    public function getItemUnidadeCaronaAdesao(int $compraItemId, int $unidadeId, int $fornecedorId)
    {
        return $this->where(['situacao' => true, 'tipo_uasg' => 'C'])
                    ->where('compra_item_id', $compraItemId)
                    ->where('unidade_id', $unidadeId)
                    ->where('fornecedor_id', $fornecedorId)
                    ->first();
    }
}
