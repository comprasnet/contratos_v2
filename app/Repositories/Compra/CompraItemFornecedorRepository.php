<?php

namespace App\Repositories\Compra;

use App\Models\CompraItemFornecedor;
use Illuminate\Support\Facades\DB;

class CompraItemFornecedorRepository extends CompraItemFornecedor
{
    /**
     * Método responsável em retornar as informações de um item de fornecedor
     */
    public function getItemFornecedor(int $idItemFornecedor)
    {
        return $this->find($idItemFornecedor);
    }

    public function getValorTotalItemFornecedor(array $itens)
    {
        $valorNegociado = $this->whereIn('id', $itens)
            ->selectRaw('SUM(
            CASE
                WHEN percentual_maior_desconto > 0
                    THEN TO_CHAR(
                        compra_item_fornecedor.valor_unitario *
                        ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100),
                        \'FM999999999.0000\'
                    )::float * compra_item_fornecedor.quantidade_homologada_vencedor
                ELSE 
                    compra_item_fornecedor.valor_negociado
            END
        ) as valor_negociado')
            ->first();

        return $valorNegociado->valor_negociado;
    }
}
