<?php

namespace App\Repositories\Compra;

use App\Models\CompraItem;
use Illuminate\Support\Facades\DB;

class CompraItemRepository extends CompraItem
{
    /**
     * Retorna a relação de fornecedores homologados para um item de compra
     *
     * @param integer $compraItemId
     * @return Object Collection com a relação de fornecedores
     */
    public function getFornecedoresHomologadosItem(int $compraItemId)
    {
        return CompraItem::select(
            DB::raw("CONCAT(fornecedores.cpf_cnpj_idgener,' - ', fornecedores.nome) fornecedor"),
            'compra_items.compra_id',
            DB::raw('compra_item_fornecedor.id compra_item_fornecedor_id'),
            'compra_item_fornecedor.quantidade_homologada_vencedor',
            'compra_item_fornecedor.valor_unitario',
            'compra_item_fornecedor.valor_negociado',
            'compra_item_fornecedor.quantidade_empenhada'
        )

            ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
            ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
            ->leftJoin(
                'compra_item_minuta_empenho',
                'compra_item_minuta_empenho.compra_item_id',
                '=',
                'compra_item_fornecedor.compra_item_id'
            )
            ->leftjoin('minutaempenhos', 'minutaempenhos.id', 'compra_item_minuta_empenho.minutaempenho_id')
            ->leftjoin('codigoitens', function ($join) {
                $join->on('codigoitens.codigo_id', '=', 'minutaempenhos.situacao_id')
                    ->where('codigoitens.descres', '=', 'EMITIDO');
            })
            ->where('compra_item_fornecedor.compra_item_id', $compraItemId)
            ->where('compra_item_fornecedor.situacao', '=', true)
            ->orderBy('compra_item_fornecedor.created_at')
            #->dd()
            ->get();
    }

    /**
     * Retorna relação de unidades participantes para um item de compra
     * Os valores são retornados em vários formatos para que a função
     *
     * @param integer $compraItemId
     * @return Object Collection com a relação de unidades
     */
    public function getUnidadesParticipantesItem(int $compraItemId)
    {
        return CompraItem::select(
            'unidades.id',
            DB::raw("CONCAT(unidades.codigo,' - ', nome) codigo_nome"),
            DB::raw("CONCAT(unidades.codigo,' - ', nomeresumido) codigo_nomeresumido"),
            'nome',
            'nomeresumido',
            'tipo_uasg',
            'quantidade_autorizada',
            'quantidade_saldo'
        )
            ->join('compra_item_unidade', function ($join) {
                $join->on('compra_item_unidade.compra_item_id', '=', 'compra_items.id')
                    ->where('compra_item_unidade.situacao', true);
            })
            ->join('unidades', 'unidades.id', 'compra_item_unidade.unidade_id')

            ->where('compra_items.situacao', true)
            ->where('compra_items.id', $compraItemId)

            ->orderBy('compra_item_unidade.created_at')
            #->dd()
            ->get();
    }


    public function getAtasDoItemDeCompra(int $compraItemId)
    {
        
        return CompraItem::select(
            'arp.*',
            DB::raw("CONCAT(arp.numero,'/', arp.ano) numero_ano"),
            DB::raw('unidades.codigo codigo_unidade'),
            'fornecedores.nome as nome_fornecedor',
            'fornecedores.cpf_cnpj_idgener',
            DB::raw("CONCAT(fornecedores.cpf_cnpj_idgener,' - ', fornecedores.nome) fornecedor"),
            'compra_item_fornecedor.valor_unitario',
            'compra_item_fornecedor.quantidade_homologada_vencedor as quantidade',
            //'compra_item_unidade.quantidade_autorizada'
            //'compra_items.qtd_total as quantidade_autorizada'
        )
            ->join('arp', 'arp.compra_id', 'compra_items.compra_id')
            ->join('arp_item', 'arp_item.arp_id', 'arp.id')
            ->join('compra_item_fornecedor', 'compra_item_fornecedor.id', 'arp_item.compra_item_fornecedor_id')
            ->join('unidades', 'unidades.id', 'arp.unidade_origem_id')
            ->join('fornecedores', 'fornecedores.id', 'compra_item_fornecedor.fornecedor_id')
            ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', 'compra_item_fornecedor.compra_item_id')
            ->where('compra_items.situacao', true)
            ->where('compra_item_fornecedor.situacao', true)
            ->where('arp.rascunho', false)
            ->where('compra_item_fornecedor.compra_item_id', $compraItemId)
            
            ->groupBy([
                'arp.id',
                'unidades.codigo',
                'fornecedores.nome',
                'fornecedores.cpf_cnpj_idgener',
                'compra_item_fornecedor.valor_unitario',
                'quantidade'
            ])
            ->get();
    }
    
    public function getItemPorIdCompraNumeroItem(int $idCompra, string $numeroCompra, bool $ativo = true)
    {
        return CompraItem::where('compra_id', $idCompra)
            ->where('numero', $numeroCompra)
            ->where('situacao', $ativo)
            ->first();
    }
}
