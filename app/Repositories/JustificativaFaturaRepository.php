<?php

namespace App\Repositories;

use App\Models\Justificativafatura;
use Illuminate\Support\Facades\DB;

class JustificativaFaturaRepository
{
    public function retornaJustificativasRepository()
    {
        $dados = Justificativafatura::select(
            'id',
            DB::raw("CONCAT(nome, ' - ', LEFT(descricao, 80)) as descricao")
        );

        return $dados->pluck('descricao', 'id')->toArray();
    }
}
