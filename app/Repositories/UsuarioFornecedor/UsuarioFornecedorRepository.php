<?php

namespace App\Repositories\UsuarioFornecedor;

use App\Models\BackpackUser;
use App\Models\Fornecedor;
use App\Models\User;

class UsuarioFornecedorRepository
{
    /**
     * Verifica se um usuário existe com o nome e CPF fornecidos.
     *
     * @param string $name O nome do usuário.
     * @param string $cpf O CPF do usuário.
     * @return \App\Models\BackpackUser|null Retorna uma instância de User se encontrado, ou null caso contrário.
     */
    public function verificarUsuarioRepository(string $cpf): ?BackpackUser
    {
        return BackpackUser::where('cpf', $cpf)
            ->first();
    }

    /**
     * Cadastra um usuário fornecedor.
     *
     * @param array $dados Os dados do usuário fornecedor.
     *   - 'name' string O nome do usuário.
     *   - 'cpf' string O CPF do usuário.
     *   - 'email' string O email do usuário.
     *   - 'telefone' string O número de telefone do usuário.
     *   - 'administrador' bool Indica se o usuário é um administrador do fornecedor.
     *   - 'password' string A senha do usuário.
     * @return \App\Models\BackpackUser|null Retorna uma instância de BackpackUser
     *  representando o usuário cadastrado ou null em caso de falha.
     */
    public function cadastrarUsuarioFornecedorRepository(array $dados): ?BackpackUser
    {
        //dd($dados);
        $backpackUser = new BackpackUser([
            'name' => $dados['name'],
            'cpf' => $dados['cpf'],
            'email' => $dados['email'],
            'password' => $dados['password'],
            'telefone' => $dados['telefone'],
            'acesso_compras_fornecedor' => $dados['acesso_compras_fornecedor']
        ]);

        // Salvando o usuário
        if ($backpackUser->save()) {
            return $backpackUser;
        }

        return null;
    }

    /**
     * Salva o campo de acesso de compras para fornecedor no usuário do Backpack.
     *
     * @param BackpackUser $backpackUser O usuário do Backpack.
     * @return void
     */
    public function salvarCampoAcessoComprasFornecedor(BackpackUser $backpackUser): void
    {
        $backpackUser->acesso_compras_fornecedor = true;
        $backpackUser->save();
    }

    /**
     * Recupera as informações do administrador do fornecedor a partir do pivô.
     *
     * @param BackpackUser $user O usuário do Backpack.
     * @param int $fornecedorId O ID do fornecedor.
     * @return mixed|null O objeto do pivô contendo as informações
     * do administrador do fornecedor ou null se não encontrado.
     */
    public function recuperarInformacoesFornecedorAdministradorPivot(BackpackUser $user, int $fornecedorId)
    {
        $administradorFornecedor = $user->fornecedores->where('id', $fornecedorId)
            ->first()
            ->pivot;

        return $administradorFornecedor;
    }

    /**
     * Verifica se o usuário é administrador ou preposto do fornecedor logado.
     *
     * @param BackpackUser $user O usuário do Backpack.
     * @param int $fornecedorId O ID do fornecedor.
     * @return bool|null True se o usuário for administrador, False se for preposto, ou null se não encontrado.
     */
    public function verificaAdministradorOuPrespostoFornecedorLogado(BackpackUser $user, int $fornecedorId): ?bool
    {
        $administradorFornecedor = $this->recuperarInformacoesFornecedorAdministradorPivot($user, $fornecedorId);

        return $administradorFornecedor->administrador_fornecedor;
    }

    /**
     * Verifica se o usuário tem uma ug primaria, para saber se é
     * servidor publico cadastrado no contratos.gov ou apenas usuario fornecedor.
     *
     * @param BackpackUser $user O usuário do Backpack.
     * @return bool True se o usuário tiver uma ugprimaria, False caso contrário.
     */
    public function verificarSeUserTemUgprimariaRepository(BackpackUser $user): bool
    {
        if ($user->ugprimaria) {
            return true;
        }
        return false;
    }

    /**
     * Altera o valor do campo de acesso de compras para fornecedor no usuário.
     *
     * @param BackpackUser $user O usuário do Backpack.
     * @return void
     */
    public function mudaValorAcessoComprasFornecedorRepsitory(BackpackUser $user): void
    {
        $user->acesso_compras_fornecedor = false;
        $user->save();
    }

    /**
     * Atualiza o status de administrador ou preposto para o usuário Fornecedor.
     *
     * @param BackpackUser $user O usuário do Backpack.
     * @param Fornecedor $fornecedor O objeto Fornecedor associado.
     * @param bool $administradorFornecedor O status de administrador do fornecedor.
     * @return void
     */
    public function updateAdministradorFornecedorRepository(
        BackpackUser $user,
        Fornecedor $fornecedor,
        bool $administradorFornecedor
    ): void {
        $user->fornecedores()
            ->updateExistingPivot(
                $fornecedor->id,
                ['administrador_fornecedor' => $administradorFornecedor]
            );
    }

    public function verificaSeUsuarioTemOutroFornecedorRepository(int $idUser, int $idFornecedor): bool
    {
        $user = BackpackUser::find($idUser);

        $exists = $user->fornecedores()
            ->wherePivot('fornecedor_id', $idFornecedor)
            ->exists();

        return $exists;
    }
}
