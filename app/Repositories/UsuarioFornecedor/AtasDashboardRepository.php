<?php

namespace App\Repositories\UsuarioFornecedor;

use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use App\Models\Arp;
use App\Models\Contrato;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class AtasDashboardRepository
{
    use UsuarioFornecedorTrait;

    public function queryBaseAtas(int $fornecedorId)
    {
        $hoje = Carbon::now()->toDateString();

        return  Arp::join('arp_item', function ($join) {
            $join->on('arp.id', 'arp_item.arp_id')
                ->whereNull('arp_item.deleted_at');
        })
            ->join('compra_items', function ($join) {
                $join->on('compra_items.compra_id', '=', 'arp.compra_id')
                    ->where('compra_items.situacao', true)
                    ->whereNull('compra_items.deleted_at');
            })
            ->join('compra_item_fornecedor', function ($join) {
                $join->on('compra_item_fornecedor.compra_item_id', 'compra_items.id')
                    ->whereColumn('compra_item_fornecedor.id', 'arp_item.compra_item_fornecedor_id')
                    ->where('compra_item_fornecedor.situacao', true);
            })
            ->join('unidades', 'unidades.id', 'arp.unidade_origem_id')
            ->join('codigoitens', function ($join) {
                $join->on('codigoitens.id', 'arp.tipo_id')
                    ->whereNotIn('codigoitens.descricao', ['Em elaboração', 'Cancelada']);
            })
            ->where('compra_item_fornecedor.fornecedor_id', $fornecedorId)
            ->whereRaw("'{$hoje}' between vigencia_inicial  and  vigencia_final");
    }
    public function retornaAtasPorUnidadeChart(int $fornecedorId, bool $tipoAcesso)
    {
        $retorno = $this->queryBaseAtas($fornecedorId);
        $retorno = $retorno->select(
            DB::raw("CONCAT(unidades.codigo, '-', unidades.nomeresumido) as unidade_codigo"),
            DB::raw("COUNT(distinct arp.id) as num_atas")
        );
        
        $retorno = $retorno->groupBy('unidades.codigo', 'unidades.nomeresumido')
           ->get();

        return $retorno;
    }

    public function retornaTotalContratadoAtasFornecedor(int $fornecedorId, bool $tipoAcesso)
    {
        $retorno = $this->queryBaseAtas($fornecedorId)->groupBy('arp.id')->select('arp.valor_total');
        
        $somatorio = 0;
        
        $retorno->each(function ($arp) use (&$somatorio) {
            $somatorio += $arp->valor_total;
        });
        
        return $somatorio;
    }
    
    public function queryBaseTodasAtas(int $fornecedorId)
    {
        return  Arp::join('arp_item', function ($join) {
                $join->on('arp.id', 'arp_item.arp_id')->whereNull('arp_item.deleted_at');
        })
            ->join('compra_items', function ($join) {
                $join->on('arp.compra_id', '=', 'compra_items.compra_id')->where('compra_items.situacao', true);
            })
            ->join('compra_item_fornecedor', function ($join) {
                $join->on('compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
                    ->where('compra_item_fornecedor.situacao', true)
                    ->whereColumn('compra_item_fornecedor.id', 'arp_item.compra_item_fornecedor_id');
            })
            ->join('unidades', 'unidades.id', 'arp.unidade_origem_id')
            ->join('codigoitens', function ($query) {
                $query->on('codigoitens.id', 'arp.tipo_id')
                    ->whereNotIn('codigoitens.descricao', ['Em elaboração', 'Cancelada']);
            })
            ->where('compra_item_fornecedor.fornecedor_id', $fornecedorId);
    }
    
    public function buscaQtdContratoPorPeriodoVencimento30Dias(int $fornecedorId, bool $tipoAcesso)
    {
        $dataMenos30Dias = Carbon::now()->subDays(30)->toDateString();
        $hoje = Carbon::now()->toDateString();
        
        $retorno = $this->queryBaseTodasAtas($fornecedorId);
        
        $retorno = $retorno->whereRaw("arp.vigencia_final between '$dataMenos30Dias' and '$hoje'");
        
        $retorno = $retorno->groupBy('arp.id');
        
        $retorno = $retorno->selectRaw('COUNT(distinct arp.id) as qtd_ata')->get()->count();
        
        return $retorno;
    }
    
    public function buscaQtdContratoPorPeriodoVencimento3090Dias(int $fornecedorId, bool $tipoAcesso)
    {
        $retorno = $this->queryBaseTodasAtas($fornecedorId);
        
        $retorno = $this->filtroBaseQueryQtdAtaPorPeriodoVencimento($retorno, 30, 90);
        
        $retorno = $retorno->groupBy('arp.id');
        
        $retorno = $retorno->selectRaw('COUNT(distinct arp.id) as qtd_ata')->get()->count();

        return $retorno;
    }
    
    public function buscaQtdContratoPorPeriodoVencimento90180Dias(int $fornecedorId, bool $tipoAcesso)
    {
        $retorno = $this->queryBaseTodasAtas($fornecedorId);
        
        $retorno = $this->filtroBaseQueryQtdAtaPorPeriodoVencimento($retorno, 90, 180);
        
        $retorno = $retorno->groupBy('arp.id');
        
        $retorno = $retorno->selectRaw('COUNT(distinct arp.id) as qtd_ata')->get()->count();

        return $retorno;
    }
    
    private function filtroBaseQueryQtdAtaPorPeriodoVencimento(
        Builder $atas,
        int $qtdDiasReduzir,
        int $qtdDiasAumentar
    ) {
        $dataMenos30Dias = Carbon::now()->addDays($qtdDiasReduzir)->toDateString();
        $dataMais90Dias = Carbon::now()->addDays($qtdDiasAumentar)->toDateString();
        
        return $atas->whereRaw("arp.vigencia_final between '$dataMenos30Dias' and '$dataMais90Dias'");
    }
}
