<?php

namespace App\Repositories\UsuarioFornecedor;

use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use App\Models\Contrato;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ContratosDashboardRepository
{

    use UsuarioFornecedorTrait;

    public function retornaContratosPorUnidadeChart(int $fornecedorId, bool $tipoAcesso)
    {
        $hoje = Carbon::now()->toDateString();
        //adicionarQueryFiltroPrepostoForaDoSetup
        $retorno =  Contrato::select(
            DB::raw("CONCAT(unidades.codigo, '-', unidades.nomeresumido) as unidade_codigo"),
            DB::raw("COUNT(contratos.id) as num_contratos")
        )
            //->leftJoin('contratopreposto', 'contratos.id', '=', 'contratopreposto.contrato_id')
            ->join('unidades', 'contratos.unidade_id', '=', 'unidades.id')
            ->where('fornecedor_id', $fornecedorId)
            ->where('contratos.elaboracao', false)
            ->whereRaw("('{$hoje}' between vigencia_inicio  and  vigencia_fim or vigencia_fim is null)");
        $retorno  = $this->adicionarQueryFiltroPrepostoForaDoSetup($tipoAcesso, $retorno);
        $retorno = $retorno->groupBy('unidades.codigo', 'unidades.nomeresumido')
            ->get();

        return $retorno;
    }

    public function calculaTotalContratadoPorFornecedor(int $fornecedorId, bool $tipoAcesso)
    {
        return $this->queryBaseContrato($fornecedorId, $tipoAcesso)->sum('valor_global');
    }
    
    private function queryBaseContrato(int $fornecedorId, bool $tipoAcesso)
    {
        $hoje = Carbon::now()->toDateString();
        $totalValorGlobal = Contrato::join('unidades', 'unidades.id', '=', 'contratos.unidade_id')
            ->join('orgaos', 'orgaos.id', '=', 'unidades.orgao_id')
            ->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id')
            ->where('contratos.situacao', true)
            ->where('contratos.elaboracao', false)
            ->where('unidades.sigilo', false)
            ->where('fornecedores.id', $fornecedorId)
            ->whereRaw("('{$hoje}' between vigencia_inicio  and  vigencia_fim or vigencia_fim is null)");
        
        return $this->adicionarQueryFiltroPrepostoForaDoSetup($tipoAcesso, $totalValorGlobal);
    }
    public function buscaQtdContratoPorPeriodoVencimento30Dias(int $fornecedorId, bool $tipoAcesso)
    {
        $contratos = $this->queryBaseContrato($fornecedorId, $tipoAcesso);
        
        $data = Carbon::now()->subDays(30)->toDateString();

        $contratos->where('contratos.vigencia_fim', '<', $data);
        
        return $contratos->count();
    }
    
    private function filtroBaseQueryQtdContratoPorPeriodoVencimento(
        Builder $contratos,
        int $qtdDiasInicio,
        int $qtdDiasFim
    ) {
        $dataInicio = Carbon::now()->addDays($qtdDiasInicio)->toDateString();
        $dataFim = Carbon::now()->addDays($qtdDiasFim)->toDateString();
        
        return $contratos->whereRaw("contratos.vigencia_fim between '$dataInicio' and '$dataFim'");
    }
    
    public function buscaQtdContratoPorPeriodoVencimento3090Dias(int $fornecedorId, bool $tipoAcesso)
    {
        $contratos = $this->queryBaseContrato($fornecedorId, $tipoAcesso);
        
        $contratos = $this->filtroBaseQueryQtdContratoPorPeriodoVencimento($contratos, 30, 90);
        
        return $contratos->count();
    }
    
    public function buscaQtdContratoPorPeriodoVencimento90180Dias(int $fornecedorId, bool $tipoAcesso)
    {
        $contratos = $this->queryBaseContrato($fornecedorId, $tipoAcesso);
        
        $contratos = $this->filtroBaseQueryQtdContratoPorPeriodoVencimento($contratos, 90, 180);
        
        return $contratos->count();
    }
}
