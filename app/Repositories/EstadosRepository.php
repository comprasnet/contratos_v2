<?php

namespace App\Repositories;

use App\Models\Estado;

class EstadosRepository extends Estado
{
    
    /**
     * @return array
     */
    public function getAllUF(): array
    {
        return collect($this->all())->pluck('sigla')->unique()->toArray();
    }
}
