<?php

namespace App\Repositories;

use App\Models\Arp;
use App\Models\Contrato;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class ContratoRepository
{
    /**
     * Obtém contratos para usuários administradores, fornecedores e prepostos.
     *
     * @param int $fornecedorId ID do fornecedor.
     * @param string|null $filterContratoFornecedor Filtro para pesquisa de contratos.
     *
     * @return Collection
     */
    public function getContratosUsuarioFornecedorAdministrador(int $fornecedorId, ?string $filterContratoFornecedor)
    {
        return $this->sqlBaseContratosUsuarioFornecedor($fornecedorId, $filterContratoFornecedor)->paginate(10);
    }

    /**
     * Obtém contratos para usuários prepostos de fornecedores.
     *
     * @param int $fornecedorId ID do fornecedor.
     * @param string $cpf CPF do preposto.
     * @param string|null $filterContratoFornecedor Filtro para pesquisa de contratos.
     *
     * @return LengthAwarePaginator
     */
    public function getContratosUsuarioFornecedorPreposto(
        int $fornecedorId,
        string $cpf,
        ?string $filterContratoFornecedor
    ): LengthAwarePaginator {
        return $this->sqlBaseContratosUsuarioFornecedor($fornecedorId, $filterContratoFornecedor)
        ->join('contratopreposto', 'contrato_id', 'contratos.id')
            ->where('contratopreposto.cpf', $cpf)
            ->where('contratopreposto.situacao', true)
            ->paginate(10);
    }

    /**
     * Constrói a base da consulta para contratos de usuários fornecedores.
     *
     * @param int $fornecedorId ID do fornecedor.
     * @param string|null $filterContratoFornecedor Filtro para pesquisa de contratos.
     *
     * @return Builder
     */
    private function sqlBaseContratosUsuarioFornecedor(int $fornecedorId, ?string $filterContratoFornecedor)
    {
        $hoje = Carbon::now()->toDateString();

        return Contrato::join('fornecedores', function ($query) use ($fornecedorId) {
            $query->on('fornecedor_id', 'fornecedores.id')
            ->where('fornecedor_id', $fornecedorId);
        })
            ->join('unidades', 'unidades.id', 'contratos.unidade_id')
            ->where('contratos.situacao', true)
            ->where(function ($query) use ($filterContratoFornecedor) {
                $query->whereRaw("contratos.numero ilike '%$filterContratoFornecedor%'")
                    ->orWhere("unidades.codigo", 'ilike', "%$filterContratoFornecedor%")
                    //->orWhere("unidades.nomeresumido", 'ilike', "%$filterContratoFornecedor%");
                    ->orWhere("unidades.nome", 'ilike', "%$filterContratoFornecedor%");
            })
            ->whereRaw("('{$hoje}' between vigencia_inicio  and  vigencia_fim or vigencia_fim is null)")
            ->selectRaw("contratos.id,
                contratos.numero,
                contratos.vigencia_inicio,
                contratos.vigencia_fim,
                contratos.objeto,
                -- CONCAT(unidades.codigo, '-', unidades.nomeresumido) as unidade_contrato,
                CONCAT(unidades.codigo, '-', unidades.nome) as unidade_contrato,
                CONCAT(fornecedores.cpf_cnpj_idgener, '-', fornecedores.nome) as fornecedor_descricao");
    }

    /**
     * Obtém atas para usuários administradores e fornecedores.
     *
     * @param int $fornecedorId ID do fornecedor.
     * @param string|null $filterAtaFornecedor Filtro para pesquisa de atas.
     *
     * @return LengthAwarePaginator
     */
    public function getAtasUsuarioFornecedorAdministrador(int $fornecedorId, ?string $filterAtaFornecedor)
    {
        return $this->sqlBaseAtasPorFornecedor($fornecedorId, $filterAtaFornecedor)->paginate(10);
    }

    /**
     * Obtém atas para usuários prepostos de fornecedores.
     *
     * @param int $fornecedorId ID do fornecedor.
     * @param string $cpf CPF do preposto.
     * @param string|null $filterContratoFornecedor Filtro para pesquisa de contratos.
     *
     * @return LengthAwarePaginator
     */
    public function getAtasUsuarioFornecedorPreposto(int $fornecedorId, string $cpf, ?string $filterContratoFornecedor)
    {
        return $this->sqlBaseAtasPorFornecedor($fornecedorId, $filterContratoFornecedor)->paginate(10);
    }

    /**
     * Constrói a base da consulta para atas por fornecedor.
     *
     * @param int $fornecedorId ID do fornecedor.
     * @param string|null $filterAtaFornecedor Filtro para pesquisa de atas.
     *
     * @return Builder
     */
    public static function sqlBaseAtasPorFornecedor(int $fornecedorId, ?string $filterAtaFornecedor)
    {
        $hoje = Carbon::now()->toDateString();

        return Arp::join('arp_item', 'arp_item.arp_id', '=', 'arp.id')
            ->join('compra_item_fornecedor', function ($query) {
                $query->on('compra_item_fornecedor.id', 'arp_item.compra_item_fornecedor_id')
                    ->where('compra_item_fornecedor.situacao', true)
                    ->whereNull('compra_item_fornecedor.deleted_at');
            })
            ->join('compra_items', 'compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')
            ->join('compras', 'compras.id', '=', 'compra_items.compra_id')
            ->join('unidades', 'unidades.id', '=', 'arp.unidade_origem_id')
            ->join('codigoitens', function ($join) {
                $join->on('codigoitens.id', 'arp.tipo_id')
                    ->whereNotIn('codigoitens.descricao', ['Em elaboração', 'Cancelada']);
            })
            ->where('compra_item_fornecedor.fornecedor_id', $fornecedorId)
            ->where(function ($query) use ($filterAtaFornecedor) {
                $query->whereRaw("CONCAT(arp.numero, '/' ,ano) ilike '%$filterAtaFornecedor%'")
                    ->orWhere("unidades.codigo", 'ilike', "%$filterAtaFornecedor%")
                    ->orWhere("unidades.nomeresumido", 'ilike', "%$filterAtaFornecedor%");
            })
            ->whereNull('arp.deleted_at')
            ->whereNull('arp_item.deleted_at')
            ->whereRaw("'{$hoje}' between vigencia_inicial  and  vigencia_final")
            ->selectRaw(
                "CONCAT(arp.numero, '/', arp.ano) as numero_ano_ata,
            arp.id,
            CONCAT(unidades.codigo, '-', unidades.nome) as unidade_gerenciadora_da_ata,
            arp.vigencia_final as vigencia_final,
            arp.objeto"
            )
            ->groupBy('arp.id', 'unidades.codigo', 'unidades.nome')
            ->orderByDesc('arp.numero');
    }

    public function retornaFiltroContratosFornecedorRepository()
    {
        return Contrato::select(
            \Illuminate\Support\Facades\DB::raw("LEFT(CONCAT(numero, ' - ', objeto), 80) AS descricao"),
            'numero'
        );
    }
}
