<?php

namespace App\Repositories;

use App\Models\Tipolistafatura;

class TipoListaFaturaRepository
{
    public function retornaTiposListaRepository()
    {
        $dados = Tipolistafatura::select('nome as descricao', 'id');

        return $dados->pluck('descricao', 'id')->toArray();
    }
}
