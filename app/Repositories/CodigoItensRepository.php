<?php

namespace App\Repositories;

use App\Models\CodigoItem;

class CodigoItensRepository
{
    public static function getIdByDescricao($descricao)
    {
        $codigoItem = CodigoItem::query()
            ->where('descricao', '=', $descricao)
            ->first();
        if ($codigoItem) {
            return $codigoItem->id;
        }
        return false;
    }

    public static function getIdByDescres($descres)
    {
        $codigoItem = CodigoItem::query()
            ->where('descres', '=', $descres)
            ->first();
        if ($codigoItem) {
            return $codigoItem->id;
        }
        return false;
    }
}
