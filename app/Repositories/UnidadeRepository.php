<?php

namespace App\Repositories;

use App\Models\Unidade;

class UnidadeRepository extends Unidade
{

    public function getUnidadePorCodigo(string $codigo, bool $situacao = true)
    {
        return $this->where("codigo", $codigo)
            ->where("situacao", $situacao)
            ->first();
    }
    
    public function getUnidadePorId(int $idUnidade)
    {
        return $this->find($idUnidade);
    }
    
    public function salvarUnidade(array $data)
    {
        return $this->create($data);
    }
}
