<?php

namespace App\Repositories\ArquivoGenerico;

use App\Models\ArquivoGenerico;

class ArquivoGenericoRepository extends ArquivoGenerico
{
    /**
     * Método responsável em recuperar o registro no banco de dados
    */
    public function getArquivoGenerico(int $id, string $classModel)
    {
        return $this->where("arquivoable_id", $id)
                    ->where("arquivoable_type", $classModel)->first();
    }

    /**
     * Método responsável em recuperar o registro único na tabela
     * arquivo_generico
     */
    public function getArquivoGenericoUnico(int $id)
    {
        return $this->find($id);
    }

    /**
     * Método responsável em retornar a URL do arquivo
     */
    public function getURLArquivoGenerico(int $id)
    {
        return $this->getArquivoGenericoUnico($id)->url;
    }

    /**
     * Método responsável em deletar o arquivo no banco de dados
     */
    public function deleteRegistroArquivoGenerico(int $id)
    {
        return $this->getArquivoGenericoUnico($id)->delete();
    }
}
