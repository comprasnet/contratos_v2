<?php

namespace App\Repositories;

use App\Helper\Date;
use App\Models\Arp;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class ArpsDashboardRepository
{
    private $vigenciaInicial;
    private $vigenciaFinal;
    private $uasgs;
    private $numeroAno;

    private $filterAta;

    /**
     * @return mixed
     */
    public function getVigenciaInicial()
    {
        return $this->vigenciaInicial;
    }

    /**
     * @param mixed $vigenciaInicial
     * @return ArpsDashboardRepository
     */
    public function setVigenciaInicial($vigenciaInicial)
    {
        $this->vigenciaInicial = $vigenciaInicial;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVigenciaFinal()
    {
        return $this->vigenciaFinal;
    }

    /**
     * @param mixed $vigenciaFinal
     * @return ArpsDashboardRepository
     */
    public function setVigenciaFinal($vigenciaFinal)
    {
        $this->vigenciaFinal = $vigenciaFinal;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUasgs()
    {
        return $this->uasgs;
    }

    /**
     * @param mixed $uasgs
     * @return ArpsDashboardRepository
     */
    public function setUasgs($uasgs)
    {
        $this->uasgs = is_null($uasgs) ? [] : $uasgs;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumeroAno()
    {
        return $this->numeroAno;
    }

    /**
     * @param mixed $numeroAno
     * @return ArpsDashboardRepository
     */
    public function setNumeroAno($numeroAno)
    {
        $this->numeroAno = $numeroAno;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFilterAta()
    {
        return $this->filterAta;
    }

    /**
     * @param mixed $filterAta
     * @return ArpsDashboardRepository
     */
    public function setFilterAta($filterAta)
    {
        $this->filterAta = $filterAta;
        return $this;
    }



    public function getArpsByFilter()
    {
        $uri = $_SERVER['REQUEST_URI'] ?? null;

        if (strpos($uri, '/transp') !== false) {
            $cacheKey = 'lista_atas_registro_preco';
            $userCache = Cache::remember($cacheKey, 18, function () {
                $arps = Arp::query()
                    ->join('unidades', 'arp.unidade_origem_id', '=', 'unidades.id')
                    ->where("rascunho", false)
                    ->orderBy('arp.created_at', 'DESC');
    
                $arps = $this->getFilterByAttrs($arps);
    
                return $arps->select([
                    'arp.*',
                    DB::raw("unidades.codigo || ' ' || unidades.nomeresumido as unidade"),
                    'ciSituacao.descricao as situacao_ata'
                ])->paginate(10);
            });
    
            return $userCache;
        } else {
            $arps = Arp::query()
                ->join('unidades', 'arp.unidade_origem_id', 'unidades.id')
                ->where("rascunho", false)
                ->orderBy('arp.created_at', 'DESC');
    
            $arps = $this->getFilterByAttrs($arps);
    
            return $arps->select([
                'arp.*',
                DB::raw("unidades.codigo || ' ' || unidades.nomeresumido as unidade"),
                'ciSituacao.descricao as situacao_ata'
            ])->paginate(10);
        }
    }

    public function arpsTotalServico()
    {
        $uri = $_SERVER['REQUEST_URI'] ?? null;

        if (strpos($uri, '/transp') !== false) {
            $cacheKey = 'total_servico';

            $userCache = Cache::remember($cacheKey, 18, function () {
                $arpsServico = Arp::join('compra_items', 'compra_items.compra_id', 'arp.compra_id')
                    ->join('codigoitens', 'codigoitens.id', 'compra_items.tipo_item_id')
                    ->where('codigoitens.descricao', 'Serviço');

                $arpsServico = $this->getFilterByAttrs($arpsServico);
                $arpsServico = $arpsServico->groupBy('arp.id');

                return $arpsServico->sum('arp.valor_total');
            });

            return $userCache;
        } else {
            $arpsServico = Arp::join('compra_items', 'compra_items.compra_id', 'arp.compra_id')
                ->join('codigoitens', 'codigoitens.id', 'compra_items.tipo_item_id')
                ->where('codigoitens.descricao', 'Serviço');

            $arpsServico = $this->getFilterByAttrs($arpsServico);
            $arpsServico = $arpsServico->groupBy('arp.id');

            return $arpsServico->sum('arp.valor_total');
        }
    }

    public function arpsTotalMaterial()
    {
        $uri = $_SERVER['REQUEST_URI'] ?? null;
        $cacheKey = 'total_material';
    
        if (strpos($uri, '/transp') !== false) {
            return Cache::remember($cacheKey, 1800, function () {
                $arpsMaterial = Arp::join('compra_items', 'compra_items.compra_id', 'arp.compra_id')
                    ->join('codigoitens', 'codigoitens.id', 'compra_items.tipo_item_id')
                    ->where('codigoitens.descricao', 'Material');
                    
                $arpsMaterial = $this->getFilterByAttrs($arpsMaterial);
                $arpsMaterial = $arpsMaterial->groupBy('arp.id');
    
                return $arpsMaterial->sum('arp.valor_total');
            });
        } else {
            $arpsMaterial = Arp::join('compra_items', 'compra_items.compra_id', 'arp.compra_id')
                ->join('codigoitens', 'codigoitens.id', 'compra_items.tipo_item_id')
                ->where('codigoitens.descricao', 'Material');
                
            $arpsMaterial = $this->getFilterByAttrs($arpsMaterial);
            $arpsMaterial = $arpsMaterial->groupBy('arp.id');
    
            return $arpsMaterial->sum('arp.valor_total');
        }
    }
    

    public function getValorTotalArp()
    {
        $uri = $_SERVER['REQUEST_URI'] ?? null;
        $isTransparencia = strpos($uri, '/transp') !== false;
    
        if ($isTransparencia) {
            $cacheKey = 'total_arp';
            $valorTotal = Cache::remember($cacheKey, 18, function () {
                return (float)$this->getFilterByAttrs(Arp::query())
                    ->where('ciSituacao.descricao', 'Ata de Registro de Preços')
                    ->sum('arp.valor_total');
            });
    
            return $valorTotal;
        } else {
            return (float)$this->getFilterByAttrs(Arp::query())
                ->where('ciSituacao.descricao', 'Ata de Registro de Preços')
                ->sum('arp.valor_total');
        }
    }

    public static function unidadeSession()
    {
        return \App\Models\Unidade::where('codigo', session('user_ug'))->first();
    }

    /**]
     * @param Builder $arps
     * @return Builder
     */
    private function getFilterByAttrs($arps)
    {

        if (!is_null($this->vigenciaInicial)) {
            //$vigenciaIinicial = Date::brlToDb($this->vigenciaFinal);
            $vigenciaIinicial = Carbon::createFromFormat('d/m/Y', $this->vigenciaInicial)->format('Y-m-d');
            $arps->where('arp.vigencia_inicial', '>=', $vigenciaIinicial);
        }

        if (!is_null($this->vigenciaFinal)) {
           // $vigenciaFinal = Date::brlToDb($this->vigenciaFinal);
            $vigenciaFinal = Carbon::createFromFormat('d/m/Y', $this->vigenciaFinal)->format('Y-m-d');

            $arps->where('arp.vigencia_inicial', '<=', $vigenciaFinal);
        }

        if (!empty($this->uasgs)) {
            $arps->whereIn('arp.unidade_origem_id', $this->uasgs);
        }

        if ($this->filterAta) {
            $numero = substr($this->filterAta, 0, 5);
            $ano = substr($this->filterAta, 6, 4);
           
            # Caso seja inserido a barra para filtrar pelo número e ano da ata
            if (str_contains($this->filterAta, '/')) {
                $arrayNumeroAta = explode('/', $this->filterAta);
                $numero = $arrayNumeroAta[0];
                $ano = $arrayNumeroAta[1];
            }
            
            if (is_numeric($numero)) {
                $arps->where(function ($query) use ($numero) {
                    $query->where('arp.numero', 'ILIKE', "%$numero%")
                        ->orWhere('arp.ano', $numero);
                });
            }

            if (!empty($ano) && is_numeric($ano)) {
                $arps->where('arp.ano', 'ILIKE', "%$ano%");
            }
        }
        if ($this->numeroAno) {
            $numero = substr($this->numeroAno, 0, 5);
            $ano = substr($this->numeroAno, 6, 4);
            
            if (str_contains($this->filterAta, '/')) {
                $arrayNumeroAta = explode('/', $this->filterAta);
                $numero = $this->preencherCasasDireitaEsquerda($arrayNumeroAta[0], 5);
                $ano = $this->preencherCasasDireitaEsquerda($arrayNumeroAta[1], 4);
            }
            
            if (is_numeric($numero)) {
                $arps->where('arp.numero', $numero);
            }
            
            if (!empty($ano) && is_numeric($ano)) {
                $arps->where('arp.ano', $ano);
            }
        }
        
        $arps->join(
            'codigoitens as ciSituacao',
            'ciSituacao.id',
            'arp.tipo_id'
        );
        
        return $arps;
    }
}
