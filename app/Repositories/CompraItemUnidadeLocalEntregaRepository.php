<?php

namespace App\Repositories;

use App\Models\CompraItemUnidadeLocalEntrega;

class CompraItemUnidadeLocalEntregaRepository extends CompraItemUnidadeLocalEntrega
{
    
    /**
     * @param array $data
     * @return mixed
     */
    public function insertCompraItemUnidadeLocalEntrega(array $data)
    {
        return $this->updateOrCreate(
            [
                'compra_item_unidade_id' => $data['compra_item_unidade_id'],
                'endereco_id' => $data['endereco_id'],
                'endereco_id_novo_divulgacao' => $data['endereco_id_novo_divulgacao']
            ],
            [
                'quantidade' => $data['quantidade']
            ],
        );
    }
}
