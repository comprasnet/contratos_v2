<?php

namespace App\Repositories;

use App\Models\Adesao;
use Illuminate\Support\Facades\DB;

class ArpSolicitacaoRepository
{
    private static function getQueryCounterSolicitacoesAnalise($me = false, $order = [])
    {
        $orderQueryArr = [];
        foreach ($order as $k => $item) {
            $imploded = implode(',', $item);
            $orderQueryArr[] = "WHEN c.id in ($imploded) THEN " . ($k + 1);
        }
        $orderQuery = implode(' ', $orderQueryArr);
        $query = Adesao::query()->select([
            DB::raw(" 
                    CASE 
                        {$orderQuery}
                        ELSE 99
                    END as status,
                    CASE 
                        WHEN c.id in (508,513,511) then 'Solicitações analisadas'
                        ELSE c.descricao
                    END as descricao,count(*) as qnt")
        ])
            ->join('codigoitens as c', 'c.id', 'arp_solicitacao.status')
            ->groupBy('c.id', 'c.descricao');
        if ($me) {
            $query->where('unidade_origem_id', '=', session('user_ug_id'));
        } else {
            $query->where('rascunho', false);
            $query->where('unidade_gerenciadora_id', '=', session('user_ug_id'));
        }
        return $query;
    }

    public static function getCounterSolicitacoes()
    {

        $sub = self::getQueryCounterSolicitacoesAnalise(true, [
            self::getArrayIdsCodigoItens(['Aceita','Aceito Parcial','Negada']),
            self::getArrayIdsCodigoItens(['Enviada para aceitação']),
            self::getArrayIdsCodigoItens(['Em elaboração'])
        ]);

        $query = DB::table(DB::raw("({$sub->toSql()}) as sub"))
            ->orderBy('status')
            ->groupBy('status', 'descricao')
            ->selectRaw('status, SUM(qnt) as qnt,descricao')
            ->mergeBindings($sub->getQuery());


        return $query->get();
    }


    public static function getCounterAnalises()
    {

        $sub = self::getQueryCounterSolicitacoesAnalise(false, [
            self::getArrayIdsCodigoItens(['Enviada para aceitação']),
            self::getArrayIdsCodigoItens(['Aceita','Aceito Parcial','Negada']),
            self::getArrayIdsCodigoItens(['Em elaboração'])
        ]);
        $query = DB::table(DB::raw("({$sub->toSql()}) as sub"))
            ->orderBy('status')
            ->groupBy('status', 'descricao')
            ->selectRaw('status, SUM(qnt) as qnt,descricao')
            ->mergeBindings($sub->getQuery());
        return $query->get();
    }

    public static function getCounterAnaliseAdesaoFornecedor()
    {
        $sub = self::getQueryCounterSolicitacoesAnaliseFornecedor([
            self::getArrayIdsCodigoItensByDescres(['anuencia_status_1'])
        ]);

        $query = DB::table(DB::raw("({$sub->toSql()}) as sub"))
            ->orderBy('status')
            ->groupBy('status', 'descricao')
            ->selectRaw('status, SUM(qnt) as qnt, descricao')
            ->mergeBindings($sub->getQuery());
        return $query->get();
    }

    private static function getArrayIdsCodigoItens($arr)
    {
        $ids = [];
        foreach ($arr as $item) {
            $id = CodigoItensRepository::getIdByDescricao($item);
            if ($id) {
                $ids[] = $id;
            }
        }
        return $ids;
    }
    private static function getArrayIdsCodigoItensByDescres($arr)
    {
        $ids = [];
        foreach ($arr as $item) {
            $id = CodigoItensRepository::getIdByDescres($item);
            if ($id) {
                $ids[] = $id;
            }
        }
        return $ids;
    }

    private static function getQueryCounterSolicitacoesAnaliseFornecedor($order = [])
    {
        $orderQueryArr = [];
        foreach ($order as $k => $item) {
            $imploded = implode(',', $item);
            $orderQueryArr[] = "WHEN c.id in ($imploded) THEN " . ($k + 1);
        }
        $orderQuery = implode(' ', $orderQueryArr);
        $query = Adesao::query()->select([
            DB::raw("
                    CASE
                        {$orderQuery}
                        ELSE 99
                    END as status,
                    CASE
                        WHEN c.id in (508,513,511) then 'Solicitações analisadas'
                        ELSE c.descricao
                    END as descricao,count(DISTINCT arp_solicitacao.id) as qnt")
        ])
            /*->join('codigoitens as c', 'c.id', 'arp_solicitacao.status')
            ->groupBy('c.id', 'c.descricao');*/
            ->join('codigoitens as c', 'c.id', '=', 'arp_solicitacao.status')
            ->join('compras', 'compras.id', '=', 'arp_solicitacao.compra_id')
            ->join('compra_items', function ($join) {
                $join->on('compras.id', '=', 'compra_items.compra_id')
                    ->where('compra_items.situacao', true);
            })
            ->join('compra_item_fornecedor', function ($join) {
                $join->on('compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
                    ->where('compra_item_fornecedor.situacao', true)
                    ->where('compra_item_fornecedor.fornecedor_id', session('fornecedor_id'));
            })
            ->where('c.descres', 'anuencia_status_1')
            ->groupBy('c.id', 'c.descricao');

        return $query;
    }
}
