<?php

namespace App\Repositories;

use App\Models\Enderecos;

class EnderecoRepository extends Enderecos
{
    
    /**
     * @param array $dadosEndereco
     * @return mixed
     */
    public function insertUpdateEndereco(array $dadosEndereco)
    {
        return $this->updateOrCreate($dadosEndereco);
    }
}
