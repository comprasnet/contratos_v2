<?php

namespace App\Rules;

use App\Repositories\EstadosRepository;
use Illuminate\Contracts\Validation\Rule;

class UFRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ufs = (new EstadosRepository())->getAllUF();

        return in_array($value, $ufs);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'UF não existe na base de dados';
    }
}
