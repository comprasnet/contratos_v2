<?php

namespace App\Rules;

use App\Models\ArpItem;
use App\Models\CompraItem;
use App\Repositories\Arp\ArpAdesaoRepository;
use App\Services\Arp\AdesaoService;
use Illuminate\Contracts\Validation\Rule;

class ValidarQuantidadeCaronaUnidadeSolicitanteArpAdesao implements Rule
{
    protected $unidadeOrigemId;
    protected $aquisicaoEmergencialMedicamentoMaterial;
    protected $execucaoDescentralizadaProgramaProjetoFederal;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(
        ?int $unidadeOrigemId,
        ?bool $aquisicaoEmergencialMedicamentoMaterial,
        ?bool $execucaoDescentralizadaProgramaProjetoFederal
    ) {
        $this->unidadeOrigemId = $unidadeOrigemId;
        $this->aquisicaoEmergencialMedicamentoMaterial = $aquisicaoEmergencialMedicamentoMaterial;
        $this->execucaoDescentralizadaProgramaProjetoFederal = $execucaoDescentralizadaProgramaProjetoFederal;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $adesaoService = new AdesaoService();
        $arrayItemFormatado = array();
        
        foreach ($value as $arpItemId => $quantidade) {
            $itemAta = ArpItem::find($arpItemId);
            $compraItemFornecedor =  $itemAta->item_fornecedor_compra;
            
            if (isset($arrayItemFormatado[$compraItemFornecedor->compra_item_id])) {
                $arrayItemFormatado[$compraItemFornecedor->compra_item_id]['quantidade'] += $quantidade;
            }
            
            if (!isset($arrayItemFormatado[$compraItemFornecedor->compra_item_id])) {
                $arrayItemFormatado[$compraItemFornecedor->compra_item_id]['quantidade'] = $quantidade;
            }
            
            $arrayItemFormatado[$compraItemFornecedor->compra_item_id]['compra_item_fornecedor_id']
                = $compraItemFornecedor->fornecedor_id;
        }

        $arpAdesaoRepository = new ArpAdesaoRepository();
        $itemValido = true;

        foreach ($arrayItemFormatado as $compraItemId => $dadosValidacao) {
            $itemValido = $adesaoService->itemValidoParaCaronaRule(
                $compraItemId,
                $dadosValidacao['compra_item_fornecedor_id'],
                $arpAdesaoRepository,
                $this->unidadeOrigemId,
                $this->execucaoDescentralizadaProgramaProjetoFederal,
                $this->aquisicaoEmergencialMedicamentoMaterial,
                $dadosValidacao['quantidade']
            );
            
            if (!$itemValido) {
                break;
            }
        }
        
        return $itemValido;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'O limite estabelecido pelo art. 32, I, do Decreto 11.462/2023 foi atingido pela unidade.';
    }
}
