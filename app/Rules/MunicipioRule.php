<?php

namespace App\Rules;

use App\Repositories\MunicipiosRepository;
use Illuminate\Contracts\Validation\Rule;

class MunicipioRule implements Rule
{
    private $uf = null;
    private $codigoMunicipio = null;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $uf, ?string $codigoMunicipio)
    {
        $this->uf = $uf;
        $this->codigoMunicipio = $codigoMunicipio;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $municipio = (new MunicipiosRepository())->getMunicipio($this->codigoMunicipio, $this->uf, $value);
        
        if (empty($municipio)) {
            return false;
        }
        
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Contratação com local de entrega inválido. Favor retificar no Compras.gov.br';
    }
}
