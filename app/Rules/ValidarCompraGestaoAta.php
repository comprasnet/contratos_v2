<?php

namespace App\Rules;

use App\Http\Controllers\Api\ConcentradorCompras\CompraSisrp;
use App\Http\Traits\LogTrait;
use Illuminate\Contracts\Validation\Rule;
use Exception;

class ValidarCompraGestaoAta implements Rule
{
    use LogTrait;
    protected $idUnidadeOrigem;
    protected $idModalidade;
    protected $idUnidadeOrigemCompra;
    
    protected $mensagem;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(int $idUnidadeOrigem, int $idModalidade, int $idUnidadeOrigemCompra)
    {
        $this->idUnidadeOrigem = $idUnidadeOrigem;
        $this->idModalidade = $idModalidade;
        $this->idUnidadeOrigemCompra = $idUnidadeOrigemCompra;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!strpos($value, '/') !== false) {
            $this->mensagem ='Número da compra inválido';
            return false;
        }
        
        $compraSisrp = new CompraSisrp();
        try {
            $retornoSiasg = $compraSisrp->getCompraLeiQuatozeUmTresTres(
                $value,
                $this->idUnidadeOrigem,
                $this->idModalidade,
                $this->idUnidadeOrigem,
                $this->idUnidadeOrigemCompra
            );
            
            $arrayCodigoRetornoErro = [202, 204];
            
            # Se o erro for retornado pelo SIASG
            if (in_array($retornoSiasg->codigoRetorno, $arrayCodigoRetornoErro)) {
                $this->mensagem = $retornoSiasg->message;
                return false;
            }
            
            return true;
        } catch (Exception $exception) {
            $this->mensagem = 'Erro ao consultar a compra';
            $this->inserirLogCustomizado('gestao_ata', 'error', $exception);
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagem;
    }
}
