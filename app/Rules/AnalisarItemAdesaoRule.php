<?php

namespace App\Rules;

use App\Models\Adesao;
use App\Models\ArpItem;
use App\Models\CodigoItem;
use App\Models\CompraItem;
use App\Repositories\Arp\ArpAdesaoItemRepository;
use App\Repositories\Arp\ArpAdesaoRepository;
use App\Services\Arp\AdesaoService;
use App\Services\Arp\ArpItemHistoricoService;
use Illuminate\Contracts\Validation\Rule;

class AnalisarItemAdesaoRule implements Rule
{
    protected $statusId;
    
    protected $motivoAnalise;
    
    protected $mensagem;

    protected $idAdesao;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(?array $statusId, ?array $motivoAnalise, ?int $idAdesao)
    {
        $this->statusId = $statusId;
        $this->motivoAnalise = $motivoAnalise;
        $this->idAdesao = $idAdesao;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $arpAdesaoItemRepository = new ArpAdesaoItemRepository();
        $arpItemHistoricoService = new ArpItemHistoricoService();
        $arrayItemFormatado = array();

        foreach ($value as $idItemAdesao => $quantidade) {
            $itemAdesao = $arpAdesaoItemRepository->getItemAdesaoUnico($idItemAdesao);
            $itemCanceladoRemovido =
                $arpItemHistoricoService->itemAtaCanceladoRemovido($itemAdesao->item_arp_fornecedor_id);

            if ($itemCanceladoRemovido) {
                continue;
            }
            
            $situacaoId = $this->statusId[$idItemAdesao] ?? null;

            if (empty($situacaoId)) {
                $this->mensagem = 'A situação informada do item está inválida';
                return false;
                break;
            }

            $situacaoAnalise = CodigoItem::find($situacaoId);
            $descricaoSituacao = $situacaoAnalise->descricao;

            if ($descricaoSituacao == 'Aceitar Parcialmente' && empty($quantidade)) {
                $this->mensagem =
                    'A quantidade para a situação Aceitar Parcialmente deve ser informada';
                return false;
                break;
            }

            if ($descricaoSituacao == 'Aceitar Parcialmente' && $quantidade == 0) {
                $this->mensagem =
                    'A quantidade para a situação Aceitar Parcialmente deve ser superio a zero';
                return false;
                break;
            }

            $motivo = $this->motivoAnalise[$idItemAdesao] ?? null;

            if ($descricaoSituacao == 'Aceitar Parcialmente' && empty($motivo)) {
                $this->mensagem =
                    'Para a situação Aceitar Parcialmente deve ser informado um motivo';
                return false;
                break;
            }

            if ($descricaoSituacao == 'Aceitar' && $quantidade != $itemAdesao->quantidade_solicitada
                && !empty($quantidade)
            ) {
                $this->mensagem = 'A quantidade para a situação Aceitar deve ser o total solicitado';
                return false;
                break;
            }

            $quantidadeComparacao = $itemAdesao->quantidade_anuencia_fornecedor ?? $itemAdesao->quantidade_solicitada;
            $this->mensagem = $itemAdesao->quantidade_anuencia_fornecedor
                ? 'A quantidade para a situação Aceitar Parcialmente deve ser ' .
                'inferior a Quantidade Aceita pelo Fornecedor'
                : 'A quantidade para a situação Aceitar Parcialmente deve ser inferior ao total solicitado';
            if ($descricaoSituacao == 'Aceitar Parcialmente' && $quantidade >= $quantidadeComparacao) {
                $this->mensagem;
                return false;
                break;
            }

            if ($descricaoSituacao == 'Negar' && (!empty($quantidade) && $quantidade > 0)) {
                $this->mensagem =
                    'A quantidade para a situação Negar deve zerada';
                return false;
                break;
            }

            if ($descricaoSituacao == 'Negar' && empty($motivo)) {
                $this->mensagem =
                    'Para a situação Negar deve ser informado um motivo';
                return false;
                break;
            }

            $compraItemFornecedor =  $itemAdesao->item_arp->item_fornecedor_compra;

            if ($descricaoSituacao == 'Aceitar' && $quantidade == 0) {
                $quantidade = $itemAdesao->quantidade_solicitada;
            }

            if ($quantidade == null) {
                $quantidade = 0;
            }

            if (isset($arrayItemFormatado[$compraItemFornecedor->compra_item_id])) {
                $arrayItemFormatado[$compraItemFornecedor->compra_item_id]['quantidade'] += $quantidade;
            }

            if (!isset($arrayItemFormatado[$compraItemFornecedor->compra_item_id])) {
                $arrayItemFormatado[$compraItemFornecedor->compra_item_id]['quantidade'] = $quantidade;
            }

            $arrayItemFormatado[$compraItemFornecedor->compra_item_id]['compra_item_fornecedor_id']
                = $compraItemFornecedor->fornecedor_id;

            $arrayItemFormatado[$compraItemFornecedor->compra_item_id]['situacao']
                = $descricaoSituacao;
        }

        $arpAdesaoRepository = new ArpAdesaoRepository();
        $adesaoService = new AdesaoService();

        $itemValido = true;
        $adesao = Adesao::find($this->idAdesao);

        foreach ($arrayItemFormatado as $compraItemId => $dadosValidacao) {
            if ($dadosValidacao['situacao'] == 'Negar') {
                continue;
            }

            $itemValido = $adesaoService->itemValidoParaCaronaRule(
                $compraItemId,
                $dadosValidacao['compra_item_fornecedor_id'],
                $arpAdesaoRepository,
                $adesao->unidade_origem_id,
                $adesao->execucao_descentralizada_programa_projeto_federal,
                $adesao->aquisicao_emergencial_medicamento_material,
                $dadosValidacao['quantidade']
            );

            if (!$itemValido) {
                $this->mensagem =
                    'O limite estabelecido pelo art. 32, I, do Decreto 11.462/2023 foi atingido pela unidade.';
                break;
            }
        }

        return $itemValido;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagem;
    }
}
