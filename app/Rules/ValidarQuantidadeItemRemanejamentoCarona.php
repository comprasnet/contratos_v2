<?php

namespace App\Rules;

use App\Repositories\Arp\ArpRemanejamentoItemRepository;
use App\Repositories\Compra\CompraItemUnidadeRepository;
use Illuminate\Contracts\Validation\Rule;

class ValidarQuantidadeItemRemanejamentoCarona implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $idItemRemanejamento = explode('.', $attribute)[1];
        $arpRemanejamentoItemRepository = new ArpRemanejamentoItemRepository();
        $itemRemanejamento = $arpRemanejamentoItemRepository->getRemanejamentoItemUnico($idItemRemanejamento);
        $dadosUnidadeReceber = $itemRemanejamento->itemUnidadeReceber;
        
        if ($dadosUnidadeReceber->tipo_uasg != 'C') {
            return true;
        }
        
        $compraItemUnidadeRepository = new CompraItemUnidadeRepository();
        $somatoriaTodasUnidadeItem = $compraItemUnidadeRepository->getItemUnidadePorTipoUasg(
            $dadosUnidadeReceber->compra_item_id,
            ['G', 'P']
        )->sum('quantidade_adquirir');

        $quantidadeCinquantaPorCento = $somatoriaTodasUnidadeItem * 0.5;
        
        $totalQuantidadeReceberCarona = $dadosUnidadeReceber->quantidade_autorizada + $value;

        if ($totalQuantidadeReceberCarona > $quantidadeCinquantaPorCento) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'A quantidade solicitada para o item ultrapassa a quantidade máxima permitida para
         unidade não participante';
    }
}
