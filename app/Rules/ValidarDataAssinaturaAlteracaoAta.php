<?php

namespace App\Rules;

use App\Models\Arp;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class ValidarDataAssinaturaAlteracaoAta implements Rule
{
    private $idAta;
    private $dataAssinaturaAlteracaoFimVigencia;

    private $dataAssinaturaAlteracaoVigencia;

    private $mensagem;


    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(
        ?int $idAta,
        ?string $dataAssinaturaAlteracaoFimVigencia,
        ?string $dataAssinaturaAlteracaoVigencia
    ) {
        $this->idAta = $idAta;
        $this->dataAssinaturaAlteracaoFimVigencia = $dataAssinaturaAlteracaoFimVigencia;
        $this->dataAssinaturaAlteracaoVigencia = $dataAssinaturaAlteracaoVigencia;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ata = Arp::find($this->idAta);
        $dataMaximoVigenciaAta = Carbon::parse($ata->vigencia_final)->addYear();
        $dataVigenciaInicialAta = Carbon::parse($ata->vigencia_inicial);
        $dataMaximoVigenciaInformada = Carbon::parse($this->dataAssinaturaAlteracaoFimVigencia);
        $datAssinaturaAlteracaoInformada = Carbon::parse($this->dataAssinaturaAlteracaoVigencia);
        $dataFinalVigenciaAta = Carbon::parse($ata->vigencia_final);

        if ($datAssinaturaAlteracaoInformada->isBefore($dataVigenciaInicialAta)) {
            $this->mensagem ='A data de assinatura deve ser posterior a '
                . $dataVigenciaInicialAta->format('d/m/Y');
            return false;
        }

        if ($dataMaximoVigenciaInformada->isAfter($dataMaximoVigenciaAta)) {
            $this->mensagem ='A nova data fim da vigência não pode ser superior a '
                . $dataMaximoVigenciaAta->format('d/m/Y');
            return false;
        }

        if ($datAssinaturaAlteracaoInformada->isAfter($dataFinalVigenciaAta)) {
            $this->mensagem ='A data de assinatura deve ser anterior a '
                . $dataFinalVigenciaAta->format('d/m/Y');
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagem;
    }
}
