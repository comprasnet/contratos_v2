<?php

namespace App\Rules;

use App\Repositories\Arp\ArpAdesaoRepository;
use App\Repositories\Arp\ArpItemRepository;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\Collection;

class ItemAdesaoExisteMinutaContratoRule implements Rule
{
    private $tipoBloqueio = null;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $adesao = (new ArpAdesaoRepository())->getAdesaoUnico($value);

        if ($adesao->situacao->descricao === 'Enviada para aceitação') {
            return true;
        }
        
        $itemAdesao = $adesao->item;
        $arpItemRepository = new ArpItemRepository();
        $idUnidadeLogada = $adesao->unidade_origem_id;

        foreach ($itemAdesao as $item) {
            $compraItemFornecedor = $item->item_arp->item_fornecedor_compra;
            $compraItemId = $compraItemFornecedor->compra_item_id;
            
            $itemConsumido = $arpItemRepository->getMinutasTipoCompraPorItemConsumiuSaldoUnidade(
                $compraItemId,
                $idUnidadeLogada,
                $compraItemFornecedor->id
            );
            
            
            $contratoPorItem = $arpItemRepository->getContratosPorItemFornecedorUnidadeOrigemContrato(
                $compraItemId,
                $compraItemFornecedor->fornecedor_id,
                $idUnidadeLogada
            );
            
            $itemNaoConsumidoMinutaEmpenho = $this->itemNaoConsumidoMinutaEmpenho($itemConsumido);

            $qtdContrato = $contratoPorItem->count();
            
            if ($itemNaoConsumidoMinutaEmpenho && $qtdContrato > 0) {
                $this->tipoBloqueio =
                    "A adesão não pode ser cancelada devido ter minuta de empenho ou contrato vinculado";
                return false;
                break;
            }
            
            if ($itemNaoConsumidoMinutaEmpenho) {
                $this->tipoBloqueio = "A adesão não pode ser cancelada devido ter minuta de empenho vinculada.";
                return false;
                break;
            }
            
            if ($qtdContrato > 0) {
                $this->tipoBloqueio = "A adesão não pode ser cancelada devido ter contrato vinculado.";
                return false;
                break;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->tipoBloqueio;
    }
    
    private function itemNaoConsumidoMinutaEmpenho(Collection $itemConsumido)
    {
        $itemNaoConsumidoMinutaEmpenho = false;
        
        if ($itemConsumido->count() > 0) {
            $itemNaoConsumidoMinutaEmpenho = true;
        }
        
        // Comentado para quando entrar a issue de avaliar por situação da minuta de empenho
//        foreach ($itemConsumido as $item) {
//            if ($item) {
//                $itemNaoConsumidoMinutaEmpenho = $item->saldo_consumido;
//                break;
//            }
//        }
        
        return $itemNaoConsumidoMinutaEmpenho;
    }
}
