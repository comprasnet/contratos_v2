<?php

namespace App\Rules;

use App\Models\ArpItem;
use App\Models\CodigoItem;
use Illuminate\Contracts\Validation\Rule;

class ValidarCancelamentoItemAlteracaoAta implements Rule
{
    private $idAta;
    private $itemCancelado;
    private $rascunho;
    private $mensagem;
    private $justificativaMotivoCancelamento;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(
        ?int $idAta,
        ?array $itemCancelado,
        ?bool $rascunho,
        ?string $justificativaMotivoCancelamento
    ) {
        $this->idAta = $idAta;
        $this->itemCancelado = $itemCancelado;
        $this->rascunho = $rascunho;
        $this->justificativaMotivoCancelamento = $justificativaMotivoCancelamento;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!$value) {
            return true;
        }

        $alteracaoCancelamento = CodigoItem::where('descricao', 'Cancelamento de item(ns)')->first();
        $itemDisponivel =
            ArpItem::getItemAlteracaoPorAta($this->idAta, $alteracaoCancelamento->id)->pluck('id')->toArray();

        $arpItemId = array_keys($this->itemCancelado);

        foreach ($arpItemId as $id) {
            if (!in_array($id, $itemDisponivel)) {
                $this->mensagem = 'O item selecionado não pode ser cancelado.';
                return false;
                break;
            }
        }

        if (!$this->rascunho && empty($this->justificativaMotivoCancelamento)) {
            $this->mensagem = 'Favor preencher uma justificativa para o cancelamento.';
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagem;
    }
}
