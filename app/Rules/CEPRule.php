<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CEPRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $validarFormatoCEP = preg_match('/^\d{5}-\d{3}|\d{8}$/', $value);
        
        if (!$validarFormatoCEP) {
            return $validarFormatoCEP;
        }
        
        return $validarFormatoCEP;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'CEP inválido';
    }
}
