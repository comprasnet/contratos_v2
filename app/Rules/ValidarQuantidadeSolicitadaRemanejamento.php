<?php

namespace App\Rules;

use App\Http\Traits\Arp\ArpRemanejamentoTrait;
use App\Repositories\Arp\ArpRemanejamentoItemRepository;
use App\Repositories\Compra\CompraItemRepository;
use App\Repositories\Compra\CompraItemUnidadeRepository;
use Illuminate\Contracts\Validation\Rule;

class ValidarQuantidadeSolicitadaRemanejamento implements Rule
{
    use ArpRemanejamentoTrait;
    
    protected $mensagemErro = '';
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
    
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!is_array($value)) {
            $this->mensagemErro = 'Preencha ao menos um item para o remanejamento';
            return false;
        }

        $compraItemUnidadeRepository = new CompraItemUnidadeRepository();
        foreach ($value as $idItemUnidade => $quantidadeAnalisada) {
            if (is_array($quantidadeAnalisada)) {
                $existeSaldoUnidadeParaRemanejar = true;
                foreach ($quantidadeAnalisada as $idItemUnidadeFornecedor => $quantidade) {
                    if (empty($quantidade)) {
                        continue;
                    }
                    
                    $itemRemanejamento = $compraItemUnidadeRepository->getItemUnidade($idItemUnidade);

                    $existeSaldoUnidadeParaRemanejar = $this->existeSaldoUnidadeParaRemanejar(
                        $itemRemanejamento,
                        $quantidade
                    );
                    
                    if ($existeSaldoUnidadeParaRemanejar == false) {
                        $this->mensagemErro =
                            'Ação não permitida por estar ultrapassando o limite permitido para remanejamento.';
                        break;
                    }
                }
                
                return $existeSaldoUnidadeParaRemanejar;
            }

            $itemRemanejamento = $compraItemUnidadeRepository->getItemUnidade($idItemUnidade);
            
            $existeSaldoUnidadeParaRemanejar = $this->existeSaldoUnidadeParaRemanejar(
                $itemRemanejamento,
                $quantidadeAnalisada
            );

            if ($existeSaldoUnidadeParaRemanejar == false) {
                $this->mensagemErro =
                    'Ação não permitida por estar ultrapassando o limite permitido para remanejamento.';
                return $existeSaldoUnidadeParaRemanejar;
            }
        }

        return true;
    }
    
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagemErro;
    }
}
