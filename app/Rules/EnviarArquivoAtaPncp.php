<?php

namespace App\Rules;

use App\Models\Arp;
use App\Models\ArpArquivoTipo;
use Illuminate\Contracts\Validation\Rule;

class EnviarArquivoAtaPncp implements Rule
{
    protected $restrito;
    protected $mensagem;
    protected $arpId;
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(?bool $restrito, int $arpId)
    {
        $this->restrito = $restrito;
        $this->arpId = $arpId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $tipoArquivo = ArpArquivoTipo::find($value);
        
        if (empty($tipoArquivo)) {
            $this->mensagem = 'Tipo de arquivo não encontrado';
            return false;
        }
        
        if ($tipoArquivo->nome === 'Ata de Registro de Preços' && $this->restrito) {
            $this->mensagem = "Para o tipo {$tipoArquivo->nome} o arquivo deve ser público";
            return false;
        }
        
        $ata = Arp::find($this->arpId);
        if (empty($ata)) {
            $this->mensagem = 'Ata não encontrada';
            return false;
        }
        
        if (empty($ata->enviaDadosPncp)) {
            $this->mensagem = 'Ata não encontrada para enviar ao pncp';
            return false;
        }
        
        if (empty($ata->enviaDadosPncp->link_pncp) || empty($ata->enviaDadosPncp->sequencialPNCP)) {
            $this->mensagem = 'Ata não enviada para o PNCP, sem link ou sequencial';
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagem;
    }
}
