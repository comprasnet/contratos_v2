<?php

namespace App\Rules;

use App\Repositories\Arp\ArpAdesaoRepository;
use App\Repositories\Arp\ArpItemRepository;
use App\Repositories\Arp\ArpRemanejamentoRepository;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Database\Eloquent\Collection;

class ItemCaronaRecebeuRemanejamentoRule implements Rule
{
    private $tipoBloqueio = null;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $adesao = (new ArpAdesaoRepository())->getAdesaoUnico($value);

        if ($adesao->situacao->descricao === 'Enviada para aceitação') {
            return true;
        }
        
        $quantidadeRemanejamentoAceito =
            (new ArpRemanejamentoRepository())->quantidadeRemanejamentoAceitoPorAdesaoQuantitativo($value);
        
        if ($quantidadeRemanejamentoAceito > 0) {
            return false;
        }
        
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'A adesão não pode ser cancelada devido o item ter recebido um remanejamento de quantitativo';
    }
}
