<?php

namespace App\Rules;

use App\Http\Traits\ExternalServices;
use Illuminate\Contracts\Validation\Rule;

class ItemNovoDivulgacaoRule implements Rule
{
    use ExternalServices;
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $url = config('api.novodivulgacaocompra.url');
        
        $arrayUrl = explode('/', $value);
        $endpoint = end($arrayUrl);

        try {
            $this->makeRequest(
                'GET',
                $url,
                $endpoint,
                null,
                [],
                null,
                []
            );
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Item não encontrado no novo divulgação.';
    }
}
