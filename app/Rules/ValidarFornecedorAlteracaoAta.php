<?php

namespace App\Rules;

use App\Models\Fornecedor;
use Illuminate\Contracts\Validation\Rule;

class ValidarFornecedorAlteracaoAta implements Rule
{
    private $novoCnpjFornecedor;
    private $novoNomeCnpjFornecedor;
    private $mensagem;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(?array $novoCnpjFornecedor, ?array $novoNomeCnpjFornecedor)
    {
        $this->novoCnpjFornecedor = $novoCnpjFornecedor;
        $this->novoNomeCnpjFornecedor = $novoNomeCnpjFornecedor;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        foreach ($this->novoCnpjFornecedor as $arpItemId => $cnpjCpfFornecedor) {
            $fornecedor = Fornecedor::where('cpf_cnpj_idgener', $cnpjCpfFornecedor)->first();
            $nomeFornecedor = $this->novoNomeCnpjFornecedor[$arpItemId] ?? null;

            if (!empty($fornecedor) && !empty($nomeFornecedor)) {
                $this->mensagem = 'Não pode alterar o nome do fornecedor, o mesmo existe na base de dados';
                return false;
                break;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagem;
    }
}
