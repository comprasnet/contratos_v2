<?php

namespace App\Rules;

use App\Http\Traits\Arp\ArpTrait;
use App\Repositories\Compra\CompraItemFornecedorRepository;
use Illuminate\Contracts\Validation\Rule;

class ValidarItemAtaSolicitacao implements Rule
{
    use ArpTrait;
    protected $arpItemid;
    protected $idAta;
    protected $mensagem;
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(array $arpItemid = [], ?int $idAta = null)
    {
        $this->arpItemid = $arpItemid;
        $this->idAta = $idAta;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $compraItemFornecedorRepository = new CompraItemFornecedorRepository();
        $itemCompraItemFornecedor = $compraItemFornecedorRepository->getItemFornecedor($value);
        $itemValido = $this->itemValidoParaSelecionar($itemCompraItemFornecedor, $this->arpItemid, $this->idAta);
        
        if (!$itemValido) {
            $nomeItem = $itemCompraItemFornecedor->compraItens->exibirItemCompleto();
            $this->mensagem = "O item {$nomeItem} não pode ser selecionado, ".
                "devido pertencer a outra ata de registro de preço ativa";
            return false;
        }
        
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagem;
    }
}
