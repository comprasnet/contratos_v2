<?php

namespace App\Rules;

use App\Http\Traits\Arp\ArpRemanejamentoTrait;
use App\Models\CodigoItem;
use App\Models\CompraItemUnidade;
use App\Repositories\Arp\ArpRemanejamentoItemRepository;
use App\Services\Arp\ArpRemanejamentoService;
use Illuminate\Contracts\Validation\Rule;

class ValidarQuantidadeAnalisadaRemanejamentoUnidadeGerenciadoraAta implements Rule
{
    use ArpRemanejamentoTrait;
    
    protected $mensagemErro = '';

    private $rascunho;

    private $statusId;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(?bool $rascunho, ?array $statusId)
    {
        $this->rascunho = $rascunho;
        $this->statusId = $statusId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $arpRemanejamentoItemRepository = new ArpRemanejamentoItemRepository();

        $arrayValidacaoCompraItemUnidadeCarona = array();

        foreach ($value as $idRemanejamentoItem => $quantidadeAnalisada) {
            $itemRemanejamento = $arpRemanejamentoItemRepository->getRemanejamentoItemUnico($idRemanejamentoItem);

            $existeSaldoUnidadeParaRemanejar = $this->existeSaldoUnidadeParaRemanejar(
                $itemRemanejamento->itemUnidade,
                $itemRemanejamento->quantidade_autorizada
            );

            if (!$existeSaldoUnidadeParaRemanejar) {
                $this->mensagemErro =
                    'Ação não permitida por estar ultrapassando o limite permitido para remanejamento.';
                return $existeSaldoUnidadeParaRemanejar;
            }

            $quantidadeValidacao =
                $itemRemanejamento->quantidade_aprovada_participante ?? $itemRemanejamento->quantidade_solicitada;

            if ($itemRemanejamento->situacaoItem->descricao === 'Aguardando aceitação da unidade participante') {
                $quantidadeValidacao = $itemRemanejamento->quantidade_solicitada;
            }

            if ($quantidadeValidacao < $quantidadeAnalisada) {
                $this->mensagemErro = 'A quantidade solicitada deve ser menor que a aprovada pela Unidade Participante';
                return false;
            }

            $idSituacaoSelecionado = $this->statusId[$idRemanejamentoItem] ?? null;

            if (!empty($idSituacaoSelecionado)) {
                $situacaoAnaliseSelecionada = CodigoItem::find($idSituacaoSelecionado);
                if ($situacaoAnaliseSelecionada->descricao == 'Aceitar Parcialmente' &&
                    $quantidadeValidacao == $quantidadeAnalisada
                ) {
                    $this->mensagemErro =
                        'Quando aceito parcialmente a quantidade aprovada deve ser inferior a solicitada.';
                    return false;
                }
            }

            if ($itemRemanejamento->itemUnidadeReceber->tipo_uasg === 'C') {
                if (isset($arrayValidacaoCompraItemUnidadeCarona[$itemRemanejamento->id_item_unidade])) {
                    $arrayValidacaoCompraItemUnidadeCarona[$itemRemanejamento->id_item_unidade]['quantidade'] +=
                        $quantidadeAnalisada;
                }

                if (!isset($arrayValidacaoCompraItemUnidadeCarona[$itemRemanejamento->id_item_unidade])) {
                    $arrayValidacaoCompraItemUnidadeCarona[$itemRemanejamento->id_item_unidade]['quantidade'] =
                        $quantidadeAnalisada;
                }

                $arrayValidacaoCompraItemUnidadeCarona[$itemRemanejamento->id_item_unidade]['unidadeSolicitanteId'] =
                $itemRemanejamento->remanejamento->unidade_solicitante_id;
            }
        }
        $remanejamentoService = new ArpRemanejamentoService();

        foreach ($arrayValidacaoCompraItemUnidadeCarona as $compraItemUnidadeCeder => $dadosValidacao) {
            $unidadeCeder = CompraItemUnidade::find($compraItemUnidadeCeder);

            if (empty($unidadeCeder)) {
                $this->mensagemErro = 'Não encontrou a unidade para ceder o item para remanejamento';
                return false;
            }

            if ($unidadeCeder->quantidade_saldo < $dadosValidacao['quantidade']) {
                $this->mensagemErro = 'O somatório das unidades solicitantes não podem ultrapassar do total
                do saldo para o item';
                return false;
            }

            $retornoValidacao = $remanejamentoService->usuarioPodeSolicitarQuantitativo(
                $unidadeCeder->compra_item_id,
                $dadosValidacao['unidadeSolicitanteId'],
                $dadosValidacao['quantidade']
            );

            if (!$retornoValidacao['podeSolicitar']) {
                $this->mensagemErro =
                    'O limite estabelecido pelo art. 32, I, do Decreto 11.462/2023 foi atingido pela unidade.';
                return false;
            }
        }

        return true;
    }
    
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagemErro;
    }
}
