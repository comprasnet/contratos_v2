<?php

namespace App\Rules;

use App\Repositories\Arp\ArpRemanejamentoItemRepository;
use App\Repositories\Compra\CompraItemUnidadeRepository;
use App\Services\Arp\ArpRemanejamentoService;
use Illuminate\Contracts\Validation\Rule;

class ValidarQuantidadeItemRemanejamentoCaronaSolicitacao implements Rule
{
    private $itemPrincipalSolicitado;
    private $quantidadeSolicitada;
    private $unidadeSolicitante;
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(
        ?array $itemPrincipalSolicitado,
        ?array $quantidadeSolicitada,
        ?int $unidadeSolicitante
    ) {
        $this->itemPrincipalSolicitado = $itemPrincipalSolicitado;
        $this->quantidadeSolicitada = $quantidadeSolicitada;
        $this->unidadeSolicitante = $unidadeSolicitante;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $sucesso = true;
        $itemUnidadeRepository = new CompraItemUnidadeRepository();
        $remanejamentoService = new ArpRemanejamentoService();
        
        foreach ($value as $arpId => $compraItemUnidade) {
            $arrayCompraItemUnidade = array();

            foreach ($compraItemUnidade as $itemUnidade) {
                $compraItemUnidadePrincipal = $this->itemPrincipalSolicitado[$itemUnidade];
                $quantidadeSolicitada = $this->quantidadeSolicitada[$itemUnidade];

                if (isset($arrayCompraItemUnidade[$compraItemUnidadePrincipal])) {
                    $arrayCompraItemUnidade[$compraItemUnidadePrincipal]['somatorioItem'] += $quantidadeSolicitada;
                }
                
                if (!isset($arrayCompraItemUnidade[$compraItemUnidadePrincipal])) {
                    $arrayCompraItemUnidade[$compraItemUnidadePrincipal]['somatorioItem'] = $quantidadeSolicitada;
                }
            }
            
            foreach ($arrayCompraItemUnidade as $compraItemUnidade => $dadosValidacao) {
                $itemUnidadeValidacao = $itemUnidadeRepository->getItemUnidade($compraItemUnidade);
                $unidadeSolicitanteCarona = $remanejamentoService->unidadeSolicitanteCaronaItem(
                    $itemUnidadeValidacao->compra_item_id,
                    $this->unidadeSolicitante
                );

                if (!$unidadeSolicitanteCarona) {
                    continue;
                }

                $retornoValidacao = $remanejamentoService->usuarioPodeSolicitarQuantitativo(
                    $itemUnidadeValidacao->compra_item_id,
                    $this->unidadeSolicitante,
                    $dadosValidacao['somatorioItem']
                );
                
                if (!$retornoValidacao['podeSolicitar']) {
                    $sucesso = $retornoValidacao['podeSolicitar'];
                }
            }
        }

        return $sucesso;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'O limite estabelecido pelo art. 32, I, do Decreto 11.462/2023 foi atingido pela unidade.';
    }
}
