<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidarJustificativaItemIsoladoArpAdesaoRule implements Rule
{
    private $grupoCompraQuantidadeSolicitada;
    private $rascunho;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(?array $grupoCompraQuantidadeSolicitada, ?bool $rascunho)
    {
        $this->grupoCompraQuantidadeSolicitada = $grupoCompraQuantidadeSolicitada;
        $this->rascunho = $rascunho;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->rascunho) {
            return true;
        }
        
        $frequencia = array();
        
        $existeGrupoCompraZero = false;
        
        foreach ($this->grupoCompraQuantidadeSolicitada as $grupoCompra) {
            if ($grupoCompra == '00000') {
                $existeGrupoCompraZero = true;
            }

            if (isset($frequencia[$grupoCompra])) {
                $frequencia[$grupoCompra]++;
                continue;
            }
            
            $frequencia[$grupoCompra] = 1;
        }
        
        if ($existeGrupoCompraZero) {
            unset($frequencia['00000']);
        }
        
        if (count($frequencia) == 1 && $existeGrupoCompraZero) {
            return true;
        }
        
        if (count($frequencia) == 1) {
            return true;
        }

        if (empty($frequencia)) {
            return true;
        }
        
        if (count($frequencia) >= 1 && !empty($value)) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Inserir uma justificativa para o grupo da compra dos itens';
    }
}
