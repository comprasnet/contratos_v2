<?php

namespace App\Rules;

use App\Models\CompraItemFornecedor;
use App\Models\CompraItemUnidade;
use Illuminate\Contracts\Validation\Rule;
use App\Models\ArpItem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;

class FornecedoresAdesaoDiferentesRule implements Rule
{

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $inputBag;

    public function __construct($inputBag)
    {
        $this->inputBag = $inputBag;
    }
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */

    public function passes($attribute, $value)
    {
        //$quantidadeSolicitada = $this->inputBag->get('quantidade_solicitada');
        $quantidadeSolicitada02 = $this->inputBag->get('item_solicitacao_lancamento');
        $valores = array_filter($quantidadeSolicitada02);

        $ids = array_map('intval', $valores);



        // Convertendo os $ids para uma coleção
        $idsCollection = collect(array_map('intval', $valores));

        $arpItems = ArpItem::select('arp_item.id')
            ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
            ->join('arp_solicitacao_item', 'arp_solicitacao_item.item_arp_fornecedor_id', '=', 'arp_item.id')
            ->join('arp_solicitacao', 'arp_solicitacao.id', '=', 'arp_solicitacao_item.arp_solicitacao_id')
            ->where(
                'arp_solicitacao.unidade_origem_id',
                session('user_ug_id')
            )
            ->get()
            ->pluck('id');


        $mergedResults = $idsCollection->merge($arpItems);

        $catmat = ArpItem::select(
            'compra_items.catmatseritem_id',
            'compra_items.numero',
            'arp_item.compra_item_fornecedor_id'
        )
            ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
            ->join('compra_items', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
            ->whereIn('arp_item.id', $ids)

            ->get();

        $itemDoisFornecedores = ArpItem::select(
            'compra_items.catmatseritem_id',
            'compra_items.numero',
            'compra_items.compra_id',
            'arp_item.compra_item_fornecedor_id',
            'compra_item_fornecedor.fornecedor_id'
        )
            ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
            ->join('compra_items', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
            ->join(
                'arp_solicitacao_item',
                'arp_solicitacao_item.item_arp_fornecedor_id',
                '=',
                'arp_item.id'
            )
            ->join('arp_solicitacao', function ($query) {
                $query->on('arp_solicitacao.id', 'arp_solicitacao_item.arp_solicitacao_id')
                    ->where("arp_solicitacao.rascunho", false);
            })
            ->join(
                'codigoitens',
                'arp_solicitacao.status',
                '=',
                'codigoitens.id'
            )
            ->whereIn('arp_item.id', $arpItems)
            ->whereNotIn("codigoitens.descricao", ["Negada","Negar",
                "Cancelada pelo solicitante"])
            ->whereNull('arp_solicitacao.data_cancelamento')
            ->where(
                'arp_solicitacao.unidade_origem_id',
                session('user_ug_id')
            )
            ->distinct()

            ->get();


        $catmatseritem_ids = $catmat->pluck('catmatseritem_id');
        $numeros = $catmat->pluck('numero');
        $fornecedores_ids = $catmat->pluck('compra_item_fornecedor_id')->unique();


        // Verificar se todos os catmatseritem_id são iguais
        $catmatseritem_id_unico = $catmatseritem_ids->unique()->count() === 1;

        // Verificar se todos os números são diferentes
        $numeros_unicos = $numeros->unique()->count() === $catmat->count();

        // Verificar se todos os números dos itens são diferentes
        $numeros_diferentes = $numeros->count() === $numeros->unique()->count();

        // Verificar se todos os números são iguais
        $numeros_iguais = $numeros->count() === 1 || $numeros->unique()->count() === 1;

        $fornecedores_diferentes = $fornecedores_ids->count() > 1;

        $compra_item_fornecedor_id = $this->inputBag->get('compra_item_fornecedor_id');


        foreach ($itemDoisFornecedores as $item) {
            if ($catmatseritem_id_unico && $numeros_iguais &&
                $this->inputBag->get('compra_id') === $item->compra_id &&
                $numeros->contains($item->numero) &&
                $this->inputBag->get('fornecedor_id') !== $item->fornecedor_id
            ) {
                return false;
            }
        }


        // Se os catmatseritem_id forem iguais e os números diferentes, retorna true
        if ($catmatseritem_id_unico && $numeros_unicos && $numeros_diferentes) {
            return true;
        }

        if ($numeros_iguais && $catmatseritem_id_unico &&
            $catmat->pluck('compra_item_fornecedor_id')->unique()->count() === 1) {
            return true;
        }


        // Verificar se existem catmatseritem_id iguais, mas todos os números são diferentes
        if ($catmatseritem_ids->count() !== $catmat->count()) {
            // Verificar se todos os números são diferentes
            if ($numeros_diferentes) {
                return true;
            }
        }

        // Verificar se todos os catmatseritem_id são diferentes e todos os números são diferentes
        if ($catmatseritem_ids->unique()->count() === $catmat->count() && $numeros_diferentes) {
            return true;
        }
        if ($fornecedores_diferentes && $numeros_diferentes) {
            return true;
        }


        $resultado = ArpItem::select('*')
            ->join(
                'compra_item_fornecedor',
                'arp_item.compra_item_fornecedor_id',
                '=',
                'compra_item_fornecedor.id'
            )
            ->join(
                'compra_items',
                'compra_item_fornecedor.compra_item_id',
                '=',
                'compra_items.id'
            )
            ->join('compra_item_unidade', function ($join) {
                $join->on('compra_items.id', '=', 'compra_item_unidade.compra_item_id')
                    ->where('compra_item_unidade.tipo_uasg', 'C')
                    ->where('compra_item_unidade.situacao', true);
            })
            ->join(
                'arp_solicitacao_item',
                'arp_solicitacao_item.item_arp_fornecedor_id',
                '=',
                'arp_item.id'
            )
            ->join(
                'arp_solicitacao',
                'arp_solicitacao.id',
                '=',
                'arp_solicitacao_item.arp_solicitacao_id'
            )
            ->join(
                'codigoitens',
                'arp_solicitacao_item.status_id',
                '=',
                'codigoitens.id'
            )
            ->whereNotIn('arp_item.id', $ids)
            ->where(
                'arp_solicitacao.unidade_origem_id',
                '=',
                session('user_ug_id')
            )
            ->where(
                'compra_item_unidade.unidade_id',
                '=',
                session('user_ug_id')
            )
            ->whereIN("codigoitens.descricao", ["Aceitar","Aceita", "Aceitar Parcialmente",
                "Aceito Parcial"])
            ->whereExists(function ($query) {
                $query->select('fornecedor_id')
                    ->from('compra_item_fornecedor')
                    ->whereColumn('id', 'arp_item.compra_item_fornecedor_id');
            })
            ->get();



        // Verifica se há resultado na consulta
        if ($resultado->isEmpty()) {
            // Se não houver resultado, chama a segunda função
            return $this->passesArpSolicitacao();
        }


        // Verifica se os fornecedor_id são diferentes
        foreach ($resultado as $item) {
            $catmatseritem_id = $item->catmatseritem_id;

            if ($item->item_fornecedor_compra->compraItens->catmatseritem_id ===
                $catmatseritem_id ||$item->item_fornecedor_compra->fornecedor_id !==
                $this->inputBag->get('fornecedor_id')) {
                // Se os fornecedor_id forem diferentes, a validação falha
                return false;
            }
        }
        // Se todos os fornecedor_id forem iguais, a validação passa
        return true;
    }


    public function passesArpSolicitacao()
    {
        $quantidadeSolicitada02 = $this->inputBag->get('item_solicitacao_lancamento');
        $valores = array_filter($quantidadeSolicitada02);
        $ids = array_map('intval', $valores);
        $fornecedorId = $this->inputBag->get('fornecedor_id');

        $results = ArpItem::join(
            'compra_item_fornecedor',
            'arp_item.compra_item_fornecedor_id',
            '=',
            'compra_item_fornecedor.id'
        )
            ->join(
                'compra_items',
                'compra_item_fornecedor.compra_item_id',
                '=',
                'compra_items.id'
            )
            ->join(
                'arp_solicitacao_item',
                'arp_solicitacao_item.item_arp_fornecedor_id',
                '=',
                'arp_item.id'
            )
            ->join(
                'arp_solicitacao',
                'arp_solicitacao.id',
                '=',
                'arp_solicitacao_item.arp_solicitacao_id'
            )
            ->join(
                'codigoitens',
                'arp_solicitacao_item.status_id',
                '=',
                'codigoitens.id'
            )
            ->whereNotIn('arp_item.id', $ids)
            ->where(
                'arp_solicitacao.unidade_origem_id',
                session('user_ug_id')
            )
//            ->whereIN("codigoitens.descricao", ["Negada","Negar","Aceitar",
//                "Aceita", "Aceitar Parcialmente", "Aceito Parcial","Enviada para aceitação"])

               ->whereNotIn("codigoitens.descricao", ["Negada","Negar"])
            ->whereExists(function ($query) {
                $query->select('fornecedor_id')
                    ->from('compra_item_fornecedor')
                    ->whereColumn(
                        'id',
                        'arp_item.compra_item_fornecedor_id'
                    );
            })

            ->get();

        // Verifica se há resultado na consulta
        if ($results->isEmpty()) {
            // Se não houver resultado, chama a segunda função
            return $this->passesStoreSolicitacaoUnicoFornecedor();
        }



        // Verifica se o fornecedor_id retornado é
        // diferente do fornecedor_id fornecido no request
        foreach ($results as $item) {
            if ($item->item_fornecedor_compra->fornecedor_id !== $fornecedorId  && $item->descricao !== "Negar") {
                return false;
            }
        }


        return true;
    }

    public function passesStoreSolicitacaoUnicoFornecedor()
    {

        $quantidadeSolicitada02 = $this->inputBag->get('item_solicitacao_lancamento');
        $valores = array_filter($quantidadeSolicitada02);
        $ids = array_map('intval', $valores);
        $fornecedorId = $this->inputBag->get('fornecedor_id');
        // Obtém os itens relacionados aos IDs fornecidos
        $items = ArpItem::whereIn('id', $ids)->get();


        $uniqueCatmatseritemIds = [];

        if ($items) {
            foreach ($items as $item) {
                if ($item->item_fornecedor_compra) {
                    $catmatseritemId = $item->item_fornecedor_compra->compraItens->numero;

                    if ($catmatseritemId !== null && !in_array(
                        $catmatseritemId,
                        $uniqueCatmatseritemIds,
                        true
                    )) {
                        $uniqueCatmatseritemIds[] = $catmatseritemId;


                        $itemsWithSameCatmatseritem = ArpItem::whereHas(
                            'item_fornecedor_compra',
                            function ($query) use ($uniqueCatmatseritemIds) {
                                $query->whereHas('compraItens', function ($query) use ($uniqueCatmatseritemIds) {
                                    $query->where('numero', $uniqueCatmatseritemIds);
                                });
                            }
                        )->whereIn('id', $ids)->get();

                        $fornecedores = $itemsWithSameCatmatseritem
                            ->pluck('item_fornecedor_compra.fornecedor_id')->unique();

                        if ($fornecedores->count() > 1) {
                            return false;
                        }
                    }
                }
            }
        }

        // Verifica se não há re
        //sultado na consulta
        if ($items->isEmpty()) {
            // Se não houver resultado, verificar os catmatseritem_id dos itens e o fornecedor_id
            foreach ($ids as $itemId) {
                $item = ArpItem::find($itemId);


                // Verifica se o item existe e se o catmatseritem_id é válido
                if ($item && $item->item_fornecedor_compra) {
                    $catmatseritemId = $item->item_fornecedor_compra->compraItens->numero;
                    $uniqueCatmatseritemIds[] = $catmatseritemId;

                        // Verifica se todos os itens com o mesmo catmatseritem_id tem o mesmo fornecedor_id
                        $itemsWithSameCatmatseritem = ArpItem::whereHas(
                            'item_fornecedor_compra',
                            function ($query) use ($catmatseritemId) {
                                $query->whereHas('compraItens', function ($query) use ($catmatseritemId) {
                                    $query->where('numero', $catmatseritemId);
                                });
                            }
                        )->whereIn('id', $ids)->get();

                        $fornecedores = $itemsWithSameCatmatseritem->pluck(
                            'compra_item_fornecedor.fornecedor_id'
                        )->unique();

                    if ($fornecedores->count() > 1 || ($fornecedores->first() !== $fornecedorId)) {
                        // Se houver mais de um fornecedor diferente para o mesmo catmatseritem_id ou
                        // se o fornecedor_id não for o mesmo do fornecedorId fornecido, a validação falha
                        return false;
                    }
                }
            }
        }

        return true;
    }





    public function message()
    {
        return 'Já há uma solicitação registrada para o mesmo item por outro
         fornecedor. Por favor, verifique a outra solicitação';
    }
}
