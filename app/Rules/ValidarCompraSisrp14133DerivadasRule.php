<?php

namespace App\Rules;

use App\Http\Controllers\Api\ConcentradorCompras\CompraSisrp;
use App\Models\Compras;
use App\Services\CompraService;
use Illuminate\Contracts\Validation\Rule;
use function PHPUnit\Framework\isJson;

class ValidarCompraSisrp14133DerivadasRule implements Rule
{
    protected $message;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $compra = Compras::find($value);
        $retornoSiasg = (new CompraService())->validarCompraAPI($compra);
        
        if ($retornoSiasg->codigoRetorno <> 200) {
            $this->message = $retornoSiasg->message;
            return false;
        }
        
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
