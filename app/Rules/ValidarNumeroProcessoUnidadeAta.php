<?php

namespace App\Rules;

use App\Http\Traits\Unidade\ProcessoTrait;
use App\Repositories\UnidadeRepository;
use Illuminate\Contracts\Validation\Rule;

class ValidarNumeroProcessoUnidadeAta implements Rule
{
    use ProcessoTrait;
    
    protected $idUnidadeOrigemAta;
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(int $idUnidadeOrigemAta)
    {
        $this->idUnidadeOrigemAta = $idUnidadeOrigemAta;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $unidadeRepository = new UnidadeRepository();
        
        $dadosUnidade = $unidadeRepository->getUnidadePorId($this->idUnidadeOrigemAta);
        $mascaraProcesso = $this->getProcessoMask($this->idUnidadeOrigemAta);
        
        return $this->validarNumeroProcesso($value, $mascaraProcesso, $dadosUnidade->sisg);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Número do processo inválido';
    }
}
