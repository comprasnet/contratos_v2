<?php

namespace App\Rules;

use App\Models\Arp;
use Illuminate\Contracts\Validation\Rule;

class ValidarInformativoAlteracaoAta implements Rule
{
    private $idAta;
    private $objetoAlteracao;
    private $rascunho;
    private $mensagem;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(?int $idAta, ?string $objetoAlteracao, ?string $rascunho)
    {
        $this->idAta = $idAta;
        $this->objetoAlteracao = $objetoAlteracao;
        $this->rascunho = $rascunho;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!$value) {
            return true;
        }

        $ata = Arp::find($this->idAta);

        if ($ata->objeto == $this->objetoAlteracao) {
            $this->mensagem = 'Não pode alterar o tipo informativo com o mesmo objeto da ata';
            return false;
        }

        if (!$this->rascunho && empty($this->objetoAlteracao)) {
            $this->mensagem = 'Favor informar o Objeto da alteração';
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagem;
    }
}
