<?php

namespace App\Rules;

use App\Models\CompraItemUnidade;
use Illuminate\Contracts\Validation\Rule;

class ValidarSolicitarItemRemanejamento implements Rule
{
    protected $mensagem;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $tipoValido = is_numeric($value) || is_array($value);
        
        if (!$tipoValido) {
            $this->mensagem = 'O campo :attribute deve ser numérico ou um array.';
            return false;
        }
        $idCompraItemUnidadeItemPrincipal = explode('.', $attribute)[1];
        $compraItemUnidadeItemPrincipal = CompraItemUnidade::find($idCompraItemUnidadeItemPrincipal);
        
        if (empty($compraItemUnidadeItemPrincipal)) {
            $this->mensagem = 'Não encontrou o compra item unidade';
            return false;
        }

        if ($compraItemUnidadeItemPrincipal->quantidade_saldo <= 0) {
            $this->mensagem = 'O campo :attribute deve ser maior que 0.';
            return false;
        }
        
        return is_numeric($value) || is_array($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagem;
    }
}
