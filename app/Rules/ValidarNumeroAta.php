<?php

namespace App\Rules;

use App\Http\Traits\Arp\ArpTrait;
use Illuminate\Contracts\Validation\Rule;

class ValidarNumeroAta implements Rule
{
    use ArpTrait;
    
    protected $ano;
    protected $idUnidadeOrigemAta;
    protected $rascunho;
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(int $ano, int $idUnidadeOrigemAta, bool $rascunho)
    {
        $this->ano = $ano;
        $this->idUnidadeOrigemAta = $idUnidadeOrigemAta;
        $this->rascunho = $rascunho;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->ataJaExiste($value, $this->ano, $this->idUnidadeOrigemAta) && !$this->rascunho) {
            return false;
        }
        
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Ata já cadastrada para essa unidade';
    }
}
