<?php

namespace App\Rules;

use App\Models\Adesao;
use Illuminate\Contracts\Validation\Rule;

class AnalisarSituacaoAnaliseAdesaoRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $adesao = Adesao::find($value);
        if (in_array($adesao->situacao->descricao, [
            'Enviada para aceitação',
            'Aceita Parcialmente pelo Fornecedor',
            'Aceita pelo Fornecedor'
        ])) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Somente adesão com situação enviada para aceitação pode ser analisada';
    }
}
