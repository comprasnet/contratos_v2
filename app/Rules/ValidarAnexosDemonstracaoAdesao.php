<?php

namespace App\Rules;

use App\Models\Adesao;
use Illuminate\Contracts\Validation\Rule;

class ValidarAnexosDemonstracaoAdesao implements Rule
{
    private $rascunho;
    private $idAdesao;
    private $method;

    private $mensagem;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(?bool $rascunho, ?int $idAdesao, string $method)
    {
        $this->rascunho = $rascunho;
        $this->idAdesao = $idAdesao;
        $this->method = $method;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->method == 'POST' && $this->rascunho) {
            return true;
        }

        if ($this->method == 'PUT' && $this->rascunho) {
            return true;
        }

        if (!is_array($value)) {
            return false;
        }

        $extensoesPermitidas = ['ppt', 'xls', 'pdf'];

        $tamanhoMaximo = 30 * 1024 * 1024;

        $value = array_filter($value);

        foreach ($value as $arquivo) {
            if (!$arquivo instanceof \Illuminate\Http\UploadedFile) {
                $this->mensagem = 'Favor informar um arquivo no campo Anexo demonstração';
                return false;
            }

            $extensao = strtolower($arquivo->getClientOriginalExtension());
            if (!in_array($extensao, $extensoesPermitidas)) {
                $this->mensagem = 'O campo Anexo demonstração informado não é uma extensão válida, 
                podendo ser "ppt", "xls", "pdf"';
                return false;
            }

            if ($arquivo->getSize() > $tamanhoMaximo) {
                $this->mensagem = 'O campo Anexo demonstração não pode ser maior que 30 MB';
                return false;
            }
        }

        if (empty($value) && $this->method == 'PUT') {
            $adesao = Adesao::find($this->idAdesao);

            if (count($adesao->demonstracao) == 0) {
                $this->mensagem = 'Favor informar um arquivo no campo Anexo demonstração';
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagem;
    }
}
