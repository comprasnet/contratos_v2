<?php

namespace App\Rules;

use App\Models\Arp;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class ValidarAlteracaoAtaVigencia implements Rule
{
    private $idAta;
    private $dataAssinaturaAlteracaoFimVigencia;

    private $dataAssinaturaAlteracaoVigencia;

    private $mensagem;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(
        ?int $idAta,
        ?string $dataAssinaturaAlteracaoFimVigencia,
        ?string $dataAssinaturaAlteracaoVigencia
    ) {
        $this->idAta = $idAta;
        $this->dataAssinaturaAlteracaoFimVigencia = $dataAssinaturaAlteracaoFimVigencia;
        $this->dataAssinaturaAlteracaoVigencia = $dataAssinaturaAlteracaoVigencia;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!$value) {
            return true;
        }

        $ata = Arp::find($this->idAta);
        $dataMaximoVigenciaAta = Carbon::parse($ata->vigencia_final)->addYear();
        $datAssinaturaAlteracaoInformada = Carbon::parse($this->dataAssinaturaAlteracaoVigencia);

        if ($datAssinaturaAlteracaoInformada->isAfter($dataMaximoVigenciaAta)) {
            $this->mensagem ='A data de assinatura deve ser anterior a '
                . $dataMaximoVigenciaAta->format('d/m/Y');
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagem;
    }
}
