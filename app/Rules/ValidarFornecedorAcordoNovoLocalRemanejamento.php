<?php

namespace App\Rules;

use App\Models\CompraItemUnidade;
use Illuminate\Contracts\Validation\Rule;

class ValidarFornecedorAcordoNovoLocalRemanejamento implements Rule
{
    protected $mensagem;
    protected $remanejamentoEntreUnidadeEstadoDfMunicipioDistinto;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(?bool $remanejamentoEntreUnidadeEstadoDfMunicipioDistinto)
    {
        $this->remanejamentoEntreUnidadeEstadoDfMunicipioDistinto = $remanejamentoEntreUnidadeEstadoDfMunicipioDistinto;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->remanejamentoEntreUnidadeEstadoDfMunicipioDistinto == false && $value == true) {
            $this->mensagem ='Para remanejamento entre unidades de estado, distrito federal ou município distintos,
                é necessário que o fornecedor beneficiário da ata esteja de acordo.';
            return false;
        }

        if ($this->remanejamentoEntreUnidadeEstadoDfMunicipioDistinto == true && $value == false) {
            $this->mensagem = 'Para remanejamento entre unidades de estado, distrito federal ou município distintos,
            é necessário que o fornecedor beneficiário da ata esteja de acordo.';
            return false;
        }
        
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->mensagem;
    }
}
