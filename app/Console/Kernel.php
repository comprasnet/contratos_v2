<?php

namespace App\Console;

use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoExecucaoEntrega;
use App\Models\CodigoItem;
use App\Repositories\AutorizacaoExecucao\UsuarioFornecedorAutorizacaoExecucaoEntregaRepository;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoHistoricoAlterarService;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use App\Services\AutorizacaoExecucao\UsuarioFornecedorAutorizacaoExecucaoEntregaService;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    protected $schedule = null;
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $this->schedule = $schedule;
        # Método que contem todos os jobs
        $this->criarJobs();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    /**
     * Método responsável em iniciar todos os JOBs em uma única vez
     */
    protected function criarJobs()
    {
        # Método responsável em reprocessar o item da compra para
        # os participantes que ocorreu erro na hora da importação
        $this->jobReprocessaItemCompraParticipanteGestaoAta();

        # Método responsável em recuperar as atas que estão com status Enviar PNCP
        # e enviar para o PNCP
        $this->jobEnviarAtaPNCP();

        $this->jobAtualizarQuantitativoRenovadoArp();

        $this->jobAtualizaSituacaoAutorizacaoExecucao();

        $this->jobNotificaPrazosEntrega();
    }

    /**
     * Método responsável em reprocessar os itens com erros dos participantes
     * O tempo de execução será a cada 30 minutos
     * Utilizei o função 'withoutOverlapping' para executar o próximo ao finalizar
     * o que está em execução
     */
    protected function jobReprocessaItemCompraParticipanteGestaoAta()
    {
        $this->schedule->call(
            'App\Http\Controllers\Arp\ArpCrudController@jobReprocessaItemCompraParticipanteGestaoAta'
        )
        ->timezone('America/Sao_Paulo')
        ->name('reprocessa-item-compra-participante-gestao-ata')
        ->withoutOverlapping()
        ->everyThirtyMinutes();
    }

    /**
     * Método responsável em processar as atas que estão com o status Enviar PNCP
     */
    protected function jobEnviarAtaPNCP()
    {
        $this->schedule->call(
            'App\Http\Controllers\Arp\ArpCrudController@jobEnviarAtaPNCP'
        )
        ->timezone('America/Sao_Paulo')
        ->name('enviar-ata-pncp-gestao-ata')
        ->withoutOverlapping()
        ->everyFiveMinutes();
    }
    /**
     * Método responsável por agendar o job AtualizarQuantitativoRenovadoArpJob
     */
    protected function jobAtualizarQuantitativoRenovadoArp()
    {
        $this->schedule->call(
            'App\Http\Controllers\Arp\ArpAlteracaoCrudController@criarJobReset'
        )
            ->timezone('America/Sao_Paulo')
            ->name('atualizar-quantitativo-renovado-arp')
            ->withoutOverlapping()
            ->at('00:30');
    }

    protected function jobAtualizaSituacaoAutorizacaoExecucao()
    {
        $this->schedule->call(function () {
            try {
                DB::beginTransaction();

                $autorizacaoExecucaoService = new AutorizacaoExecucaoService();
                $autorizacaoExecucaoService->alteraSituacaoSeAtingirInicioVigencia();
                $autorizacaoExecucaoService->alteraSituacaoSeAtingirVigenciaFim();

                $ae = new AutorizacaoExecucao();
                (new AutorizacaoExecucaoHistoricoAlterarService($ae))->alteraSituacaoSeAtingirInicioAlteracao();
                DB::commit();
            } catch (\Exception $e) {
                \Log::error($e);
                DB::rollBack();
            }
        })
            ->timezone('America/Sao_Paulo')
            ->name('atualiza-situacao-autorizacao-execucao')
            ->withoutOverlapping()
            ->daily();
    }

    protected function jobNotificaPrazosEntrega()
    {
        $this->schedule->call(
            'App\Http\Controllers\AutorizacaoExecucao\AutorizacaoExecucaoEntregaCrudController@' .
                'notificarFiscaisDataPrazoTRP'
        )
            ->timezone('America/Sao_Paulo')
            ->name('notifica-prazos-entrega')
            ->withoutOverlapping()
            ->at('01:00');
    }
}
