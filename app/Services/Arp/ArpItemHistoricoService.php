<?php

namespace App\Services\Arp;

use App\Models\ArpItemHistorico;
use Illuminate\Support\Facades\DB;

class ArpItemHistoricoService
{
    public function itemAtaCanceladoRemovido(int $arpItemId)
    {
        return ArpItemHistorico::select(DB::raw(1))
            ->join('arp_historico', 'arp_historico.id', 'arp_item_historico.arp_historico_id')
            ->join('arp_item', 'arp_item_historico.arp_item_id', 'arp_item.id')
            ->whereRaw('arp_item_historico.arp_item_id = arp_item.id')
            ->where('arp_item_historico.arp_item_id', $arpItemId)
            ->where(function ($query) {
                $query->where('arp_item_historico.item_cancelado', true)
                    ->orWhere('arp_item_historico.item_removido_retificacao', true);
            })->exists();
    }
}
