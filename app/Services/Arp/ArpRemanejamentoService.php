<?php

namespace App\Services\Arp;

use App\Models\CompraItem;
use App\Repositories\Arp\ArpRemanejamentoRepository;
use App\Repositories\Compra\CompraItemUnidadeRepository;
use Illuminate\Database\Eloquent\Collection;

class ArpRemanejamentoService
{
    public function queryBaseRecuperarItensCaronaPorUnidadeSolicitante(int $compraItemId, int $unidadeSolicitanteId)
    {
        return (new CompraItemUnidadeRepository())->getItemUnidadePorTipoUasg($compraItemId, ['C'])
        ->where('unidade_id', $unidadeSolicitanteId);
    }
    
    public function unidadeSolicitanteCaronaItem(int $compraItemId, int $unidadeSolicitanteId)
    {
        return $this->queryBaseRecuperarItensCaronaPorUnidadeSolicitante($compraItemId, $unidadeSolicitanteId)
            ->exists();
    }
    
    public function recuperarQuantidadeTotalAutorizadoCarona(int $compraItemId, int $unidadeSolicitanteId)
    {
        return $this->queryBaseRecuperarItensCaronaPorUnidadeSolicitante($compraItemId, $unidadeSolicitanteId)
            ->sum('quantidade_autorizada');
    }
    
    public function calcularQuantidadeTotalSolicitar(
        float $totalQuantidadeAutorizada,
        float $somatorioQuantidadeItemSolicitado
    ) {
        return $totalQuantidadeAutorizada + $somatorioQuantidadeItemSolicitado;
    }
    
    public function calcularQuantidadeMaximaCarona(float $qtdTotal)
    {
        return $qtdTotal * 0.5;
    }
    
    public function usuarioPodeSolicitarQuantitativo(
        int $compraItemId,
        int $unidadeSolicitanteId,
        float $somatorioQuantidadeItemSolicitado,
        bool $exibirSelecao = false
    ) {
        $totalQuantidadeAutorizada =
            $this->recuperarQuantidadeTotalAutorizadoCarona($compraItemId, $unidadeSolicitanteId);

        $totalSolicitacao = $this->calcularQuantidadeTotalSolicitar(
            $totalQuantidadeAutorizada,
            $somatorioQuantidadeItemSolicitado
        );
        
        $itemSolicitado = CompraItem::find($compraItemId);

        $quantidadeMaximaCarona = $this->calcularQuantidadeMaximaCarona($itemSolicitado->qtd_total);

        $arrayRetorno = array('podeSolicitar' => true);

        /*
         * Realizar a validação dos 50% para carona pela unidade
         */
        $quantidadeTotalCaronaPorUnidadeItem =
            $this->recuperarQuantidadeTotalCaronaPorUnidadeSolicitanteItem($compraItemId, $unidadeSolicitanteId);

        $saldoToTalItensCarona = ($itemSolicitado->qtd_total * 0.5) - $quantidadeTotalCaronaPorUnidadeItem;

        if ($saldoToTalItensCarona <= 0) {
            $arrayRetorno['podeSolicitar'] = false;
            $arrayRetorno['quantidadeSaldo'] = 0;

            return $arrayRetorno;
        }

        /*
         * Realizar a validação dos 50% para carona pelo máximo de adesão do item
         */
        $quantidadeTotalCaronaPorItem = $this->recuperarQuantidadeTotalCaronaPorItem($compraItemId);

        $saldoToTalItensCarona = $itemSolicitado->maximo_adesao - $quantidadeTotalCaronaPorItem;


        if ($saldoToTalItensCarona <= 0) {
            $arrayRetorno['podeSolicitar'] = false;
            $arrayRetorno['quantidadeSaldo'] = 0;

            return $arrayRetorno;
        }

        if ($saldoToTalItensCarona < $somatorioQuantidadeItemSolicitado) {
            $arrayRetorno['podeSolicitar'] = false;
            $arrayRetorno['quantidadeSaldo'] = $saldoToTalItensCarona;

            return $arrayRetorno;
        }

//        if ($saldoToTalItensCarona < $totalSolicitacao) {
//            $arrayRetorno['podeSolicitar'] = false;
//            $arrayRetorno['quantidadeSaldo'] = $saldoToTalItensCarona;
//
//            return $arrayRetorno;
//        }

        if ($quantidadeMaximaCarona < $totalSolicitacao && !$exibirSelecao) {
            $arrayRetorno['podeSolicitar'] = false;
            $arrayRetorno['quantidadeSaldo'] = $quantidadeMaximaCarona - $totalQuantidadeAutorizada;
        }
        
        if ($quantidadeMaximaCarona < $totalSolicitacao && $exibirSelecao) {
            $arrayRetorno['podeSolicitar'] = true;
        }

        return $arrayRetorno;
    }

    public function recuperarQuantidadeTotalCaronaPorItem(int $compraItemId)
    {
        return (new CompraItemUnidadeRepository())->getItemUnidadePorTipoUasg($compraItemId, ['C'])
            ->sum('quantidade_autorizada');
    }

    public function recuperarQuantidadeTotalCaronaPorUnidadeSolicitanteItem(
        int $compraItemId,
        int $idUnidadeSolicitante
    ) {
        return (new CompraItemUnidadeRepository())->getItemUnidadePorTipoUasg($compraItemId, ['C'])
            ->where('unidade_id', $idUnidadeSolicitante)
            ->sum('quantidade_autorizada');
    }

    public function getItemRemanejamentoPorCompraItemFornecedorId(int $compraItemFornecedorId)
    {
        return (new ArpRemanejamentoRepository())
            ->getItemRemanejamentoPorCompraItemFornecedorId($compraItemFornecedorId);
    }

    public function itemFornecedorPendenteAnaliseRemanejamento(Collection $remanejamentos)
    {
        $situacaoRemanejamento = array();
        $remanejamentos->each(function ($itemRemanejamento) use (&$situacaoRemanejamento) {
            $situacaoRemanejamento[] = $itemRemanejamento->getSituacao();
        });

        $situacaoRemanejamento = array_unique($situacaoRemanejamento);

        $situacoesAguardandoAnalise = [
            'Aguardando aceitação da unidade gerenciadora',
            'Aguardando aceitação da unidade participante',
            'Aguardando aceitação'
        ];

        $remanejamentoPendenteAnalise = false;
        foreach ($situacoesAguardandoAnalise as $situacaoAguardandoAnalise) {
            if (in_array($situacaoAguardandoAnalise, $situacaoRemanejamento)) {
                $remanejamentoPendenteAnalise = true;
            }

            if ($remanejamentoPendenteAnalise) {
                break;
            }
        }

        return $remanejamentoPendenteAnalise;
    }
}
