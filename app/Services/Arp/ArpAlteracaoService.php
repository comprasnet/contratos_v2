<?php

namespace App\Services\Arp;

use App\Http\Traits\Arp\ArpAlteracaoCancelamentoTrait;
use App\Http\Traits\Arp\ArpItemHistoricoTrait;
use App\Models\Arp;
use App\Models\ArpHistorico;
use App\Models\ArpItem;
use App\Models\ArpItemHistorico;
use App\Models\CodigoItem;
use App\Repositories\Arp\ArpItemRepository;
use App\Services\Arp\AlteracaoArp\AlteracaoTipoCancelamentoItemService;
use App\Services\Arp\AlteracaoArp\AlteracaoTipoVigenciaService;
use Illuminate\Database\Eloquent\Collection;

class ArpAlteracaoService
{
    use ArpAlteracaoCancelamentoTrait;
    use ArpItemHistoricoTrait;

    private $alteracaoTipoCancelamentoItemService;
    private $alteracaoTipoVigenciaService;

    public function __construct(
        AlteracaoTipoCancelamentoItemService $alteracaoTipoCancelamentoItemService,
        AlteracaoTipoVigenciaService $alteracaoTipoVigenciaService
    ) {
        $this->alteracaoTipoCancelamentoItemService = $alteracaoTipoCancelamentoItemService;
        $this->alteracaoTipoVigenciaService = $alteracaoTipoVigenciaService;
    }

    public function montarInformacoesUpdate(array $dados, int $tipo, array $dadosAtaInicial)
    {
        $arpInicial =  ArpHistorico::where("arp_id", $dados['arp_id'])->oldest()->first()->toArray();
        $dadosAtaInicial['arp_alteracao_id'] = $dadosAtaInicial['arp_alteracao_id'] ?? $dados['id'];
        $dadosAtaInicial['tipo_id'] = $tipo;
        $dadosAtaInicial = array_merge($arpInicial, $dadosAtaInicial);

        return $dadosAtaInicial;
    }

    /**
     * Método responsável em cancelar os itens
     */
    public function montarInformacaoInicial(int $tipo, array $dados, $arpHistorico, string $metodo)
    {
        # Se o método for para criar, recupera as informações da tabela código para poder informar
        # no tipo que está sendo salvo
        if ($metodo == 'store') {
            $tipoInformado = CodigoItem::find($tipo);
            $arpHistorico['data_assinatura_alteracao_vigencia'] = $dados['data_assinatura_alteracao_vigencia'];
            $arpHistorico['tipo_id'] = $tipoInformado->id;
            $arpHistorico['descricao_tipo'] = $tipoInformado->descricao;

            return $arpHistorico;
        }

        # Se for no rascunho, recupera os dados do form e insere na model da Arp histórico
        if ($metodo == 'update') {
            $arpHistorico->data_assinatura_alteracao_vigencia = $dados['data_assinatura_alteracao_vigencia'];
            $arpHistorico->descricao_tipo = $arpHistorico->status->descricao;

            return $arpHistorico;
        }
    }

    public function realizarCancelamentoItem(
        ?ArpHistorico $arpHistorico,
        int $tipoAlteracao,
        array $dados,
        $dadosAtaInicial
    ) {
        if (empty($arpHistorico)) {
            $dadosAtaInicial = $this->montarInformacoesUpdate($dados, $tipoAlteracao, $dadosAtaInicial);
            return $this->alteracaoTipoCancelamentoItemService->alterarCancelamentoItem($dados, $dadosAtaInicial);
        }

        return $this->alteracaoTipoCancelamentoItemService->alterarCancelamentoItemUpdate($dados, $dadosAtaInicial);
    }

    public function realizarAlteracaoVigencia(
        ?ArpHistorico $arpHistorico,
        int $tipoAlteracao,
        array $dados,
        $dadosAtaInicial
    ) {
        if (empty($arpHistorico)) {
            $dadosAtaInicial = $this->montarInformacoesUpdate($dados, $tipoAlteracao, $dadosAtaInicial);
            return $this->alteracaoTipoVigenciaService->alterarVigencia($dados, $dadosAtaInicial);
        }

        return $this->alteracaoTipoVigenciaService->alterarVigenciaUpdate($dados, $dadosAtaInicial);
    }
}
