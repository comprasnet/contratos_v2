<?php

namespace App\Services\Arp;

use App\Repositories\Arp\ArpRepositoy;

class ArpService
{
    protected $arpRepositoy;
    
    public function __construct(ArpRepositoy $arpRepositoy)
    {
        $this->arpRepositoy = $arpRepositoy;
    }
    
    public function montarSelectNumeroCompraAta(string $textoDigitado, int $idUnidadeCompra)
    {
        return $this->arpRepositoy->montarSelectNumeroCompraAta($textoDigitado, $idUnidadeCompra)->paginate(10);
    }
    
    public function montarSelectModalidadeCompraAta(string $textoDigitado, int $idUnidadeCompra)
    {
        return $this->arpRepositoy->montarSelectModalidadeCompraAta($textoDigitado, $idUnidadeCompra)->paginate(10);
    }
}
