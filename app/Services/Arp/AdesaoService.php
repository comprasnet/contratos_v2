<?php

namespace App\Services\Arp;

use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\Formatador;
use App\Http\Traits\LogTrait;
use App\Http\Traits\UgPrimariaTrait;
use App\Models\Adesao;
use App\Models\ArpRemanejamento;
use App\Models\ArpSolicitacaoHistorico;
use App\Models\CompraItem;
use App\Models\AdesaoItem;
use App\Repositories\Arp\ArpAdesaoItemRepository;
use App\Repositories\Arp\ArpAdesaoRepository;
use App\Repositories\Compra\CompraItemUnidadeRepository;
use App\Services\Unidades\UnidadesService;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class AdesaoService
{
    use CompraTrait;
    use BuscaCodigoItens;
    use LogTrait;
    use Formatador;
    use UgPrimariaTrait;

    public function cancelarAdesao(int $idAdesao, int $idUsuario, int $idUnidadeLogado, string $justificativa)
    {
        DB::beginTransaction();
        try {
            $adesao = (new ArpAdesaoRepository())->getAdesaoUnico($idAdesao);
            $descresCancelamento = 'Cancelada pelo solicitante';
            if (in_array($adesao->situacao->descricao, ['Enviada para aceitação', 'Aguardando Aceitação do Fornecedor',
                'Aceita Parcialmente pelo Fornecedor'])
            ) {
                    $this->inserirInformacoesCancelamentoAdesao(
                        $adesao,
                        $justificativa,
                        $idUsuario,
                        $idUnidadeLogado,
                        $descresCancelamento
                    );
                
                DB::commit();
                
                return $this->montarRespostaAlertJs(
                    200,
                    'Adesão cancelada com sucesso',
                    "success"
                );
            }
            
            $compraItemUnidadeRepository = new CompraItemUnidadeRepository();
            $itensAdesao = $adesao->item;
            foreach ($itensAdesao as $itemAdesao) {
                $compraItemFornecedor = $itemAdesao->item_arp->item_fornecedor_compra;
                
                $itemCarona = $compraItemUnidadeRepository->getItemUnidadeCaronaAdesao(
                    $compraItemFornecedor->compra_item_id,
                    $adesao->unidade_origem_id,
                    $compraItemFornecedor->fornecedor_id
                );

                $itemCarona->quantidade_autorizada -= $itemAdesao->quantidade_aprovada;
                $itemCarona->quantidade_adquirir -= $itemAdesao->quantidade_aprovada;
                $itemCarona->save();
                
                $saldoUnidadeRetirar = $this->retornaSaldoAtualizado(
                    $itemCarona->compra_item_id,
                    $itemCarona->unidade_id,
                    $itemCarona->id
                );
                
                $itemCarona->quantidade_saldo = (int)$saldoUnidadeRetirar->saldo;
                $itemCarona->save();
            }
            
            if ($idUnidadeLogado == $adesao->unidade_gerenciadora_id) {
                $descresCancelamento = 'Cancelada pela gerenciadora';
            }
            
            $this->inserirInformacoesCancelamentoAdesao(
                $adesao,
                $justificativa,
                $idUsuario,
                $idUnidadeLogado,
                $descresCancelamento
            );
            
            DB::commit();
            
            return $this->montarRespostaAlertJs(200, 'Adesão cancelada com sucesso', "success");
        } catch (Exception $exception) {
            DB::rollBack();
            $this->inserirLogCustomizado('gestao_ata', 'error', $exception);
            return $this->montarRespostaAlertJs(
                500,
                'Erro ao realizar o cancelamento da adesão',
                "error"
            );
        }
    }
    
    public function inserirInformacoesCancelamentoAdesao(
        Adesao $adesao,
        string $justificativa,
        int $idUsuario,
        int $idUnidadeLogado,
        string $descCodItem
    ) {
        $idSituacaoCancelada =
            $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', $descCodItem);
        
        $adesao->status = $idSituacaoCancelada;
        $adesao->justificativa_cancelamento = $justificativa;
        $adesao->unidade_id_cancelamento = $idUnidadeLogado;
        $adesao->usuario_id_cancelamento = $idUsuario;
        $adesao->data_cancelamento = Carbon::now()->format('Y-m-d H:i:s');
        $adesao->save();
    }
    
    /**
     * Método responsável em montar as colunas da tabela para exibir os items da adesão
     */
    public function camposTabelaItemAdesao(string $unidadeSolicitante = null)
    {
        $colunas = [
            'botaoitem' => '<i class="fas fa-info-circle" title="Digitar uma quantidade para selecionar o item"></i>',
            'unidadeadesao' => 'Unidade Gerenciadora',
            'numerocompras' => 'Número da compra/Ano',
            'modalidadeadesao' => 'Modalidade',
            'grupo_compra_tabela' => 'Grupo',
            'numeroata' => 'Nº ata',
            'nomefornecedor' => 'Fornecedor (Classificação)',
            'numero' => 'Nº item',
            'descricaodetalhada' => 'Descrição',
            'descresitem' => 'Tipo',
            'quantidaderegistrada' => 'Quantidade Registrada',
            'codigo_item' => 'Codigo Item',
            'valorunitario' => 'Valor Unitário',
            'vigencia' => 'Vigência',
            'maximoadesao' => 'Quantidade Disponível para Adesão',
            'quantidadesolicitada' => 'Quantidade Solicitada',
            'valornegociado' => 'Valor Total para Adesão',
        ];
        
        return $colunas;
    }
    
    public function camposTabelaAnalisarItemAdesao(
        string $unidadeSolicitante = null,
        bool $fornecedorLogado = null,
        $itemFornecedor = null
    ) {
        $column = [
            'statusitem' => 'Status',
            'numeroata' => 'Nº ata',
            'numero' => 'Nº item',
            'descricaodetalhada' => 'Descrição',
            'nomefornecedor' => 'Fornecedor',
            'quantidaderegistrada' => 'Quantidade Registrada',
            'valorunitario' => 'Valor unitário',
            'vigencia' => 'Vigência',
            'maximoadesao' => 'Quantidade disponível para adesão',
            'quantidadesolicitada' => 'Quantidade Solicitada',
            'quantidadeaprovadafornecedor' => 'Quantidade Aceita pelo Fornecedor',
            'motivoanalisefornecedor' => 'Justificativa/Motivação Fornecedor',
            'quantidadeaprovada' => 'Analisar',
            'motivoanalise' => 'Justificativa/Motivação',
        ];

        if (($itemFornecedor->first() && $itemFornecedor->first()->quantidade_anuencia_fornecedor === null)
            ||
            $fornecedorLogado
        ) {
            unset($column['quantidadeaprovadafornecedor'], $column['motivoanalisefornecedor']);
        }

        if ($this->unidadeUsuarioPertenceCentralCompras($unidadeSolicitante)) {
            unset($column['maximoadesao']);
        }
        
        return $column;
    }

    public function retornarQuantidadeMenorParaSolicitar(
        ?float $maximoAdesaoItem,
        float $quantidadeHomologadaVencedorFornecedor,
        ArpAdesaoRepository $arpAdesaoRepository,
        int $compraItemId,
        int $unidadeSolicitanteId,
        float $quantidadeItem,
        ?bool $execucaoDescentralizadaProgramaProjetoFederal
    ) {
        $quantidadeTotalAprovadaTodasUnidades = $this->retornarQuantidadeTotalAprovadaPorItem($compraItemId);

        $saldoTodasUnidades =
            $this->calcularSaldoMaximoAdesaoTodasUnidades($maximoAdesaoItem, $quantidadeTotalAprovadaTodasUnidades);
        if ($saldoTodasUnidades <= 0) {
            return 0;
        }

        $quantidadeTotalAprovadaAdesao =
            $this->retornarQuantidadeTotalAprovadaPorItemUnidadeSolicitante(
                $arpAdesaoRepository,
                $compraItemId,
                $unidadeSolicitanteId
            );

        if ($execucaoDescentralizadaProgramaProjetoFederal) {
            $saldoTodasUnidades = $maximoAdesaoItem;
        }

        $quantidadeMenorMaximoAdesao = min($saldoTodasUnidades, $maximoAdesaoItem);

        $quantidadeMaximaUnidade =
            $this->calcularQuantidadeMaximaCarona($quantidadeItem) - $quantidadeTotalAprovadaAdesao;

        if ($quantidadeMaximaUnidade <= 0) {
            return 0;
        }

        return min($quantidadeMaximaUnidade, $quantidadeMenorMaximoAdesao);
    }

    public function queryBaseTotalQuantidadeAprovada(
        ArpAdesaoRepository $arpAdesaoRepository,
        int $compraItemId,
        int $fornecedorId
    ) {
        return $arpAdesaoRepository->queryBaseTotalQuantidadeAprovada($compraItemId, $fornecedorId);
    }

    public function retornarQuantidadeTotalAprovadaPorItem(
        int $compraItemId
    ) {
        return  (new CompraItemUnidadeRepository())->getItemUnidadePorTipoUasg($compraItemId, ['C'])
            ->sum('quantidade_autorizada');
    }

    public function retornarQuantidadeTotalAprovadaPorItemFornecedorUnidadeSolicitante(
        ArpAdesaoRepository $arpAdesaoRepository,
        int $compraItemId,
        int $fornecedorId,
        int $unidadeSolicitanteId
    ) {
        return $this->queryBaseTotalQuantidadeAprovada(
            $arpAdesaoRepository,
            $compraItemId,
            $fornecedorId
        )->where('arp_solicitacao.unidade_origem_id', $unidadeSolicitanteId)
            ->sum('quantidade_aprovada');
    }

    public function calcularSaldoFornecedorAceitoAdesao(
        float $quantidadeMenorParaSolicitar,
        float $quantidadeTotalAprovadaAdesao
    ) {
        return $quantidadeMenorParaSolicitar - $quantidadeTotalAprovadaAdesao;
    }

    public function menorQuantidadeUnidadeSolicitante(
        float $maximoAdesaoItem,
        float $quantidadeHomologadaVencedorFornecedor
    ) {
        $percentualMaximoAdesao = 0.5;
        $resultadoMaximoAdesao = $maximoAdesaoItem * $percentualMaximoAdesao;

        return min($resultadoMaximoAdesao, $quantidadeHomologadaVencedorFornecedor);
    }

    public function calcularQuantidadeRestanteUnidadeSolicitante(
        float $quantidadeMenorUnidadeSolicitante,
        float $quantidadeTotalAprovadaAdesaoPorUnidade
    ) {
        return $quantidadeMenorUnidadeSolicitante - $quantidadeTotalAprovadaAdesaoPorUnidade;
    }

    public function retornarMenorMaximoAdesaoItemFornecedor(
        float $quantidadeMenorUnidadeSolicitante,
        float $quantidadeSaldoFornecedorAceitoAdesao
    ) {
        return min($quantidadeMenorUnidadeSolicitante, $quantidadeSaldoFornecedorAceitoAdesao);
    }

    public function aquisicaoMedicamentoMaterialSelecionado(?bool $aquisicaoEmergencialMedicamentoMaterial)
    {
        if ($aquisicaoEmergencialMedicamentoMaterial === null) {
            return false;
        }

        return $aquisicaoEmergencialMedicamentoMaterial;
    }

    public function usuarioPodeSelecionarItemExcecao(
        bool $aquisicaoMedicamentoMaterialSelecionado,
        float $maximoAdesao
    ) {
        if ($maximoAdesao <= 0 && !$aquisicaoMedicamentoMaterialSelecionado) {
            return false;
        }

        return true;
    }

    public function usuarioPodeSelecionarItemExcecaoCarona(
        bool $aquisicaoMedicamentoMaterialSelecionado,
        float $maximoAdesao
    ) {
        if ($maximoAdesao <= 0 && !$aquisicaoMedicamentoMaterialSelecionado) {
            return false;
        }

        return true;
    }

    public function usuarioPodeSelecionarItemExcecaoValidacaoCarona(
        bool $aquisicaoMedicamentoMaterialSelecionado,
        float $maximoAdesao
    ) {
        if ($maximoAdesao < 0 && !$aquisicaoMedicamentoMaterialSelecionado) {
            return false;
        }

        return true;
    }

    public function calcularQuantidadeSaldoExcecao(
        float $maximoAdesaoItem,
        int $quantidadeHomologadaVencedorFornecedor,
        ArpAdesaoRepository $arpAdesaoRepository,
        int $compraItemId,
        int $unidadeSolicitanteId
    ) {
        $quantidadeTotalAprovadaAdesao =
            $this->retornarQuantidadeTotalAprovadaPorItemUnidadeSolicitante(
                $arpAdesaoRepository,
                $compraItemId,
                $unidadeSolicitanteId
            );

        $quantidadeMenorUnidadeLogada =
            $this->menorQuantidadeUnidadeSolicitante(
                $maximoAdesaoItem,
                $quantidadeHomologadaVencedorFornecedor
            );

        $quantidadeMenorUnidadeLogada = $this->calcularQuantidadeRestanteUnidadeSolicitante(
            $quantidadeMenorUnidadeLogada,
            $quantidadeTotalAprovadaAdesao
        );

        $quantidadeMenorUnidadeLogada = $this->calcularQuantidadeMaximoPorRemanejamento(
            $unidadeSolicitanteId,
            $compraItemId,
            $quantidadeMenorUnidadeLogada
        );

        return (int) ceil($quantidadeMenorUnidadeLogada);
    }

    public function calcularMaximoAdesaoPorUnidadeSolicitante(
        int $fornecedorId,
        ?float $maximoAdesaoItem,
        float $quantidadeHomologadaVencedorFornecedor,
        ArpAdesaoRepository $arpAdesaoRepository,
        int $compraItemId,
        int $unidadeSolicitanteId,
        float $quantidadeItem,
        ?bool $execucaoDescentralizadaProgramaProjetoFederal
    ) {
        $quantidadeMenorAnalise = $this->retornarQuantidadeMenorParaSolicitar(
            $maximoAdesaoItem,
            $quantidadeHomologadaVencedorFornecedor,
            $arpAdesaoRepository,
            $compraItemId,
            $unidadeSolicitanteId,
            $quantidadeItem,
            $execucaoDescentralizadaProgramaProjetoFederal
        );

        if ($quantidadeMenorAnalise <= 0) {
            return 0;
        }

//        $quantidadeTotalAprovadaAdesao =
//            $this->retornarQuantidadeTotalAprovadaPorItemUnidadeSolicitante(
//                $arpAdesaoRepository,
//                $compraItemId,
//                $unidadeSolicitanteId
//            );
//        dd($quantidadeMenorAnalise, $quantidadeTotalAprovadaAdesao);
//
//        $quantidadeSaldoFornecedorAceitoAdesao =
//            $this->calcularSaldoFornecedorAceitoAdesao(
//                $quantidadeMenorAnalise,
//                $quantidadeTotalAprovadaAdesao
//            );

        $quantidadeMenorUnidadeLogada =
            $this->menorQuantidadeUnidadeSolicitante($maximoAdesaoItem, $quantidadeMenorAnalise);

        # Recupera a menor quantidade entre os 50% permitidos para a unidade e o saldo que foi aprovado
        $quantidadeMinimaAnalise = $this->retornarMenorMaximoAdesaoItemFornecedor(
            $quantidadeMenorUnidadeLogada,
            $quantidadeMenorAnalise
        );

        $quantidadeMinimaAnalise = $this->calcularQuantidadeMaximoPorRemanejamento(
            $unidadeSolicitanteId,
            $compraItemId,
            $quantidadeMinimaAnalise
        );

        # arrendodar para cima a quantidade
        return (int) ceil($quantidadeMinimaAnalise);
    }

    public function retornarQuantidadeTotalAprovadaPorItemUnidadeSolicitante(
        ArpAdesaoRepository $arpAdesaoRepository,
        int $compraItemId,
        int $unidadeSolicitanteId
    ) {
        return $arpAdesaoRepository->queryBaseTotalQuantidadeAprovadaPorItemCompra($compraItemId)
            ->where('arp_solicitacao.unidade_origem_id', $unidadeSolicitanteId)
            ->sum('quantidade_aprovada');
    }

    public function retornarQuantidadeAprovadaRemanejamento(int $unidadeSolicitanteId, $compraItemId)
    {
        return ArpRemanejamento::join(
            'arp_remanejamento_itens',
            'arp_remanejamento.id',
            'arp_remanejamento_itens.remanejamento_id'
        )->join('codigoitens', 'arp_remanejamento_itens.situacao_id', 'codigoitens.id')
            ->join('compra_item_unidade', function ($query) {
                $query->on('compra_item_unidade.unidade_id', 'arp_remanejamento.unidade_solicitante_id')
                    ->where('compra_item_unidade.situacao', true)
                    ->whereColumn('arp_remanejamento_itens.id_item_unidade_receber', 'compra_item_unidade.id');
            })
            ->where('arp_remanejamento.rascunho', false)
            ->whereIN('codigoitens.descricao', ['Item Aceito', 'Item Aceito Parcialmente'])
            ->whereNotNull('data_aprovacao_gerenciadora')
            ->where('arp_remanejamento.unidade_solicitante_id', $unidadeSolicitanteId)
            ->where('compra_item_unidade.compra_item_id', $compraItemId)
            ->where('compra_item_unidade.tipo_uasg', 'C')
            ->sum('arp_remanejamento_itens.quantidade_aprovada_gerenciadora');
    }

    public function calcularQuantidadeMaximoPorRemanejamento(
        int $unidadeSolicitanteId,
        $compraItemId,
        float $quantidadeMinimaAnalise
    ) {
        $quantidadeAprovadaRemanejamento =
            $this->retornarQuantidadeAprovadaRemanejamento($unidadeSolicitanteId, $compraItemId);

        $quantidadeMinimaAnalise -= $quantidadeAprovadaRemanejamento;

        return $quantidadeMinimaAnalise;
    }

    public function validarItemCaronaRequest(
        int $fornecedorId,
        CompraItem $itemSelecionado,
        float $quantidadeHomologadaVencedor,
        ArpAdesaoRepository $arpAdesaoRepository,
        int $unidadeSolicitacao,
        ?bool $execucaoDescentralizadaProgramaProjetoFederal,
        ?bool $aquisicaoEmergencialMedicamentoMaterial,
        float $quantidadeTotalSolicitada
    ) {
        $maximoAdesao = $this->calcularMaximoAdesaoPorUnidadeSolicitante(
            $fornecedorId,
            $itemSelecionado->maximo_adesao,
            $quantidadeHomologadaVencedor,
            $arpAdesaoRepository,
            $itemSelecionado->id,
            $unidadeSolicitacao,
            $itemSelecionado->qtd_total,
            $execucaoDescentralizadaProgramaProjetoFederal
        );

        if ($maximoAdesao < $quantidadeTotalSolicitada) {
            return false;
        }

        $aquisicaoMedicamentoMaterialSelecionadoProjetoFederalMarcado =
            $this->aquisicaoMedicamentoMaterialSelecionado($aquisicaoEmergencialMedicamentoMaterial);

        $itemPodeSerSelecionadoExcecao =
            $this->usuarioPodeSelecionarItemExcecaoCarona(
                $aquisicaoMedicamentoMaterialSelecionadoProjetoFederalMarcado,
                $maximoAdesao
            );

        if (!$itemPodeSerSelecionadoExcecao) {
            return false;
        }

        if ($aquisicaoMedicamentoMaterialSelecionadoProjetoFederalMarcado) {
            $maximoAdesao = $this->calcularQuantidadeSaldoExcecao(
                $itemSelecionado->maximo_adesao,
                $quantidadeHomologadaVencedor,
                $arpAdesaoRepository,
                $itemSelecionado->id,
                $unidadeSolicitacao
            );

            if ($maximoAdesao < $quantidadeTotalSolicitada) {
                return false;
            }
        }

        return true;
    }

    public function calcularSaldoMaximoAdesaoTodasUnidades(?float $maximoAdesaoItem, float $quantidadeTotalAprovada)
    {
        $maximoAdesaoItem -= $quantidadeTotalAprovada;

        return $maximoAdesaoItem;
    }

    public function calcularQuantidadeMaximaCarona(float $quantidadeItem)
    {
        return $quantidadeItem * 0.5;
    }

    public function itemValidoParaCaronaRule(
        int $compraItemId,
        int $compraItemFornecedorId,
        ArpAdesaoRepository $arpAdesaoRepository,
        int $unidadeOrigemId,
        ?bool $execucaoDescentralizadaProgramaProjetoFederal,
        ?bool $aquisicaoEmergencialMedicamentoMaterial,
        float $quantidadeTotalSolicitada
    ) {
        $itemSelecionado = CompraItem::find($compraItemId);
        $quantidadeHomologadaVencedorTodosItens = $itemSelecionado->qtd_total * 0.5;

        return $this->validarItemCaronaRequest(
            $compraItemFornecedorId,
            $itemSelecionado,
            $quantidadeHomologadaVencedorTodosItens,
            $arpAdesaoRepository,
            $unidadeOrigemId,
            $execucaoDescentralizadaProgramaProjetoFederal,
            $aquisicaoEmergencialMedicamentoMaterial,
            $quantidadeTotalSolicitada
        );
    }

    public function retornarSituacaoAdesao(bool $rascunho, ?bool $enviarFornecedor)
    {

        if ($enviarFornecedor) {
            return $this->retornaIdCodigoItem(
                'Situação Anuência ARP Fornecedor',
                'Aguardando Aceitação do Fornecedor'
            );
        }

        $descricaoCodigoItem = 'Enviada para aceitação';
        if ($rascunho) {
            $descricaoCodigoItem = 'Em elaboração';
        }

        return $this->retornaIdCodigoItem('Tipo de Ata de Registro de Preços', $descricaoCodigoItem);
    }

    public function salvarHistorico($idSolicitacao, $idStatus)
    {

        $historico = new ArpSolicitacaoHistorico();
        $historico->arp_solicitacao_id = $idSolicitacao;
        $historico->usuario_id = backpack_user()->id;
        $historico->unidade_id = session('user_ug_id');
        $historico->situacao_id = $idStatus;


        $historico->save();
    }

    public function exibirCampoExecucaoDescentralizadaProgramaProjetoFederal(
        int $unidadeSolicitanteId,
        int $unidadeGerenciadoraCompraId
    ) {
        $esferaUnidadeSolicitanteExibirCampo = ['Estadual', 'Municipal'];

        $unidadeService = new UnidadesService();
        $esferaUnidadeSolicitante = $unidadeService->getEsferaUnidade($unidadeSolicitanteId);
        $esferaUnidadeGerenciadoraCompra =
            $unidadeService->getEsferaUnidade($unidadeGerenciadoraCompraId);

        if (in_array($esferaUnidadeSolicitante, $esferaUnidadeSolicitanteExibirCampo) &&
            $esferaUnidadeGerenciadoraCompra == 'Federal') {
            return true;
        }

        return false;
    }

    public function getSolicitacaoAdesaoAceitaPorItemAta(int $idItemAta)
    {
        return (new ArpAdesaoItemRepository())->getSolicitacaoAdesaoAceitaPorItemAta($idItemAta);
    }

    public function itemFornecedorPendenteAnaliseAdesao(Collection $adesaoAtaPorItem)
    {
        $situacaoAdesao = array();
        $adesaoAtaPorItem->each(function ($itemRemanejamento) use (&$situacaoAdesao) {
            $situacaoAdesao[] = $itemRemanejamento->adesao->getSituacao();
        });

        $situacaoAdesao = array_unique($situacaoAdesao);

        return in_array('Enviada para aceitação', $situacaoAdesao);
    }
}
