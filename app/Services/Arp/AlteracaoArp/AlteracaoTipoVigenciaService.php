<?php

namespace App\Services\Arp\AlteracaoArp;

use App\Http\Traits\Arp\ArpItemHistoricoTrait;
use App\Models\Arp;
use App\Models\ArpHistorico;
use App\Models\ArpItem;
use Illuminate\Database\Eloquent\Collection;

class AlteracaoTipoVigenciaService
{
    use ArpItemHistoricoTrait;

    public function alterarVigencia(array $dadosFormulario, array $dadosHistoricoArp)
    {
        $itemInicial = ArpItem::where("arp_id", $dadosFormulario['arp_id'])->get();

        $itemHistorico = clone $itemInicial;

        $itemHistorico = $itemHistorico->pluck("id")->toArray();
        $dadosHistoricoArp['data_assinatura_alteracao_inicio_vigencia'] =
            $dadosHistoricoArp['vigencia_inicial'];

        $dadosHistoricoArp['data_assinatura_alteracao_fim_vigencia'] =
            $dadosFormulario['data_assinatura_alteracao_fim_vigencia'];

        $dadosHistoricoArp['amparo_legal_renovacao_quantitativo'] =
            $dadosFormulario['amparo_legal_renovacao_quantitativo'] ?? null;

        $dadosHistoricoArp['quantitativo_renovado_vigencia'] =
            $dadosFormulario['quantitativo_renovado_vigencia'] ?? false;

        $dadosHistoricoArp['justificativa_alteracao_vigencia'] = $dadosFormulario['justificativa_alteracao_vigencia'];

        $arpHistorico = ArpHistorico::create($dadosHistoricoArp);

        $this->inserirItemArpHistorico(
            $itemHistorico,
            $arpHistorico->id,
            $dadosHistoricoArp['data_assinatura_alteracao_inicio_vigencia'],
            $dadosHistoricoArp['data_assinatura_alteracao_fim_vigencia']
        );

        $this->atualizarVigenciaItemPNCP(
            $itemInicial,
            $dadosFormulario['rascunho'],
            $dadosFormulario['arp_id'],
            $dadosHistoricoArp['data_assinatura_alteracao_inicio_vigencia'],
            $dadosHistoricoArp['data_assinatura_alteracao_fim_vigencia']
        );

        return $arpHistorico;
    }

    public function alterarVigenciaUpdate(array $dadosFormulario, ArpHistorico $dadosHistoricoArp)
    {
        # Remove o tipo de descrição devido não conter essa coluna no banco
        unset($dadosHistoricoArp['descricao_tipo']);
        $dadosHistoricoArp->data_assinatura_alteracao_fim_vigencia =
            $dadosFormulario['data_assinatura_alteracao_fim_vigencia'];

        $dadosHistoricoArp->quantitativo_renovado_vigencia =
            $dadosFormulario['quantitativo_renovado_vigencia'] ?? false;

        $dadosHistoricoArp->amparo_legal_renovacao_quantitativo =
            $dadosFormulario['amparo_legal_renovacao_quantitativo'] ?? null;

        $dadosHistoricoArp->justificativa_alteracao_vigencia = $dadosFormulario['justificativa_alteracao_vigencia'];

        $dadosHistoricoArp->save();

        $this->atualizarVigenciaItemPNCP(
            $dadosHistoricoArp->item,
            $dadosFormulario['rascunho'],
            $dadosHistoricoArp->arp_id,
            $dadosHistoricoArp->data_assinatura_alteracao_inicio_vigencia,
            $dadosHistoricoArp->data_assinatura_alteracao_fim_vigencia
        );

        return $dadosHistoricoArp;
    }

    public function atualizarVigenciaItemPNCP(
        Collection $itensAta,
        bool $rascunho,
        int $idArp,
        $dataAssinaturaAlteracaoInicioVigencia,
        $dataAssinaturaAlteracaoFimVigencia
    ) {
        foreach ($itensAta as $item) {
            # Recuperar as informações se o item foi cancelado
            $arrayItemCancelado = $item->itemHistorico->pluck('item_cancelado')->toArray();

            # Não pode atualizar a vigência para o item que sofreu cancelamento
            if (in_array(true, $arrayItemCancelado)) {
                continue;
            }

            # Se for diferente de rascunho, salva a vigência alterada para os itens da ata
            if (!$rascunho) {
                # Altera a data inicial de vigência
                $item->item_fornecedor_compra->ata_vigencia_inicio = $dataAssinaturaAlteracaoInicioVigencia;

                # Altera a data final de vigência
                $item->item_fornecedor_compra->ata_vigencia_fim = $dataAssinaturaAlteracaoFimVigencia;

                # Salva as alterações
                $item->item_fornecedor_compra->save();
            }
        }

        # Se for diferente de rascunho, altera a data final de vigência
        if (!$rascunho) {
            # Recupera a ata principal para atualizar a vigência final
            $arpPrincipal = Arp::find($idArp);

            # Altera a data da vigência final
            $arpPrincipal->vigencia_final = $dataAssinaturaAlteracaoFimVigencia;
            $arpPrincipal->save();
        }
    }
}
