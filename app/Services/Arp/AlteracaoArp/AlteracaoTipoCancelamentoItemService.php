<?php

namespace App\Services\Arp\AlteracaoArp;

use App\Http\Traits\Arp\ArpAlteracaoCancelamentoTrait;
use App\Models\Arp;
use App\Models\ArpHistorico;
use App\Models\ArpItem;
use App\Models\ArpItemHistorico;
use App\Repositories\Arp\ArpItemRepository;

class AlteracaoTipoCancelamentoItemService
{
    use ArpAlteracaoCancelamentoTrait;
    public function alterarCancelamentoItem(array $dadosFormulario, array $dadosHistoricoArp)
    {
        if (!array_key_exists('item_cancelado', $dadosFormulario) || $dadosFormulario['item_cancelado'] == '[]') {
            return;
        }

        # Criação do registro do item no histórico
        $dadosHistoricoArp['justificativa_motivo_cancelamento'] = $dadosFormulario['justificativa_motivo_cancelamento'];
        $arpHistorico = ArpHistorico::create($dadosHistoricoArp);

        $this->salvarItemAlteracaoCancelamento(
            $dadosFormulario['item_cancelado'],
            $arpHistorico->id,
            $dadosFormulario['data_assinatura_alteracao_vigencia'],
            $dadosFormulario['rascunho'],
            $dadosFormulario['arp_id']
        );

        return $arpHistorico;
    }

    public function alterarCancelamentoItemUpdate(array $dadosFormulario, ArpHistorico $dadosHistoricoArp)
    {
        # Se não tiver nenhum item selecionado, sai do método
        if (!isset($dadosFormulario['item_cancelado'])) {
            return;
        }

        $dadosHistoricoArp->justificativa_motivo_cancelamento = $dadosFormulario['justificativa_motivo_cancelamento'];
        # Remove a descrição para que não aconteça erro ao atualizar
        unset($dadosHistoricoArp['descricao_tipo']);
        $dadosHistoricoArp->save();

        # Salve as mudanças no banco de dados
        # Se existir registros na histórico, recuperamos os campos básicos para os próximos insert
        # E apaga os os antigos para inserir os novos marcados
        if (!empty($dadosHistoricoArp->item_historico)) {
            foreach ($dadosHistoricoArp->item_historico as $itemHistoricoDeletar) {
                $itemHistoricoDeletar->forceDelete();
            }
        }

        $this->salvarItemAlteracaoCancelamento(
            $dadosFormulario['item_cancelado'],
            $dadosHistoricoArp->id,
            $dadosFormulario['data_assinatura_alteracao_vigencia'],
            $dadosFormulario['rascunho'],
            $dadosFormulario['arp_id']
        );

        return $dadosHistoricoArp;
    }

    public function salvarItemAlteracaoCancelamento(
        array $itemCancelado,
        int $idArpHistorico,
        ?string $dataAssinaturaAlteracaoVigencia,
        bool $rascunho,
        int $arpId
    ) {
        # Remove os valores que não foram utilizados
        $arrayItemCancelado = array_filter($itemCancelado);
        # Percorre os itens informados pelo usuário

        $arrayItensAta = array();
        $arpItemRepository = new ArpItemRepository();
        $valorTotalDosItensRemovidos = 0 ;

        foreach ($arrayItemCancelado as $numeroItemAlterado => $valorItemAlteracao) {
            $item = ArpItem::find($numeroItemAlterado);
            $dadosItem = $item->toArray();

            $dadosItem['vigencia_inicial'] = null;
            $dadosItem['vigencia_final'] = null;
            $dadosItem['arp_historico_id'] = $idArpHistorico;
            $dadosItem['arp_item_id'] = $item->id;
            $dadosItem['valor'] = 0;
            $dadosItem['item_cancelado'] = $valorItemAlteracao;

            # Se a ação não for um rascunho, atualiza a tabela fornecedor
            if (!$rascunho) {
                $itemDeCompra = $arpItemRepository->getItemDeCompraDoItemDaAta($dadosItem['arp_item_id']);
                $valorTotalDosItensRemovidos += $itemDeCompra->valor_negociado_desconto;

                # Altera a data fim de vigência do item do fornecedor
                $item->item_fornecedor_compra->ata_vigencia_fim = $dataAssinaturaAlteracaoVigencia;
                $item->item_fornecedor_compra->save();
            }

            ArpItemHistorico::create($dadosItem);
        }

        if (!$rascunho) {
            $totalItensAta = ArpItem::where("arp_id", $arpId)->count();

            $this->verificarCancelamentoArp($arpId, $totalItensAta);
        }

        $this->recalcularValorAtaCancelamento(
            $rascunho,
            $arpId,
            $valorTotalDosItensRemovidos
        );
    }

    private function recalcularValorAtaCancelamento(bool $ataEmRascunho, int $arpId, float $valorTotalDosItensRemovidos)
    {
        if (!$ataEmRascunho && $valorTotalDosItensRemovidos > 0) {
            $ata = Arp::find($arpId);

            # Subtrai, do valor total da ata, o valor total dos itens removidos
            $ata->valor_total -= $valorTotalDosItensRemovidos;

            # Atualiza o valor total da ata
            $ata->save();
        }
    }
}
