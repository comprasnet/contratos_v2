<?php

namespace App\Services;

use App\Api\Externa\ApiCatalogoCompras;
use App\Models\Catmatpdms;
use App\Models\Catmatseritem;
use App\Models\Contratoitem;
use App\Repositories\CatmatPdmRepository;
use App\Repositories\UnidadeMedidaRepository;

class ContratoItemService
{
    /**
     * @param Contratoitem $contratoItem
     * @return array
     */
    public static function getUnidades($contratoItem)
    {
        $catmatserItem = Catmatseritem::find($contratoItem->catmatseritem_id);
        $code = $catmatserItem->codigo_siasg;
        $tipoContratoItem = $contratoItem->tipo->descres;

        $type = UnidadeMedidaRepository::TYPE_SERVICO;
        if ($tipoContratoItem == 'MATERIAL') {
            $type = UnidadeMedidaRepository::TYPE_MATERIAL;
            self::setCatmatPdmIfCatematpdmIdIsNullByApi($catmatserItem);
            if (!is_null($catmatserItem->catmatpdm_id)) {
                $catmatpdm = Catmatpdms::find($catmatserItem->catmatpdm_id);
                if ($catmatpdm) {
                    $code = $catmatpdm->codigo;
                }
            }
        }
        return UnidadeMedidaService::createByApiAndGet($type, $code);
    }

    /**
     * @param Catmatseritem $catmatserItem
     * @return void
     * @throws \Exception
     */
    private static function setCatmatPdmIfCatematpdmIdIsNullByApi($catmatserItem)
    {
        if (is_null($catmatserItem->catmatpdm_id)) {
            $code = (new ApiCatalogoCompras())->getPdmMaterialByCode(
                $catmatserItem->codigo_siasg
            );
            $catmatPdmItem = CatmatPdmRepository::getPDMByCode($code);
            if (!is_null($catmatPdmItem)) {
                $catmatserItem->catmatpdm_id = $catmatPdmItem->id;
                $catmatserItem->save();
            }
        }
    }
}
