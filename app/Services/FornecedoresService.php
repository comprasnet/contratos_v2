<?php

namespace App\Services;

use App\Models\Fornecedor;
use App\Repositories\Fornecedor\FornecedorRepository;

class FornecedoresService
{
    public function recuperarNomeFornecedorPorCpfCnpj(string $cpfCnpj): ?string
    {
        $fornecedor = (new FornecedorRepository())->recuperarFornecedorPorCpfCnpj($cpfCnpj);
        
        if (empty($fornecedor)) {
            return $fornecedor;
        }
        
        return $fornecedor->nome;
    }
    
    public function inserirFornecedorAlteracaoAta(string $cpfCnpj, string $nome)
    {
        $digitosCpfCnpj = preg_replace('/[^0-9]/', '', $cpfCnpj);
        
        $tipoFornecedor = 'FISICA';
        
        if (strlen($digitosCpfCnpj) > 11) {
            $tipoFornecedor = 'JURIDICA';
        }
        
        $dadosInserir = [
            'tipo_fornecedor' => $tipoFornecedor,
            'cpf_cnpj_idgener' => $cpfCnpj,
            'nome' => $nome
        ];
        
        return (new FornecedorRepository())->inserirFornecedor($dadosInserir);
    }
    
    public function recuperarFornecedorPorCpfCnpj(string $cpfCnpj): ?Fornecedor
    {
        return (new FornecedorRepository())->recuperarFornecedorPorCpfCnpj($cpfCnpj);
    }
}
