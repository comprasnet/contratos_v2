<?php

namespace App\Services;

use Dompdf\Dompdf;

class RelatorioPdfService extends Dompdf
{
    const LANDSCAPE = 'landscape';
    const PORTRAIT = 'portrait';

    public function addNumber($numeroAtaTexto, $unidadeAtaTexto, $dataGeracao, $titulo = 'Ata de Registro de Preços')
    {
        $font = $this->getFontMetrics()->get_font(
            "helvetica",
            "normal"
        );
        $canvas = $this->getCanvas();
        $pageCount = $this->getCanvas()->get_page_count();
        if ($this->getPaperOrientation() == self::LANDSCAPE) {
            $canvas->page_text(
                800,
                565,
                "{PAGE_NUM}/{PAGE_COUNT}",
                $font,
                10,
                [0, 0, 0]
            );
            $this->drawBottomBorder(820, 565);
            $canvas->page_text(
                580,
                566,
                "$titulo nº $numeroAtaTexto - $unidadeAtaTexto - P. ",
                $font,
                9,
                [0, 0, 0]
            );
            $canvas->page_text(
                40,
                566,
                "Relatório gerado através do Contratos.gov.br em $dataGeracao",
                $font,
                9,
                [0, 0, 0]
            );

            return;
        }
        $canvas->page_text(
            565,
            820,
            "{PAGE_NUM}/{PAGE_COUNT}",
            $font,
            18,
            [0, 0, 0]
        );
        $this->drawBottomBorder(565, 820);
        $canvas->page_text(50, 820, "", $font, 18, [0, 0, 0]);
    }
    protected function drawBottomBorder($x, $y)
    {
        $canvas = $this->getCanvas();
        $grayColor = [204, 204, 204];
        ##Definir a cor em formato decimal
        $grayColorDec = array_map(function ($color) {
            return $color / 255;
        }, $grayColor);

        $canvas->line(
            $x - 15,
            $y + 24,
            $x + 152,
            $y + 23,
            $grayColorDec,
            2
        );
    }
    public static function create($paper = 'A4', $orientation = self::PORTRAIT)
    {
        $dompdf = new static();
        $dompdf->setPaper($paper, $orientation);
        return $dompdf;
    }
}
