<?php

namespace App\Services\ModelSignatario;

use App\Models\CodigoItem;
use App\Services\ModelSignatario\ModelSignatarioService;

class TermoRecebimentoDefinitivoAssinador extends ModelSignatarioService
{
    public function assinar(SignatarioDTO $signatarioDTO)
    {
        parent::assinar($signatarioDTO);

        $funcao = $signatarioDTO->signatario->funcao;
        if (!empty($funcao) &&
            in_array(
                $funcao->descres,
                ['AUTCOMP', 'GESTOR', 'GESTORSUB']
            )
        ) {
            $descres = 'trd_status_5';
            if ($this->modelDTO->model->incluir_instrumento_cobranca) {
                $descres = 'trd_status_4';
            }
            $status = CodigoItem::where('descres', $descres)->first();
            $this->modelDTO->model->update(['situacao_id' => $status->id]);
        }
    }
}
