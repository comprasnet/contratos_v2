<?php

namespace App\Services\ModelSignatario;

use App\Models\CodigoItem;
use App\Services\ModelSignatario\ModelSignatarioService;

class TermoRecebimentoProvisorioAssinador extends ModelSignatarioService
{
    public function assinar(SignatarioDTO $signatarioDTO)
    {
        parent::assinar($signatarioDTO);

        $funcao = $signatarioDTO->signatario->funcao;
        if (!empty($funcao) &&
            in_array(
                $funcao->descres,
                ['AUTCOMP', 'FSCTEC', 'FSCTECSUB']
            )
        ) {
            $statusEmExecucao = CodigoItem::where('descres', 'trp_status_3')->first();
            $this->modelDTO->model->update(['situacao_id' => $statusEmExecucao->id]);
        }
    }
}
