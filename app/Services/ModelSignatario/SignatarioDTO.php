<?php

namespace App\Services\ModelSignatario;

use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoExecucaoEntrega;
use App\Models\AutorizacaoexecucaoHistorico;
use App\Models\Contratopreposto;
use App\Models\Contratoresponsavel;
use Illuminate\Database\Eloquent\Model;

class SignatarioDTO
{
    public $signatario;
    public $fkColumn;

    public function __construct(Model $model)
    {
        switch (get_class($model)) {
            case Contratoresponsavel::class:
                $this->fkColumn = 'signatario_contratoresponsavel_id';
                break;
            case Contratopreposto::class:
                $this->fkColumn = 'signatario_contratopreposto_id';
                break;

//          Adicione aqui os outros signatários seguindo o mesmo padrao
//            case SuaModel::class:
//                $this->fkColumn = 'signatario_sua_model_id'; // fk da tabela models_signatarios
//                break;

            default:
                throw new \Exception('Signatário inválido');
        }

        $this->signatario = $model;
    }
}
