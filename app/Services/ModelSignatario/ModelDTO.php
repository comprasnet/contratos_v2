<?php

namespace App\Services\ModelSignatario;

use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoExecucaoEntregaTRD;
use App\Models\AutorizacaoExecucaoEntregaTRP;
use App\Models\AutorizacaoexecucaoHistorico;
use App\Models\TermoRecebimentoDefinitivo;
use App\Models\TermoRecebimentoProvisorio;
use Illuminate\Database\Eloquent\Model;

class ModelDTO
{
    public $model;
    public $fkColumn;

    public function __construct(Model $model)
    {
        switch (get_class($model)) {
            case AutorizacaoExecucao::class:
                $this->fkColumn = 'model_autorizacaoexecucao_id';
                break;
            case AutorizacaoexecucaoHistorico::class:
                $this->fkColumn = 'model_autorizacaoexecucao_historico_id';
                break;
            case AutorizacaoExecucaoEntregaTRP::class:
                $this->fkColumn = 'model_autorizacaoexecucao_entrega_trp_id';
                break;
            case AutorizacaoExecucaoEntregaTRD::class:
                $this->fkColumn = 'model_autorizacaoexecucao_entrega_trd_id';
                break;
            case TermoRecebimentoProvisorio::class:
                $this->fkColumn = 'model_termo_recebimento_provisorio_id';
                break;
            case TermoRecebimentoDefinitivo::class:
                $this->fkColumn = 'model_termo_recebimento_definitivo_id';
                break;

//          Adicione aqui os outros modelos seguindo o mesmo padrao
//            case SuaModel::class:
//                $this->fkColumn = 'model_sua_model_id'; // fk da tabela models_signatarios
//                break;

            default:
                throw new \Exception('Model inválida');
        }

        $this->model = $model;
    }
}
