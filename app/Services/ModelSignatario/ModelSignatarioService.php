<?php

namespace App\Services\ModelSignatario;

use App\Models\CodigoItem;
use App\Models\ModelSignatario;
use Carbon\Carbon;

class ModelSignatarioService
{
    protected $modelDTO;

    public function __construct(ModelDTO $modelDTO)
    {
        $this->modelDTO = $modelDTO;
    }

    public function addSignatario(SignatarioDTO $signatarioDTO, array $arquivoData)
    {
        $statusAguardandoAssinatura = CodigoItem::where('descres', 'status_assinatura')
            ->where('descricao', 'Aguardando')
            ->first();

        ModelSignatario::create([
            $this->modelDTO->fkColumn => $this->modelDTO->model->id,
            $signatarioDTO->fkColumn => $signatarioDTO->signatario->id,
            'status_assinatura_id' => $statusAguardandoAssinatura->id,
            'arquivo_generico_id' => $arquivoData['arquivo_generico_id'],
            'posicao_x_assinatura' => $arquivoData['posicao_x_assinatura'],
            'posicao_y_assinatura' => $arquivoData['posicao_y_assinatura'],
            'pagina_assinatura' => $arquivoData['pagina_assinatura'],
        ]);
    }

    public function assinar(SignatarioDTO $signatarioDTO)
    {
        $statusAssinado = CodigoItem::where('descres', 'status_assinatura')
            ->where('descricao', 'Assinado')
            ->first();

        ModelSignatario::where($this->modelDTO->fkColumn, $this->modelDTO->model->id)
            ->where($signatarioDTO->fkColumn, $signatarioDTO->signatario->id)
            ->update([
                'status_assinatura_id' => $statusAssinado->id,
                'data_operacao' => Carbon::now(),
            ]);
    }

    public function recusar(SignatarioDTO $signatarioDTO)
    {
        $statusAssinado = CodigoItem::where('descres', 'status_assinatura')
            ->where('descricao', 'Recusado')
            ->first();

        ModelSignatario::where($this->modelDTO->fkColumn, $this->modelDTO->model->id)
            ->where($signatarioDTO->fkColumn, $signatarioDTO->signatario->id)
            ->update([
                'status_assinatura_id' => $statusAssinado->id,
                'data_operacao' => Carbon::now(),
            ]);
    }

    public function removerTodosSignatarios()
    {
        ModelSignatario::where($this->modelDTO->fkColumn, $this->modelDTO->model->id)->delete();
    }

    public function removerSignatario(SignatarioDTO $signatarioDTO)
    {
        ModelSignatario::where($this->modelDTO->fkColumn, $this->modelDTO->model->id)
            ->where($signatarioDTO->fkColumn, $signatarioDTO->signatario->id)
            ->delete();
    }

    public function verifyUsuarioCanAssinar(): bool
    {
        $result = $this->modelDTO->model->signatariosResponsaveis()->where('user_id', backpack_user()->id)
            ->whereNull('models_signatarios.data_operacao')
            ->exists();

        if (!$result) {
            $result = $this->modelDTO->model->signatariosPrepostos()
                ->whereNull('models_signatarios.data_operacao')
                ->where(function ($query) {
                    $query->where('user_id', backpack_user()->id)
                        ->orWhere('cpf', backpack_user()->cpf);
                })
                ->exists();

            if (session()->get('tipo_acesso') == 'Administrador') {
                $result = $this->verificaSeNaoExisteAssinaturaPreposto();
            }
        }
        return $result;
    }

    public function verificaSeNaoExisteAssinaturaPreposto(): bool
    {
        return  !$this->modelDTO->model->signatariosPrepostos()
            ->whereNotNull('models_signatarios.data_operacao')
            ->exists();
    }
}
