<?php

namespace App\Services\EntregaTrpTrd;

use App\Http\Traits\Formatador;
use App\Http\Traits\NumeroTrait;
use App\Http\Traits\SignatarioTrait;
use App\Mail\NotificarAssinaturaTRDMail;
use App\Models\AutorizacaoexecucaoItens;
use App\Models\CodigoItem;
use App\Models\TermoRecebimentoDefinitivo;
use App\Models\TermoRecebimentoDefinitivoItem;
use App\Models\TermoRecebimentoProvisorio;
use App\Services\ModelSignatario\ModelDTO;
use App\Services\ModelSignatario\SignatarioDTO;
use App\Services\ModelSignatario\TermoRecebimentoDefinitivoAssinador;
use App\Services\RelatorioPdfService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class TermoRecebimentoDefinitivoService
{
    use SignatarioTrait;
    use Formatador;
    use NumeroTrait;

    private $trd;

    public function create(array $data, int $contratoId)
    {
        $this->trd = TermoRecebimentoDefinitivo::create([
            'contrato_id' => $contratoId,
            'situacao_id' => $this->getSituacaoId($data['rascunho']),
            'numero' => $this->getProximoNumero(TermoRecebimentoDefinitivo::class, $contratoId),
            'rascunho' => $data['rascunho'],
            'introducao' => $data['introducao'],
            'valor_total' => $data['valor_total'] ?? null,
            'informacoes_complementares' => $data['informacoes_complementares'] ?? null,
            'incluir_instrumento_cobranca' => $data['incluir_instrumento_cobranca'],
        ]);

        $vinculosIds = empty($data['vinculos']) ? [] : array_column($data['vinculos'], 'id');
        $this->trd->termosRecebimentoProvisorio()->attach($vinculosIds);
        foreach ($data['vinculos'] as $vinculo) {
            if (isset($vinculo['itens'])) {
                $this->createItens($vinculo['itens']);
            }
        }

        $this->gerarDocumentoAssinatura($data);
    }

    public function update(array $data, int $id)
    {
        $this->trd = TermoRecebimentoDefinitivo::findOrFail($id);
        $this->trd->update([
            'situacao_id' => $this->getSituacaoId($data['rascunho']),
            'rascunho' => $data['rascunho'],
            'introducao' => $data['introducao'],
            'valor_total' => $data['valor_total'] ?? null,
            'informacoes_complementares' => $data['informacoes_complementares'] ?? null,
            'incluir_instrumento_cobranca' => $data['incluir_instrumento_cobranca'],
        ]);

        $vinculosIds = empty($data['vinculos']) ? [] : array_column($data['vinculos'], 'id');
        $this->trd->termosRecebimentoProvisorio()->sync($vinculosIds);
        $this->trd->itens()->delete();
        foreach ($data['vinculos'] as $vinculo) {
            if (isset($vinculo['itens'])) {
                $this->createItens($vinculo['itens']);
            }
        }

        $this->gerarDocumentoAssinatura($data);
    }

    public function ajustar(int $id)
    {
        $trd = TermoRecebimentoDefinitivo::findOrFail($id);

        $trd->update(['situacao_id' => $this->getSituacaoId(true)]);

        Storage::disk('public')->delete($trd->arquivo->url);
        $trd->arquivo()->delete();

        $trdAssinador = new TermoRecebimentoDefinitivoAssinador(new ModelDTO($trd));
        $trdAssinador->removerTodosSignatarios();
    }

    private function getSituacaoId(bool $rascunho): int
    {
        $descres = 'trd_status_2';
        if ($rascunho) {
            $descres = 'trd_status_1';
        }

        return CodigoItem::where('descres', $descres)->first()->id;
    }

    private function createItens(array $itens): void
    {
        if (!empty($itens)) {
            foreach ($itens as $item) {
                TermoRecebimentoDefinitivoItem::create([
                    'termo_recebimento_definitivo_id' => $this->trd->id,
                    'termo_recebimento_provisorio_item_id' => $item['termo_recebimento_provisorio_item_id'],
                    'itemable_id' => $item['id'],
                    'itemable_type' => AutorizacaoexecucaoItens::class,
                    'quantidade_informada' => $item['quantidade_informada'] ?? 0,
                    'valor_glosa' => $item['valor_glosa'] ?? 0,
                    'mes_ano_competencia' => $item['mes_ano_competencia'] ?? null,
                    'processo_sei' => $item['processo_sei'] ?? null,
                    'data_inicio' => $item['data_inicio'] ?? null,
                    'data_fim' => $item['data_fim'] ?? null,
                    'horario' => $item['horario'] ?? null,
                ]);
            }
        }
    }

    private function gerarDocumentoAssinatura(array $data)
    {
        if (!$data['rascunho']) {
            $trdAssinador = new TermoRecebimentoDefinitivoAssinador(new ModelDTO($this->trd));

            $signatarios = $this->converteModalSelectPrepostosResponsaveisParaSignatarios(
                $data['modal_select_prepostos_responsaveis']
            );

            $this->notificarAssinatura(
                $signatarios,
                new NotificarAssinaturaTRDMail($this->trd)
            );

            $pathPDFassinatura = $this->gerarPDF($signatarios);
            $assinaturas = $this->getPosicoesAssinaturas($pathPDFassinatura, $signatarios);
            $nomeArquivo = last(explode('/', $pathPDFassinatura));

            $arquivoGenerico = $this->trd
                ->arquivo()
                ->create([
                    'tipo_id' => CodigoItem::where('descres', 'ae_arquivo_assinado')->first()->id,
                    'url' => $pathPDFassinatura,
                    'nome' => $nomeArquivo,
                    'descricao' => '',
                    'restrito' => false,
                    'user_id' => backpack_auth()->user()->id,
                ]);

            $trdAssinador->removerTodosSignatarios();
            $signatarios->each(function (
                $signatario,
                $key
            ) use (
                $trdAssinador,
                $assinaturas,
                $arquivoGenerico
            ) {
                $assinaturas[$key]['arquivo_generico_id'] = $arquivoGenerico->id;

                $signatarioDTO = new SignatarioDTO($signatario);
                $trdAssinador->addSignatario($signatarioDTO, $assinaturas[$key]);
            });
        }
    }

    private function gerarPDF($signatarios)
    {
        $contrato = $this->trd->contrato;
        $prepostos = [];
        foreach ($contrato->prepostos as $preposto) {
            if ($preposto->situacao) {
                $prepostos[] = $preposto->nome . ' - ' . $preposto->email;
            }
        }
        $prepostos = implode('<br> ', $prepostos);

        $gestores = [];
        foreach ($contrato->responsaveis as $responsavel) {
            if (in_array($responsavel->funcao->descricao, ['Gestor Substituto', 'Gestor'])) {
                $gestores[] = $responsavel->user->name .
                    ' - ' . $responsavel->funcao->descricao .
                    ' (' . $responsavel->portaria . ')';
            }
        }
        $gestores = implode('<br> ', $gestores);

        $html = view(
            "relatorio.termo-recebimento-definitivo-pdf",
            [
                'titulo' => 'Termo de Recebimento Provisório nº ' . $this->trd->numero .
                    ' Contrato nº '. $contrato->numero,
                'unidadeGerenciadora' => 'Unidade Gestora Atual ' .
                    $contrato->unidade->codigosiasg . ' - ' .
                    $contrato->unidade->nome,
                'contrato' => $contrato,
                'contratante' => $this->formataCnpj($contrato->unidade->cnpj) . ' - ' .
                    $contrato->unidade->nome,
                'prepostos' => $prepostos,
                'gestores' => $gestores,
                'termoRecebimento' => $this->trd,
                'trps' => $this->trd->termosRecebimentoProvisorio,
                'itens' => $this->trd->itens()->with('trpItem')->get(),
                'anexos' => $this->trd->getAnexos(),
                'valorTotal' => $this->trd->valor_total,
                'valorPorExtenso' => $this->valorPorExtenso($this->trd->valor_total),
                'assinantes' => $signatarios
            ]
        )->render();


        $dompdf = RelatorioPdfService::create();
        $dompdf->loadHtml($html);
        $dompdf->render();
        $font = $dompdf->getFontMetrics()->getFont(
            "helvetica",
            "normal"
        );
        $canvas = $dompdf->getCanvas();

        $canvas->page_text(
            35,
            820,
            "Contrato nº {$contrato->numero} - " .
            "{$contrato->unidade->codigosiasg}",
            $font,
            10,
        );

        $canvas->page_text(
            565,
            820,
            "{PAGE_NUM}/{PAGE_COUNT}",
            $font,
            10,
        );

        $numeroExploded = explode('/', $this->trd->numero);
        $sequencial = $numeroExploded[0];
        $ano = $numeroExploded[1];
        $filename = 'termo-recebimento-definitivo-' . $sequencial . '-' . $ano;

        $path = 'autorizacaoexecucao/entrega/' .
            Carbon::now()->format('Y') . '/' .
            Carbon::now()->format('m') . '/' .
            $filename . '.pdf';

        Storage::disk('public')->put($path, $dompdf->output());

        return $path;
    }
}
