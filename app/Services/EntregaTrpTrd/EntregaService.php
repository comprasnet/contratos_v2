<?php

namespace App\Services\EntregaTrpTrd;

use App\Http\Traits\NumeroTrait;
use App\Models\AutorizacaoexecucaoItens;
use App\Models\CodigoItem;
use App\Models\Entrega;
use App\Models\EntregaItem;
use App\Services\AnexosService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;

class EntregaService
{
    use NumeroTrait;

    private $entrega;

    public function create(array $data, int $contratoId)
    {
        $this->entrega = Entrega::create([
            'contrato_id' => $contratoId,
            'situacao_id' => $this->getSituacaoId($data['rascunho']),
            'numero' => $this->getProximoNumero(Entrega::class, $contratoId),
            'rascunho' => $data['rascunho'],
            'valor_total' => $data['valor_total'] ?? null,
            'mes_ano_referencia' => $data['mes_ano_referencia'] ?? null,
            'data_inicio' => $data['data_inicio'] ?? null,
            'data_fim' => $data['data_fim'] ?? null,
            'data_entrega' => $data['data_entrega'] ?? null,
            'data_prevista_trp' => $data['data_prevista_trp'] ?? null,
            'data_prevista_trd' => $data['data_prevista_trd'] ?? null,
            'informacoes_complementares' => $data['informacoes_complementares'] ?? null,
            'originado_sistema_externo' => $data['originado_sistema_externo'] ?? false
        ]);

        $vinculosIds = empty($data['vinculos']) ? [] : array_column($data['vinculos'], 'id');
        $this->entrega->autorizacoes()->attach($vinculosIds);
        foreach ($data['vinculos'] as $vinculo) {
            if (isset($vinculo['itens'])) {
                $this->createItens($vinculo['itens']);
            }
        }

        (new AnexosService(
            $data['anexos'] ?? [],
            $this->entrega,
            'autorizacaoexecucao/entrega/' . Carbon::now()->format('Y') . '/' . Carbon::now()->format('m'),
            CodigoItem::where('descres', 'ae_entrega_arquivo')->first()
        ))->manipulaAnexos();

        $this->entrega->locaisExecucao()->attach($data['locais_execucao']);
    }

    public function update(array $data, int $id)
    {
        $this->entrega = Entrega::findOrFail($id);

        $this->entrega->update([
            'situacao_id' => $this->getSituacaoId($data['rascunho']),
            'rascunho' => $data['rascunho'],
            'valor_total' => $data['valor_total'] ?? null,
            'mes_ano_referencia' => $data['mes_ano_referencia'] ?? null,
            'data_inicio' => $data['data_inicio'] ?? null,
            'data_fim' => $data['data_fim'] ?? null,
            'data_entrega' => $data['data_entrega'] ?? null,
            'data_prevista_trp' => $data['data_prevista_trp'] ?? null,
            'data_prevista_trd' => $data['data_prevista_trd'] ?? null,
            'informacoes_complementares' => $data['informacoes_complementares'] ?? null,
            'originado_sistema_externo' => $data['originado_sistema_externo'] ?? false
        ]);

        $vinculosIds = empty($data['vinculos']) ? [] : array_column($data['vinculos'], 'id');
        $this->entrega->autorizacoes()->sync($vinculosIds);
        $this->entrega->itens()->delete();
        foreach ($data['vinculos'] as $vinculo) {
            if (isset($vinculo['itens'])) {
                $this->createItens($vinculo['itens']);
            }
        }

        (new AnexosService(
            $data['anexos'] ?? [],
            $this->entrega,
            'autorizacaoexecucao/entrega/' . Carbon::now()->format('Y') . '/' . Carbon::now()->format('m'),
            CodigoItem::where('descres', 'ae_entrega_arquivo')->first()
        ))->manipulaAnexos();

        $this->entrega->locaisExecucao()->sync($data['locais_execucao']);
    }

    public function duplicar(int $entrega_id)
    {
        $entrega = Entrega::findOrFail($entrega_id);

        $entregaDuplicada = Entrega::create([
            'contrato_id' => $entrega->contrato_id,
            'situacao_id' => $this->getSituacaoId(true),
            'numero' => $this->getProximoNumero(Entrega::class, $entrega->contrato_id),
            'rascunho' => true,
            'valor_total' => $entrega->valor_total,
            'mes_ano_referencia' => $entrega->mes_ano_referencia,
            'data_inicio' => $entrega->data_inicio,
            'data_fim' => $entrega->data_fim,
            'data_entrega' => $entrega->data_entrega,
            'data_prevista_trp' => $entrega->data_prevista_trp,
            'data_prevista_trd' => $entrega->data_prevista_trd,
            'informacoes_complementares' => $entrega->informacoes_complementares,
            'originado_sistema_externo' => $entrega->originado_sistema_externo
        ]);

        $entregaDuplicada->autorizacoes()->attach($entrega->autorizacoes->pluck('id')->toArray());
        $entregaDuplicada->itens()->createMany($entrega->itens->toArray());

        $this->duplicarAnexos($entrega->anexos, $entregaDuplicada);
    }

    private function getSituacaoId(bool $rascunho): int
    {
        $descres = session('fornecedor_id') ? 'entrega_status_2' : 'entrega_status_3';
        if ($rascunho) {
            $descres = 'entrega_status_1';
        }

        return CodigoItem::where('descres', $descres)->first()->id;
    }

    private function createItens(array $itens): void
    {
        if (!empty($itens)) {
            foreach ($itens as $item) {
                EntregaItem::create([
                    'entrega_id' => $this->entrega->id,
                    'itemable_id' => $item['id'],
                    'itemable_type' => AutorizacaoexecucaoItens::class,
                    'quantidade_informada' => $item['quantidade_informada'] ?? 0,
                    'valor_glosa' => $item['valor_glosa'] ?? 0,
                ]);
            }
        }
    }

    private function duplicarAnexos(Collection $anexos, Entrega $entregaDuplicada)
    {
        $anexos->each(function ($anexo) use ($entregaDuplicada) {
            $filename = last(explode('/', $anexo->url));

            $date = Carbon::now();
            $path = 'autorizacaoexecucao/entrega/' .
                $date->format('Y') . '/' .
                $date->format('m') . '/' .
                $entregaDuplicada->id . '-' . $filename;

            Storage::disk('public')->copy($anexo->url, $path);

            $entregaDuplicada->anexos()->create([
                'nome' => $anexo->nome,
                'tipo_id' => $anexo->tipo_id,
                'descricao' => $anexo->descricao,
                'url' => $path,
                'restrito' => $anexo->restrito,
            ]);
        });
    }
}
