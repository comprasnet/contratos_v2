<?php

namespace App\Services\EntregaTrpTrd;

use App\Http\Traits\Formatador;
use App\Http\Traits\NumeroTrait;
use App\Http\Traits\SignatarioTrait;
use App\Mail\NotificarAssinaturaTRPMail;
use App\Models\AutorizacaoexecucaoItens;
use App\Models\CodigoItem;
use App\Models\Entrega;
use App\Models\TermoRecebimentoProvisorio;
use App\Models\TermoRecebimentoProvisorioItem;
use App\Services\ModelSignatario\ModelDTO;
use App\Services\ModelSignatario\SignatarioDTO;
use App\Services\ModelSignatario\TermoRecebimentoProvisorioAssinador;
use App\Services\RelatorioPdfService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class TermoRecebimentoProvisorioService
{
    use SignatarioTrait;
    use Formatador;
    use NumeroTrait;

    private $trp;

    public function create(array $data, int $contratoId)
    {
        $this->trp = TermoRecebimentoProvisorio::create([
            'contrato_id' => $contratoId,
            'situacao_id' => $this->getSituacaoId($data['rascunho']),
            'numero' => $this->getProximoNumero(TermoRecebimentoProvisorio::class, $contratoId),
            'rascunho' => $data['rascunho'],
            'introducao' => $data['introducao'],
            'valor_total' => $data['valor_total'] ?? null,
            'informacoes_complementares' => $data['informacoes_complementares'] ?? null,
        ]);

        $vinculosIds = empty($data['vinculos']) ? [] : array_column($data['vinculos'], 'id');
        $this->trp->entregas()->attach($vinculosIds);
        foreach ($data['vinculos'] as $vinculo) {
            $entrega = Entrega::findOrFail($vinculo['id']);
            $entrega->update([
                'mes_ano_referencia' => $vinculo['mes_ano_referencia'] ?? null
            ]);
            $entrega->locaisExecucao()->sync($vinculo['locais_execucao'] ?? []);
            if (isset($vinculo['itens'])) {
                $this->createItens($vinculo['itens']);
            }
        }

        $this->gerarDocumentoAssinatura($data);
    }

    public function update(array $data, int $id)
    {
        $this->trp = TermoRecebimentoProvisorio::findOrFail($id);
        $this->trp->update([
            'situacao_id' => $this->getSituacaoId($data['rascunho']),
            'rascunho' => $data['rascunho'],
            'introducao' => $data['introducao'],
            'valor_total' => $data['valor_total'] ?? null,
            'informacoes_complementares' => $data['informacoes_complementares'] ?? null,
        ]);

        $vinculosIds = empty($data['vinculos']) ? [] : array_column($data['vinculos'], 'id');
        $this->trp->entregas()->sync($vinculosIds);
        $this->trp->itens()->delete();
        foreach ($data['vinculos'] as $vinculo) {
            $entrega = Entrega::findOrFail($vinculo['id']);
            $entrega->update([
                'mes_ano_referencia' => $vinculo['mes_ano_referencia']
            ]);
            $entrega->locaisExecucao()->sync($vinculo['locais_execucao'] ?? []);
            if (isset($vinculo['itens'])) {
                $this->createItens($vinculo['itens']);
            }
        }

        $this->gerarDocumentoAssinatura($data);
    }

    public function ajustar(int $id)
    {
        $trp = TermoRecebimentoProvisorio::findOrFail($id);

        $trp->update(['situacao_id' => $this->getSituacaoId(true)]);

        Storage::disk('public')->delete($trp->arquivo->url);
        $trp->arquivo()->delete();

        $trpAssinador = new TermoRecebimentoProvisorioAssinador(new ModelDTO($trp));
        $trpAssinador->removerTodosSignatarios();
    }

    private function getSituacaoId(bool $rascunho): int
    {
        $descres = 'trp_status_2';
        if ($rascunho) {
            $descres = 'trp_status_1';
        }

        return CodigoItem::where('descres', $descres)->first()->id;
    }

    private function createItens(array $itens): void
    {
        if (!empty($itens)) {
            foreach ($itens as $item) {
                TermoRecebimentoProvisorioItem::create([
                    'termo_recebimento_provisorio_id' => $this->trp->id,
                    'entrega_item_id' => $item['entrega_item_id'],
                    'itemable_id' => $item['id'],
                    'itemable_type' => AutorizacaoexecucaoItens::class,
                    'quantidade_informada' => $item['quantidade_informada'] ?? 0,
                    'valor_glosa' => $item['valor_glosa'] ?? 0,
                    'mes_ano_competencia' => $item['mes_ano_competencia'] ?? null,
                    'processo_sei' => $item['processo_sei'] ?? null,
                    'data_inicio' => $item['data_inicio'] ?? null,
                    'data_fim' => $item['data_fim'] ?? null,
                    'horario' => $item['horario'] ?? null,
                ]);
            }
        }
    }

    private function gerarDocumentoAssinatura(array $data)
    {
        if (!$data['rascunho']) {
            $trpAssinador = new TermoRecebimentoProvisorioAssinador(new ModelDTO($this->trp));

            $signatarios = $this->converteModalSelectPrepostosResponsaveisParaSignatarios(
                $data['modal_select_prepostos_responsaveis']
            );

            $this->notificarAssinatura(
                $signatarios,
                new NotificarAssinaturaTRPMail($this->trp)
            );

            $pathPDFassinatura = $this->gerarPDF($signatarios);
            $assinaturas = $this->getPosicoesAssinaturas($pathPDFassinatura, $signatarios);
            $nomeArquivo = last(explode('/', $pathPDFassinatura));

            $arquivoGenerico = $this->trp
                ->arquivo()
                ->create([
                    'tipo_id' => CodigoItem::where('descres', 'ae_arquivo_assinado')->first()->id,
                    'url' => $pathPDFassinatura,
                    'nome' => $nomeArquivo,
                    'descricao' => '',
                    'restrito' => false,
                    'user_id' => backpack_auth()->user()->id,
                ]);

            $trpAssinador->removerTodosSignatarios();
            $signatarios->each(function (
                $signatario,
                $key
            ) use (
                $trpAssinador,
                $assinaturas,
                $arquivoGenerico
            ) {
                $assinaturas[$key]['arquivo_generico_id'] = $arquivoGenerico->id;

                $signatarioDTO = new SignatarioDTO($signatario);
                $trpAssinador->addSignatario($signatarioDTO, $assinaturas[$key]);
            });
        }
    }

    private function gerarPDF($signatarios)
    {
        $contrato = $this->trp->contrato;
        $prepostos = [];
        foreach ($contrato->prepostos as $preposto) {
            if ($preposto->situacao) {
                $prepostos[] = $preposto->nome . ' - ' . $preposto->email;
            }
        }
        $prepostos = implode('<br> ', $prepostos);

        $gestores = [];
        foreach ($contrato->responsaveis as $responsavel) {
            if (in_array($responsavel->funcao->descricao, ['Gestor Substituto', 'Gestor'])) {
                $gestores[] = $responsavel->user->name .
                    ' - ' . $responsavel->funcao->descricao .
                    ' (' . $responsavel->portaria . ')';
            }
        }
        $gestores = implode('<br> ', $gestores);

        $html = view(
            "relatorio.termo-recebimento-provisorio-pdf",
            [
                'titulo' => 'Termo de Recebimento Provisório nº ' . $this->trp->numero .
                    ' Contrato nº '. $contrato->numero,
                'unidadeGerenciadora' => 'Unidade Gestora Atual ' .
                    $contrato->unidade->codigosiasg . ' - ' .
                    $contrato->unidade->nome,
                'contrato' => $contrato,
                'contratante' => $this->formataCnpj($contrato->unidade->cnpj) . ' - ' .
                    $contrato->unidade->nome,
                'prepostos' => $prepostos,
                'gestores' => $gestores,
                'termoRecebimento' => $this->trp,
                'entregas' => $this->trp->entregas,
                'itens' => $this->trp->itens()->with('entregaItem')->get(),
                'anexos' => $this->trp->getAnexos(),
                'valorTotal' => $this->trp->valor_total,
                'valorPorExtenso' => $this->valorPorExtenso($this->trp->valor_total),
                'assinantes' => $signatarios
            ]
        )->render();


        $dompdf = RelatorioPdfService::create();
        $dompdf->loadHtml($html);
        $dompdf->render();
        $font = $dompdf->getFontMetrics()->getFont(
            "helvetica",
            "normal"
        );
        $canvas = $dompdf->getCanvas();

        $canvas->page_text(
            35,
            820,
            "Contrato nº {$contrato->numero} - " .
            "{$contrato->unidade->codigosiasg}",
            $font,
            10,
        );

        $canvas->page_text(
            565,
            820,
            "{PAGE_NUM}/{PAGE_COUNT}",
            $font,
            10,
        );

        $numeroExploded = explode('/', $this->trp->numero);
        $sequencial = $numeroExploded[0];
        $ano = $numeroExploded[1];
        $filename = 'termo-recebimento-provisorio-' . $sequencial . '-' . $ano;

        $path = 'autorizacaoexecucao/entrega/' .
            Carbon::now()->format('Y') . '/' .
            Carbon::now()->format('m') . '/' .
            $filename . '.pdf';

        Storage::disk('public')->put($path, $dompdf->output());

        return $path;
    }
}
