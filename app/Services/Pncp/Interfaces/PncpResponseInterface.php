<?php

namespace App\Services\Pncp\Interfaces;

use App\Models\EnviaDadosPncpHistorico;
use GuzzleHttp\Psr7\Response;
use Illuminate\Database\Eloquent\Model;
use App\Models\EnviaDadosPncp;

interface PncpResponseInterface
{
    public function saveResponseFromPncp(
        Response $response,
        Model $model,
        EnviaDadosPncp $enviaDadosPncp,
        PncpServiceInterface $pncpService
    ) : EnviaDadosPncpHistorico;
}
