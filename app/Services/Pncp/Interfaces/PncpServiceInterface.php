<?php

namespace App\Services\Pncp\Interfaces;

use GuzzleHttp\Psr7\Response;

interface PncpServiceInterface
{
    public function sendToPncp($resourceModel) : Response;

    public function methodToSend() : string;

    public function logName() : string;
}