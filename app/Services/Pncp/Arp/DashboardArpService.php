<?php

namespace App\Services\Pncp\Arp;

use App\Repositories\ArpsDashboardRepository;

class DashboardArpService
{
    /**
     * @var ArpsDashboardRepository
     */
    public static $repository;

    public static function getRepositoryByRequest(){
        if(!isset(self::$repository)){
            $arpsDashboard = new ArpsDashboardRepository();
            $request = request();

            $unidades = $request->input('uasg');
            $hasFilter = (bool)(int)$request->input('has_filter');

            if(is_null($unidades) and !$hasFilter){
                $unidade = ArpsDashboardRepository::unidadeSession();
                if(!is_null($unidade)) {
                    $unidades[] = $unidade->id;
                }
            }

            $arpsDashboard->setVigenciaInicial($request->input('vigencia_inicial'));
            $arpsDashboard->setVigenciaFinal($request->input('vigencia_final'));

            if(!empty($unidades)) {
                $arpsDashboard->setUasgs($unidades);
            }

            $arpsDashboard->setNumeroAno($request->input('numero_ano'));
            $arpsDashboard->setFilterAta($request->input('filter-ata'));

            self::$repository = $arpsDashboard;
        }
        return self::$repository;
    }
}