<?php

namespace App\Services\Pncp\Arp;

use App\Services\Pncp\Interfaces\PncpServiceInterface;
use App\Services\Pncp\PncpService;
use GuzzleHttp\Psr7\Response;
use App\Http\Traits\ExternalServices;

class GetNumSeqArpPncpService extends PncpService implements PncpServiceInterface
{

    use ExternalServices;

    public function sendToPncp($resourceModel): Response
    {
        $link_pncp = parse_url($resourceModel->enviaDadosPncp->link_pncp);
        $base_uri = $link_pncp['scheme'].'://'.$link_pncp['host'];
        $endpoint = $link_pncp['path'];

        return $this->makeRequest(
            'GET',
            $base_uri,
            $endpoint,
            null,
            [],
            '',
            []
        );
    }

    public function methodToSend(): string
    {
        return 'GET';
    }

    public function logName(): string
    {
        return 'consultar_sequencialPNCP';
    }
}
