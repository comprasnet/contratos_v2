<?php

namespace App\Services\Pncp\Arp;

use App\Services\Pncp\Interfaces\PncpServiceInterface;
use App\Services\Pncp\PncpService;
use GuzzleHttp\Psr7\Response;
use App\Http\Traits\ExternalServices;

class ConsultarArquivoArpPncpService extends PncpService implements PncpServiceInterface
{

    use ExternalServices;

    private $endpointName = 'recuperar_arquivo_ata';

    /**
     * envia dados da ata para o pncp
     * salva registro na tabela envia_dados_pncp
     * @param array $params array de dados da ata
     * @return Response
     */
    public function sendToPncp($resourceModel): Response
    {
        $dadosCompra = $resourceModel->arp->compras;
        $cnpj = $dadosCompra->cnpjOrgao;
        $anoCompra = explode('/', $dadosCompra->numero_ano)[1];
        $sequencialCompra = intval($dadosCompra->id_unico);

        $enviaDadosPncpArquivo =  $resourceModel->enviaDadosPncp;

        if (empty($enviaDadosPncpArquivo)) {
            return new Response(404, [], 'Documento não encontrado');
        }

        $sequencialAtaPncp = $resourceModel->arp->enviaDadosPncp->getSequencialSalvoLinkPncp();
        $sequencialArquivoAtaPncp = $resourceModel->enviaDadosPncp->getSequencialSalvoLinkPncp();

        $endpoint = $this->resolveEndpoint(
            $this->endpointName,
            [$cnpj, $anoCompra, $sequencialCompra, $sequencialAtaPncp, $sequencialArquivoAtaPncp]
        );

        $base_uri = config('api.pncp.base_uri');

        $response = $this->makeRequest(
            'GET',
            $base_uri,
            $endpoint,
            null,
            [],
            null,
            []
        );

        # Insere a URL da busca de todas as ata para incluir o ID da ata
        # quando recuperado do serviço
        $response->location = $base_uri . $endpoint;

        return $response;
    }

    public function methodToSend(): string
    {
        return 'GET';
    }

    public function logName(): string
    {
        return 'recuperar_arquivo_ata';
    }
}
