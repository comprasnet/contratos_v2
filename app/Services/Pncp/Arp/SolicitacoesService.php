<?php

namespace App\Services\Pncp\Arp;

use App\Repositories\ArpSolicitacaoRepository;

class SolicitacoesService
{
    public static function getNumberSolicitacao($me = false)
    {
        $arrData = [];
        if ($me) {
            $solicitacao = ArpSolicitacaoRepository::getCounterSolicitacoes();
        } else {
            $solicitacao = ArpSolicitacaoRepository::getCounterAnalises();
        }
        foreach ($solicitacao as $solicitacaoItem) {
            $arrData[] = $solicitacaoItem;
        }
        return empty($arrData)?false:$arrData[0];
    }

    public static function getNumberSolicitacaoAdesaoFornecedor()
    {
        $solicitacao = ArpSolicitacaoRepository::getCounterAnaliseAdesaoFornecedor();

        foreach ($solicitacao as $solicitacaoItem) {
            $arrData[] = $solicitacaoItem;
        }
        //dd(empty($arrData)?false:$arrData[0]);
        return empty($arrData)?false:$arrData[0];
    }
}
