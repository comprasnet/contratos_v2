<?php

namespace App\Services\Pncp\Arp;

use App\Services\Pncp\Interfaces\PncpServiceInterface;
use App\Services\Pncp\PncpService;
use GuzzleHttp\Psr7\Response;

class InsertArpPncpService extends PncpService implements PncpServiceInterface
{

    private $endpointName = 'inserir_ata';

    /**
     * envia dados da ata para o pncp
     * salva registro na tabela envia_dados_pncp
     * @param array $params array de dados da ata
     * @return Response
     */
    public function sendToPncp($resourceModel) : Response
    {
        $params = $resourceModel->toArray();
        $cnpj = $resourceModel->compras->cnpjOrgao;
        $anoCompra =  explode('/', $resourceModel->compras->numero_ano)[1];
        // $params['ano'];
        $sequencialCompra = intval($resourceModel->compras->id_unico);
        $header = ['Content-Type' => 'application/json'];
        $body = json_encode($this->bindAttributesToPncp($params));
        $stack = $this->addPnpcAuthenticationMiddleware();
        $endpoint = $this->resolveEndpoint($this->endpointName, [$cnpj, $anoCompra, $sequencialCompra]);

        $response = $this->genericRequest(
            $resourceModel,
            $this->logName(),
            $this->methodToSend(),
            $endpoint,
            $header,
            $body,
            $stack,
            []
        );

        $response->requestBody = $body;

        return $response;
    }

    public function methodToSend(): string
    {
        return 'POST';
    }

    public function logName(): string
    {
        return $this->endpointName;
    }

    /**
     * Pega o array de dados da Ata e formata para que corresponda aos dados da ata no Pncp
     * @param array $attributes
     * @return array
     */
    private function bindAttributesToPncp(array $attributes): array
    {
        return [
            'numeroAtaRegistroPreco' => $attributes['numero'],
            'anoAta' => $attributes['ano'],
            'dataAssinatura' => $attributes['data_assinatura'],
            'dataVigenciaInicio' => $attributes['vigencia_inicial'],
            'dataVigenciaFim' => $attributes['vigencia_final'],
        ];
    }
}
