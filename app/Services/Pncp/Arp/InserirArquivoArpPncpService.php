<?php

namespace App\Services\Pncp\Arp;

use App\Services\Pncp\Interfaces\PncpServiceInterface;
use App\Services\Pncp\PncpService;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class InserirArquivoArpPncpService extends PncpService implements PncpServiceInterface
{

    private $endpointName = 'inserir_arquivo_ata';

    public function sendToPncp($resourceModel): Response
    {
        $cnpj = $resourceModel->arp->compras->cnpjOrgao;
        $anoCompra = explode('/', $resourceModel->arp->compras->numero_ano)[1];

        $sequencialCompra = intval($resourceModel->arp->compras->id_unico);
        
        # Limitado devido o tamanho de caracteres aceito no PNCP
        $tituloDocumentoLimitado = Str::limit($resourceModel->descricao, 250);

        $tipoDocumento = 11;
        if ($resourceModel->tipo->nome === 'Outros') {
            $tipoDocumento = 16;
        }

        $header = [
            "Titulo-Documento"  => utf8_decode($tituloDocumentoLimitado),
            "Tipo-Documento" => $tipoDocumento,
            "Content-Type"      => 'multipart/form-data'
        ];

        $stack = $this->addPnpcAuthenticationMiddleware();
        $senquencialAta = $this->recuperarSequencialModel($resourceModel->arp, 'inserir_ata');

        # Se não existir o sequencial devido algum erro que aconteceu no envio da ata
        # para o PNCP, o método abaixo busca todas a ata em específico para atualizar
        # o registro na nossa base
        if (empty($senquencialAta)) {
            $senquencialAta = $this->recuperarSequencialModel($resourceModel->arp, 'consultar_todasAtasPNCP');
        }

        $endpoint = $this->resolveEndpoint(
            $this->endpointName,
            [
                $cnpj,
                $anoCompra,
                $sequencialCompra,
                $senquencialAta
            ]
        );

        Log::debug($endpoint);

        $file = storage_path('app/public/' . $resourceModel->url);

        $requestOptions = [
            'multipart' => [
                [
                    'name' => 'arquivo',
                    'contents' => fopen($file, 'r'),
                ]
            ]
        ];

        $response = $this->genericRequest(
            $resourceModel,
            $this->logName(),
            $this->methodToSend(),
            $endpoint,
            $header,
            '',
            $stack,
            $requestOptions
        );
        $response->requestBody = '';

        return $response;
    }

    public function methodToSend(): string
    {
        return 'POST';
    }

    /**
     * O logname deve ter o mesmo nome da key da variável endpoints no arquivo de config da arp
     * config -> api -> pncp -> endpoints
     * @return string
     */
    public function logName(): string
    {
        return $this->endpointName;
    }
}
