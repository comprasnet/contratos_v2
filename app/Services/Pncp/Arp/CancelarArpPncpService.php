<?php

namespace App\Services\Pncp\Arp;

use App\Http\Traits\Arp\ArpAlteracaoCancelamentoTrait;
use App\Models\ArpHistorico;
use App\Services\Pncp\Interfaces\PncpServiceInterface;
use App\Services\Pncp\PncpService;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;

class CancelarArpPncpService extends PncpService implements PncpServiceInterface
{
    use ArpAlteracaoCancelamentoTrait;

    private $data_assinatura_alteracao_vigencia;

    public function __construct($data_assinatura_alteracao_vigencia)
    {
        parent::__construct();
        $this->data_assinatura_alteracao_vigencia = $data_assinatura_alteracao_vigencia;
    }


    private $endpointName = 'cancelar_ata';

    public function sendToPncp($resourceModel): Response
    {
        $params = $resourceModel->toArray();
        $cnpj = $resourceModel->compras->cnpjOrgao;

        $anoCompra = explode('/', $resourceModel->compras->numero_ano)[1];
        $sequencialCompra = intval($resourceModel->compras->id_unico);

        $header = ['Content-Type' => 'application/json'];
        $body = json_encode(
            $this->bindAttributesToPncpCancelamento(
                $params,
                $this->data_assinatura_alteracao_vigencia
            ),
            JSON_UNESCAPED_UNICODE
        );

        $stack = $this->addPnpcAuthenticationMiddleware();
        $senquencialAta = $this->recuperarSequencialModel($resourceModel, 'inserir_ata');

        # Se não existir o sequencial devido algum erro que aconteceu no envio da ata
        # para o PNCP, o método abaixo busca todas a ata em específico para atualizar
        # o registro na nossa base
        if (empty($senquencialAta)) {
            $senquencialAta = $this->recuperarSequencialModel($resourceModel, 'consultar_todasAtasPNCP');
        }

        $endpoint = $this->resolveEndpoint(
            $this->endpointName,
            [
                $cnpj,
                $anoCompra,
                $sequencialCompra,
                $senquencialAta
            ]
        );

        $response = $this->genericRequest(
            $resourceModel,
            $this->logName(),
            $this->methodToSend(),
            $endpoint,
            $header,
            $body,
            $stack,
            []
        );

        $response->requestBody = $body;

        return $response;
    }

    public function methodToSend(): string
    {
        return 'PUT';
    }

    public function logName(): string
    {
        return $this->endpointName;
    }

    /**
     * Pega o array de dados da Ata e formata para que corresponda aos dados da ata no Pncp
     * @param array $attributes
     * @return array
     */
    private function bindAttributesToPncpCancelamento(array $attributes, $dataCancelamento): array
    {
        $arpHistorico = $this->recuperarUltimoCancelamentoItens($attributes['id']);

        $dataCancelamento = is_string($dataCancelamento) ? Carbon::parse($dataCancelamento) : $dataCancelamento;

        $dataCancelamento->setTime(0, 0, 0);

        return [
            'numeroAtaRegistroPreco' => $attributes['numero'],
            'anoAta' => $attributes['ano'],
            'dataAssinatura' => $attributes['data_assinatura'],
            'dataVigenciaInicio' => $attributes['vigencia_inicial'],
            'dataVigenciaFim' => $attributes['vigencia_final'],
            'dataCancelamento' => $dataCancelamento->format('Y-m-d\TH:i:s'),
            'cancelado' => true,
            'justificativa' => $this->formatarTextoJustificativa($arpHistorico['justificativa_motivo_cancelamento'])
        ];
    }

    private function formatarTextoJustificativa(string $justificativa)
    {
        $originalEncoding = mb_detect_encoding($justificativa, ['UTF-8', 'ISO-8859-1', 'Windows-1252'], true);

        if ($originalEncoding !== 'UTF-8') {
            $justificativa = mb_convert_encoding($justificativa, 'UTF-8', $originalEncoding);
        }

        $justificativa = mb_convert_encoding($justificativa, 'UTF-8', 'UTF-8');

        return mb_substr($justificativa, 0, 250, 'UTF-8');
    }
}
