<?php

namespace App\Services\Pncp\Arp;

use App\Services\Pncp\Interfaces\PncpServiceInterface;
use App\Services\Pncp\PncpService;
use GuzzleHttp\Psr7\Response;

class RemoverArquivoArpPncpService extends PncpService implements PncpServiceInterface
{

    private $endpointName = 'deletar_arquivo_ata';

    public function sendToPncp($resourceModel): Response
    {
        $cnpj = $resourceModel->arp->compras->cnpjOrgao;
        $anoCompra =  explode('/', $resourceModel->arp->compras->numero_ano)[1];
        $sequencialCompra = intval($resourceModel->arp->compras->id_unico);
        $senquencialAta = $this->recuperarSequencialModel($resourceModel->arp, 'inserir_ata');
        $senquencialArquivo = $this->recuperarSequencialModel($resourceModel, 'inserir_arquivo_ata');
        $header = [];
        $body = json_encode(['justificativa' => '']);
        $stack = $this->addPnpcAuthenticationMiddleware();
        $endpoint = $this->resolveEndpoint(
            $this->endpointName,
            [
                $cnpj,
                $anoCompra,
                $sequencialCompra,
                $senquencialAta,
                $senquencialArquivo
            ]
        );

        return $this->genericRequest(
            $resourceModel,
            $this->logName(),
            $this->methodToSend(),
            $endpoint,
            $header,
            $body,
            $stack,
            []
        );
    }

    public function methodToSend(): string
    {
        return 'DELETE';
    }

    public function logName(): string
    {
        return 'deletar_arquivo_ata';
    }
}
