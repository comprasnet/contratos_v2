<?php

namespace App\Services\Pncp;

use App\Models\EnviaDadosPncp;
use App\Models\EnviaDadosPncpHistorico;
use App\Services\Pncp\Arp\GetNumSeqArpPncpService;
use App\Services\Pncp\Interfaces\PncpServiceInterface;
use App\Services\Pncp\Interfaces\PncpResponseInterface;
use App\Services\Pncp\Responses\PncpGetNumSeqResponseService;
use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\BuscaCodigoItens;

class PncpManagerService
{
    use BuscaCodigoItens;

    /**
     * Coloque aqui entidades que necessitam do num seq do pncp
     * @var string[]
     */
    protected static $pncpablesNeedNumSeq = [
        'App\Models\Arp',
    ];

    public function managePncp(
        PncpServiceInterface  $pncpService,
        PncpResponseInterface $pncpResponse,
        Model                 $resourceModel,
        bool $inserirPncp = true
    ) {
        #cria ou retorna dados da entidade que deve ser enviada para o pncp
        $enviaDadosPncp = $this->createOrReturnEnviaDadosPncp($resourceModel);
        #envia entidade para o pncp
        $response = $pncpService->sendToPncp($resourceModel);
        #salva retorno do pncp na tabela envia_dados_pncp_historico
        $pncpHistorico = $pncpResponse->saveResponseFromPncp($response, $resourceModel, $enviaDadosPncp, $pncpService);
        #caso precise busca o numseq da entidade no pncp
        if ($inserirPncp) {
            $this->getNumSeqInPncp($pncpHistorico, $pncpResponse);
        }

        return $pncpHistorico;
    }

    private function createOrReturnEnviaDadosPncp(Model $resourceModel)
    {
        $arrayMatch = [
            'pncpable_type' => $resourceModel->getMorphClass(),
            'pncpable_id' => $resourceModel->id,
        ];

        $arrComplete = [
            'pncpable_type' => $resourceModel->getMorphClass(),
            'pncpable_id' => $resourceModel->id,
            'situacao' => $this->retornaIdCodigoItemPorDescres(
                'INCPEN',
                'Situação Envia Dados PNCP'
            )
        ];

        $enviaDadosPncp = EnviaDadosPncp::firstOrCreate($arrayMatch, $arrComplete);

        $arrMatchEnviaDadosPncpHistorico = array_merge($arrayMatch, ['log_name' => 'criado']);
        $arrCompleteEnviaDadosPncpHistorico = array_merge($arrComplete, ['envia_dados_pncp_id' => $enviaDadosPncp->id]);

        EnviaDadosPncpHistorico::firstOrCreate($arrMatchEnviaDadosPncpHistorico, $arrCompleteEnviaDadosPncpHistorico);

        return $enviaDadosPncp;
    }

    /**
     * Caso entidade já tenha sido enviada pro pncp e não tenha sequencialPNCP, faz a consulta no pncp para buscar.
     * @param EnviaDadosPncp $enviaDadosPncp
     * @param $alreadyExistsInPncp
     * @return void
     */
    public function getNumSeqInPncp(EnviaDadosPncpHistorico $pncpHistorico, $pncpResponse)
    {
        $alreadyExistsInPncp = in_array((int) $pncpHistorico->status_code, $pncpResponse::$successCode);
        #verifica se o registro criado existe no pncp e tem o campo sequencialPNCP nulo caso sim  busca no pncp
        # o sequencialPNCP
        if ($alreadyExistsInPncp && $pncpHistorico->sequencialPNCP === null) {
            #busca no pncp apenas numero sequencial  para as entidades que precisam dele.
            if (in_array($pncpHistorico->pncpable_type, self::$pncpablesNeedNumSeq)) {
                $this->managePncp(
                    new GetNumSeqArpPncpService(),
                    new PncpGetNumSeqResponseService(),
                    $pncpHistorico->enviaDadosPncp->pncpable
                );
            }
        }
    }
}
