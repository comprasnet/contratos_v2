<?php

namespace App\Services\Pncp;

use App\Http\Traits\Pncp\PncpLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PncpApi
{
    use PncpLogin;

    private $header = ['Content-Type' => 'application/json'];


    public function listOrgaos()
    {
        try {
            $id = $this->getPncpUserid();
            $request = $this->makeRequestAuthenticated(Request::METHOD_GET, 'v1/usuarios/' . $id);
            return json_decode($request->getBody())->entesAutorizados;
        } catch (Exception $e) {
            Log::error("Erro ao listar órgão na linha 23: " . $e->getMessage());
        }
    }


    public function addOrgao($cnpj)
    {
        try {
            $id = $this->getPncpUserid();

            $data = new \stdClass();
            $data->entesAutorizados = [$cnpj];
            $request = $this->makeRequestAuthenticated(Request::METHOD_POST, 'v1/usuarios/' . $id . '/orgaos', $data);
        
            return $request;
        } catch (Exception $e) {
            Log::error("Erro ao adicionar órgão na linha 40: " . $e->getMessage());
        }
    }


    public function deleteOrgao($cnpj)
    {
        try {
            $id = $this->getPncpUserid();

            $data = new \stdClass();
            $data->entesAutorizados = [$cnpj];

            $request = $this->makeRequestAuthenticated(Request::METHOD_DELETE, 'v1/usuarios/' . $id . '/orgaos', $data);
            return $request;
        } catch (Exception $e) {
            Log::error("Erro ao deletar órgão na linha 54: " . $e->getMessage());
        }
    }
}
