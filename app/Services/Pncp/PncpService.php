<?php

namespace App\Services\Pncp;

use App\Http\Middleware\PncpAuthenticationHandler;
use App\Http\Traits\ExternalServices;
use App\Models\EnviaDadosPncp;
use App\Models\EnviaDadosPncpHistorico;
use App\Models\PncpUsuario;
use App\Models\Unidade;
use App\Services\Pncp\Responses\PncpSaveExceptionService;
use Carbon\Carbon;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\BuscaCodigoItens;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\ClientException;

use function config;
use function preg_replace_array;

class PncpService
{

    const PNCP_EXCEPTION_CODE = 701;

    use ExternalServices, BuscaCodigoItens;

    /**
     * @var string
     */
    private $baseUri;

    /**
     * @var array
     */
    private $endpoints;

    private static $orgaos = [];

    public function __construct()
    {
        $this->baseUri = config('api.pncp.base_uri');
        $this->endpoints = config('api.pncp.endpoints');
    }


    public static function checkPermissionOrgao($cnpj, $refresh = false)
    {
        $entidades = self::listOrgaos($refresh);
        $founder = array_filter($entidades, function ($item) use ($cnpj) {
            return $item->cnpj == $cnpj;
        });
        return count($founder) > 0;
    }

    public static function listOrgaos($refresh = false)
    {
        if (empty(self::$orgaos) or $refresh) {
            self::$orgaos = (new PncpApi())->listOrgaos();
        }
        return self::$orgaos;
    }

    public static function addOrgao($cnpj)
    {
        (new PncpApi())->addOrgao($cnpj);
    }

    public static function deleteOrgao($cnpj)
    {
        (new PncpApi())->deleteOrgao($cnpj);
    }

    public static function addIfNotExists($cnpj)
    {
        if (!self::checkPermissionOrgao($cnpj)) {
            self::addOrgao($cnpj);
        }
    }

    public function genericRequest(
        Model     $model,
        string    $logname,
        string    $method,
        string    $endpoint,
        array     $header,
        ?string   $body,
        ?callable $stack,
        ?array    $requestOptions
    ) {
        try {
            //dd($method, $endpoint, $stack, $header, $body, $requestOptions);

            return $this->makeRequest(
                $method,
                $this->baseUri,
                $endpoint,
                $stack,
                $header,
                $body,
                $requestOptions
            );
        } catch (ClientException $e) {
            $pncpSaveException = new PncpSaveExceptionService();
            $messageException = $pncpSaveException->formatMessageExceptionFromPncp($e);
            $pncpSaveException->saveExceptionResponse(
                $model,
                $logname,
                $body,
                $method,
                $messageException,
                $e->getCode()
            );

            throw new \Exception($messageException, self::PNCP_EXCEPTION_CODE, $e);
        }
    }

    /**
     * Retorna cnpj para url do recurso do pncp
     *
     * @param string $unidadeId
     * @return mixed
     */
    public function recuperarCnpjOrgao(string $unidadeId)
    {
        return Unidade::where('id', $unidadeId)->first()->orgao->cnpj;
    }


    /**
     * Verifica se o token do PNCP ainda é válido, hj o token expira em 1 hora.
     * @return bool boleando que indica se o token é válido
     */
    public static function tokenIsValid(): bool
    {

        if (is_null(self::getUsuarioPncp())) {
            return false;
        }
        $cleanToken = trim(str_replace('Bearer', '', self::getUsuarioPncp()->token));
        $payloadToken = self::getJWTPayloadToken($cleanToken);
        $expire_in = $payloadToken->exp;

        $nowInSeconds = Carbon::now()->timestamp;

        return $nowInSeconds < $expire_in;
    }

    /**
     * retorna token do usuário pncp
     * @return string
     */
    public static function getToken(): string
    {

        if (is_null(self::getUsuarioPncp())) {
            return '';
        }
        return self::getUsuarioPncp()->token;
    }

    /**
     * seta token do usuário pncp
     * @param $token
     * @return void
     */
    public static function setToken($token): void
    {
        $usuarioPncp = self::getUsuarioPncp();
        $usuarioPncp->token = $token;
        $usuarioPncp->save();
    }


    /**
     * seta token do usuário pncp
     * @param $token
     * @return void
     */
    public static function setTokenAndExpires($token, $secundsExpires): void
    {
        $usuarioPncp = self::getUsuarioPncp();
        $usuarioPncp->token = $token;
        $usuarioPncp->expires = date("Y-m-d H:i:s", strtotime("+" . $secundsExpires . " sec"));
        $usuarioPncp->save();
    }

    /**
     * Retorna json com o payload do token JWT
     * @param string $cleanToken // token sem o Bearer na string
     * @return mixed
     */
    public static function getJWTPayloadToken(string $cleanToken)
    {
        $tokenParts = explode(".", $cleanToken);
        $tokenPayload = base64_decode($tokenParts[1]);

        return json_decode($tokenPayload);
    }

    /**
     * @return mixed
     */
    public static function getUsuarioPncp()
    {
        return PncpUsuario::where('ativo', true)->first();
    }


    /**
     * Middleware que resolve a autenticação no pncp
     * @return HandlerStack
     */
    public function addPnpcAuthenticationMiddleware(): HandlerStack
    {
        $stack = HandlerStack::create(new CurlHandler());
        $stack->push(new PncpAuthenticationHandler(), 'add_auth');

        return $stack;
    }

    public function resolveEndpoint($endpointName, $bindings)
    {
        return preg_replace_array('@{.*?}@', $bindings, $this->endpoints[$endpointName]);
    }

    /**
     * Busca em EnviaDadosPncpHistorico o registro em que foi incluido a ata com sucesso e desmembra o campo link_pncp
     * para pegar o sequencial da model.
     * @param int $idContratacao
     * @return string
     */
    protected function recuperarSequencialModel(Model $resourceModel, string $logname): string
    {
        $link = EnviaDadosPncp::select('link_pncp')
            ->where('pncpable_id', $resourceModel->id)
            ->where('pncpable_type', $resourceModel->getMorphClass())
            ->first();

        # Se não encontrar valor no banco de dados, retorna nulo
        # para tentar pegar o valor da ação de buscar todas as atas
        if (empty($link)) {
            return '';
        }

        $link_array = explode('/', $link->link_pncp);

        return $link_array[count($link_array) - 1];
    }

    protected function recuperarIdTipoArquivo($descricaoCodigo, $descricaoCodigoItem)
    {
        return $this->retornaIdCodigoItem($descricaoCodigo, $descricaoCodigoItem);
    }
}
