<?php

namespace App\Services\Pncp\Responses;

use App\Models\EnviaDadosPncp;
use App\Models\EnviaDadosPncpHistorico;
use App\Services\Pncp\Interfaces\PncpResponseInterface;
use App\Services\Pncp\Interfaces\PncpServiceInterface;
use GuzzleHttp\Psr7\Response;
use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\BuscaCodigoItens;
use Exception;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Se for necessário, criar um serviço para consumo GET genérico
 */
class PncpGetArpUnicaResponseService extends PncpResponseService implements PncpResponseInterface
{
    use BuscaCodigoItens;

    public function saveResponseFromPncp(
        Response $response,
        Model $model,
        EnviaDadosPncp $enviaDadosPncp,
        PncpServiceInterface $pncpService
    ) : EnviaDadosPncpHistorico {
        $idSituacao = $enviaDadosPncp->situacao;
        # Recupera as informações do serviço
        $retornoPncp = json_decode($response->getBody(), true) ?? null;

        # Se não existir valor, retorna o valor nulo
        if (empty($retornoPncp)) {
            return $retornoPncp;
        }

        if (in_array($response->getStatusCode(), parent::$errorCode)) {
            $error = null;
            if ($response->getReasonPhrase() == 'Forbidden') {
                $error = 'Sem conexão com o servidor';
            }

            if ($response->getReasonPhrase() == 'Unprocessable Entity') {
                $error = $retornoPncp;
            }

            if (empty($error)) {
                $error = $response->getReasonPhrase();
            }

            throw new Exception($error);
        }

        if (in_array($response->getStatusCode(), parent::$successCode)) {
            $situacaoDescres = $this->retornaDescresCodigoItemById($enviaDadosPncp->situacao);

            $proximaSituacao = config('api.pncp.fluxo_situacao')[$situacaoDescres];

            $idSituacao = $this->retornaIdCodigoItemPorDescres(
                $proximaSituacao,
                'Situação Envia Dados PNCP'
            );

            # Filtra o array de atas recuperadas pelo serviço e retorna a que
            #  foi enviada pela model
            $ataRecuperada = array_filter($retornoPncp['data'], function ($var) use ($model) {
                return ($var['numeroAtaRegistroPreco'] == $model->numero);
            });

            # Recupera as informações obtidas pela ata enviada no serviço
            $keyArray = array_values($ataRecuperada)[0];

            # Adiciono o sequencial para montar o link do pncp
            $response->location .= "/{$keyArray['sequencialAta']}";

            $retornoPncp = json_encode($keyArray);
        }
      
        $arrayMatch = [
            'pncpable_type' => $model->getMorphClass(),
            'pncpable_id' => $model->id,
        ];
        
        $arrayComplete = [
            'retorno_pncp' => $retornoPncp,
            'situacao' => $idSituacao,
            'json_enviado_inclusao' => $retornoPncp,
            'link_pncp' => $response->location
        ];

        $arrEnviaDadosPncpHistorico = Arr::collapse(
            [
                $arrayMatch,
                $arrayComplete,
                [
                    'log_name' => $pncpService->logName(),
                    'status_code' => $response->getStatusCode(),
                    'envia_dados_pncp_id' => $enviaDadosPncp->id
                ]
            ]
        );

        EnviaDadosPncp::updateOrcreate($arrayMatch, $arrayComplete);

        return EnviaDadosPncpHistorico::create($arrEnviaDadosPncpHistorico);
    }
}
