<?php

namespace App\Services\Pncp\Responses;

use App\Models\EnviaDadosPncp;
use App\Models\EnviaDadosPncpHistorico;
use App\Services\Pncp\Interfaces\PncpResponseInterface;
use App\Services\Pncp\Interfaces\PncpServiceInterface;
use GuzzleHttp\Psr7\Response;
use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\BuscaCodigoItens;
use Exception;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Session\Session;

class PncpPostResponseService extends PncpResponseService implements PncpResponseInterface
{
    use BuscaCodigoItens;

    public function saveResponseFromPncp(
        Response $response,
        Model $model,
        EnviaDadosPncp $enviaDadosPncp,
        PncpServiceInterface $pncpService
    ) : EnviaDadosPncpHistorico {
        $idSituacao = $enviaDadosPncp->situacao;

        $retornoPncp = json_decode($response->getBody()->getContents(), true)['message'] ??
            $response->getHeader('location')[0] ??
            null;

        if (in_array($response->getStatusCode(), parent::$errorCode)) {
            $error = null;
            if ($response->getReasonPhrase() == 'Forbidden') {
                $error = 'Sem conexão com o servidor';
            }

            if ($response->getReasonPhrase() == 'Unprocessable Entity') {
                $error = $retornoPncp;
            }

            if (empty($error)) {
                $error = $response->getReasonPhrase();
            }

            throw new Exception($error);
        }

        if (in_array($response->getStatusCode(), parent::$successCode)) {
            $situacaoDescres = $this->retornaDescresCodigoItemById($enviaDadosPncp->situacao);

            $proximaSituacao = config('api.pncp.fluxo_situacao')[$situacaoDescres];

            $idSituacao = $this->retornaIdCodigoItemPorDescres(
                $proximaSituacao,
                'Situação Envia Dados PNCP'
            );
        }

        $arrayMatch = [
            'pncpable_type' => $model->getMorphClass(),
            'pncpable_id' => $model->id,
        ];

        $arrayComplete = [
            'retorno_pncp' => $retornoPncp,
            'situacao' => $idSituacao,
            'json_enviado_inclusao' => $response->requestBody,
            'link_pncp' => $response->getHeader('location')[0] ?? null
        ];

        $arrEnviaDadosPncpHistorico = Arr::collapse(
            [
                $arrayMatch,
                $arrayComplete,
                [
                    'log_name' => $pncpService->logName(),
                    'status_code' => $response->getStatusCode(),
                    'envia_dados_pncp_id' => $enviaDadosPncp->id
                ]
            ]
        );

        EnviaDadosPncp::updateOrcreate($arrayMatch, $arrayComplete);

        return EnviaDadosPncpHistorico::create($arrEnviaDadosPncpHistorico);
    }
}
