<?php

namespace App\Services\Pncp\Responses;

use App\Http\Traits\BuscaCodigoItens;
use App\Models\EnviaDadosPncp;
use App\Models\EnviaDadosPncpHistorico;
use App\Services\Pncp\Interfaces\PncpResponseInterface;
use App\Services\Pncp\Interfaces\PncpServiceInterface;
use GuzzleHttp\Psr7\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class PncpGetNumSeqResponseService extends PncpResponseService implements PncpResponseInterface
{
    use BuscaCodigoItens;

    public function saveResponseFromPncp(
        Response             $response,
        Model                $model,
        EnviaDadosPncp       $enviaDadosPncp,
        PncpServiceInterface $pncpService
    ) : EnviaDadosPncpHistorico {
        $idSituacao = $enviaDadosPncp->situacao;
        $numeroControlePNCP = null;

        if (in_array($response->getStatusCode(), parent::$successCode)) {
            $situacaoDescres = $this->retornaDescresCodigoItemById($enviaDadosPncp->situacao);

            $proximaSituacao = config('api.pncp.fluxo_situacao')[$situacaoDescres];

            $idSituacao = $this->retornaIdCodigoItemPorDescres(
                $proximaSituacao,
                'Situação Envia Dados PNCP'
            );

            $contents = json_decode($response->getBody()->getContents(), true);
            $numeroControlePNCP = $contents['numeroControlePNCP'];
        }
        $enviaDadosPncp->situacao = $idSituacao;
        $enviaDadosPncp->sequencialPNCP = $numeroControlePNCP;
        $enviaDadosPncp->save();

        $arrEnviaDadosPncp = Arr::except($enviaDadosPncp->toArray(), ['id', 'created_at', 'updated_at']);
        $arrEnviaDadosPncpHistorico = Arr::collapse(
            [
                $arrEnviaDadosPncp,
                [
                    'envia_dados_pncp_id' => $enviaDadosPncp->id,
                    'log_name' => $pncpService->logName(),
                    'status_code' => $response->getStatusCode()
                ]
            ]
        );

        return EnviaDadosPncpHistorico::create($arrEnviaDadosPncpHistorico);
    }
}
