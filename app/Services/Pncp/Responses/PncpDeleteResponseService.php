<?php

namespace App\Services\Pncp\Responses;

use App\Models\EnviaDadosPncp;
use App\Models\EnviaDadosPncpHistorico;
use App\Services\Pncp\Interfaces\PncpResponseInterface;
use App\Services\Pncp\Interfaces\PncpServiceInterface;
use GuzzleHttp\Psr7\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use App\Http\Traits\BuscaCodigoItens;

class PncpDeleteResponseService extends PncpResponseService implements PncpResponseInterface
{

    use BuscaCodigoItens;

    public function saveResponseFromPncp(
        Response $response,
        Model $model,
        EnviaDadosPncp $enviaDadosPncp,
        PncpServiceInterface $pncpService
    ) : EnviaDadosPncpHistorico {
        $idSituacao = $enviaDadosPncp->situacao;

        if (in_array($response->getStatusCode(), parent::$successCode)) {
            $situacaoDescres = $this->retornaDescresCodigoItemById($enviaDadosPncp->situacao);

            $proximaSituacao = config('api.pncp.fluxo_situacao')[$situacaoDescres];

            $idSituacao = $this->retornaIdCodigoItemPorDescres(
                $proximaSituacao,
                'Situação Envia Dados PNCP'
            );
        }

        $arrayMatch = [
            'pncpable_type' => $model->getMorphClass(),
            'pncpable_id' => $model->id,
        ];

        $arrayComplete = [
            'retorno_pncp' => '',
            'situacao' => $idSituacao,
            'json_enviado_inclusao' => '',
            'link_pncp' => ''
        ];

        $arrEnviaDadosPncpHistorico = Arr::collapse(
            [
                $arrayMatch,
                $arrayComplete,
                [
                    'log_name' => $pncpService->logName(),
                    'status_code' => $response->getStatusCode(),
                    'envia_dados_pncp_id' => $enviaDadosPncp->id
                ]
            ]
        );

        EnviaDadosPncp::updateOrcreate($arrayMatch, $arrayComplete);

        #remove na base (softDelete) o arquivo que foi removido do pncp
        $arquivoInPncp = EnviaDadosPncpHistorico::where('pncpable_id', $enviaDadosPncp->pncpable_id)
            ->where('pncpable_type', $enviaDadosPncp->pncpable_type)
            ->where('status_code', 201)
            ->first();

        $arquivoInPncp->delete();

        return EnviaDadosPncpHistorico::create($arrEnviaDadosPncpHistorico);
    }
}
