<?php

namespace App\Services\Pncp\Responses;

use App\Models\EnviaDadosPncpHistorico;
use App\Http\Traits\BuscaCodigoItens;
use Illuminate\Support\Str;

class PncpSaveExceptionService
{
    use BuscaCodigoItens;

    public function saveExceptionResponse($model, $logname, $body, $method, $retornoPncp, $codigoErro)
    {
        $idSituacao = $this->retornaIdCodigoItemPorDescres(
            'ERRO',
            'Situação Envia Dados PNCP'
        );

        EnviaDadosPncpHistorico::create([
            'pncpable_type' => $model->getMorphClass(),
            'pncpable_id' => $model->id,
            'retorno_pncp' => $retornoPncp,
            'situacao' => $idSituacao,
            'json_enviado_inclusao' => $method === 'POST' ? $body : '',
            'json_enviado_alteracao' => $method === 'PUT' ? $body : '',
            'link_pncp' => '',
            'log_name' => $logname,
            'status_code' => $codigoErro,
            'envia_dados_pncp_id' => $model->enviaDadosPncp->id
        ]);
    }

    public function formatMessageExceptionFromPncp($exception)
    {
        $msgErros = '';

        $busca = $exception->getResponse()->getBody()->getContents();
        $search = "message";

        if (str_contains($busca, $search)) {
            $msgErros .= Str::before(
                str_replace('"', '', stristr($busca, "message")),
                ","
            );
            $msgErros = "PNCP:$msgErros";
        }

        if (empty($msgErros)) {
            $msgErros = $busca;
        }

        return str_replace('message:', '', $msgErros);
    }
}
