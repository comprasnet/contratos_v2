<?php

namespace App\Services\Pncp\Responses;

class PncpResponseService
{
    /**
     * Array contendo codigos de retorno sucesso pncp
     * @var string[]
     */
    public static $successCode = [200, 201, 204];

    /**
     * Array contendo codigos de retorno erro pncp
     * @var string[]
     */
    public static $errorCode = [400, 401, 403, 404, 405, 422, 500];

    public function __construct()
    {
        //Injetar repository aqui
    }
}