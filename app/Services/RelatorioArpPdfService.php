<?php

namespace App\Services;

use App\Models\Arp;
use App\Models\ArpPdf;
use App\Repositories\Relatorio\ArpPdfRepository;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Illuminate\Support\Facades\Route;
use App\Jobs\GenerateRelatorioArpPdfJob;
use App\Models\ArpItem;
use App\Services\RelatorioPdfService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\Http\Traits\Transparencia\FilterDataTrait;
use Dompdf\FontMetrics;
use Illuminate\Support\Facades\Storage;

class RelatorioArpPdfService
{
    public function generatePDF(
        $arpId,
        ArpPdfRepository $arpItemPdfRepository
    ) {
        $arp = Arp::findOrFail($arpId); // Obtenha a ARP pelo ID
        $arpPdf = $arp->arpPdf;
        $numeroCompra = $arp->getNumeroCompra();
        $modalidadeCompra = $arp->getModalidadeCompra();
        $valorTotal = $arp->getValorTotal();
        $linkPncp = $arp->getLinkPNCP();
        $dadosFornecedores = $arp->getFornecedorPorAtaTransparencia();
        $arpDados =
            $arpItemPdfRepository->getArpPdfData($arpId);
        $unidadeAtaTexto = $arpDados->cod;
        $numeroAtaTexto = $arpDados->arp_numero;

        $createdAt = Carbon::now();
        if (!is_null($arpPdf)) {
            $createdAt = $arpPdf->created_at;
        }
        $dataGeracao = $createdAt->format('d/m/Y H:i:s');

        $items =
            $arpItemPdfRepository->getItemPdf($arpId);
        $arpDetalhamento =
            $arpItemPdfRepository->getItemsByNumero($arpId);
        $arpFornecedor =
            $arpItemPdfRepository->getFornecedorItens($arpId);
        $arpUnidade =
            $arpItemPdfRepository->getItemsByUnidadeParticipantePorAtaItem(
                $arpId
            );
        $adesaoTotalizador =
            $arpItemPdfRepository->getItemsByTotalizadoresAdesao($arpId);
        $adesaoAprovadas =
            $arpItemPdfRepository->getItemsByAdesoesAnalise(
                $arpId,
                'analisada'
            );
        $adesaoAnalise =
            $arpItemPdfRepository->getItemsByAdesoesAnalise(
                $arpId,
                'aguardando_analise'
            );

        $totalizadorMinutaEmpenho =
            $arpItemPdfRepository->getItemsByTotaisMinutaEmpenho(
                $arpId
            );

        $minutasEmpenhoPorUnidade =
            $arpItemPdfRepository->getItemsByMinutasEmpenhoPorUnidade(
                $arpId
            );

        $somaResultadosEmpenho = null;
        $somaResultadosSaldo = null;

        if (count($totalizadorMinutaEmpenho) > 0 or
            count($minutasEmpenhoPorUnidade) > 0) {
            $somaResultadosEmpenho =
                $arpItemPdfRepository->getItemsByTotalRegistradaAprovadaSomada(
                    $arpId
                );
            $somaResultadosSaldo =
                $arpItemPdfRepository->getItemsByTotalRegistradaSaldoSomada(
                    $arpId
                );
        }
        $arpContratos =
          $arpItemPdfRepository->getItemsByContratosItem($arpId);

        $arpAlteracao =
            $arpItemPdfRepository->getItemsByBuscaArpAlteracaoPdf(
                $arpId
            );

        foreach ($arpAlteracao as $alteracao) {
            $alteracao->status_descricao = $alteracao->status->descricao;
            $alteracao->objeto_descricao = $alteracao->objeto_alteracao;
            $alteracao->motivo_cancelamento_descricao =
                $alteracao->justificativa_motivo_cancelamento;
        }
        $dompdf = RelatorioPdfService::create(
            'A4',
            RelatorioPdfService::LANDSCAPE
        );

        $logo = 'data:image/png;base64,' . base64_encode(
            file_get_contents(public_path('img/logo_relatorio.png'))
        );

        $html = view('relatorio.rel-pdf', [
            'arp' => $arp,
            'logo' => $logo,
            'items' => $items,
            'numeroCompra' => $numeroCompra,
            'modalidadeCompra' => $modalidadeCompra,
            'valorTotal' => $valorTotal,
            'linkPncp' => $linkPncp,
            'arpDados' => $arpDados,
            'dadosFornecedores' => $dadosFornecedores,
            'arpDetalhamento' => $arpDetalhamento,
            'arpFornecedor' => $arpFornecedor,
            'arpUnidade' => $arpUnidade,
            'adesaoTotalizador' => $adesaoTotalizador,
            'adesaoAprovadas' => $adesaoAprovadas,
            'adesaoAnalise' => $adesaoAnalise,
            'somaResultadosEmpenho' => $somaResultadosEmpenho,
            'somaResultadosSaldo' => $somaResultadosSaldo,
            'totalizadorMinutaEmpenho' => $totalizadorMinutaEmpenho,
            'minutasEmpenhoPorUnidade' => $minutasEmpenhoPorUnidade,
            'arpContratos' => $arpContratos,
            'arpAlteracao' => $arpAlteracao,

        ])->render();


        $dompdf->loadHtml($html);
        $dompdf->render();
        $dompdf->addNumber($numeroAtaTexto, $unidadeAtaTexto, $dataGeracao);


        $pdfPath = 'Relatorio'.$arpId.'.pdf';
        $folder = 'app/arp_pdf/pdf/'.date('Y-m') ;
        $folderFullPath = storage_path($folder);

        if (!is_dir($folderFullPath)) {
            mkdir($folderFullPath, 0777, true);
        }
        $pdfFullPath = $folderFullPath.'/'.$pdfPath;

        $caminhoNoBanco = $folder.'/'.$pdfPath;
        $pdfRecord = new ArpPdf();
        $pdfRecord->arp_id = $arpId;
        $pdfRecord->caminho_pdf = $caminhoNoBanco;
        $pdfRecord->gerado = false;
        $pdfRecord->save();

        $pdfRecord->update(['gerado' => true]);

        $output = $dompdf->output();


        file_put_contents(
            $pdfFullPath,
            $output
        );


        //file_put_contents($pdfFullPath, $output);

     //    return $dompdf->stream('rel-pdf.pdf');

    //  return $dompdf->stream('', array("Attachment" => false));
    }
}
