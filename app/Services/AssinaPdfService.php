<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class AssinaPdfService extends \TCPDF
{
    private $pdfdoc;
    private $byte_range = [];
    private $primeiraAssinatura = false;

    const SIGNATURE_WIDTH = 80;
    const SIGNATURE_HEIGHT = 15;

    public function __construct(string $pathPdf)
    {
        parent::__construct();

        $contentPdf = Storage::disk('public')->get($pathPdf);

        if (!strstr($contentPdf, '/ByteRange')) {
            $this->primeiraAssinatura = true;
        }

        $this->state = 1;
        $this->setBuffer($contentPdf . "\n");

        $this->includeSignature();
    }

    private function includeSignature()
    {
        $bytesLastStartxref = $this->getBytesLastStartxref();
        $xrefContent = substr($this->buffer, $bytesLastStartxref);

        preg_match("/\/ID\s*\[\s*<([a-fA-F0-9]+)>/", $xrefContent, $matches);
        $fileId = $matches[1];
        preg_match("/\/Size\s+(\d+)/", $xrefContent, $matches);
        $sizeValue = $matches[1];
        preg_match("/\/Root\s+(\d+)/", $xrefContent, $matches);
        $rootValue = $matches[1];
        preg_match("/\/Info\s+(\d+)/", $xrefContent, $matches);
        $infoValue = $matches[1];

        $this->offsets[$rootValue] = $this->bufferlen;
        $this->n = $sizeValue - 1;

        $this->cloneObjCatalog($rootValue, $sizeValue);

        $paginaAssinatura = session()->get('assinagov.pagina_assinatura') ?? 1;
        $posicaoXAssinatura = session()->get('assinagov.posicao_x_assinatura') ?? -200;
        $posicaoYAssinatura = session()->get('assinagov.posicao_y_assinatura') ?? 0;

        $this->setSignatureAppearance(
            $posicaoXAssinatura,
            $posicaoYAssinatura,
            self::SIGNATURE_WIDTH,
            self::SIGNATURE_HEIGHT,
            $paginaAssinatura,
            'Assinado por: ' . backpack_user()->name,
        );
        $this->setSignature('cert', 'cert', 'cert', '', 2, [], $this->primeiraAssinatura ? '' : 'A');

        $objIdPageToLinkSignature = $this->getIdObjPaginaAssinatura($paginaAssinatura);
        if ($this->sign and isset($this->signature_data['cert_type'])) {
            // widget annotation for signature
            $out = $this->_getobj($this->sig_obj_id) . "\n";
            $out .= '<< /Type /Annot';
            $out .= ' /Subtype /Widget';
            $out .= ' /Rect [' . $this->signature_appearance['rect'] . ']';
            $out .= ' /P ' . $objIdPageToLinkSignature . ' 0 R'; // link to signature appearance page
            $out .= ' /F 4';
            $out .= ' /FT /Sig';
            $out .= ' /T ' . $this->_textstring($this->signature_appearance['name'], $this->sig_obj_id);
            $out .= ' /Ff 0';
            $out .= ' /V ' . ($this->sig_obj_id + 1) . ' 0 R';
            $out .= ' /AP << /N ' . ($this->sig_obj_id + 2) . ' 0 R>>';
            $out .= ' >>';
            $out .= "\n" . 'endobj';
            $this->_out($out);
            // signature
            $this->_putsignature();
        }

        $this->cloneObjPageToLinkSignature($objIdPageToLinkSignature, $this->sig_obj_id);

        $usuario = backpack_user();
        $userId = $usuario->id;
        $file = Storage::disk('public')->path("temporario/assinatura-$userId.png");
        $this->criarImagemAssinatura($usuario->name, $file);
        list($width, $height) = getimagesize($file);

        $this->state = 2;

        $this->startTemplate($width, $height);
        $this->Image($file, 0, 0, $width, $height);
        $this->state = 1;
        $this->_putimages();
        $this->compress = false;
        $this->_putxobjects();
        // corrige bug do calculo errado do /Length em xobjects para mais detalhes:
        // https://github.com/tecnickcom/TCPDF/issues/723
        $this->recalculaLengthStream();

        $bytesStartxref = $this->bufferlen;

        $this->includeXref($rootValue, $objIdPageToLinkSignature);

        $this->includeTrailer($rootValue, $infoValue, $bytesLastStartxref, $fileId);

        $this->_out('startxref');
        $this->_out($bytesStartxref);
        $this->_out('%%EOF');

        Storage::disk('public')->delete("temporario/assinatura-$userId.png");
    }

    private function getBytesLastStartxref()
    {
        return preg_replace('/[^0-9]/', '', substr($this->buffer, -20, -7));
    }

    private function getIdObjPaginaAssinatura(int $paginaAssinatura = 1): int
    {
        $patternKidsSection = '/\/Kids\s*\[\s*([^\]]+)\s*\]/';
        abort_unless(preg_match($patternKidsSection, $this->buffer, $matches), 500, '/Kids not found');
        $kidsContent = $matches[1];
        $objIds = explode('0 R', $kidsContent);
        return (int) trim($objIds[$paginaAssinatura - 1]);
    }

    private function cloneObjCatalog(int $objIdCatalog, int $objIdSignature)
    {
        // todo dry with cloneObjPageToLinkSignature
        $lengthEndobj = strlen('endobj');
        $startObjCatalog = strrpos($this->buffer, "\n$objIdCatalog 0 obj");
        $closeObjCatalog = strpos($this->buffer, 'endobj', $startObjCatalog) + $lengthEndobj;

        $catalogClone = substr($this->buffer, $startObjCatalog, $closeObjCatalog - $startObjCatalog);
        if ($this->primeiraAssinatura) {
            $catalogClone = substr($catalogClone, 0, strrpos($catalogClone, '>>')) .
                "/AcroForm << /Fields [$objIdSignature 0 R] /SigFlags 3 >> >>\nendobj";
        } else {
            $catalogClone = str_replace('0 R]', '0 R ' . ($objIdSignature) . ' 0 R]', $catalogClone);
        }

        $this->_out($catalogClone);
    }

    private function cloneObjPageToLinkSignature(int $objIdPageToLinkSignature, int $objIdSignature)
    {
        $this->_getobj($objIdPageToLinkSignature);

        $lengthEndobj = strlen('endobj');
        $startObj = strrpos($this->buffer, "\n$objIdPageToLinkSignature 0 obj");
        $closeObj = strpos($this->buffer, 'endobj', $startObj) + $lengthEndobj;

        $clone = substr($this->buffer, $startObj, $closeObj - $startObj);
        $patternAnnotsSection = '/\/Annots\s*\[\s*([^\]]+)\s*\]/';
        preg_match($patternAnnotsSection, $clone, $matches);
        if (isset($matches[1])) {
            $clone = str_replace($matches[1], $matches[1] . ' ' . ($objIdSignature) . ' 0 R', $clone);
        } else {
            $clone = substr($clone, 0, strrpos($clone, '>>')) .
                "/Annots [$objIdSignature 0 R] >>\nendobj";
        }
        $this->_out($clone);
    }

    private function includeXref(int $objIdCatalog, int $objIdPageToLinkSignature)
    {
        $this->_out('xref');
        $this->_out('0 1');
        $this->_out('0000000000 65535 f ');
        if (isset($this->offsets[$objIdPageToLinkSignature])) {
            $this->_out($objIdPageToLinkSignature. ' 1');
            $this->_out(sprintf('%010d 00000 n ', $this->offsets[$objIdPageToLinkSignature]));
            unset($this->offsets[$objIdPageToLinkSignature]);
        }
        if (!isset($this->offsets[$objIdCatalog + 1])) {
            $this->_out($objIdCatalog . ' 1');
            $this->_out(sprintf('%010d 00000 n ', $this->offsets[$objIdCatalog]));
            unset($this->offsets[$objIdCatalog]);

            $nextIdObject = array_key_first($this->offsets);
            $this->_out($nextIdObject . ' ' . count($this->offsets));
        } else {
            $this->_out($objIdCatalog . ' ' . count($this->offsets));
        }

        ksort($this->offsets);
        foreach ($this->offsets as $key => $offset) {
            $this->_out(sprintf('%010d 00000 n ', $offset));
        }
    }

    private function includeTrailer(int $objIdCatalog, int $objIdInfo, int $prevStartxref, string $fileIdPrev)
    {
        $out = 'trailer' . "\n";
        $out .= '<<';
        $out .= ' /Size ' . ($this->n + 1);
        $out .= ' /Root ' . $objIdCatalog . ' 0 R';
        $out .= ' /Info ' . $objIdInfo . ' 0 R';
        $out .= ' /ID [ <' . $fileIdPrev . '> <' . $this->file_id . '> ]';
        $out .= '/Prev ' . $prevStartxref;
        $out .= ' >>';
        $this->_out($out);
    }
    private function criarImagemAssinatura($nomeSigntario, $filePath)
    {
        // Dimensões da imagem final
        $width = 500; // Largura da imagem
        $height = 100; // Altura da imagem

        // Criar uma imagem em branco
        $image = imagecreatetruecolor($width, $height);

        // Definir cores
        $white = imagecolorallocate($image, 255, 255, 255);

        // Preencher a imagem com fundo branco
        imagefilledrectangle($image, 0, 0, $width, $height, $white);

        // Carregar a logo do gov.br
        $logo = imagecreatefrompng(public_path('img/govbr_logo.png'));
        list($logoWidth, $logoHeight) = getimagesize(public_path('img/govbr_logo.png'));

        // Calcular a nova altura da logo mantendo a proporção
        $newLogoWidth = 120;
        $newLogoHeight = ($logoHeight / $logoWidth) * $newLogoWidth;

        // Criar uma nova imagem temporária para a logo redimensionada
        $tempLogo = imagecreatetruecolor($newLogoWidth, $newLogoHeight);
        imagealphablending($tempLogo, false);
        imagesavealpha($tempLogo, true);

        // Redimensionar a logo
        imagecopyresampled($tempLogo, $logo, 0, 0, 0, 0, $newLogoWidth, $newLogoHeight, $logoWidth, $logoHeight);

        // Calcular a posição do logo na imagem
        $logoX = 5; // Margem esquerda
        $logoY = ($height - $newLogoHeight) / 2; // Centralizar verticalmente

        // Inserir o logo redimensionado na imagem principal
        imagecopy($image, $tempLogo, $logoX, $logoY, 0, 0, $newLogoWidth, $newLogoHeight);

        // Definir o texto e a fonte
        $fontPath = public_path('fonts/arial.ttf');
        // Calcular a posição do texto
        $textX = $logoX + $newLogoWidth  + 10; // Logo + margem
        $textY = 20; // Primeira linha de texto

        $black = imagecolorallocate($image, 0, 0, 0);
        // Adicionar o texto na imagem

        imagettftext(
            $image,
            10,
            0,
            $textX,
            $textY,
            $black,
            public_path('fonts/arialbd.ttf'),
            'Documento assinado digitalmente'
        );

        $count = 0;
        $nome = array_reduce(explode(' ', $nomeSigntario), function ($carry, $item) use (&$count) {
            if ($count < 3) {
                $carry .= $item . ' ';
            }
            $count++;
            return $carry;
        });
        imagettftext(
            $image,
            12,
            0,
            $textX,
            $textY + 25,
            $black,
            $fontPath,
            strtoupper($nome)
        );
        imagettftext(
            $image,
            10,
            0,
            $textX,
            $textY + 50,
            $black,
            $fontPath,
            'Data: ' . Carbon::now()->format('d/m/Y H:i:sP')
        );

        $linkVerificador = (config('app.app_amb') == 'Ambiente Produção') ? 'https://validar.iti.gov.br' :
            'https://verificador.staging.iti.br';
        imagettftext(
            $image,
            10,
            0,
            $textX,
            $textY + 70,
            $black,
            $fontPath,
            "Verifique em $linkVerificador"
        );

        // Salvar a imagem
        imagepng($image, $filePath);

        // Liberar a memória
        imagedestroy($image);
        imagedestroy($logo);
    }

    public function getConteudoByteRange(): string
    {
        $this->pdfdoc = !empty($this->buffer) ? $this->buffer : $this->pdf->getPDFData();
        throw_if(
            !strstr($this->pdfdoc, \TCPDF_STATIC::$byterange_string),
            'ByteRange não encontrado'
        );

        $lengthOpenCloseSig = strlen('<>');
        $byterange_string_len = strlen(\TCPDF_STATIC::$byterange_string);
        $this->byte_range[0] = 0;
        $this->byte_range[1] = strpos($this->pdfdoc, \TCPDF_STATIC::$byterange_string) + $byterange_string_len + 10;
        $this->byte_range[2] = $this->byte_range[1] + $this->signature_max_length + $lengthOpenCloseSig;
        $this->byte_range[3] = strlen($this->pdfdoc) - $this->byte_range[2];

        // replace the ByteRange
        $byterange = sprintf(
            '/ByteRange[0 %u %u %u]',
            $this->byte_range[1],
            $this->byte_range[2],
            $this->byte_range[3],
        );
        $byterange .= str_repeat(' ', ($byterange_string_len - strlen($byterange)));
        $this->pdfdoc = str_replace(\TCPDF_STATIC::$byterange_string, $byterange, $this->pdfdoc);

        $this->pdfdoc = substr($this->pdfdoc, 0, $this->byte_range[1]) . substr($this->pdfdoc, $this->byte_range[2]);

        return $this->pdfdoc;
    }

    public function addAssinaturaDigital(string $assinaturaBase64): string
    {
        $signature = bin2hex($assinaturaBase64);
        $signature = str_pad($signature, $this->signature_max_length, '0');

        throw_if(
            empty($this->byte_range),
            'ByteRange não encontrado, por favor formate o arquivo PDF antes de adicionar a assinatura digital'
        );

        $this->pdfdoc = substr($this->pdfdoc, 0, $this->byte_range[1]) .
            '<' . $signature . '>' .
            substr($this->pdfdoc, $this->byte_range[1]);

        return $this->pdfdoc;
    }

    private function recalculaLengthStream(): void
    {
        $lengthStream = strrpos($this->buffer, "/Length ") + strlen("/Length ");

        $startStream = strrpos($this->buffer, " >> stream");
        $finishStream = strrpos($this->buffer, "endstream");
        $stream = substr($this->buffer, $startStream, $finishStream - ($startStream + strlen(" >> stream")));
        $newLengthStream = strlen($stream);

        $this->buffer = substr($this->buffer, 0, $lengthStream) . $newLengthStream .
            substr($this->buffer, $startStream);
    }
}
