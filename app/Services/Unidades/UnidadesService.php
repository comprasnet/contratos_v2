<?php

namespace App\Services\Unidades;

use App\Api\Externa\ApiUnidades;
use App\Http\Traits\ExternalServices;
use App\Repositories\OrgaoRepository;
use App\Repositories\UnidadeRepository;
use App\Services\Orgao\OrgaoService;

class UnidadesService extends ApiUnidades
{
    
    public function getUnidadePorCodigo(string $codigo)
    {
        $dadosUnidade = $this->getUnidadePorCodigoService($codigo);

        $orgaoRepository = new OrgaoRepository();
        $dadosOrgaoBase =  $orgaoRepository->getOrgaoPorCodigo($dadosUnidade->numeroOrgao);
        
        if (empty($dadosOrgaoBase)) {
            $orgaoService = new OrgaoService();
            $dadosOrgaoBase = $orgaoService->getOrgaoAPI($dadosUnidade->numeroOrgao);
        }
        
        $arrayInserUnidade = [
            'orgao_id' => $dadosOrgaoBase->id,
            'codigo' => $dadosUnidade->numero,
            'codigosiasg' => $dadosUnidade->numero,
            'nome' => $dadosUnidade->nome,
            'nomeresumido' => $dadosUnidade->nome,
            'tipo' => 'C',
            'situacao' => $dadosUnidade->ativa,
            'sisg' =>  $dadosUnidade->indicadorSisg,
            'aderiu_siasg' =>  $dadosUnidade->adesaoSiasg,
        ];
        try {
            $unidadeRepository = new UnidadeRepository();
            return $unidadeRepository->salvarUnidade($arrayInserUnidade);
        } catch (\Exception $ex) {
            $this->inserirLogCustomizado('unidades', 'error', $ex);
        }
    }

    public function getEsferaUnidade(int $unidadeId)
    {
        $unidadeRepository = new UnidadeRepository();
        $unidade = $unidadeRepository->getUnidadePorId($unidadeId);

        return $unidade->esfera;
    }
}
