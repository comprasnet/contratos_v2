<?php

namespace App\Services\UsuarioFornecedor;

use App\Models\Contratoarquivo;
use App\Models\Contratopreposto;
use App\Models\CodigoItem;
use App\Models\HistoricoPrepostoIndicacaoFornecedor;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;

class UsuarioFornecedorContratoPrepostoService
{
    /**
     * Salva ou modifica os dados de um preposto.
     *
     * @param array $dados
     * @param bool $update
     * @param int|null $prepostoId
     * @return Contratopreposto
     * @throws Exception
     */
    
    public function salvarAtualizarPreposto(
        array $dados,
        bool $update = false,
        ?int $prepostoId = null
    ): Contratopreposto {
        return DB::transaction(function () use ($dados, $update, $prepostoId) {

            if (array_key_exists('usuarioInterno', $dados)) {
                return $this->salvarAtualizarPrepostoInternamente($dados, $update, $prepostoId);
            }

            $statusId = CodigoItem::where('descres', 'sit_indica_prepost_1')->firstOrFail()->id;
            if ($update && $prepostoId) {
                $preposto = Contratopreposto::findOrFail($prepostoId);
                $preposto->fill($dados);
            } else {
                $preposto = new Contratopreposto($dados);
                $preposto->situacao = false;
                $preposto->status_indicacao_id = $statusId;
            }

            $preposto->save();

            return $preposto;
        });
    }

    /**
     * Salva o histórico de status da indicação de um preposto.
     *
     * @param int $userId ID do usuário que realizou a operação.
     * @param int $status Código do status.
     * @param int $contratoPrepostoId ID do contrato do preposto.
     * @return HistoricoPrepostoIndicacaoFornecedor|null Retorna o histórico salvo ou null em caso de falha.
     */
    public function salvarHistoricoStatusIndicacaoPreposto(
        int $userId,
        int $status,
        int $contratoPrepostoId
    ): ?HistoricoPrepostoIndicacaoFornecedor {
        $historico = new HistoricoPrepostoIndicacaoFornecedor();
        $historico->user_id = $userId;
        $historico->status = $status;
        $historico->contratopreposto_id = $contratoPrepostoId;

        return $historico->save() ? $historico : null;
    }

    /**
     * Salva a aceitação ou recusa da indicação do preposto e altera status ativo/inativo conforme aceitação ou recusa.
     *
     * @param int $contratoPrepostoId ID do registro do preposto indicado na tabela contratopreposto.
     * @param string $statusIndicacaoDescres Descres do status de aceitação para busca na tabela codigoitens.
     * @param int $contratoId ID do contrato.
     * @return void.
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException Se o preposto ou o status não forem encontrados.
     */
    public function statusAceitacaoIndicacaoPreposto(
        int $contratoPrepostoId,
        string $statusIndicacaoDescres,
        int $contratoId
    ): void {
        $preposto = Contratopreposto::findOrFail($contratoPrepostoId);
        $novoStatus = CodigoItem::where('descres', $statusIndicacaoDescres)->firstOrFail();
        $preposto->situacao = $statusIndicacaoDescres == 'sit_indica_prepost_2';
        $preposto->status_indicacao_id = $novoStatus->id;
        $preposto->save();
        // se aceito desative os outros ativos
        if ($preposto->situacao) {
            $this->desativarOutrosPrepostosDoContrato($contratoId, $preposto->id);
        }

        // salve o histórico
        $this->salvarHistoricoStatusIndicacaoPreposto(
            backpack_user()->id,
            $preposto->status_indicacao_id,
            $preposto->id
        );
    }

    /**
     * Desativa todos os prepostos de um contrato, exceto o preposto especificado.
     *
     * Este método atualiza o campo `situacao` para `false` em todos os registros de prepostos relacionados
     * ao contrato, exceto para o preposto com o ID especificado.
     *
     * @param int $contratoId ID do contrato ao qual os prepostos estão vinculados.
     * @param int $contratoPrepostoId ID do preposto que deve permanecer ativo.
     * @return void
     */
    public function desativarOutrosPrepostosDoContrato(int $contratoId, int $contratoPrepostoId): void
    {
        // encontre os prepostos ativos neste contrato exceto o que vai ser aceito
        $prepostosAtivosContrato = Contratopreposto::where('contrato_id', $contratoId)
            ->where('id', '!=', $contratoPrepostoId)
            ->where('situacao', true)->get();

        // $statusDesativarId = CodigoItem::where('descres', 'sit_indica_prepost_4')->firstOrFail()->id;
        $statusDesativarId = CodigoItem::where('descres', 'sit_indica_prepost_3')->firstOrFail()->id;

        foreach ($prepostosAtivosContrato as $contratoPreposto) {
            // desative o preposto
            $contratoPreposto->situacao = false;
            $contratoPreposto->update();
            // salve no histórico a ação
            $this->salvarHistoricoStatusIndicacaoPreposto(
                backpack_user()->id,
                $statusDesativarId,
                $contratoPreposto->id
            );
        }
    }

    /**
     * Salva um arquivo associado ao contrato.
     *
     * @param \Illuminate\Http\UploadedFile $file O arquivo enviado.
     * @param int $contratoId ID do contrato.
     * @return Contratoarquivo O arquivo salvo.
     */
    public function salvarArquivo($file, int $contratoId, int $contratoPrepostoId): Contratoarquivo
    {
        $filePath = $file->store(
            'contratopreposto/' . Carbon::now()->format('Y_m') . '/' . $contratoId,
            'public'
        );

        $contratoArquivo = Contratoarquivo::updateOrCreate([
            'contrato_id' => $contratoId,
            'tipo' => CodigoItem::where('descricao', 'Outros Arquivos')->firstOrFail()->id,
            'origem' => 3,
            'descricao' => $file->getClientOriginalName(),
            'arquivos' => $filePath,
            'restrito' => true,
            'envio_v2' => true,
            'contratopreposto_id' => $contratoPrepostoId
        ]);

        //todo Salvar o arquivo sem disparar  o observer contratoObserve
        //$contratoArquivo->saveWithoutEvents();

        return $contratoArquivo;
    }

    /**
     * Salva ou modifica os dados de um preposto quando internamente pelo sistema (não no módulo Fornecedor).
     *
     * @param array $dados
     * @param bool $update
     * @param int|null $prepostoId
     * @return Contratopreposto
     * @throws Exception
     */

    public function salvarAtualizarPrepostoInternamente(
        array $dados,
        bool $update = false,
        ?int $prepostoId = null
    ): Contratopreposto {
        // resolve se é update ou create e seta os dados no conforme
        if ($update && $prepostoId) {
            $preposto = Contratopreposto::findOrFail($prepostoId);
            $aceitoId = CodigoItem::where('descres', 'sit_indica_prepost_2')->firstOrFail()->id;
            if ($dados['situacao']) {
                $preposto->status_indicacao_id = $aceitoId;
            }
            $preposto->fill($dados);
        } else {
            $preposto = new Contratopreposto($dados);
            $preposto->status_indicacao_id = $dados['situacao']
                ? CodigoItem::where('descres', 'sit_indica_prepost_2')->firstOrFail()->id
                : CodigoItem::where('descres', 'sit_indica_prepost_1')->firstOrFail()->id;
        }

        $preposto->save();

        // se esta criando ou editando já ativo, desative os outros prepostos do contrato
        if ($dados['situacao']) {
            $this->desativarOutrosPrepostosDoContrato(intval($dados['contrato_id']), $preposto->id);
        }

        // após criado o preposto, salve o histórico
        $this->salvarHistoricoStatusIndicacaoPreposto(
            backpack_user()->id,
            $preposto->status_indicacao_id,
            $preposto->id
        );

        return $preposto;
    }
}
