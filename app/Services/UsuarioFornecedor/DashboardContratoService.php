<?php

namespace App\Services\UsuarioFornecedor;

use App\Repositories\UsuarioFornecedor\ContratosDashboardRepository;

class DashboardContratoService
{
    /**
     * @var ContratosDashboardRepository
     */
    public static $repository;

    public static function getRepositoryByRequest()
    {
        if (!isset(self::$repository)) {
            $contratosDashboard = new ContratosDashboardRepository();
            self::$repository = $contratosDashboard;
        }
        return self::$repository;
    }
}
