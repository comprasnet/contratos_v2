<?php

namespace App\Services\UsuarioFornecedor;

use App\Repositories\UsuarioFornecedor\AtasDashboardRepository;

class DashboardArpFornecedorService
{

    /**
     * @var AtasDashboardRepository
     */
    public static $repository;

    public static function getRepositoryByRequest()
    {
        if (!isset(self::$repository)) {
            $contratosDashboard = new AtasDashboardRepository();
            self::$repository = $contratosDashboard;
        }
        return self::$repository;
    }
}
