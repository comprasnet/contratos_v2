<?php

namespace App\Services\UsuarioFornecedor;

use App\Api\Externa\ApiUsuarioFornecedor;
use App\Http\Traits\ImportContent;
use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use App\Models\BackpackUser;
use App\Models\CodigoItem;
use App\Models\Contrato;
use App\Models\Contratopreposto;
use App\Models\Fornecedor;
use App\Http\Traits\Formatador;
use App\Models\Tipolistafatura;
use App\Repositories\AmparoLegalRepository;
use App\Repositories\JustificativaFaturaRepository;
use App\Repositories\TipoListaFaturaRepository;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use App\Repositories\UsuarioFornecedor\UsuarioFornecedorRepository;
use App\Repositories\Fornecedor\FornecedorRepository;
use App\Repositories\ContratoRepository;
use App\Repositories\CodigoItemRepository;
use App\Http\Requests\UsuarioFornecedorRequest;
use App\Http\Traits\TracksUserLogin;

class UsuarioFornecedorService extends ApiUsuarioFornecedor
{
    use ImportContent;
    use Formatador;
    use UsuarioFornecedorTrait;
    use TracksUserLogin;

    protected $usuarioFornecedorRepository;
    protected $fornecedorRepository;
    protected $contratoRepository;
    protected $codigoItemRepository;
    protected $amparoLegalRepository;
    protected $tipoListaFaturaRepository;
    protected $justificativaFaturaRepository;

    public function __construct(
        UsuarioFornecedorRepository $usuarioFornecedorRepository,
        FornecedorRepository $fornecedorRepository,
        ContratoRepository $contratoRepository,
        CodigoItemRepository $codigoItemRepository,
        AmparoLegalRepository $amparoLegalRepository,
        TipoListaFaturaRepository $tipoListaFaturaRepository,
        JustificativaFaturaRepository $justificativaFaturaRepository
    ) {
        parent::__construct();
        $this->usuarioFornecedorRepository = $usuarioFornecedorRepository;
        $this->fornecedorRepository = $fornecedorRepository;
        $this->contratoRepository = $contratoRepository;
        $this->codigoItemRepository = $codigoItemRepository;
        $this->amparoLegalRepository = $amparoLegalRepository;
        $this->tipoListaFaturaRepository = $tipoListaFaturaRepository;
        $this->justificativaFaturaRepository = $justificativaFaturaRepository;
    }

    /**
     * Procura um usuário pelo campo  cpf.
     *
     * @param string $cpf O CPF do usuario.
     * @param string $tipoFornecedor O tipo do fornecedor.
     *@return BackpackUser|null Retorna uam instãncia de  User caso seja encontrado, ou null se não encontrado
     */
    public function verificarUsuario(string $cpf, string $tipoFornecedor): ?BackpackUser
    {
        if ($tipoFornecedor == 'E') {
            return $this->usuarioFornecedorRepository->verificarUsuarioRepository($cpf);
        }

        return $this->usuarioFornecedorRepository->verificarUsuarioRepository($this->formataCpf2($cpf));
    }

    /**
     * Registra um usuário para o fornecedor.
     *
     * @param array $request Os dados do usuário.
     * @return BackpackUser Retorna uma instância de BackpackUser representando o usuário registrado.
     */
    public function cadastraUsuarioFornecedor(array $request): ?BackpackUser
    {
        //dd(gettype($request), $request);
        try {
            DB::beginTransaction();
            //dados gerais
            $dados = $request;
            //dados do usuario
            $dados['name'] = strtoupper($request['nome']);
            $dados['cpf'] = $this->retornaCpfCnpjUsuarioFornecedorAcessoComprasGov(
                $dados['tipoPessoaFornecedor'],
                $request['identificadorUsuario']
            );
            $randomPassword = Str::random();
            $dados['password'] = Hash::make($randomPassword);
            $dados['acesso_compras_fornecedor'] = true;
            $dados['administrador'] = $dados['administrador'] == '1';
            //dados do fornecedor
            $dados['tipo_fornecedor'] = $this->retornaTipoFornecedorAcessoComprasGov($dados['tipoPessoaFornecedor']);
            $dados['cpf_cnpj_idgener'] = $this->retornaCpfCnpjFornecedorAcessoComprasGov(
                $dados['tipoPessoaFornecedor'],
                $dados['identificadorFornecedor']
            );
            $dados['nome'] = $dados['nomeRazaoSocialFornecedor'];

            if ($user = $this->usuarioFornecedorRepository->verificarUsuarioRepository($dados['cpf'])) {
                return $user;
            }
            $usuarioFornecedor = $this->usuarioFornecedorRepository->cadastrarUsuarioFornecedorRepository($dados);
            $usuarioFornecedor->assignRole('Fornecedor');

            $fornecedor = $this->fornecedorRepository->cadastrarAtualizarFornecedorRepository($dados);
            $this->fornecedorRepository->vincularUsuarioFornecedorRepository(
                $fornecedor,
                $usuarioFornecedor,
                $dados['administrador']
            );

            DB::commit();
            return $usuarioFornecedor;
        } catch (Exception $e) {
            //dd($e->getMessage());
            DB::rollBack();
            Log::error('Erro ao cadastrar usuário: ' . $e->getMessage());
            return null;
        }
    }

    /**
     * Define os campos padrão para a exibição do formulário de usuário fornecedor.
     *
     * @param array $usuarioFornecedor Os dados do usuário fornecedor.
     *   - 'tipoUsuario' string O tipo de usuário.
     *   - 'identificadorUsuario' string O identificador do usuário.
     *   - 'nome' string O nome do usuário.
     *   - 'login' string O login do usuário.
     *   - 'tipoPessoaFornecedor' string O tipo de pessoa do fornecedor (F, J, E).
     *   - 'administrador' bool Indica se o usuário é administrador.
     *   - 'identificadorFornecedor' string O identificador do fornecedor.
     *   - 'nomeRazaoSocialFornecedor' string O nome ou razão social do fornecedor.
     * @return array Os campos padrão formatados para a exibição no formulário.
     */
    public function camposPadrao(Array $usuarioFornecedor): array
    {
        $this->data['telefone'] = 'telefone';
        $this->data['email'] = 'email';
        $this->data['tipoUsuario'] = $usuarioFornecedor['tipoUsuario'];
        $this->data['identificadorUsuario'] =  $usuarioFornecedor['identificadorUsuario'];
        $this->data['nome'] =  $usuarioFornecedor['nome'];
        $this->data['login'] = $usuarioFornecedor['login'];
        $this->data['tipoUsuario'] = $usuarioFornecedor['tipoUsuario'];
        $this->data['tipoPessoaFornecedor'] =  $usuarioFornecedor['tipoPessoaFornecedor'];
        $this->data['administrador'] =  $usuarioFornecedor['administrador'];
        $this->data['identificadorFornecedor'] =  $usuarioFornecedor['identificadorFornecedor'];
        $this->data['nomeRazaoSocialFornecedor'] =  $usuarioFornecedor['nomeRazaoSocialFornecedor'];
        $this->data['cnpjFormatado'] = $this->retornaCpfCnpjFornecedorAcessoComprasGov(
            $usuarioFornecedor['tipoPessoaFornecedor'],
            $usuarioFornecedor['identificadorFornecedor']
        );
        $this->data['cpfUsuarioFormatado'] = $this->retornaMascaraCpf(
            $this->retornaCpfCnpjUsuarioFornecedorAcessoComprasGov(
                $usuarioFornecedor['tipoPessoaFornecedor'],
                $usuarioFornecedor['identificadorUsuario']
            )
        );

        $arquivoJS = [
            'assets/js/blockui/jquery.blockUI.js',
            'packages/laravel-paginate/laravel-paginate.js',
            'assets/js/admin/dashboard.js'
        ];

        $this->importarScriptJs($arquivoJS);
        $this->importarScriptCss([
            'packages/backpack/base/css/dashboard/dashboard.css'
        ]);

        return $this->data;
    }

    public function tratarUsuarioFornecedorJaCadastrado(array $dadosFornecedor, BackpackUser $usuarioFornecedor): ?bool
    {
        try {
            DB::beginTransaction();
            // Atualiza ou cadastra o fornecedor
            $fornecedor = $this->fornecedorRepository->cadastrarAtualizarFornecedorRepository([
                'cpf_cnpj_idgener' =>  $this->retornaCpfCnpjFornecedorAcessoComprasGov(
                    $dadosFornecedor['tipoPessoaFornecedor'],
                    $dadosFornecedor['identificadorFornecedor'],
                ),
                'tipo_fornecedor' => $this->retornaTipoFornecedorAcessoComprasGov(
                    $dadosFornecedor['tipoPessoaFornecedor']
                ),
                'nome' => $dadosFornecedor['nomeRazaoSocialFornecedor'],
            ]);
            // Verifica se há vínculo entre o usuário e o fornecedor
            if (!$this->fornecedorRepository->verificarVinculoUsuarioFornecedorRepository(
                $fornecedor,
                $usuarioFornecedor
            )
            ) {
                // Vincula o usuário ao fornecedor
                $this->fornecedorRepository
                    ->vincularUsuarioFornecedorRepository(
                        $fornecedor,
                        $usuarioFornecedor,
                        $dadosFornecedor['administrador']
                    );

                // Diz que o suário tem acesso do compras
            }
            $this->usuarioFornecedorRepository->salvarCampoAcessoComprasFornecedor($usuarioFornecedor);
            //caso ele já tenha o vinculo dá um update no campo $dadosFornecedor
            //['administrador'] conforme o que veio da url
            $this->updateAdministradorFornecedorService(
                $usuarioFornecedor,
                $fornecedor,
                $dadosFornecedor['administrador']
            );
            //verifica se tem perfil Fornecedor, se não tem coloca
            if (!$usuarioFornecedor->hasRole('Fornecedor')) {
                $usuarioFornecedor->assignRole('Fornecedor');
            }

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Erro ao tratar usuário fornecedor: ' . $e->getMessage());
            return null;
        }
    }

    /**
     * Trata o redirecionamento com base no retorno.
     *
     * @param array $retorno Os dados de retorno do compras.gov.br.
     *
    @return \Illuminate\Contracts\Foundation\Application|
     * \Illuminate\Contracts\View\Factory|
     * \Illuminate\Contracts\View\View|
     * \Illuminate\Http\RedirectResponse
     */
    public function tratarRedirecionarRetorno(array $retorno)
    {
        $usuarioFornecedor  = $this->verificarUsuario(
            $retorno['identificadorUsuario'],
            $retorno['tipoPessoaFornecedor']
        );


        // Se o usuário não está cadastrado, redirecione para a tela de cadastro
        if (!$usuarioFornecedor) {
            $camposPadrao = $this->camposPadrao($retorno);
            return view(backpack_view('usuario-fornecedor.login'), $camposPadrao);
        }

        // já existe então loga
        backpack_auth()->login($usuarioFornecedor);
        $this->trackLogin($usuarioFornecedor);


        // Trata e verifica usuario ja existente e fornecedor logado
        $this->tratarUsuarioFornecedorJaCadastrado($retorno, $usuarioFornecedor);
        $this->salvarFornecedorNaSessao($retorno);

        Cache::forget('responseUsuarioCompras');

        if (!$retorno['administrador']) {
            return redirect()->route('fornecedor.preposto.inicio', '');
        }

        return redirect()->route('fornecedor.administrador.inicio', '');
    }

    /**
     * Salva os dados do fornecedor na sessão
     * @param array $retorno
     * @return void
     */
    public function salvarFornecedorNaSessao(array $retorno): void
    {
        //busca o fornecedor logado para sessão
        $fornecedorLogado = $this->fornecedorRepository->verificarFornecedorRepository(
            $this->retornaCpfCnpjFornecedorAcessoComprasGov(
                $retorno['tipoPessoaFornecedor'],
                $retorno['identificadorFornecedor'],
            )
        );

        session(['fornecedor_cpf_cnpj_idgener' => $fornecedorLogado->cpf_cnpj_idgener]);
        session(['fornecedor_id' => $fornecedorLogado->id]);
        $tipoAcesso = $retorno['administrador'] ? 'Administrador': 'Preposto';
        session(['tipo_acesso' => $tipoAcesso]);
        session(['nomeRazaoSocialFornecedor' => $fornecedorLogado->nome]);
        session(['acesso_pelo_compras' => true]);
    }

    /**
     * Verifica se o usuário é do sistema contratos e ainda não selecionou o fornecedor
     * @return bool
     */
    public function verificarAcessoEFornecedor(): bool
    {
        $fornecedorIdSession = session()->get('fornecedor_id');
        if ((!backpack_user()->acesso_compras_fornecedor)
            &&
           (!isset($fornecedorIdSession) || session()->get('fornecedor_id') === null)
        ) {
            return true;
        }

        return false;
    }

    public function buscarListasContratosFornecedorLogado(array $user, ?string $filterContratoFornecedor)
    {
        $fornecedorId = session()->get('fornecedor_id');

        if ($this->verificaAdministradorOuPrespostoFornecedorLogado(backpack_user(), $fornecedorId)
        ) {
            return $this->contratoRepository->getContratosUsuarioFornecedorAdministrador(
                $fornecedorId,
                $filterContratoFornecedor
            );
        }

        return $this->contratoRepository->getContratosUsuarioFornecedorPreposto(
            $fornecedorId,
            $user['cpf'],
            $filterContratoFornecedor
        );
    }

    public function buscarListasAtasFornecedorLogado(array $user, ?string $filterContratoFornecedor)
    {
        $fornecedorId = session()->get('fornecedor_id');
        
        return $this->contratoRepository->getAtasUsuarioFornecedorAdministrador(
            $fornecedorId,
            $filterContratoFornecedor
        );
    }

    public function recuperarInformacoesFornecedorAdministradorPivot(BackpackUser $user, int $fornecedorId)
    {
        return $this->usuarioFornecedorRepository->recuperarInformacoesFornecedorAdministradorPivot(
            $user,
            $fornecedorId
        );
    }

    public function verificaAdministradorOuPrespostoFornecedorLogado(BackpackUser $user, int $fonecedorId): bool
    {
        if (in_array('Administrador', backpack_user()->getRoleNames()->toArray())
            &&
            backpack_user()->acesso_compras_fornecedor == false
            &&
            session()->get('tipo_acesso') == 'Administrador'
        ) {
            return true;
        }

        $administrador = $this->usuarioFornecedorRepository->verificaAdministradorOuPrespostoFornecedorLogado(
            $user,
            $fonecedorId
        );

        if ($administrador) {
            return true;
        }

        return false;
    }

    public function verificarFornecedor(string $cpf_cnpj_idgener): ?Fornecedor
    {
        return $this->fornecedorRepository->verificarFornecedorRepository($cpf_cnpj_idgener);
    }

    public function verificarSeUserTemUgprimaria(BackpackUser $user): bool
    {
        return $this->usuarioFornecedorRepository->verificarSeUserTemUgprimariaRepository($user);
    }

    public function mudaValorAcessoComprasFornecedor(BackpackUser $user): void
    {
        $this->usuarioFornecedorRepository->mudaValorAcessoComprasFornecedorRepsitory($user);
    }

    public function updateAdministradorFornecedorService(
        BackpackUser $user,
        Fornecedor $fornecedor,
        bool $administradorFornecedor
    ): void {
        $this->usuarioFornecedorRepository
            ->updateAdministradorFornecedorRepository(
                $user,
                $fornecedor,
                $administradorFornecedor
            );
    }

    public function verificaSeUsuarioTemOutroFornecedor(int $idUser, int $idFornecedor): bool
    {
        return $this->usuarioFornecedorRepository
            ->verificaSeUsuarioTemOutroFornecedorRepository($idUser, $idFornecedor);
    }

    public function verificaSeTemLoginRealizado(): bool
    {
        // verifica se algum usuário esté autenticado
        if (Session::has('fornecedor_id') || Session::has('user_ug')) {
            return true;
        }

        return false;
    }



    public function destruirLoginPrevio(): void
    {
        $idUsuario  = auth()->id();
        if ($idUsuario != null) {
            $this->removerDadosUsuarioRedis($idUsuario);
        }

        // Se sou usuário do contratos e estava logado como usuário fornecedor no compras
        if (backpack_user()) {
            if ($this->verificarSeUserTemUgprimaria(backpack_user())) {
                $this->mudaValorAcessoComprasFornecedor(backpack_user());
            }
        }

        $this->guard()->logout();
        auth()->logout();

        Session::flush();
        if (!empty(Cache::get(session()->get('chave_responseUsuarioCompras')))) {
            Cache::forget(session()->get('chave_responseUsuarioCompras'));
        }
    }

    /**
     * Get the guard to be used during logout.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return backpack_auth();
    }

    public function retornaTiposService()
    {
        return $this->codigoItemRepository->retornaTiposRepository();
    }

    public function retornaCategoriasService()
    {
        return $this->codigoItemRepository->retornaCategoriasRepository();
    }

    public function retornaModalidadeCompraService()
    {
        return $this->codigoItemRepository->retornaModalidadeCompraRepository();
    }

    public function retornaOpcoesFiltroAmaparoLegalContratoService(string $value)
    {
        return $this->amparoLegalRepository->retornaOpcoesFiltroAmaparoLegalContratoRepository($value);
    }

    public function retornaTiposListaService()
    {
        return $this->tipoListaFaturaRepository->retornaTiposListaRepository();
    }

    public function retornaJustificativasService()
    {
        return $this->justificativaFaturaRepository->retornaJustificativasRepository();
    }

    public function retornaFiltroContratosFornecedorService()
    {
        $dados = $this->contratoRepository->retornaFiltroContratosFornecedorRepository();

        $dados->where('situacao', true);
        $dados->where('fornecedor_id', session('fornecedor_id'));
        $dados->orderBy('id'); // 'data_publicacao'

        return $dados->pluck('descricao', 'numero')->toArray();
    }

    public function retornaPrepostosContratoPorId(int $contratoId)
    {
        $prepostos = Contratopreposto::where('contrato_id', $contratoId)
            ->with(['historico' => function ($query) {
                $query->orderBy('created_at', 'asc');
            }])
            ->orderByDesc('situacao')
            ->orderByDesc('created_at')
            //->get();
            ->paginate(5);

        if ($prepostos->isEmpty()) {
            return response()->json([
                'status' => 'no_prepostos',
                'message' => 'Este contrato não tem prepostos.',
            ]);
        }

        $aceita = CodigoItem::where('descres', 'sit_indica_prepost_2')->firstOrFail()->id;
        $recusada = CodigoItem::where('descres', 'sit_indica_prepost_3')->firstOrFail()->id;
        $pendente = CodigoItem::where('descres', 'sit_indica_prepost_1')->firstOrFail()->id;

        $prepostosLGPD = $prepostos->map(function ($preposto) use ($aceita, $recusada, $pendente) {
            $preposto->cpf = $this->retornaMascaraCpf($preposto->cpf);
            $preposto->situacao = $preposto->situacao ? 'Ativo' : 'Inativo';
            $historico = $preposto->historico;

            // para quem não tem histórico(dados legados)
            $dataAtivacao = $this->retornaDataAPartirDeCampo($preposto->data_inicio);
            $dataDesativacao = 'NDA';

            if ($historico->isNotEmpty()) {
                $historicoAceito = $historico->last()->status == $aceita;
                $historicoRecusado = $historico->last()->status == $recusada;
                $historicoPendente = $historico->last()->status == $pendente;

                //quando ainda não foi aceito e está pendente
                if ($preposto->situacao == 'Inativo' && $historicoPendente) {
                    $dataAtivacao    = 'NDA';
                    $dataDesativacao = 'NDA';
                }

                //quando foi aceito (desde xx/xx/xxxx)
                if ($historicoAceito) {
                    $dataAtivacao = $historico->where('status', $aceita)->last()->created_at->format('d/m/Y');
                }

                // quando já foi aceito e depois desativado (de xx/xx/xxxx a xx/xx/xxxx)
                if ($preposto->situacao == 'Inativo' && $historicoRecusado) {
                    $dataAtivacao = $historico->where('status', $aceita)->last()->created_at->format('d/m/Y');
                    $dataDesativacao = $historico->where('status', $recusada)->last()->created_at->format('d/m/Y');
                }
            }

            $preposto->periodo = $preposto->situacao == 'Ativo'
                ? "Desde $dataAtivacao"
                : "$dataAtivacao a $dataDesativacao";

            return $preposto;
        });

        return response()->json([
            'status' => 'success',
            'data' => $prepostosLGPD,
            'pagination' => [
                'current_page' => $prepostos->currentPage(),
                'last_page' => $prepostos->lastPage(),
                'total' => $prepostos->total(),
            ],
        ]);
    }
}
