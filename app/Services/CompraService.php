<?php

namespace App\Services;

use App\Http\Controllers\Api\ConcentradorCompras\CompraSisrp;
use App\Http\Traits\Arp\ArpTrait;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\Formatador;
use App\Http\Traits\JobUserTrait;
use App\Models\ArpItemHistoricoAlteracaoFornecedor;
use App\Models\CompraItem;
use App\Models\CompraItemFornecedor;
use App\Models\CompraItemUnidade;
use App\Models\Compras;
use Illuminate\Http\Request;

class CompraService
{
    use CompraTrait;
    use JobUserTrait;
    use ArpTrait;
    use Formatador;
    
    public function inserirAtualizarCompraApi(int $compraId)
    {
        $compraOrigem = Compras::find($compraId);
        
        $retornoSiasg = $this->validarCompraAPI($compraOrigem);

        $dadosCompra = new Request();
        
        $this->formatarRequestDadosCompra($dadosCompra, $compraOrigem);
        
        if ($retornoSiasg->origemConsulta !== 'base_de_dados') {
            $this->montaParametrosCompra($retornoSiasg, $dadosCompra);
        }
        
        if ($retornoSiasg->origemConsulta !== 'base_de_dados') {
            # Grava/Atualiza os dados da compra
            $compraOrigem = $this->updateOrCreateCompra($dadosCompra);
        }
        
        return ['compra' => $compraOrigem, 'request' => $retornoSiasg];
    }
    
    public function validarCompraAPI(Compras $compra)
    {
        $compraSisrp = new CompraSisrp();
        return $compraSisrp->getCompraLeiQuatozeUmTresTres(
            $compra->numero_ano,
            $compra->unidade_origem_id,
            $compra->modalidade_id,
            $compra->unidade_subrrogada_id ?? $compra->unidade_origem_id,
            $compra->unidade_origem_id,
        );
    }
    
    public function formatarRequestDadosCompra(Request $dadosCompra, Compras $compra)
    {
            $dadosCompra->request->set('numero_ano', $compra->numero_ano);
            $dadosCompra->request->set('unidade_origem_id', $compra->unidade_origem_id);
            $dadosCompra->request->set('modalidade_id', $compra->modalidade_id);
            $dadosCompra->request->set('unidade_origem_compra_id', $compra->unidade_origem_id);
    }
    
    public function inserirAtualizarItemCompra(Compras $compra, object $retornoSiasg, int $idUsuario, $api = false)
    {
        $compraSisrp = new CompraSisrp();
        
        $jobCompraExecutado = $this->recuperarQuantidadeJobCompra($compra->id);
        
        # Se não existir processamento, recupera os itens e inclui o JOB para processamento externo
        if ($jobCompraExecutado == 0) {
            # Formatar a URL devido está pegando a do ajax
            if ($retornoSiasg->origemConsulta === 'api_novo_divulgacao') {
                $retornoSiasg->data->linkSisrpCompleto = array_map(function ($link) {
                    return [
                        'linkSisrpCompleto' => str_replace(
                            "validarcompra",
                            "item",
                            $link['linkSisrpCompleto']
                        )
                    ];
                }, $retornoSiasg->data->linkSisrpCompleto);
            }

            if (!$api) {
                $this->formatarLinkItemDisponivelAta(
                    $retornoSiasg->data->linkSisrpCompleto,
                    $compra->id,
                    $retornoSiasg->origemConsulta
                );
            }

            $this->inativarItensCompra(
                $retornoSiasg->data->linkSisrpCompleto,
                $retornoSiasg->origemConsulta,
                $compra->id
            );

            if (empty($retornoSiasg->data->linkSisrpCompleto)) {
                return;
            }

            $compraSisrp->gravarItemCompra($retornoSiasg, $compra, $idUsuario);
            
            if ($retornoSiasg->origemConsulta === 'api_siasg') {
                $itens = $this->retornarItensPodeAtualizar(
                    $retornoSiasg->data->linkSisrpCompleto,
                    $retornoSiasg->origemConsulta,
                    $compra->id
                );

                $compraSisrp->criarJOBItemParticipanteGestaoAtaAPISiasg($compra, $itens, $idUsuario);
            }
        }
    }
    
    public function validarCompraAta(Request $dadosCompra, $unidadeIdUsuarioLogado)
    {
        $compraSisrp = new CompraSisrp();
        return $compraSisrp->getCompraLeiQuatozeUmTresTres(
            $dadosCompra->get('numero_ano'),
            $dadosCompra->get('unidade_origem_id'),
            $dadosCompra->get('modalidade_id'),
            $unidadeIdUsuarioLogado,
            $dadosCompra->get('unidade_origem_compra_id')
        );
    }
    
    public function inserirAtualizarCompraAta(object $retornoSiasg, Request $dadosCompra)
    {
        if ($retornoSiasg->origemConsulta !== 'base_de_dados') {
            $this->montaParametrosCompra($retornoSiasg, $dadosCompra);
        }
        
        if ($retornoSiasg->origemConsulta !== 'base_de_dados') {
            # Grava/Atualiza os dados da compra
            $compraOrigem = $this->updateOrCreateCompra($dadosCompra);
        }
        
        return ['compra' => $compraOrigem, 'request' => $retornoSiasg];
    }
    
    public function formatarLinkItemDisponivelAta(
        array &$linkSisrpCompleto,
        int $compraId,
        string $origemConsulta
    ) {
        $arrayItemCompra = $this->retornarSomenteNumeroItemLinkCompra($linkSisrpCompleto, $origemConsulta);
        
        $historicoItensAta = $this->queryVerificarHistoricoAtaPorCompraNumeroItem($compraId, $arrayItemCompra);

        $arrayItemNaoDisponivel = array();
        $situacaoPodeSelecionarItem = ['Cancelamento de item(ns)', 'Retificar - Dados da ata e exclusão de item'];

        $itemPorSituacao = [];
        foreach ($historicoItensAta as $item) {
            $itemPorSituacao[$item['numero_item']][] = $item['descricao'];
        }

        foreach ($itemPorSituacao as $item => $situacoes) {
            $itemIndisponivel = false;
            foreach ($situacoes as $situacao) {
                if (!in_array($situacao, $situacaoPodeSelecionarItem)) {
                    $itemIndisponivel = true;
                    break;
                }
            }

            if (!$itemIndisponivel) {
                continue;
            }

            $arrayItemNaoDisponivel[] = $item;
        }

        foreach ($arrayItemNaoDisponivel as $numeroItem) {
            $linkSisrpCompleto = array_filter(
                $linkSisrpCompleto,
                function ($linkItem) use ($numeroItem, $origemConsulta) {
                    $linkItem = (object) $linkItem;
                    
                    $nomeCampo = 'numeroItem';
                    
                    if ($origemConsulta == 'api_novo_divulgacao') {
                        $nomeCampo = 'sequencialItem';
                        $numeroItem = ltrim($numeroItem, '0');
                    }
                    
                    return strpos($linkItem->linkSisrpCompleto, "{$nomeCampo}={$numeroItem}") === false;
                }
            );
        }
    }
    
    public function retornarSomenteNumeroItemLinkCompra(array $linkSisrpCompleto, string $origemConsulta)
    {
        $arrayItemCompra = array();
        foreach ($linkSisrpCompleto as $urlLink) {
            $urlLink = (object) $urlLink;
            $parsed_url = parse_url($urlLink->linkSisrpCompleto, PHP_URL_QUERY);
            
            parse_str($parsed_url, $params);

            $numeroItem = $params['numeroItem'] ?? $params['sequencialItem'];

            $arrayItemCompra[] = $this->preencherCasasDireitaEsquerda($numeroItem, 5) ;
        }
        
        return $arrayItemCompra;
    }
    
    public function inativarItensCompra(array $linkSisrpCompleto, string $origemConsulta, int $compraId)
    {
        # Variável responsável para inativar os itens
        $situacaoItem = false;
        
        # Recuperar os itens para poder inativar
        $itensCompra = $this->retornarItensPodeAtualizar($linkSisrpCompleto, $origemConsulta, $compraId);
        
        foreach ($itensCompra as $item) {
            # Inativa o item na tabela compra_items
            $item->situacao = $situacaoItem;
            
            $this->inativarUnidadesItem($item->id);
            
            $this->inativarFornecedorItem($item);
            
            $item->save();
        }
    }
    
    public function inativarUnidadesItem(int $compraItemId)
    {
        # Inativa o item na tabela compra_item_unidade
        CompraItemUnidade::where(
            [
                "compra_item_id" => $compraItemId,
                'situacao' => true
            ]
        )
            ->where('tipo_uasg', '<>', 'C')
            ->update(['situacao' => false]);
    }
    
    public function inativarFornecedorItem(CompraItem $item)
    {
        $compraItemFornecedorAtivo = $item->compraItemFornecedor->pluck('id')->toArray();
        
        $itemAlteradoFornecedor =
            ArpItemHistoricoAlteracaoFornecedor::whereIn(
                'compra_item_fornecedor_origem_id',
                $compraItemFornecedorAtivo
            )->whereNotNull('compra_item_fornecedor_destino_id')
                ->select('compra_item_fornecedor_destino_id')
                ->get()
                ->pluck('compra_item_fornecedor_destino_id')
                ->toArray();
        
        # Inativa o item na tabela compra_item_fornecedor
        CompraItemFornecedor::where("compra_item_id", $item->id)
            ->where('situacao', true)
            ->whereNotIn('id', $itemAlteradoFornecedor)
            ->update(['situacao' => false]);
    }

    public function retornarItensPodeAtualizar(array $linkSisrpCompleto, string $origemConsulta, int $compraId)
    {
        $numeroItemDisponivelAta = $this->retornarSomenteNumeroItemLinkCompra($linkSisrpCompleto, $origemConsulta);

        # Variável responsável para inativar os itens
        $situacaoItem = false;

        # Recuperar os itens para poder inativar
        return CompraItem::where(['compra_id' => $compraId, 'situacao' => true])
            ->whereIn('compra_items.numero', $numeroItemDisponivelAta)
            ->get();
    }
}
