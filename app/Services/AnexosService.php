<?php

namespace App\Services;

use App\Http\Traits\ArquivoGenericoTrait;
use App\Models\CodigoItem;
use App\Repositories\ArquivoGenerico\ArquivoGenericoRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class AnexosService
{
    use ArquivoGenericoTrait;

    private $anexos;
    private $model;
    private $urlDestino;
    private $tipoId;

    public function __construct(array $anexos, Model $model, string $urlDestino, CodigoItem $codigoItem)
    {
        $this->anexos = $anexos;
        $this->model = $model;
        if (substr($urlDestino, -1) != '/') {
            $urlDestino .= '/';
        }
        $this->urlDestino = $urlDestino;
        $this->tipoId = $codigoItem->id;
    }


    public function manipulaAnexos()
    {
        if (!empty($this->anexos)) {
            foreach ($this->anexos as $anexo) {
                if ($anexo['remove_file']) {
                    $this->excluiAnexo($anexo);
                } elseif (empty($anexo['id'])) {
                    $this->salvarAnexo($anexo);
                }
            }
        }
    }

    private function excluiAnexo(array $anexo)
    {
        if (empty($anexo['id'])) {
            $this->deleteAnexoTemporario($anexo['url_arquivo_anexo']);
        } else {
            $this->excluiArquivo($anexo['id'], false);
        }
    }

    private function salvarAnexo(array $anexo)
    {
        $urlDestino = $this->moveAnexoTemporario($anexo['url_arquivo_anexo']);
        $this->model->anexos()->create([
            'nome' => $anexo['nome_arquivo_anexo'],
            'descricao' => $anexo['descricao_arquivo_anexo'] ?? '',
            'url' => $urlDestino,
            'tipo_id' => $this->tipoId,
            'restrito' => false,
        ]);
    }

    private function deleteAnexoTemporario(string $urlArquivoAnexo): void
    {
        throw_unless(
            Storage::disk('public')->delete($urlArquivoAnexo),
            500,
            'Não foi possivel deletar o anexo temporário'
        );
    }

    private function excluiArquivo(int $id, $idIsArquivoable = true)
    {
        $arquivoGenerico = new ArquivoGenericoRepository();

        if ($idIsArquivoable) {
            $anexo = $arquivoGenerico->getArquivoGenerico($id, get_class($this->model));
        } else {
            $anexo = $arquivoGenerico->getArquivoGenericoUnico($id);
        }
        if ($anexo) {
            $this->deletarArquivoFiscamente($arquivoGenerico, $anexo->id);
            $anexo->delete();
        }
    }

    private function moveAnexoTemporario(string $urlArquivoAnexo): string
    {
        $nomeArquivoTemporario = last(explode('/', $urlArquivoAnexo));
        $result = $this->urlDestino .  '/' . $nomeArquivoTemporario;
        throw_unless(
            Storage::disk('public')->move($urlArquivoAnexo, $result),
            500,
            'Não foi possivel mover o anexo temporário'
        );
        return $result;
    }
}
