<?php

namespace App\Services\Orgao;

use App\Api\Externa\ApiOrgao;
use App\Http\Traits\ExternalServices;
use App\Http\Traits\LogTrait;
use App\Repositories\OrgaoRepository;
use App\Repositories\OrgaoSuperioresRepository;

class OrgaoService extends ApiOrgao
{
    protected $url;
    
    public function getOrgaoAPI(string $codigoOrgao)
    {
        $dadosOrgaoAPI = $this->getOrgao($codigoOrgao);

        try {
            $orgaoRepository = new OrgaoRepository();
            $orgaoSuperiorRepository = new OrgaoSuperioresRepository();
            $orgaoSuperiorBase =  $orgaoSuperiorRepository->getOrgaoSuperior($codigoOrgao);
            
            if (empty($orgaoSuperiorBase)) {
                $arrayInsertOrgao = [
                    'codigo' => $dadosOrgaoAPI->numero,
                    'nome' => $dadosOrgaoAPI->nome,
                    'situacao' => true
                ];
                $orgaoSuperiorBase = $orgaoSuperiorRepository->salvarAtualizarOrgaoSuperior($arrayInsertOrgao);
            }
            
            $orgaoBase = $orgaoRepository->getOrgaoPorCodigo($codigoOrgao);
            
            if (empty($orgaoBase)) {
                $arrayInsertOrgao = [
                    'orgaosuperior_id' => $orgaoSuperiorBase->id,
                    'codigo' => $orgaoSuperiorBase->codigo,
                    'codigosiasg' => $orgaoSuperiorBase->codigo,
                    'nome' => $orgaoSuperiorBase->nome,
                    'situacao' => true,
                    'cnpj' => $dadosOrgaoAPI->cnpj
                ];
                $orgaoBase = $orgaoRepository->salvarAtualizarOrgao($arrayInsertOrgao);
            }
            
            return $orgaoBase;
        } catch (\Exception $ex) {
            $this->inserirLogCustomizado('orgao', 'error', $ex);
        }
    }
}
