<?php

namespace App\Services\AutorizacaoExecucao;

use App\Mail\NotificarAutorizacaoExecucaoEntregaMail;
use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoExecucaoEntrega;
use App\Models\AutorizacaoExecucaoEntregaTRD;
use App\Models\AutorizacaoExecucaoEntregaTRP;
use App\Repositories\Contrato\ContratoPrepostoRepository;
use App\Services\ModelSignatario\ModelDTO;
use Illuminate\Database\Eloquent\Model;

class AutorizacaoExecucaoEntregaSignatarioService extends AbstractAutorizacaoExecucaoSignatarioService
{
    public function __construct(AutorizacaoExecucaoEntrega $aeModel)
    {
        $this->aeModel = $aeModel;
    }

    public function assinar(Model $signatarioModel)
    {
        parent::assinar($signatarioModel);

        // notificar preposto por e-mail
        if ((
                get_class($this->aeModel) === AutorizacaoExecucaoEntregaTRD::class &&
                in_array($signatarioModel->funcao->descres, ['GESTOR', 'GESTORSUB'])
            )
            ||
            (
                get_class($this->aeModel) === AutorizacaoExecucaoEntregaTRP::class &&
                in_array($signatarioModel->funcao->descres, ['FSCTEC', 'FSCTECSUB'])
            )
        ) {
            $contratoPrepostoRepository = new ContratoPrepostoRepository(
                $this->aeModel->autorizacao->contrato_id
            );

            $this->notificarAssinatura(
                $contratoPrepostoRepository->getBySituacao(),
                new NotificarAutorizacaoExecucaoEntregaMail($this->aeModel)
            );
        }
    }
}
