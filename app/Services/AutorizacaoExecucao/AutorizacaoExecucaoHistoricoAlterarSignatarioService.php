<?php

namespace App\Services\AutorizacaoExecucao;

use App\Models\AutorizacaoexecucaoHistorico;
use App\Services\AutorizacaoExecucao\AbstractAutorizacaoExecucaoSignatarioService;
use Illuminate\Database\Eloquent\Model;

class AutorizacaoExecucaoHistoricoAlterarSignatarioService extends AbstractAutorizacaoExecucaoSignatarioService
{
    public function __construct(AutorizacaoexecucaoHistorico $aehModel)
    {
        $this->aeModel = $aehModel;
    }

    public function assinar(Model $signatarioModel): void
    {
        parent::assinar($signatarioModel);

        AutorizacaoExecucaoStatusHistoricoService::addStatusAssinarAlteracao(
            $this->aeModel->autorizacaoexecucoes_id
        );
    }

    public function recusarAssinatura(Model $signatarioModel, string $motivoRecusa): void
    {
        parent::recusarAssinatura($signatarioModel, $motivoRecusa);

        AutorizacaoExecucaoStatusHistoricoService::addStatusRecusarAssinaturaAlteracao(
            $this->aeModel->autorizacaoexecucoes_id,
            $motivoRecusa
        );
    }
}
