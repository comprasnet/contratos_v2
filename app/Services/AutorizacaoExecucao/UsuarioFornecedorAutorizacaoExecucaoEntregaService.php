<?php

namespace App\Services\AutorizacaoExecucao;

use App\Http\Traits\ArquivoGenericoTrait;
use App\Http\Traits\Formatador;
use App\Mail\AssinarTRPAutorizacaoExecucaoEntregaMail;
use App\Mail\NotificarAutorizacaoExecucaoEntregaMail;
use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoexecucaoContratoLocalExecucao;
use App\Models\AutorizacaoExecucaoEntrega;
use App\Models\AutorizacaoExecucaoEntregaTRD;
use App\Models\AutorizacaoExecucaoEntregaTRP;
use App\Models\CodigoItem;
use App\Repositories\ArquivoGenerico\ArquivoGenericoRepository;
use App\Repositories\AutorizacaoExecucao\UsuarioFornecedorAutorizacaoExecucaoEntregaRepository;
use App\Repositories\Contrato\ContratoResponsavelRepository;
use App\Services\ModelSignatario\ModelDTO;
use App\Services\ModelSignatario\ModelSignatarioService;
use App\Services\ModelSignatario\SignatarioDTO;
use App\Services\RelatorioPdfService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class UsuarioFornecedorAutorizacaoExecucaoEntregaService
{
    use ArquivoGenericoTrait;
    use Formatador;

    protected $usuarioFornecedorAutorizacaoExecucaoRepository;


    public function __construct(
        UsuarioFornecedorAutorizacaoExecucaoEntregaRepository $usuarioFornecedorAutorizacaoExecucaoRepository
    ) {
        $this->usuarioFornecedorAutorizacaoExecucaoRepository = $usuarioFornecedorAutorizacaoExecucaoRepository;
    }

    public function retornaOrdemServicoService(int $entregaId): ?AutorizacaoExecucao
    {
        return $this->usuarioFornecedorAutorizacaoExecucaoRepository->retornaOrdemServicoRepository($entregaId);
    }

    public function getSequencialService(int $contratoId): int
    {
        $currentSequencial = AutorizacaoExecucaoEntrega::where('contrato_id', $contratoId)
            ->where('ano', Carbon::now()->format('Y'))
            ->orderBy('sequencial', 'desc')
            ->first('sequencial');

        $sequencial = $currentSequencial->sequencial ?? 0;

        return $sequencial + 1;
    }

    public function salvarAnaliseEntrega(array $dadosValidados, int $entregaId)
    {
        $entrega = AutorizacaoExecucaoEntrega::findOrFail($entregaId);

        $this->alterarSituacaoAutorizacaoExecucao($dadosValidados, $entrega);

        $entrega->entregaItens()->delete();
        $arrayItens = $this->retornaItensEntrega($dadosValidados['itens']);
        $entrega->entregaItens()->createMany($arrayItens);

        $this->notificarUsuarioAutorizacaoExecucao($dadosValidados, $entrega);
    }

    public function salvarEntrega(array $dadosValidados, int $contratoId, int $autorizacaoExecucaoId)
    {
        $entrega = AutorizacaoExecucaoEntrega::create([
            'contrato_id' => $contratoId,
            'autorizacaoexecucao_id' => $autorizacaoExecucaoId,
            'situacao_id' => $this->retornaSituacao(
                $dadosValidados['rascunho'],
                $dadosValidados['informar_trp'] ?? false,
                $dadosValidados['informar_trd'] ?? false,
                $dadosValidados['incluir_instrumento_cobranca'] ?? false,
            ),
            'sequencial' => $this->getSequencialService($contratoId),
            'ano' => Carbon::now()->format('Y'),
            'rascunho' => $dadosValidados['rascunho'],
            'informacoes_complementares' => $dadosValidados['informacoes_complementares'],
            'valor_entrega' => $this->retornaValorEntrega($dadosValidados['itens']),
            'mes_ano_referencia' => $dadosValidados['mes_ano_referencia'],
            'data_entrega' => $dadosValidados['data_entrega'] ?? null,
            'data_prazo_trp' => $dadosValidados['data_prazo_trp'] ?? null,
            'data_prazo_trd' => $dadosValidados['data_prazo_trd'] ?? null,
            'user_id_criacao' => $dadosValidados['rascunho'] ? null : backpack_user()->id,
            'incluir_instrumento_cobranca' => $dadosValidados['incluir_instrumento_cobranca'] ?? false,
            'originado_sistema_externo' => $dadosValidados['originado_sistema_externo'] ?? false,
        ]);

        $arrayItens = $this->retornaItensEntrega($dadosValidados['itens']);

        $entrega->entregaItens()->createMany($arrayItens);

        // Salvar os locais de execução
        $this->salvarLocaisExecucaoEntrega($dadosValidados, $entrega);

        $this->manipulaArquivos($dadosValidados, $entrega);

        if ($dadosValidados['rascunho'] == false) {
            $this->notificarFiscaisContrato($entrega);
        }

        return $entrega;
    }

    public function alterarEntrega(array $dadosValidados, int $entregaId, int $autorizacaoExecucaoId = null)
    {
        $entrega = AutorizacaoExecucaoEntrega::findOrFail($entregaId);

        $entrega->update([
            'rascunho' => $dadosValidados['rascunho'],
            'informacoes_complementares' => $dadosValidados['informacoes_complementares'],
            'situacao_id' => $this->retornaSituacao(
                $dadosValidados['rascunho'],
                $dadosValidados['informar_trp'] ?? false,
                $dadosValidados['informar_trd'] ?? false,
                $dadosValidados['incluir_instrumento_cobranca'] ?? false,
            ),
            'valor_entrega' => $this->retornaValorEntrega($dadosValidados['itens']),
            'mes_ano_referencia' => $dadosValidados['mes_ano_referencia'] ?? null,
            'data_entrega' => $dadosValidados['data_entrega'] ?? null,
            'data_prazo_trp' => $dadosValidados['data_prazo_trp'] ?? null,
            'data_prazo_trd' => $dadosValidados['data_prazo_trd'] ?? null,
            'user_id_criacao' => $dadosValidados['rascunho'] ? null : backpack_user()->id,
            'incluir_instrumento_cobranca' => $dadosValidados['incluir_instrumento_cobranca'] ?? false,
        ]);

        $entrega->entregaItens()->delete();

        $arrayItens = $this->retornaItensEntrega($dadosValidados['itens']);
        $entrega->entregaItens()->createMany($arrayItens);

        $this->salvarLocaisExecucaoEntrega($dadosValidados, $entrega);

        $this->manipulaArquivos($dadosValidados, $entrega);

        if ($dadosValidados['rascunho'] == false) {
            $this->notificarFiscaisContrato($entrega);
        }

        return $entrega;
    }

    public function duplicarEntrega(int $entregaId): void
    {
        $entrega = AutorizacaoExecucaoEntrega::findOrFail($entregaId);
        $situacaoEmElaboracaoId = CodigoItem::where('descres', 'ae_entrega_status_9')->first()->id;

        $entregaDuplicada = AutorizacaoExecucaoEntrega::create([
            'autorizacaoexecucao_id' => $entrega->autorizacaoexecucao_id,
            'situacao_id' => $situacaoEmElaboracaoId,
            'sequencial' => $this->getSequencialService($entrega->contrato_id),
            'ano' => Carbon::now()->format('Y'),
            'rascunho' => true,
            'informacoes_complementares' => $entrega->informacoes_complementares,
            'valor_entrega' => $entrega->valor_entrega,
            'mes_ano_referencia' => $entrega->mes_ano_referencia,
        ]);

        $entregaDuplicada->entregaItens()->createMany($entrega->entregaItens->toArray());

        $this->duplicaAnexos($entrega->anexos, $entregaDuplicada);
    }

    public function salvarAnexo(AutorizacaoExecucaoEntrega $entrega, array $anexoData)
    {
        $urlDestino = $this->moveAnexoTemporario($anexoData['url_arquivo_anexo']);
        $entrega->anexos()->create([
            'nome' => $anexoData['nome_arquivo_anexo'],
            'descricao' => $anexoData['descricao_arquivo_anexo'] ?? '',
            'url' => $urlDestino,
            'tipo_id' => CodigoItem::where('descres', 'ae_entrega_arquivo')->first()->id,
            'restrito' => false,
        ]);
    }

    private function moveAnexoTemporario(string $urlArquivoAnexo): string
    {
        $date = Carbon::now();
        $nomeArquivoTemporario = last(explode('/', $urlArquivoAnexo));
        $urlDestino = 'autorizacaoexecucao/entrega/' .
            $date->format('Y') . '/' .
            $date->format('m') . '/' .
            $nomeArquivoTemporario;
        throw_unless(
            Storage::disk('public')->move($urlArquivoAnexo, $urlDestino),
            500,
            'Não foi possivel mover o anexo temporário'
        );
        return $urlDestino;
    }

    public function excluiAnexo(array $anexoData)
    {
        if (empty($anexoData['id'])) {
            $this->deleteAnexoTemporario($anexoData['url_arquivo_anexo']);
        } else {
            $this->excluiArquivo($anexoData['id'], false);
        }
    }

    private function deleteAnexoTemporario(string $urlArquivoAnexo): void
    {
        throw_unless(
            Storage::disk('public')->delete($urlArquivoAnexo),
            500,
            'Não foi possivel deletar o anexo temporário'
        );
    }

    private function duplicaAnexos(Collection $anexos, AutorizacaoExecucaoEntrega $entrega)
    {
        $anexos->each(function ($anexo) use ($entrega) {
            $filename = last(explode('/', $anexo->url));

            $date = Carbon::now();
            $path = 'autorizacaoexecucao/entrega/' .
                $date->format('Y') . '/' .
                $date->format('m') . '/' .
                $entrega->id . '-' . $filename;

            Storage::disk('public')->copy($anexo->url, $path);

            $entrega->anexos()->create([
                'nome' => $anexo->nome,
                'tipo_id' => $anexo->tipo_id,
                'descricao' => $anexo->descricao,
                'url' => $path,
                'restrito' => $anexo->restrito,
            ]);
        });
    }

    public function excluiArquivo(int $id, $idIsArquivoable = true)
    {
        $arquivoGenerico = new ArquivoGenericoRepository();

        if ($idIsArquivoable) {
            $anexo = $arquivoGenerico->getArquivoGenerico($id, AutorizacaoExecucaoEntrega::class);
        } else {
            $anexo = $arquivoGenerico->getArquivoGenericoUnico($id);
        }
        if ($anexo) {
            $this->deletarArquivoFiscamente($arquivoGenerico, $anexo->id);
            $anexo->delete();
        }
    }

    /**
     * @param $rascunho
     * @return Carbon|null
     */
    private function retornaDataEntrega(bool $rascunho): ?Carbon
    {
        return !$rascunho ? Carbon::now() : null;
    }

    private function retornaValorEntrega($itens)
    {
        return array_reduce($itens, function ($current, $item) {
            $current += $item['valor_total_entrega'];
            return $current;
        }, 0);
    }

    /**
     * @param $rascunho
     * @return mixed
     */
    private function retornaSituacao(
        $rascunho,
        $trpInformado = false,
        $trdInformado = false,
        $incluirInstrumentoCobranca = false
    ) {
        $proximaSituacaoFiscalGestor = 'ae_entrega_status_2';
        if ($trdInformado) {
            $proximaSituacaoFiscalGestor = $incluirInstrumentoCobranca ? 'ae_entrega_status_12' : 'ae_entrega_status_7';
        } elseif ($trpInformado) {
            $proximaSituacaoFiscalGestor = 'ae_entrega_status_10';
        }

        $proximaSituacao = session('fornecedor_id') ? 'ae_entrega_status_1' : $proximaSituacaoFiscalGestor;
        $descres = $rascunho ? 'ae_entrega_status_9' : $proximaSituacao;
        $situacaoId = CodigoItem::where('descres', $descres)->first()->id;
        return $situacaoId;
    }

    /**
     * @param $itens
     * @return array|array[]
     */
    public function retornaItensEntrega($itens): array
    {
        return array_map(function ($item) {
            return [
                'autorizacao_execucao_itens_id' => $item['itens_selecionados'],
                'quantidade_informada' => $item['quantidade_informada'],
                'valor_glosa' => $item['valor_glosa'] ?? 0,
                'mes_ano_competencia' => $item['mes_ano_competencia'] ?? null,
                'processo_sei' => $item['processo_sei'] ?? null,
                'data_inicio' => $item['data_inicio'] ?? null,
                'data_fim' => $item['data_fim'] ?? null,
                'horario' => $item['horario'] ?? null,
            ];
        }, $itens);
    }

    /**
     * @param array $dadosValidados
     * @param $entrega
     * @return void
     */
    public function manipulaArquivos(array $dadosValidados, $entrega): void
    {
        if (!empty($dadosValidados['anexos'])) {
            foreach ($dadosValidados['anexos'] as $anexo) {
                if ($anexo['remove_file']) {
                    $this->excluiAnexo($anexo);
                } elseif (empty($anexo['id'])) {
                    $this->salvarAnexo($entrega, $anexo);
                }
            }
        }

        $trpService = new AutorizacaoExecucaoEntregaTRPTRDService(AutorizacaoExecucaoEntregaTRP::find($entrega->id));
        if (!empty($dadosValidados['arquivo_trp'])) {
            $trpService->salvarArquivo($dadosValidados['arquivo_trp']->get());
        } elseif (!empty($dadosValidados['arquivo_trp_clear'])) {
            $trpService->excluirArquivo();
        }

        $trdService = new AutorizacaoExecucaoEntregaTRPTRDService(AutorizacaoExecucaoEntregaTRD::find($entrega->id));
        if (!empty($dadosValidados['arquivo_trd'])) {
            $trdService->salvarArquivo($dadosValidados['arquivo_trd']->get());
        } elseif (!empty($dadosValidados['arquivo_trd_clear'])) {
            $trdService->excluirArquivo();
        }
    }

    public function notificarFiscaisContrato(AutorizacaoExecucaoEntrega $entrega): void
    {
        $contratoResponsavelRepository = new ContratoResponsavelRepository($entrega->getContrato()->id);
        $fiscaisContrato = $contratoResponsavelRepository->getFiscaisContrato();

        $emails = $fiscaisContrato->map(function ($fiscal) {
            return $fiscal->user->email;
        });

        if ($emails->isEmpty()) {
            return;
        }
        Mail::to($emails->toArray())->send(new NotificarAutorizacaoExecucaoEntregaMail($entrega));
    }

    public function alterarSituacaoAutorizacaoExecucao(array $dadosValidados, AutorizacaoExecucaoEntrega $entrega)
    {
        $status = '';
        switch ($dadosValidados['acao']) {
            case 'aprovar':
                $status = 'ae_entrega_status_2';
                break;
            case 'recusar':
                $status = 'ae_entrega_status_3';
                break;
            case 'cancelar':
                $status = 'ae_entrega_status_5';
                break;
        }

        $situacaoId = CodigoItem::where('descres', $status)->first()->id;
        $entrega->update([
            'situacao_id' => $situacaoId,
            'valor_entrega' => $this->retornaValorEntrega($dadosValidados['itens']),
            'justificativa_motivo_analise' => $dadosValidados['justificativa_motivo_analise']
        ]);
    }

    public function notificarUsuarioAutorizacaoExecucao(array $dadosValidados, AutorizacaoExecucaoEntrega $entrega)
    {
        if ($entrega->user_id_criacao && (
                $dadosValidados['acao'] == 'recusar' ||
                $dadosValidados['acao'] == 'cancelar'
            )
        ) {
            Mail::to($entrega->usuarioCriacao->email)->send(
                new NotificarAutorizacaoExecucaoEntregaMail($entrega)
            );
        }
    }

    public function salvarAnaliseEntregaFornecedor(array $dadosValidados)
    {
        $entrega = AutorizacaoExecucaoEntrega::findOrFail($dadosValidados['entregaIdCancelamento']);

        $this->alterarSituacaoAutorizacaoExecucao($dadosValidados, $entrega);

        $this->notificarUsuarioAutorizacaoExecucao($dadosValidados, $entrega);
    }

    public function salvarLocaisExecucaoEntrega(array $dadosValidados, AutorizacaoExecucaoEntrega $entrega)
    {
        // remove todos os locais anteriores dessa entrega
        $queryEntregaLocalExecucao = AutorizacaoexecucaoContratoLocalExecucao::where(
            'autorizacaoexecucao_id',
            $entrega->autorizacaoexecucao_id
        );
        $queryEntregaLocalExecucao->update(['autorizacaoexecucao_entrega_id' => null]);

       /* foreach ($dadosValidados['locais_execucao_entrega'] as $localId) {
            $queryEntregaLocalExecucao->where('contrato_local_execucao_id',
                $localId)->update(['autorizacaoexecucao_entrega_id' => $entrega->id]);
            // dump($localId);
        }*/
        if (!empty($dadosValidados['locais_execucao_entrega'])) {
            $queryEntregaLocalExecucao->whereIn(
                'contrato_local_execucao_id',
                $dadosValidados['locais_execucao_entrega']
            )->update([
                'autorizacaoexecucao_entrega_id' => $entrega->id
            ]);
        }
        // dd($dadosValidados);
    }
}
