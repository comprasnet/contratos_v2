<?php

namespace App\Services\AutorizacaoExecucao;

use App\Models\AutorizacaoExecucaoStatusHistorico;
use App\Models\CodigoItem;

class AutorizacaoExecucaoStatusHistoricoService
{
    public static function addStatusInformar(int $autorizacaoexecucao_id)
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_1')->firstOrFail();

        AutorizacaoExecucaoStatusHistorico::create([
            'autorizacaoexecucao_id' => $autorizacaoexecucao_id,
            'status_id' => $status->id,
            'usuario_id' => backpack_user()->id
        ]);
    }

    public static function addStatusEnviarAssinatura(int $autorizacaoexecucao_id)
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_2')->firstOrFail();

        AutorizacaoExecucaoStatusHistorico::create([
            'autorizacaoexecucao_id' => $autorizacaoexecucao_id,
            'status_id' => $status->id,
            'usuario_id' => backpack_user()->id
        ]);
    }

    public static function addStatusAssinar(int $autorizacaoexecucao_id)
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_3')->firstOrFail();

        AutorizacaoExecucaoStatusHistorico::create([
            'autorizacaoexecucao_id' => $autorizacaoexecucao_id,
            'status_id' => $status->id,
            'usuario_id' => backpack_user()->id
        ]);
    }

    public static function addStatusRecusarAssinatura(int $autorizacaoexecucao_id, string $observacao)
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_4')->firstOrFail();

        AutorizacaoExecucaoStatusHistorico::create([
            'autorizacaoexecucao_id' => $autorizacaoexecucao_id,
            'status_id' => $status->id,
            'usuario_id' => backpack_user()->id,
            'observacao' => $observacao
        ]);
    }

    public static function addStatusEnviarAlteracaoAssinatura(int $autoracaoexecucao_id, string $observacao)
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_8')->firstOrFail();

        AutorizacaoExecucaoStatusHistorico::create([
            'autorizacaoexecucao_id' => $autoracaoexecucao_id,
            'status_id' => $status->id,
            'usuario_id' => backpack_user()->id,
            'observacao' => $observacao
        ]);
    }

    public static function addStatusAssinarAlteracao(int $autorizacaoexecucao_id)
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_9')->firstOrFail();

        AutorizacaoExecucaoStatusHistorico::create([
            'autorizacaoexecucao_id' => $autorizacaoexecucao_id,
            'status_id' => $status->id,
            'usuario_id' => backpack_user()->id
        ]);
    }

    public static function addStatusRecusarAssinaturaAlteracao(int $autorizacaoexecucao_id, string $observacao)
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_10')->firstOrFail();

        AutorizacaoExecucaoStatusHistorico::create([
            'autorizacaoexecucao_id' => $autorizacaoexecucao_id,
            'status_id' => $status->id,
            'usuario_id' => backpack_user()->id,
            'observacao' => $observacao
        ]);
    }

    public static function addStatusAlterar(int $autorizacaoexecucao_id, int $usuario_id = null)
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_5')->firstOrFail();

        AutorizacaoExecucaoStatusHistorico::create([
            'autorizacaoexecucao_id' => $autorizacaoexecucao_id,
            'status_id' => $status->id,
            'usuario_id' => backpack_user()->id ?? $usuario_id
        ]);
    }

    public static function addStatusExtinguir(int $autorizacaoexecucao_id, int $usuario_id = null)
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_6')->firstOrFail();

        AutorizacaoExecucaoStatusHistorico::create([
            'autorizacaoexecucao_id' => $autorizacaoexecucao_id,
            'status_id' => $status->id,
            'usuario_id' => backpack_user()->id ?? $usuario_id
        ]);
    }

    public static function addStatusRetificar(int $autorizacaoexecucao_id, string $observacao = null)
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_7')->firstOrFail();

        AutorizacaoExecucaoStatusHistorico::create([
            'autorizacaoexecucao_id' => $autorizacaoexecucao_id,
            'status_id' => $status->id,
            'usuario_id' => backpack_user()->id,
            'observacao' => $observacao,
        ]);
    }
}
