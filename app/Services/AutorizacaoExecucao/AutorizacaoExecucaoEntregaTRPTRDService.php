<?php

namespace App\Services\AutorizacaoExecucao;

use App\Http\Traits\Formatador;
use App\Mail\NotificarAutorizacaoExecucaoEntregaMail;
use App\Models\AutorizacaoexecucaoContratoLocalExecucao;
use App\Models\AutorizacaoExecucaoEntrega;
use App\Models\AutorizacaoExecucaoEntregaTRD;
use App\Models\AutorizacaoExecucaoEntregaTRP;
use App\Models\CodigoItem;
use App\Services\ModelSignatario\ModelDTO;
use App\Services\ModelSignatario\ModelSignatarioService;
use App\Services\ModelSignatario\SignatarioDTO;
use App\Services\RelatorioPdfService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class AutorizacaoExecucaoEntregaTRPTRDService
{
    use Formatador;

    private $model;

    public function __construct(AutorizacaoExecucaoEntrega $model)
    {
        throw_unless(
            AutorizacaoExecucaoEntregaTRP::class == get_class($model) ||
            AutorizacaoExecucaoEntregaTRD::class == get_class($model),
            500,
            'Modelo informado não é uma instância de AutoracaoExecucaoEntregaTRP ou' .
                'AutoracaoExecucaoEntregaTRD.'
        );

        $this->model = $model;
    }

    public function gerar(array $dadosValidados, int $entregaId)
    {
        $termoRecebimento = $this->model::findOrFail($entregaId);

        $update = $this->getDadosParaAlteracao($dadosValidados);
        $update['situacao_id'] = $this->getSituacaoIdByRascunho($dadosValidados['rascunho']);
        $update['valor_entrega'] = $this->retornaValorEntrega($dadosValidados['itens']);

        $termoRecebimento->update($update);

        $this->salvarLocaisExecucaoEntrega($dadosValidados['locais_execucao_entrega'] ?? [], $termoRecebimento);

        $termoRecebimento->entregaItens()->delete();

        $arrayItens = $this->retornaItensEntrega($dadosValidados['itens']);
        $termoRecebimento->entregaItens()->createMany($arrayItens);

        if (!$dadosValidados['rascunho']) {
            $aeEntregaSignatarioService = new AutorizacaoExecucaoEntregaSignatarioService($termoRecebimento);
            $signatarios = $aeEntregaSignatarioService->converteModalSelectPrepostosResponsaveisParaSignatarios(
                $dadosValidados['modal_select_prepostos_responsaveis']
            );

            $aeEntregaSignatarioService->notificarAssinatura(
                $signatarios,
                new NotificarAutorizacaoExecucaoEntregaMail($termoRecebimento)
            );

            $pathPDFassinatura = $this->generatePDFAssinatura($termoRecebimento, $signatarios);

            $assinaturas = $aeEntregaSignatarioService->getPosicoesAssinaturas($pathPDFassinatura, $signatarios);

            $nomeArquivo = last(explode('/', $pathPDFassinatura));
            $arquivoGenerico = $termoRecebimento
                ->arquivo()
                ->create([
                    'tipo_id' => CodigoItem::where('descres', 'ae_arquivo_assinado')->first()->id,
                    'url' => $pathPDFassinatura,
                    'nome' => $nomeArquivo,
                    'descricao' => '',
                    'restrito' => false,
                    'user_id' => backpack_auth()->user()->id,
                ]);

            $modelSignatarioService = new ModelSignatarioService(new ModelDTO($termoRecebimento));
            $modelSignatarioService->removerTodosSignatarios();

            $signatarios->each(function (
                $signatario,
                $key
            ) use (
                $modelSignatarioService,
                $assinaturas,
                $arquivoGenerico
            ) {
                $assinaturas[$key]['arquivo_generico_id'] = $arquivoGenerico->id;

                $signatarioDTO = new SignatarioDTO($signatario);
                $modelSignatarioService->addSignatario($signatarioDTO, $assinaturas[$key]);
            });
        }
    }

    public function ajustar(int $entregaId)
    {
        $termoRecebimento = $this->model::findOrFail($entregaId);

        $termoRecebimento->update(['situacao_id' => $this->getSituacaoIdByRascunho(true)]);

        Storage::disk('public')->delete($termoRecebimento->arquivo->url);
        $termoRecebimento->arquivo()->delete();

        $modelSignatarioService = new ModelSignatarioService(new ModelDTO($termoRecebimento));
        $modelSignatarioService->removerTodosSignatarios();
    }

    public function salvarArquivo($contents)
    {
        $prefix = 'TRD';
        if (AutorizacaoExecucaoEntregaTRP::class === get_class($this->model)) {
            $prefix = 'TRP';
        }

        $filename =  $prefix . '-' . $this->model->sequencial . '-' . $this->model->ano;
        $path = 'autorizacaoexecucao/entrega/' .
            Carbon::now()->format('Y') . '/' .
            Carbon::now()->format('m') . '/' .
            $filename . '.pdf';

        Storage::disk('public')->put($path, $contents);

        $this->excluirArquivo();
        $this->model->arquivo()->create([
            'tipo_id' => CodigoItem::where('descres', 'ae_arquivo_assinado')->first()->id,
            'url' => $path,
            'nome' => $filename,
            'descricao' => '',
            'restrito' => false,
            'user_id' => backpack_auth()->user()->id,
        ]);
    }

    public function excluirArquivo()
    {
        $this->model->arquivo()->delete();
    }

    private function getSituacaoIdByRascunho(bool $rascunho): int
    {
        $descres = AutorizacaoExecucaoEntregaTRP::class === get_class($this->model) ?
            'ae_entrega_status_4' : 'ae_entrega_status_8';

        if ($rascunho) {
            $descres = AutorizacaoExecucaoEntregaTRP::class === get_class($this->model) ?
                'ae_entrega_status_2' : 'ae_entrega_status_11';
        }

        return CodigoItem::where('descres', $descres)->first()->id;
    }

    private function getDadosParaAlteracao(array $dadosValidados): array
    {
        if (AutorizacaoExecucaoEntregaTRP::class === get_class($this->model)) {
            $result = [
                'introducao_trp' => $dadosValidados['introducao_trp'],
                'informacoes_complementares_trp' => $dadosValidados['informacoes_complementares_trp'],
            ];
        } else {
            $result = [
                'introducao_trd' => $dadosValidados['introducao_trd'],
                'informacoes_complementares_trd' => $dadosValidados['informacoes_complementares_trd'],
                'incluir_instrumento_cobranca' => $dadosValidados['incluir_instrumento_cobranca'],
            ];
        }

        $result['mes_ano_referencia'] = $dadosValidados['mes_ano_referencia'];

        return $result;
    }

    private function retornaValorEntrega($itens)
    {
        return array_reduce($itens, function ($current, $item) {
            $current += $item['valor_total_entrega'];
            return $current;
        }, 0);
    }


    private function retornaItensEntrega($itens): array
    {
        return array_map(function ($item) {
            return [
                'autorizacao_execucao_itens_id' => $item['itens_selecionados'],
                'quantidade_informada' => $item['quantidade_informada'],
                'valor_glosa' => $item['valor_glosa'] ?? 0,
                'mes_ano_competencia' => $item['mes_ano_competencia'] ?? null,
                'processo_sei' => $item['processo_sei'] ?? null,
                'data_inicio' => $item['data_inicio'] ?? null,
                'data_fim' => $item['data_fim'] ?? null,
                'horario' => $item['horario'] ?? null,
            ];
        }, $itens);
    }

    private function generatePDFAssinatura(AutorizacaoExecucaoEntrega $entrega, $signatarios)
    {
        $autorizacaoExecucao = $entrega->autorizacao;
        $contrato = $autorizacaoExecucao->contrato;

        $aeService = new AutorizacaoExecucaoService();
        $aeService->setAutorizacaoExecucao($autorizacaoExecucao);
        $aeService->setContrato($contrato);
        $fields = $aeService->getFieldLabels();

        $valorTotal = 0;

        $acao = 'trd';
        $prefixoTitulo = 'Termo de Recebimento Definitivo';
        if (AutorizacaoExecucaoEntregaTRP::class === get_class($this->model)) {
            $acao = 'trp';
            $prefixoTitulo = 'Termo de Recebimento Provisório';
        }

        $html = view(
            "relatorio.{$acao}-pdf",
            [
                'titulo' => $prefixoTitulo . ' nº ' . $entrega->getFormattedSequencial() .
                    ' Ordem de Serviço / Fornecimento nº ' .
                    $autorizacaoExecucao->numero .
                    ' Contrato nº '. $contrato->numero,
                'unidadeGerenciadora' => 'Unidade Gestora Atual ' .
                    $contrato->unidade->codigosiasg . ' - ' .
                    $contrato->unidade->nome,
                'fields' => $fields,
                'termoRecebimento' => $entrega,
                'anexos' => $entrega->getAnexos(),
                'valorTotal' => $valorTotal,
                'valorPorExtenso' => $this->valorPorExtenso($valorTotal),
                'assinantes' => $signatarios
            ]
        )->render();


        $dompdf = RelatorioPdfService::create();
        $dompdf->loadHtml($html);
        $dompdf->render();
        $font = $dompdf->getFontMetrics()->getFont(
            "helvetica",
            "normal"
        );
        $canvas = $dompdf->getCanvas();

        $canvas->page_text(
            35,
            820,
            "{$fields['contrato']['label']} nº {$fields['contrato']['value']} - " .
            "{$contrato->unidade->codigosiasg}",
            $font,
            10,
        );

        $canvas->page_text(
            565,
            820,
            "{PAGE_NUM}/{PAGE_COUNT}",
            $font,
            10,
        );


        $filename = strtoupper($acao) . '-' . $entrega->sequencial . '-' . $entrega->ano;

        $path = 'autorizacaoexecucao/entrega/' .
            Carbon::now()->format('Y') . '/' .
            Carbon::now()->format('m') . '/' .
            $filename . '.pdf';

        Storage::disk('public')->put($path, $dompdf->output());

        return $path;
    }

    private function salvarLocaisExecucaoEntrega(array $locaisEntrega, AutorizacaoExecucaoEntrega $entrega)
    {
        $queryEntregaLocalExecucao = AutorizacaoexecucaoContratoLocalExecucao::where(
            'autorizacaoexecucao_id',
            $entrega->autorizacaoexecucao_id
        );
        $queryEntregaLocalExecucao->update(['autorizacaoexecucao_entrega_id' => null]);

        if ($locaisEntrega) {
            $queryEntregaLocalExecucao->whereIn(
                'contrato_local_execucao_id',
                $locaisEntrega
            )->update([
                'autorizacaoexecucao_entrega_id' => $entrega->id
            ]);
        }
    }
}
