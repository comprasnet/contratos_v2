<?php

namespace App\Services\AutorizacaoExecucao;

use App\Http\Traits\ArquivoGenericoTrait;
use App\Models\AutorizacaoexecucaoHistorico;
use App\Models\CodigoItem;
use App\Repositories\AutorizacaoExecucao\AutorizacaoExecucaoRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class AutorizacaoExecucaoHistoricoRetificarService extends AbstractAutorizacaoExecucaoHistoricoService
{
    public function getCamposRetificados($array): array
    {
        return array_filter($array, function ($value, $key) {
            if ($key != 'autorizacaoexecucoes_id' &&
                $key != 'justificativa_motivo' &&
                $key != 'unidaderequisitante_ids' &&
                $key != 'autorizacaoexecucaoItens' &&
                $key != 'empenhos' &&
                $key != 'contrato_local_execucao_ids' &&
                $this->autorizacaoExecucao->$key != $value) {
                return $value;
            }

            // compara os arrays de ids
            if ($key === 'unidaderequisitante_ids') {
                $autorizacaoUnidades = $this->autorizacaoExecucao
                    ->autorizacaoexecucaoUnidadeRequisitantes
                    ->pluck('unidade_requisitante_id')
                    ->toArray();


                if (!empty(array_diff($autorizacaoUnidades, $value)) ||
                    !empty(array_diff($value, $autorizacaoUnidades))) {
                    return $value;
                }
            }

            // compara os arrays de ids
            if ($key === 'empenhos') {
                $autorizacaoEmpenhos = $this->autorizacaoExecucao
                    ->autorizacaoexecucaoEmpenhos
                    ->pluck('empenhos_id')
                    ->toArray();

                if (!empty(array_diff($autorizacaoEmpenhos, $value)) ||
                    !empty(array_diff($value, $autorizacaoEmpenhos))) {
                    return $value;
                }
            }

            // compara os arrays de ids
            if ($key === 'contrato_local_execucao_ids') {
                $locaisExecucao = $this->autorizacaoExecucao
                    ->locaisExecucao
                    ->pluck('id')
                    ->toArray();

                if (!empty(array_diff($locaisExecucao, $value)) ||
                    !empty(array_diff($value, $locaisExecucao))) {
                    return $value;
                }
            }

            if ($key === 'autorizacaoexecucaoItens') {
                $autorizacaoItens = $this->autorizacaoExecucao
                    ->autorizacaoexecucaoItens
                    ->makeHidden('contratoitem_id')
                    ->toArray();

                if (count($autorizacaoItens) != count($value)) {
                    return $value;
                }

                $key = 0;
                foreach ($value as $autorizacaoItem) {
                    unset(
                        $autorizacaoItem['contratoitem_id'],
                        $autorizacaoItem['numero_parcelas'],
                        $autorizacaoItem['quantidade_item'],
                        $autorizacaoItem['autorizacaoexecucao_itens_id'],
                        $autorizacaoItem['quantidade_itens_entrega'],
                    );

                    $autorizacaoItens[$key]['quantidade'] = $this->retornaFormatoAmericano(
                        $autorizacaoItens[$key]['quantidade']
                    );
                    $autorizacaoItens[$key]['valor_unitario'] = $this->retornaFormatoAmericano(
                        $autorizacaoItens[$key]['valor_unitario']
                    );
                    $autorizacaoItem['subcontratacao'] = $autorizacaoItem['subcontratacao'] ? true : false;
                    if (!empty(array_diff($autorizacaoItem, $autorizacaoItens[$key]))) {
                        return $value;
                    }
                    $key++;
                }
            }
        }, ARRAY_FILTER_USE_BOTH);
    }

    public function create(array $camposRetificados, string $justificativaMotivo)
    {
        if (!empty($camposRetificados['arquivo'])) {
            $aeRepository = new AutorizacaoExecucaoRepository();
            $urlArquivoAntes = $this->autorizacaoExecucao->arquivo->url;

            $this->autorizacaoExecucao->arquivo()->delete();
            $aeRepository->salvarArquivo($this->autorizacaoExecucao, $camposRetificados['arquivo']);
            $urlArquivoDepois = $this->autorizacaoExecucao->load('arquivo')->arquivo->url;

            unset($camposRetificados['arquivo']);
        }

        $autorizacaoexecucaoHistorico = $this->autorizacaoExecucao
            ->autorizacaoexecucaoHistorico()
            ->create(
                array_merge(
                    $this->dataTransform(
                        $camposRetificados,
                        CodigoItem::where('descres', 'aeh_retificar')->first()
                    ),
                    ['justificativa_motivo' => $justificativaMotivo],
                    ['url_arquivo_antes' => $urlArquivoAntes ?? null],
                    ['url_arquivo_depois' => $urlArquivoDepois ?? null],
                )
            );

        $situacaoVigenciaExpirada = CodigoItem::where('descres', 'ae_status_8')->first();
        if (!empty($camposRetificados['data_vigencia_fim']) &&
            $this->autorizacaoExecucao->situacao_id == $situacaoVigenciaExpirada->id &&
            $camposRetificados['data_vigencia_fim'] > Carbon::now()
        ) {
            $situacaoEmExecucao = CodigoItem::where('descres', 'ae_status_2')->first();

            $camposRetificados['situacao_id'] = $situacaoEmExecucao->id;
        }

        $this->autorizacaoExecucao->update($camposRetificados);

        if (!empty($camposRetificados['unidaderequisitante_ids'])) {
            $this->createHistoricosUnidadeRequisitante(
                $autorizacaoexecucaoHistorico,
                $camposRetificados['unidaderequisitante_ids']
            );

            (new AutorizacaoExecucaoUnidadeRequisitantesService())->recreateUnidadeRequisitantes(
                $this->autorizacaoExecucao,
                $camposRetificados['unidaderequisitante_ids']
            );
        }

        if (!empty($camposRetificados['empenhos'])) {
            $this->createHistoricosEmpenhos(
                $autorizacaoexecucaoHistorico,
                $camposRetificados['empenhos']
            );

            (new AutorizacaoExecucaoEmpenhosService())->recreateEmpenhos(
                $this->autorizacaoExecucao,
                $camposRetificados['empenhos']
            );
        }

        if (!empty($camposRetificados['contrato_local_execucao_ids'])) {
            $this->createHistoricoLocaisExecucao(
                $autorizacaoexecucaoHistorico,
                $camposRetificados['contrato_local_execucao_ids']
            );

            $this->autorizacaoExecucao->locaisExecucao()->sync($camposRetificados['contrato_local_execucao_ids']);
        }

        if (!empty($camposRetificados['autorizacaoexecucaoItens'])) {
            $this->recreateHistoricosItens(
                $autorizacaoexecucaoHistorico,
                $camposRetificados['autorizacaoexecucaoItens']
            );

            (new AutorizacaoExecucaoItensService())->recreateItens(
                $this->autorizacaoExecucao,
                $camposRetificados['autorizacaoexecucaoItens']
            );
        }

        AutorizacaoExecucaoStatusHistoricoService::addStatusRetificar(
            $this->autorizacaoExecucao->id,
            $justificativaMotivo
        );
    }

    private function createHistoricosUnidadeRequisitante(
        AutorizacaoexecucaoHistorico $autorizacaoexecucaoHistorico,
        $unidaderequisitante_ids
    ): void {
        $this->attachIdsAntesDepois(
            $autorizacaoexecucaoHistorico->autorizacaoexecucaoUnidadeRequisitantesHistorico(),
            $this->autorizacaoExecucao
                ->autorizacaoexecucaoUnidadeRequisitantes
                ->pluck('unidade_requisitante_id')
                ->toArray(),
            $unidaderequisitante_ids
        );
    }

    private function createHistoricosEmpenhos(
        AutorizacaoexecucaoHistorico $autorizacaoexecucaoHistorico,
        $empenhos
    ): void {
        $this->attachIdsAntesDepois(
            $autorizacaoexecucaoHistorico->autorizacaoexecucaoEmpenhosHistorico(),
            $this->autorizacaoExecucao->autorizacaoexecucaoEmpenhos->pluck('empenhos_id')->toArray(),
            $empenhos
        );
    }

    private function createHistoricoLocaisExecucao(
        AutorizacaoexecucaoHistorico $autorizacaoexecucaoHistorico,
        $contrato_local_execucao_ids
    ): void {
        $this->attachIdsAntesDepois(
            $autorizacaoexecucaoHistorico->autorizacaoexecucaoLocaisExecucaoHistorico(),
            $this->autorizacaoExecucao->locaisExecucao->pluck('id')->toArray(),
            $contrato_local_execucao_ids
        );
    }

    private function attachIdsAntesDepois(
        BelongsToMany $belongsToMany,
        array $idsAntes,
        array $idsDepois
    ) {
        foreach ($idsAntes as $id) {
            $belongsToMany->attach($id, ['tipo_historico' => 'antes']);
        }

        foreach ($idsDepois as $id) {
            $belongsToMany->attach($id, ['tipo_historico' => 'depois']);
        }
    }
}
