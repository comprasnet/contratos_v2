<?php

namespace App\Services\AutorizacaoExecucao;

use App\Http\Traits\ArquivoGenericoTrait;
use App\Http\Traits\Formatador;
use App\Mail\AssinarAlteracaoAutorizacaoExecucaoMail;
use App\Models\AutorizacaoexecucaoHistorico;
use App\Models\CodigoItem;
use App\Notifications\NotificationFornecedor;
use App\Services\ModelSignatario\ModelDTO;
use App\Services\ModelSignatario\ModelSignatarioService;
use App\Services\ModelSignatario\SignatarioDTO;
use App\Services\RelatorioPdfService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class AutorizacaoExecucaoHistoricoAlterarService extends AbstractAutorizacaoExecucaoHistoricoService
{
    use Formatador;
    use ArquivoGenericoTrait;

    public function create(array $campos)
    {
        $tipoHistorico = CodigoItem::where('descres', 'aeh_alterar')->first();

        $autorizacaoexecucaoHistorico = $this->autorizacaoExecucao
            ->autorizacaoexecucaoHistorico()
            ->create(
                array_merge(
                    ['situacao_id' => $this->getSituacaoId($campos['rascunho'])],
                    $this->dataTransform($campos, $tipoHistorico),
                )
            );

        if (!empty($campos['autorizacaoexecucaoItens'])) {
            $this->recreateHistoricosItens(
                $autorizacaoexecucaoHistorico,
                $campos['autorizacaoexecucaoItens']
            );
        }

        if ($campos['rascunho'] == 0) {
            AutorizacaoExecucaoStatusHistoricoService::addStatusEnviarAlteracaoAssinatura(
                $this->autorizacaoExecucao->id,
                $campos['justificativa_motivo']
            );

            $aeSignatarioService = new AutorizacaoExecucaoHistoricoAlterarSignatarioService(
                $autorizacaoexecucaoHistorico
            );

            $signtarios = $aeSignatarioService->converteModalSelectPrepostosResponsaveisParaSignatarios(
                $campos['modal_select_prepostos_responsaveis']
            );

            $pathPDFassinatura = $this->generatePDFAssinatura($autorizacaoexecucaoHistorico, $signtarios);

            $assinaturas = $aeSignatarioService->getPosicoesAssinaturas($pathPDFassinatura, $signtarios);

            $nomeArquivo = last(explode('/', $pathPDFassinatura));
            $arquivoGenerico = $autorizacaoexecucaoHistorico
                ->arquivo()
                ->create([
                    'tipo_id' => CodigoItem::where('descres', 'ae_arquivo_assinado')->first()->id,
                    'url' => $pathPDFassinatura,
                    'nome' => $nomeArquivo,
                    'descricao' => '',
                    'restrito' => false,
                    'user_id' => backpack_auth()->user()->id,
                ]);

            $aeSignatarioService->notificarAssinatura(
                $signtarios,
                new AssinarAlteracaoAutorizacaoExecucaoMail($autorizacaoexecucaoHistorico)
            );

            $modelSignatarioService = new ModelSignatarioService(new ModelDTO($autorizacaoexecucaoHistorico));
            $modelSignatarioService->removerTodosSignatarios();

            $signtarios->each(function (
                $signatario,
                $key
            ) use (
                $modelSignatarioService,
                $assinaturas,
                $arquivoGenerico,
                $autorizacaoexecucaoHistorico
            ) {
                $assinaturas[$key]['arquivo_generico_id'] = $arquivoGenerico->id;

                $signatarioDTO = new SignatarioDTO($signatario);
                $modelSignatarioService->addSignatario($signatarioDTO, $assinaturas[$key]);

                // Notificar somente os prepostos do contrato
                if ($signatarioDTO->fkColumn === 'signatario_contratopreposto_id' && !empty($signatario->userCpf)) {
                    Notification::send($signatario->userCpf, new NotificationFornecedor($autorizacaoexecucaoHistorico));
                }
            });
        }
    }

    public function update(array $campos, int $idHistorico)
    {
        $autorizacaoexecucaoHistorico = AutorizacaoexecucaoHistorico::findOrFail($idHistorico);

        $autorizacaoexecucaoHistorico->update(
            array_merge(
                [
                    'situacao_id' => $this->getSituacaoId(
                        $campos['rascunho'],
                        $autorizacaoexecucaoHistorico->situacao_id
                    )
                ],
                $this->dataTransform(
                    $campos,
                    $autorizacaoexecucaoHistorico->tipoHistorico,
                    $campos['rascunho'] == 0
                ),
            )
        );

        if (!empty($campos['autorizacaoexecucaoItens'])) {
            $this->recreateHistoricosItens(
                $autorizacaoexecucaoHistorico,
                $campos['autorizacaoexecucaoItens']
            );
        }

        if ($campos['rascunho'] == 0) {
            AutorizacaoExecucaoStatusHistoricoService::addStatusEnviarAlteracaoAssinatura(
                $this->autorizacaoExecucao->id,
                $campos['justificativa_motivo']
            );

            $aeSignatarioService = new AutorizacaoExecucaoHistoricoAlterarSignatarioService(
                $autorizacaoexecucaoHistorico
            );

            $signtarios = $aeSignatarioService->converteModalSelectPrepostosResponsaveisParaSignatarios(
                $campos['modal_select_prepostos_responsaveis']
            );

            $pathPDFassinatura = $this->generatePDFAssinatura($autorizacaoexecucaoHistorico, $signtarios);

            $assinaturas = $aeSignatarioService->getPosicoesAssinaturas($pathPDFassinatura, $signtarios);

            $nomeArquivo = last(explode('/', $pathPDFassinatura));
            $arquivoGenerico = $autorizacaoexecucaoHistorico
                ->arquivo()
                ->create([
                    'tipo_id' => CodigoItem::where('descres', 'ae_arquivo_assinado')->first()->id,
                    'url' => $pathPDFassinatura,
                    'nome' => $nomeArquivo,
                    'descricao' => '',
                    'restrito' => false,
                    'user_id' => backpack_auth()->user()->id,
                ]);

            $aeSignatarioService->notificarAssinatura(
                $signtarios,
                new AssinarAlteracaoAutorizacaoExecucaoMail($autorizacaoexecucaoHistorico)
            );

            $modelSignatarioService = new ModelSignatarioService(new ModelDTO($autorizacaoexecucaoHistorico));
            $modelSignatarioService->removerTodosSignatarios();

            $signtarios->each(function (
                $signatario,
                $key
            ) use (
                $modelSignatarioService,
                $assinaturas,
                $arquivoGenerico,
                $autorizacaoexecucaoHistorico
            ) {
                $assinaturas[$key]['arquivo_generico_id'] = $arquivoGenerico->id;

                $signatarioDTO = new SignatarioDTO($signatario);
                $modelSignatarioService->addSignatario($signatarioDTO, $assinaturas[$key]);

                // Notificar somente os prepostos do contrato
                if ($signatarioDTO->fkColumn === 'signatario_contratopreposto_id' && !empty($signatario->userCpf)) {
                    Notification::send($signatario->userCpf, new NotificationFornecedor($autorizacaoexecucaoHistorico));
                }
            });
        }
    }

    public function generatePDFAssinatura(AutorizacaoexecucaoHistorico $autorizacaoexecucaoHistorico, $signtarios)
    {
        $contrato = $this->autorizacaoExecucao->contrato;

        $aeService = new AutorizacaoExecucaoService();
        $aeService->setAutorizacaoExecucao($this->autorizacaoExecucao);
        $aeService->setContrato($contrato);
        $fields = $aeService->getFieldLabels();

        $valorTotal = 0;
        $subcontratacaoItens = [];
        $hiddenColumnsItens = ['especificacao', 'subcontratacao', 'numero_demanda_sistema_externo', 'horario_execucao'];
        if ($autorizacaoexecucaoHistorico->verificaTipoAlteracao('itens')) {
            $fields['autorizacaoexecucaoItens']['value'] = $autorizacaoexecucaoHistorico
                ->autorizacaoexecucaoItens()->where('tipo_historico', 'depois')
                ->get();


            foreach ($fields['autorizacaoexecucaoItens']['value'] as $item) {
                $valorTotal += (float) $item->getTotalSemFormatar();
                if ($item->especificacao != null) {
                    $hiddenColumnsItens = array_diff($hiddenColumnsItens, ['especificacao']);
                }
                if ($item->numero_demanda_sistema_externo != null) {
                    $hiddenColumnsItens = array_diff($hiddenColumnsItens, ['numero_demanda_sistema_externo']);
                }
                if ($item->horario != null || $item->horario_fim != null) {
                    $hiddenColumnsItens = array_diff($hiddenColumnsItens, ['horario_execucao']);
                }
                if ($item->subcontratacao) {
                    $subcontratacaoItens[] = $item->numero_item_compra . ' - ' . $item->item;
                }
            }
        }

        $html = view(
            'relatorio.autorizacao-execucao-alterar-os-pdf',
            [
                'titulo' => 'Ordem de Serviço / Fornecimento nº ' .
                    $this->autorizacaoExecucao->numero .
                    ' Contrato nº '. $contrato->numero,
                'unidadeGerenciadora' => 'Unidade Gestora Atual ' .
                    $contrato->unidade->codigosiasg . ' - ' .
                    $contrato->unidade->nome,
                'autorizacaoexecucaoHistorico' => $autorizacaoexecucaoHistorico,
                'historicoAlteracoes' => $this->autorizacaoExecucao->getHistoricoAlteracoes(),
                'fields' => $fields,
                'valorTotal' => $valorTotal,
                'valorPorExtenso' => $this->valorPorExtenso($valorTotal),
                'hiddenColumnsItens' => $hiddenColumnsItens,
                'subcontratacaoItens' => $subcontratacaoItens,
                'assinantes' => $signtarios
            ]
        )->render();


        $dompdf = RelatorioPdfService::create();
        $dompdf->loadHtml($html);
        $dompdf->render();
        $font = $dompdf->getFontMetrics()->getFont(
            "helvetica",
            "normal"
        );
        $canvas = $dompdf->getCanvas();

        $canvas->page_text(
            35,
            820,
            "{$fields['contrato']['label']} nº {$fields['contrato']['value']} - " .
            "{$contrato->unidade->codigosiasg}",
            $font,
            10,
        );

        $canvas->page_text(
            565,
            820,
            "{PAGE_NUM}/{PAGE_COUNT}",
            $font,
            10,
        );

        $codigoSiasgUnidadeOrigem = $contrato->unidadeorigem->codigosiasg;
        $numeroContrato = str_replace('/', '', $contrato->numero);
        $numeroAE = explode('/', $this->autorizacaoExecucao->numero);
        $filename = 'ALTERACAO-OS-F' . $codigoSiasgUnidadeOrigem . $numeroContrato . $numeroAE[0] ?? '' .
            $numeroAE[1] ?? '';

        $path = 'autorizacaoexecucao/' .
            Carbon::now()->format('Y') . '/' .
            Carbon::now()->format('m') . '/' .
            $filename . '.pdf';

        Storage::disk('public')->put($path, $dompdf->output());

        return $path;
    }

    public function getCamposAlterados(array $campos): array
    {
        $result = [];
        if ($campos['tipo_alteracao']['extingir'] == 1) {
            $result['situacao_id'] = CodigoItem::where('descres', 'ae_status_3')->first()->id;
        }

        if (!empty($campos['data_vigencia_fim'])) {
            $result['data_vigencia_fim'] = $campos['data_vigencia_fim'];
        }

        return $result;
    }

    public function verifyAlteracoesAguardandoAssinaturaOuAssinada(): bool
    {
        return AutorizacaoexecucaoHistorico::where('autorizacaoexecucoes_id', $this->autorizacaoExecucao->id)
            ->where('tipo_historico_id', CodigoItem::where('descres', 'aeh_alterar')->first()->id)
            ->whereIn('situacao_id', CodigoItem::whereIn('descres', ['ae_status_5', 'ae_status_7'])->get()->pluck('id'))
            ->exists();
    }

    public function getSaldoAlteradoDoSaldoHistoricoItensIds(int $contratoId, int $autorizacaoexecucaoId = null)
    {
        $situacaoIds = CodigoItem::whereIn('descres', ['ae_status_5', 'ae_status_7'])->pluck('id');
        $autorizacaoexecucaoHistoricos = AutorizacaoexecucaoHistorico::select(
            'aeih.saldohistoricoitens_id',
            'aeih.tipo_historico'
        )
            ->selectRaw('sum(aeih.quantidade * aeih.parcela) as quantidade')
            ->join(
                'autorizacaoexecucao_itens_historico as aeih',
                'aeih.autorizacaoexecucao_historico_id',
                '=',
                'autorizacaoexecucao_historico.id'
            )
            ->join(
                'autorizacaoexecucoes as ae',
                function ($join) use ($contratoId, $autorizacaoexecucaoId) {
                    $join->on('ae.id', '=', 'autorizacaoexecucao_historico.autorizacaoexecucoes_id')
                        ->where('ae.id', '<>', $autorizacaoexecucaoId)
                        ->where('ae.contrato_id', '=', $contratoId);
                }
            )
            ->whereIn('autorizacaoexecucao_historico.situacao_id', $situacaoIds)
            ->where('tipo_alteracao', 'like', '%itens%')
            ->groupBy('aeih.saldohistoricoitens_id', 'aeih.tipo_historico')
            ->orderBy('aeih.saldohistoricoitens_id')
            ->get();

        $saldoHistoricoItensAlterados = [];
        // agrupa por saldohistoricoitens_id
        $autorizacaoexecucaoHistoricos->each(function ($historico) use (&$saldoHistoricoItensAlterados) {
            if (!isset($saldoHistoricoItensAlterados[$historico->saldohistoricoitens_id])) {
                $saldoHistoricoItensAlterados[$historico->saldohistoricoitens_id] = ["antes" => 0, "depois" => 0];
            }
            $saldoHistoricoItensAlterados[$historico->saldohistoricoitens_id][$historico->tipo_historico] =
                floatval($historico->quantidade);
        });

        $result = [];
        // calcula a diferença
        foreach ($saldoHistoricoItensAlterados as $saldohistoricoitens_id => $saldos) {
            $result[] = [
                'saldohistoricoitens_id' => $saldohistoricoitens_id,
                'quantidade_total' => $saldos['depois'] - $saldos['antes']
            ];
        }

        // retira os itens que não foram alterados, ou seja, diferença entre depois e antes = 0
        return array_filter($result, function ($item) {
            return $item['quantidade_total'] != 0;
        });
    }

    public function alteraSituacaoSeAtingirInicioAlteracao()
    {
        $situacaoAssinada = CodigoItem::where('descres', 'ae_status_7')->first();
        $situacaoEmExecucao = CodigoItem::where('descres', 'ae_status_2')->first();

        $aeAlterarHistoricoBuilder = AutorizacaoexecucaoHistorico::where('situacao_id', $situacaoAssinada->id)
            ->where('data_inicio_alteracao', '<=', Carbon::now())
            ->where('tipo_historico_id', CodigoItem::where('descres', 'aeh_alterar')->first()->id);

        $this->aplicarAlteracoesNaAutorizacaoExecucao($aeAlterarHistoricoBuilder);

        $aeAlterarHistoricoBuilder->update(['situacao_id' => $situacaoEmExecucao->id]);

        $situacaoAguardandoAssinatura = CodigoItem::where('descres', 'ae_status_5')->first();
        $situacaoAssinaturaRecusada = CodigoItem::where('descres', 'ae_status_6')->first();

        AutorizacaoexecucaoHistorico::where('situacao_id', $situacaoAguardandoAssinatura->id)
            ->whereHas('signatarios', function ($query) {
                $query->where('status_assinatura_id', CodigoItem::where('descricao', 'Assinado')->first()->id);
            })
            ->where('data_inicio_alteracao', '<=', Carbon::now())
            ->update(['situacao_id' => $situacaoAssinaturaRecusada->id]);


        $situacaoEmElaboracao = CodigoItem::where('descres', 'ae_status_1')->first();

        AutorizacaoexecucaoHistorico::where('situacao_id', $situacaoAguardandoAssinatura->id)
            ->where('data_inicio_alteracao', '<=', Carbon::now())
            ->update(['situacao_id' => $situacaoEmElaboracao->id]);
    }

    private function aplicarAlteracoesNaAutorizacaoExecucao(Builder $aeAlterarHistoricoBuilder): void
    {
        $aeAlteracoesHistorico = $aeAlterarHistoricoBuilder->with([
            'autorizacaoexecucao', 'autorizacaoexecucaoItens' => function ($query) {
                $query->where('tipo_historico', 'depois');
            }
        ])->get();

        $aeAlteracoesHistorico->each(function ($aeAlteracaoHistorico) {
            if (strpos($aeAlteracaoHistorico->tipo_alteracao, 'extingir') !== false) {
                $aeAlteracaoHistorico->autorizacaoexecucao->update([
                    'situacao_id' => CodigoItem::where('descres', 'ae_status_3')->first()->id,
                ]);

                AutorizacaoExecucaoStatusHistoricoService::addStatusExtinguir(
                    $aeAlteracaoHistorico->autorizacaoexecucoes_id,
                    $aeAlteracaoHistorico->usuario_responsavel_id
                );
            } else {
                if (strpos($aeAlteracaoHistorico->tipo_alteracao, 'itens') !== false) {
                    $itens = [];
                    $aeAlteracaoHistorico->autorizacaoexecucaoItens->each(function ($aeItem) use (&$itens) {
                        $itens[] = $aeItem->getAttributes();
                    });
                    (new AutorizacaoExecucaoItensService())->recreateItens(
                        $aeAlteracaoHistorico->autorizacaoExecucao,
                        $itens
                    );
                }
                if (strpos($aeAlteracaoHistorico->tipo_alteracao, 'vigencia') !== false) {
                    $camposAlterados = [
                        'data_vigencia_fim' => $aeAlteracaoHistorico->getAttributes()['data_vigencia_fim_depois']
                    ];
                    $situacaoVigenciaExpirada = CodigoItem::where('descres', 'ae_status_8')->first();
                    if ($aeAlteracaoHistorico->autorizacaoexecucao->situacao_id == $situacaoVigenciaExpirada->id &&
                        $aeAlteracaoHistorico->getAttributes()['data_vigencia_fim_depois'] > Carbon::now()
                    ) {
                        $situacaoEmExecucao = CodigoItem::where('descres', 'ae_status_2')->first();
                        $camposAlterados['situacao_id'] = $situacaoEmExecucao->id;
                    }
                    $aeAlteracaoHistorico->autorizacaoexecucao->update($camposAlterados);
                }

                AutorizacaoExecucaoStatusHistoricoService::addStatusAlterar(
                    $aeAlteracaoHistorico->autorizacaoexecucoes_id,
                    $aeAlteracaoHistorico->usuario_responsavel_id
                );
            }
        });
    }

    private function getSituacaoId(bool $rascunho = false, int $situacaoAtualId = null): int
    {
        $situacaoEmElaboracao = CodigoItem::where('descres', 'ae_status_1')->first();

        if ($rascunho) {
            if ($situacaoAtualId === null || $situacaoAtualId === $situacaoEmElaboracao->id) {
                return CodigoItem::where('descres', 'ae_status_1')->first()->id;
            } else {
                return $situacaoAtualId;
            }
        }

        return CodigoItem::where('descres', 'ae_status_5')->first()->id;
    }
}
