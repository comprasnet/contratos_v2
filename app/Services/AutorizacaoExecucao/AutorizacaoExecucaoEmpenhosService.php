<?php

namespace App\Services\AutorizacaoExecucao;

use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoexecucaoEmpenhos;

class AutorizacaoExecucaoEmpenhosService
{
    public function recreateEmpenhos(AutorizacaoExecucao $autorizacaoExecucao, array $empenhos)
    {
        $autorizacaoExecucao->autorizacaoexecucaoEmpenhos()->delete();

        foreach ($empenhos as $empenho) {
            AutorizacaoexecucaoEmpenhos::create([
                'autorizacaoexecucoes_id' => $autorizacaoExecucao->id,
                'empenhos_id' => $empenho,
            ]);
        }
    }
}
