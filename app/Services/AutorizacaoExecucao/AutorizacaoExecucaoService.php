<?php

namespace App\Services\AutorizacaoExecucao;

use App\Http\Traits\Formatador;
use App\Mail\AssinarAutorizacaoExecucaoMail;
use App\Models\AutorizacaoExecucao;
use App\Models\BackpackUser;
use App\Models\CodigoItem;
use App\Models\Contrato;
use App\Notifications\NotificationFornecedor;
use App\Repositories\AutorizacaoExecucao\AutorizacaoExecucaoRepository;
use App\Repositories\Contrato\ContratoPrepostoRepository;
use App\Repositories\Contrato\ContratoResponsavelRepository;
use App\Services\ModelSignatario\ModelDTO;
use App\Services\ModelSignatario\ModelSignatarioService;
use App\Services\ModelSignatario\SignatarioDTO;
use App\Services\RelatorioPdfService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use PDFMerger\PDFMerger;
use Smalot\PdfParser\Parser;
use Illuminate\Support\Facades\Notification;

class AutorizacaoExecucaoService
{
    use Formatador;

    /**
     * @var AutorizacaoExecucao
     */
    private $autorizacaoexecucao;
    /**
     * @var Contrato|mixed
     */
    private $contrato;

    /**
     * @param  AutorizacaoExecucao  $autorizacaoexecucao
     */
    public function setAutorizacaoexecucao(AutorizacaoExecucao $autorizacaoexecucao): void
    {
        $this->autorizacaoexecucao = $autorizacaoexecucao;
    }

    /**
     * @param mixed $contrato
     */
    public function setContrato(Contrato $contrato): void
    {
        $this->contrato = $contrato;
    }
    /**
     * @return AutorizacaoExecucao
     */
    public function getAutorizacaoexecucao(): AutorizacaoExecucao
    {
        return $this->autorizacaoexecucao;
    }

    /**
     * @return Contrato|mixed
     */
    public function getContrato(): Contrato
    {
        return $this->contrato;
    }

    public function upsert(array $data)
    {
        $autorizacaoExecucaoRepository = new AutorizacaoExecucaoRepository();

        if (!empty($this->autorizacaoexecucao)) {
            $data['originado_sistema_externo'] = $this->autorizacaoexecucao->originado_sistema_externo;
            abort_unless($autorizacaoExecucaoRepository->update($this->autorizacaoexecucao, $data), 500);
        } else {
            $this->autorizacaoexecucao = $autorizacaoExecucaoRepository->create($this->contrato, $data);
        }

        if (!$data['rascunho']) {
            if ($data['originado_sistema_externo']) {
                AutorizacaoExecucaoStatusHistoricoService::addStatusInformar($this->autorizacaoexecucao->id);
            } else {
                AutorizacaoExecucaoStatusHistoricoService::addStatusEnviarAssinatura($this->autorizacaoexecucao->id);
            }
        }

        (new AutorizacaoExecucaoItensService())->recreateItens(
            $this->autorizacaoexecucao,
            $data['autorizacaoexecucaoItens'] ?? []
        );

        if (isset($data['arquivo_clear']) && $data['arquivo_clear']) {
            $autorizacaoExecucaoRepository->excluiArquivo($this->autorizacaoexecucao->id);
        }

        if (!empty($data['arquivo'])) {
            $autorizacaoExecucaoRepository->excluiArquivo($this->autorizacaoexecucao->id);

            $autorizacaoExecucaoRepository->salvarArquivo($this->autorizacaoexecucao, $data['arquivo']);
        }

        if (!empty($data['anexos'])) {
            foreach ($data['anexos'] as $anexo) {
                if ($anexo['remove_file']) {
                    $autorizacaoExecucaoRepository->excluiAnexo($anexo);
                } elseif (empty($anexo['id'])) {
                    $autorizacaoExecucaoRepository->salvarAnexo($this->autorizacaoexecucao, $anexo);
                }
            }
        }

        (new AutorizacaoExecucaoEmpenhosService())->recreateEmpenhos(
            $this->autorizacaoexecucao,
            $data['empenhos'] ?? []
        );

        (new AutorizacaoExecucaoUnidadeRequisitantesService())->recreateUnidadeRequisitantes(
            $this->autorizacaoexecucao,
            $data['unidaderequisitante_ids'] ?? []
        );

        $this->autorizacaoexecucao->locaisExecucao()->sync($data['contrato_local_execucao_ids']);

        if (!empty($data['enviar_assinatura'])) {
            $aeSignatarioService = new AutorizacaoExecucaoSignatarioService($this->autorizacaoexecucao);
            $signtarios = $aeSignatarioService->converteModalSelectPrepostosResponsaveisParaSignatarios(
                $data['modal_select_prepostos_responsaveis']
            );

            $aeSignatarioService->notificarAssinatura(
                $signtarios,
                new AssinarAutorizacaoExecucaoMail($this)
            );

            $pathPDFassinatura = $this->generatePDFAssinatura($data['modal_select_prepostos_responsaveis'], true);

            $assinaturas = $aeSignatarioService->getPosicoesAssinaturas($pathPDFassinatura, $signtarios);

            $anexos = $this->autorizacaoexecucao->anexos()
                ->where('tipo_id', CodigoItem::where('descres', 'ae_arquivo_anexo')->first()->id)
                ->get();

            $pdfMerger = new PDFMerger();
            $pdfMerger->addPDF(Storage::disk('public')->path($pathPDFassinatura));

            foreach ($anexos as $anexo) {
                $pdfMerger->addPDF(Storage::disk('public')->path($anexo->url));
            }

            Storage::disk('public')->put($pathPDFassinatura, $pdfMerger->merge('string'));

            $nomeArquivo = last(explode('/', $pathPDFassinatura));
            $arquivoGenerico = $this->autorizacaoexecucao
                ->arquivo()
                ->create([
                    'tipo_id' => CodigoItem::where('descres', 'ae_arquivo_assinado')->first()->id,
                    'url' => $pathPDFassinatura,
                    'nome' => $nomeArquivo,
                    'descricao' => '',
                    'restrito' => false,
                    'user_id' => backpack_auth()->user()->id,
                ]);

            $modelSignatarioService = new ModelSignatarioService(new ModelDTO($this->autorizacaoexecucao));
            $modelSignatarioService->removerTodosSignatarios();

            $signtarios->each(function (
                $signatario,
                $key
            ) use (
                $modelSignatarioService,
                $assinaturas,
                $arquivoGenerico
            ) {
                $assinaturas[$key]['arquivo_generico_id'] = $arquivoGenerico->id;

                $signatarioDTO = new SignatarioDTO($signatario);
                $modelSignatarioService->addSignatario($signatarioDTO, $assinaturas[$key]);

                // Notificar somente os prepostos do contrato
                if ($signatarioDTO->fkColumn === 'signatario_contratopreposto_id' && !empty($signatario->userCpf)) {
                    Notification::send($signatario->userCpf, new NotificationFornecedor($this->autorizacaoexecucao));
                }
            });
        }

        return $this->autorizacaoexecucao;
    }

    public function alteraSituacaoSeAtingirInicioVigencia()
    {
        $situacoes = CodigoItem::whereIn('descres', ['ae_status_1', 'ae_status_5', 'ae_status_6'])
            ->orderBy('descres')
            ->get();

        $situacaoAguardandoAssinatura = $situacoes[1];

        $codigoItensStatusAssinatura = CodigoItem::where('descres', 'status_assinatura')->get();
        $assinado = $codigoItensStatusAssinatura->where('descricao', 'Assinado')->first();


        $situacaoAssinaturaRecusada = $situacoes[2];
        AutorizacaoExecucao::where('situacao_id', $situacaoAguardandoAssinatura->id)
            ->where('data_vigencia_inicio', '<=', Carbon::now())
            ->whereHas('signatarios', function ($query) use ($assinado) {
                $query->where('status_assinatura_id', $assinado->id);
            })
            ->update(['situacao_id' => $situacaoAssinaturaRecusada->id]);

        $situacaoEmElaboracao = $situacoes[0];
        AutorizacaoExecucao::where('situacao_id', $situacaoAguardandoAssinatura->id)
            ->where('data_vigencia_inicio', '<=', Carbon::now())
            ->update(['situacao_id' => $situacaoEmElaboracao->id]);
    }

    public function alteraSituacaoSeAtingirVigenciaFim()
    {
        $situacaoEmExecucao = CodigoItem::where('descres', 'ae_status_2')->first();
        $situacaoVigenciaExpirada = CodigoItem::where('descres', 'ae_status_8')->first();

        AutorizacaoExecucao::where('situacao_id', $situacaoEmExecucao->id)
            ->where('data_vigencia_fim', '<=', Carbon::now())
            ->update(['situacao_id' => $situacaoVigenciaExpirada->id]);
    }

    public function alteraSituacaoSeAtingirSaldoSolicitadoAposTRD()
    {
        $situacaoConcluido = CodigoItem::where('descres', 'ae_status_4')->first();

        $qtdTotalSolicitada = $this->autorizacaoexecucao->getQuantidadeTotalSemFormatacao();
        $qtdQtdTotalAposTrd = $this->autorizacaoexecucao->getQuantidadeTotalQtdAnaliseAposTRD();

        if ($qtdTotalSolicitada === $qtdQtdTotalAposTrd) {
            $this->autorizacaoexecucao->update(['situacao_id' => $situacaoConcluido->id]);
        }
    }

    public function generatePDFRelatorio(): void
    {
        $dompdf = RelatorioPdfService::create(
            'A4',
            RelatorioPdfService::LANDSCAPE
        );

        $tituloRelatorio = 'Ordem de Serviço / Fornecimento';
        $html = view(
            'relatorio.autorizacao-execucao-pdf',
            [
                'titulo' => $tituloRelatorio,
                'unidadeGerenciadora' => $this->contrato->unidade->codigosiasg . ' - ' .
                    $this->contrato->unidade->nome,
                'fields' => $this->getFieldLabels()
            ]
        )->render();

        $dompdf->loadHtml($html);
        $dompdf->render();
        $dompdf->addNumber(
            $this->autorizacaoexecucao->numero,
            $this->contrato->unidade->codigosiasg,
            Carbon::now()->format('d/m/Y H:i:s'),
            $tituloRelatorio
        );

        $dompdf->stream(
            'Ordem de Serviço / Fornecimento Nº ' .
            str_replace('/', '-', $this->autorizacaoexecucao->numero) .
            '.pdf'
        );
    }

    public function generatePDFAssinatura(array $assinantes, bool $saveToStorage = false)
    {
        $assinantes = $this->convertPrepostosResponsaveisToAssinantes($assinantes);

        $fields = $this->getFieldLabels();

        $subcontratacaoItens = [];
        $hiddenColumnsItens = ['especificacao', 'subcontratacao', 'numero_demanda_sistema_externo', 'horario_execucao'];
        $valorTotal = 0;
        foreach ($fields['autorizacaoexecucaoItens']['value'] as $item) {
            $valorTotal += (float) $item->getTotalSemFormatar();
            if ($item->especificacao != null) {
                $hiddenColumnsItens = array_diff($hiddenColumnsItens, ['especificacao']);
            }
            if ($item->numero_demanda_sistema_externo != null) {
                $hiddenColumnsItens = array_diff($hiddenColumnsItens, ['numero_demanda_sistema_externo']);
            }
            if ($item->horario != null || $item->horario_fim != null) {
                $hiddenColumnsItens = array_diff($hiddenColumnsItens, ['horario_execucao']);
            }
            if ($item->subcontratacao) {
                $subcontratacaoItens[] = $item->numero_item_compra . ' - ' . $item->item;
            }
        }

        $html = view(
            'relatorio.autorizacao-execucao-os-pdf',
            [
                'titulo' => 'Ordem de Serviço / Fornecimento nº ' .
                    $this->autorizacaoexecucao->numero .
                    ' Contrato nº ' .
                    $this->contrato->numero,
                'unidadeGerenciadora' => 'Unidade Gestora Atual ' .
                    $this->contrato->unidade->codigosiasg . ' - ' .
                    $this->contrato->unidade->nome,
                'fields' => $this->getFieldLabels(),
                'valorTotal' => $valorTotal,
                'valorPorExtenso' => $this->valorPorExtenso($valorTotal),
                'hiddenColumnsItens' => $hiddenColumnsItens,
                'subcontratacaoItens' => $subcontratacaoItens,
                'assinantes' => $assinantes
            ]
        )->render();

        $dompdf = RelatorioPdfService::create();
        $dompdf->loadHtml($html);
        $dompdf->render();
        $font = $dompdf->getFontMetrics()->getFont(
            "helvetica",
            "normal"
        );
        $canvas = $dompdf->getCanvas();

        $canvas->page_text(
            35,
            820,
            "{$fields['contrato']['label']} nº {$fields['contrato']['value']} - " .
                "{$this->contrato->unidade->codigosiasg}",
            $font,
            10,
        );

        $canvas->page_text(
            565,
            820,
            "{PAGE_NUM}/{PAGE_COUNT}",
            $font,
            10,
        );

        $codigoSiasgUnidadeOrigem = $this->contrato->unidadeorigem->codigosiasg;
        $numeroContrato = str_replace('/', '', $this->contrato->numero);
        $numeroAE = explode('/', $this->autorizacaoexecucao->numero);
        $filename = 'OS-F' . $codigoSiasgUnidadeOrigem . $numeroContrato . $numeroAE[0] ?? '' . $numeroAE[1] ?? '';

        if ($saveToStorage) {
            $path = 'autorizacaoexecucao/' .
                Carbon::now()->format('Y') . '/' .
                Carbon::now()->format('m') . '/' .
                $filename . '.pdf';
            Storage::disk('public')->put($path, $dompdf->output());

            return $path;
        } else {
            $dompdf->stream($filename . '.pdf');
        }
//        $dompdf->stream('', array("Attachment" => false));
    }

    public function getFieldLabels(bool $withValues = true): array
    {
        $result = [];

        $result['contrato']['label'] = 'Contrato';
        $result['unidade_gestora']['label'] = 'Unidade Gestora Atual';
        $result['fornecedor']['label'] = 'Fornecedor';
        $result['contratante']['label'] = 'Contratante';
        $result['restrito']['label'] = 'Objeto';
        $result['vigencia_inicial_label']['label'] = 'Vigência Inicial';
        $result['vigencia_final_label']['label'] = 'Vigência Final';
        $result['amparo_legal']['label'] = 'Amparo Legal';
        $result['contrato_processo']['label'] = 'Número do processo de contratação';
        $result['preposto']['label'] = 'Preposto';
        $result['gestores']['label'] = 'Gestores';

        $result['processo'] = [
            'label' => 'Numero do processo SEI',
            'label_short' => 'Nº processo SEI',
        ];
        $result['tipo_id']['label'] = 'Tipo';
        $result['numero'] = [
            'label' => 'Número/Ano da Ordem de Serviço / Fornecimento',
            'label_short' => 'Número/Ano',
        ];
        $result['data_assinatura']['label'] = 'Data de assinatura';
        $result['data_vigencia_inicio']['label'] = 'Vigência início';
        $result['data_vigencia_fim']['label'] = 'Vigência fim';
        $result['numero_sistema_origem'] = [
            'label' => 'Número da ordem de serviço no sistema de origem',
            'label_short' => 'Nº no sistema de origem',
        ];
        $result['unidaderequisitante_ids']['label'] = 'Unidade requisitante';
        $result['locais_execucao']['label'] = 'Locais de Execução';
        $result['informacoes_complementares']['label'] = 'Informações complementares';
        $result['empenhos']['label'] = 'Empenhos';
        $result['situacao']['label'] = 'Situação';
        $result['originado_sistema_externo']['label'] = 'Originado por sistema externo?';

        $result['autorizacaoexecucaoItens']['label'] = [
            'periodo' => 'Período',
            'tipo_item' => 'Tipo Item',
            'numero_item_compra' => 'Núm. item Compra',
            'item' => 'Item',
            'especificacao' => 'Especificações Complementares',
            'quantidade_contratada' => 'Quantidade Contratada',
            'valor_unitario_contratado' => 'Valor unitário contratado',
            'unidade_medida_id' => 'Unidade de Fornecimento',
            'quantidade' => 'Quantidade',
            'parcela' => 'Parcela',
            'quantidade_total' => 'Quant. solicitada',
            'quantidade_itens_entrega' => 'Quantidade já entregue',
            'valor_unitario' => 'Valor unitário',
            'valor_total' => 'Valor total',
            'horario' => 'Horário início execução',
            'horario_fim' => 'Horário fim execução',
            'subcontratacao' => 'Subcontratação',
            'numero_demanda_sistema_externo' => 'Número da demanda em sistema externo',
        ];

        if ($withValues) {
            $this->addValues($result);
        }

        return $result;
    }

    public function getPrepostosResponsaveisContrato(): array
    {
        $contratoPreposto = new ContratoPrepostoRepository($this->contrato->id);
        $prepostos = $contratoPreposto->getBySituacao();

        $contratoResponsavel = new ContratoResponsavelRepository($this->contrato->id);
        $responsaveis = $contratoResponsavel->getWithFuncoesExcept(['RESPREQUI', 'RESPSETCON']);

        return [
            'prepostos' => $prepostos,
            'responsaveis' => $responsaveis
        ];
    }

    private function addValues(array &$fields): void
    {
        $fields['contrato']['value'] = $this->contrato->numero;
        $fields['unidade_gestora']['value'] = $this->contrato->unidade->codigo;
        $fields['fornecedor']['value'] = "{$this->contrato->fornecedor->cpf_cnpj_idgener} - 
                {$this->contrato->fornecedor->nome}";

        $cnpj = $this->formataCnpj($this->contrato->unidade->cnpj);
        $fields['contratante']['value'] = $cnpj . ' - ' . $this->contrato->unidade->nome;

        $fields['restrito']['value'] = $this->contrato->objeto;
        $fields['vigencia_inicial_label']['value'] = (new \DateTime($this->contrato->vigencia_inicio))
            ->format('d/m/Y');

        $fields['vigencia_final_label']['value'] = $this->contrato->vigencia_fim ?
            (new \DateTime($this->contrato->vigencia_fim))->format('d/m/Y') : 'Indeterminado';

        $fields['amparo_legal']['value'] = $this->contrato->retornaAmparo();
        $fields['contrato_processo']['value'] = $this->contrato->processo;

        $prepostos = [];
        foreach ($this->contrato->prepostos as $preposto) {
            if ($preposto->situacao) {
                $prepostos[] = $preposto->nome . ' - ' . $preposto->email;
            }
        }
        $fields['preposto']['value'] = implode('<br> ', $prepostos);

        $responsaveis = [];
        foreach ($this->contrato->responsaveis as $responsavel) {
            if (in_array($responsavel->funcao->descricao, ['Gestor Substituto', 'Gestor'])) {
                $responsaveis[] = $responsavel->user->name .
                    ' - ' . $responsavel->funcao->descricao .
                    ' (' . $responsavel->portaria . ')';
            }
        }
        $fields['gestores']['value'] = implode('<br> ', $responsaveis);


        if (!empty($this->autorizacaoexecucao)) {
            $fields['processo']['value'] = $this->autorizacaoexecucao->processo ?? $this->contrato->processo;
            $fields['tipo_id']['value'] = [
                'id' => $this->autorizacaoexecucao->tipo_id,
                'descricao' => $this->autorizacaoexecucao->getTipo()
            ];

            $fields['numero']['value'] = $this->autorizacaoexecucao->numero ??
                (new AutorizacaoExecucaoRepository())->getProximoNumero(
                    AutorizacaoExecucao::class,
                    $this->contrato->id
                );

            $fields['data_assinatura']['value'] = [
                'original' => $this->autorizacaoexecucao->data_assinatura,
                'formated' => (new \DateTime($this->autorizacaoexecucao->data_assinatura))->format('d/m/Y'),
            ];

            $fields['data_vigencia_inicio']['value'] = [
                'original' => $this->autorizacaoexecucao->data_vigencia_inicio,
                'formated' => (new \DateTime($this->autorizacaoexecucao->data_vigencia_inicio))->format('d/m/Y'),
            ];

            $fields['data_vigencia_fim']['value'] = [
                'original' => $this->autorizacaoexecucao->data_vigencia_fim,
                'formated' => (new \DateTime($this->autorizacaoexecucao->data_vigencia_fim))->format('d/m/Y'),
            ];

            $fields['numero_sistema_origem']['value'] = $this->autorizacaoexecucao->numero_sistema_origem;


            $fields['unidaderequisitante_ids']['value'] = [
                'ids' => $this->autorizacaoexecucao ? array_map(function (array $item) {
                    return $item['unidade_requisitante_id'];
                }, $this->autorizacaoexecucao->autorizacaoexecucaoUnidadeRequisitantes->toArray()) : [],
                'unidades_descricao' => $this->autorizacaoexecucao
                    ->unidadeRequisitantes()
                    ->get(['codigosiasg', 'nomeresumido'])
                    ->map(function ($unidade) {
                        return $unidade->codigosiasg . ' - ' . $unidade->nomeresumido;
                    })
                    ->reduce(function ($carry, $item) {
                        if (!$carry) {
                            return $item;
                        }
                        return $carry . '<br/>' . $item;
                    })
            ];
            $fields['locais_execucao']['value'] = [
                'ids' => $this->autorizacaoexecucao ? array_map(function (array $item) {
                    return $item['id'];
                }, $this->autorizacaoexecucao->locaisExecucao->toArray()) : [],
                'descricao' => $this->autorizacaoexecucao
                    ->locaisExecucao()
                    ->get(['descricao'])
                    ->map(function ($local) {
                        return $local->descricao;
                    })
                    ->reduce(function ($carry, $item) {
                        if (!$carry) {
                            return $item;
                        }
                        return $carry . '<br/>' . $item;
                    })
            ];
            $fields['informacoes_complementares']['value'] = $this->autorizacaoexecucao->informacoes_complementares;
            $fields['empenhos']['value'] = [
                'ids' => $this->autorizacaoexecucao ? array_map(function (array $item) {
                    return $item['empenhos_id'];
                }, $this->autorizacaoexecucao->autorizacaoexecucaoEmpenhos->toArray()) : [],
                'empenhos_descricao' => $this->autorizacaoexecucao
                    ->empenhos()
                    ->get(['numero', 'unidade_id'])
                    ->map(function ($empenho) {
                        $unidade = $empenho->unidade;
                        if ($unidade) {
                            return $empenho->numero . ' - UG:' . $unidade->codigo . ' - ' . $unidade->nomeresumido;
                        }
                        return $empenho->numero;
                    })
                    ->reduce(function ($carry, $item) {
                        if (!$carry) {
                            return $item;
                        }
                        return $carry . '<br/>' . $item;
                    })
            ];

            $fields['situacao']['value'] = $this->autorizacaoexecucao->situacao_id ?
                $this->autorizacaoexecucao->getSituacao() :
                null;

            $fields['originado_sistema_externo']['value'] = $this->autorizacaoexecucao->originado_sistema_externo;


            $fields['autorizacaoexecucaoItens']['value'] = AutorizacaoExecucaoItensService::selectItensAndSumEntregas(
                $this->autorizacaoexecucao->autorizacaoexecucaoItens()
            )
            ->with(['unidadeMedida'])
            ->get()
            ->map(function ($item) {
                $item->autorizacaoexecucao_itens_id = $item->id;
                return $item;
            });
        }
    }

    /**
     * @param array $tiposWithIds : formato ['preposto-1', 'responsavel-2', 'responsavel-3']
     * @return array
     */
    public function convertPrepostosResponsaveisToAssinantes(array $tiposWithIds): array
    {
        $prepostosResponsaveis = $this->getPrepostosResponsaveisContrato();

        $assinantes = array_map(function (string $tipoWithId) use ($prepostosResponsaveis) {
            $search = explode('-', $tipoWithId);
            throw_if(empty($search[0]) || count($search) !== 2, 'Parâmetro idsWithTipos inválido');
            if (strstr($tipoWithId, 'preposto')) {
                return $prepostosResponsaveis['prepostos']->where('id', $search[1])->first();
            }
            return $prepostosResponsaveis['responsaveis']->where('id', $search[1])->first();
        }, $tiposWithIds);

        $assinantes = array_filter($assinantes, function ($assinante) {
            return !empty($assinante);
        });
        return $assinantes;
    }

    private function getAssinaturasPositions(string $pathPDF, array $assinantes): array
    {
        $parser = new Parser();
        $pdfParser = $parser->parseFile(Storage::disk('public')->path($pathPDF));
        $pages = $pdfParser->getPages();
        $countPages = count($pages);
        $pagesReverse = array_reverse($pages);

        $pointToMilimiter = 0.352777778;
        foreach ($assinantes as $key => $assinante) {
            $posicaoXAssinatura = 0;
            $posicaoYAssinatura = 0;
            $paginaAssinatura = 0;
            $nomeAssinante = $assinante->user ? $assinante->user->name : $assinante->nome;
            foreach ($pagesReverse as $keyPage => $page) {
                $break = false;
                $details = $page->getDetails();
                $heightPage = $details['MediaBox'][3];
                $dataTm = $page->getDataTm();
                foreach ($dataTm as $keyTm => $tm) {
                    if (isset($tm[1]) && $tm[1] === $nomeAssinante) {
                        $break = true;
                        $posicaoXAssinatura = $dataTm[$keyTm - 1][0][4] * $pointToMilimiter;
                        $posicaoYAssinatura = ($heightPage - $dataTm[$keyTm - 1][0][5]) * $pointToMilimiter;
                        $paginaAssinatura = $countPages - $keyPage;
                        break;
                    }
                }
                if ($break) {
                    break;
                }
            }

            $result[$key] = [
                'arquivo_generico_id' => null,
                'posicao_x_assinatura' => $posicaoXAssinatura,
                'posicao_y_assinatura' => $posicaoYAssinatura,
                'pagina_assinatura' => $paginaAssinatura,
            ];
        }

        return $result;
    }

    public function getQuantidadeTotalAutorizacaoExecucao(int $autorizacaoExecucaoId)
    {
        $autorizacaoExecucao = AutorizacaoExecucao::find($autorizacaoExecucaoId);

        //Qtde. Solicitada (OS/F)
        $qtdTotal = $autorizacaoExecucao->getQuantidadeTotalSemFormatacao();
        $valorGlosa = $autorizacaoExecucao->getValorTotalGlosaEntregaAtiva();
        $valorTotal = $autorizacaoExecucao->getValorTotalSemFormatacao();

        //Qtde. em Análise (Até TRP)
        $qtdEmAnaliseAteTrpQtdTotal = $autorizacaoExecucao->getQuantidadeTotalQtdAnaliseAteTRP();
        $qtdEmAnaliseAteTrpValorGlosa = $autorizacaoExecucao->getValorTotalGlosaQtdAnaliseAteTRP();
        $qtdEmAnaliseAteTrpValorTotal = $autorizacaoExecucao->getValorTotalQtdAnaliseAteTRP();

        //Qtde. em Avaliação (Até TRD)
        $qtdEmAnaliseAteTrdQtdTotal = $autorizacaoExecucao->getQuantidadeTotalQtdAnaliseAteTRD();
        $qtdEmAnaliseAteTrdValorGlosa = $autorizacaoExecucao->getValorTotalGlosaQtdAnaliseAteTRD();
        $qtdEmAnaliseAteTrdValorTotal = $autorizacaoExecucao->getValorTotalQtdAnaliseAteTRD();

        //Qtde. Executada (Após TRD)
        $qtdEmAnaliseAposTrdQtdTotal = $autorizacaoExecucao->getQuantidadeTotalQtdAnaliseAposTRD();
        $qtdEmAnaliseAposTrdValorGlosa = $autorizacaoExecucao->getValorTotalGlosaQtdAnaliseAposTRD();
        $qtdEmAnaliseAposTrdValorTotal = $autorizacaoExecucao->getValorTotalQtdAnaliseAposTRD();

        //Saldo a Executar
        // Incluir o somatório das demais linhas dentro do parentese
        $qtdTotalSaldoExecutar =
            $qtdTotal - (
                $qtdEmAnaliseAteTrpQtdTotal + $qtdEmAnaliseAteTrdQtdTotal + $qtdEmAnaliseAposTrdQtdTotal
            );

//        $valorGlosaSaldoExecutar =
//            $valorGlosa - (
//                $qtdEmAnaliseAteTrpValorGlosa + $qtdEmAnaliseAteTrdValorGlosa + $qtdEmAnaliseAposTrdValorGlosa
//            );

        $valorTotalSaldoExecutar =
            $valorTotal - (
                $qtdEmAnaliseAteTrpValorTotal + $qtdEmAnaliseAteTrdValorTotal + $qtdEmAnaliseAposTrdValorTotal +
                $valorGlosa
            );

        $informacoesQuadroResumo = [
            //Qtde. Solicitada (OS/F)
            'qtd_solicitaca_osf' => [
                'qtd_total' => str_replace('.', ',', (float) $qtdTotal),
//                'valor_glosa' => number_format($valorGlosa, 2, ',', '.'),
                'valor_glosa' => null,
                'valor_total' => number_format($valorTotal, 2, ',', '.'),
            ],
            //Qtde. em Análise (Até TRP)
            'qtd_em_analise_ate_trp' => [
                'qtd_total' =>
                    str_replace('.', ',', (float) $qtdEmAnaliseAteTrpQtdTotal),
                'valor_glosa' =>
                    number_format($qtdEmAnaliseAteTrpValorGlosa, 2, ',', '.'),
                'valor_total' =>
                    number_format($qtdEmAnaliseAteTrpValorTotal, 2, ',', '.'),
            ],
            //Qtde. em Avaliação (Até TRD)
            'qtd_em_analise_ate_trd' => [
                'qtd_total' =>
                    str_replace('.', ',', (float) $qtdEmAnaliseAteTrdQtdTotal),
                'valor_glosa' =>
                    number_format($qtdEmAnaliseAteTrdValorGlosa, 2, ',', '.'),
                'valor_total' =>
                    number_format($qtdEmAnaliseAteTrdValorTotal, 2, ',', '.'),
            ],
            //Qtde. Executada (Após TRD)
            'qtd_em_analise_apos_trd' => [
                'qtd_total' =>
                    str_replace('.', ',', (float) $qtdEmAnaliseAposTrdQtdTotal),
                'valor_glosa' =>
                    number_format($qtdEmAnaliseAposTrdValorGlosa, 2, ',', '.'),
                'valor_total' =>
                    number_format($qtdEmAnaliseAposTrdValorTotal, 2, ',', '.'),
            ],
            //Saldo a Executar
            'saldo_executar' => [
                'qtd_total' =>
                    str_replace('.', ',', round($qtdTotalSaldoExecutar, 10)),
                'valor_glosa' => null,
//                    number_format($valorGlosaSaldoExecutar, 2, ',', '.'),
                'valor_total' =>
                    number_format($valorTotalSaldoExecutar, 2, ',', '.'),
            ]
        ];

        return $informacoesQuadroResumo;
    }
}
