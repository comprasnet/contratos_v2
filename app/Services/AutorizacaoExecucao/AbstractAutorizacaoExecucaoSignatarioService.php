<?php

namespace App\Services\AutorizacaoExecucao;

use App\Http\Traits\SignatarioTrait;
use App\Models\CodigoItem;
use App\Services\ModelSignatario\ModelDTO;
use App\Services\ModelSignatario\ModelSignatarioService;
use App\Services\ModelSignatario\SignatarioDTO;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Smalot\PdfParser\Parser;

/**

 * @deprecated prefira usar a service ModelSignatarioService

 */
abstract class AbstractAutorizacaoExecucaoSignatarioService
{
    use SignatarioTrait;

    protected $aeModel;

    protected function assinar(Model $signatarioModel)
    {
        $modelDTO = new ModelDTO($this->aeModel);
        $modelSignatarioService = new ModelSignatarioService($modelDTO);
        $signtarioDTO = new SignatarioDTO($signatarioModel);
        $modelSignatarioService->assinar($signtarioDTO);

        if ($signtarioDTO->fkColumn === 'signatario_contratoresponsavel_id') {
            $isAutoridadeCompetente = in_array($signatarioModel->funcao->descres, ['AUTCOMP']);
            $isGestor = in_array($signatarioModel->funcao->descricao, ['Gestor Substituto', 'Gestor']);
            $isFiscalTecnico = in_array($signatarioModel->funcao->descres, ['FSCTEC', 'FSCTECSUB']);
            if (($isGestor || $isAutoridadeCompetente) && (
                    $modelDTO->fkColumn === 'model_autorizacaoexecucao_id' ||
                    $modelDTO->fkColumn === 'model_autorizacaoexecucao_historico_id'
                )
            ) {
                // condição provisoria até issue de alterar status para assinada para AE
                $status = CodigoItem::where('descres', 'ae_status_7')->first(); // para autorizacaoexecucao_historico
                if ($modelDTO->fkColumn === 'model_autorizacaoexecucao_id') {
                    $status = CodigoItem::where('descres', 'ae_status_2')->first(); // para autorizacaoexecucao
                }
                $this->aeModel->update(['situacao_id' => $status->id, 'data_assinatura' => Carbon::now()]);
            } elseif (($isFiscalTecnico || $isAutoridadeCompetente) &&
                $modelDTO->fkColumn === 'model_autorizacaoexecucao_entrega_trp_id'
            ) {
                $status = CodigoItem::where('descres', 'ae_entrega_status_10')->first();
                $this->aeModel->update(['situacao_id' => $status->id]);
            } elseif (($isGestor || $isAutoridadeCompetente) &&
                $modelDTO->fkColumn === 'model_autorizacaoexecucao_entrega_trd_id'
            ) {
                $descres = $this->aeModel->incluir_instrumento_cobranca ?
                    'ae_entrega_status_12' : 'ae_entrega_status_7';
                $status = CodigoItem::where('descres', $descres)->first();
                $this->aeModel->update(['situacao_id' => $status->id]);
            }
        }
    }

    protected function recusarAssinatura(Model $signatarioModel, string $motivoRecusa): void
    {
        $modelSignatarioService = new ModelSignatarioService(new ModelDTO($this->aeModel));
        $modelSignatarioService->recusar(new SignatarioDTO($signatarioModel));

        $status = CodigoItem::where('descres', 'ae_status_6')->first();
        $updateFields['situacao_id'] = $status->id;

        $this->aeModel->update($updateFields);
    }

    public function verifyUsuarioCanAssinar(): bool
    {
        $result = $this->aeModel->signatariosResponsaveis()->where('user_id', backpack_user()->id)
            ->whereNull('models_signatarios.data_operacao')
            ->exists();

        if (!$result) {
            $result = $this->aeModel->signatariosPrepostos()
                ->whereNull('models_signatarios.data_operacao')
                ->where(function ($query) {
                    $query->where('user_id', backpack_user()->id)
                        ->orWhere('cpf', backpack_user()->cpf);
                })
                ->exists();

            if (session()->get('tipo_acesso') == 'Administrador') {
                $result = $this->verificaSeNaoExisteAssinaturaPreposto();
            }
        }
        return $result;
    }

    public function verificaSeNaoExisteAssinaturaPreposto(): bool
    {
        return  !$this->aeModel->signatariosPrepostos()
        ->whereNotNull('models_signatarios.data_operacao')
        ->exists();
    }

    public function verifyUsuarioResponsavelCanRecusar(): bool
    {
        $aguardandoAssinatura = CodigoItem::where('descres', 'ae_status_5')->first();

        return $this->aeModel->signatariosResponsaveis()->where('user_id', backpack_user()->id)
                ->whereNull('models_signatarios.data_operacao')
                ->exists() && $this->aeModel->situacao_id === $aguardandoAssinatura->id;
    }



    public function getSignatarioByUsuario(): ?Model
    {
        $result = $this->aeModel
            ->signatariosResponsaveis()
            ->whereNull('models_signatarios.data_operacao')
            ->where('user_id', backpack_user()->id)
            ->first();

        if (!$result) {
            $result = $this->aeModel->load(['signatariosPrepostos' => function ($query) {
                $query ->where('user_id', backpack_user()->id)
                    ->orWhere('cpf', backpack_user()->cpf);
            }]);

            if ($result->signatariosPrepostos) {
                $result = $result->signatariosPrepostos->first();
            }
        }

        return $result;
    }

    public function getPrimeiroSignatarioPreposto(): ?Model
    {
        return $this->aeModel->signatariosPrepostos()->first();
    }
}
