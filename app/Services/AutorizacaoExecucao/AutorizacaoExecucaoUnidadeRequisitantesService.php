<?php

namespace App\Services\AutorizacaoExecucao;

use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoexecucaoUnidadeRequisitantes;

class AutorizacaoExecucaoUnidadeRequisitantesService
{
    public function recreateUnidadeRequisitantes(AutorizacaoExecucao $autorizacaoExecucao, array $unidadeRequisitantes)
    {
        $autorizacaoExecucao->autorizacaoexecucaoUnidadeRequisitantes()->delete();

        foreach ($unidadeRequisitantes as $unidadeRequisitante) {
            AutorizacaoexecucaoUnidadeRequisitantes::create([
                'autorizacaoexecucoes_id' => $autorizacaoExecucao->id,
                'unidade_requisitante_id' => $unidadeRequisitante,
            ]);
        }
    }
}
