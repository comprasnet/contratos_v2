<?php

namespace App\Services\AutorizacaoExecucao;

use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoexecucaoItens;
use App\Models\CodigoItem;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;

class AutorizacaoExecucaoItensService
{
    public function recreateItens(AutorizacaoExecucao $autorizacaoExecucao, array $itens)
    {
        $autorizacaoExecucao->autorizacaoexecucaoItens()
            ->whereNotIn('id', array_map(function ($item) {
                return $item['autorizacaoexecucao_itens_id'] ?? 0;
            }, $itens))
            ->delete();

        foreach ($itens as $item) {
            if (empty($item['contratohistorico_id'])) {
                continue;
            }

            $campos = [
                'contratohistorico_id' => $item['contratohistorico_id'],
                'saldohistoricoitens_id' => $item['saldohistoricoitens_id'],
                'quantidade' => is_numeric($item['quantidade']) ? $item['quantidade'] : null,
                'parcela' => is_numeric($item['parcela']) ? $item['parcela'] : null,
                'valor_unitario' => $item['valor_unitario'],
                'horario' => isset($item['horario']) ? $item['horario'] : null,
                'horario_fim' => isset($item['horario_fim']) ? $item['horario_fim'] : null,
                'subcontratacao' => isset($item['subcontratacao']) ? $item['subcontratacao'] : null,
                'unidade_medida_id' => isset($item['unidade_medida_id']) ? $item['unidade_medida_id'] : null,
                'especificacao' => $item['especificacao'],
                'numero_demanda_sistema_externo' => $item['numero_demanda_sistema_externo'],
                'periodo_vigencia_inicio' => $item['periodo_vigencia_inicio'] ?? null,
                'periodo_vigencia_fim' => $item['periodo_vigencia_fim'] ?? null,
            ];

            if (!empty($item['autorizacaoexecucao_itens_id'])) {
                AutorizacaoexecucaoItens::findOrFail($item['autorizacaoexecucao_itens_id'])->update($campos);
            } else {
                $campos['autorizacaoexecucoes_id'] = $autorizacaoExecucao->id;
                AutorizacaoexecucaoItens::create($campos);
            }
        }
    }

    public static function selectItensAndSumEntregas(HasMany $itens): HasMany
    {
        $table = $itens->getModel()->getTable();
        $columnRelationship = "{$table}.id";
        if ($table === 'autorizacaoexecucao_itens_historico') {
            $columnRelationship = "{$table}.autorizacaoexecucao_itens_id";
        }

        return $itens->select(["{$table}.*"])
            ->selectSub(function ($query) use ($columnRelationship) {
                $query->from('autorizacaoexecucao_entrega_itens')
                    ->selectRaw('COALESCE(SUM(autorizacaoexecucao_entrega_itens.quantidade_informada), 0)')
                    ->join('autorizacaoexecucao_entrega', function ($join) {
                        $situacaoEmElaboracao = CodigoItem::where('descres', 'ae_entrega_status_9')->first();
                        $join->on(
                            'autorizacaoexecucao_entrega.id',
                            '=',
                            'autorizacaoexecucao_entrega_itens.autorizacao_execucao_entrega_id'
                        )->where('autorizacaoexecucao_entrega.situacao_id', '<>', $situacaoEmElaboracao->id);
                    })
                    ->whereColumn(
                        "$columnRelationship",
                        'autorizacaoexecucao_entrega_itens.autorizacao_execucao_itens_id'
                    );
            }, 'quantidade_itens_entrega');
    }
}
