<?php

namespace App\Services\AutorizacaoExecucao;

use App\Models\AutorizacaoExecucao;
use Illuminate\Database\Eloquent\Model;

class AutorizacaoExecucaoSignatarioService extends AbstractAutorizacaoExecucaoSignatarioService
{
    public function __construct(AutorizacaoExecucao $aeModel)
    {
        $this->aeModel = $aeModel;
    }

    public function assinar(Model $signatarioModel)
    {
        parent::assinar($signatarioModel);

        AutorizacaoExecucaoStatusHistoricoService::addStatusAssinar($this->aeModel->id);
    }

    public function recusarAssinatura(Model $signatarioModel, string $motivoRecusa): void
    {
        parent::recusarAssinatura($signatarioModel, $motivoRecusa);

        AutorizacaoExecucaoStatusHistoricoService::addStatusRecusarAssinatura($this->aeModel->id, $motivoRecusa);
    }
}
