<?php

namespace App\Services\AutorizacaoExecucao;

use App\Http\Traits\Formatador;
use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoexecucaoHistorico;
use App\Models\CodigoItem;
use App\Repositories\AutorizacaoExecucao\AutorizacaoExecucaoHistoricoRepository;

abstract class AbstractAutorizacaoExecucaoHistoricoService
{
    use Formatador;

    protected $autorizacaoExecucao;

    public function __construct(AutorizacaoExecucao $autorizacaoExecucao)
    {
        $this->autorizacaoExecucao = $autorizacaoExecucao;
    }

    protected function recreateHistoricosItens(
        AutorizacaoexecucaoHistorico $autorizacaoexecucaoHistorico,
        $autorizacaoexecucaoItens
    ): void {
        $autorizacaoexecucaoHistorico->autorizacaoexecucaoItens()->delete();

        $this->autorizacaoExecucao->autorizacaoexecucaoItens->each(
            function ($autorizacaoexecucaoItem) use ($autorizacaoexecucaoHistorico) {
                $itemArray = $autorizacaoexecucaoItem->toArray();
                $itemArray['quantidade'] = $this->retornaFormatoAmericano($itemArray['quantidade']);
                $itemArray['valor_unitario'] = $this->retornaFormatoAmericano($itemArray['valor_unitario']);
                $itemArray['autorizacaoexecucao_itens_id'] = $autorizacaoexecucaoItem->id;
                $itemArray['tipo_historico'] = 'antes';

                $autorizacaoexecucaoHistorico->autorizacaoexecucaoItens()->create($itemArray);
            }
        );

        foreach ($autorizacaoexecucaoItens as $autorizacaoexecucaoItem) {
            $autorizacaoexecucaoItem['tipo_historico'] = 'depois';
            $autorizacaoexecucaoHistorico->autorizacaoexecucaoItens()->create($autorizacaoexecucaoItem);
        }
    }

    protected function dataTransform(array $data, CodigoItem $tipoHistorico, bool $addSequencial = true): array
    {
        $result = [
            'tipo_historico_id' => $tipoHistorico->id,
        ];

        if ($tipoHistorico->descres == 'aeh_alterar') {
            $result['tipo_alteracao'] = $this->getTipoAlteracao($data['tipo_alteracao']);
            $result['rascunho'] = $data['rascunho'];
            $result['data_vigencia_fim_depois'] = $data['data_vigencia_fim'];
            $result['data_inicio_alteracao'] = $data['data_inicio_alteracao'];
            $result['data_extincao'] = $data['data_extincao'];
            $result['justificativa_motivo'] = $data['justificativa_motivo'];
        }

        if ($tipoHistorico->descres == 'aeh_retificar' || $data['rascunho'] == 0) {
            $originalValues = $this->autorizacaoExecucao->toArray();
            $result = array_merge(
                $result,
                $this->getCamposAntesDepois($originalValues, $data),
                ['usuario_responsavel_id' => backpack_user()->id]
            );
        }

        if ($addSequencial) {
            $result['sequencial'] = (new AutorizacaoExecucaoHistoricoRepository($this->autorizacaoExecucao))
                ->getSequencial($tipoHistorico->id, $data['rascunho'] ?? 0);
        }

        return $result;
    }

    /**
     * @param $tipo_alteracao
     * @return string
     */
    private function getTipoAlteracao(array $tipo_alteracao): string
    {
        $result = array_filter(
            array_keys($tipo_alteracao),
            function ($key) use ($tipo_alteracao) {
                if ($tipo_alteracao[$key] == 1) {
                    return $key;
                }
            },
        );
        return implode(";", $result);
    }

    private function getCamposAntesDepois(array $antes, array $depois): array
    {
        $result = [];

        foreach ($depois as $key => $value) {
            if ($key === 'updated_at' || empty($antes[$key])) {
                continue;
            }

            $result[$key . '_antes'] = $antes[$key];
            $result[$key . '_depois'] = $value;
        }

        return $result;
    }
}
