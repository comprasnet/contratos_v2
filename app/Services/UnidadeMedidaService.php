<?php

namespace App\Services;

use App\Api\Externa\ApiCatalogoCompras;
use App\Models\UnidadeMedida;
use App\Repositories\UnidadeMedidaRepository;

class UnidadeMedidaService
{
    /**
     * @throws \Exception
     */
    public static function createByApiAndGet($type, $code)
    {
        $itens = ApiCatalogoCompras::getByTypeAndCode($type, $code);
        UnidadeMedidaRepository::createByCode($type, $code, $itens);
        return UnidadeMedidaRepository::getByTypeAndCode($type, $code);
    }
}
