<?php

namespace App\Services;

use App\Http\Traits\ArquivoGenericoTrait;
use App\Repositories\ArquivoGenerico\ArquivoGenericoRepository;

class ArquivoGenericoService
{
    use ArquivoGenericoTrait;

    public function delete(int $id, string $model, bool $deletarFisicamente = true)
    {
        $arquivoGenerico = new ArquivoGenericoRepository();

        $anexo = $arquivoGenerico->getArquivoGenerico($id, $model);

        if ($anexo) {
            if ($deletarFisicamente) {
                $this->deletarArquivoFiscamente($arquivoGenerico, $anexo->id);
            }
            $anexo->delete();
        }
    }
}
