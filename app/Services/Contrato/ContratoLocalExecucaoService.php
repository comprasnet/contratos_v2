<?php

namespace App\Services\Contrato;

use App\Models\ContratoLocalExecucao;
use App\Models\Enderecos;

class ContratoLocalExecucaoService
{
    public function create(array $data, int $contrato_id): ContratoLocalExecucao
    {
        $enderecos = Enderecos::create([
            'municipios_id' => $data['municipios_id'],
            'cep' => $data['cep'],
            'bairro' => $data['bairro'],
            'logradouro' => $data['logradouro'],
            'complemento' => $data['complemento'],
            'numero' => $data['numero_endereco'],
        ]);

        $contratoLocalExecucao = ContratoLocalExecucao::create([
            'endereco_id' => $enderecos->id,
            'contrato_id' => $contrato_id,
            'descricao' => $data['descricao'],
        ]);

        return $contratoLocalExecucao;
    }
}
