<?php

namespace App\Services\NovoDivulgacaoCompra;

use App\Repositories\CompraItemUnidadeLocalEntregaRepository;

class CompraItemUnidadeLocalEntregaService extends CompraItemUnidadeLocalEntregaRepository
{
    public function insertLocalEntrega(array $data)
    {
        return $this->insertCompraItemUnidadeLocalEntrega($data);
    }
}
