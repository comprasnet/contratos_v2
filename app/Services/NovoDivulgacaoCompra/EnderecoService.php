<?php

namespace App\Services\NovoDivulgacaoCompra;

use App\Models\Enderecos;
use App\Repositories\EnderecoRepository;
use App\Repositories\MunicipiosRepository;
use Exception;

class EnderecoService extends EnderecoRepository
{
    /**
     * @param array $dadosEndereco
     * @return array
     */
    public function insertEndereco(array $dadosEndereco): array
    {
        try {
            $dadosMunicipio =
                (new MunicipiosRepository())->getMunicipio(
                    $dadosEndereco['codigoMunicipio'],
                    $dadosEndereco['uf'],
                    $dadosEndereco['municipio']
                );

            if (empty($dadosMunicipio)) {
                return $this->mountReturnDataEndereco(
                    204,
                    null,
                    "Contratação com local de entrega inválido. Favor retificar no Compras.gov.br"
                );
            }
            $codigoMunicipio = null;
            if (!empty($dadosEndereco['codigoMunicipio'])) {
                $codigoMunicipio = $dadosEndereco['codigoMunicipio'];
            }
            
            $dadosEnderecoFiltro = [
                'municipios_id' => $dadosMunicipio->id,
                'cep' => $dadosEndereco['cep'],
                'bairro' => $dadosEndereco['bairro'],
                'logradouro' => $dadosEndereco['logradouro'],
                'complemento' => $dadosEndereco['complemento'],
                'numero' => $dadosEndereco['numero'],
                'codigo_municipio_ndc' => $codigoMunicipio
            ];
            
            $dadosEnderecoBanco =  $this->insertUpdateEndereco($dadosEnderecoFiltro)->toArray();
            return $this->mountReturnDataEndereco(200, $dadosEnderecoBanco);
        } catch (Exception $e) {
            return $this->mountReturnDataEndereco(500, null, $e->getMessage());
        }
    }
    
    /**
     *
     * @param int $statusCode
     * @param array|null $endereco
     * @param string|null $erro
     * @return array
     */
    private function mountReturnDataEndereco(int $statusCode, ?array $endereco, ?string $erro = null): array
    {
        $dataRetorno = [
            'code' => $statusCode,
            'data' => $endereco
        ];
        
        if (!empty($erro)) {
            $dataRetorno['error'] = $erro;
        }
        
        return $dataRetorno;
    }
}
