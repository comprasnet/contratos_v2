<?php

namespace App\Services\NovoDivulgacaoCompra;

use App\Api\Externa\ApiCatalogoCompras;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\CompraTrait;
use App\Http\Traits\ExternalServices;
use App\Http\Traits\Formatador;
use App\Http\Traits\LogTrait;
use App\Repositories\CodigoItemRepository;
use App\Repositories\MunicipiosRepository;
use App\Repositories\UnidadeRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\Types\Integer;
use phpDocumentor\Reflection\Types\Object_;

class NovoDivulgacaoCompraService
{
    use ExternalServices;
    use CompraTrait;
    use Formatador;
    use LogTrait;
    use BuscaCodigoItens;
    
    protected $url;
    private $service;
    
    protected $arrayErro = [404, 403, 422];

    protected $arraySucesso = [200, 201];
    
    public function __construct()
    {
        $this->url = config('api.novodivulgacaocompra.url');
        $this->service = config('api.novodivulgacaocompra.servico');
    }

    /**
     *
     * Método responsável em retornar as informações da contratação pelo Novo Divulgação
     *
     * @param array $data
     * @return mixed|null
     * @throws GuzzleException
     */
    public function getContratacao(array $data)
    {
        return $this->getNovoContratacaoCompra($data);
    }

    /**
     *
     * Método responsável em retornar as informações da compra a partir da API
     * sendo a prioridade do Novo divulgação de compra e caso não encontre a informação
     * vai buscar na API antiga
     *
     * @param array $data
     * @throws GuzzleException
     */
    private function getNovoContratacaoCompra(array $data)
    {
        $dataCompra = null;

        try {
            $endpoint = $this->mountEndpoint(
                $this->service['contratacao'],
                $data['numeroCompra'],
                $data['anoCompra'],
                $data['uasgCompra']
            );

            # Enviar a requisição
            $response = $this->makeRequest(
                'GET',
                $this->url,
                $endpoint,
                null,
                [],
                null,
                []
            );

            $dataAPI = json_decode($response->getBody()->getContents(), true);

            $dataCompra = $this->formatDataAPICompra($dataAPI);
        } catch (ClientException $clientException) {
            $messageError = 'Erro ao consultar o novo divulgação de compras: ';
            $dataCompra['data'] = null;
            $dataCompra['codigoRetorno'] = $clientException->getResponse()->getStatusCode();
            $dataErro = json_decode($clientException->getResponse()->getBody()->getContents(), true);

            # Se o acesso for negado ou não estiver na VPN
            if (in_array($dataCompra['codigoRetorno'], $this->arrayErro)) {
                $dataCompra['messagem'] = $messageError . ($dataErro['message'] ?? $dataErro['resumoErros']) . '.';
                $this->inserirLogCustomizado('novo_divulgacao_compra', 'error', $clientException);
                return $dataCompra;
            }

            # Recuperar a mensagem enviada pelo serviço
            $messageDetail = utf8_encode($clientException->getResponse()->getHeader('message-detail')[0]);

            # Se não existir a compra que o usuário informou para o Novo Divulgação
            if (false !==
                strpos(
                    $messageDetail,
                    'Não existe execução de contratação associada com a chave enviada.'
                )
            ) {
                $dataCompra['codigoRetorno'] = 204;
                $dataCompra['messagem'] = $messageError . $clientException->getMessage() . '.';
                $this->inserirLogCustomizado('novo_divulgacao_compra', 'error', $clientException);
                return $dataCompra;
            }

            $this->inserirLogCustomizado('novo_divulgacao_compra', 'error', $clientException);
        } catch (\Exception $ex) {
            $dataCompra['data'] = null;
            $dataCompra['codigoRetorno'] = $ex->getCode() !== 0 ? $ex->getCode() : 500;

            $dataCompra['messagem'] = 'Erro ao consultar o novo divulgação de compras: ' . $ex->getMessage();

            $this->inserirLogCustomizado('novo_divulgacao_compra', 'error', $ex);
            return $dataCompra;
        }

        return $dataCompra;
    }

    /**
     *
     * Método responsável em recuperar a compra a partir do serviço do Compras SIASG
     *
     * @param array $data
     * @return mixed
     */
    private function getAPICompra(array $data)
    {
        $arrayRequest = $this->mountArrayRequestAPICompras($data);

        $requestCompra = new Request($arrayRequest);

        return $this->consultaCompraSiasg($requestCompra);
    }

    /**
     *
     * Método responsável em retornar a querystring para montar url
     *
     * @param string $uri
     * @param int $numeroCompra
     * @param string $uasgCompra
     * @param int $anoCompra
     * @return string
     */
    private function mountEndpoint(string $uri, int $numeroCompra, string $uasgCompra, int $anoCompra): string
    {
        $arrayQueryString = [
            'numeroCompra' => $numeroCompra,
            'anoCompra' => $uasgCompra,
            'uasgCompra' => $anoCompra,
        ];

        $queryString = http_build_query($arrayQueryString);

        return "{$uri}?{$queryString}";
    }

    /**
     * Método responsável em formatar o número da compra no padrão que está salvo no banco de dados
     * @param int $numeroCompra
     * @param int $anoCompra
     * @return string
     */
    private function formatNumeroCompra(int $numeroCompra, int $anoCompra): string
    {
        $numberFormatCompra = $this->preencherCasasDireitaEsquerda($numeroCompra, 5);

        return "{$numberFormatCompra}/{$anoCompra}";
    }

    /**
     * Método responsável em montar o array para criar o request
     * @param array $dataRequest
     * @return array
     */
    private function mountArrayRequestAPICompras(array $dataRequest): array
    {
        $codigoItemRepository = new CodigoItemRepository();
        $unidadeRepository = new UnidadeRepository();

        # Recuperar o id da unidade de origem
        $unidadeOrigemId = $unidadeRepository->getUnidadePorCodigo($dataRequest['uasgCompra'])->id;

        # Formatar o número da compra conforme salvo em banco
        $numero_ano = $this->formatNumeroCompra($dataRequest['numeroCompra'], $dataRequest['anoCompra']);

        # Montar o array para criar o request
        $arrayRequest = [
            'modalidade_id' =>
                $codigoItemRepository->recuperarIdModalidadePorDescres($dataRequest['modalidadeCompra']),
            'unidade_origem_id' => $unidadeOrigemId,
            'unidade_origem_compra_id' => $unidadeOrigemId,
            'numero_ano' => $numero_ano
        ];

        # Se buscar por uma unidade sub-rogada
        if ($dataRequest['uasgCompra'] != $dataRequest['uasgCompraOrigem']) {
            $arrayRequest['unidade_origem_compra_id'] =
                $unidadeRepository->getUnidadePorCodigo($dataRequest['uasgCompraOrigem'])->id;
        }

        return $arrayRequest;
    }

    /**
     * Método responsável em formatar a saída do Novo divulgação para o padrão do antigo
     * para poder alterar as informações somente no arquivo do CompraTrait
     *
     * @param array $dataAPINovoDivulgacao
     * @return array
     */
    private function formatDataAPICompra(array $dataAPINovoDivulgacao): array
    {
        $dataReturn = array();

        # Montar as informações básicas da compra
        $dataReturn['data']['compraSispp']['tipoCompra'] =
            $this->formatFieldTipoCompra($dataAPINovoDivulgacao['idenficadorCompraSRP']);

        $dataReturn['data']['compraSispp']['inciso'] = $dataAPINovoDivulgacao['inciso'];
        $dataReturn['data']['compraSispp']['lei'] = $this->formatLei($dataAPINovoDivulgacao['lei']);
        $dataReturn['data']['compraSispp']['artigo'] = $dataAPINovoDivulgacao['artigo'];
        
        $dataAPINovoDivulgacao['codigoUasgSubRogada'] = $dataAPINovoDivulgacao['codigoUasgSubRogada'] ?? null;

        $dataReturn['data']['compraSispp']['subrogada'] =
            $this->formatFieldSubRogada($dataAPINovoDivulgacao['codigoUasgSubRogada']);

        if (!isset($dataAPINovoDivulgacao['identificadorPNCP'])) {
            throw new \Exception(
                'Não existe o campo identificadorPNCP.',
                500
            );
        }
        $dataReturn['data']['compraSispp']['idUnico'] =
            $this->preencherCasasDireitaEsquerda($dataAPINovoDivulgacao['identificadorPNCP'], 6);

        $dataReturn['data']['compraSispp']['cnpjOrgao'] =
            $this->preencherCasasDireitaEsquerda($dataAPINovoDivulgacao['cnpjOrgao'], 14);

        $dataReturn['data']['compraSispp']['ano'] = $dataAPINovoDivulgacao['anoCompra'];
        
        $dataReturn['data']['compraSispp']['codigoModalidade'] =
            $this->preencherCasasDireitaEsquerda($dataAPINovoDivulgacao['idModalidade']);
        $dataReturn['data']['compraSispp']['nomeModalidade'] = $dataAPINovoDivulgacao['nomeModalidade'];
        
        $dataReturn['data']['linkSisrpCompleto'] = $this->mountItemCompraSisrp(
            $dataReturn['data']['compraSispp']['tipoCompra'],
            $dataAPINovoDivulgacao['links']
        );
        $dataReturn['data']['itemCompraSisppDTO'] = $this->mountItemCompraSispp(
            $dataReturn['data']['compraSispp']['tipoCompra'],
            $dataAPINovoDivulgacao['links']
        );

        $dataReturn['data']['api_origem'] = 'novo_divulgacao_compra';
        $dataReturn['codigoRetorno'] = 200;
        $dataReturn['messagem'] = 'Sucesso';

        return $dataReturn;
    }

    /**
     *
     * Método responsável em formatar o nome da LEI no padrão salvo em banco de dados
     *
     * @param $nomeLei
     * @return mixed|string
     */
    private function formatLei($nomeLei)
    {
        $leiFormatada = $nomeLei;

        switch ($nomeLei) {
            case 14133:
            case 14628:
            case 11652:
            case 14744:
            case 6855:
            case 14981:
            case 12351:
                $leiFormatada = "LEI{$nomeLei}";
                break;
            case 1221:
                $leiFormatada = "MP{$nomeLei}";
                break;
        }

        return $leiFormatada;
    }

    /**
     *
     * Método responsável em percorrer todos os links dos itens da compra
     * do tipo SISPP e retornar dentro da requisição todas as informações
     * sobre o item
     *
     * @param int $tipoCompra
     * @param array $linkItens
     * @return array
     * @throws GuzzleException
     */
    private function mountItemCompraSispp(int $tipoCompra, array $linkItens): array
    {
        $itensSispp = array();
        if ($tipoCompra === 1) {
            foreach ($linkItens as $key => $linkItem) {
                $queryString = explode('?', $linkItem)[1];

                $endpointItem = $this->service['item'] . "?" . $queryString;
                try {
                    $responseItem = $this->makeRequest(
                        'GET',
                        $this->url,
                        $endpointItem,
                        null,
                        [],
                        null,
                        []
                    );
                    
                    $dataItemAPI = json_decode($responseItem->getBody()->getContents(), true);
                    
                    $dataItemAPI = $this->inserirUasgCompra($endpointItem, $dataItemAPI);
                    
                    $itensSispp[$key] = $this->formatItemNovoDivulgacaoParaPadraoCompras($dataItemAPI, $tipoCompra);
                } catch (ClientException $clientException) {
                    $mensagem = "Erro ao consultar o endpoint: $endpointItem \n\r";
                    $json = json_decode($clientException->getResponse()->getBody()->getContents());
                    $mensagem .= "Código: {$json->status}, Mensagem: {$json->message}";
                    Log::error($mensagem);
                } catch (Exception $exception) {
                    $mensagem = "Erro ao consultar o endpoint: $endpointItem \n\r";
                    Log::error($mensagem);
                    Log::error($exception);
                }
            }
        }
        
        return $itensSispp;
    }
    
    /**
     *
     * Método responsável em formatar as informações do item que serão retornados pela API do Novo Divulgação
     * para o padrão que utilizamos no contratos
     *
     * @param array $dataItemAPI
     * @return array
     * @throws GuzzleException
     */
    protected function formatItemNovoDivulgacaoParaPadraoCompras(
        array $dataItemAPI,
        int $tipoCompra = 1,
        ?string $lei = null
    ): array {
        $arrayInformacaoItem = array();
        $textoNumero = 'numeroItem';

        //caso seja SISPP
        if ($tipoCompra === 1) {
            $textoNumero = 'numero';
            $informacoesFornecedorNDC = $dataItemAPI['resultados'][0];
            
            if (!isset($informacoesFornecedorNDC['identificacaoFornecedor']) &&
                !isset($informacoesFornecedorNDC['nomeFornecedor'])) {
                throw new \Exception(
                    'Não existem os campos identificacaoFornecedor e nomeFornecedor',
                    500
                );
            }
            
            $arrayInformacaoItem['niFornecedor']   = $informacoesFornecedorNDC['identificacaoFornecedor'];
            $arrayInformacaoItem['nomeFornecedor'] = $informacoesFornecedorNDC['nomeFornecedor'];
        }
        
        $arrayInformacaoItem[$textoNumero] = $this->preencherCasasDireitaEsquerda(
            $dataItemAPI['numeroItem'],
            5
        );

        $arrayInformacaoItem['tipo'] = $dataItemAPI['materialServico'];
        $arrayInformacaoItem['codigo'] = $dataItemAPI['codigoItem'];
        $arrayInformacaoItem['descricao'] = $dataItemAPI['nomePdm'] ?: $dataItemAPI['descricaoItem'];
        $arrayInformacaoItem['descricaoDetalhada'] = $dataItemAPI['descricaoItem'];
        $arrayInformacaoItem['quantidadeTotal'] =
            array_sum(array_column($dataItemAPI['locaisEntrega'], 'quantidade'));
        
        $dadosFornecedor = $dataItemAPI['resultados'];
        
        $arrayInformacaoItem['valorUnitario'] =
            array_sum(array_column($dadosFornecedor, 'valorUnitarioItem'));
        
        $arrayInformacaoItem['valorTotal'] =
            array_sum(array_column($dadosFornecedor, 'valorTotal'));

        # Informação ainda não retorna no serviço
        $arrayInformacaoItem['situacaoSicaf'] = $dataItemAPI['situacaoSicaf'] ?? null;
        $arrayInformacaoItem['tipoBeneficio'] = $dataItemAPI['tipoBeneficio'] ?? null;
        $this->formatCriterioJulgamento($arrayInformacaoItem, $dataItemAPI);

        $arrayInformacaoItem['percentualMaiorDesconto'] = $dataItemAPI['percentualMaiorDesconto'] ?? null;
        $arrayInformacaoItem['maximoAdesao'] =
            $this->formataMaximoAdesao($arrayInformacaoItem['quantidadeTotal'], $lei);
        
        $arrayInformacaoItem['permiteCarona'] = true;
        
        $arrayInformacaoItem['pdm'] = null;
        
        if ($arrayInformacaoItem['tipo'] === 'M') {
            $arrayInformacaoItem['pdm'] =
                $this->preencherCasasDireitaEsquerda($dataItemAPI['codigoPdm'], 5);
        }

        $arrayInformacaoItem = $this->formatDataPdmAPI($arrayInformacaoItem);
        
        # Informação ainda não retorna no serviço
        $arrayInformacaoItem['grupoCompra'] = $dataItemAPI['grupoCompra'] ?? null;
        $arrayInformacaoItem['unidadeFornecimento'] = $dataItemAPI['unidadeFornecimento'] ?? null;
        $arrayInformacaoItem['capacidade'] =  $dataItemAPI['capacidade'] ?? null;
        $arrayInformacaoItem['unidadeMedida'] = $dataItemAPI['unidadeMedida'] ?? null;
        $arrayInformacaoItem['marca'] = $dataItemAPI['marca'] ?? null;
        $arrayInformacaoItem['dadosBasicosSubcontratadas'] = [];
        
        # Substituir o atributo identificacaoFornecedor para o niFornecedor
        # necessário para a criação do registro na compra item fornecedor
        $dadosFornecedor = $this->formatDataFornecedor($dadosFornecedor, $dataItemAPI);
        
        $arrayInformacaoItem['resultados'] = $dadosFornecedor;
        $arrayInformacaoItem['locaisEntrega'] = $dataItemAPI['locaisEntrega'];

        $arrayInformacaoItem['dadosUasgItemCompra'] =
            $this->formatDataUnidadeItem($arrayInformacaoItem['locaisEntrega'], $dataItemAPI['uasgCompra']);

        $arrayInformacaoItem['locaisEntregaPorUnidade'] = $this->formatDataLocalEntregaUnidade(
            $arrayInformacaoItem['locaisEntrega'],
            $arrayInformacaoItem['dadosUasgItemCompra'],
            $arrayInformacaoItem[$textoNumero]
        );
        
        return $arrayInformacaoItem;
    }
    
    /**
     * @param array $locaisEntrega
     * @param array $unidadesItemCompra
     * @return array
     */
    private function formatDataLocalEntregaUnidade(
        array $locaisEntrega,
        array $unidadesItemCompra,
        string $numeroItem
    ): array {
        $arrayLocalEntrega = array();
        $municipioRepository = new MunicipiosRepository();
        foreach ($unidadesItemCompra as $unidade) {
            $dadosLocalEntrega['localEntrega'] =
                array_map(function ($item) use ($unidade, $municipioRepository, $numeroItem) {
                    if ($item['numeroUasg'] == $unidade['uasg']) {
                        $municipioNaBase = $municipioRepository->getMunicipio(
                            $item['codigoMunicipio'],
                            $item['uf'],
                            $item['municipio']
                        );

                        if (empty($municipioNaBase)) {
                            $unidadeErro = "Item {$numeroItem} da unidade {$unidade['uasg']} com o endereço: ";
                            $unidadeErro .= "{$item['uf']} - {$item['municipio']}";
                            $mensagemErro = "$unidadeErro <br>";
                            $mensagemErro .=
                                'Contratação com local de entrega inválido. Favor retificar no Compras.gov.br';
                            throw new \Exception($mensagemErro, 500);
                        }

                        $item['municipio'] = $municipioNaBase->nome;
                        $item['uf'] = $municipioNaBase->sigla;
                        $item['codigoMunicipio'] = $municipioNaBase->codigo_ibge;

                        unset($item['numeroUasg'], $item['nomeUasg']);

                        return $item;
                    }
                }, $locaisEntrega);

            $dadosLocalEntrega['quantidadeAAdquirir'] =
            array_sum(array_column($dadosLocalEntrega['localEntrega'], 'quantidade'));
            
            $dadosLocalEntrega['quantidadeAdquirida'] = 0;
            
            $dadosLocalEntrega['quantidadeAAdquirir'] =
                array_sum(array_column($dadosLocalEntrega['localEntrega'], 'quantidade'));
            
            $dadosLocalEntrega['tipoUasg'] = $unidade['tipoUasg'];
            
            $dadosLocalEntrega['localEntrega'] = array_filter($dadosLocalEntrega['localEntrega']);

            $arrayLocalEntrega[$this->preencherCasasDireitaEsquerda($unidade['uasg'], 6)] = $dadosLocalEntrega;
        }

        return $arrayLocalEntrega;
    }
    
    /**
     * @param array $locaisEntrega
     * @param string $uasgCompra
     * @return array
     */
    private function formatDataUnidadeItem(array $locaisEntrega, string $uasgCompra): array
    {
        # Recupera somente as informações das unidades
        $dadosUnidadeLocaisEntrega = array_map(function ($item) {
            $unidadeEncontrada = array();
            $unidadeEncontrada['uasg'] = $this->preencherCasasDireitaEsquerda($item['numeroUasg'], 6);
            $unidadeEncontrada['nomeUasg'] = $item['nomeUasg'];
            
            return $unidadeEncontrada;
        }, $locaisEntrega);

        # Filtra para recuperar os valores distintos
        $arrayUnidadesDistintas =
            array_map("unserialize", array_unique(array_map("serialize", $dadosUnidadeLocaisEntrega)));
        
        # Verificar se a gerenciadora não tem quantidade para o item
        $gerenciadoraComQuantidadeZerada = true;
        
        $dadosUnidadeItemFormatado = array_map(function ($item) use ($uasgCompra, &$gerenciadoraComQuantidadeZerada) {
            $item['tipoUasg'] = 'G';
            
            if ($item['uasg'] != $uasgCompra) {
                $item['tipoUasg'] = 'P';
            }
            
            if ($item['uasg'] == $uasgCompra) {
                $gerenciadoraComQuantidadeZerada = false;
            }

            return $item;
        }, $arrayUnidadesDistintas);

        # Montar a informação da gerenciadora para o item
        if ($gerenciadoraComQuantidadeZerada) {
            $value = [
                'uasg' => $uasgCompra,
                'nomeUasg' => '',
                'tipoUasg' => 'G'
            ];
            $dadosUnidadeItemFormatado[] = $value;
        }

        return $dadosUnidadeItemFormatado;
    }
    
    /**
     * @param array $dadosFornecedor
     * @param array $dataItemAPI
     * @return array|array[]
     */
    private function formatDataFornecedor(array $dadosFornecedor, array $dataItemAPI): array
    {
        $dadosFornecedorFormatado = $this->convertKeyNovoDivulgacaoParaContratos(
            $dadosFornecedor,
            'identificacaoFornecedor',
            'niFornecedor'
        );
        
        $dadosFornecedorFormatado = $this->convertKeyNovoDivulgacaoParaContratos(
            $dadosFornecedorFormatado,
            'valorTotal',
            'valorNegociado'
        );
        
        $dadosFornecedorFormatado = $this->convertKeyNovoDivulgacaoParaContratos(
            $dadosFornecedorFormatado,
            'valorUnitarioItem',
            'valorUnitario'
        );
        
        $dadosFornecedorFormatado = $this->convertKeyNovoDivulgacaoParaContratos(
            $dadosFornecedorFormatado,
            'classificacaoFornecedor',
            'classificacao'
        );
        
        # Formatar a informação da classificação do fornecedor
        $dadosFornecedorFormatado = array_map(function ($fornecedor) {
            $fornecedor['classificacao'] =
                $this->preencherCasasDireitaEsquerda($fornecedor['classificacao'], 3);
            return $fornecedor;
        }, $dadosFornecedorFormatado);
        
        $dadosFornecedorFormatado = $this->convertKeyNovoDivulgacaoParaContratos(
            $dadosFornecedorFormatado,
            'fornecedorCredenciadoSicaf',
            'situacaoSicaf'
        );
        
        # Inclusão do atributo percentualMaiorDesconto recuperado pelo item
        $dadosFornecedorFormatado = array_map(function ($item) use ($dataItemAPI) {
            $item['percentualMaiorDesconto'] = $dataItemAPI['percentualMaiorDesconto'];
            return $item;
        }, $dadosFornecedorFormatado);
        
        $dadosFornecedorFormatado = array_map(function ($item) {
            $item['quantidadeHomologadaVencedor'] = $item['quantidadeItens'];
            return $item;
        }, $dadosFornecedorFormatado);

        return $dadosFornecedorFormatado;
    }
    
    /**
     * @param array $dadosAPI
     * @param string $keyServico
     * @param string $keyCompras
     * @return array|array[]
     */
    private function convertKeyNovoDivulgacaoParaContratos(
        array $dadosAPI,
        string $keyServico,
        string $keyCompras
    ): array {
        return array_map(
            function ($array) use ($keyServico, $keyCompras) {
                return $this->callBackFormatArrayFornecedor($array, $keyServico, $keyCompras);
            },
            $dadosAPI
        );
    }
    
    /**
     * @param array $array
     * @param string $keyServico
     * @param string $keyCompras
     * @return array
     */
    private function callBackFormatArrayFornecedor(
        array $array,
        string $keyServico,
        string $keyCompras
    ): array {
        # Se a chave não existir no serviço, então criamos com o valor nulo
        if (!isset($array[$keyServico])) {
            $array[$keyServico] = null;
        }
        
        $array[$keyCompras] = $array[$keyServico];
        
        unset($array[$keyServico]);
        
        return $array;
    }
    
    /**
     *
     * Método responsável em retornar as informações do PDM do item
     *
     * @param array $arrayInformacaoItem
     * @return array
     * @throws GuzzleException
     */
    private function formatDataPdmAPI(array $arrayInformacaoItem): array
    {
        $arrayInformacaoItem['nomePdm'] = null;

        # Se o tipo do item for material, então recupera o nome do PDM na API do Catálago de Compras
        if ($arrayInformacaoItem['tipo'] == 'M') {
            # Recuperar as informações do PDM na API do Catálago de Compras
            $dataPdmAPI = (new ApiCatalogoCompras())->getPdmByCodigoSiasg($arrayInformacaoItem['codigo']);

            if (!empty($dataPdmAPI)) {
                $nomePdm = $dataPdmAPI['nomePdm'] ?? '';
                $arrayInformacaoItem['nomePdm'] = mb_strtoupper($nomePdm, 'UTF-8');
            }
        }

        return $arrayInformacaoItem;
    }

    /**
     *
     * Método responsável em montar o link dos itens para o tipo da compra
     * SISRP
     *
     * @param int $tipoCompra
     * @param array $linkItens
     * @return array|\string[][]|null
     */
    private function mountItemCompraSisrp(int $tipoCompra, array $linkItens): ?array
    {
        if ($tipoCompra == 2) {
            # Formatar o link para buscar o item no serviço dois
            return array_map(function ($value) {
                return ['linkSisrpCompleto' => $this->formatUrlItem($value)];
            }, $linkItens);
        }
        return null;
    }

    /**
     * Método responsável em formatar a URL do item para ser consumido no serviço dois
     * @param string $urlItem
     * @return string
     */
    private function formatUrlItem(string $urlItem): string
    {
        $queryString = explode('?', $urlItem)[1];
        $urlItem = str_replace('contratacao', 'item?', url()->current());

        return "{$urlItem}?{$queryString}";
    }

    /**
     *
     * Método responsável em converter o valor do identificador da compra para o valor inteiro
     * salvo em banco de dados
     *
     * @param bool $idenficadorCompraSRP
     * @return int
     */
    private function formatFieldTipoCompra(bool $idenficadorCompraSRP): int
    {
        if ($idenficadorCompraSRP) {
            return 2;
        }

        return 1;
    }

    /**
     * Método responsável em retornar o código da uasg se for a compra for sub-rogada
     * @param string $codigoUasgSubRogada
     * @return string
     */
    private function formatFieldSubRogada(?string $codigoUasgSubRogada): string
    {
        # Se a compra não for sub-rogada
        if (empty($codigoUasgSubRogada)) {
            return '000000';
        }

        return $codigoUasgSubRogada;
    }
    
    /**
     * Método responsável em retornar o item formatado no padrão do Compras
     * @param string $urlItem
     * @return array
     * @throws GuzzleException
     */
    public function getItemCompraNovoDivulgacao(string $urlItem): array
    {
        $arrayUrlItem = explode('/', $urlItem);
        $endpointItem = end($arrayUrlItem);
        
        $responseItem = $this->makeRequest(
            'GET',
            $this->url,
            $endpointItem,
            null,
            [],
            null,
            []
        );

        $dataItemCompraAPI = json_decode($responseItem->getBody()->getContents(), true);
        $dataItemCompraAPI = $this->inserirUasgCompra($endpointItem, $dataItemCompraAPI);
        
        try {
            return $this->formatItemNovoDivulgacaoParaPadraoCompras($dataItemCompraAPI);
        } catch (\Exception $ex) {
            $dataCompra['codigoRetorno'] = $ex->getCode();
            $dataCompra['messagem'] = 'Erro ao consultar o novo divulgação de compras: ' . $ex->getMessage() . '.';
            
            $this->inserirLogCustomizado('novo_divulgacao_compra', 'error', $ex);
            return $dataCompra;
        }
    }
    
    /**
     * @param string $endpointItem
     * @param array $dataItemCompraAPI
     * @return array
     */
    protected function inserirUasgCompra(string $endpointItem, array $dataItemCompraAPI): array
    {
        $queryString = str_replace("item?", "", $endpointItem);
        parse_str($queryString, $queryParams);
        
        $dataItemCompraAPI['uasgCompra'] = $queryParams['uasgCompra'];
        return $dataItemCompraAPI;
    }

    /**
     * Formata o campo 'criterioJulgamento' para o padrao do Contratos
     *
     * @param array $arrayInformacaoItem The array to be updated with the formatted fields.
     * @param array $dataItemAPI The array containing the 'criterioJulgamento' field.
     * @return void
     */
    private function formatCriterioJulgamento(array &$arrayInformacaoItem, array $dataItemAPI): void
    {
        $arrayInformacaoItem['criterioJulgamento']   = 'V';
        $arrayInformacaoItem['criterioJulgamentoId'] = null;

        if (isset($dataItemAPI['criterioJulgamento']['id']) && $dataItemAPI['criterioJulgamento']['id'] == 2) {
            $arrayInformacaoItem['criterioJulgamento'] = 'D';
        }

        if (isset($dataItemAPI['criterioJulgamento']['id'])) {
            $criterioJulgamentoId = $this->retornaIdCodigoItemPorDescres(
                $dataItemAPI['criterioJulgamento']['id'],
                'Critério de Julgamento vindo do NDC'
            );
            $arrayInformacaoItem['criterioJulgamentoId'] = $criterioJulgamentoId;
        }
        //TODO: verificar se é para salvar "codigoFaseExterna" e "codigoPncp"
    }
    
    public function containsAny($haystack, array $needles)
    {
        $retorno = false;
        foreach ($needles as $needle) {
            if (str_contains($haystack, $needle)) {
                $retorno = true;
                break;
            }
        }
        return $retorno;
    }
    
    public function formataMaximoAdesao(float $quantidadeTotal, ?string $lei)
    {
        $fatorMultiplicacao = 2;
        
        $compraPertenceMPCalamidadePublica = $this->compraPertenceMPCalamidadePublica($lei);
        
        
        if ($compraPertenceMPCalamidadePublica) {
            $fatorMultiplicacao = 5;
        }

        return $quantidadeTotal * $fatorMultiplicacao;
    }
}
