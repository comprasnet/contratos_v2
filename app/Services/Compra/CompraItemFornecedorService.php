<?php

namespace App\Services\Compra;

use App\Models\CompraItemFornecedor;

class CompraItemFornecedorService
{
    public function fornecedorExisteItemPorCnpj(int $compraItemId, string $cpnj)
    {
        return CompraItemFornecedor::where('compra_item_id', $compraItemId)
            ->where('ni_fornecedor', $cpnj)
            ->exists();
    }
}
