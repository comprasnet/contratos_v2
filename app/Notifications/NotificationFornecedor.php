<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NotificationFornecedor extends Notification
{
    use Queueable;
    protected $model;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    public function toDatabase($notifiable)
    {

        $montarUrlTexto = $this->montarUrlTexto($this->model);
        return [
            'data' => [
                'ae_id' => $this->model->id,
                'url' => url($montarUrlTexto['url']),
                'texto_lista_suspesa' => $montarUrlTexto['texto_lista_suspesa'],
                'texto_area_notificacao' => $montarUrlTexto['texto_area_notificacao']
            ],
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
    
    private function montarUrlTexto(Model $model)
    {
        switch (get_class($model)) {
            case 'App\Models\AutorizacaoExecucao':
                $arrayRetorno = [
                    'url' => "fornecedor/autorizacaoexecucao/{$model->contrato_id}/{$model->id}/assinatura/create",
                    'texto_lista_suspesa' =>
                        "OS/F: {$model->numero} do contrato {$model->contrato->numero} está pendente de assinatura",

                    'texto_area_notificacao' =>
                        "Ordem de Serviço e Fornecimento: {$model->numero} do contrato {$model->contrato->numero} ".
                        "está pendente de assinatura"
                ];

                if ($model->notificacao_situacao_alterada) {
                    $arrayRetorno = [
                        'url' => "fornecedor/autorizacaoexecucao/{$model->id}/show",
                        'texto_lista_suspesa' =>
                            "Alteração na situação da OS/F: {$model->numero} do contrato {$model->contrato->numero}",
                        'texto_area_notificacao' =>
                            "Alteração da ordem de Serviço e Fornecimento: {$model->numero} ".
                            "do contrato {$model->contrato->numero} para {$model->situacao->descricao}."
                    ];
                }
                break;
            case 'App\Models\AutorizacaoexecucaoHistorico':
                $arrayRetorno = [
                    'url' => "fornecedor/autorizacaoexecucao/{$model->autorizacaoexecucao->contrato_id}/".
                             "{$model->autorizacaoexecucoes_id}/alterar/{$model->id}/assinatura/create",
                    'texto_lista_suspesa' =>
                        "Alteração da OS/F: {$model->autorizacaoexecucao->numero} ".
                        "do contrato {$model->autorizacaoexecucao->contrato->numero} ".
                        "está pendente de assinatura",
                    'texto_area_notificacao' =>
                        "Alteração da Ordem de Serviço e Fornecimento: {$model->autorizacaoexecucao->numero} ".
                        "do contrato {$model->autorizacaoexecucao->contrato->numero} ".
                        "está pendente de assinatura"
                ];
                break;
            case 'App\Models\AutorizacaoExecucaoEntrega':
                $arrayRetorno = [
                    'url' => "fornecedor/arp/adesao/analisar/{$model->adesao->id}/edit",
                    'texto_lista_suspesa' =>
                        "Alteração na situação da entrega {$model->getFormattedSequencial()} ".
                        "da OS/F: {$model->autorizacao->numero} do contrato {$model->autorizacao->contrato->numero}",
                    'texto_area_notificacao' =>
                        "Alteração na situação da entrega {$model->getFormattedSequencial()} ".
                        "da Ordem de Serviço e Fornecimento: {$model->autorizacao->numero} ".
                        "do contrato {$model->autorizacao->contrato->numero} para {$model->situacao->descricao}"
                ];
                break;

            case 'App\Repositories\Arp\ArpAdesaoItemRepository':
                $arrayRetorno = [
                    'url' => "fornecedor/arp/adesao/analisar/{$model->adesao->id}/edit",
                    'texto_lista_suspesa' => "Solicitação de adesão à Ata " .
                        "{$model->item_arp->arp->getNumeroAno()} pendente de análise.",
                    'texto_area_notificacao' => "Solicitação de anuência a adesão à ".
                    "Ata de Registro de Preço {$model->item_arp->arp->getNumeroAno()} da ".
                    " UG {$model->adesao->getUnidadeGerenciadora()} pendente de análise."
                ];

                break;
            default:
                $arrayRetorno = ['url' => 'Tipo não definido', 'texto' => 'Tipo não definido'];
        }

        return $arrayRetorno;
    }
}
