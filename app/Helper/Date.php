<?php

namespace App\Helper;

class Date
{
    public static function convertFormat($date, $from, $to){
        return \Carbon\Carbon::createFromFormat($from, $date)
            ->format($to);
    }

    public static function brlToDb($date){
        return self::convertFormat($date,'d/m/Y', 'Y-m-d');
    }
}