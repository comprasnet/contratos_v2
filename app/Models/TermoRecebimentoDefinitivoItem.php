<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TermoRecebimentoDefinitivoItem extends Model
{
    use HasFactory;
    use CrudTrait;

    protected $table = 'termo_recebimento_definitivo_itens';

    protected $fillable = [
        'termo_recebimento_definitivo_id',
        'termo_recebimento_provisorio_item_id',
        'itemable_id',
        'itemable_type',
        'quantidade_informada',
        'valor_glosa',
        'mes_ano_competencia',
        'processo_sei',
        'data_inicio',
        'data_fim',
        'horario',
    ];

    public function trd()
    {
        return $this->belongsTo(TermoRecebimentoDefinitivo::class, 'termo_recebimento_definitivo_id');
    }

    public function trpItem()
    {
        return $this->belongsTo(TermoRecebimentoProvisorioItem::class, 'termo_recebimento_provisorio_item_id');
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getAutorizacaoExecucaoItem()
    {
        return AutorizacaoexecucaoItens::find($this->itemable_id);
    }
}
