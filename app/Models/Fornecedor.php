<?php

namespace App\Models;

use App\Http\Traits\Formatador;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fornecedor extends Model
{
    use CrudTrait;
    use Formatador;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'fornecedores';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function buscaFornecedorPorNumero($numero)
    {
        $Numeroformatado = $numero === 'ESTRANGEIRO' ? 'ESTRANGEIRO' : $this->formataCnpjCpf($numero);
        $fornecedor = Fornecedor::where('cpf_cnpj_idgener', $Numeroformatado)->first();
        return $fornecedor;
    }

    public function getNomeFornecedorCompleto()
    {
        return $this->cpf_cnpj_idgener.' - '.$this->nome;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function users()
    {
        return $this->belongsToMany(
            BackpackUser::class,
            'users_fornecedores',
            'fornecedor_id',
            'user_id',
        )->withPivot('administrador_fornecedor');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
