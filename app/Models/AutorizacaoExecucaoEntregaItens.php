<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\Formatador;

class AutorizacaoExecucaoEntregaItens extends Model
{
    use CrudTrait;
    use Formatador;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'autorizacaoexecucao_entrega_itens';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    //protected $guarded = ['id'];

    protected $fillable = [
        'autorizacao_execucao_entrega_id',
        'autorizacao_execucao_itens_id',
        'quantidade_informada',
        'valor_glosa',
        'mes_ano_competencia',
        'processo_sei',
        'data_inicio',
        'data_fim',
        'horario',
        'local',
        'ocorrencia',
        'requisitante',
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function entrega()
    {
        return $this->belongsTo(AutorizacaoExecucaoEntrega::class, 'autorizacao_execucao_entrega_id');
    }

    public function itens()
    {
        return $this->belongsTo(AutorizacaoexecucaoItens::class, 'autorizacao_execucao_itens_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getQuantidadeInformadaFormatadoAttribute($value)
    {
        if ($this->itens->tipo_item == 'Serviço') {
            return str_replace('.', ',', rtrim($this->quantidade_informada, '0'));
        } else {
            return (int) $this->quantidade_informada;
        }
    }

    public function getTipoMaterialOsEntregaAttribute($value)
    {
        return $this->itens->saldohistoricoitem->getTipoItem();
    }

    public function getTipoItemAttribute($value)
    {
        return $this->itens->saldohistoricoitem->getContratoItem();
    }

    public function getValorUnitarioItemAttribute($value)
    {
        return $this->itens->saldohistoricoitem->formatValorUnitarioItem();
    }

    public function getValorGlosaFormatadoAttribute()
    {
        return $this->retornaCampoFormatadoComoNumero($this->valor_glosa, true, 2);
    }

    public function getValorTotalItemAttribute($value)
    {
        $valor = ($this->quantidade_informada * $this->itens->valor_unitario_sem_formatar) - $this->valor_glosa;

        return $this->retornaCampoFormatadoComoNumero($valor, true, 2);
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
