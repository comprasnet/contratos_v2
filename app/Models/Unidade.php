<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Database\Seeders\MunicipiosSeeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

class Unidade extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected static $logFillable = true;
    protected static $logName = 'unidade';

    protected $table = 'unidades';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = [
        'orgao_id',
        'codigo',
        'gestao',
        'codigosiasg',
        'nome',
        'nomeresumido',
        'telefone',
        'tipo',
        'situacao',
        'sisg',
        'municipio_id',
        'esfera',
        'poder',
        'tipo_adm',
        'aderiu_siasg',
        'utiliza_siafi',
        'codigo_siorg',
        'sigilo',
        'cnpj'

    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function buscaUnidadeExecutoraPorCodigo($codigo)
    {
        $unidade = $this->where('codigo', $codigo)
            ->where('tipo', 'E')
            ->first();
        return $unidade->id;
    }

    public function getOrgao()
    {
        if ($this->orgao_id) {
            $orgao = Orgao::find($this->orgao_id);
            return $orgao->codigo . " - " . $orgao->nome;
        }

        return '';
    }

    public function getTipo()
    {

        if ($this->tipo == 'E') {
            $tipo = "Executora";
        }

        if ($this->tipo == 'C') {
            $tipo = "Controle";
        }

        if ($this->tipo == 'S') {
            $tipo = "Setorial Contábil";
        }

        return $tipo;
    }

    public function getMunicipio()
    {
        if (!$this->municipio_id) {
            return '';
        }
        return $this->municipio->nome;
    }

    public function getUF()
    {
        if (!$this->municipio_id) {
            return '';
        }
        return $this->municipio->estado->sigla;
    }

    public function serializaPNCP()
    {
        return [
            'codigoIBGE' => @$this->municipio->codigo_ibge,
            'codigoUnidade' => $this->codigo,
            'nomeUnidade' => $this->nome
        ];
    }

    public function exibicaoCompletaUASG()
    {
        return "{$this->codigo} - {$this->nomeresumido}";
    }

    /**
     * Obtém as esferas de unidade de forma dinâmica
     *
     * @return Object Collection com as esferas
     */
    public function getEsferas()
    {
        return $this->distinct()->whereNotNull('esfera')->where('esfera', '<>', '')->pluck('esfera');
    }

    /**
     * Obtém as esferas de unidade de forma fixa
     * Este método foi necessário, pois há bases onde não há unidades com todas as esferas.
     * Quando for necessário exibir todas as esferas mesmo não tendo todas na base, este método retorna todas possíveis.
     *
     * @return Object Collection com as esferas
     */
    public function getEsferasStatic()
    {
        $esferas = [
            'Federal',
            'Estadual',
            'Distrital',
            'Municipal'
        ];

        // É retornado em collection para trabalhar com o mesmo tipo de resultado do método getEsferas()
        $collection = new Collection();
        return $collection->merge($esferas);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function compras()
    {
        return $this->hasMany(Siasgcompra::class, 'unidade_id');
    }

    public function configuracao()
    {
        return $this->hasOne(Unidadeconfiguracao::class, 'unidade_id');
    }

    public function contratos()
    {
        return $this->hasMany(Contrato::class, 'unidade_id');
    }

    public function minuta_empenhos()
    {
        return $this->hasMany(MinutaEmpenho::class);
    }
    public function municipio()
    {
        return $this->belongsTo(Municipio::class);
    }
    public function orgao()
    {
        return $this->belongsTo(Orgao::class, 'orgao_id');
    }

    public function users()
    {
        return $this->belongsToMany(BackpackUser::class, 'unidadesusers', 'unidade_id', 'user_id');
    }

    public function user()
    {
        return $this->hasOne(BackpackUser::class, 'ugprimaria');
    }

    public function compraItem()
    {
        return $this->belongsToMany(
            'App\Models\Unidade',
            'compra_item_unidade',
            'compra_item_id',
            'unidade_id'
        );
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getUnidadeCodigoNomeresumidoAttribute()
    {
        return "{$this->codigo} - {$this->nomeresumido}";
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setCertificadoPathAttribute($value)
    {
        $attribute_name = "certificado_path";
        $disk = "local";
        $destination_path = "certificados/" . $this->codigo_unidade;

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);

        // return $this->attributes[{$attribute_name}]; // uncomment if this is a translatable field
    }
}
