<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class AutorizacaoexecucaoItensHistorico extends AutorizacaoexecucaoItens
{
    use HasFactory;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'autorizacaoexecucao_itens_historico';

    protected $fillable = [
        'autorizacaoexecucao_historico_id',
        'contratohistorico_id',
        'saldohistoricoitens_id',
        'autorizacaoexecucao_itens_id',
        'unidade_medida_id',
        'especificacao',
        'quantidade',
        'parcela',
        'horario',
        'horario_fim',
        'subcontratacao',
        'valor_unitario',
        'numero_demanda_sistema_externo',
        'localidade_de_execucao',
        'tipo_historico',
        'periodo_vigencia_inicio',
        'periodo_vigencia_fim',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function itensEntrega(): HasManyThrough
    {
        return $this->hasManyThrough(
            AutorizacaoExecucaoEntregaItens::class,
            AutorizacaoexecucaoItens::class,
            'id',
            'autorizacao_execucao_itens_id',
            'autorizacaoexecucao_itens_id',
            'id'
        );
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
