<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class CompraItemMinutaEmpenho extends Model
{
    use CrudTrait;
    use LogsActivity;
//    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */


    protected $table = 'compra_item_minuta_empenho';
    protected $guarded = [
        //
    ];

    protected $fillable = [
        'compra_item_id',   // Chave composta: 1/3
        'minutaempenho_id', // Chave composta: 2/3
        'subelemento_id',
        'operacao_id',
        'remessa', // Chave composta: 3/3
        'quantidade',
        'valor',
        'minutaempenhos_remessa_id',
        'numseq'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function retornaItensMinuta($id_minuta) {

        $itens = $this->select(
            'compra_item_minuta_empenho.compra_item_id',
            'compra_item_minuta_empenho.quantidade',
            'compra_item_minuta_empenho.valor',
            'compra_item_minuta_empenho.numseq as sequencial_siafi',
            'codigoitens.descres as tipo_operacao',
            'compra_items.numero as numero_item_compra',
            'catmatseritens.codigo_siasg as codigo_item',
            'catmatseritens.descricao',
            'compra_item_fornecedor.valor_unitario as valorunitario',
            'compra_items.descricaodetalhada as descricao_detalhada')
            ->join('compra_items', 'compra_items.id', 'compra_item_minuta_empenho.compra_item_id')
            ->join('catmatseritens', 'catmatseritens.id', 'compra_items.catmatseritem_id')
            ->join('codigoitens', 'codigoitens.id', 'compra_item_minuta_empenho.operacao_id')
            ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', 'compra_item_minuta_empenho.compra_item_id')
            ->where('compra_item_minuta_empenho.minutaempenho_id', $id_minuta)
            ->whereIn('codigoitens.descres', ['INCLUSAO'])
            ->distinct()->get();
        
        return $itens;

    }

    public function itemAPI($saldo_quantidade_item,$saldo_valor_total_item)
    {
        return [
            'sequencial_siafi' => $this->sequencial_siafi,
            'numero_item_compra' => $this->numero_item_compra,
            'codigo_item' => $this->codigo_item,
            'descricao' => $this->descricao,
            'descricao_detalhada' => $this->descricao_detalhada,
            'quantidade' => number_format($saldo_quantidade_item, 5, ',', '.'),
            'valor_unitario' => number_format($this->valorunitario, 4, ',', '.'),
            'valor_total' => number_format($saldo_valor_total_item, 2, ',', '.')
        ];
    }        

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function compra_item()
    {
        return $this->belongsTo(CompraItem::class, 'compra_item_id');
    }

    public function minutaempenho()
    {
        return $this->belongsTo(MinutaEmpenho::class, 'minutaempenho_id');
    }

    public function subelemento()
    {
        return $this->belongsTo(Naturezasubitem::class, 'subelemento_id');
    }

    public function minutaempenhos_remessa()
    {
        return $this->belongsTo(MinutaEmpenhoRemessa::class, 'minutaempenhos_remessa_id');
    }

    public function operacao()
    {
        return $this->belongsTo(Codigoitem::class, 'operacao_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getSituacaoRemessaAttribute(): string
    {
        return $this->minutaempenhos_remessa->situacao->descricao;
    }

    public function getOperacaoAttribute(): string
    {
        return $this->operacao()->first()->descricao;
    }

    public function getOperacaoDescresAttribute(): string
    {
        return $this->operacao()->first()->descres;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
