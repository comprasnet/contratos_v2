<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelSignatario extends Model
{
    use HasFactory;
    use CrudTrait;

    protected $table = 'models_signatarios';

    protected $fillable = [
        'model_autorizacaoexecucao_id',
        'model_autorizacaoexecucao_historico_id',
        'model_autorizacaoexecucao_entrega_trp_id',
        'model_autorizacaoexecucao_entrega_trd_id',
        'model_termo_recebimento_provisorio_id',
        'model_termo_recebimento_definitivo_id',
        'signatario_contratoresponsavel_id',
        'signatario_contratopreposto_id',
        'status_assinatura_id',
        'arquivo_generico_id',
        'posicao_x_assinatura',
        'posicao_y_assinatura',
        'pagina_assinatura',
        'data_operacao',
    ];
}
