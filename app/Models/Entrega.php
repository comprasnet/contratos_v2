<?php

namespace App\Models;

use App\Http\Traits\EntregaTrpTrd\EntregaTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Entrega extends Model
{
    use EntregaTrait;

    protected $fillable = [
        'contrato_id',
        'situacao_id',
        'numero',
        'rascunho',
        'valor_total',
        'mes_ano_referencia',
        'data_inicio',
        'data_fim',
        'data_entrega',
        'data_prevista_trp',
        'data_prevista_trd',
        'informacoes_complementares',
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function anexos()
    {
        return $this->morphMany(ArquivoGenerico::class, 'arquivoable');
    }

    public function autorizacoes()
    {
        return $this->belongsToMany(
            AutorizacaoExecucao::class,
            'entrega_autorizacaoexecucao',
            'entrega_id',
            'autorizacaoexecucao_id'
        );
    }

    public function itens(): HasMany
    {
        return $this->hasMany(EntregaItem::class);
    }

    public function autorizacaoItens(): MorphToMany
    {
        return $this->morphedByMany(AutorizacaoexecucaoItens::class, 'itemable', 'entrega_itens')
            ->withPivot('id', 'quantidade_informada', 'valor_glosa');
    }

    public function locaisExecucao()
    {
        return $this->morphToMany(
            ContratoLocalExecucao::class,
            'modelable',
            'locais_execucao',
        );
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getSituacaoChipComponent()
    {
        switch ($this->situacao->descres) {
            case 'entrega_status_1':
                $cor = '#a3a3a3';
                $corFont = '#fff';
                break;
            case 'entrega_status_2':
                $cor = '#f5a623';
                $corFont = '#fff';
                break;
            case 'entrega_status_3':
                $cor = '#168821';
                $corFont = '#fff';
                break;
            case 'entrega_status_4':
                $cor = '#1351b4';
                $corFont = '#fff';
                break;
            default:
                $cor = '#000';
                $corFont = '#fff';
                break;
        }

        return view(
            'components_v2.chip_status',
            [
                'conteudo' => $this->situacao->descricao,
                'cor' => $cor,
                'corFont' => $corFont
            ]
        );
    }

    public function getLocaisExecucao()
    {
        $locais = [];

        foreach ($this->locaisExecucao as $localExecucao) {
            $locais[] = $localExecucao->descricao;
        }

        return implode(', ', $locais);
    }


    public function getMesAnoReferencia()
    {
        if (empty($this->mes_ano_referencia)) {
            return '';
        }

        return Carbon::parse($this->mes_ano_referencia)->format('m/Y');
    }

    public function getRedirectAutorizacoesComponent()
    {
        return view(
            'crud::columns.redirect-autorizacoes',
            [
                'autorizacoes' => $this->autorizacoes()->orderBy('created_at')->get()
            ]
        );
    }

    public function getViewButtons($crud)
    {
        if ($this->situacao->descres == 'entrega_status_1') {
            $crud->allowAccess('update');
            $crud->allowAccess('delete');
        } else {
            $crud->denyAccess('update');
            $crud->denyAccess('delete');
        }

        if ($crud->isUsuarioResponsavelContrato && $this->situacao->descres == 'entrega_status_3') {
            return view(
                'crud::buttons.button_trp',
                ['prependIcon' => false, 'link' => '/trp/' . $this->contrato_id . '/'. $this->getKey()]
            );
        }
    }

    public function getTableItensView()
    {
        $itens = $this->autorizacaoItens()->with('autorizacaoexecucao')->get();

        return view('crud::autorizacaoexecucao.vinculo.table-vinculo-itens', [
            'itens' => $itens,
        ])->render();
    }

    public function getAnexosViewLink()
    {
        $codigoItem = CodigoItem::where('descres', 'ae_entrega_arquivo')->first();
        $query = $this->anexos()->where('tipo_id', $codigoItem->id);
        if ($query->exists()) {
            return view('crud::autorizacaoexecucao.entrega.table-anexos', [
                'anexos' => $query->get(),
            ])->render();
        }
        return '';
    }
}
