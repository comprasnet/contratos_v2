<?php

namespace App\Models;

use App\Http\Traits\Arp\ArpTrait;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class ArpRemanejamento extends Model
{
    use CrudTrait;
    use SoftDeletes;
    use ArpTrait;
    use LogsActivity;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'arp_remanejamento';
    protected static $logFillable = true;
    protected static $logName = 'arp_remanejamento';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable =
        [
            'sequencial',
            'arp_id',
            'situacao_id',
            'unidade_solicitante_id',
            'rascunho',
            'remanejamento_entre_unidade_estado_df_municipio_distinto',
            'fornecedor_de_acordo_novo_local',
            'data_envio_solicitacao'
        ];
    // protected $hidden = [];
    // protected $dates = [];

    protected $casts = [
        'remanejamento_entre_unidade_estado_df_municipio_distinto' => 'boolean',
        'fornecedor_de_acordo_novo_local' => 'boolean'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Método responsável em formatar o sequencial com base na coluna rascunho
     * Se o valor for false, então o remanejamento foi solicitado
     * Se o valor for true, então está em rascunho
     */
    public function getNumeroSolicitacao()
    {
        # Formata o sequencial com cinco dígitos, preenchendo com zero a esquerda
        $sequencialFormatado = str_pad($this->sequencial, 5, '0', STR_PAD_LEFT);
        $anoSolicitacao = $this->created_at->format('Y');

        $sequencialFormatado = "{$sequencialFormatado}/{$anoSolicitacao}";

        if ($this->rascunho) {
            return $sequencialFormatado . "-R";
        }

        return $sequencialFormatado;
    }

    /**
     * Método responsável em exibir o número da ata
     */
    public function getNumeroAta()
    {
        return $this->arp->getNumeroAno();
    }

    /**
     * Método responsável em exibir o número da compra
     */
    public function getNumeroCompra()
    {
        return $this->arp->getNumeroCompra();
    }

    /**
     * Método responsável em exibir a modalidade da compra
     */
    public function getModalidadeCompra()
    {
        return $this->arp->getModalidadeCompra();
    }

    /**
     * Método responsável em exibir a unidade de origem (quem solicitou o remanejamento)
     */
    public function getUnidadeDestino()
    {
        return $this->unidadeDestino->exibicaoCompletaUASG();
    }

    /**
     * Método responsável em exibir a situação do remanejamento
     */
    public function getSituacao()
    {
        return $this->situacao ? $this->situacao->descricao : null;
    }


    /**
     * Método responsável em exibir o o texto do campo
     *
     * O remanejamento está sendo feito entre
     *  unidades de estado, distrito federal ou
     * município distintos
     */
    public function getRemanejamentoEntreUnidadeEstadoDfMunicipioDistinto()
    {

        if ($this->remanejamento_entre_unidade_estado_df_municipio_distinto === null) {
            return '';
        }

        if ($this->remanejamento_entre_unidade_estado_df_municipio_distinto) {
            return 'Sim';
        }

        return 'Não';
    }

    /**
     * Método responsável em exibir o campo
     *
     * O remanejamento está sendo feito entre
     *  unidades de estado, distrito federal ou
     * município distintos
     */
    public function getFornecedorDeAcordoNovoLocal()
    {

        if ($this->fornecedor_de_acordo_novo_local === null) {
            return '';
        }

        if ($this->fornecedor_de_acordo_novo_local) {
            return 'Sim';
        }

        return 'Não';
    }

    /**
     * Método responsável em exibir os botões de ação para o usuário
     */
    public function getViewButtonCancelar($crud)
    {
        # Se ainda for rascunho, habilita os botões de deletar e rascunho
        if ($this->rascunho) {
            $crud->allowAccess('update');
            $crud->allowAccess('delete');

            return null;
        }

        # Bloqueia os botões de edição e deletar
        $crud->denyAccess('update');
        $crud->denyAccess('delete');

        $botaoRetificar = null;

        # Recupera a situação do remanejamento
        $situacao = $this->getSituacao();

        # O status do remanejamento que devem exibir o botão de cancelamento
        $arrayStatusAceitaCancelamento = [
            'Aguardando aceitação da unidade participante',
            'Aguardando aceitação da unidade gerenciadora',
            'Aceita Parcialmente',
            'Aceita',
            'Analisado pela unidade gerenciadora da ata',
            'Aguardando aceitação'
        ];

        # Se a situação for uma do array, então exibe o botoão para o usuário
        if (in_array($situacao, $arrayStatusAceitaCancelamento)) {
            $botaoRetificar =
                "<a
                class='btn btn-sm btn-link'
                href='#'
                data-toggle='tooltip'
                title='Cancelar'
                onclick='cancelarRemanejamento({$this->id})'>
                    <i class='fas fa-ban'></i>
            </a>";
        }

        return $botaoRetificar;
    }
    
    public function getUnidadeGerenciadoraAta()
    {
        return $this->arp->getUnidadeOrigem();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function unidadeDestino()
    {
        return $this->belongsTo(Unidade::class, "unidade_solicitante_id");
    }

    public function situacao()
    {
        return $this->belongsTo(CodigoItem::class, "situacao_id");
    }

    public function arp()
    {
        return $this->belongsTo(Arp::class, "arp_id");
    }

    /**
     * Relacionamento com a tabela arquivo_generico
     */
    public function arpRemanejamentoArquivo()
    {
        return $this->morphOne(ArquivoGenerico::class, 'arquivoable');
    }

    /**
     * Relacionamento necessário para que seja exibido na tela de rascunho
     * o valor do filtro no campo Número/Ano da Compra
     */
    public function compra()
    {
        return $this->belongsTo(Compras::class);
    }

    /**
     * Relacionamento necessário para que seja exibido na tela de rascunho
     * o valor do filtro no campo Modalidade da Compra
     */
    public function modalidade()
    {
        return $this->belongsTo(CodigoItem::class);
    }

    /**
     * Relacionamento necessário para que seja exibido os itens selecionado para o remanejamento
     */
    public function itemRemanejamento()
    {
        return $this->hasMany(ArpRemanejamentoItem::class, 'remanejamento_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
