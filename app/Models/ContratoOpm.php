<?php

namespace App\Models;

use App\Http\Traits\Formatador;
use App\Repositories\Contrato\ContratoResponsavelRepository;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class ContratoOpm extends Model
{
    use HasFactory;
    use CrudTrait;
    use LogsActivity;
    use Formatador;
    use SoftDeletes;


    protected $table = 'contrato_opm';
    protected $dates = ['data_assinatura','deleted_at'];

    protected $fillable = [
        'contrato_id',
        'quantidade_familiar',
        'quantidade_contratadas',
        'sequencial',
        'sequencia_ano',
        'situacao_id',
        'percentual_minimo',
        'rascunho',
        'data_cancelamento',
        'justificativa_cancelamento',
        'unidade_id_cancelamento',
        'usuario_id',
        'data_assinatura',
        'contrato_responsavel_id',
    ];
    protected $casts = [
        'percentual_minimo' => 'boolean',
        'rascunho' => 'boolean',

    ];
    public function getSequencial(): string
    {
        $anoAtual = date('Y');
        $ultimoSequencial = self::whereYear('created_at', $anoAtual)->max('sequencial');
        $proximoSequencial = $ultimoSequencial ? $ultimoSequencial + 1 : 1;
        return $proximoSequencial;
    }
    public function getSituacao()
    {
        return $this->situacao->descricao;
    }

    public function getFormataSequencial(): string
    {
        $sequencial = $this->sequencial;
        if ($sequencial === null) {
            return '';
        }

        if ($this->rascunho) {
            return $sequencial . " - R";
        }

        return $sequencial;
    }
    public function getNumeroSolicitacao(): string
    {
        return $this->getFormataSequencial();
    }
    public function getUsuarioCancelamento()
    {
        if ($this->usuarioCancelamento) {
            return $this->retornaMascaraCpf($this->usuarioCancelamento->cpf) . " - " . $this->usuarioCancelamento->name;
        } else {
            return 'Usuário não especificado';
        }
    }
    public function getUsuario()
    {
        if ($this->usuario) {
            return $this->retornaMascaraCpf($this->usuario->cpf) . " - " . $this->usuario->name;
        } else {
            return 'Usuário não especificado';
        }
    }




    public function getContrato()
    {
        return $this->contrato->numero;
    }
    public function contratoResponsavel()
    {
        return $this->belongsTo(Contratoresponsavel::class, 'contrato_responsavel_id');
    }
    public function getResponsavelContrato()
    {
        if ($this->contratoResponsavel()->withTrashed()->first()) {
            $user = $this->contratoResponsavel()->withTrashed()->first()->user;
            return $user ? $this->retornaMascaraCpf($user->cpf) . ' - ' . $user->name : '';
        }
        return '';
    }



    # Método responsável em exibir os botões de ações para o usuário conforme o status da ata
    public function getViewButtonDeclaracao($crud)
    {
        if ($this->rascunho) {
            $crud->allowAccess('update');
            $crud->allowAccess('delete');

            return null;
        }

        $crud->denyAccess('update');
        $crud->denyAccess('delete');


        $botaoRelatorio = '<a class="btn btn-sm btn-link"
                     href="' . route('buscarPDF', ['contrato_id' => $this->contrato->id, 'id' => $this->id]) . '"
                     data-toggle="tooltip" title="Download da Declaração">
                     <i class="fas fa-download"></i></a>';


        return $botaoRelatorio;
    }


    public function getFornecedor()
    {
        return $this->contrato->getFornecedor();
    }

    public function contrato()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }

    public function situacao()
    {
        return $this->belongsTo(CodigoItem::class, 'situacao_id');
    }
    public function usuarioCancelamento()
    {
        return $this->belongsTo(BackpackUser::class, 'usuario_id_cancelamento');
    }
    public function usuario()
    {
        return $this->belongsTo(BackpackUser::class, 'usuario_id');
    }
}
