<?php

namespace App\Models;

use App\Http\Traits\Arp\ArpTrait;
use App\Http\Traits\Formatador;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

class Adesao extends Model
{
    use CrudTrait;
    use ArpTrait;
    use SoftDeletes;
    use LogsActivity;
    use Formatador;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'arp_solicitacao';
    
    protected static $logFillable = true;
    protected static $logName = 'arp_solicitacao';
    
    // protected $primaryKey = 'id';
    // public $timestamps = false;

    // protected $guarded = ['id'];
    protected $fillable = [
        'unidade_origem_id', 'compra_id', 'status',
        'sequencial','ano', 'processo_adesao', 'texto_justificativa',
        'justificativa', 'demonstracao','demonstracao_valores_registrados',
        'consulta_aceitacao_fornecedor', 'aceitacao','rascunho',
        'responsavel_id', 'unidade_gerenciadora_id',
        'aquisicao_emergencial_medicamento_material',
        'execucao_descentralizada_programa_projeto_federal',
        'data_envio_analise',
        'data_aprovacao_analise',
        'justificativa_item_isolado',
        'texto_justificativa_item_isolado',
        'ata_enfrentando_impacto_decorrente_calamidade_publica',
        'justificativa_anexo_comprovacao_execucao_descentralizada_progra',
        'anexo_comprovacao_execucao_descentralizada_programa_projeto_fed',
        'ciente_analise_entre_projeto_federal'
    ];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = [
        'demonstracao' => 'array',
        'aceitacao' => 'array',
        'justificativa_item_isolado' => 'array',
        'consulta_aceitacao_fornecedor' => 'boolean',
        'demonstracao_valores_registrados' => 'boolean',
        'aquisicao_emergencial_medicamento_material' => 'boolean',
        'execucao_descentralizada_programa_projeto_federal' => 'boolean',
        'ata_enfrentando_impacto_decorrente_calamidade_publica' => 'boolean'
    ];


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getViewButtonAdesao($crud, $id = 0)
    {
        if (!$this->rascunho) {
            $crud->denyAccess('update');
            $crud->denyAccess('delete');

            return null;
        }
        $crud->allowAccess('update');
        $crud->allowAccess('delete');

        return ;
    }
    public function getNumeroGera()
    {
        return $this->item[0]->getNumeroGeral();
    }

    public function getViewButtonAnalise($crud, $id = 0)
    {
        if ($this->situacao->descricao == 'Enviada para aceitação') {
            $crud->allowAccess('update');

            return ;
        }

        $crud->denyAccess('update');

        return ;
    }

    public function getNumeroSolicitacao()
    {
        return $this->sequencialFormatado($this->sequencial, $this->ano, $this->rascunho);
    }

    public function getUnidadeSolicitante()
    {
        return $this->unidades->codigo." - ".$this->unidades->nomeresumido;
    }

    public function getUnidadeGerenciadora()
    {
        return $this->unidade_gerenciadora->codigo." - ".$this->unidade_gerenciadora->nomeresumido;
    }

    public function getNumeroCompra()
    {
        return $this->compra->numero_ano;
    }


    public function getModalidade()
    {
        return $this->compra->getModalidade();
    }

    public function getButtonUpload()
    {
        return '<a href="'.url('arp/adesao/item/'.$this->id).'"
                class="btn btn-link"><i class="fas fa-list" title="Gerenciar item"></i></a>';
    }

    public function getSituacao()
    {
        return $this->situacao->descricao;
    }


    public function getSituacaoPersonalizada()
    {
        $uri = $_SERVER['REQUEST_URI'] ?? null;
        if (strpos($uri, 'arp/adesao') === false) {
            return;
        }

        $descricao = $this->situacao->descricao;
        $cor = '';
        $largura = '';
        $paddingT = '';
        $paddingB = '';
        $paddingR = '';
        $paddingL = '';
        $corTile = '';
        switch ($descricao) {
            case 'Aceita':
                $cor = '#168821';
                $largura = 'calc(100% + 53%)';
                $paddingT = '7px';
                $paddingB = '7px';
                $paddingR = '40px';
                $paddingL = '40px';
                $corTile = '#fff';
                break;
            case 'Aceito Parcial':
                $descricao = 'Aceita parcial';
                $cor = '#389718d6';
                $paddingT = '7px';
                $paddingB = '7px';
                $paddingR = '16px';
                $paddingL = '16px';
                $corTile = '#fff';
                $largura = 'calc(100% + 50%)';
                break;
            case 'Negada':
                $cor = '#e52207';
                $paddingT = '7px';
                $paddingB = '7px';
                $paddingR = '34px';
                $paddingL = '34px';
                $corTile = '#fff';
                $largura = 'calc(100% + 55%)';
                break;
            case 'Cancelada':
                $cor = '#000000';
                $paddingT = '7px';
                $paddingB = '7px';
                $paddingR = '25px';
                $paddingL = '25px';
                $corTile = '#fff';
                $largura = 'calc(100% + 23%)';
                break;
            case 'Em elaboração':
                $cor = '#a3a3a3';
                $paddingT = '7px';
                $paddingB = '7px';
                $paddingR = '25px';
                $paddingL = '25px';
                $corTile = '#fff';
                $largura = 'calc(100% + 24%)';
                break;
            case 'Enviada para aceitação':
                $cor = '#ffcd07';
                $paddingT = '7px';
                $paddingB = '7px';
                $paddingR = '16px';
                $paddingL = '16px';
                $corTile = '#fff';
                break;
            case 'Aguardando Aceitação do Fornecedor':
                $cor = 'orange';
                $paddingT = '7px';
                $paddingB = '7px';
                $paddingR = '16px';
                $paddingL = '16px';
                $corTile = '#fff';
                break;
            case 'Aceita pelo Fornecedor':
                $cor = '#182a46';
                $paddingT = '7px';
                $paddingB = '7px';
                $paddingR = '16px';
                $paddingL = '16px';
                $corTile = '#fff';
                break;
            case 'Aceita Parcialmente pelo Fornecedor':
                $cor = '#2c5375';
                $paddingT = '7px';
                $paddingB = '7px';
                $paddingR = '16px';
                $paddingL = '16px';
                $corTile = '#fff';
                break;
            case 'Negada pelo Fornecedor':
                $cor = '#e52207';
                $paddingT = '7px';
                $paddingB = '7px';
                $paddingR = '16px';
                $paddingL = '16px';
                $corTile = '#fff';
                break;
            default:
                $cor = '#000';
                $paddingT = '7px';
                $paddingB = '7px';
                $paddingR = '16px';
                $paddingL = '16px';
                $corTile = '#fff';
                break;
        }
        // Retorna a descrição com a cor e borda definidas
        return "<span class='br-tag  small' style='background: $cor;font-weight: bold;width: $largura; display: flex;justify-content: center;border-radius: 50px;   padding-top:$paddingT; padding-bottom:$paddingB;padding-right:$paddingR; padding-left:$paddingL;color: $corTile'>$descricao</span>";
    }


    public function getJustificativa()
    {
        if (empty($this->justificativa)) {
            return ;
        }
        $arrayJustificativa = explode('-', $this->justificativa);
        return '<a target="_blank" href="'.url("storage/$arrayJustificativa[0]").'">'.$arrayJustificativa[1].'</a>';
    }

    public static function getDemonstracao(int $adesao)
    {
        $adesao = Adesao::find($adesao);
        $arraySaida = [];
        $count =0;
        foreach ($adesao->demonstracao as $demonstracao) {
            $arrayArquivo = explode('-', $demonstracao);
            $arraySaida['Nome'][$count] = str_replace("_", " ", $arrayArquivo[1]);
            $arraySaida['Visualizar'][$count] =
            '<a target="_blank" href="'.url("storage/$arrayArquivo[0]").'"><i class="fas fa-eye"></i></a>';
            $count++;
        }

        return $arraySaida;
    }

    public static function getAnuenciaFornecedor(int $adesao)
    {
        $adesao = Adesao::find($adesao);

        return $adesao->anuencia_fornecedor;
    }

    /**
     * Função auxiliar para processar uma lista de arquivos e gerar o array de saída.
     *
     * @param array $arquivos Lista de arquivos a serem processados.
     * @return array Array formatado contendo os nomes e links de visualização dos arquivos.
     */
    private static function processarArquivos(array $arquivos): array
    {
        $arraySaida = [];
        foreach ($arquivos as $index => $arquivo) {
            $arrayArquivo = explode('-', $arquivo);
            $arraySaida['Nome'][$index] = str_replace("_", " ", $arrayArquivo[1]);
            $arraySaida['Visualizar'][$index] =
                '<a target="_blank" href="' . url("storage/$arrayArquivo[0]") . '"><i class="fas fa-eye"></i></a>';
        }

        return $arraySaida;
    }

    /**
     * Obtém a lista de aceitação formatada para a adesão especificada.
     *
     * @param int $adesao ID da adesão.
     * @return array Array formatado contendo os nomes e links de visualização dos arquivos de aceitação.
     */
    public static function getAceitacao(int $adesao): array
    {
        $adesao = Adesao::find($adesao);

        $aceitacao = self::processarArquivos($adesao->aceitacao);

        $nomes = $aceitacao['Nome'];
        $indicesOrdenados = array_keys($nomes);

        usort($indicesOrdenados, function ($a, $b) use ($nomes) {
            return strcmp($nomes[$a], $nomes[$b]);
        });

        $aceitacaoOrdenada = [
            'Nome' => array_values(array_intersect_key($aceitacao['Nome'], array_flip($indicesOrdenados))),
            'Visualizar' => array_values(array_intersect_key($aceitacao['Visualizar'], array_flip($indicesOrdenados))),
        ];

        return $aceitacaoOrdenada;
    }

    /**
     * Obtém a lista de anuência do fornecedor formatada para a adesão especificada.
     *
     * @param int $adesao ID da adesão.
     * @return array Array formatado contendo os nomes e links de visualização dos arquivos de anuência do fornecedor.
     */
    public static function getArrayAnuenciaFornecedor(int $adesao): array
    {
        $adesao = Adesao::find($adesao);
        $arrayAnuenciaFornecedor = [$adesao->anuencia_fornecedor];
        return self::processarArquivos($arrayAnuenciaFornecedor);
    }

    public static function getItemIsolado(int $adesao)
    {
        $adesao = Adesao::find($adesao);
        $arraySaida = [];
        $count =0;
        foreach ($adesao->justificativa_item_isolado as $key => $justItemIsolado) {
            $arrayArquivo = explode('-', $justItemIsolado);
            $arraySaida['Nome'][$count] = str_replace("_", " ", $arrayArquivo[1]);
            $arraySaida['Visualizar'][$count] =
                '<a target="_blank" href="'.url("storage/$arrayArquivo[0]").'"><i class="fas fa-eye"></i></a>';
            $count++;
        }

        return $arraySaida;
    }


    public function getDemonstracaoValor()
    {
        if ($this->demonstracao_valores_registrados) {
            return 'Sim';
        }

        if ($this->demonstracao_valores_registrados === null) {
            return;
        }

        return 'Não';
    }

    public function getAceitacaoValor()
    {
        if ($this->consulta_aceitacao_fornecedor) {
            return 'Sim';
        }

        if ($this->consulta_aceitacao_fornecedor === null) {
            return;
        }

        return 'Não';
    }

    public function getResponsavelAdesao()
    {
        return $this->responsavel->name." - ".$this->responsavel->email;
    }

    public function getExecucaoDescentralizadaProgramaProjetoFederal()
    {
        if ($this->execucao_descentralizada_programa_projeto_federal) {
            return 'Sim';
        }

        if ($this->execucao_descentralizada_programa_projeto_federal === null) {
            return;
        }

        return 'Não';
    }

    public function getCienteAnaliseEntreProjetoFederal()
    {
        if ($this->ciente_analise_entre_projeto_federal) {
            return 'Sim';
        }

        if ($this->ciente_analise_entre_projeto_federal === null) {
            return;
        }

        return 'Não';
    }

    public function getAquisicaoEmergencialMedicamentoMaterial()
    {
        if ($this->aquisicao_emergencial_medicamento_material) {
            return 'Sim';
        }

        if ($this->aquisicao_emergencial_medicamento_material === null) {
            return;
        }

        return 'Não';
    }
    
    public function getUsuarioCancelamentoAdesao()
    {
        return $this->retornaMascaraCpf($this->usuarioCancelamento->cpf)." - ".$this->usuarioCancelamento->name;
    }
    
    public function getUnidadeCancelamentoAdesao()
    {
        return $this->unidadeCancelamento->exibicaoCompletaUASG();
    }
    
    public function getAtaEnfrentandoImpactoDecorrenteCalamidadePublica()
    {
        if ($this->ata_enfrentando_impacto_decorrente_calamidade_publica) {
            return 'Sim';
        }
        
        if ($this->ata_enfrentando_impacto_decorrente_calamidade_publica === null) {
            return;
        }
        
        return 'Não';
    }

    public function getAtasSoliciatao()
    {
        $atas = [];

        $this->item->each(
            function ($item) use (&$atas) {
                $atas[] = $item->item_arp->arp->getNumeroAno();
            }
        );

        $atas = array_unique($atas);

        return implode(', ', $atas);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function unidades()
    {
        return $this->belongsTo(Unidade::class, 'unidade_origem_id');
    }
    public function unidade_gerenciadora()
    {
        return $this->belongsTo(Unidade::class, 'unidade_gerenciadora_id');
    }
    public function situacao()
    {
        return $this->belongsTo(CodigoItem::class, 'status');
    }
    public function compra()
    {
        return $this->belongsTo(Compras::class, 'compra_id');
    }
    public function fornecedor()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_id');
    }
    public function arp()
    {
        return $this->belongsTo(Arp::class, 'arp_id');
    }
    public function fornecedor_compra()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_compra_id');
    }
    public function responsavel()
    {
        return $this->belongsTo(BackpackUser::class, 'responsavel_id');
    }
    
    public function usuarioCancelamento()
    {
        return $this->belongsTo(BackpackUser::class, 'usuario_id_cancelamento');
    }
    
    public function unidadeCancelamento()
    {
        return $this->belongsTo(Unidade::class, 'unidade_id_cancelamento');
    }

    public function item()
    {
        return $this->hasMany(
            AdesaoItem::class,
            'arp_solicitacao_id'
        );
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    private function pastaArquivo($pasta)
    {
        return  "arp/{$this->unidade_gerenciadora->codigo}_{$this->compra->modalidade->descres}_".str_replace("/", "_", $this->compra->numero_ano)."/adesao_solicitacao/{$this->unidades->codigo}/{$pasta}";
    }

    public function montarPastaArquivo($pasta)
    {
        return $this->pastaArquivo($pasta);
    }

    private function salvarColuna(string $attribute_name, array $value)
    {
        $arquivosSalvos = json_decode($this->attributes[$attribute_name], true);
        $arquivosNovos = array_filter($arquivosSalvos, function ($value) {
            return strstr($value, '-') === false;
        });
        $arquivosNovos = array_values($arquivosNovos);

        $arquivosAntigos = array_filter($arquivosSalvos, function ($value) {
            return strstr($value, '-') == true;
        });

        $attribute_value = null;
        foreach ($value as $key => $arquivo) {
            if (!empty($arquivo)) {
                $nomeArquivoFormatado = str_replace(" ", "_", $arquivo->getClientOriginalName());
                $attribute_value[] = $arquivosNovos[$key]."-".$nomeArquivoFormatado;
            }
        }

        if (!empty($attribute_value)) {
            $arquivosAntigos = array_merge($arquivosAntigos, $attribute_value);
        }
        $files_to_clear = request()->get('clear_'.$attribute_name);
        if ($files_to_clear) {
            foreach ($files_to_clear as $one) {
                foreach ($arquivosAntigos as $key => $arquivo) {
                    if (strpos($arquivo, $one) !== false) {
                        unset($arquivosAntigos[$key]);
                    }
                }
            }
        }

        return $this->attributes[$attribute_name] = json_encode($arquivosAntigos);
    }

    public function setJustificativaAttribute($value)
    {
        if (!empty($this->compra)) {
            $attribute_name = "justificativa";
            $destination_path = $this->pastaArquivo($attribute_name);
            $this->uploadFileToDisk($value, $attribute_name, "public", $destination_path);
            $nomeArquivoFormatado = str_replace(" ", "_", $value->getClientOriginalName());

            return $this->attributes[$attribute_name] = $this->attributes[$attribute_name]."-".$nomeArquivoFormatado;
        }
    }


    public function setDemonstracaoAttribute($value)
    {
        if (!empty($this->compra)) {
            $attribute_name = "demonstracao";
            $destination_path = $this->pastaArquivo($attribute_name);

            $this->uploadMultipleFilesToDisk($value, $attribute_name, "public", $destination_path);

            $this->salvarColuna($attribute_name, $value);
        }
    }

    public function setAceitacaoAttribute($value)
    {
        if (!empty($this->compra)) {
            $attribute_name = "aceitacao";

            $destination_path = $this->pastaArquivo($attribute_name);

            $this->uploadMultipleFilesToDisk($value, $attribute_name, "public", $destination_path);

            $this->salvarColuna($attribute_name, $value);
        }
    }
    public function setJustificativaItemIsoladoAttribute($value)
    {
        if (!empty($this->compra)) {
            $attribute_name = "justificativa_item_isolado";

            $destination_path = $this->pastaArquivo($attribute_name);

            $this->uploadMultipleFilesToDisk($value, $attribute_name, "public", $destination_path);

            $this->salvarColuna($attribute_name, $value);
        }
    }
    
    public function getAdesaoAquisicaoMedicamentoMaterialAttribute()
    {
        if ($this->aquisicao_emergencial_medicamento_material ||
            $this->execucao_descentralizada_programa_projeto_federal
        ) {
            return true;
        }
        
        return false;
    }

    public function setAnexoComprovacaoExecucaoDescentralizadaProgramaProjetoFedAttribute($value)
    {
        if (!empty($this->compra)) {
            $attribute_name = "anexo_comprovacao_execucao_descentralizada_programa_projeto_fed";
            $destination_path = $this->pastaArquivo($attribute_name);
            $this->uploadFileToDisk($value, $attribute_name, "public", $destination_path);
            $nomeArquivoFormatado = str_replace(" ", "_", $value->getClientOriginalName());

            return $this->attributes[$attribute_name] = $this->attributes[$attribute_name]."-".$nomeArquivoFormatado;
        }
    }
}
