<?php

namespace App\Models;

use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoTrait;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\Formatador;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class AutorizacaoExecucao extends Model
{

    use Formatador;
    use CrudTrait;
    use BuscaCodigoItens;
    use SoftDeletes;
    use AutorizacaoexecucaoTrait;
    use HasFactory;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'autorizacaoexecucoes';

    protected $fillable = [
        'contrato_id',
        'processo',
        'numero_sistema_origem',
        'tipo_id',
        'situacao_id',
        'numero',
        'data_assinatura',
        'data_vigencia_inicio',
        'data_vigencia_fim',
        'informacoes_complementares',
        'rascunho',
        'originado_sistema_externo'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getTipo()
    {
        if ($this->tipo_id) {
            $tipo = CodigoItem::find($this->tipo_id);

            return $tipo->descricao;
        } else {
            return '';
        }
    }

    public function getAnexosViewLink()
    {
        return $this->generateTableAnexos($this->anexos());
    }

    public function getOriginadoSistemaExterno()
    {
        if ($this->originado_sistema_externo) {
            return 'Sim';
        } else {
            return 'Não';
        }
    }

    public function getViewButtons($crud)
    {
        if (($this->situacao->descres == 'ae_status_1' || $this->situacao->descres == 'ae_status_6')
            && backpack_user()->can('autorizacaoexecucao_editar')) {
            $crud->allowAccess('update');
        } else {
            $crud->denyAccess('update');
        }

        if ($this->situacao->descres == 'ae_status_1' && backpack_user()->can('autorizacaoexecucao_deletar')) {
            $crud->allowAccess('delete');
        } else {
            $crud->denyAccess('delete');
        }
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function anexos()
    {
        return $this->morphMany(ArquivoGenerico::class, 'arquivoable');
    }

    public function arquivo()
    {
        return $this->morphOne(ArquivoGenerico::class, 'arquivoable');
    }

    public function fornecedor()
    {
        return $this->belongsTo(Fornecedor::class, "fornecedor_id");
    }

    public function tipo()
    {
        return $this->belongsTo(CodigoItem::class, 'tipo_id');
    }

    public function contrato()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }

    public function situacao()
    {
        return $this->belongsTo(CodigoItem::class, 'situacao_id');
    }

    // !IMPORTANTE! manter o nome desse método igual ao da model AutorizacaoexecucaoHistorico
    public function autorizacaoexecucaoItens()
    {
        return $this->hasMany(AutorizacaoexecucaoItens::class, "autorizacaoexecucoes_id");
    }

    public function autorizacaoexecucaoUnidadeRequisitantes()
    {
        return $this->hasMany(AutorizacaoexecucaoUnidadeRequisitantes::class, "autorizacaoexecucoes_id");
    }

    public function unidadeRequisitantes()
    {
        return $this->belongsToMany(
            Unidade::class,
            'autorizacaoexecucao_unidade_requisitantes',
            'autorizacaoexecucoes_id',
            'unidade_requisitante_id'
        )->withTimestamps();
    }

    public function autorizacaoexecucaoEmpenhos()
    {
        return $this->hasMany(AutorizacaoexecucaoEmpenhos::class, "autorizacaoexecucoes_id");
    }

    public function empenhos()
    {
        return $this->belongsToMany(
            Empenho::class,
            'autorizacaoexecucao_empenhos',
            'autorizacaoexecucoes_id',
            'empenhos_id'
        )->withTimestamps();
    }

    public function autorizacaoexecucaoHistorico()
    {
        return $this->hasMany(AutorizacaoexecucaoHistorico::class, "autorizacaoexecucoes_id");
    }

    public function entregas()
    {
        return $this->hasMany(AutorizacaoExecucaoEntrega::class, "autorizacaoexecucao_id", "id");
    }

    public function locaisExecucao()
    {
        return $this->belongsToMany(
            ContratoLocalExecucao::class,
            'autorizacaoexecucao_contrato_local_execucao',
            'autorizacaoexecucao_id',
            'contrato_local_execucao_id'
        )->withTimestamps();
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getContrato()
    {
        return $this->contrato->numero;
    }

    public function getFornecedor()
    {
        return $this->contrato->getFornecedor();
    }

    public function getUnidadeRequisitantes()
    {
        $unidades = [];

        foreach ($this->autorizacaoexecucaoUnidadeRequisitantes as $unidadeRequisitante) {
            $unidades[] = $unidadeRequisitante->unidadeRequisitante->getUnidadeCodigoNomeresumidoAttribute();
        }

        return implode(', ', $unidades);
    }

    public function getEmpenhos()
    {
        $unidades = [];

        foreach ($this->autorizacaoexecucaoEmpenhos as $autorizacaoexecucaoEmpenho) {
            $unidades[] = $autorizacaoexecucaoEmpenho->empenho->getNumeroUnidadeAttribute();
        }

        return implode(', ', $unidades);
    }

    public function getLocaisExecucao()
    {
        $locais = [];

        foreach ($this->locaisExecucao as $localExecucao) {
            $locais[] = $localExecucao->descricao;
        }

        return implode(', ', $locais);
    }

    public function getValorTotal()
    {
        $total = AutorizacaoexecucaoItens::where('autorizacaoexecucoes_id', $this->id)
            ->sum(DB::raw('quantidade * parcela * valor_unitario'));

        return $this->retornaCampoFormatadoComoNumero($total, true);
    }

    public function getItens()
    {
        return $this->generateTableItens($this->autorizacaoexecucaoItens);
    }

    public function getHistoricoAlteracoes(): Collection
    {
        $tipoHistoricoAlterar = CodigoItem::where('descres', 'aeh_alterar')->first();
        $situacaoEmExecucaoEAssiando = CodigoItem::whereIn('descres', ['ae_status_2', 'ae_status_7'])->pluck('id');

        return $this->autorizacaoexecucaoHistorico()
            ->whereIn('situacao_id', $situacaoEmExecucaoEAssiando->toArray())
            ->where('tipo_historico_id', $tipoHistoricoAlterar->id)
            ->orderBy('sequencial', 'desc')
            ->get();
    }

    public function getHistoricoAlteracoesViewLink()
    {
        return view('crud::autorizacaoexecucao.table-historico-alteracoes', [
            'historicos' => $this->getHistoricoAlteracoes(),
        ])->render();
    }

    public function getQuantidadeTotalSemFormatacao()
    {
        return AutorizacaoexecucaoItens::where('autorizacaoexecucoes_id', $this->id)
            ->sum(DB::raw('quantidade * parcela'));
    }

    public function getValorTotalSemFormatacao()
    {
        $total = AutorizacaoexecucaoItens::where('autorizacaoexecucoes_id', $this->id)
            ->sum(DB::raw('quantidade * parcela * valor_unitario'));

        return $total;
    }

    public function queryBaseEntregaAtiva()
    {
        return $this->join('autorizacaoexecucao_entrega', 'autorizacaoexecucoes.id', 'autorizacaoexecucao_id')
            ->join('codigoitens', 'codigoitens.id', 'autorizacaoexecucao_entrega.situacao_id')
            ->where('autorizacaoexecucao_entrega.rascunho', false)
            ->where('autorizacaoexecucoes.rascunho', false)
            ->whereNotIn('codigoitens.descres', ['ae_entrega_status_5', 'ae_entrega_status_3', 'ae_entrega_status_9'])
            ->where('autorizacaoexecucoes.id', $this->id);
    }

    public function getValorTotalEntregaAtiva()
    {
        $entregaAtiva = $this->queryBaseEntregaAtiva()->sum('valor_entrega');

        return $entregaAtiva;
    }

    public function getValorTotalGlosaEntregaAtiva()
    {
        $entregaAtiva = $this->queryValorTotalGlosaEntregaAtiva($this->queryBaseEntregaAtiva());

        return $entregaAtiva;
    }

    public function queryValorTotalGlosaEntregaAtiva(Builder $query)
    {
        return $query->join(
            'autorizacaoexecucao_entrega_itens',
            'autorizacaoexecucao_entrega.id',
            'autorizacaoexecucao_entrega_itens.autorizacao_execucao_entrega_id'
        )->sum('valor_glosa');
    }

    public function getQuantidadeTotalEntregaAtiva()
    {
        $entregaAtiva = $this->queryQuantidadeTotalEntregaAtiva($this->queryBaseEntregaAtiva());

        return $entregaAtiva;
    }

    public function queryQuantidadeTotalEntregaAtiva(Builder $query)
    {
        return $query->join(
            'autorizacaoexecucao_itens',
            'autorizacaoexecucoes.id',
            'autorizacaoexecucao_itens.autorizacaoexecucoes_id'
        )->sum('quantidade');
    }

    public function queryQuantidadeTotalEntregaAtivaEntregas(Builder $query)
    {
        return $query->join(
            'autorizacaoexecucao_entrega_itens',
            'autorizacaoexecucao_entrega.id',
            'autorizacaoexecucao_entrega_itens.autorizacao_execucao_entrega_id'
        )->sum('quantidade_informada');
    }

    public function queryBasePorSituacao(array $situacoes)
    {
        return $this->join('autorizacaoexecucao_entrega', 'autorizacaoexecucoes.id', 'autorizacaoexecucao_id')
            ->join('codigoitens', 'codigoitens.id', 'autorizacaoexecucao_entrega.situacao_id')
            ->where('autorizacaoexecucao_entrega.rascunho', false)
            ->where('autorizacaoexecucoes.rascunho', false)
            ->whereIn('codigoitens.descres', $situacoes)
            ->where('autorizacaoexecucoes.id', $this->id);
    }
    //Qtde. em Avaliação (Até TRP) linha 2
    public function getQuantidadeTotalQtdAnaliseAteTRP()
    {
        $situacoes = [
            'ae_entrega_status_1',
            'ae_entrega_status_2',
            'ae_entrega_status_4',
            'ae_entrega_status_6',
        ];

        $entregaAtiva = $this->queryQuantidadeTotalEntregaAtivaEntregas($this->queryBasePorSituacao($situacoes));

        return $entregaAtiva;
    }

    public function getValorTotalGlosaQtdAnaliseAteTRP()
    {
        $situacoes = [
            'ae_entrega_status_1',
            'ae_entrega_status_2',
            'ae_entrega_status_4',
            'ae_entrega_status_6',
        ];

        $entregaAtiva = $this->queryValorTotalGlosaEntregaAtiva($this->queryBasePorSituacao($situacoes));

        return $entregaAtiva;
    }

    public function getValorTotalQtdAnaliseAteTRP()
    {
        $situacoes = [
            'ae_entrega_status_1',
            'ae_entrega_status_2',
            'ae_entrega_status_4',
            'ae_entrega_status_6',
        ];

        $entregaAtiva = $this->queryBasePorSituacao($situacoes)->sum('valor_entrega');

        return $entregaAtiva;
    }

    //Qtde. em Avaliação (Até TRD)
    public function getQuantidadeTotalQtdAnaliseAteTRD()
    {
        $situacoes = [
            'ae_entrega_status_11',
            'ae_entrega_status_8',
            'ae_entrega_status_10'
        ];

        $entregaAtiva = $this->queryQuantidadeTotalEntregaAtivaEntregas($this->queryBasePorSituacao($situacoes));

        return $entregaAtiva;
    }

    public function getValorTotalGlosaQtdAnaliseAteTRD()
    {
        $situacoes = [
            'ae_entrega_status_11',
            'ae_entrega_status_8',
            'ae_entrega_status_10'
        ];

        $entregaAtiva = $this->queryValorTotalGlosaEntregaAtiva($this->queryBasePorSituacao($situacoes));

        return $entregaAtiva;
    }

    public function getValorTotalQtdAnaliseAteTRD()
    {
        $situacoes = [
            'ae_entrega_status_11',
            'ae_entrega_status_8',
            'ae_entrega_status_10'
        ];

        $entregaAtiva = $this->queryBasePorSituacao($situacoes)->sum('valor_entrega');

        return $entregaAtiva;
    }

    //Qtde. Executada (Após TRD) ae_entrega_status_12
    public function getQuantidadeTotalQtdAnaliseAposTRD()
    {
        $situacoes = [
            'ae_entrega_status_12',
            'ae_entrega_status_7'
        ];

        $entregaAtiva = $this->queryQuantidadeTotalEntregaAtivaEntregas($this->queryBasePorSituacao($situacoes));

        return $entregaAtiva;
    }

    public function getValorTotalGlosaQtdAnaliseAposTRD()
    {
        $situacoes = [
            'ae_entrega_status_12',
            'ae_entrega_status_7'
        ];

        $entregaAtiva = $this->queryValorTotalGlosaEntregaAtiva($this->queryBasePorSituacao($situacoes));

        return $entregaAtiva;
    }

    public function getValorTotalQtdAnaliseAposTRD()
    {
        $situacoes = [
            'ae_entrega_status_12',
            'ae_entrega_status_7'
        ];

        $entregaAtiva = $this->queryBasePorSituacao($situacoes)->sum('valor_entrega');

        return $entregaAtiva;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
