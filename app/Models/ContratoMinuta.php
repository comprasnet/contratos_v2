<?php

namespace App\Models;

use App\Models\ModeloDocumento;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;


class ContratoMinuta extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contrato_minutas';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['data','contrato_id','tipo_modelo_documento_id','contrato_minutas_status_id','texto_minuta','codigoitens_id','documento_formatado','qtd_min_assinaturas','numero_termo_contrato'];
    // protected $hidden = [];
    // protected $dates = [];
    protected $contrato_model;

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    function getNumeroContrato(){
        return $this->Contrato->numero;
    }
    
    function getNumeroProcesso()
    {
        return $this->Contrato->processo;
    }
    
    function getTipoDeDocumento()
    {

        if(empty($this->ModeloDocumento)) {
            return $this->TipoDocumento->descricao;
            // return 'Padrão de Documento Não Selecionado';
            return $this->CodigoItem->descricao;
        } else {
            return $this->ModeloDocumento->CodigoItem->descricao;
        }

        // 
    }
    
    function getDescricaoModeloDocumento(){
        
        if(empty($this->ModeloDocumento)) {
            return 'Padrão de Documento Não Selecionado';
        } else {
            return $this->ModeloDocumento->descricao;
        }

    }
    
    function getStatus()
    {
        return $this->Status->descricao;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    function ModeloDocumento(){
        return $this->belongsTo(ModeloDocumento::class,'tipo_modelo_documento_id');
    }
    function Contrato()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }
    function Status()
    {
        return $this->belongsTo(Codigoitem::class, 'contrato_minutas_status_id');
    }

    function TipoDocumento() {
        return $this->belongsTo(Codigoitem::class,'codigoitens_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
