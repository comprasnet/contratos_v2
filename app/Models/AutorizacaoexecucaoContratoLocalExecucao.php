<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AutorizacaoexecucaoContratoLocalExecucao extends Model
{
    use HasFactory;

    protected $table = 'autorizacaoexecucao_contrato_local_execucao';
    public $timestamps = true;
    protected $fillable = [
        'autorizacaoexecucao_id',
        'contrato_local_execucao_id',
        'autorizacaoexecucao_entrega_id'
    ];
}
