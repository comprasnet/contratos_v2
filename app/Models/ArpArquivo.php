<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\HasIdentifiableAttribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArpArquivo extends Model
{
    use HasFactory, CrudTrait, HasIdentifiableAttribute;
   // use SoftDeletes;

    protected $table = 'arp_arquivos';

    protected $fillable = [
        'nome',
        'arp_id',
        'tipo_id',
        'url',
        'descricao',
        'restrito',
    ];

    public function arp()
    {
        return $this->belongsTo(Arp::class, 'arp_id');
    }

    public function tipo()
    {
        return $this->belongsTo(ArpArquivoTipo::class, 'tipo_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getRestrito()
    {
        return $this->restrito ? 'Sim' : 'Não';
    }

    public function enviaDadosPncp()
    {
        return $this->morphOne(EnviaDadosPncp::class, 'pncpable');
    }

    public function getUser(): string
    {
        $user = $this->user()->first();
        if (isset($user)) {
            return $user->name;
        }

        return '-';
    }

    /**
     * Retorna todos os arquivos de uma Ata de Registro de Preços
     * Inclui a descrição do Tipo da Ata
     *
     * @param int $idArp id de uma ata de Registro de Preços
     * @return Illuminate\Support\Collection Retorna os arquivos da Ata especificada
     */
    public static function getArquivosPorAta(int $idArp): \Illuminate\Support\Collection
    {
        return ArpArquivo::join('arp_arquivo_tipos', 'arp_arquivos.tipo_id', '=', 'arp_arquivo_tipos.id')
            ->select(
                'arp_arquivo_tipos.nome as tipo',
                'arp_arquivos.nome',
                'arp_arquivos.created_at',
                'url'
            )
            ->where('arp_id', $idArp)
            ->where('restrito', false)
            #->dd()
            ->get();
    }
}
