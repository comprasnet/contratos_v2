<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

class Empenho extends Model
{
    use CrudTrait;
    use LogsActivity;

    protected static $logFillable = true;
    protected static $logName = 'empenho';
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'empenhos';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'numero',
        'unidade_id',
        'fornecedor_id',
        'planointerno_id',
        'naturezadespesa_id',
        'empenhado',
        'aliquidar',
        'liquidado',
        'pago',
        'rpinscrito',
        'rpaliquidar',
        'rpliquidado',
        'rppago',
        'rp',
        'fonte',
        'informacao_complementar',
        'sistema_origem',
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function inserirEmpenhoMigracaoConta(array $dados)
    {
        $this->fill($dados);
        $this->save();

        return $this;
    }

    public function buscaEmpenhosPorAnoUg(int $ano, int $unidade)
    {
        $empenhos = Empenho::whereHas('unidade', function ($q) use ($unidade) {
            $q->where('codigo', $unidade);
        })
            ->where('numero', 'LIKE', $ano . 'NE%')
            ->get();

        return $empenhos;
    }

    public function buscaEmpenhosPorUg(int $unidade)
    {
        $empenhos = Empenho::whereHas('unidade', function ($q) use ($unidade) {
            $q->where('codigo', $unidade);
        })
            ->get();

        return $empenhos;
    }

    public function getFornecedor()
    {
        $fornecedor = Fornecedor::find($this->fornecedor_id);
        return $fornecedor->cpf_cnpj_idgener . ' - ' . $fornecedor->nome;
    }

    public function getUnidade()
    {
        $unidade = Unidade::find($this->unidade_id);
        return $unidade->codigo . ' - ' . $unidade->nomeresumido;
    }

    public function getNatureza()
    {
        if ($this->naturezadespesa_id) {
            $naturezadespesa = Naturezadespesa::find($this->naturezadespesa_id);
            return $naturezadespesa->codigo . ' - ' . $naturezadespesa->descricao;
        } else {
            return '';
        }
    }

    public function getPi()
    {
        if ($this->planointerno_id) {
            $planointerno = Planointerno::find($this->planointerno_id);
            return $planointerno->codigo . ' - ' . $planointerno->descricao;
        } else {
            return '-';
        }
    }

    public function formatVlrEmpenhado()
    {
        return 'R$ ' . number_format($this->empenhado, 2, ',', '.');
    }

    public function formatVlraLiquidar()
    {
        return 'R$ ' . number_format($this->aliquidar, 2, ',', '.');
    }

    public function formatVlrLiquidado()
    {
        return 'R$ ' . number_format($this->liquidado, 2, ',', '.');
    }

    public function formatVlrPago()
    {
        return 'R$ ' . number_format($this->pago, 2, ',', '.');
    }

    public function formatVlrRpInscrito()
    {
        return 'R$ ' . number_format($this->rpinscrito, 2, ',', '.');
    }

    public function formatVlrRpaLiquidar()
    {
        return 'R$ ' . number_format($this->rpaliquidar, 2, ',', '.');
    }

    public function formatVlrRpLiquidado()
    {
        return 'R$ ' . number_format($this->rpliquidado, 2, ',', '.');
    }

    public function formatVlrRpPago()
    {
        return 'R$ ' . number_format($this->rppago, 2, ',', '.');
    }

    /**
     * Retorna Empenhos e Fontes conforme $conta informada
     *
     * @param string $conta
     * @return array
     */
    public function retornaEmpenhoFontePorConta($conta)
    {
        // Dados de todos os empenhos - em memória
        $empenhos = session('empenho.fonte.conta');
        $pkCount = (is_array($empenhos) ? count($empenhos) : 0);
        if ($pkCount == 0) {
            // Se não houver dados na session, busca os dados no banco
            $empenhos = $this->retornaEmpenhosFonteConta($conta);
            session(['empenho.fonte.conta' => $empenhos]);
        }


        $registrosEncontrados = array_filter($empenhos, function ($empenho) use ($conta) {

            return ($empenho->nd == $conta);
        });


        return $registrosEncontrados;
    }

    /**
     * Retorna conjunto de Empenhos, fonte e conta (nd + subitem) por $ug
     *
     * @return array
     */
    public function retornaEmpenhosFonteConta()
    {
        $ug = session('user_ug_id');

        $sql = '';
        $sql .= 'SELECT ';
        $sql .= '	E.numero AS ne, ';
        $sql .= "	E.fonte AS fonte, ";
        $sql .= '	N.codigo || I.codigo AS nd ';
        $sql .= 'FROM';
        $sql .= '	empenhos AS E ';
        $sql .= 'LEFT JOIN ';
        $sql .= '	empenhodetalhado AS D on ';
        $sql .= '	D.empenho_id = E.id ';
        $sql .= 'LEFT JOIN ';
        $sql .= '	naturezasubitem AS I on ';
        $sql .= '	I.id = D.naturezasubitem_id ';
        $sql .= 'LEFT JOIN ';
        $sql .= '	naturezadespesa AS N on ';
        $sql .= '	N.id = I.naturezadespesa_id ';
        $sql .= 'WHERE ';
        $sql .= '	E.unidade_id = ?';
        $sql .= 'ORDER BY ';
        $sql .= '    nd, ';
        $sql .= '    ne ';

        $dados = DB::select($sql, [$ug]);

        return $dados;
    }

    public function retornaDadosEmpenhosGroupUgArray()
    {

        $unidade = Unidade::find(session()->get('user_ug_id'));

        $valores_empenhos = Empenho::whereHas('unidade', function ($q) use ($unidade) {
            $q->whereHas('orgao', function ($o) use ($unidade) {
                $o->where('id', $unidade->orgao_id);
            });
            $q->where('situacao', '=', true);
        });
        $valores_empenhos->whereHas('naturezadespesa', function ($q) {
            $q->where('codigo', 'LIKE', '33%');
        });
        $valores_empenhos->leftjoin('unidades', 'empenhos.unidade_id', '=', 'unidades.id');
        $valores_empenhos->orderBy('nome');
        $valores_empenhos->groupBy('unidades.codigo');
        $valores_empenhos->groupBy('unidades.nomeresumido');
        $valores_empenhos->select([
            DB::raw("unidades.codigo ||' - '||unidades.nomeresumido as nome"),
            DB::raw('sum(empenhos.empenhado) as empenhado'),
            DB::raw("sum(empenhos.aliquidar) as aliquidar"),
            DB::raw("sum(empenhos.liquidado) as liquidado"),
            DB::raw("sum(empenhos.pago) as pago")
        ]);

        return $valores_empenhos->get()->toArray();
    }

    public function retornaDadosEmpenhosSumArray()
    {

        $valores_empenhos = Empenho::whereHas('unidade', function ($q) {
            $q->where('situacao', '=', true);
        });
        $valores_empenhos->whereHas('naturezadespesa', function ($q) {
            $q->where('codigo', 'LIKE', '33%');
        });
        $valores_empenhos->leftjoin('unidades', 'empenhos.unidade_id', '=', 'unidades.id');
        $valores_empenhos->select([
//            DB::raw("unidades.codigo ||' - '||unidades.nomeresumido as nome"),
            DB::raw('sum(empenhos.empenhado) as empenhado'),
            DB::raw("sum(empenhos.aliquidar) as aliquidar"),
            DB::raw("sum(empenhos.liquidado) as liquidado"),
            DB::raw("sum(empenhos.pago) as pago")
        ]);

        return $valores_empenhos->get()->toArray();
    }
    // public function getTodosEmpenhosByUasgByNumeroEmpenho($uasgEmitente, $numeroEmpenho){

    //     /*

    //     Retorno:

    //     "id":
    //     "contrato_id":
    //     "compra_id":
    //     "numero": "2021NE000024",
    //     "unidade": "170195 - GRA/GO",
    //     "fornecedor": "04.795.101/0001-57 - FENIX ASSESSORIA & GESTAO EMPRESARIAL LTDA",

    //     PROBLEMAS:
    //     "naturezadespesa": "339039 - OUTROS SERVICOS DE TERCEIROS - PESSOA JURIDICA", => minutaempenho não faz relacionamento com naturezadespesa
    //     "empenhado": "54.491,21", => só em em empenhos
    //     "aliquidar": "0,00", => só em em empenhos
    //     "liquidado": "0,00", => só em em empenhos
    //     "pago": "54.491,21", => só em em empenhos
    //     "rpinscrito": "0,00", => só em em empenhos
    //     "rpaliquidar": "0,00", => só em em empenhos
    //     "rpaliquidado": "0,00", => só em em empenhos
    //     "rppago": "0,00" => só em em empenhos


    //     select
    //         m.id as minutaempenho_id, m.contrato_id, m.compra_id, m.mensagem_siafi as minutaempenho_numero,
    //         u.id as unidade_id, u.codigo as unidade_codigo, u.nomeresumido as unidade_nomeresumido,
    //         fc.nome as fornecedor_compra_nome, fc.cpf_cnpj_idgener as fornecedor_compra_cpf_cnpj_idgener,
    //         fe.nome as fornecedor_empenho_nome, fe.cpf_cnpj_idgener as fornecedor_empenho_cpf_cnpj_idgener

    //     from minutaempenhos m
    //     inner    join unidades u on u.id = m.unidade_id
    //     inner    join fornecedores fc on fc.id = m.fornecedor_compra_id
    //     inner    join fornecedores fe on fe.id = m.fornecedor_empenho_id
    //     where    m.mensagem_siafi = '0001NE002021'
    //     and      u.codigo = '070001'
    //     */


    //     $empenhos = Empenho::where('empenhos.numero', $numeroEmpenho)
    //         ->select(

    //                     'minutaempenhos.id as minutaempenho_id')

    //         ->where('empenhos.numero', $numeroEmpenho)
    //         ->join('unidades', 'unidades.id', 'empenhos.unidade_id')
    //         ->join('contratoempenhos', 'contratoempenhos.empenho_id', 'empenhos.id')
    //         ->join('contratos', 'contratos.id', 'contratoempenhos.contrato_id')
    //         ->join('fornecedores', 'fornecedores.id', 'contratos.fornecedor_id')
    //         ->join('naturezadespesa', 'naturezadespesa.id', 'empenhos.naturezadespesa_id')

    //         // minuta empenho do contrato
    //         ->leftjoin('minutaempenhos', 'minutaempenhos.contrato_id', 'contratos.id')

    //         ->where('unidades.codigo', $uasgEmitente)->get();
    //     return $empenhos;


    //     $empenhos = Empenho::where('empenhos.numero', $numeroEmpenho)
    //         ->select(

    //                     'empenhos.id as empenho_id', 'empenhos.numero as empenho_numero', 'empenhos.empenhado as empenho_valor_empenhado',
    //                     'empenhos.aliquidar as empenho_valor_a_liquidar', 'empenhos.pago as empenho_valor_pago', 'empenhos.rpinscrito as empenho_valor_rpinscrito',
    //                     'empenhos.rpaliquidar as empenho_valor_rpaliquidar', 'empenhos.rpliquidado as empenho_valor_rpliquidado', 'empenhos.rppago as empenho_valor_rppago',
    //                     'contratoempenhos.contrato_id as contrato_id',
    //                     'unidades.codigo as unidade_codigo', 'unidades.nomeresumido as unidade_nomeresumido',
    //                     'fornecedores.cpf_cnpj_idgener as fornecedor_cpf_cnpj_idgener', 'fornecedores.nome as fornecedor_nome',
    //                     'naturezadespesa.codigo as naturezadespesa_codigo', 'naturezadespesa.descricao as naturezadespesa_descricao',
    //                     'minutaempenhos.compra_id as minutaempenhos_compra_id', 'minutaempenhos.empenhocontrato as minutaempenho_empenhocontrato')

    //         ->where('empenhos.numero', $numeroEmpenho)
    //         ->join('unidades', 'unidades.id', 'empenhos.unidade_id')
    //         ->join('contratoempenhos', 'contratoempenhos.empenho_id', 'empenhos.id')
    //         ->join('contratos', 'contratos.id', 'contratoempenhos.contrato_id')
    //         ->join('fornecedores', 'fornecedores.id', 'contratos.fornecedor_id')
    //         ->join('naturezadespesa', 'naturezadespesa.id', 'empenhos.naturezadespesa_id')

    //         // minuta empenho do contrato
    //         ->leftjoin('minutaempenhos', 'minutaempenhos.contrato_id', 'contratos.id')

    //         ->where('unidades.codigo', $uasgEmitente)->get();
    //     return $empenhos;
    // }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function unidade()
    {
        return $this->belongsTo(Unidade::class, 'unidade_id');
    }

    public function fornecedor()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_id');
    }

    public function naturezadespesa()
    {
        return $this->belongsTo(Naturezadespesa::class, 'naturezadespesa_id');
    }

    public function planointerno()
    {
        return $this->belongsTo(Planointerno::class, 'planointerno_id');
    }

    public function empenhodetalhado()
    {
        return $this->hasMany(Empenhodetalhado::class, 'empenho_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function getNumeroAliquidarAttribute()
    {
        return $this->numero . ' - ' . $this->aliquidar;
    }

    public function getNumeroUnidadeAttribute()
    {
        return $this->numero . ' - UG: ' . $this->unidade->codigo . ' - ' . $this->unidade->nome .
            ' - Empenhado: ' . $this->formatVlrEmpenhado() . ' - A Liquidar: ' . $this->formatVlraLiquidar() .
            ' - Liquidado: ' . $this->formatVlrLiquidado() . ' - Pago: ' . $this->formatVlrPago();
    }
}
