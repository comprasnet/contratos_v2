<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

class Compras extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'compras';
    
    protected static $logFillable = true;
    protected static $logName = 'compras_v2';
    
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getUnidadeOrigem()
    {
        if (!isset($this->unidadeOrigem->codigo)) {
            return '';
        }

        return $this->unidadeOrigem->codigo . " - " . $this->unidadeOrigem->nomeresumido;
    }

    public function getUnidadeSubrrogada()
    {
        if (!isset($this->unidadeSubrrogada->codigo)) {
            return '';
        }

        return $this->unidadeSubrrogada->codigo . " - " . $this->unidadeSubrrogada->nomeresumido;
    }

    public function getUnidadeBeneficiaria()
    {
        if (!isset($this->unidadeBeneficiaria->codigo)) {
            return '';
        }

        return $this->unidadeBeneficiaria->codigo . " - " . $this->unidadeBeneficiaria->nomeresumido;
    }

    public function getModalidade()
    {
        if (!isset($this->modalidade->descres)) {
            return '';
        }

        return $this->modalidade->descres . " - " . $this->modalidade->descricao;
    }

    public function getTipoCompra()
    {
        if (!isset($this->tipoCompra->descres)) {
            return '';
        }

        return $this->tipoCompra->descricao;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function unidadeOrigem()
    {
        return $this->belongsTo(Unidade::class, 'unidade_origem_id');
    }

    public function unidadeSubrrogada()
    {
        return $this->belongsTo(Unidade::class, 'unidade_subrrogada_id');
    }

    public function unidadeBeneficiaria()
    {
        return $this->belongsTo(Unidade::class, 'uasg_beneficiaria_id');
    }

    public function modalidade()
    {
        return $this->belongsTo(CodigoItem::class, 'modalidade_id');
    }

    public function tipoCompra()
    {
        return $this->belongsTo(CodigoItem::class, 'tipo_compra_id');
    }

    public function item_compra()
    {
        return $this->hasMany(CompraItem::class, "compra_id");
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    # Recupera as informações dos fornecedores para a gestão de atas
//    public static function fornecedorCompra(array $idCompra)
//    {
//        # Recupera os itens que foram cadastrados em todas as atas para não deixar exibir
//        $itensCadastradoArp = ArpItem::groupby("compra_item_fornecedor_id")
//        ->pluck("compra_item_fornecedor_id")->toArray();
//
//        return Compras::join('compra_items', function ($query) {
//            $query->on('compra_items.compra_id', 'compras.id')
//                ->whereNull('compra_items.deleted_at');
//        })
//        ->join('compra_item_unidade', function ($query) {
//            $query->on('compra_item_unidade.compra_item_id', 'compra_items.id')
//            ->where('compra_item_unidade.situacao', true);
//        })
//        ->join('unidades', 'unidades.id', '=', 'compra_item_unidade.unidade_id')
//        ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', 'compra_items.id')
//        ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
//        ->whereIn('compras.id', $idCompra)
//        ->where(function ($query) {
//            $query->where('compras.unidade_origem_id', session('user_ug_id'))
//            ->orWhere('compras.unidade_subrrogada_id', session('user_ug_id'));
//        })
//        // ->where('compras.unidade_origem_id', session('user_ug_id'))
//        ->where('compra_item_unidade.quantidade_saldo', '>', 0)
//        ->whereNotNull('compra_item_unidade.tipo_uasg')
//        // ->whereRaw('compra_item_unidade.fornecedor_id = fornecedores.id')
//        ->whereRaw('compra_item_fornecedor.fornecedor_id = fornecedores.id')
//        ->where(function ($query) {
//            $query->whereNull('compra_items.ata_vigencia_fim')
//            ->orWhere('compra_items.ata_vigencia_fim', '>=', Carbon::now()->toDateString());
//        })
//        ->distinct()
//        ->select([
//            'fornecedores.id', 'fornecedores.nome',
//            'fornecedores.cpf_cnpj_idgener',
//            'compra_item_fornecedor.situacao_sicaf'
//        ])
//        ->orderBy('fornecedores.nome')
//        ->get()
//        ->toArray();
//    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
