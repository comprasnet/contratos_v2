<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArpRemanejamentoItem extends Model
{
    use CrudTrait;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'arp_remanejamento_itens';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable =
    [
        'remanejamento_id',
        'quantidade_solicitada',
        'quantidade_aprovada_participante',
        'data_aprovacao_participante',
        'quantidade_aprovada_gerenciadora',
        'data_aprovacao_gerenciadora',
        'id_item_unidade',
        'id_usuario_solicitante',
        'id_usuario_aprovacao_participante',
        'id_usuario_aprovacao_gerenciadora',
        'situacao_id',
        'status_analise_id',
        'justificativa_analise',
        'id_item_unidade_receber'
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Relacionamento necessário para exibir o item vinculada a unidade
     */
    public function itemUnidade()
    {
        return $this->belongsTo(CompraItemUnidade::class, "id_item_unidade");
    }

    /**
     * Relacionamento necessário para exibir as informações sobre o remanejamento
     */
    public function remanejamento()
    {
        return $this->belongsTo(ArpRemanejamento::class, "remanejamento_id");
    }

    /**
     * Relacionamento necessário para exibir as informações do usuário solicitante
     */
    public function usuarioSolicitante()
    {
        return $this->belongsTo(BackpackUser::class, "id_usuario_solicitante");
    }

    /**
     * Relacionamento necessário para exibir as informações do usuário que analisou por parte da unidade dona do item
     */
    public function usuarioAnaliseUnidadeOrigem()
    {
        return $this->belongsTo(BackpackUser::class, "id_usuario_aprovacao_participante");
    }

    /**
     * Relacionamento necessário para exibir as informações do usuário que analisou por parte da unidade dona do item
     */
    public function usuarioAnaliseUnidadeGestoraAta()
    {
        return $this->belongsTo(BackpackUser::class, "id_usuario_aprovacao_gerenciadora");
    }

    /**
     * Relacionamento necessário para exibir as informações do status
     */
    public function situacaoItem()
    {
        return $this->belongsTo(CodigoItem::class, 'situacao_id');
    }

    /**
     * Relacionamento necessário para exibir o status da análise no item
     */
    public function situacaoAnaliseItem()
    {
        return $this->belongsTo(CodigoItem::class, 'status_analise_id');
    }
    
    public function itemUnidadeReceber()
    {
        return $this->belongsTo(CompraItemUnidade::class, "id_item_unidade_receber");
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
