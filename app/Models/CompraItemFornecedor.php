<?php

namespace App\Models;

use App\Http\Controllers\Arp\CompraItemFornecedorCrudController;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

class CompraItemFornecedor extends Model
{
    use CrudTrait;
    use LogsActivity;

    protected $table = 'compra_item_fornecedor';
    
    protected static $logFillable = true;
    protected static $logName = 'compra_item_fornecedor_v2';
    
    public $timestamps = true;

    protected $fillable = [
        'compra_item_id',
        'fornecedor_id',
        'ni_fornecedor',
        'classificacao',
        'situacao_sicaf',
        'quantidade_homologada_vencedor',
        'valor_unitario',
        'valor_negociado',
        'quantidade_empenhada',
        'ata_vigencia_inicio',
        'ata_vigencia_fim',
        'percentual_maior_desconto',
        'novo_gestao_ata',
        'situacao'
    ];

    public function fornecedor()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_id');
    }
    public function contratosFornecedor()
    {
        return $this->hasMany(Contrato::class, 'fornecedor_id', 'fornecedor_id');
    }

    public function compraItens()
    {
        return $this->belongsTo(CompraItem::class, 'compra_item_id');
    }
    
    public function itemAta()
    {
        return $this->hasMany(ArpItem::class, 'compra_item_fornecedor_id');
    }
    
    public function getFornecedor()
    {
        return $this->fornecedor->cpf_cnpj_idgener . ' - ' . $this->fornecedor->nome;
    }

    public function getTipoItem()
    {
        return $this->compraItens->getTipoItem();
    }

    public function getValorUnitario()
    {
        return number_format($this->valor_unitario, 2, ',', '.');
    }

    public function getValorNegociado()
    {
        return number_format($this->valor_negociado, 2, ',', '.');
    }

    public function getQuantidadeEmpenhada()
    {
        return number_format(DB::table('compra_item_fornecedor')
            ->join('compra_item_minuta_empenho', 'compra_item_minuta_empenho.compra_item_id', 'compra_item_fornecedor.compra_item_id')
            ->join('minutaempenhos', 'minutaempenhos.id', 'compra_item_minuta_empenho.minutaempenho_id')
            ->join('codigoitens', 'codigoitens.id', 'minutaempenhos.situacao_id')
            ->where('codigoitens.descres', 'EMITIDO')
            ->where('compra_item_minuta_empenho.compra_item_id', $this->compra_item_id)
            ->where('compra_item_fornecedor.fornecedor_id', $this->fornecedor_id)
            ->sum('compra_item_minuta_empenho.quantidade'), 5);
    }
    
    public static function fornecedorCompra(array $idCompra)
    {
        return self::join('compra_items', function ($query) {
            $query->on('compra_items.id', 'compra_item_fornecedor.compra_item_id')
                ->whereNull('compra_items.deleted_at')
                ->where('compra_item_fornecedor.situacao', true);
        })
            ->join('compra_item_unidade', function ($query) {
                $query->on('compra_item_unidade.compra_item_id', 'compra_items.id')
                    ->where('compra_item_unidade.situacao', true);
            })
            ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
            ->whereRaw("compra_item_unidade.unidade_id IN (SELECT distinct ciu.unidade_id
                                         from compra_item_unidade ciu
                                         inner join compra_items ci1 on ciu.compra_item_id = ci1.id
                                         where ci1.compra_id = {$idCompra[0]})")
            ->where('compra_item_unidade.quantidade_saldo', '>', 0)
//            ->where(function ($query) {
//                $query->whereNull('compra_item_fornecedor.ata_vigencia_fim')
//                    ->orWhere('compra_item_fornecedor.ata_vigencia_fim', '>=', Carbon::now()->toDateString());
//            })
            ->where("compra_items.compra_id", $idCompra[0])
            ->distinct()
            ->select('compra_item_fornecedor.*')
            ->get();
    }

    public function getValorUnitarioFinal()
    {
        if ($this->percentual_maior_desconto > 0) {
            $valorUnitario = $this->valor_unitario * ((100 - $this->percentual_maior_desconto) / 100);
        } else {
            $valorUnitario = $this->valor_unitario;
        }

        return number_format($valorUnitario, 4, ',', '.');
    }
}
