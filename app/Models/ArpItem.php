<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Type\Integer;

class ArpItem extends Model
{
    use CrudTrait;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'arp_item';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['arp_id', 'compra_item_fornecedor_id', 'quantidade_max_adesao', 'aceita_adesao'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function getUnidadeParticipantePorAta(int $idAta, bool $somenteParticipante = true)
    {
        $unidadeParticipante = ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("compra_items", "compra_items.id", "=", "compra_item_fornecedor.compra_item_id")
            ->join(
                "compra_item_unidade",
                "compra_item_unidade.compra_item_id",
                "=",
                "compra_item_fornecedor.compra_item_id"
            )
            ->join("unidades", "unidades.id", "=", "compra_item_unidade.unidade_id")
            ->where("arp_item.arp_id", $idAta);

        # Se for para exibir os participantes e carona, aplica o filtro do
        # tipo da uasg para G = Gerenciadora
        if ($somenteParticipante) {
            $unidadeParticipante = $unidadeParticipante->where("compra_item_unidade.tipo_uasg", "<>", "G");
        }

        $unidadeParticipante =
            $unidadeParticipante->selectRaw(
                "CONCAT(codigo, ' - ' ,nomeresumido) AS nome_participante,
                codigo, nomeresumido, compra_item_unidade.tipo_uasg"
            )
            ->orderByRaw("CASE
                        WHEN compra_item_unidade.tipo_uasg = 'G' THEN 1
                        WHEN compra_item_unidade.tipo_uasg = 'P' THEN 2
                        WHEN compra_item_unidade.tipo_uasg = 'C' THEN 3
                        ELSE 0 END")
            ->groupBy("codigo", "nomeresumido", "compra_item_unidade.tipo_uasg")
            ->get();

        return $unidadeParticipante;
    }

    # Método responsável por recuperar os itens que o usuário salvou como rascunho
    public static function getItemAtaEdicao(int $idArp)
    {
        return ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("compra_items", "compra_item_fornecedor.compra_item_id", "=", "compra_items.id")
            ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compra_items.id')
            ->join('unidades', 'unidades.id', '=', 'compra_item_unidade.unidade_id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id')
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->join(
                'catmatseritens',
                'catmatseritens.id',
                '=',
                'compra_items.catmatseritem_id'
            )
            ->where("arp_id", $idArp)
            ->select([
                "compra_items.numero",
                "codigoitens.descricao",
                "catmatseritens.codigo_siasg",
                "compra_item_fornecedor.valor_unitario",
                "compra_item_fornecedor.valor_negociado",
                "compra_items.criterio_julgamento",
                "compra_item_fornecedor.percentual_maior_desconto",
                "compra_items.maximo_adesao",
                "compra_items.permite_carona",
                DB::raw("compra_items.descricaodetalhada AS descricaosimplificada"),

                DB::raw("string_agg(unidades.codigo, ', ')  as codigouasg"),
                DB::raw("string_agg(compra_item_fornecedor.id::varchar, ', ')  as compraItemFornecedorId"),
                DB::raw(
                    "string_agg(compra_item_unidade.quantidade_autorizada::varchar, ', ')
                    as quantidadeautorizadadetalhe"
                ),
                DB::raw("string_agg(
                CASE
                    WHEN tipo_uasg = 'G' THEN 'Gerenciadora'
                    WHEN tipo_uasg = 'P' THEN 'Participante'
                    WHEN tipo_uasg = 'C' THEN 'Não participante' -- Carona
                    END , ',')  as tipouasgdetalhe"),

                DB::raw(
                    "compra_item_fornecedor.valor_unitario *
                    ((100-compra_item_fornecedor.percentual_maior_desconto)/100)
                    as valor_unitario_desconto"
                ),
                DB::raw(
                    "compra_item_fornecedor.valor_negociado *
                    ((100-compra_item_fornecedor.percentual_maior_desconto)/100)
                    as valor_negociado_desconto"
                ),
                DB::raw("sum(compra_item_unidade.quantidade_saldo) AS quantidade_saldo"),
                DB::raw("sum(compra_item_unidade.quantidade_adquirir) AS quantidade_adquirir"),
                DB::raw("sum(compra_item_unidade.quantidade_autorizada) AS quantidadeautorizada"),
                DB::raw("quantidade_homologada_vencedor AS quantidadehomologadavencedor"),
                "fornecedores.cpf_cnpj_idgener",
                "fornecedores.nome AS nomefornecedor",
                "compra_item_fornecedor.classificacao",
                "compra_item_fornecedor.id as idCompraItemFornecedor"
            ])
            ->groupBy(
                "compra_items.numero",
                "codigoitens.descricao",
                "catmatseritens.codigo_siasg",
                "compra_items.descricaodetalhada",
                "compra_item_fornecedor.valor_unitario",
                "compra_item_fornecedor.valor_negociado",
                "compra_items.criterio_julgamento",
                "compra_item_fornecedor.percentual_maior_desconto",
                "compra_item_fornecedor.valor_unitario",
                "compra_item_fornecedor.valor_negociado",
                "compra_items.maximo_adesao",
                "compra_items.permite_carona",
                "fornecedores.cpf_cnpj_idgener",
                "fornecedores.nome",
                "compra_item_fornecedor.classificacao",
                "quantidade_homologada_vencedor",
                "compra_item_fornecedor.id"
            )
            ->orderByRaw("codigoitens.descricao ASC, compra_items.numero ASC")
            ->get();
    }

    /**
     * Retorna a lista de unidades de um item.
     * Não considera as unidades 'Não participantes' (Carona)
     * Precisa fazer join com minuta empenho para poder trazer o saldo
     *
     * @param integer $idAta id da ata
     * @param string $numero_item número do item
     * @return Collection Objeto com a lista de unidades e quantidades
     */
    public static function getUnidadeParticipantePorAtaItem(int $idAta, string $numero_item)
    {
        $codigoItem = CodigoItem::where('descres', 'statushistorico')
                ->where('descricao', 'Retificar - Excluir item')
                ->first();
        return ArpItem::select(
            DB::raw(
                "CASE compra_item_unidade.tipo_uasg 
                    WHEN 'G' THEN 'Gerenciadora'
                    WHEN 'P' THEN 'Participante'
                    WHEN 'C' THEN 'Não participante' -- Carona
                END as tipo_uasg"
            ),
            "unidades.codigo",
            "unidades.nomeresumido",
            "compra_item_unidade.quantidade_autorizada",
            # Se o saldo for negativo, retorna zero
            DB::raw(
                "CASE
                    WHEN COALESCE(SUM(compra_item_minuta_empenho.quantidade), 0) <= 0
                    THEN compra_item_unidade.quantidade_autorizada
                    ELSE GREATEST(
                            compra_item_unidade.quantidade_autorizada - 
                            COALESCE(SUM(compra_item_minuta_empenho.quantidade), 0), 0)
                END AS quantidade_saldo"
            )
        ) # fim da cláusula 'select'

            ->join('compra_item_fornecedor', function ($join) {
                $join->on('compra_item_fornecedor.id', '=', 'arp_item.compra_item_fornecedor_id')
                    ->where('compra_item_fornecedor.situacao', '=', true);
            })
            ->join(
                'compra_items',
                function ($join) {
                    $join->on('compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')
                        ->where('compra_items.situacao', '=', true);
                }
            )
            ->join('compra_item_unidade', function ($join) {
                $join->on('compra_item_unidade.compra_item_id', '=', 'compra_items.id')
                    #->on("compra_item_unidade.unidade_id", "=", "minutaempenhos.unidade_id")
                    ->where('compra_item_unidade.situacao', '=', true)
                    ->where('compra_item_unidade.tipo_uasg', '<>', 'C'); # Não trazer os Caronas
            })

            # Relaciona os empenhos com leftJoin pois a unidade pode não ter empenhos mas precisa ser considerada
            ->leftJoin("minutaempenhos", function ($join) {
                $join->on("minutaempenhos.unidade_id", "=", "compra_item_unidade.unidade_id")
                    ->on("minutaempenhos.fornecedor_compra_id", "=", "compra_item_fornecedor.fornecedor_id")
                    ->on("minutaempenhos.compra_id", "=", "compra_items.compra_id")
                    ->where('minutaempenhos.situacao_id', '=', 217);
            })
            ->leftJoin("compra_item_minuta_empenho", function ($join) {
                $join->on("minutaempenhos.id", "=", "compra_item_minuta_empenho.minutaempenho_id")
                    ->on("compra_items.id", "=", "compra_item_minuta_empenho.compra_item_id");
            })
            ->leftJoin('minutaempenhos_remessa', function ($join) {
                $join->on('compra_item_minuta_empenho.minutaempenhos_remessa_id', '=', 'minutaempenhos_remessa.id')
                    ->where(function ($query) {
                        $query->where(function ($subquery) {
                            $subquery->where('minutaempenhos_remessa.remessa', 0)
                                ->where('minutaempenhos_remessa.situacao_id', 215);
                        })
                            ->orWhere(function ($subquery) {
                                $subquery->where('minutaempenhos_remessa.remessa', '>', 0)
                                    ->where('minutaempenhos_remessa.situacao_id', 217);
                            });
                    });
            })

            # Traz somente os empenhos que forem de compra
            ->leftJoin('codigoitens', function ($join) {
                $join->on('codigoitens.id', '=', 'minutaempenhos.tipo_empenhopor_id')
                ->where('codigoitens.descricao', '=', 'Compra');
            })

            ->join('unidades', 'unidades.id', '=', 'compra_item_unidade.unidade_id')

            ->where('arp_id', '=', $idAta)
            ->where('compra_items.numero', '=', $numero_item)

            ->groupBy(
                'unidades.codigo',
                'compra_item_unidade.tipo_uasg',
                'unidades.nomeresumido',
                'compra_item_unidade.quantidade_autorizada'
            )
            ->get();
    }

    public static function getItemPorAtaMinutaEmpenho(int $situacao, string $numero_item, int $idAta)
    {
        return Compras::select('minutaempenhos.mensagem_siafi', 'minutaempenhos.created_at', 'unidades.codigo')
            ->addSelect([
                DB::raw("fornecedores.cpf_cnpj_idgener || '<br>' ||fornecedores.nome as fornecedor_empenho"),
                DB::raw("sum(compra_item_minuta_empenho.quantidade) as quantidade_minuta_empenhada"),
                DB::raw("sum(compra_item_unidade.quantidade_autorizada) as soma_quantidade_autorizada"),
                /*DB::raw("sum(compra_item_unidade.quantidade_autorizada) - " .
                "sum(compra_item_minuta_empenho.quantidade) as saldo_minuta_empenho"),*/
            ])
            ->join('compra_items', 'compra_items.compra_id', '=', 'compras.id')
            ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
            ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
            ->join('compra_item_minuta_empenho', 'compra_item_minuta_empenho.compra_item_id', '=', 'compra_items.id')
            ->join('minutaempenhos', 'minutaempenhos.id', '=', 'compra_item_minuta_empenho.minutaempenho_id')
            ->join('minutaempenhos_remessa', 'minutaempenhos.id', '=', 'minutaempenhos_remessa.minutaempenho_id')
            ->join('unidades', 'unidades.id', '=', 'minutaempenhos.id')
            ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compra_items.id')
            ->join('arp_item', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
            ->where('compra_items.numero', '=', $numero_item)
            ->where('minutaempenhos.situacao_id', '=', $situacao)
            ->where('arp_item.arp_id', '=', $idAta)
            ->groupBy(
                'minutaempenhos.mensagem_siafi',
                'minutaempenhos.created_at',
                'unidades.codigo',
                'fornecedor_empenho'
            )
            ->orderBy('minutaempenhos.mensagem_siafi', 'desc')
            ->get();
    }

    public static function getItemPorAtaMinutaEmpenhoSimplificada(int $idAta, string $numero_item, int $situacao)
    {
        return Arp::select(
            DB::raw("sum(quantidade_autorizada) as quantidade_autorizada"),
            DB::raw("sum(quantidade_autorizada) - sum(compra_item_minuta_empenho.quantidade) AS saldo_para_empenho")
        )
            ->join('arp_item', 'arp.id', '=', 'arp_item.arp_id')
            ->join('arp_unidades', 'arp_item.id', '=', 'arp_unidades.arp_item_id')
            ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
            ->join('compra_items', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
            ->leftJoin(
                'compra_item_minuta_empenho',
                'compra_items.id',
                '=',
                'compra_item_minuta_empenho.compra_item_id'
            )
            ->leftJoin('minutaempenhos', 'minutaempenhos.id', '=', 'compra_item_minuta_empenho.minutaempenho_id')
            ->where('arp.id', '=', $idAta)
            ->where('compra_items.numero', '=', $numero_item)
            ->where('minutaempenhos.situacao_id', '=', $situacao)
            ->get();
    }

    /**
     * Retorna a quantidade total registrada de um item de uma Arp
     *
     * @param integer $idAta id da ata
     * @param string $numero_item numero do item
     * @return Object Quantidade total registrada
     */
    public static function getQuantidadeTotalRegistrada(int $idAta, string $numero_item)
    {
        $subQuery = ArpItem::selectRaw('MAX(compra_item_unidade.quantidade_autorizada) as quantidade_autorizada')
            ->join('compra_item_fornecedor', function ($join) {
                $join->on('compra_item_fornecedor.id', '=', 'arp_item.compra_item_fornecedor_id')
                    ->where('compra_item_fornecedor.situacao', true);
            })
            ->join('compra_items', function ($join) {
                $join->on('compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')
                    ->where('compra_items.situacao', true);
            })
            ->join('compra_item_unidade', function ($join) {
                $join->on('compra_item_unidade.compra_item_id', '=', 'compra_items.id')
                    ->where('compra_item_unidade.situacao', true)
                    ->where('compra_item_unidade.tipo_uasg', '<>', 'C');
            })

            ->where('arp_item.arp_id', $idAta)
            ->where('compra_items.numero', $numero_item)
            ->whereNull('arp_item.deleted_at')
            ->groupBy('compra_item_unidade.unidade_id');

        return DB::table(DB::raw("({$subQuery->toSql()}) as subquery"))
            ->mergeBindings($subQuery->getQuery())
            ->selectRaw('SUM(subquery.quantidade_autorizada) as quantidade_total_registrada')
            ->first();
    }

    /**
     * Retorna a quantidade total aprovada para adesão de um item de uma Arp
     *
     * @param integer $idAta id da ata
     * @param string $numero_item numero do item
     * @return Object Quantidade total aprovada para adesão (do item)
     */
    public static function getQuantidadeTotalAprovadaParaAdesao(int $idAta, string $numero_item)
    {
        return ArpItem::select(
            DB::raw("sum(arp_solicitacao_item.quantidade_aprovada) quantidade_total_aprovada_adesao")
        )
            ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
            ->join('compra_items', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
            ->join('arp_solicitacao_item', 'arp_item.id', '=', 'arp_solicitacao_item.item_arp_fornecedor_id')
            ->join('arp_solicitacao', function ($join) {
                $join->on('arp_solicitacao_item.arp_solicitacao_id', '=', 'arp_solicitacao.id')
                    ->where('arp_solicitacao.rascunho', '=', false);
            })
            ->join('unidades', 'arp_solicitacao.unidade_origem_id', '=', 'unidades.id')
            ->where('arp_id', '=', $idAta)
            ->where('compra_items.numero', '=', $numero_item)
            # Se a quantidade aprovada não for nula, já foi feita a análise
            # Se for zero, ou seja, tudo foi negado, a análise foi feita de qualquer forma
            ->whereNotNull('arp_solicitacao_item.quantidade_aprovada')
            # Traz somente as solicitações que tiveram alguma aprovação (total ou parcial)
            ->where('arp_solicitacao_item.quantidade_aprovada', '>', 0)
            //->dd()
            ->first();
    }

    /**
     * Retorna, dependendo do parâmetro $valorRetorno:
     * a) A lista de unidades que possuem empenho e suas quantidades
     * b) A quantidade empenhada total das unidades
     *
     * @param int $idAta
     * @param string $numero_item
     * @param $valorRetorno Indica o que o método deverá retornar
     * @return Collection Objeto contendo um dos retornos especificados
     */
    public static function getTotaisMinutaEmpenhoPorUnidade($idAta, $numero_item, $valorRetorno = '*')
    {
        switch ($valorRetorno) {
            case 'quantidade_total_empenhada':
                $query = ArpItem::select(
                    DB::raw("sum(compra_item_minuta_empenho.quantidade) as quantidade_total_empenhada")
                );
                break;
            case '*':
                $query = ArpItem::select(
                    DB::raw("(unidades.codigo || ' - ' || unidades.nomeresumido) as unidade"),
                    DB::raw(
                        "CASE compra_item_unidade.tipo_uasg 
                            WHEN 'G' THEN 'Gerenciadora'
                            WHEN 'P' THEN 'Participante'
                            WHEN 'C' THEN 'Não participante' -- Carona
                        END as tipo_uasg"
                    ),
                    DB::raw("compra_item_unidade.quantidade_autorizada"),
                    DB::raw("sum(compra_item_minuta_empenho.quantidade) as quantidade_empenhada"),
                    DB::raw(
                        "CASE
                            WHEN 
                            compra_item_unidade.quantidade_autorizada - sum(compra_item_minuta_empenho.quantidade) < 0
                            THEN 0
                            ELSE compra_item_unidade.quantidade_autorizada - sum(compra_item_minuta_empenho.quantidade)
                        END as saldo_para_empenho"
                    )
                )->groupBy(
                    "unidades.codigo",
                    "unidades.nomeresumido",
                    "compra_item_unidade.quantidade_autorizada",
                    "compra_item_unidade.quantidade_saldo",
                    "tipo_uasg"
                );
                break;
        }

        return $query->join("arp", "arp.id", "=", "arp_item.arp_id")
            ->join("arp_unidades", "arp_item.id", "=", "arp_unidades.arp_item_id")
            ->join("compra_item_fornecedor", "arp_item.compra_item_fornecedor_id", "=", "compra_item_fornecedor.id")
            ->join("compra_items", "compra_item_fornecedor.compra_item_id", "=", "compra_items.id")
            ->join("compras", "compra_items.compra_id", "=", "compras.id")
            ->join("compra_item_minuta_empenho", "compra_items.id", "=", "compra_item_minuta_empenho.compra_item_id")

            ->join("minutaempenhos", function ($join) {
                $join->on("compra_item_minuta_empenho.minutaempenho_id", "=", "minutaempenhos.id")
                    ->on("minutaempenhos.fornecedor_compra_id", "=", "compra_item_fornecedor.fornecedor_id");
            })
            ->join("compra_item_unidade", function ($join) {
                $join->on("compra_items.id", "=", "compra_item_unidade.compra_item_id")
                    ->on("compra_item_unidade.unidade_id", "=", "minutaempenhos.unidade_id")
                    ->where("compra_item_unidade.situacao", "=", true);
            })
            ->join("unidades", "unidades.id", "=", "minutaempenhos.unidade_id")
            ->join(
                "minutaempenhos_remessa",
                "compra_item_minuta_empenho.minutaempenhos_remessa_id",
                "=",
                "minutaempenhos_remessa.id"
            )

            ->whereNull("arp_item.deleted_at")
            ->where("arp.id", $idAta)
            ->where('compra_items.numero', '=', $numero_item)
            ->whereRaw("
                    minutaempenhos.situacao_id = 217
                        and (
                        (minutaempenhos_remessa.remessa = 0 and minutaempenhos_remessa.situacao_id = 215)
                        or
                        (minutaempenhos_remessa.remessa > 0 AND minutaempenhos_remessa.situacao_id = 217)
                    )")
            #->dd()
            ->get();
    }

    /**
     * Retorna para um item de uma Arp, a lista de empenhos com quantidades por unidade
     *
     * @param int $idAta
     * @param string $numero_item
     * @return Collection Objeto contendo a lista de empenhos
     */
    public static function getMinutasEmpenhoPorUnidade($idAta, $numero_item)
    {
        return ArpItem::select(
            "minutaempenhos.mensagem_siafi",
            DB::raw("(unidades.codigo || ' - ' || unidades.nomeresumido) as unidade"),
            "minutaempenhos.data_emissao",
            DB::raw("(fornecedores.cpf_cnpj_idgener || ' - ' ||fornecedores.nome) as fornecedor"),
            DB::raw(
                "SUM(CASE
                        WHEN compra_item_minuta_empenho.operacao_id = 220
                        THEN compra_item_minuta_empenho.quantidade ELSE 0.00000 END
                    ) AS quantidade_inclusao"
            ),
            DB::raw(
                "SUM(CASE
                        WHEN compra_item_minuta_empenho.operacao_id = 221
                        THEN compra_item_minuta_empenho.quantidade ELSE 0.00000 END
                    ) AS quantidade_reforco"
            ),
            DB::raw(
                "SUM(CASE
                        WHEN compra_item_minuta_empenho.operacao_id = 222
                        THEN compra_item_minuta_empenho.quantidade ELSE 0.00000 END
                    ) AS quantidade_anulacao"
            ),
            DB::raw(
                "(SUM(CASE 
                    WHEN compra_item_minuta_empenho.operacao_id = 220
                    THEN compra_item_minuta_empenho.quantidade ELSE 0.00000 END
                ) +
                SUM(CASE
                    WHEN compra_item_minuta_empenho.operacao_id = 221
                    THEN compra_item_minuta_empenho.quantidade ELSE 0.00000 END
                ) + 
                SUM(CASE
                    WHEN compra_item_minuta_empenho.operacao_id = 222
                    THEN compra_item_minuta_empenho.quantidade ELSE 0.00000 END
                )) AS quantidade_empenhada"
            ),
            DB::raw(
                "SUM(compra_item_minuta_empenho.valor) as valor"
            )
        )
            ->join("arp_unidades", "arp_item.id", "=", "arp_unidades.arp_item_id")
            ->join("compra_item_fornecedor", "arp_item.compra_item_fornecedor_id", "=", "compra_item_fornecedor.id")
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->join("compra_items", "compra_item_fornecedor.compra_item_id", "=", "compra_items.id")
            ->join("compras", "compra_items.compra_id", "=", "compras.id")
            ->join("compra_item_minuta_empenho", "compra_items.id", "=", "compra_item_minuta_empenho.compra_item_id")

            ->join("minutaempenhos", function ($join) {
                $join->on("compra_item_minuta_empenho.minutaempenho_id", "=", "minutaempenhos.id")
                    ->on("minutaempenhos.fornecedor_compra_id", "=", "compra_item_fornecedor.fornecedor_id");
            })
            ->join("compra_item_unidade", function ($join) {
                $join->on("compra_items.id", "=", "compra_item_unidade.compra_item_id")
                    ->on("compra_item_unidade.unidade_id", "=", "minutaempenhos.unidade_id")
                    ->where("compra_item_unidade.situacao", "=", true);
            })
            ->join("unidades", "unidades.id", "=", "minutaempenhos.unidade_id")
            ->join(
                "minutaempenhos_remessa",
                "compra_item_minuta_empenho.minutaempenhos_remessa_id",
                "=",
                "minutaempenhos_remessa.id"
            )

            ->whereNull("arp_item.deleted_at")
            ->where("arp_item.arp_id", $idAta)
            ->where('compra_items.numero', '=', $numero_item)
            ->whereRaw("
                minutaempenhos.situacao_id = 217
                    and (
                    (minutaempenhos_remessa.remessa = 0 and minutaempenhos_remessa.situacao_id = 215)
                    or
                    (minutaempenhos_remessa.remessa > 0 AND minutaempenhos_remessa.situacao_id = 217)
                )")
            ->groupBy(
                "minutaempenhos.mensagem_siafi",
                "unidade",
                "minutaempenhos.data_emissao",
                "fornecedor"
            )
            ->orderBy("minutaempenhos.data_emissao")
            #->dd()
            ->get();
    }

    /***
     * Obtém os totalizadores de adesão para um determinado item de uma ata.
     *
     * @param int $idAta O ID da ata à qual o item pertence.
     * @param string $numero_item O número do item da ata.
     * @return Collection Retorna um objeto contendo os totalizadores
     */
    public static function getTotalizadoresAdesao(int $idAta, string $numero_item)
    {
        $query = DB::table('compra_items')
            ->select(
                DB::raw(
                    "compra_items.maximo_adesao,
                    compra_items.maximo_adesao - SUM(arp_solicitacao_item.quantidade_aprovada)
                    as quantidade_disponivel_adesao"
                ),
                DB::raw(
                    # Obtém a soma das quantidades em que a solicitação do item está aguardando análise
                    "SUM(
                        CASE
                            /* Quando a quantidade aprovada é nula, é por que não foi analisado ainda */
                            WHEN arp_solicitacao_item.quantidade_aprovada IS NULL
                            AND arp_solicitacao.rascunho = false
                            /* neste caso, soma a qtd aguardando análise */
                            THEN arp_solicitacao_item.quantidade_solicitada                            
                            /* Se a quantidade tiver valor (mesmo que seja zero), já foi analisado.
                             Neste caso, retorna 0 para não somar na quantidade 'aguardando análise' */
                            ELSE 0 END
                        ) AS quantidade_aguardando_analise"
                ),
                DB::raw(
                    "CASE
                        WHEN compra_items.permite_carona = true THEN 'Sim'
                        WHEN compra_items.permite_carona = false THEN 'Não'
                    END AS aceita_adesao"
                ),
                'compra_items.maximo_adesao_informado_compra'
            )
            ->join('compra_item_fornecedor', 'compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')
            ->join('arp_item', 'compra_item_fornecedor.id', '=', 'arp_item.compra_item_fornecedor_id')
            ->join('arp', 'arp_item.arp_id', '=', 'arp.id')
            ->leftJoin('arp_solicitacao_item', 'arp_item.id', '=', 'arp_solicitacao_item.item_arp_fornecedor_id')
            ->leftJoin('arp_solicitacao', function ($join) {
                $join->on('arp_solicitacao_item.arp_solicitacao_id', '=', 'arp_solicitacao.id')
                    ->where('arp_solicitacao.rascunho', '=', false);
            })
            ->where('arp.id', '=', $idAta)
            ->where('compra_items.numero', '=', $numero_item)
            ->groupBy(
                'compra_items.maximo_adesao',
                'aceita_adesao',
                'compra_items.maximo_adesao_informado_compra'
            );

        return ($query->get()->first());
    }

    /**
     * Obtém os dados de adesão de um item da ata de acordo com o status dos itens
     *
     * @param int $idAta O ID da ata à qual o item pertence.
     * @param string $numero_item O número do item da ata.
     * @param string $status Situação dos itens
     * @return Collection com os itens
     */
    public static function getAdesoesItemPorUnidade(int $idAta, string $numero_item, string $status = null)
    {
        $query = DB::table('arp')
            ->select(
                DB::raw(
                    "unidades.codigo || '-' || unidades.nomeresumido AS unidade_solicitacao,
                    arp_solicitacao_item.quantidade_aprovada,
                    compra_items.maximo_adesao,arp_solicitacao.data_aprovacao_analise"
                )
            )
            ->join('arp_item', 'arp.id', '=', 'arp_item.arp_id')
            ->join('compra_item_fornecedor', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')
            ->join('compra_items', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
            ->join('arp_solicitacao_item', 'arp_item.id', '=', 'arp_solicitacao_item.item_arp_fornecedor_id')
            ->join('arp_solicitacao', function ($join) {
                $join->on('arp_solicitacao_item.arp_solicitacao_id', '=', 'arp_solicitacao.id')
                    ->where('arp_solicitacao.rascunho', '=', false);
            })
            ->join('unidades', 'arp_solicitacao.unidade_origem_id', '=', 'unidades.id')
            ->where('arp_id', '=', $idAta)
            ->where('compra_items.numero', '=', $numero_item);

        # Dependendo do status desejado do item, adiciona:
        # a) o respectivo filtro no where
        # b) os valores a trazer
        # c) a ordem de exibição
        switch ($status) {
            case 'aguardando_analise':
                # Traz a quantidade solicitada para adesão e a data da solicitação
                $query->addSelect(
                    DB::raw(
                        'arp_solicitacao_item.quantidade_solicitada,
                        arp_solicitacao.created_at as data_hora_solicitacao'
                    )
                )
                    # Se a quantidade aprovada é nula, ainda está aguardando aprovação
                    ->whereNull('arp_solicitacao_item.quantidade_aprovada')

                    # Ordena cronologicamente do mais antigo para o mais recente
                    ->orderBy('arp_solicitacao.created_at', 'asc');

                break;
            case 'analisada':
                # Traz a quantidade aprovada para adesão
                $query->addSelect('arp_solicitacao_item.quantidade_aprovada')

                    # Se a quantidade aprovada não for nula, já foi feita a análise
                    # Se for zero, ou seja, tudo foi negado, a análise foi feita de qualquer forma
                    ->whereNotNull('arp_solicitacao_item.quantidade_aprovada')

                    # Traz somente as solicitações que tiveram alguma aprovação (total ou parcial)
                    ->where('arp_solicitacao_item.quantidade_aprovada', '>', 0);

                break;

            case 'negada':
                # Traz a quantidade solicitada (mas que foi negada pro completo)
                $query->addSelect('arp_solicitacao_item.quantidade_solicitada')

                    # Se a quantidade aprovada for zero, a solicitação foi negada por completo
                    ->where('arp_solicitacao_item.quantidade_aprovada', '=', '0')
                    # Verifica se não é nula para garantir que tenha sido feito o processo de análise
                    ->whereNotNull('arp_solicitacao_item.quantidade_aprovada');


                break;
        }

        return collect($query->get());
    }

    public static function getItemPorAtaSimplificada(int $idAta)
    {
        return ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->join("compra_items", "compra_items.id", "=", "compra_item_fornecedor.compra_item_id")
            ->where("arp_item.arp_id", $idAta)
            ->selectRaw("CASE
                        WHEN compra_items.permite_carona = true THEN 'Sim'
                        WHEN compra_items.permite_carona = false THEN 'Não'
                        END AS aceita_adesao,
                        compra_items.numero, compra_items.descricaodetalhada, compra_items.maximo_adesao,
                        fornecedores.cpf_cnpj_idgener, fornecedores.nome AS nomefornecedor")
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('arp_item_historico')
                    ->join('arp_historico', 'arp_historico.id', '=', 'arp_item_historico.arp_historico_id')
                    ->whereRaw('arp_item_historico.arp_item_id = arp_item.id')
                    ->where('arp_item_historico.item_cancelado', true);
            })
            ->distinct("compra_items.numero")
            ->orderBy("compra_items.numero")
            ->get();
    }

    /**
     * Retorna a lista de contratos vinculados ao item da ata
     *
     * @param integer $idAta id da ata
     * @param string $numero_item número do item de compra
     * @return Object Collection com os contratos
     */
    public static function getContratosItem(int $idAta, string $numero_item)
    {
        return Contrato::select(
            'contratos.id',
            'contratos.numero',
            DB::raw("unidades.codigo || ' - ' || unidades.nomeresumido unidade"),
            DB::raw("fornecedores.cpf_cnpj_idgener || ' - ' ||fornecedores.nome as fornecedor_contrato"),
            DB::raw("sum(contratoitens.quantidade) quantidade_contratada")
        )
            ->join('contratoitens', function ($join) {
                $join->on('contratoitens.contrato_id', '=', 'contratos.id')
                    ->whereNull('contratoitens.deleted_at');
            })
            ->join('compras', function ($join) {
                $join->on('contratos.unidadecompra_id', '=', 'compras.unidade_origem_id')
                    ->on('contratos.modalidade_id', '=', 'compras.modalidade_id')
                    ->on('contratos.licitacao_numero', '=', 'compras.numero_ano');
            })
            ->join('compra_items', function ($join) {
                $join->on('compra_items.compra_id', '=', 'compras.id')
                    ->where('compra_items.situacao', true)
                    ->whereColumn('compra_items.numero', '=', 'contratoitens.numero_item_compra');
            })
            ->join('compra_item_fornecedor', function ($join) {
                $join->on('compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
                    ->where('compra_item_fornecedor.situacao', true)
                    ->whereColumn('compra_item_fornecedor.fornecedor_id', '=', 'contratos.fornecedor_id');
            })
            ->join('arp_item', 'arp_item.compra_item_fornecedor_id', '=', 'compra_item_fornecedor.id')

            ->join('fornecedores', 'fornecedores.id', '=', 'contratos.fornecedor_id')
            ->join('unidades', 'contratos.unidade_id', '=', 'unidades.id')
            ->join('codigoitens', 'codigoitens.id', 'contratoitens.tipo_id')

            ->where('arp_item.arp_id', $idAta)
            ->where('contratoitens.numero_item_compra', $numero_item)

            ->groupBy(
                "contratos.id",
                "contratos.numero",
                "unidade",
                "fornecedor_contrato"
            )
            ->get();
    }

    public function getItemPorAtaMinutaEmpenhoNovo(int $situacao, string $numero_item)
    {
        $query = DB::table('compras')
            ->select(
                DB::raw('min(minutaempenhos.mensagem_siafi) as mensagem_siafi'),
                DB::raw('min(minutaempenhos.created_at) as created_at'),
                DB::raw('min(unidades.codigo) as codigo'),
                DB::raw('sum(compra_item_minuta_empenho.quantidade) as quantidade_minuta_empenhado_vitor'),
                DB::raw('sum(compra_item_unidade.quantidade_autorizada) as soma_quantidade_autorizada_luis'),
                DB::raw(
                    'sum(compra_item_unidade.quantidade_autorizada) - 
                    sum(compra_item_minuta_empenho.quantidade) as saldo_empenho_joao'
                )
            )
            ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
            ->join('compra_item_minuta_empenho', 'compra_item_minuta_empenho.compra_item_id', '=', 'compra_items.id')
            ->join('minutaempenhos', 'minutaempenhos.id', '=', 'compra_item_minuta_empenho.minutaempenho_id')
            ->join('minutaempenhos_remessa', 'minutaempenhos.id', '=', 'minutaempenhos_remessa.minutaempenho_id')
            ->join('unidades', 'unidades.id', '=', 'minutaempenhos.id')
            ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compra_items.id')
            ->where('compra_items.numero', '=', $numero_item)
            ->where('minutaempenhos.situacao_id', '=', $situacao)
            ->groupBy('minutaempenhos.mensagem_siafi', 'minutaempenhos.created_at', 'unidades.codigo')
            ->get();
        return $query;
    }



    public static function getFornecedorPorAta(int $idAta)
    {
        return ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->where("arp_item.arp_id", $idAta)
            ->selectRaw("CONCAT(cpf_cnpj_idgener, ' - ' ,nome) AS nome_fornecedor, cpf_cnpj_idgener, nome")
            ->groupBy("cpf_cnpj_idgener", "nome")
            ->get();
    }

    public static function getContratosPorAta(int $idAta)
    {
        return ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->where("arp_item.arp_id", $idAta)
            ->selectRaw("CONCAT(cpf_cnpj_idgener, ' - ' ,nome) AS nome_fornecedor, cpf_cnpj_idgener, nome")
            ->groupBy("cpf_cnpj_idgener", "nome")
            ->get();
    }

    # Método responsável em recuperar os itens da ata
    public static function getItemPorAta(int $idAta)
    {
        return ArpItem::select(
            'compra_items.permite_carona',
            'arp_item.id as arp_item_id',
            'compra_items.compra_id',
            'compra_items.id as compra_item_id',
            'compra_items.numero',
            'compra_items.descricaodetalhada',
            'compra_items.maximo_adesao',
            'fornecedores.cpf_cnpj_idgener',
            'compra_item_fornecedor.id as fornecedor_id',
            'fornecedores.nome AS nomefornecedor',
            'compra_item_fornecedor.valor_unitario',
            'compra_item_fornecedor.valor_negociado',
            'compra_item_fornecedor.classificacao',
            'compra_item_fornecedor.quantidade_homologada_vencedor',
            'compra_item_fornecedor.percentual_maior_desconto',
            DB::raw("TO_CHAR(compra_item_fornecedor.valor_unitario *
    ((100-compra_item_fornecedor.percentual_maior_desconto)/100), 'FM999999999.0000') as valor_unitario_desconto"),
            DB::raw("TO_CHAR(
        compra_item_fornecedor.valor_unitario * ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100),
        'FM999999999.0000'
    )::float * compra_item_fornecedor.quantidade_homologada_vencedor as valor_negociado_desconto"),
            'catmatseritens.codigo_siasg',
            'codigoitens.descricao',
            'compra_items.maximo_adesao_informado_compra',
        )
            ->join('compra_item_fornecedor', function ($query) {
                $query->on('arp_item.compra_item_fornecedor_id', 'compra_item_fornecedor.id')
                    ->where('compra_item_fornecedor.situacao', true);
            })
            ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
            ->join('compra_items', 'compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id')
            ->join('catmatseritens', 'catmatseritens.id', '=', 'compra_items.catmatseritem_id')
            ->where('arp_item.arp_id', $idAta)
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('arp_item_historico')
                    ->join('arp_historico', 'arp_historico.id', '=', 'arp_item_historico.arp_historico_id')
                    ->join('arp_alteracao', function ($query) {
                        $query->on('arp_historico.arp_alteracao_id', 'arp_alteracao.id')
                            ->where('arp_alteracao.rascunho', false);
                    })
                    ->whereRaw('arp_item_historico.arp_item_id = arp_item.id')
                    ->where(function ($query) {
                        $query->where('arp_item_historico.item_cancelado', true)
                            ->orWhere('arp_item_historico.item_removido_retificacao', true);
                    });
            })

            ->orderBy('compra_items.numero')
            ->get();
    }

    /**
     * Método responsável em recuperar os itens da ata para alteração
     */
    public static function getItemAlteracaoPorAta(int $idAta, int $arpAlteracaoId)
    {
        return ArpItem::join(
            'compra_item_fornecedor',
            function ($query) {
                $query->on('arp_item.compra_item_fornecedor_id', 'compra_item_fornecedor.id')
                    ->where('compra_item_fornecedor.situacao', true);
            }
        )
            ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
            ->join('compra_items', 'compra_items.id', '=', 'compra_item_fornecedor.compra_item_id')

            ->selectRaw("
        CASE
            WHEN compra_items.permite_carona = true THEN 'Sim'
            WHEN compra_items.permite_carona = false THEN 'Não'
        END AS aceita_adesao,
        compra_items.numero,
        compra_items.descricaodetalhada,
        compra_items.maximo_adesao,
        compra_item_fornecedor.valor_unitario,
        compra_items.criterio_julgamento,
        compra_item_fornecedor.percentual_maior_desconto,
        (
            SELECT arp_item_historico.percentual_maior_desconto
            FROM arp_item_historico      
            WHERE arp_item_historico.arp_item_id = arp_item.id
            AND arp_item_historico.percentual_maior_desconto IS NOT NULL
            ORDER BY arp_item_historico.id DESC
            LIMIT 1
        ) AS percentual_desconto,
        (
            SELECT arp_item_historico.valor
            FROM arp_item_historico
            INNER JOIN arp_historico on arp_historico.id = arp_item_historico.arp_historico_id
            WHERE arp_item_historico.arp_item_id = arp_item.id   
            AND arp_item_historico.valor IS NOT NULL
            AND arp_historico.arp_alteracao_id = $arpAlteracaoId
            ORDER BY arp_item_historico.id DESC
            LIMIT 1
        ) AS valor_rascunho,
        arp_item.id,
        CONCAT(
            fornecedores.cpf_cnpj_idgener,
            ' - ',
            fornecedores.nome,
            ' (',
            compra_item_fornecedor.classificacao,
            ')'
        ) AS nomeFornecedor,
        arp_item.compra_item_fornecedor_id,
        compra_item_fornecedor.compra_item_id, compra_item_fornecedor.fornecedor_id")
            ->where("arp_item.arp_id", $idAta)
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('arp_item_historico')
                    ->join('arp_historico', 'arp_historico.id', '=', 'arp_item_historico.arp_historico_id')
                    ->join('arp_alteracao', function ($query) {
                        $query->on('arp_historico.arp_alteracao_id', 'arp_alteracao.id')
                            ->where('arp_alteracao.rascunho', false);
                    })
                    ->whereRaw('arp_item_historico.arp_item_id = arp_item.id')
                    ->where(function ($query) {
                        $query->where('arp_item_historico.item_cancelado', true)
                            ->orWhere('arp_item_historico.item_removido_retificacao', true);
                    });
            })
            ->whereNull('arp_item.deleted_at')
            ->orderBy('compra_items.numero', 'asc')
            ->get();
    }

    public static function getValorTotalAta(int $idAta)
    {
        return ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->join("compra_items", "compra_items.id", "=", "compra_item_fornecedor.compra_item_id")
            ->where("arp_item.arp_id", $idAta)
            ->sum("compra_item_fornecedor.valor_negociado");
    }

    public static function detalhesItem(string $id)
    {
        return ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->join("compra_items", "compra_items.id", "=", "compra_item_fornecedor.compra_item_id")
            ->where("compra_items.numero", $id)
            ->selectRaw("CASE
        WHEN compra_items.permite_carona = true THEN 'Sim'
        WHEN compra_items.permite_carona = false THEN 'Não'
        END AS aceita_adesao,
        compra_items.numero, compra_items.descricaodetalhada, compra_items.maximo_adesao")
            ->groupby(
                'compra_items.numero',
                'compra_items.descricaodetalhada',
                'compra_items.maximo_adesao',
                'compra_items.permite_carona'
            )
            ->first();
    }


    public function getDescricaoDetalhada()
    {
        $item = $this->detalhesItem($this->numero);

        return $item->descricaodetalhada;
    }

    public static function getUnidadeParticipante(int $idAta)
    {
        return ArpItem::join("arp_unidades", "arp_item.id", "=", "arp_unidades.arp_item_id")
            ->join("unidades", "unidades.id", "=", "arp_unidades.unidade_id")
            ->where("arp_id", $idAta)
            ->where("tipo_unidade", "<>", 'G')
            ->select("unidades.id")
            ->groupBy("unidades.id")
            ->get()
            ->pluck('id')
            ->toArray();
    }

    /**
     * Método responsável em recuperar os itens na tela de rascunho
     */
    public static function itensAtaRascunho(int $idArp)
    {
        return ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("compra_items", "compra_item_fornecedor.compra_item_id", "=", "compra_items.id")
            // ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'arp_item.id')
            // ->join('unidades', 'unidades.id', '=', 'compra_item_unidade.unidade_id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id')
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->join('compras', 'compras.id', '=', 'compra_items.compra_id')
            ->join(
                'catmatseritens',
                'catmatseritens.id',
                '=',
                'compra_items.catmatseritem_id'
            )
            ->where("arp_id", $idArp)
            ->select([
                "compra_items.numero",
                "codigoitens.descricao",
                "catmatseritens.codigo_siasg",
                "compra_item_fornecedor.valor_unitario",
                "compra_item_fornecedor.valor_negociado",
                "compra_items.criterio_julgamento",
                "compra_item_fornecedor.percentual_maior_desconto",
                "compra_items.maximo_adesao",
                "compra_items.permite_carona",
                DB::raw("compra_items.descricaodetalhada AS descricaosimplificada"),

                // DB::raw("string_agg(unidades.codigo, ', ')  as codigouasg"),
                DB::raw("string_agg(compra_item_fornecedor.id::varchar, ', ')  as compraItemFornecedorId"),
                // DB::raw("string_agg(quantidade_autorizada::varchar, ', ')  as quantidadeautorizadadetalhe"),
                // DB::raw("string_agg(
                // CASE
                // WHEN tipo_uasg = 'G' THEN 'Gerenciadora'
                // WHEN tipo_uasg = 'P' THEN 'Participante'
                // WHEN tipo_uasg = 'C' THEN 'Não participante' -- Carona
                // END , ',')  as tipouasgdetalhe"),

                DB::raw("TO_CHAR(compra_item_fornecedor.valor_unitario *
    ((100-compra_item_fornecedor.percentual_maior_desconto)/100), 'FM999999999.0000') as valor_unitario_desconto"),

                DB::raw("TO_CHAR(
        compra_item_fornecedor.valor_unitario * ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100),
        'FM999999999.0000'
    )::float * compra_item_fornecedor.quantidade_homologada_vencedor as valor_negociado_desconto"),



                // DB::raw("sum(compra_item_unidade.quantidade_saldo) AS quantidade_saldo"),
                // DB::raw("sum(compra_item_unidade.quantidade_adquirir) AS quantidade_adquirir"),
                // DB::raw("sum(quantidade_autorizada) AS quantidadeautorizada"),
//                DB::raw("sum(quantidade_homologada_vencedor) AS quantidadehomologadavencedor"),
                "compra_item_fornecedor.quantidade_homologada_vencedor",
                "fornecedores.cpf_cnpj_idgener",
                "fornecedores.nome AS nomefornecedor",
                "compras.id as compraId",
                "compra_items.id as compraItemId",
                "compra_item_fornecedor.id as idCompraItemFornecedor",
                "compra_item_fornecedor.classificacao",
                "compra_items.maximo_adesao_informado_compra",
                "compras.lei"
            ])
            ->groupBy(
                "compra_items.numero",
                "codigoitens.descricao",
                "catmatseritens.codigo_siasg",
                "compra_items.descricaodetalhada",
                "compra_item_fornecedor.valor_unitario",
                "compra_item_fornecedor.valor_negociado",
                "compra_items.criterio_julgamento",
                "compra_item_fornecedor.percentual_maior_desconto",
                "compra_item_fornecedor.valor_unitario",
                "compra_item_fornecedor.valor_negociado",
                "compra_items.maximo_adesao",
                "compra_items.permite_carona",
                "fornecedores.cpf_cnpj_idgener",
                "fornecedores.nome",
                "compras.id",
                "compra_items.id",
                "compra_item_fornecedor.id",
                "compra_item_fornecedor.classificacao",
            )
            ->orderByRaw("codigoitens.descricao ASC, compra_items.numero ASC")
            ->get();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function unidade_participante()
    {
        return $this->hasMany(ArpUnidades::class, 'arp_item_id');
    }

    public function fornecedor_compra()
    {
        return $this->hasMany(CompraItemFornecedor::class, 'compra_item_id');
    }

    public function item_fornecedor_compra()
    {
        return $this->belongsTo(CompraItemFornecedor::class, 'compra_item_fornecedor_id');
    }

    public function unidades()
    {
        $arp = new Arp();
        return $arp->unidades();
    }

    public function arp()
    {
        return $this->belongsTo(Arp::class, 'arp_id');
    }
    
    public function itemHistorico()
    {
        return $this->hasMany(ArpItemHistorico::class, 'arp_item_id');
    }

    public function arpHistorico()
    {
        return $this->hasMany(ArpHistorico::class, 'arp_id', 'arp_id');
    }

    public function itensAdesao()
    {
        return $this->hasMany(AdesaoItem::class, 'item_arp_fornecedor_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
