<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AmparoLegalRestricaoCodigo extends Model
{

    use LogsActivity;
   

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */


    protected $table = 'amparo_legal_restricao_codigos';


    

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
   

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function amparoLegalRestricao()
    {
        return $this->belongsTo(AmparoLegalRestricao::class, 'amparo_legal_restricao_id');
    }
}
