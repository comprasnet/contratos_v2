<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class EnviaDadosPncp extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'enviadadospncp_v2';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'envia_dados_pncp';
    protected $fillable = [
        'id',
        'pncpable_type',
        'pncpable_id',
        'json_enviado_inclusao',
        'json_enviado_alteracao',
        'link_pncp',
        'situacao',
        'contrato_id',
        'sequencialPNCP',
        'tipo_contrato',
        'linkArquivoEmpenho',
        'retorno_pncp',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getSequencialSalvoLinkPncp()
    {
        $arrayLinkPncp = explode('/', $this->link_pncp) ;
        return end($arrayLinkPncp);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function status()
    {
        return $this->belongsTo(Codigoitem::class, 'situacao');
    }

    public function minuta_empenho()
    {
        return $this->morphedByMany(MinutaEmpenho::class, 'pncpable');
    }

    public function contrato_historico()
    {
        return $this->morphedByMany(Contratohistorico::class, 'pncpable');
    }

    public function contrato_arquivos()
    {
        return $this->morphedByMany(Contratoarquivo::class, 'pncpable');
    }

    public function envia_dados_pncp_historico()
    {
        return $this->hasMany(EnviaDadosPncpHistorico::class, 'envia_dados_pncp_id');
    }

    public function pncpable()
    {
        return $this->morphTo();
    }
}
