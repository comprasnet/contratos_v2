<?php

namespace App\Models;

use App\Models\ContratoBase as Model;
use App\Http\Traits\Formatador;
use App\Http\Traits\UsuarioFornecedor\UsuarioFornecedorTrait;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Contratopreposto extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;
    use Formatador;
    use UsuarioFornecedorTrait;
    use HasFactory;

    protected static $logFillable = true;
    protected static $logName = 'contratopreposto';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contratopreposto';
    protected $fillable = [
        'contrato_id',
        'user_id',
        'cpf',
        'nome',
        'email',
        'telefonefixo',
        'celular',
        'doc_formalizacao',
        'informacao_complementar',
        'data_inicio',
        'data_fim',
        'situacao',
        'status_indicacao_id'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getContrato()
    {
        return $this->getContratoNumero();
    }

    public function prepostoAPI($usuarioTransparencia)
    {
        return [
                'id' => $this->id,
                'contrato_id' => $this->contrato_id,
                'usuario' => $usuarioTransparencia,
                'email' => $this->email,
                'telefonefixo' => $this->telefonefixo,
                'celular' => $this->celular,
                'doc_formalizacao' => $this->doc_formalizacao,
                'informacao_complementar' => $this->informacao_complementar,
                'data_inicio' => $this->data_inicio,
                'data_fim' => $this->data_fim,
                'situacao' => $this->situacao == true ? 'Ativo' : 'Inativo',
        ];
    }

    public function buscaPrepostosPorContratoId(int $contrato_id, $range)
    {
        $prepostos = $this::whereHas('contrato', function ($c) {
            $c->whereHas('unidade', function ($u) {
                $u->where('sigilo', "=", false);
            });
        })
            ->where('contrato_id', $contrato_id)
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('contratopreposto.updated_at', [$range[0], $range[1]]);
            })
            ->get();

        return $prepostos;
    }

    public function buscaPrepostos($range)
    {
        $prepostos = $this::whereHas('contrato', function ($c) {
            $c->whereHas('unidade', function ($u) {
                $u->where('sigilo', "=", false);
            });
        })
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('contratopreposto.updated_at', [$range[0], $range[1]]);
            })
            ->get();

        return $prepostos;
    }

    public function getStatusIndicacaoDescricaoCores()
    {
        $descres  = $this->statusIndicacao ? $this->statusIndicacao->descres : null;

        return $this->exibirCorStatusIndicacaoPreposto($descres);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function user()
    {
        return $this->belongsTo(BackpackUser::class, 'user_id');
    }
    
    public function userCpf()
    {
        return $this->belongsTo(BackpackUser::class, 'cpf', 'cpf');
    }

    public function statusIndicacao()
    {
        return $this->belongsTo(CodigoItem::class, 'status_indicacao_id');
    }

    public function historico()
    {
        return $this->hasMany(HistoricoPrepostoIndicacaoFornecedor::class, 'contratopreposto_id');
    }

    public function arquivo()
    {
        return $this->belongsTo(Contratoarquivo::class, 'id', 'contratopreposto_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getMaskedCpfAttribute($value)
    {
        return $this->retornaMascaraCpf($this->cpf);
    }

    /**
     * Recupera a descrição do status da indicação.
     *
     * @return string|null
     */
    public function getStatusIndicacaoDescricaoAttribute()
    {
        return $this->statusIndicacao ? $this->statusIndicacao->descricao : null;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
