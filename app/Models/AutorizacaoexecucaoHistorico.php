<?php

namespace App\Models;

use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoTrait;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AutorizacaoexecucaoHistorico extends Model
{
    use CrudTrait;
    use AutorizacaoexecucaoTrait;
    use HasFactory;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'autorizacaoexecucao_historico';

    protected $fillable = [
        'tipo_historico_id',
        'situacao_id',
        'autorizacaoexecucoes_id',
        'usuario_responsavel_id',
        'tipo_id_antes',
        'tipo_id_depois',
        'tipo_alteracao',
        'sequencial',
        'processo_antes',
        'processo_depois',
        'numero_antes',
        'numero_depois',
        'numero_sistema_origem_antes',
        'numero_sistema_origem_depois',
        'data_assinatura_antes',
        'data_assinatura_depois',
        'informacoes_complementares_antes',
        'informacoes_complementares_depois',
        'data_vigencia_inicio_antes',
        'data_vigencia_inicio_depois',
        'data_vigencia_fim_antes',
        'data_vigencia_fim_depois',
        'url_arquivo_antes',
        'url_arquivo_depois',
        'data_assinatura_alteracao',
        'data_inicio_alteracao',
        'data_extincao',
        'justificativa_motivo',
        'rascunho'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getContrato()
    {
        return $this->autorizacaoexecucao->contrato;
    }

    public function getAntesDepoisColumnTable(string $column, string $atual = null)
    {
        $antes = $this->{$column . '_antes'};
        $depois = $this->{$column . '_depois'};

        $result = [];

        if (!empty($antes)) {
            $result['Antes'] = [$antes];
        } elseif (!empty($atual)) {
            $result['Atual'] = [$atual];
        }

        if (!empty($depois)) {
            $result['Depois'] = [$depois];
        }
        return $result;
    }

    public function getViewLink($arquivo)
    {
        return '<a target="_blank" href="' . url("storage/{$this->{$arquivo}}") . '">
            <i class="fas fa-eye"></i>
        </a>';
    }

    public function getAntesDepoisRelationshipColumnTable(string $relation, string $column)
    {
        $antes = $this->{$relation . 'Antes'} ? $this->{$relation . 'Antes'}->{$column} : null;
        $depois = $this->{$relation . 'Depois'} ? $this->{$relation . 'Depois'}->{$column} : null;

        if (!empty($antes) || !empty($depois)) {
            return ['Antes' => [$antes], 'Depois' => [$depois]];
        }
        return [];
    }

    public function getDataRetificacao()
    {
        return  Carbon::parse($this->created_at)->format('d/m/y H:i:s');
    }

    public function getDataExtincao()
    {
        return  Carbon::parse($this->data_extincao)->format('d/m/Y');
    }

    public function getDataAssinaturaAlteracao()
    {
        return $this->data_assinatura_alteracao ?
            Carbon::parse($this->data_assinatura_alteracao)->format('d/m/Y') :
            null;
    }

    public function getDataInicioAlteracao()
    {
        return  Carbon::parse($this->data_inicio_alteracao)->format('d/m/Y');
    }

    public function getUsuarioResponsavel()
    {
        if (!empty($this->usuarioResponsavel)) {
            return $this->retornaMascaraCpf($this->usuarioResponsavel->cpf) . ' ' . $this->usuarioResponsavel->name;
        }

        return null;
    }
    public function getViewButtonsAlterar($crud)
    {
        if ($this->situacao->descres == 'ae_status_1') {
            $crud->allowAccess('update');
            $crud->allowAccess('delete');
        } elseif ($this->situacao->descres == 'ae_status_6') {
            $crud->allowAccess('update');
            $crud->denyAccess('delete');
        } else {
            $crud->denyAccess('update');
            $crud->denyAccess('delete');
        }
    }

    public function getSequencial()
    {
        $ano = date_format($this->created_at, 'Y');
        $sequencialFormatado = str_pad($this->sequencial, 5, '0', STR_PAD_LEFT) . '/' . $ano;
        if ($this->rascunho) {
            return $sequencialFormatado." - R";
        }

        return  $sequencialFormatado;
    }

    public function getTipoAlteracao(): string
    {
        $result = '';
        $tiposAlteracao = explode(';', $this->tipo_alteracao);
        foreach ($tiposAlteracao as $tipo) {
            switch ($tipo) {
                case 'vigencia':
                    $result .= 'Vigência';
                    break;
                case 'itens':
                    $result .= 'Item(ns) (Acréscimo ou Supressão)';
                    break;
                case 'extingir':
                    $result .= 'Extingir Ordem de Serviço / Fornecimento';
                    break;
            }

            if ($tipo != end($tiposAlteracao)) {
                $result .= ', ';
            }
        }

        return $result;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function arquivo()
    {
        return $this->morphOne(ArquivoGenerico::class, 'arquivoable');
    }

    public function autorizacaoexecucao()
    {
        return $this->belongsTo(AutorizacaoExecucao::class, 'autorizacaoexecucoes_id');
    }

    public function tipoHistorico()
    {
        return $this->belongsTo(CodigoItem::class, 'tipo_historico_id');
    }

    public function tipoAntes()
    {
        return $this->belongsTo(CodigoItem::class, 'tipo_id_antes');
    }

    public function tipoDepois()
    {
        return $this->belongsTo(CodigoItem::class, 'tipo_id_depois');
    }

    public function autorizacaoexecucaoUnidadeRequisitantesHistorico()
    {
        return $this->belongsToMany(
            Unidade::class,
            'autorizacaoexecucao_unidade_requisitantes_historico',
            'autorizacaoexecucao_historico_id',
            'unidade_requisitante_id'
        )->withPivot('tipo_historico')
            ->withTimestamps();
    }

    public function autorizacaoexecucaoEmpenhosHistorico(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(
            Empenho::class,
            'autorizacaoexecucao_empenhos_historico',
            'autorizacaoexecucao_historico_id',
            'empenho_id'
        )->withPivot('tipo_historico')
            ->withTimestamps();
    }

    public function autorizacaoexecucaoLocaisExecucaoHistorico()
    {
        return $this->belongsToMany(
            ContratoLocalExecucao::class,
            'autorizacaoexecucao_contrato_local_execucao_historico',
            'autorizacaoexecucao_historico_id',
            'contrato_local_execucao_id'
        )->withPivot('tipo_historico')
            ->withTimestamps();
    }

    // !IMPORTANTE! manter o nome desse método igual ao da model Autorizacaoexecucao
    public function autorizacaoexecucaoItens()
    {
        return $this->hasMany(AutorizacaoexecucaoItensHistorico::class, "autorizacaoexecucao_historico_id");
    }

    public function usuarioResponsavel()
    {
        return $this->belongsTo(User::class, 'usuario_responsavel_id');
    }

    public function getItensAntes()
    {
        return $this->generateTableItens(
            $this->autorizacaoexecucaoItens()
                ->where('tipo_historico', 'antes')
                ->get()
        );
    }

    public function getItensDepois()
    {
        return $this->generateTableItens(
            $this->autorizacaoexecucaoItens()
                ->where('tipo_historico', 'depois')
                ->get()
        );
    }

    public function situacao()
    {
        return $this->belongsTo(CodigoItem::class, 'situacao_id');
    }

    public function verificaTipoAlteracao(string $tipo): bool
    {
        return in_array($tipo, explode(';', $this->tipo_alteracao));
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getDataAssinaturaAntesAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('d/m/Y') : null;
    }

    public function getDataAssinaturaDepoisAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('d/m/Y') : null;
    }

    public function getDataVigenciaInicioAntesAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('d/m/Y') : null;
    }

    public function getDataVigenciaInicioDepoisAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('d/m/Y') : null;
    }

    public function getDataVigenciaFimAntesAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('d/m/Y') : null;
    }

    public function getDataVigenciaFimDepoisAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('d/m/Y') : null;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
