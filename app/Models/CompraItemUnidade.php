<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class CompraItemUnidade extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected $table = 'compra_item_unidade';
    
    protected static $logFillable = true;
    protected static $logName = 'compra_item_unidade_v2';
    
    public $timestamps = true;

    protected $fillable = [
        'compra_item_id',
        'unidade_id',
        'fornecedor_id',
        'quantidade_autorizada',
        'quantidade_saldo',
        'tipo_uasg',
        'novo_gestao_ata',
        'situacao'
    ];

    protected $appends = ['codigo_unidade'];


    public function unidades()
    {
        return $this->belongsTo(Unidade::class, 'unidade_id');
    }

    public function compraItens()
    {
        return $this->belongsTo(CompraItem::class, 'compra_item_id');
    }
    
    public function fornecedor()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_id');
    }

    public function getCodigoUnidadeAttribute()
    {
        return $this->unidades->codigo . ' - ' .  $this->unidades->nomeresumido;
    }

    public function tipoUasgConvertido()
    {
        switch ($this->tipo_uasg) {
            case 'G':
                return 'Gerenciadora';
            break;
            case 'P':
                return 'Participante';
            break;
            case 'C':
                return 'Carona';
            break;
        }
    }
}
