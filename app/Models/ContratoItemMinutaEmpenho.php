<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Facades\DB;

class ContratoItemMinutaEmpenho extends Model
{
    use CrudTrait;
    use LogsActivity;
//    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected static $logFillable = true;
    protected static $logName = 'contrato_item_minuta_empenho';

    protected $table = 'contrato_item_minuta_empenho';
//    protected $primaryKey = ['contrato_item_id', 'minutaempenho_id'];
    protected $guarded = [
        //
    ];

    protected $fillable = [
        'contrato_item_id',   // Chave composta: 1/3
        'minutaempenho_id', // Chave composta: 2/3
        'subelemento_id',
        'operacao_id',
        'minutaempenhos_remessa_id',
        'quantidade',
        'valor',
        'numseq'
    ];

    /**
     * Informa que não utilizará os campos create_at e update_at do Laravel
     *
     * @var boolean
     */
    public $timestamps = true;

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function gravaContratoItemMinuta($params)
    {
        $this->contrato_item_id = $params['contrato_item_id'];
        $this->minutaempenho_id = $params['minutaempenho_id'];

        $this->save($params);

        return $this->contrato_item_id;
    }

    public function retornaItensMinuta($id_minuta) {

        $itens = $this->select(
            'contrato_item_minuta_empenho.contrato_item_id',                    
            'contrato_item_minuta_empenho.quantidade',
            'contrato_item_minuta_empenho.valor',
            DB::raw('TO_NUMBER(contrato_item_minuta_empenho.numseq, \'9999\') as sequencial_siafi'),
            'codigoitens.descres as tipo_operacao',
            'contratoitens.numero_item_compra as numero_item_compra',
            'catmatseritens.codigo_siasg as codigo_item',
            'catmatseritens.descricao',
            'contratoitens.valorunitario',
            'contratoitens.descricao_complementar as descricao_detalhada')
            ->join('contratoitens', 'contratoitens.id', 'contrato_item_minuta_empenho.contrato_item_id')
            ->join('catmatseritens', 'catmatseritens.id', 'contratoitens.catmatseritem_id')
            ->join('codigoitens', 'codigoitens.id', 'contrato_item_minuta_empenho.operacao_id')
            ->where('contrato_item_minuta_empenho.minutaempenho_id', $id_minuta)
            ->whereIn('codigoitens.descres', ['INCLUSAO'])
            ->orderByRaw('sequencial_siafi ASC')
            ->get();
        
        return $itens;

    }

    public function itemAPI($saldo_quantidade_item,$saldo_valor_total_item)
    {
        return [            
            'sequencial_siafi' => $this->sequencial_siafi,
            'numero_item_compra' => $this->numero_item_compra,
            'codigo_item' => $this->codigo_item,
            'descricao' => $this->descricao,
            'descricao_detalhada' => $this->descricao_detalhada,
            'quantidade' => number_format($saldo_quantidade_item, 5, ',', '.'),
            'valor_unitario' => number_format($this->valorunitario, 4, ',', '.'),
            'valor_total' => number_format($saldo_valor_total_item, 2, ',', '.')
        ];
    }    

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function contrato_item()
    {
        return $this->belongsTo(Contratoitem::class, 'contrato_item_id');
    }

    public function minutaempenho()
    {
        return $this->belongsTo(MinutaEmpenho::class, 'minutaempenho_id');
    }

    public function subelemento()
    {
        return $this->belongsTo(Naturezasubitem::class, 'subelemento_id');
    }

    public function minutaempenhos_remessa()
    {
        return $this->belongsTo(MinutaEmpenhoRemessa::class, 'minutaempenhos_remessa_id');
    }

    public function operacao()
    {
        return $this->belongsTo(Codigoitem::class, 'operacao_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getSituacaoRemessaAttribute(): string
    {
        return $this->minutaempenhos_remessa->situacao->descricao;
    }

    public function getOperacaoAttribute(): string
    {
        return $this->operacao()->first()->descricao;
    }

    public function getOperacaoDescresAttribute(): string
    {
        return $this->operacao()->first()->descres;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
