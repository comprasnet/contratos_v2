<?php

namespace App\Models;

use App\Http\Traits\Arp\ArpTrait;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ArpPdf extends Model
{
    protected $table = 'arp_pdf';

    protected $fillable = [
        'arp_id',
        'caminho_pdf',
        'gerado',
    ];

    public function arp()
    {
        return $this->belongsTo(Arp::class, 'arp_id');
    }
}
