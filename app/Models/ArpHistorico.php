<?php

namespace App\Models;

use App\Casts\DateGovBr;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArpHistorico extends Model
{
    use CrudTrait;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'arp_historico';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'unidade_origem_id',
        'compra_id',
        'tipo_id',
        'data_assinatura',
        'vigencia_inicial',
        'vigencia_final',
        'valor_total',
        'numero',
        'ano',
        'modalidade_id',
        'objeto_alteracao',
        'arp_id',
        'compra_centralizada',
        'data_assinatura_alteracao_inicio_vigencia',
        'data_assinatura_alteracao_fim_vigencia',
        'arquivo_alteracao',
        'data_assinatura_alteracao_vigencia',
        'arp_alteracao_id',
        'objeto',
        'descricao_anexo',
        'objeto_alteracao',
        'vigencia_inicial_alteracao',
        'vigencia_final_alteracao',
        'data_assinatura_alteracao',
        'compra_centralizada_alteracao',
        'justificativa_motivo_cancelamento',
        'sequencial',
        'responsavel_acao_id',
        'numero_alteracao',
        'quantitativo_renovado_vigencia',
        'amparo_legal_renovacao_quantitativo',
        'unidade_executora_id',
        'justificativa_alteracao_vigencia'
    ];
    // protected $hidden = [];
    // protected $dates = [];
    public static $datesGovBr = ['data_assinatura_alteracao_vigencia'];

    protected $casts = [
        'compra_centralizada' => 'boolean',
        'compra_centralizada_alteracao' => 'boolean'
    ];
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getTipoAlteracao()
    {
        return $this->status->descricao;
    }

    public function getDataRetificacao()
    {
        return Carbon::parse($this->created_at)->format('d/m/y H:i:s');
    }

    public function getCompraCentralizadaAlteracao()
    {
        if ($this->compra_centralizada_alteracao === null) {
            return;
        }

        if ($this->compra_centralizada_alteracao) {
            return "Sim";
        }

        return "Não";
    }

    public static function getAutoridadeSignataria(int $idAta)
    {
        $arp = ArpHistorico::find($idAta);
        $autoridades = array();
        foreach ($arp->autoridade_signataria as $key => $autoridade_signataria) {
            $autoridades['Nome'][$key] = $autoridade_signataria->user->autoridade_signataria;
            $autoridades['Cargo'][$key] = $autoridade_signataria->user->cargo_autoridade_signataria;
        }

        return $autoridades;
    }

    public function getSequencial()
    {
        $sequencialFormatado = str_pad($this->sequencial, 4, '0', STR_PAD_LEFT);
        if ($this->rascunho) {
            return $sequencialFormatado . " - R";
        }

        return $sequencialFormatado;
    }

    public function getResponsavel()
    {
        return isset($this->responsavel->name) ? $this->responsavel->name : null;
    }
    
    public function getAnoAlteracao()
    {
        if ($this->arp->ano != $this->ano) {
            return $this->arp->ano;
        }
        
        return '';
    }



    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function status()
    {
        return $this->belongsTo(CodigoItem::class, 'tipo_id');
    }

    public function autoridade_signataria()
    {
        return $this->hasMany(ArpAutoridadeSignatariaHistorico::class, 'arp_historico_id');
    }

    public function responsavel()
    {
        return $this->belongsTo(BackpackUser::class, "responsavel_acao_id");
    }

    public function item_historico()
    {
        return $this->hasMany(ArpItemHistorico::class, "arp_historico_id");
    }

    public function alteracao()
    {
        return $this->hasMany(ArpAlteracao::class, 'arp_historico_id');
    }

    public function alteracaoHistorico()
    {
        return $this->belongsTo(ArpAlteracao::class, "arp_alteracao_id", "id");
    }

    public function item()
    {
        // dd($this->arp_id);
        return $this->hasMany(ArpItem::class, "arp_id", "arp_id");
    }

    public function arp()
    {
        return $this->hasOne(Arp::class, "id", "arp_id");
    }
    
    public function itemAlteracaoFornecedor()
    {
        return $this->hasMany(ArpItemHistoricoAlteracaoFornecedor::class, "arp_historico_id", "id");
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setArquivoAlteracaoAttribute($value)
    {
        $attribute_name = "arquivo_alteracao";
        $disk = "local";
        $destination_path = "alteracao_arp/" . $this->id;

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);

        // return $this->attributes[{$attribute_name}]; // uncomment if this is a translatable field
    }

    public function getNomeAttribute()
    {
        $user = $this->user()->first();
        return $user->autoridade_signataria . ' - ' . $user->cargo_autoridade_signataria;
    }
}
