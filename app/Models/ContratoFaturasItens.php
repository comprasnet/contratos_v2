<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class ContratoFaturasItens extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'contratofaturas_itens';
    protected $table = 'contratofaturas_itens';

    protected $fillable = [
        'contratofaturas_id',
        'saldohistoricoitens_id',
        'quantidade_faturado',
        'valorunitario_faturado',
        'valortotal_faturado',
    ];

    /*
   |--------------------------------------------------------------------------
   | RELATIONS
   |--------------------------------------------------------------------------
   */
    public function saldohistoricoitem()
    {
        return $this->hasMany(Saldohistoricoitem::class, 'saldohistoricoitens_id');
    }

    public function contratofaturas()
    {
        return $this->belongsTo(Contratofatura::class, 'contratofaturas_id');
    }
}
