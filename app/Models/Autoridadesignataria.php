<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class AutoridadeSignataria extends Model
{
    use CrudTrait;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'autoridadesignataria';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['unidade_id', 'autoridade_signataria', 'cargo_autoridade_signataria', 'titular', 'ativo'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getAtivo()
    {
        if ($this->ativo) {
            return 'Ativa';
        }
        
        return 'Inativa';
    }
    public function getTitular()
    {
        if ($this->titular) {
            return 'Titular';
        }

        return 'Substituta';
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public static function autoridadeSignatariaArp(?string $search_term)
    {
        if ($search_term) {
            $search_term = strtoupper($search_term);
            $results = AutoridadeSignataria::where("unidade_id", session('user_ug_id'))
            ->where(function ($query) use ($search_term) {
                $query->where('autoridade_signataria', 'ILIKE', "%{$search_term}%")
                      ->orWhere('cargo_autoridade_signataria', 'ILIKE', "%{$search_term}%");
            })
            ->select('id', DB::raw("CONCAT(autoridade_signataria,' - ',cargo_autoridade_signataria) AS nome"))
            ->where('ativo', true)
            ->paginate(10);
        } else {
            $results = AutoridadeSignataria:: where("unidade_id", session('user_ug_id'))
            ->select('id', DB::raw("CONCAT(autoridade_signataria,' - ',cargo_autoridade_signataria) AS nome"))
            ->paginate(10);
        }

        return $results;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
