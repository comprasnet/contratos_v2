<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContratoParametro extends Model
{
    use HasFactory;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contrato_parametros';
    protected $fillable = [
        'pz_recebimento_provisorio',
        'pz_recebimento_definitivo',
        'pz_pagamento',
        'data_pagamento_recorrente',
        'registro',
        'contrato_id',
        'created_at',
        'updated_at',
        'tipo_recebimento_provisorio',
        'tipo_recebimento_definitivo',
        'tipo_pagamento',
        'tipo_pagamento_recorrente',
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function contrato()
    {
        return $this->belongsTo(Contrato::class);
    }
}
