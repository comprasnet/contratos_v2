<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdesaoItem extends Model
{
    use CrudTrait;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'arp_solicitacao_item';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function getItemPorAdesao(int $idAdesao)
    {
        $itens = AdesaoItem::where("arp_solicitacao_id", $idAdesao)->get();

        $arrayItens = [];
        $count = 0;
        foreach ($itens as $item) {
            $itemArp = $item->item_arp()->withTrashed()->first();

            $arrayItens['Fornecedor'][$count] = $itemArp->item_fornecedor_compra->getFornecedor();
            $arrayItens['Número'][$count] = $itemArp->item_fornecedor_compra->compraItens->numero;
            $arrayItens['Descrição'][$count] = $itemArp->item_fornecedor_compra->compraItens->descricaodetalhada;
            $arrayItens['Quantidade_Solicitada'][$count] = $item->quantidade_solicitada;
            $arrayItens['Quantidade_Autorizada'][$count] = $item->quantidade_aprovada;
            $arrayItens['Justificativa'][$count] = $item->motivo_analise;
            //$arrayItens['Status'][$count] = $item->status->descricao;
            if ($item->status->descricao === 'Aceitar Parcialmente') {
                $arrayItens['Status'][$count] = 'Aceita parcialmente';
            } elseif ($item->status->descricao === 'Aceitar') {
                $arrayItens['Status'][$count] = 'Aceita';
            } elseif ($item->status->descricao === 'Negar') {
                $arrayItens['Status'][$count] = 'Negado';
            } else {
                $arrayItens['Status'][$count] = $item->status->descricao;
            }
            $count++;
        }

        return $arrayItens;
    }

    public static function getItemAnalisadoPorAdesao(int $idAdesao)
    {
        $itens = AdesaoItem::where("arp_solicitacao_id", $idAdesao)
                            ->where("quantidade_solicitada", ">", 0)
                            ->get();
        $arrayItens = [];
        foreach ($itens as $key => $item) {
            $arrayItens['Número'][$key] = $item->item_arp->item_fornecedor_compra->compraItens->numero;
            $arrayItens['Descrição'][$key] = $item->item_arp->item_fornecedor_compra->compraItens->descricaodetalhada;
            $arrayItens['Quantidade_Solicitada'][$key] = $item->quantidade_solicitada;
            $arrayItens['Quantidade_Aprovada'][$key] = $item->quantidade_aprovada;
            $arrayItens['Fornecedor'][$key] = $item->
            item_arp->item_fornecedor_compra->fornecedor->
            getNomeFornecedorCompleto();

            switch ($item->status->descricao) {
                case 'Item Não Avaliado':
                    break;
                case 'Aceitar':
                     $item->status->descricao ='Aceito';
                    break;
                case 'Negar':
                    $item->status->descricao ='Negado';
                    break;
                case 'Aceitar Parcialmente':
                    $item->status->descricao ='Aceito Parcialmente';
                    break;
            }

            $arrayItens['Status'][$key] = $item->status->descricao;
            $arrayItens['Motivo/Justificativa'][$key] = $item->motivo_analise;
        }

        return $arrayItens;
    }

    public function getNumero()
    {
        return $this->item_arp->item_fornecedor_compra->compraItens->numero;
    }

    public function getNumeroGeral()
    {
        return $this->item_arp->arp->numero . '/' . $this->item_arp->arp->ano;
    }
    public function getDescricao()
    {
        return $this->item_arp->item_fornecedor_compra->compraItens->descricaodetalhada;
    }


    public function getStatus()
    {
        return $this->status->descricao;
    }

    public function getVigenciaAtaAnaliseAdesao()
    {
        return (new Carbon($this->item_arp->item_fornecedor_compra->compraItens->ata_vigencia_inicio))
                ->format('d/m/y') .
            "<br>-<br>" .
            (new Carbon($this->item_arp->item_fornecedor_compra->compraItens->ata_vigencia_fim))
                ->format('d/m/y');
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function item_arp()
    {
        return $this->belongsTo(ArpItem::class, 'item_arp_fornecedor_id');
    }


    public function status()
    {
        return $this->belongsTo(CodigoItem::class, 'status_id');
    }
    
    public function adesao()
    {
        return $this->belongsTo(Adesao::class, 'arp_solicitacao_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
