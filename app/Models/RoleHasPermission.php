<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;

class RoleHasPermission extends Model
{
    use CrudTrait;
    use LogsActivity;

    protected static $logFillable = true;
    protected static $logName = 'role_has_permissions';
    public $timestamps = false;
    
    protected $table = 'role_has_permissions';

    protected $fillable = ['permission_id', 'role_id'];
}
