<?php

namespace App\Models;

use App\Http\Traits\Formatador;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AutorizacaoexecucaoItens extends Model
{

    use CrudTrait;

    use Formatador;
    use HasFactory;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'autorizacaoexecucao_itens';

    protected $fillable = [
        'autorizacaoexecucoes_id',
        'contratohistorico_id',
        'saldohistoricoitens_id',
        'especificacao',
        'quantidade',
        'parcela',
        'valor_unitario',
        'valor_total',
        'horario',
        'horario_fim',
        'subcontratacao',
        'unidade_medida_id',
        'numero_demanda_sistema_externo',
        'localidade_de_execucao',
        'numero_demanda_sistema_externo',
        'localidade_de_execucao',
        'periodo_vigencia_inicio',
        'periodo_vigencia_fim',
    ];

    protected $appends = ['contratoitem_id', 'periodo'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function autorizacaoexecucao()
    {
        return $this->belongsTo(Autorizacaoexecucao::class, 'autorizacaoexecucoes_id');
    }

    public function saldohistoricoitem()
    {
        return $this->belongsTo(Saldohistoricoitem::class, 'saldohistoricoitens_id');
    }

    public function unidadeMedida()
    {
        return $this->belongsTo(UnidadeMedida::class, 'unidade_medida_id');
    }

    public function itensEntrega()
    {
        return $this->hasOne(AutorizacaoExecucaoEntregaItens::class, 'autorizacao_execucao_itens_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function getTotal()
    {
        return $this->retornaCampoFormatadoComoNumero($this->getTotalSemFormatar());
    }

    public function getQuantidadeTotal()
    {
        return str_replace('.', ',', $this->getQuantidadeTotalSemFormatar());
    }

    public function getQuantidadeTotalSemFormatar()
    {
        return (float) $this->attributes['quantidade'] * $this->attributes['parcela'];
    }

    public function getTotalSemFormatar()
    {
        return $this->attributes['quantidade'] * $this->attributes['parcela'] * $this->attributes['valor_unitario'];
    }

    public function getPeriodoAttribute()
    {
        if (!empty($this->attributes['periodo_vigencia_inicio']) && !empty($this->attributes['periodo_vigencia_fim'])) {
            return "De: {$this->retornaDataAPartirDeCampo($this->attributes['periodo_vigencia_inicio'])}" .
                " Até: {$this->retornaDataAPartirDeCampo($this->attributes['periodo_vigencia_fim'])}";
        }

        return '::Legado::';
    }

    public function getContratoItemIdAttribute()
    {
        $saldohistoricoitem = $this->saldohistoricoitem;

        return $saldohistoricoitem->contratoitem_id;
    }

    public function getTipoItemAttribute()
    {
        $saldohistoricoitem = $this->saldohistoricoitem;

        return $saldohistoricoitem->getTipoItem();
    }

    public function getNumeroItemCompraAttribute()
    {
        $saldohistoricoitem = $this->saldohistoricoitem;

        return $saldohistoricoitem->numero_item_compra;
    }

    public function getItemAttribute()
    {
        $saldohistoricoitem = $this->saldohistoricoitem;

        return $saldohistoricoitem->contratoItem->getCatmatseritem(true);
    }

    public function getvalorUnitarioContratadoAttribute()
    {
        $saldohistoricoitem = $this->saldohistoricoitem;

        return $this->retornaCampoFormatadoComoNumero($saldohistoricoitem->valorunitario, false, 4);
    }

    public function getQuantidadeAttribute()
    {
        return str_replace('.', ',', (float) $this->attributes['quantidade']);
    }

    public function getValorUnitarioAttribute()
    {
        return $this->retornaCampoFormatadoComoNumero($this->attributes['valor_unitario'], false, 2);
    }

    public function getValorUnitarioSemFormatarAttribute()
    {
        return $this->attributes['valor_unitario'];
    }

    public function getHorarioAttribute()
    {
        $datetime = \DateTime::createFromFormat('H:i:s', $this->attributes['horario']);

        if ($datetime) {
            return $datetime->format('H:i');
        }
    }

    public function getHorarioFimAttribute()
    {
        $datetime = \DateTime::createFromFormat('H:i:s', $this->attributes['horario_fim']);

        if ($datetime) {
            return $datetime->format('H:i');
        }
    }
}
