<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntregaItem extends Model
{
    use HasFactory;
    use CrudTrait;

    protected $table = 'entrega_itens';

    protected $fillable = [
        'entrega_id',
        'itemable_id',
        'itemable_type',
        'quantidade_informada',
        'valor_glosa',
    ];

    public function entrega()
    {
        return $this->belongsTo(Entrega::class, 'entrega_id');
    }
}
