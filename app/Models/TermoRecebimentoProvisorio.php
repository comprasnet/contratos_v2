<?php

namespace App\Models;

use App\Http\Traits\EntregaTrpTrd\TrpTrdTrait;
use App\Http\Traits\SignatarioTrait;
use App\Http\Traits\Unidade\ProcessoTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Facades\DB;

class TermoRecebimentoProvisorio extends Model
{
    use TrpTrdTrait;
    use SignatarioTrait;
    use ProcessoTrait;

    protected $table = 'termos_recebimento_provisorio';

    protected $fillable = [
        'contrato_id',
        'situacao_id',
        'numero',
        'rascunho',
        'valor_total',
        'introducao',
        'informacoes_complementares',
        'originado_sistema_externo'
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function itens(): HasMany
    {
        return $this->hasMany(TermoRecebimentoProvisorioItem::class);
    }

    public function autorizacaoItens(): MorphToMany
    {
        return $this->morphedByMany(
            AutorizacaoexecucaoItens::class,
            'itemable',
            'termo_recebimento_provisorio_itens'
        )->withPivot('id', 'quantidade_informada', 'valor_glosa');
    }

    public function entregas()
    {
        return $this->belongsToMany(
            Entrega::class,
            'termo_recebimento_provisorio_entrega',
            'termo_recebimento_provisorio_id',
            'entrega_id'
        );
    }


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getSituacaoChipComponent()
    {
        switch ($this->situacao->descres) {
            case 'trp_status_1':
                $cor = '#a3a3a3';
                $corFont = '#fff';
                break;
            case 'trp_status_2':
                $cor = '#f5a623';
                $corFont = '#fff';
                break;
            case 'trp_status_3':
                $cor = '#168821';
                $corFont = '#fff';
                break;
            case 'trp_status_4':
                $cor = '#1351b4';
                $corFont = '#fff';
                break;
            default:
                $cor = '#000';
                $corFont = '#fff';
                break;
        }

        return view(
            'components_v2.chip_status',
            [
                'conteudo' => $this->situacao->descricao,
                'cor' => $cor,
                'corFont' => $corFont
            ]
        );
    }

    public function getRedirectAutorizacoesComponent()
    {
        return view(
            'crud::columns.redirect-autorizacoes',
            [
                'autorizacoes' => $this->getAutorizacoes()
            ]
        );
    }

    public function getAutorizacoes()
    {
        return DB::table('termo_recebimento_provisorio_itens')
            ->select('autorizacaoexecucoes.*')
            ->distinct()
            ->join('autorizacaoexecucao_itens', function ($query) {
                $query->on('autorizacaoexecucao_itens.id', 'itemable_id')
                    ->where('itemable_type', AutorizacaoexecucaoItens::class);
            })
            ->join(
                'autorizacaoexecucoes',
                'autorizacaoexecucoes.id',
                '=',
                'autorizacaoexecucao_itens.autorizacaoexecucoes_id'
            )
            ->where('termo_recebimento_provisorio_itens.termo_recebimento_provisorio_id', $this->id)
            ->get();
    }

    public function getAnexos()
    {
        return DB::table('termos_recebimento_provisorio')
            ->join(
                'termo_recebimento_provisorio_entrega',
                'termo_recebimento_provisorio_entrega.termo_recebimento_provisorio_id',
                '=',
                'termos_recebimento_provisorio.id'
            )
            ->join(
                'entregas',
                'entregas.id',
                '=',
                'termo_recebimento_provisorio_entrega.entrega_id'
            )
            ->join('arquivo_generico', function ($query) {
                $query->on('arquivo_generico.arquivoable_id', 'termo_recebimento_provisorio_entrega.entrega_id')
                    ->where('arquivo_generico.arquivoable_type', '=', Entrega::class);
            })
            ->where('termos_recebimento_provisorio.id', $this->id)
            ->get();
    }

    public function getViewButtons($crud)
    {
        if ($this->situacao->descres == 'trp_status_1') {
            $crud->allowAccess('update');
            $crud->allowAccess('delete');
        } else {
            $crud->denyAccess('update');
            $crud->denyAccess('delete');
        }

        if (in_array($this->situacao->descres, ['trp_status_3', 'trp_status_4'])) {
            return view(
                'crud::buttons.button_trd',
                ['prependIcon' => false, 'link' => '/trd/' . $this->contrato_id . '/'. $this->getKey()]
            );
        }
    }
}
