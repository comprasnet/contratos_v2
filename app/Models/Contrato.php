<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Spatie\Activitylog\Traits\LogsActivity;
use function foo\func;

// use Illuminate\Database\Eloquent\SoftDeletes;

class Contrato extends Model
{
    use CrudTrait;
    use LogsActivity;
    use HasFactory;

    // use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'contrato';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contratos';
    protected $fillable = [
        'numero',
        'fornecedor_id',
        'unidade_id',
        'unidadeorigem_id',
        'tipo_id',
        'subtipo',
        'categoria_id',
        'subcategoria_id',
        'processo',
        'objeto',
        'info_complementar',
        'receita_despesa',
        'fundamento_legal',
        'modalidade_id',
        'licitacao_numero',
        'data_assinatura',
        'data_publicacao',
        'vigencia_inicio',
        'vigencia_fim',
        'valor_inicial',
        'valor_global',
        'num_parcelas',
        'valor_parcela',
        'valor_acumulado',
        'situacao_siasg',
        'situacao',
        'unidades_requisitantes',
        'unidadecompra_id',
        'numero_compra',
        'publicado',
        'prorrogavel',
        'unidadebeneficiaria_id',
        'is_prazo_indefinido',
        'justificativa_contrato_inativo_id',

        'data_encerramento',
        'is_objeto_contratual_entregue',
        'is_cumpridas_obrigacoes_financeiras',
        'is_saldo_conta_vinculada_liberado',
        'is_garantia_contratual_devolvida',

        'hora_encerramento',
        'nome_responsavel_encerramento',
        'cpf_responsavel_encerramento',
        'user_id_responsavel_encerramento',
        'aplicavel_decreto',
        'cronograma_automatico'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    // método que retorna as justificativas que estão salvas em codigoitens
    public static function getArrayComJustificativas()
    {
        $array = CodigoItem::whereHas('codigo', function ($query) {
                $query->where('descricao', '=', 'Justificativa Contrato Inativo');
        })
            ->orderBy('descricao')
            ->pluck('descricao', 'id')
            ->toArray();
        return $array;
    }
    public function getSituacao()
    {
        if ($this->situacao) {
            return 'Ativo';
        } elseif ($this->justificativa_contrato_inativo_id == null) {
            return 'Inativo';
        } else {
            // aqui a situação do contrato é inativo. vamos buscar a justificativa em codigo itens
            $objJustificativa = CodigoItem::where('id', $this->justificativa_contrato_inativo_id)->first();
            if (is_object($objJustificativa)) {
                $descricao = $objJustificativa->descricao;
                return 'Inativo ('.$descricao.')';
            } else {
                return 'Inativo';
            }
        }
    }
    public function inserirContratoMigracaoConta(array $dados)
    {
        $this->fill($dados);
        $this->save();

        return $this;
    }

    // Alterado para contemplar Autoridades Signatárias - mvascs@gmail.com 09/03/2022
    public function buscaListaContratosUg($filtro)
    {
        $unidade_user = Unidade::find(session()->get('user_ug_id'));
        $sql = '    select                                                                                                                  ';
        $sql .= "           CONCAT(O.codigo, ' - ', O.nome) as orgao,                                                                       ";
        $sql .= "           CONCAT(U.codigo, ' - ', U.nomeresumido) as unidade,                                                             ";
        $sql .= '           CASE                                                                                                            ';
        $sql .= "               WHEN    contratos.receita_despesa = 'D'                                                                     ";
        $sql .= "               THEN    'Despesa'                                                                                           ";
        $sql .= "               ELSE    'Receita'                                                                                           ";
        $sql .= '           END as receita_despesa,                                                                                         ';
        $sql .= '           contratos.unidades_requisitantes, contratos.numero,                                                             ';
        $sql .= '           F.cpf_cnpj_idgener as fornecedor_codigo, F.nome as fornecedor_nome,                                             ';
        $sql .= '           T.descricao as tipo,                                                                                            ';
        $sql .= '           C.descricao as categoria,                                                                                       ';
        $sql .= '           S.descricao as subcategoria,                                                                                    ';
        $sql .= '           contratos.processo, contratos.objeto, contratos.info_complementar,                                              ';
        $sql .= '           M.descricao as modalidade,                                                                                      ';
        $sql .= '           contratos.licitacao_numero, contratos.data_assinatura, contratos.data_publicacao,                               ';
        $sql .= '           contratos.vigencia_inicio, contratos.vigencia_fim, contratos.valor_inicial,                                     ';
        $sql .= '           contratos.valor_global, contratos.num_parcelas, contratos.valor_parcela, contratos.valor_acumulado,             ';
        $sql .= '           CASE                                                                                                            ';
        $sql .= "               WHEN    contratos.situacao = 't'                                                                            ";
        $sql .= "               THEN    'Ativo'                                                                                             ";
        $sql .= "               ELSE    'Inativo'                                                                                           ";
        $sql .= '           END as situacao,                                                                                                ';
        $sql .= '           contratos.unidades_requisitantes,                                                                               ';
        $sql .= '           CASE                                                                                                            ';
        $sql .= "               WHEN    contratos.prorrogavel = 't'     THEN    'Sim'                                                       ";
        $sql .= "               WHEN    contratos.prorrogavel = 'f'     THEN    'Não'                                                       ";
        $sql .= '           END as prorrogavel,                                                                                             ';
        $sql .= '           (                                                                                                               ';
        $sql .= '                   select   aut3.autoridade_signataria                                                                     ';
        $sql .= '                   from     autoridadesignataria aut3                                                                      ';
        $sql .= '                   where    aut3.ativo = true                                                                              ';
        $sql .= '                   and      aut3.id in (                                                                                   ';
        $sql .= '                               select  cas.autoridadesignataria_id                                                         ';
        $sql .= '                               from    contrato_autoridade_signataria cas                                                  ';
        $sql .= '                               where   cas.contratohistorico_id = H.id                                                     ';
        $sql .= '                   )                                                                                                       ';
        $sql .= '           ) as autoridade_signataria,                                                                                     ';
        $sql .= '           (                                                                                                               ';
        $sql .= '                   select                                                                                                  ';
        $sql .= '                       CASE                                                                                                ';
        $sql .= '                           WHEN    aut4.titular = true                                                                     ';
        $sql .= "                           THEN    'Titular'                                                                               ";
        $sql .= "                           ELSE    'Substituta'                                                                            ";
        $sql .= "                        END as qualificacao_da_autoridade                                                                  ";
        $sql .= '                   from     autoridadesignataria aut4                                                                      ';
        $sql .= '                   where    aut4.ativo = true                                                                              ';
        $sql .= '                   and      aut4.id in (                                                                                   ';
        $sql .= '                               select  cas.autoridadesignataria_id                                                         ';
        $sql .= '                               from    contrato_autoridade_signataria cas                                                  ';
        $sql .= '                               where   cas.contratohistorico_id = H.id                                                     ';
        $sql .= '                   )                                                                                                       ';
        $sql .= '           ) as qualificacao_da_autoridade                                                                                 ';
        $sql .= '   from    contratos                                                                                                       ';
        $sql .= '   left    join orgaosubcategorias as S on S.id = contratos.subcategoria_id                                                ';
        $sql .= '   left    join codigoitens as M on M.id = contratos.modalidade_id                                                         ';
        $sql .= '   left    join codigoitens as C on C.id = contratos.categoria_id                                                          ';
        $sql .= '   left    join codigoitens as T on T.id = contratos.tipo_id                                                               ';
        $sql .= '   left    join fornecedores as F on F.id = contratos.fornecedor_id                                                        ';
        $sql .= '   left    join unidades as U on U.id = contratos.unidade_id                                                               ';
        $sql .= '   left    join orgaos as O on O.id = U.orgao_id                                                                           ';
        $sql .= '   left    join contratohistorico as H on H.contrato_id = contratos.id and H.tipo_id = (                                   ';
        $sql .= '           select  id from codigoitens where descricao =                                                                   ';
        $sql .= "           'Contrato'                                                                                                      ";
        $sql .= '           and     codigo_id = ( select id from codigos where descricao =                                                  ';
        $sql .= "           'Tipo de Contrato'";
        $sql .= '    ) )                                                                                                                    ';
        $sql .= "   where U.id = $unidade_user->id                                                                                          ";
        $sql .= '   order   by contratos.numero, fornecedor_codigo                                                                          ';

        $resultado2 = DB::select($sql);
        return $resultado2;
    }

    public function buscaListaContratosOrgao($filtro)
    {
        $unidade_user = Unidade::find(session()->get('user_ug_id'));
        $idOrgaoUnidade = $unidade_user->orgao->id;

        $sql = '    select                                                                                                                  ';
        $sql .= "           CONCAT(O.codigo, ' - ', O.nome) as orgao,                                                                       ";
        $sql .= "           CONCAT(U.codigo, ' - ', U.nomeresumido) as unidade,                                                             ";
        $sql .= '           CASE                                                                                                            ';
        $sql .= "               WHEN    contratos.receita_despesa = 'D'                                                                     ";
        $sql .= "               THEN    'Despesa'                                                                                           ";
        $sql .= "               ELSE    'Receita'                                                                                           ";
        $sql .= '           END as receita_despesa,                                                                                         ';
        $sql .= '           contratos.unidades_requisitantes, contratos.numero,                                                             ';
        $sql .= '           F.cpf_cnpj_idgener as fornecedor_codigo, F.nome as fornecedor_nome,                                             ';
        $sql .= '           T.descricao as tipo,                                                                                            ';
        $sql .= '           C.descricao as categoria,                                                                                       ';
        $sql .= '           S.descricao as subcategoria,                                                                                    ';
        $sql .= '           contratos.processo, contratos.objeto, contratos.info_complementar,                                              ';
        $sql .= '           M.descricao as modalidade,                                                                                      ';
        $sql .= '           contratos.licitacao_numero, contratos.data_assinatura, contratos.data_publicacao,                               ';
        $sql .= '           contratos.vigencia_inicio, contratos.vigencia_fim, contratos.valor_inicial,                                     ';
        $sql .= '           contratos.valor_global, contratos.num_parcelas, contratos.valor_parcela, contratos.valor_acumulado,             ';
        $sql .= '           CASE                                                                                                            ';
        $sql .= "               WHEN    contratos.situacao = 't'                                                                            ";
        $sql .= "               THEN    'Ativo'                                                                                             ";
        $sql .= "               ELSE    'Inativo'                                                                                           ";
        $sql .= '           END as situacao,                                                                                                ';
        $sql .= '           contratos.unidades_requisitantes,                                                                               ';
        $sql .= '           CASE                                                                                                            ';
        $sql .= "               WHEN    contratos.prorrogavel = 't'     THEN    'Sim'                                                       ";
        $sql .= "               WHEN    contratos.prorrogavel = 'f'     THEN    'Não'                                                       ";
        $sql .= '           END as prorrogavel,                                                                                             ';
        $sql .= '           (                                                                                                               ';
        $sql .= '                   select   aut3.autoridade_signataria                                                                     ';
        $sql .= '                   from     autoridadesignataria aut3                                                                      ';
        $sql .= '                   where    aut3.ativo = true                                                                              ';
        $sql .= '                   and      aut3.id in (                                                                                   ';
        $sql .= '                               select  cas.autoridadesignataria_id                                                         ';
        $sql .= '                               from    contrato_autoridade_signataria cas                                                  ';
        $sql .= '                               where   cas.contratohistorico_id = H.id                                                     ';
        $sql .= '                   )                                                                                                       ';
        $sql .= '           ) as autoridade_signataria,                                                                                     ';
        $sql .= '           (                                                                                                               ';
        $sql .= '                   select                                                                                                  ';
        $sql .= '                       CASE                                                                                                ';
        $sql .= '                           WHEN    aut4.titular = true                                                                     ';
        $sql .= "                           THEN    'Titular'                                                                               ";
        $sql .= "                           ELSE    'Substituta'                                                                            ";
        $sql .= "                        END as qualificacao_da_autoridade                                                                  ";
        $sql .= '                   from     autoridadesignataria aut4                                                                      ';
        $sql .= '                   where    aut4.ativo = true                                                                              ';
        $sql .= '                   and      aut4.id in (                                                                                   ';
        $sql .= '                               select  cas.autoridadesignataria_id                                                         ';
        $sql .= '                               from    contrato_autoridade_signataria cas                                                  ';
        $sql .= '                               where   cas.contratohistorico_id = H.id                                                     ';
        $sql .= '                   )                                                                                                       ';
        $sql .= '           ) as qualificacao_da_autoridade                                                                                 ';
        $sql .= '   from    contratos                                                                                                       ';
        $sql .= '   left    join orgaosubcategorias as S on S.id = contratos.subcategoria_id                                                ';
        $sql .= '   left    join codigoitens as M on M.id = contratos.modalidade_id                                                         ';
        $sql .= '   left    join codigoitens as C on C.id = contratos.categoria_id                                                          ';
        $sql .= '   left    join codigoitens as T on T.id = contratos.tipo_id                                                               ';
        $sql .= '   left    join fornecedores as F on F.id = contratos.fornecedor_id                                                        ';
        $sql .= '   left    join unidades as U on U.id = contratos.unidade_id                                                               ';
        $sql .= '   left    join orgaos as O on O.id = U.orgao_id                                                                           ';

        $sql .= '   left    join contratohistorico as H on H.contrato_id = contratos.id and H.tipo_id = (                                   ';
        $sql .= '           select  id from codigoitens where descricao =                                                                   ';
        $sql .= "           'Contrato'                                                                                                      ";
        $sql .= '           and     codigo_id = ( select id from codigos where descricao =                                                  ';
        $sql .= "           'Tipo de Contrato'";
        $sql .= '    ) )                                                                                                                    ';

        $sql .= "   where O.id = $idOrgaoUnidade                                                                                            ";
        $sql .= '   order   by contratos.numero, fornecedor_codigo                                                                          ';
        $resultado2 = DB::select($sql);
        return $resultado2;
    }

    public function buscaListaTodosContratos($filtro)
    {
        $lista = $this->select([
            DB::raw('CONCAT("O"."codigo", \' - \', "O"."nome")  as orgao'),
            DB::raw('CONCAT("U"."codigo",\' - \',"U"."nomeresumido")  as unidade'),
            DB::raw('CASE
                                WHEN receita_despesa = \'D\'
                                THEN \'Despesa\'
                                ELSE \'Receita\'
                           END as receita_despesa'),
            'unidades_requisitantes',
            'numero',
            DB::raw('"F"."cpf_cnpj_idgener" as fornecedor_codigo'),
            DB::raw('"F"."nome" as fornecedor_nome'),
            'T.descricao as tipo',
            'C.descricao as categoria',
            'S.descricao as subcategoria',
            'processo',
            'objeto',
            'info_complementar',
            'M.descricao as modalidade',
            'licitacao_numero',
            'data_assinatura',
            'data_publicacao',
            'vigencia_inicio',
            'vigencia_fim',
            'valor_inicial',
            'valor_global',
            'num_parcelas',
            'valor_parcela',
            'valor_acumulado',
            DB::raw('CASE
                                WHEN contratos.situacao = \'t\'
                                THEN \'Ativo\'
                                ELSE \'Inativo\'
                           END as situacao'),
            'unidades_requisitantes',
            DB::raw('CASE
            WHEN contratos.prorrogavel = \'t\' THEN \'Sim\'
            WHEN contratos.prorrogavel = \'f\' THEN \'Não\'
            END as prorrogavel'),
        ]);

        $lista->leftjoin('orgaosubcategorias as S', 'S.id', '=', 'contratos.subcategoria_id');
        $lista->leftjoin('codigoitens as M', 'M.id', '=', 'contratos.modalidade_id');
        $lista->leftjoin('codigoitens as C', 'C.id', '=', 'contratos.categoria_id');
        $lista->leftjoin('codigoitens as T', 'T.id', '=', 'contratos.tipo_id');
        $lista->leftjoin('fornecedores as F', 'F.id', '=', 'contratos.fornecedor_id');
        $lista->leftjoin('unidades as U', 'U.id', '=', 'contratos.unidade_id');
        $lista->leftjoin('orgaos as O', 'O.id', '=', 'U.orgao_id');

        return $lista->get();
    }

    public function buscaContratosNovosPorUg(int $unidade_id)
    {
        $data = date('Y-m-d H:i:s', strtotime('-5 days'));
        $contratos = $this->where('unidade_id', $unidade_id)
            ->where('created_at', '>=', $data)
            ->get();

        return $contratos->count();
    }

    public function buscaContratosAtualizadosPorUg(int $unidade_id)
    {
        $data = date('Y-m-d H:i:s', strtotime('-5 days'));
        $contratos = $this->where('unidade_id', $unidade_id)
            ->where('updated_at', '>=', $data)
            ->get();

        return $contratos->count();
    }

    public function buscaContratosVencidosPorUg(int $unidade_id)
    {
        $data = date('Y-m-d');
        $contratos = $this->where('unidade_id', $unidade_id)
            ->where('vigencia_fim', '<', $data)
            ->where('situacao', true)
            ->get();

        return $contratos->count();
    }

    public function atualizaContratoFromHistorico(string $contrato_id, array $array)
    {
        $this->where('id', '=', $contrato_id)
            ->update($array);

        return $this;
    }

    public function atualizaValorAcumuladoFromCronograma(Contratocronograma $contratocronograma)
    {
        $contrato_id = $contratocronograma->contrato_id;

        $valor_acumulado = $contratocronograma->where('contrato_id', '=', $contrato_id)
            ->sum('valor');

        $this->where('id', '=', $contrato_id)
            ->update(['valor_acumulado' => $valor_acumulado]);
    }

    public function getFornecedor()
    {
        $fornecedor = Fornecedor::find($this->fornecedor_id);
        return $fornecedor->cpf_cnpj_idgener . ' - ' . $fornecedor->nome;
    }

    public function getReceitaDespesa()
    {
        if ($this->receita_despesa == 'D') {
            return 'Despesa';
        }
        if ($this->receita_despesa == 'R') {
            return 'Receita';
        }

        return '';
    }

    public function getUnidade()
    {
        $unidade = Unidade::find($this->unidade_id);
        return $unidade->codigo . ' - ' . $unidade->nomeresumido;
    }

    public function getUnidadeOrigem()
    {
        if (!isset($this->unidadeorigem_id)) {
            return '';
        }

        return $this->unidadeorigem->codigo . ' - ' . $this->unidadeorigem->nomeresumido;
    }

    public function getUnidadeCompra()
    {
        if (!isset($this->unidadecompra_id)) {
            return '';
        }

        return $this->unidadecompra->codigo . ' - ' . $this->unidadecompra->nomeresumido;
    }

    public function getNumeroCompra()
    {
        return $this->licitacao_numero;
    }

    public function getOrgao()
    {
        $orgao = Orgao::whereHas('unidades', function ($query) {
            $query->where('id', '=', $this->unidade_id);
        })->first();

        return $orgao->codigo . ' - ' . $orgao->nome;
    }

    public function getTipo()
    {
        if ($this->tipo_id) {
            $tipo = CodigoItem::find($this->tipo_id);

            return $tipo->descricao;
        } else {
            return '';
        }
    }

    public function getCategoria()
    {
        return $this->categoria->descricao;
    }

    public function getSubCategoria()
    {
        if ($this->subcategoria_id) {
            $subcategoria = OrgaoSubcategoria::find($this->subcategoria_id);
            return $subcategoria->descricao;
        } else {
            return '';
        }
    }

    public function getModalidade()
    {
        if (!isset($this->modalidade->descres)) {
            return '';
        }

        return $this->modalidade->descricao;
    }

    #140
    public function getVigenciaFimPrazoIndeterminado()
    {
        if ($this->is_prazo_indefinido == '1' || $this->is_prazo_indefinido == true) {
            return 'Indeterminado';
        }
            $vigenciafim = date_create($this->vigencia_fim);
            return  date_formaT($vigenciafim, 'd/m/Y');
    }

    public function getVigenciainicio()
    {
        $vigenciaInicio = date_create($this->vigencia_inicio);
        return  date_formaT($vigenciaInicio, 'd/m/Y');
    }
    #140
    public function getNumeroParcelasPrazoIndeterminado()
    {
        if ($this->is_prazo_indefinido == '1' || $this->is_prazo_indefinido == true) {
            return 'Indeterminado';
        }
        return  $this->num_parcelas;
    }

    public function getLinkShowInV1()
    {
        $host = str_replace('appredirect/', '', env('URL_CONTRATO_VERSAO_UM'));
        return  $host . "transparencia/contratos/{$this->id}";
    }

    public function getLinkPNCP()
    {
        $codigoExcluido = CodigoItem::whereHas('codigo', function ($q) {
            $q->where('descricao', 'Situação Envia Dados PNCP');
        })->where('descres', 'EXCLUIDO')->first()->id;

        $pncp = @EnviaDadosPncp::where([
            ['pncpable_type', Contratohistorico::class],
            ['contrato_id', '=', $this->id],
            ['situacao','<>', $codigoExcluido],
            ['sequencialPNCP', '<>', null]
        ])->oldest()->first();
        if (isset($pncp->sequencialPNCP) && isset($pncp->link_pncp)) {
            /*Mapa:
            de: [0]https://[2]pncp.gov.br/pncp-api/v1/orgaos/[6]10626896000172/contratos/[8]2022/[9]42
            de: [0]00489828000155-[1]2-[2]000032/[1]2022
            para: https://pncp.gov.br/app/contratos/10626896000172/2022/42
            */
            $host = explode("/", $pncp->link_pncp);
            $arr = explode("-", str_replace('/', '-', $pncp->sequencialPNCP));
            $link_pncp = $host[0].'//'.$host[2].'/app/contratos/'.$arr[0].'/'.$arr[3].'/'.$arr[2];
            return $link_pncp;
        }

        return null;
    }

    public function getLinkTransparenciaV1($rota)
    {
        $codigoUnidade = $this->unidade->codigo;

        $host = parse_url(URL::to(env('URL_CONTRATO_VERSAO_UM')), PHP_URL_HOST);
        $link = "//$host/transparencia/$rota?unidade=$codigoUnidade&numerocontrato=$this->numero";

        return "<a href='$link' target='__blank'>Clique aqui para acessar</a>";
    }

    public function formatVlrParcela()
    {
        return 'R$ ' . number_format($this->valor_parcela, 2, ',', '.');
    }


    public function retornaAmparo()
    {
        $amparo = "";
        $cont = (is_array($this->amparoslegais)) ? count($this->amparoslegais) : 0;

        foreach ($this->amparoslegais as $key => $value) {
            if ($cont < 2) {
                $amparo .= $value->ato_normativo;
                $amparo .= (!is_null($value->artigo)) ? " - Artigo: " . $value->artigo : "";
                $amparo .= (!is_null($value->paragrafo)) ? " - Parágrafo: " . $value->paragrafo : "";
                $amparo .= (!is_null($value->inciso)) ? " - Inciso: " . $value->inciso : "";
                $amparo .= (!is_null($value->alinea)) ? " - Alinea: " . $value->alinea : "";
            }
            if ($key == 0 && $cont > 1) {
                $amparo .= $value->ato_normativo;
                $amparo .= (!is_null($value->artigo)) ? " - Artigo: " . $value->artigo : "";
                $amparo .= (!is_null($value->paragrafo)) ? " - Parágrafo: " . $value->paragrafo : "";
                $amparo .= (!is_null($value->inciso)) ? " - Inciso: " . $value->inciso : "";
                $amparo .= (!is_null($value->alinea)) ? " - Alinea: " . $value->alinea : "";
            }
            if ($key > 0 && $key < ($cont - 1)) {
                $amparo .= ", " . $value->ato_normativo;
                $amparo .= (!is_null($value->artigo)) ? " - Artigo: " . $value->artigo : "";
                $amparo .= (!is_null($value->paragrafo)) ? " - Parágrafo: " . $value->paragrafo : "";
                $amparo .= (!is_null($value->inciso)) ? " - Inciso: " . $value->inciso : "";
                $amparo .= (!is_null($value->alinea)) ? " - Alinea: " . $value->alinea : "";
            }
            if ($key == ($cont - 1)) {
                $amparo .= " e " . $value->ato_normativo;
                $amparo .= (!is_null($value->artigo)) ? " - Artigo: " . $value->artigo : "";
                $amparo .= (!is_null($value->paragrafo)) ? " - Parágrafo: " . $value->paragrafo : "";
                $amparo .= (!is_null($value->inciso)) ? " - Inciso: " . $value->inciso : "";
                $amparo .= (!is_null($value->alinea)) ? " - Alinea: " . $value->alinea : "";
            }
        }

        return $amparo;
    }

    public function formatVlrGlobal()
    {
        return 'R$ ' . number_format($this->valor_global, 2, ',', '.');
    }

    public function formatVlrAcumulado(): string
    {
        return 'R$ ' . number_format($this->valor_acumulado, 2, ',', '.');
    }

    public function formatTotalDespesasAcessorias()
    {
        return 'R$ ' . number_format($this->total_despesas_acessorias, 2, ',', '.');
    }

    public function contratoAPI(string $prefixoAPI)
    {

        return [
            'id' => $this->id,
            'receita_despesa' => ($this->receita_despesa) == 'D' ? 'Despesa' : 'Receita',
            'numero' => $this->numero,
            'contratante' => [
                'orgao_origem' => [
                    'codigo' => @$this->unidadeorigem->orgao->codigo,
                    'nome' => @$this->unidadeorigem->orgao->nome,
                    'unidade_gestora_origem' => [
                        'codigo' => @$this->unidadeorigem->codigo,
                        'nome_resumido' => @$this->unidadeorigem->nomeresumido,
                        'nome' => @$this->unidadeorigem->nome,
                        'sisg' => @$this->unidadeorigem->sisg == true ? 'Sim' : 'Não',
                        'utiliza_siafi' => @$this->unidadeorigem->utiliza_siafi == true ? 'Sim' : 'Não'
                    ],
                ],
                'orgao' => [
                    'codigo' => $this->unidade->orgao->codigo,
                    'nome' => $this->unidade->orgao->nome,
                    'unidade_gestora' => [
                        'codigo' => $this->unidade->codigo,
                        'nome_resumido' => $this->unidade->nomeresumido,
                        'nome' => $this->unidade->nome,
                        'sisg' => $this->unidade->sisg == true ? 'Sim' : 'Não',
                        'utiliza_siafi' => @$this->unidadeorigem->utiliza_siafi == true ? 'Sim' : 'Não'
                    ],
                ],
            ],
            'fornecedor' => [
                'tipo' => $this->fornecedor->tipo_fornecedor,
                'cnpj_cpf_idgener' => $this->fornecedor->cpf_cnpj_idgener,
                'nome' => $this->fornecedor->nome,
            ],
            'codigo_tipo' => @$this->tipo->descres,
            'tipo' => $this->tipo->descricao,
            'subtipo' => @$this->subtipo,
            'prorrogavel' => $this->getProrrogavelColunaAttribute() == '' ? null : $this->getProrrogavelColunaAttribute(),
            'situacao' => $this->situacao == true ? 'Ativo' : 'Inativo',
            'justificativa_inativo' => @$this->justificativa_inativo->descricao,
            'categoria' => $this->categoria->descricao,
            'subcategoria' => @$this->orgaosubcategoria->descricao,
            'unidades_requisitantes' => $this->unidades_requisitantes,
            'processo' => $this->processo,
            'objeto' => $this->objeto,
            'amparo_legal' => @$this->retornaAmparo(),
            'informacao_complementar' => $this->info_complementar,
            'codigo_modalidade' => @$this->modalidade->descres,
            'modalidade' => $this->modalidade->descricao,
            'unidade_compra' => @$this->unidadecompra->codigo,
            'licitacao_numero' => $this->licitacao_numero,
            'sistema_origem_licitacao' => @$this->sistema_origem_licitacao,
            'data_assinatura' => $this->data_assinatura,
            'data_publicacao' => $this->data_publicacao,
            'vigencia_inicio' => $this->vigencia_inicio,
            'vigencia_fim' => $this->vigencia_fim,
            'valor_inicial' => number_format($this->valor_inicial, 2, ',', '.'),
            'valor_global' => number_format($this->valor_global, 2, ',', '.'),
            'num_parcelas' => $this->num_parcelas,
            'valor_parcela' => number_format($this->valor_parcela, 2, ',', '.'),
            'valor_acumulado' => number_format($this->valor_acumulado, 2, ',', '.'),
            'links' => [
                'historico' => url($prefixoAPI. '/' . $this->id . '/historico/'),
                'empenhos' => url($prefixoAPI. '/' . $this->id . '/empenhos/'),
                'cronograma' => url($prefixoAPI. '/' . $this->id . '/cronograma/'),
                'garantias' => url($prefixoAPI. '/' . $this->id . '/garantias/'),
                'itens' => url($prefixoAPI. '/' . $this->id . '/itens/'),
                'prepostos' => url($prefixoAPI. '/' . $this->id . '/prepostos/'),
                'responsaveis' => url($prefixoAPI. '/' . $this->id . '/responsaveis/'),
                'despesas_acessorias' => url($prefixoAPI. '/' . $this->id . '/despesas_acessorias/'),
                'faturas' => url($prefixoAPI. '/' . $this->id . '/faturas/'),
                'ocorrencias' => url($prefixoAPI. '/' . $this->id . '/ocorrencias/'),
                'terceirizados' => url($prefixoAPI. '/' . $this->id . '/terceirizados/'),
                'arquivos' => url($prefixoAPI. '/' . $this->id . '/arquivos/'),
            ]
        ];
    }

    public function buscaContratoPorId(int $contrato_id)
    {

        $contrato = Contrato::whereHas('unidade', function ($q) {
            $q->whereHas('orgao', function ($o) {
                $o->where('situacao', true);
            })->where('sigilo', false);
        })
        ->where('id', $contrato_id)
        ->get();

        return $contrato;
    }

    public function getNumeroContrato()
    {
        return $this->numero;
    }

    public function getProcessoContrato()
    {
        return $this->processo;
    }

    public function getObjetoContrato()
    {
        return $this->objeto;
    }

    /*public function getModalidade()
    {
        return $this->modalidade->descricao;
    }*/


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function contrato_autoridade_signataria()
    {
        return $this->belongsToMany(
            'App\Models\AutoridadeSignataria',
            'contrato_autoridade_signataria',           // from esse
            'contratohistorico_id', // where esse - 3  = contrato_id
            'autoridadesignataria_id',          // select esse - 1
        );
    }
    public function autoridades_signatarias()
    {
        return $this->belongsToMany(
            Contratohistorico::class,
            'contrato_autoridade_signataria',           // from esse
            'contratohistorico_id', // where esse - 3  = contrato_id
            'id',          // select esse - 1
        );
    }
    public function historicocontrato()
    {
        return $this->belongsToMany(
            Contratohistorico::class,
            Contratohistorico::class,        // from esse - 2
            'contrato_id', // where esse - 3  = contrato_id
            'id',          // select esse - 1
        );
    }
    public function arquivos()
    {
        return $this->hasMany(Contratoarquivo::class, 'contrato_id');
    }
    public function contratoOpm()
    {
        return $this->hasMany(ContratoOpm::class, 'contrato_id');
    }

    public function locaisExecucao()
    {
        return $this->hasMany(ContratoLocalExecucao::class, 'contrato_id');
    }

    public function categoria()
    {
        return $this->belongsTo(CodigoItem::class, 'categoria_id');
    }

    public function cronograma()
    {
        return $this->hasMany(Contratocronograma::class, 'contrato_id');
    }

    public function despesasacessorias()
    {
        return $this->hasMany(Contratodespesaacessoria::class, 'contrato_id');
    }

    public function empenhos()
    {
        return $this->hasMany(Contratoempenho::class, 'contrato_id');
    }

    public function faturas()
    {
        return $this->hasMany(Contratofatura::class, 'contrato_id');
    }

    public function fornecedor()
    {
        return $this->belongsTo(Fornecedor::class, 'fornecedor_id');
    }

    public function garantias()
    {
        return $this->hasMany(Contratogarantia::class, 'contrato_id');
    }

    public function historico()
    {
        return $this->hasMany(Contratohistorico::class, 'contrato_id');
    }

    public function itens()
    {
        return $this->hasMany(Contratoitem::class, 'contrato_id');
    }

    public function modalidade()
    {
        return $this->belongsTo(CodigoItem::class, 'modalidade_id');
    }

    public function ocorrencias()
    {
        return $this->hasMany(Contratoocorrencia::class, 'contrato_id');
    }

    public function unidadesdescentralizadas()
    {
        return $this->hasMany(Contratounidadedescentralizada::class, 'contrato_id');
    }

    public function orgaosubcategoria()
    {
        return $this->belongsTo(OrgaoSubcategoria::class, 'subcategoria_id');
    }

    public function prepostos()
    {
        /*dd($this->hasMany(Contratopreposto::class, 'contrato_id', 'id'));*/
        return $this->hasMany(Contratopreposto::class, 'contrato_id');
    }
    /**
     * alterado por mvascs@gmail.com -> retirado where situacao,
     * para que todos os responsáveis fossem trazidos para o extrato pdf e não apenas os ativos
     */
    public function responsaveis()
    {
        return $this->hasMany(Contratoresponsavel::class, 'contrato_id');
    }

    public function terceirizados()
    {
        return $this->hasMany(Contratoterceirizado::class, 'contrato_id');
    }

    public function tipo()
    {
        return $this->belongsTo(CodigoItem::class, 'tipo_id');
    }

    public function unidade()
    {
        return $this->belongsTo(Unidade::class, 'unidade_id');
    }

    public function unidadeorigem()
    {
        return $this->belongsTo(Unidade::class, 'unidadeorigem_id');
    }

    public function unidadecompra()
    {
        return $this->belongsTo(Unidade::class, 'unidadecompra_id');
    }

    public function unidadebeneficiaria()
    {
        return $this->belongsTo(Unidade::class, 'unidadebeneficiaria_id');
    }

    public function publicacao()
    {
        return $this->hasOne(Contratopublicacoes::class, 'contrato_id');
    }

    public function parametros()
    {
        return $this->hasOne(ContratoParametro::class, 'contrato_id');
    }

    public function amparoslegais()
    {
        return $this->belongsToMany(
            'App\Models\AmparoLegal',
            'amparo_legal_contrato',
            'contrato_id',
            'amparo_legal_id'
        );
    }

    public function minutasempenho()
    {
        return $this->belongsToMany(
            'App\Models\MinutaEmpenho',
            'contrato_minuta_empenho_pivot',
            'contrato_id',
            'minuta_empenho_id'
        );
    }

    public function fornecedoresSubcontratados()
    {
        return $this->belongsToMany(
            Fornecedor::class,
            'contratos_fornecedores_pivot',
            'contrato_id',
            'fornecedor_id'
        );
    }

    public function antecipagovs()
    {
        return $this->belongsToMany(
            AntecipaGov::class,
            'contrato_antecipagov',
            'contrato_id',
            'antecipagov_id'
        );
    }

    public function justificativa_inativo()
    {
        return $this->belongsTo(CodigoItem::class, 'justificativa_contrato_inativo_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getProrrogavelColunaAttribute()
    {
        if ($this->prorrogavel === null) {
            return '';
        }
        return $this->prorrogavel == 1 ? 'Sim' : 'Não';
    }

    public function getLicitacaoNumeroLimpaAttribute()
    {
        return str_replace('/', '', $this->licitacao_numero);
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
