<?php

namespace App\Models;

use App\Http\Traits\Arp\ArpTrait;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ArpAlteracao extends Model
{
    use CrudTrait;
    use ArpTrait;
    use LogsActivity;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'arp_alteracao';
    
    protected static $logFillable = true;
    protected static $logName = 'arp_alteracao';
    
    /**
     * The primary key for the model.
     *
     * @var string
     */
    // protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    // public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    // protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['arp_id', 'rascunho', 'sequencial'];

    /**
     * The attributes that should be hidden for arrays
     *
     * @var array
     */
    // protected $hidden = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = [];

    protected $casts = [
        'quantitativo_renovado_vigencia' => 'boolean'
    ];
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getViewButtonArp($crud, $id = 0)
    {

        if (!$this->rascunho) {
            $crud->denyAccess('update');
            $crud->denyAccess('delete');
            return;
        }
        $crud->allowAccess('update');
        $crud->allowAccess('delete');

        return null;
    }
    public function getQuantitativoRenovado()
    {
        if ($this->quantitativo_renovado_vigencia) {
            return 'Sim';
        }

        if ($this->quantitativo_renovado_vigencia === null) {
            return;
        }

        return 'Não';
    }

    public function getSequencial()
    {
        $ano = date_format($this->created_at, 'Y');
        return $this->sequencialFormatado($this->sequencial, $ano, $this->rascunho);
    }

    public function getTipoAlteracao()
    {
        $tipoAlteração = null;
        # Percorre todos as alterações selecionadas pelo usuário
        foreach ($this->historico_alteracao as $historicoAlteracao) {
            $tipoAlteração .= $historicoAlteracao->status->descricao.", ";
        }

        # Se não encontrar tipo, retorna nulo
        if (empty($tipoAlteração)) {
            return $tipoAlteração;
        }

        # Se encontrar algum tipo, remove os dois últimos caracteres
        return substr($tipoAlteração, 0, -2);
    }
//    public function retornaAmparo()
//    {
//        $amparo = "";
//        $cont = (is_array($this->amparoslegais)) ? count($this->amparoslegais) : 0;
//
//        foreach ($this->amparoslegais as $key => $value) {
//            if ($cont < 2) {
//                $amparo .= $value->ato_normativo;
//                $amparo .= (!is_null($value->artigo)) ? " - Artigo: " . $value->artigo : "";
//                $amparo .= (!is_null($value->paragrafo)) ? " - Parágrafo: " . $value->paragrafo : "";
//                $amparo .= (!is_null($value->inciso)) ? " - Inciso: " . $value->inciso : "";
//                $amparo .= (!is_null($value->alinea)) ? " - Alinea: " . $value->alinea : "";
//            }
//            if ($key == 0 && $cont > 1) {
//                $amparo .= $value->ato_normativo;
//                $amparo .= (!is_null($value->artigo)) ? " - Artigo: " . $value->artigo : "";
//                $amparo .= (!is_null($value->paragrafo)) ? " - Parágrafo: " . $value->paragrafo : "";
//                $amparo .= (!is_null($value->inciso)) ? " - Inciso: " . $value->inciso : "";
//                $amparo .= (!is_null($value->alinea)) ? " - Alinea: " . $value->alinea : "";
//            }
//            if ($key > 0 && $key < ($cont - 1)) {
//                $amparo .= ", " . $value->ato_normativo;
//                $amparo .= (!is_null($value->artigo)) ? " - Artigo: " . $value->artigo : "";
//                $amparo .= (!is_null($value->paragrafo)) ? " - Parágrafo: " . $value->paragrafo : "";
//                $amparo .= (!is_null($value->inciso)) ? " - Inciso: " . $value->inciso : "";
//                $amparo .= (!is_null($value->alinea)) ? " - Alinea: " . $value->alinea : "";
//            }
//            if ($key == ($cont - 1)) {
//                $amparo .= " e " . $value->ato_normativo;
//                $amparo .= (!is_null($value->artigo)) ? " - Artigo: " . $value->artigo : "";
//                $amparo .= (!is_null($value->paragrafo)) ? " - Parágrafo: " . $value->paragrafo : "";
//                $amparo .= (!is_null($value->inciso)) ? " - Inciso: " . $value->inciso : "";
//                $amparo .= (!is_null($value->alinea)) ? " - Alinea: " . $value->alinea : "";
//            }
//        }
//
//        dd($amparo, 'entrou');
//        return $amparo;
//    }
//    public function amparoslegais()
//    {
//        return $this->belongsToMany(
//            AmparoLegal::class,
//            'amparo_legal_contrato',
//            'arp_id',
//            'amparo_legal_id'
//        );
//    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function status()
    {
        return $this->belongsTo(CodigoItem::class, 'tipo_id');
    }
    
    public function arp()
    {
        return $this->belongsTo(Arp::class, "arp_id");
    }

    public function historico_alteracao()
    {
        return $this->hasMany(ArpHistorico::class, "arp_alteracao_id");
    }

    public function arquivo()
    {
        return $this->hasMany(ArpArquivo::class, "arp_alteracao_id");
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
