<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ArpArquivoTipo extends Model
{
    use HasFactory, CrudTrait;

    protected $table = 'arp_arquivo_tipos';

    protected $fillable = [
        'nome',
    ];

    public function arquivos(): HasMany
    {
        return $this->hasMany(ArpArquivo::class);
    }
}
