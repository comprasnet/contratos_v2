<?php

namespace App\Models;

use App\Http\Traits\Formatador;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompraItem extends Model
{
    use CrudTrait;
    use LogsActivity;
    use Formatador;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'compra_items';
    protected static $logFillable = true;
    protected static $logName = 'compra_items_v2';
    
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = [
        'compra_id',
        'tipo_item_id',
        'catmatseritem_id',
        'descricaodetalhada',
        'valorunitario',
        'qtd_total',
        'valortotal',
        'qtd_restante',
        'numero',
        'ata_vigencia_inicio',
        'ata_vigencia_fim',
        'criterio_julgamento',
        'situacao',
        'grupo_compra',
        'criterio_julgamento_id',
        'codigo_ncmnbs',
        'aplica_margem_ncmnbs',
        'descricao_ncmnbs',
        'maximo_adesao_informado_compra'
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getDescricaoCompleta()
    {
        $idBotao = "botaoDescricaoCompletaCompra_" . $this->id;
        $idTexto = "textoDescricaoCompletaCompra_" . $this->id;

        $descricaoCompleta =
        $this->catMatSerItem->descricao . '<br><br>Descrição Detalhada:<br><br> ' . $this->descricaodetalhada;

        $descricaoResumida = $this->catMatSerItem->descricao;

        $botaoResumido =
        "<button type='button'
                class='btn btn-default'
                style='width: 25px; height: 25px; padding: 0; line-height: 10px; margin-left: 10px;'
                onclick='exibirDescricaoCompleta(`{$descricaoCompleta}`,`{$descricaoResumida}`,`{$this->id}`)' >
                    <span id='{$idBotao}' data-tipo='resumido' >
                        <i class='fas fa-plus'></i>
                    </span>
        </button>";

        return "<span id='{$idTexto}' style='white-space: normal;' >{$descricaoResumida}</span>{$botaoResumido}";
    }

    public function getTipoItem()
    {
        return $this->tipoItem->descricao;
    }

    public function getCatMatSerItem()
    {
        return $this->catMatSerItem->codigo_siasg . ' - ' . $this->catMatSerItem->descricao;
    }

    /**
     * Método responsável em retornar o nome completo do item concatenando o
     * Número do item + Descrição do item
     */
    public function exibirItemCompleto()
    {
        return "{$this->numero} - {$this->descricaodetalhada}";
    }

    /**
     * Método responsável em inserir o item da compra na tabela compra_item
     */
    public function updateOrCreateCompraItemSisrp($compra, $catmatseritem, $dadosata)
    {
        # Recupera as informações do arquivo de
        # configuração para material e serviço
        $material = config('api.siasg.material');
        $servico = config('api.siasg.servico');

        # Monta um array dos tipos para recuperar na coluna tipo_item_id
        $tipo = ['S' => $servico[0], 'M' => $material[0]];

        $dadosata->dataInicioVigencia = null;
        # Se o retorno do serviço for uma data válida,
        # então formata a data para a inclusão
        if ($this->validateDate($dadosata->dataInicioVigencia)) {
            $dadosata->dataInicioVigencia = $this->formataDataSiasg($dadosata->dataInicioVigencia);
        }

        # Se o retorno do serviço for uma data válida,
        # então formata a data para a inclusão
        $dadosata->dataFimVigencia = null;
        if ($this->validateDate($dadosata->dataFimVigencia)) {
            $dadosata->dataFimVigencia = $this->formataDataSiasg($dadosata->dataFimVigencia);
        }

        # Recupera o grupo da compra vindo do serviço, se não existir salva como nulo
        $grupoCompra = (isset($dadosata->grupoCompra)) ? $dadosata->grupoCompra : null;

        # Atualiza a informação do item
        $compraitem = CompraItem::updateOrCreate(
            [
                'compra_id' => $compra->id,
                'tipo_item_id' => (int)$tipo[$dadosata->tipo],
                'catmatseritem_id' => (int)$catmatseritem->id,
                'numero' => $dadosata->numeroItem
            ],
            [
                'descricaodetalhada' =>
                (!empty($dadosata->descricaoDetalhada)) ? $dadosata->descricaoDetalhada : $dadosata->descricao,
                'qtd_total' => $dadosata->quantidadeTotal,
                // 'ata_vigencia_inicio' => $dadosata->dataInicioVigencia,
                // 'ata_vigencia_fim' => $dadosata->dataFimVigencia,
                'criterio_julgamento' => $dadosata->criterioJulgamento,
                'criterio_julgamento_id' => $dadosata->criterioJulgamentoId ?? null,
                'situacao' => true,
                'grupo_compra' => $grupoCompra,
                'codigo_ncmnbs' => $dadosata->codigoNcmNbs ?? null,
                'aplica_margem_ncmnbs' => $dadosata->aplicaMargemNcmNbs ?? false,
                'descricao_ncmnbs' => $dadosata->descricaoNcmNbs ?? null,
            ]
        );

        return $compraitem;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function compra()
    {
        return $this->belongsTo(Compras::class, 'compra_id');
    }

    public function tipoItem()
    {
        return $this->belongsTo(CodigoItem::class, 'tipo_item_id');
    }

    public function catMatSerItem()
    {
        return $this->belongsTo(Catmatseritem::class, 'catmatseritem_id');
    }

    public function compraItemUnidade()
    {
        return $this->hasMany(CompraItemUnidade::class, 'compra_item_id');
    }

    public function compraItemFornecedor()
    {
        return $this->hasMany(CompraItemFornecedor::class, 'compra_item_id');
    }

    public function unidade_autorizada()
    {
        return $this->belongsTo(Unidade::class, 'unidade_autorizada_id');
    }

    public function unidade()
    {
        return $this->belongsToMany(
            'App\Models\CompraItem',
            'compra_item_unidade',
            'unidade_id',
            'compra_item_id'
        );
    }

    public function fornecedor()
    {
        return $this->belongsToMany(
            'App\Models\CompraItem',
            'compra_item_unidade',
            'fornecedor_id',
            'compra_item_id'
        );
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    # Responsável em recuperar os itens a partir do fornecedor selecionado no cadastro da ata
    public static function itemCompraPorFornecedor(
        string $numero_compra,
        int $idFornecedor,
        int $idUnidade,
        int $modalidadeId,
        array $itensNaoExibir = null
    ) {
        # Recupera os itens que foram cadastrados em todas as atas para não deixar exibir
//        $itensCadastradoArp = ArpItem::groupby("compra_item_fornecedor_id")
//->pluck("compra_item_fornecedor_id")->toArray();
        
        # Recupera os itens para o fornecedor selecionado
        $itens = CompraItem::join('compras', 'compras.id', '=', 'compra_items.compra_id')
            ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
            ->join('fornecedores', 'fornecedores.id', '=', 'compra_item_fornecedor.fornecedor_id')
            ->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', '=', 'compra_items.id')
            ->join('unidades', 'unidades.id', '=', 'compra_item_unidade.unidade_id')
            ->join('codigoitens', 'codigoitens.id', '=', 'compra_items.tipo_item_id')
            ->join(
                'catmatseritens',
                'catmatseritens.id',
                '=',
                'compra_items.catmatseritem_id'
            )
//        ->whereNotIn("compra_item_fornecedor.id", $itensCadastradoArp)
//            ->where(function ($query) {
//                $query->whereNull('compra_item_fornecedor.ata_vigencia_fim')
//                    ->orWhere('compra_item_fornecedor.ata_vigencia_fim', '>=', Carbon::now()->toDateString());
//            })
            ->where('compra_item_unidade.quantidade_saldo', '>', 0)
            ->where('compras.unidade_origem_id', $idUnidade)
            ->where('compras.numero_ano', $numero_compra)
            ->where('compra_item_fornecedor.situacao', true)
            // ->where('compra_item.situacao', true)
            ->where('compra_item_unidade.situacao', true)
            ->where('compras.modalidade_id', $modalidadeId)
            ->where('compra_item_fornecedor.fornecedor_id', $idFornecedor)
            ->whereNotNull('compra_item_unidade.tipo_uasg')
            ->select([
                "compra_items.numero","codigoitens.descricao","catmatseritens.codigo_siasg",
                "compra_item_fornecedor.valor_unitario", "compra_item_fornecedor.valor_negociado",
                "compra_items.criterio_julgamento", "compra_item_fornecedor.percentual_maior_desconto",
                "compra_items.maximo_adesao","compra_items.permite_carona",
                DB::raw("compra_items.descricaodetalhada AS descricaosimplificada"),
                DB::raw("string_agg(unidades.codigo, ', ')  as codigouasg"),
                'compra_item_fornecedor.id AS compraItemFornecedorId',
                DB::raw("string_agg(compra_item_unidade.quantidade_autorizada::varchar, ', ')
        as quantidadeautorizadadetalhe"),
                DB::raw("string_agg(
            CASE
                WHEN tipo_uasg = 'G' THEN 'Gerenciadora'
                WHEN tipo_uasg = 'P' THEN 'Participante'
                WHEN tipo_uasg = 'C' THEN 'Não participante'
                END , ',')  as tipouasgdetalhe"),

                DB::raw("TO_CHAR(compra_item_fornecedor.valor_unitario *
    ((100-compra_item_fornecedor.percentual_maior_desconto)/100), 'FM999999999.0000') as valor_unitario_desconto"),

                DB::raw("TO_CHAR(
        compra_item_fornecedor.valor_unitario * ((100 - compra_item_fornecedor.percentual_maior_desconto) / 100),
        'FM999999999.0000'
    )::float * compra_item_fornecedor.quantidade_homologada_vencedor as valor_negociado_desconto"),

                DB::raw("sum(compra_item_unidade.quantidade_saldo) AS quantidade_saldo"),
                DB::raw("sum(compra_item_unidade.quantidade_adquirir) AS quantidade_adquirir"),
                DB::raw("sum(quantidade_autorizada) AS quantidadeautorizada"),
                DB::raw("sum(quantidade_autorizada) AS quantidadeautorizadaselecionado"),
                DB::raw("quantidade_homologada_vencedor AS quantidadehomologadavencedor"),
                "fornecedores.cpf_cnpj_idgener", "fornecedores.nome AS nomefornecedor",
                "compra_item_fornecedor.classificacao",
                "compras.id as compraId",
                "compra_items.id as compraItemId",
                "compra_items.maximo_adesao_informado_compra",
                "compras.lei"
            ])
            ->groupBy(
                "compra_items.numero",
                "codigoitens.descricao",
                "catmatseritens.codigo_siasg",
                "compra_items.descricaodetalhada",
                "compra_item_fornecedor.valor_unitario",
                "compra_item_fornecedor.valor_negociado",
                "compra_items.criterio_julgamento",
                "compra_item_fornecedor.percentual_maior_desconto",
                "compra_item_fornecedor.valor_unitario",
                "compra_item_fornecedor.valor_negociado",
                "compra_items.maximo_adesao",
                "compra_items.permite_carona",
                "fornecedores.cpf_cnpj_idgener",
                "fornecedores.nome",
                "compra_item_fornecedor.id",
                "compra_item_fornecedor.classificacao",
                "compras.id",
                "compra_items.id"
            )
            ->orderByRaw("codigoitens.descricao ASC, compra_items.numero ASC");
        
        # Se existir itens cadastrados na tela de rascunho não serão exibidos
        if (!empty($itensNaoExibir)) {
            $itens->whereNotIn('compra_item_fornecedor.id', $itensNaoExibir);
        }
        
        return $itens->orderBy("compra_items.numero", "asc")->get();
    }
    
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
