<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class ArpItemHistoricoAlteracaoFornecedor extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'arp_item_historico_alteracao_fornecedor';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'arp_historico_id',
        'compra_item_fornecedor_origem_id',
        'compra_item_fornecedor_destino_id',
        'fornecedor_origem_id',
        'novo_cnpj',
        'novo_nome',
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function compraItemFornecedorOrigem()
    {
        return $this->belongsTo(CompraItemFornecedor::class, "compra_item_fornecedor_origem_id");
    }
    
    public function compraItemFornecedorDestino()
    {
        return $this->belongsTo(CompraItemFornecedor::class, "compra_item_fornecedor_destino_id");
    }
    
    public function fornecedorOrigem()
    {
        return $this->belongsTo(Fornecedor::class, "fornecedor_origem_id");
    }

    public function arpHistorico()
    {
        return $this->belongsTo(ArpHistorico::class, "arp_historico_id");
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
