<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class ContratoFaturaMesAno extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'contratofaturas_mes_ano';

    protected $table = 'contratofaturas_mes_ano';

    protected $fillable = [
        'contratofaturas_id',
        'mesref',
        'anoref',
        'valorref',
        'mesref_anoref_valor_json',
    ];

    /*
   |--------------------------------------------------------------------------
   | RELATIONS
   |--------------------------------------------------------------------------
   */

    public function contratoFaturas()
    {
        return $this->belongsTo(Contratofatura::class, 'contratofaturas_id');
    }
}
