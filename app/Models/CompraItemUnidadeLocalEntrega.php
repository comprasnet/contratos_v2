<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class CompraItemUnidadeLocalEntrega extends Model
{
    use CrudTrait;
    use LogsActivity;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'compra_item_unidade_local_entrega';
    protected static $logFillable = true;
    protected static $logName = 'compra_item_unidade_local_entrega_v2';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
//    protected $guarded = ['id'];
     protected $fillable = [
        'compra_item_unidade_id',
         'endereco_id',
         'quantidade',
         'endereco_id_novo_divulgacao'
     ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
     public function endereco()
     {
         return $this->belongsTo(Enderecos::class, 'endereco_id');
     }
     public function compraItemUnidade()
     {
         return $this->belongsTo(CompraItemUnidade::class, 'compra_item_unidade_id');
     }
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
