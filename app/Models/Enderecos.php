<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Enderecos extends Model
{
    use CrudTrait, LogsActivity;
    
    protected static $logFillable = true;
    protected static $logName = 'enderecos_v2';
    
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'enderecos';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
//    protected $guarded = ['id'];
     protected $fillable = [
         'municipios_id',
         'cep',
         'bairro',
         'logradouro',
         'complemento',
         'numero',
         'codigo_municipio_ndc'
     ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
     public function municipio()
     {
         return $this->belongsTo(Municipio::class, 'muninicipios_id');
     }
    
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
