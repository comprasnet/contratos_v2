<?php

namespace App\Models;

use App\Http\Traits\Autorizacaoexecucao\AutorizacaoexecucaoTrait;
use App\Http\Traits\Unidade\ProcessoTrait;
use App\Services\ModelSignatario\ModelDTO;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\Formatador;
use Illuminate\Database\Eloquent\SoftDeletes;

class AutorizacaoExecucaoEntrega extends Model
{
    use AutorizacaoexecucaoTrait;
    use CrudTrait;
    use Formatador;
    use SoftDeletes;
    use ProcessoTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'autorizacaoexecucao_entrega';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $fillable = [
        'contrato_id',
        'autorizacaoexecucao_id',
        'situacao_id',
        'user_id_criacao',
        'sequencial',
        'ano',
        'rascunho',
        'valor_entrega',
        'data_entrega',
        'informacoes_complementares',
        'mes_ano_referencia',
        'data_prazo_trp',
        'data_prazo_trd',
        'justificativa_motivo_analise',
        'introducao_trp',
        'informacoes_complementares_trp',
        'introducao_trd',
        'informacoes_complementares_trd',
        'incluir_instrumento_cobranca',
        'originado_sistema_externo',
    ];

    protected $casts = [
        'incluir_instrumento_cobranca' => 'boolean',
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function retornaTextoSituacao()
    {
        return $this->situacao->descricao ?? '';
    }

    public function retornaTextoSituacaoSpamColorido()
    {
        $descricao = $this->situacao->descricao ?? '';
        $status = $this->situacao->descres ?? '';

        if ($status == 'ae_entrega_status_9' || $status == 'ae_entrega_status_2' || $status == 'ae_entrega_status_11') {
            $cor = '#8c8c8c';
        } elseif ($status == 'ae_entrega_status_1' ||
            $status == 'ae_entrega_status_4' ||
            $status == 'ae_entrega_status_8'
        ) {
            if ($status == 'ae_entrega_status_4') {
                $descricao = 'Aguard. Assinatura TRP';
            } elseif ($status == 'ae_entrega_status_8') {
                $descricao = 'Aguard. Assinatura TRD';
            }
            $cor = '#f5a623';
        } elseif ($status == 'ae_entrega_status_6' ||
            $status == 'ae_entrega_status_10' ||
            $status == 'ae_entrega_status_12' ||
            $status == 'ae_entrega_status_7'
        ) {
            if ($status == 'ae_entrega_status_12') {
                $descricao = 'Aguard. Inst. Cobrança';
            }
            $cor = '#009d18';
        } elseif ($status == 'ae_entrega_status_3' || $status == 'ae_entrega_status_5') {
            $cor = '#de1313';
        } else {
            $cor = '#000';
        }

        $corFont = '#fff';

        return view(
            'components_v2.chip_status',
            [
                'conteudo' => $descricao,
                'cor' => $cor,
                'corFont' => $corFont
            ]
        );
    }
    public function retornaValorEntregaFormatado()
    {
        return $this->retornaCampoFormatadoComoNumero($this->valor_entrega, true);
    }

    public function retornaInformacoesComplementares()
    {
        return $this->informacoes_complementares ?? '';
    }

    public function getViewButtons($crud)
    {
        if ($this->situacao->descres == 'ae_entrega_status_9') {
            $crud->allowAccess('update');
            $crud->allowAccess('delete');
        } else {
            $crud->denyAccess('update');
            $crud->denyAccess('delete');
        }
    }

    public function getContrato(): Contrato
    {
        return $this->contrato;
    }

    public function getMesAnoReferencia()
    {
        if (empty($this->mes_ano_referencia)) {
            return '';
        }

        return Carbon::parse($this->mes_ano_referencia)->format('m/Y');
    }

    public function getDataPrazoTrp()
    {
        return $this->data_prazo_trp ? Carbon::parse($this->data_prazo_trp)->format('d/m/Y') : null;
    }

    public function getDataPrazoTrd()
    {
        return $this->data_prazo_trd ? Carbon::parse($this->data_prazo_trd)->format('d/m/Y') : null;
    }

    /**
     * Retorna o sequencial formatado com zeros à esquerda.
     *
     * @return string
     */
    public function getFormattedSequencial()
    {
        return str_pad($this->sequencial, 5, '0', STR_PAD_LEFT) . '/' . $this->ano;
    }

    public function getAnexosViewLink()
    {
        $codigoItem = CodigoItem::where('descres', 'ae_entrega_arquivo')->first();
        $query = $this->anexos()->where('tipo_id', $codigoItem->id);
        if ($query->exists()) {
            return view('crud::autorizacaoexecucao.entrega.table-anexos', [
                'anexos' => $query->get(),
            ])->render();
        }
        return '';
    }

    public function getAnexos()
    {
        return ArquivoGenerico::where('arquivoable_type', AutorizacaoExecucaoEntrega::class)
            ->where('arquivoable_id', $this->id)
            ->get();
    }

    public function getViewStatusAssinaturasSignatariosTRP()
    {
        $this->modelDTO = new ModelDTO(new AutorizacaoExecucaoEntregaTRP());

        return $this->getViewStatusAssinaturasSignatarios();
    }

    public function getViewStatusAssinaturasSignatariosTRD()
    {
        $this->modelDTO = new ModelDTO(new AutorizacaoExecucaoEntregaTRD());

        return $this->getViewStatusAssinaturasSignatarios();
    }

    public function getArquivoTRP()
    {
        return ArquivoGenerico::where('arquivoable_type', AutorizacaoExecucaoEntregaTRP::class)
            ->where('arquivoable_id', $this->id)
            ->first();
    }

    public function getArquivoTRD()
    {
        return ArquivoGenerico::where('arquivoable_type', AutorizacaoExecucaoEntregaTRD::class)
            ->where('arquivoable_id', $this->id)
            ->first();
    }

    public function getLocaisExecucao()
    {
        $locais = [];

        foreach ($this->locaisExecucao as $localExecucao) {
            $locais[] = $localExecucao->descricao;
        }

        return implode(', ', $locais);
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function autorizacao()
    {
        return $this->belongsTo(AutorizacaoExecucao::class, 'autorizacaoexecucao_id');
    }

    public function contrato()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }

    public function situacao()
    {
        return $this->belongsTo(CodigoItem::class, 'situacao_id');
    }

    public function entregaItens()
    {
        return $this->hasMany(AutorizacaoExecucaoEntregaItens::class, 'autorizacao_execucao_entrega_id', 'id');
    }

    public function usuarioCriacao()
    {
        return $this->belongsTo(User::class, 'user_id_criacao');
    }

    public function anexos()
    {
        return $this->morphMany(ArquivoGenerico::class, 'arquivoable');
    }

    public function arquivo()
    {
        return $this->morphOne(ArquivoGenerico::class, 'arquivoable');
    }

    public function locaisExecucao()
    {
        return $this->belongsToMany(
            ContratoLocalExecucao::class,
            'autorizacaoexecucao_contrato_local_execucao',
            'autorizacaoexecucao_entrega_id',
            'contrato_local_execucao_id'
        )->withTimestamps();
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
