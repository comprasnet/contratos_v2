<?php

namespace App\Models;

use App\Models\ContratoBase as Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Spatie\Activitylog\Traits\LogsActivity;
use Dompdf\Dompdf;

class Contratoarquivo extends Model
{
    use CrudTrait;
    use LogsActivity;
    use SoftDeletes;

    protected static $logFillable = true;
    protected static $logName = 'contrato_arquivos';

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'contrato_arquivos';
    protected $fillable = [
        'contrato_id',
        'tipo',
        'processo',
        'sequencial_documento',
        'descricao',
        'arquivos',
        'origem',
        'link_sei',
        'envio_pncp_pendente',
        'link_pncp',
        'sequencial_pncp',
        'contratohistorico_id',
        'justificativa_exclusao',
        'retorno_pncp',
        'assinaturas_documento',
        'restrito',
        'envio_v2',
        'contrato_opm_id',
        'rascunho',
        'contratopreposto_id'
    ];

    protected $casts = [
        'arquivos' => 'array'
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getContrato()
    {
        return $this->getContratoNumero();
    }

    public function getTipo()
    {
        return $this->codigoItem()->first()->descricao;
    }

    /*
     * @autor: Álvaro
     * @Data : 09.09.2021
     * @Imprime a origem do anexo
     */
    public function getOrigemAnexo()
    {
        switch ($this->origem) {
            case 0:
                $origem = 'Arquivo';
                break;
            case 1:
                $origem = 'Super (SEI)';
                break;
            case 2:
                $origem = 'Minuta';
                break;
            case 3:
                $origem = 'Indicação Preposto';
                break;
            default:
                    $origem = 'Origem desconhecida';
        }

        return  $origem;
    }

    public function formatarTextoRestricao()
    {
        $textoExibicao = 'Não';

        if ($this->restrito) {
            $textoExibicao = 'Sim';
        }
        return $textoExibicao;
    }

    /*
     * @autor: Álvaro/Polyana - INSS
     * @Data : 09.11.2021
     * @Imprime links da grid principal de anexos
     */
    public function getLinkAnexo()
    {

        if ($this->origem==0) {
            // $url = '';
            // if (is_array($this->arquivos)):
            //     $url = Storage::url($this->arquivos[0]);
            // else:
            //     $url = Storage::url($this->arquivos);
            // endif;

            // if(filter_var($this->arquivos, FILTER_VALIDATE_URL)) {
            //     return '<a href="'.$this->arquivos.'" target="_blank">'.$this->descricao.'</a>';
            // } else {
            //     return '<a href="'. url("gescon/consulta/download-arquivo-contrato/{$this->id}") .'" target="_blank">'.$this->descricao.'</a>';
            // }

            $retorno = $this->montarUrlDownload($this->arquivos, $this->descricao, $this->id);

            // $retorno = '<a href="'.$url.'" target="_blank">'.$this->descricao.'-'.$this->id.'</a>';
        } elseif ($this->origem==1) {
            $retorno = "<a href='".$this->link_sei."' target='_blank'>".$this->descricao."</a>";
        } elseif ($this->origem==2) {
            $retorno = "<a href='".$this->link_sei."' target='_blank'>".$this->descricao."</a>";
        }

        return $retorno;
    }

    private function montarUrlDownload(?string $campoArquivo, ?string $texto, ?int $id)
    {
        if (filter_var($campoArquivo, FILTER_VALIDATE_URL)) {
            return '<a href="'.$campoArquivo.'" target="_blank">'.$texto.'</a>';
        } else {
            return '<a href="'. url("gescon/consulta/download-arquivo-contrato/{$id}") .'" target="_blank">'.$texto.'</a>';
        }
    }


    /**
     * @author: Aruã Magalhães
     * @date: 05/08/2022
     * Método temporário para o download do arquivo na grid, devido o ambiente de desenvolvimento não está funcionando o download.
     *
     */

    public function getLinkAnexoTemporario()
    {
        // return '<a href="'. url("gescon/consulta/download-arquivo-contrato/{$this->id}") .'" target="_blank">'.$this->descricao.'</a>';
        return $this->montarUrlDownload($this->arquivos, $this->descricao, $this->id);
    }

    public function getListaArquivosComPath()
    {
        $arquivos_array = [];
        $i = 1;
        foreach ($this->arquivos as $arquivo) {
            $arquivos_array[] = [
                'arquivo_' . $i => env('APP_URL') . '/storage/' . $arquivo,
            ];
            $i++;
        }
        return $arquivos_array;
    }

    public function arquivoAPI()
    {
        return [
                'id' => $this->id,
                'contrato_id' => $this->contrato_id,
                'tipo' => $this->getTipo(),
                'processo' => $this->processo,
                'sequencial_documento' => $this->sequencial_documento,
                'descricao' => $this->descricao,
                'path_arquivo' => env('APP_URL') . '/storage/' . $this->arquivos, //Issue #137
                'origem' => ($this->arquivo=='0') ? 'Sistema' : 'SEI',
                'link_sei' => $this->link_sei,
        ];
    }

    public function serializaArquivoPNCP()
    {
        //Log::channel('schedule')->info("---------Serializando arquivo para o PNCP---------");
        if (isset($this->link_sei)) {
            //Log::channel('schedule')->info("--------- Buscando arquivo em: ".(Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."sei_temp_pncp/".$this->contrato->unidade->orgao->codigo. '/' . $this->contrato_id. '/' .$this->id.'.pdf')."---------");
            return Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."sei_temp_pncp/".$this->contrato->unidade->orgao->codigo. '/' . $this->contrato_id. '/' .$this->id.'.pdf';
             //glob($path.'*')[0];
        } else {
            $filepath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
            //Log::channel('schedule')->info("---------Não é SEI: ".$filepath."---------");
            //Log::channel('schedule')->info($filepath . "/" . ((is_array($this->arquivos)) ? $this->arquivos[0].'[ARR]' : $this->arquivos.'[NARR]'));
            //return config('APP_PATH').'/storage/app/'.$this->arquivos[0];
            return $filepath . "/" . ((is_array($this->arquivos)) ? $this->arquivos[0] : $this->arquivos);
        }
    }

    public function serializaJustificativaMinutaDocumetoPNCP()
    {
        return [
            'justificativa' => 'Inserção automática pelo Padrão de Minuta',
        ];
    }

    public function serializaExclusaoPNCP()
    {
        return [
            'justificativa' => isset($this->justificativa_exclusao)? $this->justificativa_exclusao : ' ',
        ];
    }

    public function salvaTempDocumentoSEI()
    {
        try {
            // Log::channel('schedule')->info("---------salvaTempDocumentoSEI---------");
            $path = "sei_temp_pncp/".$this->contrato->unidade->orgao->codigo. '/' . $this->contrato_id.'/';
            $nomeArquivo = $this->id;
            // Log::channel('schedule')->info("--------- Path:".$path." ---------");
            // Log::channel('schedule')->info("--------- LinkSei:".$this->link_sei." ---------");
            $ch = curl_init($this->link_sei);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_ENCODING, '');

            $arquivo = curl_exec($ch);
            // Log::channel('schedule')->info("---------arquivo: ".$arquivo." ---------");

            // Log::channel('schedule')->info("---------tipos_sei: ".curl_getinfo($ch, CURLINFO_CONTENT_TYPE)." ---------");

            $ext = config('api-pncp.tipos_sei')[curl_getinfo($ch, CURLINFO_CONTENT_TYPE)];
            // Log::channel('schedule')->info("---------Ext: ".$ext." ---------");
            curl_close($ch);
            Storage::put($path.$nomeArquivo.'.'.$ext, $arquivo);
            if ($ext == "html") {
                //Log::channel('schedule')->info("---------Iniciando conversão de HTML---------");
                $arquivo = $this->converteHTMLparaPDF($path, $nomeArquivo);
                unlink($path.$nomeArquivo.'.html');
                $ext = "pdf";
            };
            //Log::channel('schedule')->info("---------Salvo em: ".Storage::path($path.$nomeArquivo.'.'.$ext)." ---------");
            return;
        } catch (Exception $e) {
            //Não colocar no Log erro de dados provindos de Web Services devido a vulnerabilidades de XSS
            Log::error("Erro ao salvar arquivo do SEI (Contrato: ".$this->contrato_id.", Arquivo: ".$this->id.", Link SEI: ".$this->link_sei.")");
            Log::error("Erro ao salvar arquivo do SEI". $e->getMessage());
            throw $e;
        }
    }

    public function mudarStatusRestricao()
    {
        if ($this->restrito) {
            return "mudarstatusrestricao/$this->id/0";
        }

        return "mudarstatusrestricao/$this->id/1";
    }

    public function converteHTMLparaPDF($path, $nomeArquivo)
    {
        try {
        /*
        //Implementação barryvdh/snappy https://github.com/KnpLabs/snappy
            $snappy = new Pdf();
            $snappy->setBinary("C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe");
            $html = Storage::get($path.$nomeArquivo.'.html');
            $snappy->generateFromHtml($html, Storage::path($path).$nomeArquivo.'.pdf', ['encoding' => 'UTF8']);
        */

        /*
        //Implementação barryvdh/laravel-dompdf https://github.com/barryvdh/laravel-dompdf
        PDF::loadFile(Storage::get($path.$nomeArquivo.'.html'))->save(Storage::path($path).$nomeArquivo.'.pdf');
        */

        //Implementação DOMPDF/Dompdf https://github.com/dompdf/dompdf
            $dompdf = new Dompdf();
        // Log::channel('schedule')->info("---------DOMPDF instanciado---------");
            $dompdf->loadHtml(Storage::get($path.$nomeArquivo.'.html'), 'iso-8859-1');
        // Log::channel('schedule')->info("---------DOMPDF carregou o HTML---------");
            $dompdf->render();
        // Log::channel('schedule')->info("---------DOMPDF converteu---------");
            Storage::put($path.$nomeArquivo.'.pdf', $dompdf->output());
        // Log::channel('schedule')->info("---------Salvo no Storage---------");
        } catch (Exception $e) {
            //Não colocar no Log erro de dados provindos de Web Services devido a vulnerabilidades de XSS
            Log::error("Erro ao converter arquivo HTML do SEI (Contrato: ".$this->contrato_id.", Arquivo: ".$this->id.", Link SEI: ".$this->link_sei.")");
        }
    }

    public function excluiTempDocumentoSEI()
    {
        try {
            //Log::channel('schedule')->info("---------Excluindo arquivo PDF---------");
            $path = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix()."sei_temp_pncp/".$this->contrato->unidade->orgao->codigo. '/' . $this->contrato_id. '/' .$this->id.'.pdf';
            //Log::channel('schedule')->info("---------Local do arquivo: ".$path." ---------");
            unlink($path);
            //Log::channel('schedule')->info("---------PDF excluído---------");
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function buscaArquivosPorContratoId(int $contrato_id, $range)
    {
        $arquivos = $this::whereHas('contrato', function ($c) {
            $c->whereHas('unidade', function ($u) {
                $u->where('sigilo', "=", false);
            });
        })
            ->where('contrato_id', $contrato_id)
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('contrato_arquivos.updated_at', [$range[0], $range[1]]);
            })
            ->get();

        return $arquivos;
    }

    public function buscaArquivos($range)
    {
        $arquivos = $this::whereHas('contrato', function ($c) {
            $c->whereHas('unidade', function ($u) {
                $u->where('sigilo', "=", false);
            });
        })
            ->when($range != null, function ($d) use ($range) {
                $d->whereBetween('contrato_arquivos.updated_at', [$range[0], $range[1]]);
            })
            ->get();

        return $arquivos;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function envia_dados_pncp()
    {
        return $this->morphToMany(EnviaDadosPncp::class, 'pncpable');
    }

    public function contrato()
    {
        return $this->belongsTo(Contrato::class, 'contrato_id');
    }

    public function codigoItem()
    {
        return $this->belongsTo(Codigoitem::class, 'tipo');
    }

    public function contrato_historico()
    {
        return $this->belongsTo(Contratohistorico::class, 'contratohistorico_id');
    }

    public function contratoPreposto()
    {
        return $this->belongsTo(Contratopreposto::class, 'contratopreposto_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function saveWithoutEvents(array $options = [])
    {
        return static::withoutEvents(function () use ($options) {
            return $this->save($options);
        });
    }
}
