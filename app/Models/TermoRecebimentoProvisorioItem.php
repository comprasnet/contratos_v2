<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TermoRecebimentoProvisorioItem extends Model
{
    use HasFactory;
    use CrudTrait;

    protected $table = 'termo_recebimento_provisorio_itens';

    protected $fillable = [
        'termo_recebimento_provisorio_id',
        'entrega_item_id',
        'itemable_id',
        'itemable_type',
        'quantidade_informada',
        'valor_glosa',
        'mes_ano_competencia',
        'processo_sei',
        'data_inicio',
        'data_fim',
        'horario',
    ];

    public function trp()
    {
        return $this->belongsTo(TermoRecebimentoProvisorio::class, 'termo_recebimento_provisorio_id');
    }

    public function entregaItem()
    {
        return $this->belongsTo(EntregaItem::class);
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getAutorizacaoExecucaoItem()
    {
        return AutorizacaoexecucaoItens::find($this->itemable_id);
    }
}
