<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContratoLocalExecucao extends Model
{
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    /**
     * The table associated with the model.
     *
     * @var string
     */
     protected $table = 'contrato_local_execucao';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'contrato_id',
        'endereco_id',
        'descricao',
     ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

     public function contrato()
     {
         return $this->belongsTo(Contrato::class, 'contrato_id');
     }

     public function enderecos()
     {
         return $this->belongsTo(Enderecos::class, 'endereco_id');
     }

     public function autorizacoesExecucao()
     {
         return $this->belongsToMany(
             AutorizacaoExecucao::class,
             'autorizacaoexecucao_contrato_local_execucao',
             'contrato_local_execucao_id',
             'autorizacaoexecucao_id'
         );
     }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
