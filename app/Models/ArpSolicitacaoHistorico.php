<?php

namespace App\Models;

use App\Http\Traits\Formatador;
use App\Repositories\Contrato\ContratoResponsavelRepository;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ArpSolicitacaoHistorico extends Model
{
    use HasFactory, CrudTrait, LogsActivity, Formatador;

    protected $table = 'arp_solicitacao_historico';

    protected $fillable = [
        'arp_solicitacao_id',
        'data_envio_analise_fornecedor',
        'data_aprovacao_analise_fornecedor',
        'usuario_id',
        'situacao_id',
    ];

    public function adesao()
    {
        return $this->belongsTo(Adesao::class, 'arp_solicitacao_id');
    }
    public function situacao()
    {
        return $this->belongsTo(CodigoItem::class, 'situacao_id');
    }

    public function usuario()
    {
        return $this->belongsTo(BackpackUser::class, 'usuario_id');
    }
    public function unidade()
    {
        return $this->belongsTo(Unidade::class, 'unidade_id');
    }
}
