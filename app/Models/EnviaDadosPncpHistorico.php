<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use App\Services\Pncp\Responses\PncpPostResponseService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Traits\BuscaCodigoItens;

class EnviaDadosPncpHistorico extends Model
{
    use HasFactory, SoftDeletes, CrudTrait, BuscaCodigoItens;

    protected static $logFillable = true;
    protected static $logName = 'enviadadospncphistorico';

    protected $table = 'envia_dados_pncp_historico';

    protected $fillable = [
        'id',
        'envia_dados_pncp_id',
        'pncpable_type',
        'pncpable_id',
        'log_name',
        'status_code',
        'json_enviado_inclusao',
        'json_enviado_alteracao',
        'link_pncp',
        'situacao',
        'contrato_id',
        'sequencialPNCP',
        'tipo_contrato',
        'linkArquivoEmpenho',
        'retorno_pncp',
    ];

    public function enviaDadosPncp()
    {
        return $this->belongsTo(EnviaDadosPncp::class, 'envia_dados_pncp_id');
    }

    public function getTipoToListView()
    {
        $arrayPncpableName = explode("\\", $this->enviaDadosPncp->pncpable_type);

        return end($arrayPncpableName);
    }

    /**
     * Verifica se é permitido reenviar para o PNCP
     * @return bool
     */
    public function allowSendToPNCP()
    {
        $permitirReenviar = false;


        if ($this->log_name === 'retificar_ata' ||
            $this->log_name === 'inserir_ata' ||
            $this->log_name === 'inserir_arquivo_ata' ||
            $this->log_name === 'deletar_arquivo_ata'
        ) {
            $ultimaRetificacaoComSucesso = $this::where('pncpable_id', $this->pncpable_id)
                ->where('log_name', $this->log_name)
                ->whereIn('status_code', PncpPostResponseService::$successCode)
                ->latest()
                ->first();

            $ultimaRetificacaoComErro = $this::where('pncpable_id', $this->pncpable_id)
                ->where('log_name', $this->log_name)
                ->whereNotIn('status_code', PncpPostResponseService::$successCode)
                ->latest()
                ->first();

            $permitirReenviar = $ultimaRetificacaoComErro &&
                $ultimaRetificacaoComErro->id === $this->id &&
                !$ultimaRetificacaoComSucesso ||
                $ultimaRetificacaoComErro &&
                $ultimaRetificacaoComErro->id === $this->id &&
                $ultimaRetificacaoComSucesso &&
                $this->id > $ultimaRetificacaoComSucesso->id;
        }

        return $permitirReenviar;
    }
}
