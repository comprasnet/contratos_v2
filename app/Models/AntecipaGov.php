<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AntecipaGov extends Model
{
    use LogsActivity;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected static $logFillable = true;
    protected static $logName = 'antecipagov';

    protected $table = 'antecipagov';

    protected $fillable = [
        'conta_bancaria',
        'num_agencia',
        'num_banco',
        'status_operacao',
        'num_operacao',
        'num_cotacao',
        'identificador_unico',
        'data_acao',
        'valor_operacao',
        'valor_parcela',
        'situacao_id'
    ];


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function contratos(){
        return $this->belongsToMany(
            Contrato::class,
            'contrato_antecipagov',
            'antecipagov_id',
            'contrato_id'
        );
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */



}
