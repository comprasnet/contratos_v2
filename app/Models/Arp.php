<?php

namespace App\Models;

use App\Http\Traits\Formatador;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Spatie\Activitylog\Traits\LogsActivity;

class Arp extends Model
{
    use CrudTrait;
    use SoftDeletes;
    use Formatador;
    use LogsActivity;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'arp';
    
    protected static $logFillable = true;
    protected static $logName = 'arp';
    
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'unidade_origem_id',
        'compra_id',
        'tipo_id',
        'data_assinatura',
        'vigencia_inicial',
        'vigencia_final',
        'valor_total',
        'numero',
        'ano',
        'rascunho',
        'modalidade_id',
        'objeto',
        'rascunho',
        'compra_centralizada',
        'numero_processo',
        'unidade_origem_compra_id'
    ];


    // protected $hidden = [];
    public static $datesGovBr = ['data_assinatura', 'vigencia_inicial', 'vigencia_final'];

    protected $casts = [
        'compra_centralizada' => 'boolean'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getCompraCentralizada()
    {
        if ($this->compra_centralizada) {
            return "Sim";
        }

        return "Não";
    }

    public function getNumeroCompra()
    {
        return $this->compras->numero_ano;
    }

    public function getNumeroAno()
    {
        if ($this->numero !== null && $this->ano !== null) {
            return $this->numero . '/' . $this->ano;
        }

        return ''; // qualquer valor padrão desejado se as variáveis não estiverem definidas corretamente
    }

    public function getCatMatSerItem($id, $numero_item)
    {
        $result = ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->join("compra_items", "compra_items.id", "=", "compra_item_fornecedor.compra_item_id")
            ->join('catmatseritens', 'catmatseritens.id', '=', 'compra_items.catmatseritem_id')
            ->join('catmatsergrupos', 'catmatseritens.grupo_id', '=', 'catmatsergrupos.id')
            ->join('codigoitens', 'codigoitens.id', '=', 'catmatsergrupos.tipo_id')
            ->where("arp_item.arp_id", $id)
            ->where("compra_items.numero", $numero_item)
            ->selectRaw("CASE
                        WHEN compra_items.permite_carona = true THEN 'Sim'
                        WHEN compra_items.permite_carona = false THEN 'Não'
                        END AS aceita_adesao,
                        codigoitens.descricao , codigoitens.descres,
                        compra_items.numero, compra_items.descricaodetalhada, compra_items.maximo_adesao")
            ->distinct("compra_items.numero")
            ->orderBy("compra_items.numero")
            ->first();

        return $result->tipo_item;
    }

    public function getValorTotal()
    {
        return $this->valor_total;
    }

    /**
     * Método responsável em retornar a unidade de origem formatada
     */
    public function getUnidadeOrigem()
    {
        return $this->unidades->exibicaoCompletaUASG();
    }

    public function getUnidadeGerenciadora()
    {
        switch ($this->tipo_uasg) {
            case 'G':
                $textoTipoUasg = 'Gerenciadora';
                break;
            case 'P':
                $textoTipoUasg = 'Participante';
                break;
            case 'C':
                $textoTipoUasg = 'Não participante'; # Carona
                break;
        }
        return $textoTipoUasg;
    }

    /**
     * Método responsável em retornar a unidade de origem da compra formatada
     */
    public function getUnidadeOrigemCompra()
    {
        $unidadeOrigemCompra = $this->compras->unidadeOrigem;

        return $unidadeOrigemCompra->codigo . "-" . $unidadeOrigemCompra->nomeresumido;
    }

    public function getModalidadeCompra()
    {
        return $this->compras->getModalidade();
    }

    public static function getGestorAta(int $idAta)
    {
        $arp = Arp::find($idAta);
        $gestores = array();
        foreach ($arp->gestor as $key => $gestor) {
            $gestores['Nome'][$key] = $gestor->user->name;
            $gestores['Email'][$key] = $gestor->user->email;
        }

        return $gestores;
    }

    public static function getAutoridadeSignataria(int $idAta)
    {
        $arp = Arp::find($idAta);
        $autoridades = array();

        foreach ($arp->autoridade_signataria as $key => $autoridade_signataria) {
            if (isset($autoridade_signataria->user)) {
                $autoridades['Nome'][$key] = $autoridade_signataria->user->autoridade_signataria;
                $autoridades['Cargo'][$key] = $autoridade_signataria->user->cargo_autoridade_signataria;
            }
        }

        return $autoridades;
    }

    public static function getUnidadeParticipante(int $idAta)
    {
        $unidades = ArpItem::getUnidadeParticipantePorAta($idAta, false);

        $unidadeParticipante = array();
        foreach ($unidades as $key => $unidade) {
            $unidadeParticipante['Código'][$key] = $unidade->codigo;
            $unidadeParticipante['UASG'][$key] = $unidade->nomeresumido;
            $textoTipoUasg = '';
            switch ($unidade->tipo_uasg) {
                case 'G':
                    $textoTipoUasg = 'Gerenciadora';
                    break;
                case 'P':
                    $textoTipoUasg = 'Participante';
                    break;
                case 'C':
                    $textoTipoUasg = 'Não participante'; # Carona
                    break;
            }

            $unidadeParticipante['Tipo'][$key] = $textoTipoUasg;
        }
        return $unidadeParticipante;
    }

    public static function getUnidadeParticipanteItens(int $idAta, $numero_item)
    {
        $unidadesItens = ArpItem::getUnidadeParticipantePorAtaItem($idAta, $numero_item);

        $unidadeParticipanteItem = array();

        foreach ($unidadesItens as $key => $unidadeItem) {
            $unidadeParticipanteItem['Código'][$key] = $unidadeItem->codigo;
            $unidadeParticipanteItem['Unidade'][$key] = $unidadeItem->nomeresumido;
            $unidadeParticipanteItem['Tipo da unidade'][$key] = $unidadeItem->tipo_uasg;
            $unidadeParticipanteItem['Quantidade_registrada'][$key] = $unidadeItem->quantidade_autorizada;
            $unidadeParticipanteItem['Quantidade_disponível_para_remanejamento/empenho'][$key] =
                $unidadeItem->quantidade_saldo;
            //$unidadeParticipanteItem['Quantidade_Homologada'][$key] = $unidadeItem->quantidade_homologada_total;
        }

        return $unidadeParticipanteItem;
    }

    /*
        public static function getPorAtaMinutaEmpenhoItens(int $situacao,$numero_item )
        {
            $empenhosItens = ArpItem::getItemPorAtaMinutaEmpenho($situacao,  $numero_item) ;

            $empenhoMinutaItens = array();

            foreach ($empenhosItens as $key => $empenhoItens) {
                $empenhoMinutaItens['mensagem_siafi'][$key] = $empenhoItens->mensagem_siafi;
                //$empenhoMinutaItens['created_at'][$key] = $empenhoItens->created_at;
                //$empenhoMinutaItens['Codigo'][$key] = $empenhoItens->codigo;

            }

            return $empenhoMinutaItens;
        }
    */
    public static function getPorAtaMinutaEmpenhoItens(int $situacao, string $numero_item, int $idAta)
    {
        $empenhosItens = ArpItem::getItemPorAtaMinutaEmpenho($situacao, $numero_item, $idAta);

        $listaEmpenhoMinutaItens = array();

        foreach ($empenhosItens as $key => $empenhoItens) {
            $dataFormat = '';
            if ($empenhoItens->created_at) {
                $dataFormat = Carbon::createFromFormat('Y-m-d H:i:s', $empenhoItens->created_at)->format('d/m/Y');
            }
            $listaEmpenhoMinutaItens['Número do Empenho'][$key] = $empenhoItens->mensagem_siafi;
            $listaEmpenhoMinutaItens['Data do Empenho'][$key] = $dataFormat;
            $listaEmpenhoMinutaItens['Quantidade Empenhada'][$key] = $empenhoItens->quantidade_minuta_empenhada;
            //  $listaEmpenhoMinutaItens['Soma Autorizada'][$key] = $empenhoItens->soma_quantidade_autorizada;
            //$listaEmpenhoMinutaItens['Quantidade Registrada'][$key] = $empenhoItens->quantidade_registrada;
            //$listaEmpenhoMinutaItens['Saldo para Empenho'][$key] = $empenhoItens->saldo_minuta_empenho;
            // adicionando a coluna "fornecedor"
            $listaEmpenhoMinutaItens['Fornecedor do Empenho'][$key] = $empenhoItens->fornecedor_empenho;
        }

        return $listaEmpenhoMinutaItens;
    }

    /**
     * Obtém as quantidades totais registrada e autorizada para um item e retorna a soma das duas
     *
     * @param integer $idAta id da ata
     * @param string $numero_item numero do item
     * @param bool $format Indica se retorna o valor formatado ou não
     * @return integer soma das quantidades totais registradas e autorizadas
     */
    public static function getQuantidadeTotalRegistradaAutorizadaDeUmItem($idAta, $numero_item, $format = true)
    {
        $quantidadeTotalRegistrada = ArpItem::getQuantidadeTotalRegistrada($idAta, $numero_item);
        $quantidadetotalAprovadaParaAdesao = ArpItem::getQuantidadeTotalAprovadaParaAdesao($idAta, $numero_item);

        $quantidadeTotalRegistradaAutorizada = $quantidadeTotalRegistrada->quantidade_total_registrada
            +
            $quantidadetotalAprovadaParaAdesao->quantidade_total_aprovada_adesao;

        if ($format) {
            return number_format(
                $quantidadeTotalRegistradaAutorizada,
                5,
                '.',
                ''
            );
        }

        return $quantidadeTotalRegistradaAutorizada;
    }

    /**
     * Calcula a quantidade total de saldo disponível para empenho, para um item
     *
     * @param int $idAta id da ata
     * @param int $numero_item número do item
     * @return float quantidade total de saldo disponível para empenho
     */
    public static function getQuantidadeTotalSaldoEmpenhoDeUmItem($idAta, $numero_item)
    {
        $quantidadeTotalRegistradaAutorizada =
            Arp::getQuantidadeTotalRegistradaAutorizadaDeUmItem($idAta, $numero_item, false);

        $quantidadeTotalEmpenhada = ArpItem::getTotaisMinutaEmpenhoPorUnidade(
            $idAta,
            $numero_item,
            'quantidade_total_empenhada'
        );
        return number_format(
            $quantidadeTotalRegistradaAutorizada - $quantidadeTotalEmpenhada[0]->quantidade_total_empenhada,
            5,
            '.',
            ''
        );
    }

    /**
     * Retorna para um item da ata, os totais das minutas de empenho agrupados por unidade
     *
     * @param int $idAta
     * @param string $numero_item
     * @return array formatado para ser utilizado pelo backpack para montar tabela na tela
     */
    public static function getItemTotaisMinutaEmpenhoPorUnidade($idAta, $numero_item): array
    {
        $totaisPorUnidade = ArpItem::getTotaisMinutaEmpenhoPorUnidade($idAta, $numero_item);
        $listTotaisPorUnidade = array();

        foreach ($totaisPorUnidade as $key => $unidade) {
            $listTotaisPorUnidade['Unidade'][$key] = $unidade->unidade;
            $listTotaisPorUnidade['Tipo'][$key] = $unidade->tipo_uasg;
            $listTotaisPorUnidade['Quantidade registrada'][$key] = $unidade->quantidade_autorizada;
            $listTotaisPorUnidade['Quantidade empenhada'][$key] = $unidade->quantidade_empenhada;
            $listTotaisPorUnidade['Saldo para empenho'][$key] = $unidade->saldo_para_empenho;
        }
        return $listTotaisPorUnidade;
    }

    /**
     * Retorna, para um item da ata, os empenhos por unidade, com seus respectivos totais
     *
     * @param int $idAta
     * @param string $numero_item
     * @return array formatado para ser utilizado pelo backpack para montar tabela na tela
     */
    public static function getItemMinutasEmpenhoPorUnidade($idAta, $numero_item): array
    {
        $empenhosPorUnidade = ArpItem::getMinutasEmpenhoPorUnidade($idAta, $numero_item);
        $listEmepenhosPorUnidade = array();

        foreach ($empenhosPorUnidade as $key => $empenho) {
            $listEmepenhosPorUnidade['Número de empenho'][$key] = $empenho->mensagem_siafi;
            $listEmepenhosPorUnidade['Unidade'][$key] = $empenho->unidade;
            $listEmepenhosPorUnidade['Fornecedor'][$key] = $empenho->fornecedor;
            $listEmepenhosPorUnidade['Data do empenho'][$key] = Carbon::parse($empenho->data_emissao)->format('d/m/Y');
            $listEmepenhosPorUnidade['Quantidade incluída'][$key] = $empenho->quantidade_inclusao;
            $listEmepenhosPorUnidade['Reforço'][$key] = $empenho->quantidade_reforco;
            $listEmepenhosPorUnidade['Anulação'][$key] = $empenho->quantidade_anulacao;
            $listEmepenhosPorUnidade['Quantidade empenhada'][$key] = $empenho->quantidade_empenhada;
            $listEmepenhosPorUnidade['Valor'][$key] = $empenho->valor;
        }
        return $listEmepenhosPorUnidade;
    }

    /**
     * Obtém, para cada unidade, as solicitações já analisadas (aprovadas total ou parcialmente)
     *
     * @param integer $idAta ID da Ata de registro de preços
     * @param string $numero_item Número do item da ata
     * @return array Array contendo as unidades e suas respectivas quantidades aprovadas
     */
    public static function getAdesoesAprovadasDoItemPorUnidade(int $idAta, string $numero_item)
    {
        $adesoesAprovadasDoItemPorUnidade = ArpItem::getAdesoesItemPorUnidade($idAta, $numero_item, 'analisada');
        $listAdesoes = array();

        foreach ($adesoesAprovadasDoItemPorUnidade as $key => $adesaoUnidade) {
            $listAdesoes['Unidade'][$key] = $adesaoUnidade->unidade_solicitacao;
            $dataAprovacao = new DateTime($adesaoUnidade->data_aprovacao_analise);
            $listAdesoes['Data aprovação análise'][$key] = $dataAprovacao->format('d/m/Y, H:i');
            $listAdesoes['Quantidade aprovada da adesão'][$key] = $adesaoUnidade->quantidade_aprovada;
        }

        return $listAdesoes;
    }

    /**
     * Obtém, para cada unidade, as solicitações que estão aguardando análise
     *
     * @param integer $idAta ID da Ata de registro de preços
     * @param string $numero_item Número do item da ata
     * @return array Array contendo as unidades e suas respectivas quantidades solicitadas
     */
    public static function getAdesoesAguardandoAnaliseDoItemPorUnidade(int $idAta, string $numero_item)
    {
        $adesoesAguardandoAnaliseDoItemPorUnidade =
            ArpItem::getAdesoesItemPorUnidade($idAta, $numero_item, 'aguardando_analise');

        $listAdesoes = array();

        foreach ($adesoesAguardandoAnaliseDoItemPorUnidade as $key => $adesaoUnidade) {
            $listAdesoes['Unidade'][$key] = $adesaoUnidade->unidade_solicitacao;
            $listAdesoes['Data/hora da solicitação'][$key] =
                Carbon::parse($adesaoUnidade->data_hora_solicitacao)->format('d/m/Y H:i');
            $listAdesoes['Quantidade aguardando análise'][$key] = $adesaoUnidade->quantidade_solicitada;
        }

        return $listAdesoes;
    }

    /**
     * Obtém, para cada unidade, as solicitações que foram negadas por completo
     *
     * @param integer $idAta ID da Ata de registro de preços
     * @param string $numero_item Número do item da ata
     * @return array Array contendo as unidades e suas respectivas quantidades solicitadas
     */
    public static function getAdesoesNegadasDoItemPorUnidade(int $idAta, string $numero_item)
    {
        $adesoesNegadasDoItemPorUnidade = ArpItem::getAdesoesItemPorUnidade($idAta, $numero_item, 'negada');
        $listAdesoes = array();

        foreach ($adesoesNegadasDoItemPorUnidade as $key => $adesaoUnidade) {
            $listAdesoes['Unidade'][$key] = $adesaoUnidade->unidade_solicitacao;
            $listAdesoes['Quantidade solicitada'][$key] = $adesaoUnidade->quantidade_solicitada;
        }

        return $listAdesoes;
    }

    public static function getDetalhamentoContratosItens(int $idAta, string $numero_item)
    {
        $contratos = ArpItem::getContratosItem($idAta, $numero_item);
        $listContratos = array();
        foreach ($contratos as $key => $contrato) {
            $listContratos['id'][$key] = $contrato->id;
            $listContratos['Número do contrato'][$key] = $contrato->numero;
            $listContratos['Unidade'][$key] = $contrato->unidade;
            $listContratos['Fornecedor'][$key] = $contrato->fornecedor_contrato;
            $listContratos['Quantidade contratada'][$key] = number_format($contrato->quantidade_contratada, 5, '.', '');
        }
        return $listContratos;
    }

    public static function getItemAtaSimplificada(int $idAta)
    {
        $itens = ArpItem::getItemPorAtaSimplificada($idAta);
        $listaItem = array();

        foreach ($itens as $key => $itemSelecionado) {
            $listaItem['Número'][$key] = $itemSelecionado->numero;
            $listaItem['Item'][$key] = $itemSelecionado->descricaodetalhada;
            $listaItem['Quantidade_máxima_adesão'][$key] = $itemSelecionado->maximo_adesao;
            $listaItem['Aceita_adesão'][$key] = $itemSelecionado->aceita_adesao;
        }

        return $listaItem;
    }

    /**
     * Busca os arquivos de uma ata e monta em um array para ser utilizado pelo backpack
     *
     * @param int $idArp id de uma ata de Registro de Preços
     * @return array Array montado de forma a ser utilizado pelo backpack
     */
    public static function getArquivosTransparencia(int $idAta): array
    {
        # Busca os arquivos de uma ata
        $arquivos = ArpArquivo::getArquivosPorAta($idAta);

        $listaArquivos = array();

        foreach ($arquivos as $key => $arquivo) {
            $listaArquivos['Descrição'][$key] = $arquivo['nome'];
            $listaArquivos['Tipo'][$key] = $arquivo['tipo'];

            # Formata a data "indluído em" para exibição
            $incluidoEm = (new \DateTime($arquivo['created_at']))->format('d/m/Y H:i');
            $listaArquivos['Incluído em'][$key] = $incluidoEm;

            $listaArquivos['url'][$key] = $arquivo['url'];
        }

        return $listaArquivos;
    }

    public static function getFornecedorCompra(int $idAta)
    {
        $fornecedores = ArpItem::getFornecedorPorAta($idAta);

        $listaFornecedor = array();
        foreach ($fornecedores as $key => $fornecedor) {
            $listaFornecedor['CNPJ'][$key] = $fornecedor->cpf_cnpj_idgener;
            $listaFornecedor['Fornecedor'][$key] = $fornecedor->nome;
        }

        return $listaFornecedor;
    }

    public function getFornecedorPorAta()
    {
        $fornecedores = ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->where("arp_item.arp_id", $this->id)
            ->selectRaw("CONCAT(cpf_cnpj_idgener, ' - ' ,nome) AS nome_fornecedor, cpf_cnpj_idgener, nome")
            ->groupBy("cpf_cnpj_idgener", "nome")
            ->get();

        foreach ($fornecedores as $fornecedor) {
            return $fornecedor['nome_fornecedor'];
        }
    }

    public function getFornecedorPorAtaTransparencia()
    {
        $fornecedores = ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->where("arp_item.arp_id", $this->id)
            ->selectRaw("CONCAT(cpf_cnpj_idgener, ' - ' ,nome) AS nome_fornecedor, cpf_cnpj_idgener, nome")
            ->groupBy("cpf_cnpj_idgener", "nome")
            ->get();

        $fornecedoresArray = []; // Array para armazenar os fornecedores

        foreach ($fornecedores as $fornecedor) {
            $fornecedoresArray[] = $fornecedor['nome_fornecedor']; // Adicionar o fornecedor ao array
        }


        return $fornecedoresArray;
    }

    public static function getFornecedorPorAtaItem(int $idAta, string $numero_item)
    {
        $fornecedores = ArpItem::join(
            "compra_item_fornecedor",
            "arp_item.compra_item_fornecedor_id",
            "=",
            "compra_item_fornecedor.id"
        )
            ->join("fornecedores", "fornecedores.id", "=", "compra_item_fornecedor.fornecedor_id")
            ->join("compra_items", "compra_items.id", "=", "compra_item_fornecedor.compra_item_id")
            ->where("compra_items.numero", $numero_item)
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('arp_item_historico')
                    ->join(
                        'arp_historico',
                        'arp_historico.id',
                        '=',
                        'arp_item_historico.arp_historico_id'
                    )
                    ->whereRaw('arp_item_historico.arp_item_id = arp_item.id')
                    ->where('arp_item_historico.item_cancelado', true);
            })
            ->where('arp_id', '=', $idAta)
            ->select(
                DB::raw(
                    "compra_item_fornecedor.quantidade_homologada_vencedor,
                    compra_item_fornecedor.valor_unitario,
                    compra_item_fornecedor.classificacao"
                )
            )
            ->selectRaw("CONCAT(cpf_cnpj_idgener, ' - ' ,nome) AS nome_fornecedor, cpf_cnpj_idgener, nome")
            ->groupBy(
                "cpf_cnpj_idgener",
                "nome",
                "compra_item_fornecedor.quantidade_homologada_vencedor",
                "compra_item_fornecedor.valor_unitario",
                "compra_item_fornecedor.classificacao"
            )
            ->orderby("compra_item_fornecedor.classificacao")
            ->get();

        $listaFornecedor = array();
        foreach ($fornecedores as $key => $fornecedor) {
            $listaFornecedor['Classificação'][$key] = $fornecedor->classificacao;
            $listaFornecedor['CNPJ'][$key] = $fornecedor->cpf_cnpj_idgener;
            $listaFornecedor['Fornecedor'][$key] = $fornecedor->nome;
            $listaFornecedor['Quantidade_total'][$key] = number_format(
                $fornecedor->quantidade_homologada_vencedor,
                5,
                ',',
                '.'
            );
            $listaFornecedor['Valor_unitário'][$key] = 'R$ ' . number_format($fornecedor->valor_unitario, 5, ',', '.');
        }
        return $listaFornecedor;
    }

    # Método responsável em montar um array com as informações do itens da ata
    public static function getItemAta(int $idAta)
    {
        $itens = ArpItem::getItemPorAta($idAta);

        $listaItem = array();

        foreach ($itens as $key => $itemSelecionado) {
            $listaItem['CNPJ'][$key] = $itemSelecionado->cpf_cnpj_idgener;
            $listaItem['Fornecedor_(Classificação)'][$key] =
                $itemSelecionado->nomefornecedor . " ({$itemSelecionado->classificacao})";
            $listaItem['Número'][$key] = $itemSelecionado->numero;
            $listaItem['Item'][$key] = $itemSelecionado->descricaodetalhada;

            $listaItem['Quantidade_Registrada'][$key] =
                number_format($itemSelecionado->quantidade_homologada_vencedor, 5, ',', '.');

            $listaItem['Valor_unitário'][$key] = number_format($itemSelecionado->valor_unitario, 4, ',', '.');
            $listaItem['Valor_total'][$key] = number_format($itemSelecionado->valor_negociado, 4, ',', '.');

            # Se existir desconto no item, então exibe o valor com o maior desconto
            if ($itemSelecionado->percentual_maior_desconto > 0) {
                $listaItem['Valor_unitário'][$key] =
                    number_format($itemSelecionado->valor_unitario_desconto, 4, ',', '.');
                $listaItem['Valor_total'][$key] =
                    number_format($itemSelecionado->valor_negociado_desconto, 4, ',', '.');
            }

            // $valorTotal = $itemSelecionado->valor_negociado;
            // $itemSelecionado->valor_unitario * $itemSelecionado->maximo_adesao;

            # Alterado para que pegue o dobro da quantidade homologada fornecedor
            $listaItem['Qtd._limte_adesão'][$key] = (int)$itemSelecionado->maximo_adesao;
            $listaItem['Qtd._limte_adesão_informada_na_compra'][$key] =
                (int)$itemSelecionado->maximo_adesao_informado_compra;
            $listaItem['Aceita_adesão'][$key] = $itemSelecionado->permite_carona ? 'Sim' : 'Não';
        }

        return $listaItem;
    }

    # Método responsável em traduzir o status da Ata
    public function getStatus()
    {
        switch ($this->rascunho) {
            case true:
                return 'Em elaboração';
                break;
            case false:
                return 'Ativa';
                break;
        }
    }

    # Método responsável em exibir os botões de ações para o usuário conforme o status da ata
    public function getViewButtonArp($crud)
    {
        if ($this->rascunho) {
            $crud->allowAccess('update');
            $crud->allowAccess('delete');

            return null;
        }

        $crud->denyAccess('update');
        $crud->denyAccess('delete');

        if (empty($this->getLinkPNCP())) {
            return null;
        }


        if ($this->tipo->descricao == 'Cancelada') {
            return null;
        }
    }

    public function getViewButtonRetificar($crud)
    {
        # Se a unidade logada não for gerenciadora, não exibe este ícone
        if ($this->unidade_origem_id != session('user_ug_id')) {
            return null;
        }

        if (!in_array($this->tipo->descricao, ['Ata de Registro de Preços', 'Cancelada'])) {
            return null;
        }

        $botaoRetificar = '<a class="btn btn-sm btn-link"
         href="' . url('arp/retificar/' . $this->id) . '"
                           >
                            <i class="fas fa-redo"></i></a>
                            <div class="br-tooltip text-justify" role="tooltip" info="info" place="top" >
                            <span class="subtext">Retificar</span></div>';

        return $botaoRetificar;
    }

    public function getViewButtoRelatorioAta($crud)
    {
        if ($this->tipo->descricao == 'Cancelada') {
            return null;
        }

        if ($this->tipo->descricao == 'Em elaboração') {
            return null;
        }

        if ($this->arpPdf && $this->arpPdf->gerado &&
            Storage::disk('local_root')->exists($this->arpPdf->caminho_pdf) &&
            $this->arpPdf->created_at->diffInHours() < 24) {
             $botaoRelatorio = '<a  class="btn btn-sm btn-link"  
            id="download-pdf-'.$this->id.'"
            href="'. route(
                'visualizar.arp.pdf',
                ['id' => $this->id, 'tipo' => 'download']
            ) . '" 
            title="Download PDF">
            <i class="fas fa-download"></i></a>';

            $botaoRelatorio .= '<a  class="btn btn-sm btn-link"  
            data-arp-id="'.$this->id.'"
           href="'. route(
                'visualizar.arp.pdf',
                ['id' => $this->id, 'tipo' => 'visualizar']
            ) . '" 
            title="Visualizar PDF" target="_blank">
            <i class="fa fa-book-open"></i></a>';


            return $botaoRelatorio;
        }


            $botaoRelatorio = '
            <a  href="#" class="gerar-pdf pdf-spinner btn btn-sm btn-link"  
            data-arp-id="'.$this->id.'"
            data-pdf-url="'. route(
                'visualizar.arp.pdf',
                ['id' => $this->id, 'tipo' => 'visualizar']
            ) . '" 
            >
            <i class="fas fa-file-pdf"></i></a>
            <div class="br-tooltip text-justify" role="tooltip" info="info" place="top" >
            <span class="subtext">Gerar PDF</span></div>';

        return $botaoRelatorio;
    }






    /**
     * Método responsável em exibir o status para o usuário na listagem de atas
     */
    public function getStatusArp()
    {
        # Se a ata estiver em rascunho, exibir o status de Em Elaboração
        if ($this->rascunho) {
            return 'Em Elaboração';
        }

        # Se a ata estiver em concluída, exibir o status de Ativa
        if ($this->tipo->descricao == 'Ata de Registro de Preços') {
            return 'Ativa';
        }

        return $this->tipo->descricao;
    }

    public function getViewButtonAlterar($crud)
    {
        # Se a unidade logada não for gerenciadora, não exibe este ícone
        if ($this->unidade_origem_id != session('user_ug_id')) {
            return null;
        }

        if (!in_array($this->tipo->descricao, ['Ata de Registro de Preços', 'Cancelada'])) {
            return null;
        }

        $botaoAlterar = '<a class="btn btn-sm btn-link" href="' . url('arp/alteracaoarp/' . $this->id) .
            '" ><i class="fas fa-file-signature"></i></a>
            <div class="br-tooltip text-justify" role="tooltip" info="info" place="top" >
            <span class="subtext">Alterar</span></div>';

//        if ($this->rascunho) {
//            $crud->allowAccess('update');
//            $crud->allowAccess('delete');
//
//            return null;
//        }
//
//        $crud->denyAccess('update');
//        $crud->denyAccess('delete');


        return $botaoAlterar;
    }

    public function getViewButtonRelatarExecucaoDeAta($crud)
    {
        if ($this->tipo->descricao == 'Cancelada') {
            return null;
        }

        if ($this->tipo->descricao == 'Em elaboração') {
            return null;
        }

        $botaoAlterar = '<a class="btn btn-sm btn-link" id="gerar-pdf" 
        href="' . url('arp/execucao/' . $this->id . '/create') .
            '" >
            <img src="' . asset('img/relatar_execucao.svg') . '">
            </a>
            <div class="br-tooltip text-justify" role="tooltip" info="info" place="top" >
            <span class="subtext">Relatar Execução</span></div>';

        return $botaoAlterar;
    }

    public function getExibirVigenciaAtaCompleta()
    {
        $valorInicialFormatado = $this->formatDatePtBR($this->vigencia_inicial);
        $valorFinalFormatado = $this->formatDatePtBR($this->vigencia_final);
        return "{$valorInicialFormatado} - {$valorFinalFormatado}";
    }

    public function getValorTotalFormatado()
    {
        return $this->formatValuePtBR($this->valor_total);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function compras()
    {
        return $this->belongsTo(Compras::class, 'compra_id');
    }

    public function arp()
    {
        return $this->belongsTo(Arp::class, 'arp_id');
    }

    public function unidades()
    {
        return $this->belongsTo(Unidade::class, 'unidade_origem_id');
    }

    public function unidadeCompraOrigem()
    {
        return $this->belongsTo(Unidade::class, "unidade_origem_compra_id");
    }

    public function gestor()
    {
        return $this->hasMany(ArpGestor::class, 'arp_id');
    }

    public function autoridade_signataria()
    {
        return $this->hasMany(ArpAutoridadeSignataria::class, 'arp_id');
    }

    public function arpPdf()
    {
        return $this->hasOne(ArpPdf::class, 'arp_id');
    }


    public function tipo()
    {
        return $this->belongsTo(CodigoItem::class, 'tipo_id');
    }

    public function item()
    {
        return $this->hasMany(ArpItem::class, 'arp_id');
    }

    public function arquivos(): HasMany
    {
        return $this->hasMany(ArpArquivo::class);
    }

    public function enviaDadosPncp()
    {
        return $this->morphOne(EnviaDadosPncp::class, 'pncpable');
    }

    public function arpHistorico()
    {
        return $this->hasMany(ArpHistorico::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeVigenciaInicial(Builder $query, string $date): Builder
    {
        $date = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d') . ' 23:59:59';
        return $query->where('vigencia_inicial', '<=', $date);
    }

    public function scopeVigenciaFinal(Builder $query, string $date): Builder
    {
        return $query->where('vigencia_final', '>=', $date);
    }

    public function scopeAnoCompra(Builder $query, string $ano_compra): Builder
    {
        return $query->with('compras')->where('numero_ano', 'like', '%' . $ano_compra . '%');
    }

    public function scopeNumeroCompra(Builder $query, string $numero_compra): Builder
    {
        return $query->with('compras')->where('numero_ano', 'like', '%' . $numero_compra . '%');
    }

    public function scopeAnoAta(Builder $query, string $ano_ata): Builder
    {
        return $query->with('arp')
            ->where("arp.ano", "like", "%" . $ano_ata . "%");
    }

    public function scopeNumeroAta(Builder $query, string $numero_ata): Builder
    {
        return $query->with('arp')
            ->where("arp.numero", "like", "%" . $numero_ata . "%");
    }

    public function scopeDescricaoItem(Builder $query, string $descricao_item): Builder
    {
        return $query->with('compra_items')
            ->where("compra_items.descricaodetalhada", "ilike", "%" . $descricao_item . "%");
    }

    public function getAtaArpAdesaoAttribute()
    {
        return "{$this->numero}/{$this->ano}";
    }

    /**
     * Método responsável em exibir o botão para o usuário ir para a ata
     * no PNCP
     */
    public function getLinkPNCP()
    {
        # Recupera os dados da ata na tabela responsável pelo envio para o PNCP
        $enviaDadosPNCP = EnviaDadosPncp::where('pncpable_id', $this->id)
            ->where('pncpable_type', self::class)
            ->first();

        # Se não existir registro, bloqueia o botão
        if (empty($enviaDadosPNCP)) {
            return null;
        }
        // dd($enviaDadosPNCP, $enviaDadosPNCP->link_pncp);
        // $linkPncp = EnviaDadosPncpHistorico::select('link_pncp')
        //     ->where('pncpable_id', $this->id)
        //     ->where('pncpable_type', self::class)
        //     ->where('log_name', 'inserir_ata')
        //     ->whereIn('status_code', [200, 201])
        //     ->first();

        // $sequencialPNCP = EnviaDadosPncpHistorico::select('sequencialPNCP')
        //     ->where('pncpable_id', $this->id)
        //     ->where('pncpable_type', self::class)
        //     ->where('log_name', 'consultar_sequencialPNCP')
        //     ->whereIn('status_code', [200, 201])
        //     ->first();

        # Se não existir o link ou o sequencial, bloqueia o botão
        if (is_null($enviaDadosPNCP->link_pncp) || is_null($enviaDadosPNCP->sequencialPNCP)) {
            return null;
        }

        $host = explode("/", $enviaDadosPNCP->link_pncp);
        $arr = explode("-", str_replace('/', '-', $enviaDadosPNCP->sequencialPNCP));

        # Recuperar o ano da compra para montar o link
        $anoCompra = explode("/", $this->compras->numero_ano)[1];

        return $host[0] .
            '//' .
            $host[2] .
            '/app/atas/' .
            $arr[0] . // cnpj
            '/' .
            $anoCompra . // ano
            '/' .
            intval($arr[2]) . // sequencial da compra
            '/' .
            intval($arr[4]); // id da ata
    }

    /**
     * Retorna a situação da ata com o texto personalizado
     *
     * @return void
     */
    public function getSituacao()
    {
        return $this->getSituacaoPersonalizada(false);
    }

    /**
     * Trata a exibição da mensagem de situação da ata e a formatação conforme parâmetro
     *
     * @param bool $personalizado Se for true, retorna a situação formatada, se for false, apenas a descrição tratada
     * @return string Objeto HTML formatado
     */
    public function getSituacaoPersonalizada(bool $personalizado = true)
    {
        $cor = '';
        $paddingT = '7px';
        $paddingB = '7px';
        $corTile = '';
        $descricao = '';
        switch ($this->tipo->descricao) {
            case 'Carregando Compra':
                $descricao = 'Carregando compra';
                $cor = '#ffcd07';
                $corTile = '#fff';
                break;
            case 'Publicando PNCP':
            case 'Enviar PNCP':
                $descricao = 'Publicando';
                $cor = '#53A338';
                $corTile = '#fff';
                break;
            case 'Ata de Registro de Preços':
                $descricao = 'Ativa';
                $cor = '#168821';
                $corTile = '#fff';
                break;
            case 'Em elaboração':
                $descricao = 'Em elaboração';
                $cor = '#a3a3a3';
                $corTile = '#fff';
                break;
            case 'Erro Publicar':
                $descricao = 'Erro ao publicar';
                $cor = '#e52207';
                $corTile = '#fff';
                break;
            case 'Cancelada':
                $descricao = 'Cancelada';
                $cor = '#8b0a03';
                $corTile = '#fff';
                break;
            default:
                $cor = '#000';
                $corTile = '#fff';
                break;
        }

        if ($personalizado) {
            // Retorna a descrição com a cor e borda definidas
            return "<span class='br-tag small' " .
                "style='background: $cor; " .
                "font-weight: bold;" .
                "width: 168px; " .
                "justify-content: center; " .
                "border-radius: 50px; " .
                "padding-top: $paddingT; " .
                "padding-bottom: $paddingB; " .
                "color: $corTile'>" .
                $descricao .
                "</span>";
        }

        return $descricao;
    }
    
    public function getLinkCompraPncp()
    {
        if (!empty($this->enviaDadosPncp->sequencialPNCP)) {
            $arraySequencialPncp = explode('/', $this->enviaDadosPncp->sequencialPNCP);
            $arrayDadosSequencial = explode('-', $arraySequencialPncp[0]);
            $anoSequencial = explode('-', $arraySequencialPncp[1])[0];
            $idPncp = (int) $arrayDadosSequencial[2];
            // Usa o operador ternário para definir $host e garantir que tenha pelo menos 3 partes
            $host = !empty($this->enviaDadosPncp->link_pncp)
                ? explode("/", $this->enviaDadosPncp->link_pncp)
                : [];

            // Construa a URL ou defina um valor vazio caso $host não seja válido
            $urlCompleta = (count($host) >= 3)
                ? "{$host[0]}//{$host[2]}/app/editais/{$arrayDadosSequencial[0]}/{$anoSequencial}/"
                . "{$arrayDadosSequencial[2]}"
                : '';

            return $urlCompleta;
        }
        
        return '';
    }
    
    public function getSituacaoAtaAtiva()
    {
        $dataFimItem = $this->vigencia_final;
        $dataHoje = Carbon::now()->toDateString();
        
        if (Carbon::parse($dataHoje)->greaterThan(Carbon::parse($dataFimItem)->toDateString())) {
            return 'Não vigente';
        }
        
        if ($this->getSituacao() != 'Ativa') {
            return 'Não vigente';
        }
        
        return 'Vigente';
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    /**
     * Método responsável em exibir o valor selecionado usando o select2
     */
    public function getNumeroAnoAtaRemanejamentoAttribute()
    {
        $uasgCompleta = " ({$this->codigo} - {$this->nomeresumido})";

        # Se não existir o código, então está exibindo na tela de rascunho
        if (empty($this->codigo)) {
            $uasgCompleta = " ({$this->getUnidadeOrigem()})";
        }

        return $this->getNumeroAno() . $uasgCompleta;
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
