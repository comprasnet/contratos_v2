<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CodigoItem extends Model
{
    use CrudTrait;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'codigoitens';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['codigo_id', 'descres', 'descricao'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function codigo()
    {
        return $this->belongsTo(Codigo::class, 'codigo_id');
    }

    public function contratohistoricos()
    {
        return $this->belongsToMany(
            'App\Models\Contratohistorico',
            'contratohistoricoqualificacao',
            'tipo_id',
            'contratohistorico_id'
        );
    }

    public function modelosDeDocumento()
    {
        return $this->hasMany(ModeloDocumento::class, 'codigoitens_id');
    }

    public function statusAlteracaoArp()
    {
        return $this->belongsTo(ArpHistorico::class, 'tipo_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    /**
     * Método responsável em exibir o valor selecionado usando o select2
     */
    public function getModalidadeCompraAttribute()
    {
        return "{$this->descres} - {$this->descricao}";
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
