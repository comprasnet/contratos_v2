<?php

namespace App\Exceptions;

use App\Http\Traits\CustomizeErro500Trait;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Throwable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class Handler extends ExceptionHandler
{
    use CustomizeErro500Trait;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }
    
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        $debug = env('APP_DEBUG');

        if ($debug === true) {
            return parent::render($request, $exception);
        }

        $errorsCode = [400, 401, 403, 404, 405, 408, 429, 503];
        if ($exception instanceof Exception) {
            $statusCode = method_exists($exception, 'getStatusCode')
                ? $exception->getStatusCode()
                : $exception->getCode();

            if (in_array($statusCode, $errorsCode)
                || $exception instanceof ValidationException
                || $exception instanceof AuthenticationException //se não estiver autenticado
                ) {
                return parent::render($request, $exception);
            }
            return $this->customiseError500($exception);
        }
    }

    private function customiseError500(Exception $exception)
    {
        $errorCode = $this->errorCode();

        $errorTitleMsg = 'Um erro inesperado aconteceu.';
        $errorMsg = $this->mensagemUsuario($errorCode);

        $this->inserirLogCustomize500($errorCode, $exception);

        return response()->view('errors.500', compact('errorMsg', 'errorTitleMsg'), 500);
    }
}
