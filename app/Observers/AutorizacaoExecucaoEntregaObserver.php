<?php

namespace App\Observers;

use App\Models\AutorizacaoExecucaoEntrega;
use App\Notifications\NotificationFornecedor;
use Illuminate\Support\Facades\Notification;

class AutorizacaoExecucaoEntregaObserver
{
    public $afterCommit = true;
    
    public function created(AutorizacaoExecucaoEntrega $entrega)
    {
        $this->notificarPreposto($entrega);
    }
    
    public function updated(AutorizacaoExecucaoEntrega $entrega)
    {
        $this->notificarPreposto($entrega);
    }
    
    private function notificarPreposto(AutorizacaoExecucaoEntrega $entrega)
    {
        if ($entrega->rascunho) {
            return;
        }
        
        if ($entrega->wasChanged(['situacao_id'])) {
            $entrega->autorizacao->contrato->prepostos()->each(function ($preposto) use ($entrega) {
                if (!empty($preposto->userCpf)) {
                    Notification::send($preposto->userCpf, new NotificationFornecedor($entrega));
                }
            });
        }
    }
}
