<?php

namespace App\Observers;

use App\Models\Arp;
use App\Models\ArpArquivo;
use App\Models\ArpArquivoTipo;
use App\Models\EnviaDadosPncpHistorico;
use App\Services\Pncp\Arp\ConsultarArquivoArpPncpService;
use App\Services\Pncp\Arp\InserirArquivoArpPncpService;
use App\Services\Pncp\Arp\InsertArpPncpService;
use App\Services\Pncp\Arp\RemoverArquivoArpPncpService;
use App\Services\Pncp\PncpManagerService;
use App\Services\Pncp\Responses\PncpDeleteResponseService;
use App\Services\Pncp\Responses\PncpPostResponseService;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\Pncp\CommonPncpTrait;
use Illuminate\Support\Facades\Log;

class ArpArquivoObserver
{
    use BuscaCodigoItens, CommonPncpTrait;

    /**
     * @var PncpManagerService
     */
    private $pncpManagerService;

    private $tipoArquivo;
    
    public $afterCommit = true;

    public function __construct(PncpManagerService $pncpManagerService)
    {
        $this->pncpManagerService = $pncpManagerService;
    }

    /**
     * Handle the ArpArquivo "created" event.
     *
     * @param \App\Models\ArpArquivo $arpArquivo
     * @return bool|void
     */
    public function created(ArpArquivo $arpArquivo)
    {
   
        # Se for rascunho, não envia para o PNCP
        if (!$arpArquivo->arp->rascunho) {
            $this->checkIfAtaAlreadyExistsInPncp($arpArquivo->arp);
             
            # Se o tipo do arquivo for o habilitado para o envio e a ação não for rascunho
            # então envia o arquivo para o pncp
            if ($this->tipoArquivoValidoPncp($arpArquivo->tipo->nome) && $arpArquivo->restrito == false) {
                $this->pncpManagerService->managePncp(
                    new InserirArquivoArpPncpService(),
                    new PncpPostResponseService(),
                    $arpArquivo
                );
            }
        }
    }

    /**
     * Handle the ArpArquivo "updated" event.
     * @param \App\Models\ArpArquivo $arpArquivo
     * @return bool|void
     */
    public function updated(ArpArquivo $arpArquivo)
    {
        if (!$arpArquivo->arp->rascunho) {
            if ($this->tipoArquivoValidoPncp($arpArquivo->tipo->nome)) {
                #caso o campo restrito tenha mudado
                if ($arpArquivo->wasChanged('restrito')) {
                    #caso o campo restrito tenha mudado para publico entao envia pra o pncp
                    if ($arpArquivo->restrito == false) {
                        $this->pncpManagerService->managePncp(
                            new InserirArquivoArpPncpService(),
                            new PncpPostResponseService(),
                            $arpArquivo
                        );
                    }

                    #caso o campo restrito tenha mudado para restrito entao remove do pncp
                    if ($arpArquivo->restrito) {
                        #caso o isntrumento exista no pncp entao remove
                        if ($this->checkIfInstrumentoAlreadyExistsInPncp($arpArquivo)) {
                            $this->pncpManagerService->managePncp(
                                new RemoverArquivoArpPncpService(),
                                new PncpDeleteResponseService(),
                                $arpArquivo
                            );
                        }
                    }
                }
            }
        }
    }

    /**
     * Handle the ArpArquivo "deleted" event.
     *
     * @param  \App\Models\ArpArquivo  $arpArquivo
     * @return bool|void
     */
    public function deleted(ArpArquivo $arpArquivo)
    {
        if (!$arpArquivo->arp->rascunho) {
            #só envia os arquivos do tipo Arp e se não for restrito
            if ($this->tipoArquivoValidoPncp($arpArquivo->tipo->nome) && $arpArquivo->restrito == false) {
                try {
                    $arquivoSalvoPncp = (new ConsultarArquivoArpPncpService())->sendToPncp($arpArquivo);

                    if (!in_array($arquivoSalvoPncp->getStatusCode(), [200, 201])) {
                        return null;
                    }
                } catch (\Exception $exception) {
                    return null;
                }

                $this->pncpManagerService->managePncp(
                    new RemoverArquivoArpPncpService(),
                    new PncpDeleteResponseService(),
                    $arpArquivo
                );
            }
        }
    }

    /**
     * Handle the ArpArquivo "restored" event.
     *
     * @param  \App\Models\ArpArquivo  $arpArquivo
     * @return void
     */
    public function restored(ArpArquivo $arpArquivo)
    {
        //
    }

    /**
     * Handle the ArpArquivo "force deleted" event.
     *
     * @param  \App\Models\ArpArquivo  $arpArquivo
     * @return void
     */
    public function forceDeleted(ArpArquivo $arpArquivo)
    {
        if (!$arpArquivo->arp->rascunho) {
            #só envia os arquivos do tipo Arp e se não for restrito
            if ($this->tipoArquivoValidoPncp($arpArquivo->tipo->nome) && $arpArquivo->restrito == false) {
                $this->pncpManagerService->managePncp(
                    new RemoverArquivoArpPncpService(),
                    new PncpDeleteResponseService(),
                    $arpArquivo
                );
            }
        }
    }

    /**
     * Verifica se a Ata existe no pncp, caso não exista então envia a Ata para o pncp
     *
     * @param $arp
     * @return void
     */
    public function checkIfAtaAlreadyExistsInPncp($arp)
    {
        $alreadyExistsInPncp = EnviaDadosPncpHistorico::whereIn('status_code', PncpPostResponseService::$successCode)
            ->where('pncpable_id', $arp->id)
            ->where('pncpable_type', $arp->getMorphClass())
            ->get();

        if ($alreadyExistsInPncp->isEmpty()) {
            $this->pncpManagerService->managePncp(new InsertArpPncpService(), new PncpPostResponseService(), $arp);
        }
    }
    
    public function tipoArquivoValidoPncp(string $tipoArquivo)
    {
        $tipoArquivoEnviaPNCP = config('arp.arp.tipo_arquivo_pncp');
        
        return in_array($tipoArquivo, $tipoArquivoEnviaPNCP);
    }
}
