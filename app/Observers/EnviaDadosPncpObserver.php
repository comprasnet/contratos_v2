<?php

namespace App\Observers;

use App\Models\EnviaDadosPncp;
use App\Services\Pncp\Arp\GetNumSeqArpPncpService;
use App\Services\Pncp\PncpManagerService;
use App\Services\Pncp\Responses\PncpGetNumSeqResponseService;

class EnviaDadosPncpObserver
{

    /**
     * Handle the EnviaDadosPncp "created" event.
     *
     * @param  \App\Models\EnviaDadosPncp  $enviaDadosPncp
     * @return void
     */
    public function created(EnviaDadosPncp $enviaDadosPncp)
    {

    }

    /**
     * Handle the EnviaDadosPncp "updated" event.
     *
     * @param  \App\Models\EnviaDadosPncp  $enviaDadosPncp
     * @return void
     */
    public function updated(EnviaDadosPncp $enviaDadosPncp)
    {

    }

    /**
     * Handle the EnviaDadosPncp "deleted" event.
     *
     * @param  \App\Models\EnviaDadosPncp  $enviaDadosPncp
     * @return void
     */
    public function deleted(EnviaDadosPncp $enviaDadosPncp)
    {
        //
    }

    /**
     * Handle the EnviaDadosPncp "restored" event.
     *
     * @param  \App\Models\EnviaDadosPncp  $enviaDadosPncp
     * @return void
     */
    public function restored(EnviaDadosPncp $enviaDadosPncp)
    {
        //
    }

    /**
     * Handle the EnviaDadosPncp "force deleted" event.
     *
     * @param  \App\Models\EnviaDadosPncp  $enviaDadosPncp
     * @return void
     */
    public function forceDeleted(EnviaDadosPncp $enviaDadosPncp)
    {
        //
    }
}
