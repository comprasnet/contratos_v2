<?php

namespace App\Observers;

use App\Models\AutorizacaoExecucao;
use App\Notifications\NotificationFornecedor;
use Illuminate\Support\Facades\Notification;

class AutorizacaoExecucaoObserver
{
    public $afterCommit = true;
    
    public function created(AutorizacaoExecucao $autorizacaoExecucao)
    {
        $this->notificarPreposto($autorizacaoExecucao);
    }
    
    public function updated(AutorizacaoExecucao $autorizacaoExecucao)
    {
        $this->notificarPreposto($autorizacaoExecucao);
    }
    
    private function notificarPreposto(AutorizacaoExecucao $autorizacaoExecucao)
    {
        # Inclusão da variável para o controle da notificação
        $autorizacaoExecucao->notificacao_situacao_alterada = false;
        
        if ($autorizacaoExecucao->rascunho) {
            return;
        }
        
        if ($autorizacaoExecucao->wasChanged(['situacao_id']) &&
            $autorizacaoExecucao->situacao->descricao != 'Aguardando assinatura') {
            $autorizacaoExecucao->contrato->prepostos()->each(function ($preposto) use ($autorizacaoExecucao) {
                if (!empty($preposto->userCpf)) {
                    $autorizacaoExecucao->notificacao_situacao_alterada = true;
                    Notification::send($preposto->userCpf, new NotificationFornecedor($autorizacaoExecucao));
                }
            });
        }
    }
}
