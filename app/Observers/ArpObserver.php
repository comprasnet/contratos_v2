<?php

namespace App\Observers;

use App\Http\Traits\Arp\ArpAlteracaoCancelamentoTrait;
use App\Http\Traits\Arp\ArpItemTrait;
use App\Http\Traits\BuscaCodigoItens;
use App\Http\Traits\JobUserTrait;
use App\Http\Traits\LogTrait;
use App\Http\Traits\Pncp\ArpPncpTrait;
use App\Models\Arp;
use App\Models\ArpArquivo;
use App\Models\ArpArquivoTipo;
use App\Models\EnviaDadosPncpHistorico;
use App\Services\Pncp\Arp\CancelarArpPncpService;
use App\Services\Pncp\Arp\InserirArquivoArpPncpService;
use App\Services\Pncp\Arp\InsertArpPncpService;
use App\Services\Pncp\Arp\RetificarArpPncpService;
use App\Services\Pncp\PncpManagerService;
use App\Services\Pncp\Responses\PncpPostResponseService;
use App\Services\Pncp\Responses\PncpPutResponseService;
use Exception;
use Illuminate\Support\Facades\Log;

class ArpObserver
{
    use JobUserTrait;
    use BuscaCodigoItens;
    use ArpPncpTrait;
    use LogTrait;
    use ArpItemTrait;
    use ArpAlteracaoCancelamentoTrait;

    public $afterCommit = true;

    /**
     * @var PncpManagerService
     */
    private $pncpManagerService;

    public function __construct(PncpManagerService $pncpManagerService)
    {
        $this->pncpManagerService = $pncpManagerService;
    }

    /**
     * Método responsável em alterar o status para Carregar Compra
     */
    private function alterarStatusCarregandoCompra(Arp $arp, string $tipo)
    {
        $statusIdCarregandoCompra = $this->retornaIdCodigoItem(
            'Tipo de Ata de Registro de Preços',
            $tipo
        );

        # Método usado para não executar a observer
        $arp->unsetEventDispatcher();
        $arp->tipo_id = $statusIdCarregandoCompra;
        $arp->save();
    }

    /**
     * Handle the Arp "created" event.
     *
     * @param  \App\Models\Arp  $arp
     * @return void
     */
    public function created(Arp $arp)
    {
        if (!$arp->rascunho) {
            # Recupera a quantidade de JOB para a compra da Ata
            $quantidadeJob = $this->recuperarQuantidadeJobCompra($arp->compra_id);

            # Se existir JOB em execução, salva a ata com o status Carregando Compra
            if ($quantidadeJob > 0) {
                $this->alterarStatusCarregandoCompra($arp, 'Carregando Compra');
                return ;
            }

            $alreadyExistsInPncp = $this->getArpPNCP($arp);
            # Verifica se o registro existe no PNPC
            if ($alreadyExistsInPncp->isEmpty()) {
                # Try para incluir se acontecer erro na publicação do PNCP
                try {
                    # Método responsável em enviar as informações da Ata para o PNCP
                    $this->pncpManagerService->managePncp(
                        new InsertArpPncpService(),
                        new PncpPostResponseService(),
                        $arp
                    );

                    # Método responsável em enviar o arquivo para a Ata no PNCP
                    $this->publicarArquivoPNCP(
                        $arp,
                        $this->pncpManagerService,
                        new InserirArquivoArpPncpService(),
                        new PncpPostResponseService()
                    );

                    # Altera a vigência do item da compra na tabela compra_item
                    $this->inserirVigenciaItem($arp);
                    $this->alterarStatusCarregandoCompra($arp, 'Ata de Registro de Preços');
                } catch (Exception $ex) {
                    # Altera o status da ata para Erro Publicar para tentar reprocessar no JOB do Kernel
                    $this->alterarStatusCarregandoCompra($arp, 'Erro Publicar');

                    # Insere o erro no arquivo customizado
                    $this->inserirLogCustomizado('pncp_ata', 'error', $ex);
                }
            }
        }
    }

    /**
     * Handle the Arp "updated" event.
     *
     * @param  \App\Models\Arp  $arp
     * @return bool|void
     */
    public function updated(Arp $arp)
    {
        if (!$arp->rascunho) {
            # Recupera a quantidade de JOB para a compra da Ata
            $quantidadeJob = $this->recuperarQuantidadeJobCompra($arp->compra_id);

            # Se existir JOB em execução, salva a ata com o status Carregando Compra
            if ($quantidadeJob > 0) {
                $this->alterarStatusCarregandoCompra($arp, 'Carregando Compra');
                return ;
            }

            $alreadyExistsInPncp = EnviaDadosPncpHistorico::whereIn(
                'status_code',
                PncpPostResponseService::$successCode
            )
            ->where('pncpable_id', $arp->id)
            ->where('pncpable_type', $arp->getMorphClass())
            ->get();

            # Se estiver vazia, envia os dados para a criação da ata
            # se não, vai realizar a retificação
            if ($alreadyExistsInPncp->isEmpty()) {
                # Try para incluir se acontecer erro na publicação do PNCP
                try {
                    # Recupera a quantidade de JOB para a compra da Ata
                    $quantidadeJob = $this->recuperarQuantidadeJobCompra($arp->compra_id);

                    # Se existir JOB em execução, salva a ata com o status Carregando Compra
                    if ($quantidadeJob > 0) {
                        $this->alterarStatusCarregandoCompra($arp, 'Carregando Compra');
                        return ;
                    }

                # Método responsável em enviar as informações da Ata para o PNCP
                    $this->pncpManagerService->managePncp(
                        new InsertArpPncpService(),
                        new PncpPostResponseService(),
                        $arp
                    );

                    # Método responsável em enviar o arquivo para a Ata no PNCP
                    $this->publicarArquivoPNCP(
                        $arp,
                        $this->pncpManagerService,
                        new InserirArquivoArpPncpService(),
                        new PncpPostResponseService()
                    );
                    # Altera a vigência do item da compra na tabela compra_item
                    $this->inserirVigenciaItem($arp);
                    $this->alterarStatusCarregandoCompra($arp, 'Ata de Registro de Preços');
                } catch (Exception $ex) {
                    # Altera o status da ata para Erro Publicar para tentar reprocessar no JOB do Kernel
                    $this->alterarStatusCarregandoCompra($arp, 'Erro Publicar');

                    # Insere o erro no arquivo customizado
                    $this->inserirLogCustomizado('pncp_ata', 'error', $ex);
                }
            } else {
                #caso algum desses dados tenham sido alterados então retifica no pncp
                if ($arp->wasChanged(['vigencia_inicial', 'vigencia_final', 'data_assinatura', 'ano', 'numero',])) {
                    $this->pncpManagerService->managePncp(
                        new RetificarArpPncpService(),
                        new PncpPutResponseService(),
                        $arp
                    );
                }

                if ($arp->wasChanged(['tipo_id'])) {
                    $this->cancelarAtaPncp($arp);
                }
            }
        }
    }

    /**
     * Handle the Arp "deleted" event.
     *
     * @param  \App\Models\Arp  $arp
     * @return void
     */
    public function deleted(Arp $arp)
    {
        //
    }

    /**
     * Handle the Arp "restored" event.
     *
     * @param  \App\Models\Arp  $arp
     * @return void
     */
    public function restored(Arp $arp)
    {
        //
    }

    /**
     * Handle the Arp "force deleted" event.
     *
     * @param  \App\Models\Arp  $arp
     * @return void
     */
    public function forceDeleted(Arp $arp)
    {
        //
    }

    private function cancelarAtaPncp(Arp $arp)
    {
        $resultado = $this->verificarCancelamentoArp($arp->id, count($arp->item));
        if ($arp->tipo->descricao == 'Cancelada' && $resultado) {
            $ultimoCancelamento = $this->recuperarUltimoCancelamentoItens($arp->id);

            $this->pncpManagerService->managePncp(
                new CancelarArpPncpService($ultimoCancelamento->data_assinatura_alteracao_vigencia),
                new PncpPutResponseService(),
                $arp
            );
        }
    }

    private function verificarCancelamentoArp(
        int $arpId,
        int $totalItensAta
    ) {
        $itensCancelados = $this->queryBaseQuantidadeItemCanceladoPorAta($arpId);

        if ($itensCancelados === $totalItensAta) {
            return true;
        }

        return false;
    }
}
