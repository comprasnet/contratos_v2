@component('mail::message')
# Notificação de Assinatura do Termo de Recebimento Provisório

* Número do Contrato: **{{ $trp->contrato->numero }}**
* Número do TRP: **{{ $trp->numero }}**
* Situação: **{{ $trp->situacao->descricao }}**
* OS/Fs: **{{ $trp->getAutorizacoes()->pluck('numero')->implode(', ') }}**
* Valor Total: **{{ $trp->valor_total }}**


@component('mail::button', [ 'url' => $link ])
    Assinar Termo de Recebimento Provisório
@endcomponent

E-mail Gerado Automaticamente pelo Contratos.gov.br Contratos. Por favor não responda.
@endcomponent
