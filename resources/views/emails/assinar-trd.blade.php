@component('mail::message')
# Notificação de Assinatura do Termo de Recebimento Definitivo

* Número do Contrato: **{{ $trd->contrato->numero }}**
* Número do TRD: **{{ $trd->numero }}**
* Situação: **{{ $trd->situacao->descricao }}**
* OS/Fs: **{{ $trd->getAutorizacoes()->pluck('numero')->implode(', ') }}**
* Valor Total: **{{ $trd->valor_total }}**


@component('mail::button', [ 'url' => $link ])
    Assinar Termo de Recebimento Definitivo
@endcomponent

E-mail Gerado Automaticamente pelo Contratos.gov.br Contratos. Por favor não responda.
@endcomponent
