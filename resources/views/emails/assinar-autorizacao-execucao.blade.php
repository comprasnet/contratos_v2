@php
$total = 0;

foreach($fields['autorizacaoexecucaoItens']['value'] as $item) {
    $total += (float) $item->getTotalSemFormatar();
}
@endphp
@component('mail::message')
# Notificação de Assinatura da Ordem de Serviço / Fornecimento

* {{ $fields['contrato']['label'] }}: **{{ $fields['contrato']['value'] }}**
* {{ $fields['fornecedor']['label'] }}: **{{ $fields['fornecedor']['value'] }}**
* {{ $fields['processo']['label'] }}: **{{ $fields['processo']['value'] }}**
* {{ $fields['tipo_id']['label'] }}: **{{ $fields['tipo_id']['value']['descricao'] }}**
* {{ $fields['numero']['label'] }}: **{{ $fields['numero']['value'] }}**
* {{ $fields['data_assinatura']['label'] }}: **{{ $fields['data_assinatura']['value']['formated'] }}**
* {{ $fields['data_vigencia_inicio']['label'] }}: **{{ $fields['data_vigencia_inicio']['value']['formated'] }}**
* {{ $fields['data_vigencia_fim']['label'] }}: **{{ $fields['data_vigencia_fim']['value']['formated'] }}**
* {{ $fields['numero_sistema_origem']['label'] }}: **{{ $fields['numero_sistema_origem']['value'] ?? '-' }}**
* {{ $fields['unidaderequisitante_ids']['label'] }}: **{!! $fields['unidaderequisitante_ids']['value']['unidades_descricao'] !!}**
* {{ $fields['empenhos']['label'] }}: {!! $fields['empenhos']['value']['empenhos_descricao'] !!}
* Valor total: **R$ {{ number_format($total, '2', ',', '.') }}**
* {{ $fields['situacao']['label'] }}: **{{ $fields['situacao']['value'] }}**

@empty(!$fields['autorizacaoexecucaoItens']['value']->count())
## Itens:
@component('mail::table')
| {{ $fields['autorizacaoexecucaoItens']['label']['tipo_item']  }} | {{ $fields['autorizacaoexecucaoItens']['label']['numero_item_compra']  }} | {{ $fields['autorizacaoexecucaoItens']['label']['item']  }} | {{ $fields['autorizacaoexecucaoItens']['label']['especificacao']  }} | {{ $fields['autorizacaoexecucaoItens']['label']['unidade_medida_id']  }} | {{ $fields['autorizacaoexecucaoItens']['label']['quantidade']  }} | {{ $fields['autorizacaoexecucaoItens']['label']['parcela']  }} | {{ $fields['autorizacaoexecucaoItens']['label']['quantidade_total']  }} | {{ $fields['autorizacaoexecucaoItens']['label']['valor_unitario']  }} | {{ $fields['autorizacaoexecucaoItens']['label']['valor_total']  }} | Horário execução | {{ $fields['autorizacaoexecucaoItens']['label']['subcontratacao']  }} | {{ $fields['autorizacaoexecucaoItens']['label']['numero_demanda_sistema_externo']  }} |
| :---------: | :---------: | :--------- | :---------: | :--------- | :--------- | :--------- | ---------: | ---------: | ---------: | ---------: | ---------: | ---------: |
@foreach($fields['autorizacaoexecucaoItens']['value'] as $item)
| {{ $item->tipo_item }} | {{ $item->numero_item_compra }} | {{ $item->item }} | {{ $item->especificacao }} | {{ $item->unidadeMedida ? $item->unidadeMedida->nome : '' }} | {{ $item->quantidade }} | {{ $item->parcela }} | {{ $item->getQuantidadeTotal() }} | R$ {{ $item->valor_unitario }} | R$ {{ $item->getTotal() }} | <b>Início:</b> {{ $item->horario }} <br><b>Fim:</b> {{ $item->horario_fim }} |  @if($item->subcontratada) Sim @else Não @endif | {{ $item->numero_demanda_sistema_externo }} |
@endforeach
@endcomponent
@endif

## {{ $fields['informacoes_complementares']['label'] }}
{!! $fields['informacoes_complementares']['value'] !!}

@component('mail::button', [ 'url' => $link ])
    Assinar Ordem de Serviço / Fornecimento
@endcomponent

E-mail Gerado Automaticamente pelo Contratos.gov.br Contratos. Por favor não responda.
@endcomponent
