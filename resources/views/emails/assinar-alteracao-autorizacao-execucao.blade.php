@component('mail::message')
# Notificação de Assinatura de Alteração da Ordem de Serviço / Fornecimento

* {{ $fields['contrato']['label'] }}: **{{ $fields['contrato']['value'] }}**
* {{ $fields['fornecedor']['label'] }}: **{{ $fields['fornecedor']['value'] }}**
* {{ $fields['processo']['label'] }}: **{{ $fields['processo']['value'] }}**
* {{ $fields['tipo_id']['label'] }}: **{{ $fields['tipo_id']['value']['descricao'] }}**
* {{ $fields['numero']['label'] }}: **{{ $fields['numero']['value'] }}**
* {{ $fields['data_assinatura']['label'] }}: **{{ $fields['data_assinatura']['value']['formated'] }}**
* {{ $fields['data_vigencia_inicio']['label'] }}: **{{ $fields['data_vigencia_inicio']['value']['formated'] }}**
* {{ $fields['data_vigencia_fim']['label'] }}: **{{ $fields['data_vigencia_fim']['value']['formated'] }}**
* {{ $fields['numero_sistema_origem']['label'] }}: **{{ $fields['numero_sistema_origem']['value'] ?? '-' }}**
* {{ $fields['unidaderequisitante_ids']['label'] }}: **{!! $fields['unidaderequisitante_ids']['value']['unidades_descricao'] !!}**
* {{ $fields['empenhos']['label'] }}: {!! $fields['empenhos']['value']['empenhos_descricao'] !!}
--------------------------------------------
* Número da Alteração: **{{$numeroAlteracao}}**
* {{ $fields['situacao']['label'] }} da Alteração: **Aguardando Assinatura**
* Tipo da Alteração: **{{$tipoAlteracao}}**

@component('mail::button', [ 'url' => $link ])
    Assinar Alteração da Ordem de Serviço / Fornecimento
@endcomponent

E-mail Gerado Automaticamente pelo Contratos.gov.br Contratos. Por favor não responda.
@endcomponent
