@component('mail::message')
# Informações da Solicitação

Solicitação de adesão à Ata: {{ $dados->item_arp->arp->getNumeroAno() }} pendente de análise.
da UG {{ $dados->adesao->getUnidadeGerenciadora() }}

@component('mail::button', [ 'url' => $link ])
    Analisar Anuência Fornecedor
@endcomponent

E-mail Gerado Automaticamente pelo Contratos.gov.br. Por favor, não responda.
@endcomponent
