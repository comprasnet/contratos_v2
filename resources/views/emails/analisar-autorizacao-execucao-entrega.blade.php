@component('mail::message')
# {{ $title }}

* {{ $fields['contrato']['label'] }}: **{{ $fields['contrato']['value'] }}**
* {{ $fields['fornecedor']['label'] }}: **{{ $fields['fornecedor']['value'] }}**
* {{ $fields['processo']['label'] }}: **{{ $fields['processo']['value'] }}**
* {{ $fields['tipo_id']['label'] }}: **{{ $fields['tipo_id']['value']['descricao'] }}**
* {{ $fields['numero']['label'] }}: **{{ $fields['numero']['value'] }}**
* {{ $fields['data_assinatura']['label'] }}: **{{ $fields['data_assinatura']['value']['formated'] }}**
* {{ $fields['data_vigencia_inicio']['label'] }}: **{{ $fields['data_vigencia_inicio']['value']['formated'] }}**
* {{ $fields['data_vigencia_fim']['label'] }}: **{{ $fields['data_vigencia_fim']['value']['formated'] }}**
* {{ $fields['numero_sistema_origem']['label'] }}: **{{ $fields['numero_sistema_origem']['value'] ?? '-' }}**
* {{ $fields['unidaderequisitante_ids']['label'] }}: **{!! $fields['unidaderequisitante_ids']['value']['unidades_descricao'] !!}**
* {{ $fields['empenhos']['label'] }}: {!! $fields['empenhos']['value']['empenhos_descricao'] !!}
--------------------------------------------
* Número da Entrega: **{{$numeroEntrega}}**
* {{ $fields['situacao']['label'] }} da Entrega: **{{$situacaoEntrega}}**
* Previsão para o TRP: **{{$dataPrazoTrp}}**
* Previsão para o TRD: **{{$dataPrazoTrd}}**

@if (!empty($button))
    @component('mail::button', [ 'url' => $button['link']  ])
        {{ $button['texto'] }}
    @endcomponent
@endif

E-mail Gerado Automaticamente pelo Contratos.gov.br Contratos. Por favor não responda.
@endcomponent
