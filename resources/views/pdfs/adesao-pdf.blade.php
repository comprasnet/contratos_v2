<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900&amp;display=swap"/>
    <style>
        @page {
            size: A4 landscape;
            margin: 0.8cm;
        }

        .logo {
            margin-top: 95px;
            vertical-align: middle;
        }

        body {
            font-family: 'Rawline', sans-serif;
        }

        .titulo-rel {
            margin-bottom: 5px;
            line-height: 1.2;
        }


        .subtitulo {
            margin-top: 0;
            margin-bottom: 5px;
            opacity: 0.7;
            font-family: 'Rawline', sans-serif;
            font-size: 18px;
        }

        .divisoria {
            width: 100%;
            height: 2px; /* Altura da borda */
            background-color: #CCC; /* Cor da borda */
            margin-top: 3px; /* Espaçamento acima da borda */
        }

        .info-table {
            display: table;
            width: 100%;
            border-collapse: collapse;
            table-layout: auto !important;
        }

        .info-row {
            display: table-row;
        }

        .info-label,
        .info-content {
            width: 25%;
            display: table-cell;
            padding-top: 0;
            padding-bottom: 5px;
            vertical-align: middle;
        }

        .info-label {
            font-weight: bold;
            text-align: left;
        }

        .info-content {
            text-align: left;
        }

        .table {
            border-collapse: collapse;
            width: 100%;
            table-layout: auto; /* Ajusta a tabela ao espaço disponível */
            border: 1px solid #ddd;
            page-break-inside: avoid;
        }

        .table th, .table td {
            border: 1px solid #ddd;
            padding: 6px;
            text-align: left;
            word-wrap: break-word; /* Faz o texto quebrar para evitar exceder as células */
        }

        .link-pncp {
            white-space: pre-wrap !important;
        }

        .info-title {
            text-align: left;
            margin-left: 10px !important;
            padding: 6px;
            vertical-align: top;
            font-weight: 400 !important;
            font-size: 18px !important;
        }

        .titulo-ata {
            font-size: 20px;
            font-style: normal;
            line-height: normal;
            text-transform: uppercase;
            text-align: end;
            text-align: left !important;
            margin-bottom: 5px;
        }

        .avoid-break {
            page-break-inside: avoid; /* Evita quebra dentro do bloco */
        }

        table.table thead {
            background-color: #F0F0F0;
            color: #1c466b !important;
        }

        .info-table {
            margin-bottom: 0.3cm; /* Espaçamento reduzido */
        }

        /* Estilos para o rodapé */
        /*.rodape {
            text-align: center;
            margin-top: 10px; !* Reduza o espaçamento superior *!
            font-size: 12px;
            color: #555;
            background-color: #fff;
            page-break-before: avoid; !* Evita quebra antes do rodapé *!
            page-break-after: auto; !* Garante que o rodapé não quebre sozinho *!
        }*/
        .rodape {
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            text-align: center;
            font-size: 10px;
            padding: 5px 0;
            background-color: #fff;
        }

        .divisoria-rodape {
            width: 100%;
            height: 2px; /* Altura da borda */
            background-color: #CCC; /* Cor da borda */
            margin-top: 5px;
        }
        /*.rodape {
            page-break-before: always;
        }*/

        .logo-rodape {
            margin-top: 95px;
            vertical-align: middle;
        }

        /*.divisoria-rodape {
            width: 100%;
            height: 2px; !* Altura da borda *!
            background-color: #CCC; !* Cor da borda *!
            margin-top: 0px; !* Espaçamento acima da borda *!
        }*/
        .titulo-relatorio-rodape {
            margin-bottom: 5px;
            line-height: 1.2;
            font-size: 16px;
        }

        .subtitulo-rodape {
            margin-top: 0;
            margin-bottom: 5px;
            opacity: 0.7;

            font-size: 14px;
        }

        .texto-baixo-rodape {
            margin-top: 0;
           /* font-weight: 400;
            position: relative;
            padding-bottom: 20px;*/
            font-size: 10px;
        }
    </style>
</head>
<body>
<div class="avoid-break">
    <table style="margin-top:-4%;">
        <th>
        <td class="logo">
            <img src="data:image/png;base64,{{base64_encode(file_get_contents(public_path('img/logo_relatorio.png')))}}"
                 alt="logo"
                 width="130">
        </td>
        <td class="col-md-6 d-flex" style="padding-left: 27px;">
            <div>
                <h2 class="titulo-rel" style="font-weight: 400 !important;">
                    Solicitação de Adesão nº {{ $arrayDadosPdfAtas['adesao']->getNumeroSolicitacao() }}
                </h2>

                <p class="subtitulo">
                    Unidade Solicitante {{ $arrayDadosPdfAtas['adesao']->getUnidadeSolicitante() }}
                </p>
            </div>
        </td>
        </th>
    </table>
</div>
@php $paginaAtual = 1; $totalPaginas = count($arrayDadosPdfAtas['atas']); @endphp
@foreach($arrayDadosPdfAtas['atas'] as $ata)
    <div class="avoid-break">
        <div class="divisoria"></div>
        <div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.6cm;padding-bottom: 0cm;margin-bottom: 0px;">
            <h3 class="titulo-ata" style="font-weight: 500 !important;">INFORMAÇÕES DA ATA</h3>
            <div class="info-table">
                <div class="info-row">
                    <div class="info-label">Ata:</div>
                    <div class="info-label">Última Atualização:</div>
                    <div class="info-label">Link da ata no PNCP:</div>
                </div>
                <div class="info-row">
                    <div class="info-content-primeira-linha p-0">nº&nbsp;{{ $ata['cabecalhoAta']->getNumeroAno() }}</div>
                    <div class="info-contentc  p-0" style="width: 25%; display: table-cell;padding-top: 0 ;">
                        {{ $ata['cabecalhoAta']->updated_at }}
                    </div>
                    <div class="info-content">
                        <a href="{{ $ata['cabecalhoAta']->getLinkPNCP() }}" style="text-decoration: none; color: #000;">
                            {!! implode('<br>', str_split($ata['cabecalhoAta']->getLinkPNCP(), 35)) !!}
                        </a>
                    </div>
                </div>
            </div>

            <div class="info-table">
                <div class="info-row">
                    <div class="info-label">Vigência</div>
                    <div class="info-label">Órgão:</div>
                    <div class="info-label">Unidade gerenciadora:</div>
                </div>
                <div class="info-row">
                    <div class="info-content">de {{ date("d/m/Y", strtotime($ata['cabecalhoAta']->vigencia_inicial)) }}
                        a {{ date("d/m/Y", strtotime($ata['cabecalhoAta']->vigencia_final)) }}</div>
                    <div class="info-content">
                        {{ $ata['cabecalhoAta']->unidades->orgao->nome }}
                    </div>
                    <div class="info-content-intermediario p-0" style="padding-top: 0 !important; padding-bottom: 0px !important; margin-bottom: -20px !important;">
                        {{ $ata['cabecalhoAta']->getUnidadeOrigem() }}
                    </div>
                </div>
            </div>

            <div class="info-table">
                <div class="info-row">
                    <div class="info-label">Valor Contratado:</div>
                </div>
                <div class="info-row">
                    <div class="info-content">R$ {{ number_format($ata['cabecalhoAta']->getValorTotal(), 2 , ',' , '.') }}</div>
                </div>
            </div>
            <br>
            <br>
            {{--<table class="table">
                <thead>
                <tr>
                    <th>Fornecedor</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            {{
                                $ata['adesaoItem']->item_arp->item_fornecedor_compra->getFornecedor()
                            }}
                        </td>
                    </tr>
                </tbody>

            </table>--}}
        </div>
    </div>
    <table class="table avoid-break">
        <thead>
        <tr>
            <th>Nº Item</th>
            <th>Descrição</th>
            <th>Quantidade Registrada</th>
            <th>Valor Unitário</th>
            <th>Vigência</th>
            <th>Quantidade disponível para adesão</th>
            <th>Quantidade Solicitada</th>
            <th>Status Aceitação</th>
            <th>Quantidade aceita</th>
            <th>Justificativa</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $ata['adesaoItem']->getNumero() }}</td>
            <td>{{ $ata['adesaoItem']->getDescricao() }}</td>
            <td>{{ $ata['adesaoItem']->item_arp->item_fornecedor_compra->quantidade_homologada_vencedor }}</td>
            <td>{{ $ata['adesaoItem']->item_arp->item_fornecedor_compra->valor_unitario }}</td>
            <td>{!! $ata['adesaoItem']->getVigenciaAtaAnaliseAdesao() !!}</td>
            <td>{{ $ata['valorMaximoAdesaoPorItem'] }}</td>
            <td>{{ $ata['adesaoItem']->quantidade_solicitada }}</td>
            <td>{{ $ata['adesaoItem']->getStatus() }}</td>
            <td>{{ $ata['adesaoItem']->quantidade_anuencia_fornecedor }}</td>
            <td>{{ $ata['adesaoItem']->motivo_justificativa_fornecedor ?? '' }}</td>
        </tr>
        </tbody>
    </table>
    <!-- Rodapé fixo -->
    <div class="rodape">
        <hr class="divisoria-rodape">
        <p class="texto-baixo-rodape">
            Gerado em {{ date('d/m/Y H:i') }}.  Usuário: {{ backpack_user()->name }} - Página {{ $paginaAtual }} de {{ $totalPaginas }}
        </p>
    </div>
    @php $paginaAtual++; @endphp
    @if (!$loop->last)
        <div style="page-break-before: always;"></div>
    @endif
@endforeach
</body>
</html>
