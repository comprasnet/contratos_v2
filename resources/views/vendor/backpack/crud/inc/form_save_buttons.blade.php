@if(isset($saveAction['active']) && !is_null($saveAction['active']['value']))
    <div id="saveActions" class="form-group text-right">

        <input type="hidden" name="_save_action" value="{{ $saveAction['active']['value'] }}">
        @if(!empty($saveAction['options']))
            <div class="btn-group" role="group">
        @endif

        @if(isset($crud->button_guia) && $crud->button_guia)
            @include('vendor.backpack.crud.fields.button_next_previous_tab')
        @endif

        @if(!$crud->hasOperationSetting('showCancelButton') || $crud->getOperationSetting('showCancelButton') == true)
            {{-- <a href="{{ $crud->hasAccess('list') ? url($crud->route) : url()->previous() }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;{{ trans('backpack::crud.cancel') }}</a> --}}
            <button class="br-button secondary mr-3" type="button" onclick=" window.location.href= '{{ $crud->hasAccess('list') ? url($crud->route) : url()->previous()  }}'">
                <span class="la la-ban"></span> &nbsp;{{ trans('backpack::crud.cancel') }}
            </button>
        @endif

        @if(isset($crud->button_custom) && !is_null($crud->button_custom))
            @foreach ($crud->button_custom as $button)
                <button class="br-button {{ $button['button_tipo'] ?? 'primary' }} mr-3 button-custom" type="submit" id="{{ $button['button_id'] }}" onclick="{{ $button['onclick'] ?? '' }}">
                <!-- <span class="la la-save" role="presentation" aria-hidden="true"></span> -->
                    <i class="{{ $button['button_icon'] ?? 'fas fa-save' }}"></i> &nbsp;
                    <span data-value="{{ $button['button_value_action'] }}" data-valuecustom="{{ $button['button_value_action'] }}" data-namecustom="{{ $button['button_name_action'] }}">{{ $button['button_text'] }}</span>
                </button>
            @endforeach
                <input type="hidden" id="hidden_custom"/>
        @else
            <button class="br-button primary mr-3" type="submit">
                <!-- <span class="la la-save" role="presentation" aria-hidden="true"></span> -->
                <i class="fas fa-save"></i> &nbsp;
                <span data-value="{{ $saveAction['active']['value'] }}">{{ $saveAction['active']['label'] }}</span>
            </button>
        @endif

        {{-- <button type="submit" class="btn btn-success">
            <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
            <span data-value="{{ $saveAction['active']['value'] }}">{{ $saveAction['active']['label'] }}</span>
        </button>

        <div class="btn-group" role="group">
            @if(!empty($saveAction['options']))
                <button id="btnGroupDrop1" type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span><span class="sr-only">&#x25BC;</span></button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    @foreach( $saveAction['options'] as $value => $label)
                    <a class="dropdown-item" href="javascript:void(0);" data-value="{{ $value }}">{{ $label }}</a>
                    @endforeach
                </div>
            @endif
        </div> --}}

        @if(!empty($saveAction['options']))
            </div>
        @endif

    </div>
@endif

