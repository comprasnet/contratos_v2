@php
    // as it is possible that we can be redirected with persistent table we save the alerts in a variable
    // and flush them from session, so we will get them later from localStorage.
    $backpack_alerts = \Alert::getMessages();
    \Alert::flush();
@endphp

<style>

    .page {
        align-items: center;
        border: 5px solid transparent;
        border-radius: 100em;
        color: var(--interactive);
        display: inline-flex;
        justify-content: center;
        margin: 0 calc(var(--pagination-margin) * 0.5);
        min-height: var(--pagination-size);
        min-width: var(--pagination-size);
        padding: 0 var(--spacing-scale-base);
        white-space: nowrap;
    }

    .page.active {
        background: var(--active) !important;
        background-color: var(--active) !important;
        color: var(--color-dark) !important;
        font-weight: var(--font-weight-semi-bold);
    }

</style>

{{-- DATA TABLES SCRIPT --}}
<script type="text/javascript" src="{{ asset('packages/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('packages/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('packages/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('packages/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('packages/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('packages/datatables.net-fixedheader-bs4/js/fixedHeader.bootstrap4.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('packages/datatables.net/js/ellipses.js') }}"></script>

<script>
    // here we will check if the cached dataTables paginator length is conformable with current paginator settings.
    // datatables caches the ajax responses with pageLength in LocalStorage so when changing this
    // settings in controller users get unexpected results. To avoid that we will reset
    // the table cache when both lengths don't match.
    let $dtCachedInfo = JSON.parse(localStorage.getItem('DataTables_crudTable_/{{$crud->getRoute()}}'))
        ? JSON.parse(localStorage.getItem('DataTables_crudTable_/{{$crud->getRoute()}}')) : [];
    var $dtDefaultPageLength = {{ $crud->getDefaultPageLength() }};
    let $dtStoredPageLength = localStorage.getItem('DataTables_crudTable_/{{$crud->getRoute()}}_pageLength');

    if (!$dtStoredPageLength && $dtCachedInfo.length !== 0 && $dtCachedInfo.length !== $dtDefaultPageLength) {
        localStorage.removeItem('DataTables_crudTable_/{{$crud->getRoute()}}');
    }
    let searchTimeout;

    // in this page we allways pass the alerts to localStorage because we can be redirected with
    // persistent table, and this way we guarantee non-duplicate alerts.
    $oldAlerts = JSON.parse(localStorage.getItem('backpack_alerts'))
        ? JSON.parse(localStorage.getItem('backpack_alerts')) : {};

    $newAlerts = @json($backpack_alerts);

    Object.entries($newAlerts).forEach(function (type) {
        if (typeof $oldAlerts[type[0]] !== 'undefined') {
            type[1].forEach(function (msg) {
                $oldAlerts[type[0]].push(msg);
            });
        } else {
            $oldAlerts[type[0]] = type[1];
        }
    });

    // always store the alerts in localStorage for this page
    localStorage.setItem('backpack_alerts', JSON.stringify($oldAlerts));

    $(document).ready(function () {

        var token = $('meta[name="csrf-token"]').attr('content');
        let currentLocation = window.location.pathname;
        @foreach($idTable as $key => $id)
        $('#crudTable-{{  Str::slug($id)  }}').DataTable({
            "processing": true,
            "serverSide": true,
            {{--"ajax": {--}}
            {{--    "url": "{{ url($routeTable[$key]) }}",--}}
            {{--    "type": "POST"--}}
            {{--},--}}
            ajax: function(data, callback, settings) {
                if(data.draw !==  1){
                     clearTimeout(searchTimeout);
                }
                searchTimeout = setTimeout(() => {
                    $.ajax({
                        "url": "{{ url($routeTable[$key]) }}",
                        "type": "POST",
                        "data": data,
                        "success": function(response) {
                            callback(response);
                        }
                    });
                }, 2000);
            },
            "searching": @json($crud->getOperationSetting('searchableTable') ?? true),
            "autoWidth": false,
            "pageLength": {{ $crud->getDefaultPageLength() }},
            "lengthMenu": @json($crud->getPageLengthMenu()),
            "sPaginationType": "ellipses",
            "width": "100%",
            dom:
                "<'row hidden'<'col-sm-6'i><'col-sm-6 d-print-none'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row mt-2 d-print-none '<'col-sm-12 col-md-4'l><'col-sm-0 col-md-4 text-center'B><'col-sm-12 col-md-4 'p>>",
            "fixedHeader": true,
            "stateSave": true,
            "language": {
                "emptyTable": "Não existe remanejamento para analisar",
                "info": "",
                "infoEmpty": "{{ trans('backpack::crud.infoEmpty') }}",
                "infoFiltered": "{{ trans('backpack::crud.infoFiltered') }}",
                "infoPostFix": "{{ trans('backpack::crud.infoPostFix') }}",
                "thousands": "{{ trans('backpack::crud.thousands') }}",
                "lengthMenu": "{{ trans('backpack::crud.lengthMenu') }}",
                "loadingRecords": "{{ trans('backpack::crud.loadingRecords') }}",
                "processing": "<img src='{{ asset('packages/backpack/base/img/spinner.svg') }}' alt='{{ trans('backpack::crud.processing') }}'>",
                "search": "_INPUT_",
                "searchPlaceholder": "{{ trans('backpack::crud.search') }}...",
                "zeroRecords": "{{ trans('backpack::crud.zeroRecords') }}",
                "paginate": {
                    "next": '<i class="fas fa-angle-right" aria-hidden="true"></i>',
                    "previous": '<i class="fas fa-angle-left" aria-hidden="true"></i>'
                },
                "aria": {
                    "sortAscending": "{{ trans('backpack::crud.aria.sortAscending') }}",
                    "sortDescending": "{{ trans('backpack::crud.aria.sortDescending') }}"
                }
            },
            "columns": [
                    @foreach ($columns[$key]['data'] as $data)

                {
                    data: '@php  echo $data;  @endphp'
                },

                    @endforeach
                {
                    "data": null,
                    "render": function (data, type, row) {
                        // Se não existir rota customizada, então pega a padrão do javascript
                        @if (empty($routeButtonEdit[$key]))
                        let url = `${currentLocation}/${data.id}`
                        @endif

                        // Se existir rota customizada, então pega a padrão do javascript
                        @if (!empty($routeButtonEdit[$key]))
                        let url = `{{ $routeButtonEdit[$key] }}/${data.id}`
                        @endif

                        // Permissões de acesso conforme configurado na controller
                        @if ($crud->hasAccess('show'))
                        let botoesShow = `<a href="${url}/show" class="btn btn-link" title="Visualizar"><i class="fas fa-eye"></i></button>`
                        @else
                        let botoesShow = ''
                        @endif

                        let permissionUpdate = '{{$crud->hasAccess('update')}}'
                        let botoesEdit = '';

                        @if (is_array($permissionEdit[$key]))
                            let teste = {!! json_encode($permissionEdit[$key]) !!}
                            // Permissões de acesso conforme configurado na controller
                            if (permissionUpdate && teste.includes(data.situacao_remanejamento) && teste[2] < data.item_faltando_aprovar) {
                                botoesEdit = `<a href="${url}/edit" class="btn btn-link" title="Analisar"><i class="fas fa-edit"></i></button>`
                            }
                            return botoesShow + botoesEdit;
                        @endif

                        @if (!is_array($permissionEdit[$key]))
                            // Permissões de acesso conforme configurado na controller
                            if (permissionUpdate && '{{$permissionEdit[$key]}}' == data.situacao_remanejamento  && teste[2] < data.item_faltando_aprovar) {
                                botoesEdit = `<a href="${url}/edit" class="btn btn-link" title="Analisar"><i class="fas fa-edit"></i></button>`
                            }
                            return botoesShow + botoesEdit;
                        @endif
                    }
                }
            ]
        });

        $("#crudTable-tabela-gerenciadora-item_filter input").removeClass('form-control-sm');
        @endforeach
    });
</script>

@include('crud::inc.details_row_logic')
