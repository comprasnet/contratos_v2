@php
    $horizontalTabs = $crud->getTabsType()=='horizontal' ? true : false;

    if ($errors->any() && array_key_exists(array_keys($errors->messages())[0], $crud->getCurrentFields()) &&
        array_key_exists('tab', $crud->getCurrentFields()[array_keys($errors->messages())[0]])) {
        $tabWithError = ($crud->getCurrentFields()[array_keys($errors->messages())[0]]['tab']);
    }
    $totalTabs = max(array_keys($crud->getTabs()));
@endphp

@if ($crud->getFieldsWithoutATab()->filter(function ($value, $key) { return $value['type'] != 'hidden'; })->count())
<div class="card">
    <div class="card-body row">
    @include('crud::inc.show_fields', ['fields' => $crud->getFieldsWithoutATab()])
    </div>
</div>
@else
    @include('crud::inc.show_fields', ['fields' => $crud->getFieldsWithoutATab()])
@endif


<div class="br-tab pt-3" data-counter="true">
    <nav class="tab-nav">
      <ul class="nav {{ $horizontalTabs ? 'nav-tabs' : 'flex-column nav-pills'}} {{ $horizontalTabs ? '' : 'col-md-3' }}">
        @foreach ($crud->getTabs() as $k => $tab)
        <li role="presentation" class="tab-item {{ isset($tabWithError) ? ($tab == $tabWithError ? 'active' : '') : ($k == 0 ? 'active' : '') }}">
            <button type="button" data-panel="tab_{{ Str::slug($tab) }}"><span class="name">{{ $tab }}</span></button>
        </li>
        @endforeach
      </ul>
    </nav>
    <div class="tab-content p-0 col-md-12">
        @foreach ($crud->getTabs() as $k => $tab)
            @php
                $idGuia = 'tab_'.Str::slug($tab);
                $ultimaGuia = $k;
            @endphp
            <div class="tab-panel {{ isset($tabWithError) ? ($tab == $tabWithError ? ' active' : '') : ($k == 0 ? ' active' : '') }}" id="{{ $idGuia }}">

                <div class="row">
                    @include('crud::inc.show_fields', ['fields' => $crud->getTabFields($tab)])
                </div>
                {{-- @include('vendor.backpack.crud.fields.button_next_previous_tab', ['k' => $k , 'totalTabs' => $totalTabs]) --}}
                {{-- <div class="row justify-content-end">
                    @if($k == 0)
                        <a class="br-button circle primary mr-3 btnNext d-flex " style="color: white" ><i class="fas fa-forward"></i></a>
                    @endif

                    @if($k > 0 && $k < $totalTabs)
                        <a class="br-button circle primary mr-3 btnPrevious d-flex " style="color: white"><i class="fas fa-fast-backward"></i></a>
                        <a class="br-button circle primary mr-3 btnNext d-flex " style="color: white"><i class="fas fa-forward"></i></a>
                    @endif

                    @if($k == $totalTabs)
                        <a class="br-button circle primary mr-3 btnPrevious d-flex " style="color: white"><i class="fas fa-fast-backward"></i></a>
                    @endif
                </div> --}}
                
            </div>
        @endforeach
    </div>
</div>
<br><br>

{{-- <div class="tab-container {{ $horizontalTabs ? '' : 'container'}} mb-2">

    <div class="nav-tabs-custom {{ $horizontalTabs ? '' : 'row'}}" id="form_tabs">
        <ul class="nav {{ $horizontalTabs ? 'nav-tabs' : 'flex-column nav-pills'}} {{ $horizontalTabs ? '' : 'col-md-3' }}" role="tablist">
            @foreach ($crud->getTabs() as $k => $tab)
                <li role="presentation" class="nav-item">
                    <a href="#tab_{{ Str::slug($tab) }}" 
                        aria-controls="tab_{{ Str::slug($tab) }}" 
                        role="tab" 
                        tab_name="{{ Str::slug($tab) }}" 
                        data-toggle="tab" 
                        class="nav-link {{ isset($tabWithError) ? ($tab == $tabWithError ? 'active' : '') : ($k == 0 ? 'active' : '') }}"
                        >{{ $tab }}</a>
                </li>
            @endforeach
        </ul>

        <div class="tab-content p-0 {{$horizontalTabs ? '' : 'col-md-9'}}">

            @foreach ($crud->getTabs() as $k => $tab)
            <div role="tabpanel" class="tab-pane {{ isset($tabWithError) ? ($tab == $tabWithError ? ' active' : '') : ($k == 0 ? ' active' : '') }}" id="tab_{{ Str::slug($tab) }}">

                <div class="row">
                @include('crud::inc.show_fields', ['fields' => $crud->getTabFields($tab)])
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div> --}}
@push('after_scripts')
<script>
// acaoUltimaGuia(0, '{{ $idGuia }}')
$(".botaoguiavoltar").hide()

$('.nav li button').click(function(){
    let data = $(this).attr("data-panel");
    let numeroGuia = $(`#${data}`).index()

    if(numeroGuia == 0) {
        $(".botaoguiavoltar").hide()
        $(".botaoguiaproximo").show()
    }

    if(numeroGuia > 0) {
        $(".botaoguiavoltar").show()
    }

    if( numeroGuia == {{ $ultimaGuia }}) {
        $(".botaoguiaproximo").hide()
    }

    acaoUltimaGuia(data, '{{ $idGuia }}')
});

 $('.btnNext').click(function(){
    $('.nav-tabs > .active').next('li').find('button').trigger('click');
    incluirToolTip( 'idTextTooltipNext')
    incluirToolTip( 'idTextTooltipPrevious', true)
});

$('.btnPrevious').click(function(){
  $('.nav-tabs > .active').prev('li').find('button').trigger('click');
  incluirToolTip( 'idTextTooltipPrevious', true)
});

//Funcao 'abstrata' para nao acontecer erro caso nao tenha acao nenhuma na ultima guia
function acaoUltimaGuia(guiaAtual, ultimaGuia) {

}

function incluirToolTip(botacaoAcao, retornar = false) {
    let proximaGuia =  $('.nav-tabs > .active').next('li').find('button').text()

    if (retornar) {
        proximaGuia = $('.nav-tabs > .active').prev('li').find('button').text()
    }

    $(`#${botacaoAcao} .subtext`).html(proximaGuia)
}

incluirToolTip('idTextTooltipNext')

</script>
@endpush

@push('crud_fields_styles')
    <style>
        .nav-tabs-custom {
            box-shadow: none;
        }
        .nav-tabs-custom > .nav-tabs.nav-stacked > li {
            margin-right: 0;
        }

        .tab-pane .form-group h1:first-child,
        .tab-pane .form-group h2:first-child,
        .tab-pane .form-group h3:first-child {
            margin-top: 0;
        }

        /*  
            when select2 is multiple and it's not on the first displayed tab the placeholder would
            not display correctly because the element was not "visible" on the page (hidden by tab)
            thus getting `0px` width. This makes sure that the placeholder element is always 100% width
            by preventing the select2 inline style (0px) from applying using !important
        */
        .select2-container, 
        .select2-container li:only-child,
        .select2-container input:placeholder-shown {
            width: 100% !important;
        }
    </style>
@endpush

