{{-- Show the errors, if any --}}
@if ($crud->groupedErrorsEnabled() && $errors->any())
    @push('after_scripts')
        <script>
            let mensagemErro = ''

            function montarMensagem(mensagem) {
                mensagemErro += `<br><span class="message-body">${mensagem}</span>`
            }

            @foreach($errors->all() as $error)
                montarMensagem(`{{ $error }}`)
            @endforeach
            exibirAlertaNoty(`error-custom`, mensagemErro, '', 10000)
        </script>
    @endpush
@endif
