@extends(backpack_view('blank'))

<link href="packages/backpack/base/css/padrao_gov/all.min.css" rel="stylesheet" />
<link href="packages/backpack/base/css/padrao_gov/core.min.css" rel="stylesheet" />
<link href="packages/backpack/base/css/padrao_gov/rawline.css" rel="stylesheet" />
<link href="packages/backpack/base/css/padrao_gov/customize.css" rel="stylesheet" />

@push('crud_fields_styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
@endpush

@push('crud_fields_scripts')
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endpush

@php
  $dadosIcone = json_encode(['icone' => 'fas fa-angle-double-left' , 'url' => $crud->hasAccess('list') ? url($crud->route) : url()->previous()]);
  $defaultBreadcrumbs = [
    trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
    $crud->entity_name_plural => url($crud->route),
    trans('backpack::crud.preview') => false,
	// 'Icone' => $dadosIcone
  ];

  // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
  $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;

$camposNaTab = [];
$todosCampos = $crud->columns();
$nomeTab = [];
$count = 0;
$camposSemTab = [] ;
foreach ($todosCampos as $key => $column) {
	if(array_key_exists('tab', $column)) {
		if (!empty($column['tab'])) {
			$idTab =  "tab_".Str::slug($column['tab']);
			$camposNaTab[$idTab][$key] = $column;
			$nomeTab[$idTab] = $column['tab'];
			$count++;

			continue;
		}
	}
	$camposSemTab[] = $column;
}
// dd($camposNaTab);
@endphp

@section('header')


@endsection


<style>
    a.nav-item.nav-link {
        padding: 12.5px 30px;
        border: 0;
        border-radius: 0px;
        background-color: #fff !important;
        color: #000;
        font-weight: Bold;
        text-decoration: none;
    }

    a:not(:disabled):hover {
        background: none !important;
    }

    
    .br-tab {
        border-bottom: 1px solid #2c5ca6 !important;

    }

    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
        color: #000 !important;
        /*background-color: #dedede !important;*/
        border-bottom: 3px solid #0c326f;
    }

    a.nav-item.nav-link {
        --focus-offset: calc(var( --spacing-scale-half) * -1);
        background-color: transparent;
        border: 0;
        border-bottom: 4px solid transparent;
        color: var(--color);
        display: inline-block;
        font-size: var(--font-size-scale-up-02);
        font-weight: var(--font-weight-medium);
        line-height: 1;
        padding: var(--tab-size) var(--tab-padding);
        text-align: center;
        white-space: nowrap;
    }
    a.nav-item.nav-link:hover {
        background-color: #dedede !important;
        border-bottom: 3px solid #0c326f;
        color: #0c326f !important;
    }

    @media screen and (max-width: 800px) {
        nav.tab-nav.nav.nav-pills.nav-justified  {
            display: grid;
        }
        .card.no-padding.no-border {
            display: inline-flex;
            width: calc(100% - -4%);
        }
        .col.mb-5 {
            width: 528px !important;
        }
       .container-fluid.animated.fadeIn {
            width: 70%;
            margin-left: -2px;
        }
    }
</style>

@section('content')
<!-- Inicio cabeçalho -->
    <div class="row">
        <section class="container-fluid d-print-none">
            <div class="container mt-5 p-5" style="box-shadow: 0 0px 9px 8px #dfdede;background: fixed;">
                <h3 class="text-end">Dados da ata de Registro de Preços</h3>
                <div class="br-tab-cabecalho" data-counter="true">
                    @php $count = 0; $showMenuButton = false; @endphp
                    @foreach ($crud->fields() as $column)
                        @if($count == 0)
                            <div class="row">
                                @endif
                                @if(isset($column['wrapperAttributes']) && isset($column['wrapperAttributes']['class']))
                                    <div class="{{$column['wrapperAttributes']['class']}}">
                                        @endif

                                        <label>{!! $column['label'] !!}:</label>
                                        <br>
                                        @if (isset($column['value']) && !empty($column['value'] && !is_array($column['value'])))
                                            @if (isset($column['span']) && !empty($column['span']))
                                                <input class="form-control">{!! $column['value'] !!}</input>
                                            @else
                                                <label class="form-control">{!! $column['value'] !!}</label>
                                            @endif
                                        @endif
                                        @if (isset($column['value']) && !empty($column['value'] && is_array($column['value'])))
                                            @include('vendor.backpack.crud.columns.table_show',['value'=>$column['value']] )
                                        @endif

                                    </div>
                                    @php $count++; @endphp
                                    @if($count == 3)
                            </div>

                            @php $count =0; @endphp
                        @endif
                    @endforeach
                </div>
            </div>
        </section>
    </div>
<!--Fim do cabeçlho -->

    <div class="row">
        <section class="container-fluid d-print-none">
            <div class="container mt-5 p-5" style="box-shadow: 0 0px 9px 8px #dfdede;background: fixed;">
                <h3 class="text-end">Item da ata</h3>
                <div class="br-tab" data-counter="true">
                    <nav class="tab-nav nav nav-pills nav-justified" role="tablist">
                            <a class="nav-item nav-link active" aria-controls="tab1" role="tab" data-toggle="tab" href="#tab1">Detalhes do item </a>
                            <a class="nav-item nav-link" href="#tab2" role="tab" data-toggle="tab">Unidades do item</a>
                            <a class="nav-item nav-link" href="#tab4" role="tab" data-toggle="tab">Empenhos</a>
                            <a class="nav-item nav-link" href="#tab3" role="tab" data-toggle="tab">Adesões</a>
                            <a class="nav-item nav-link" href="#tab5" role="tab" data-toggle="tab">Contratos</a>
                      {{--
                      <ul>
                          <li class="tab-item" title="Sobre">
                              <button  type="button" data-panel="panel-1-inverted"><span class="name">Sobre</span></button>
                          </li>
                          <li class="tab-item" title="Todos">
                              <button type="button" data-panel="panel-2-inverted"><span class="name">Todos</span></button>
                          </li>
                          <li class="tab-item active" title="Notícias">
                              <button type="button" data-panel="panel-3-inverted"><span class="name">Notícias</span></button>
                          </li>
                          <li class="tab-item" title="Serviços">
                              <button type="button" data-panel="panel-4-inverted"><span class="name">Serviços</span></button>
                          </li>
                          <li class="tab-item" title="Aplicativos">
                              <button type="button" data-panel="panel-5-inverted"><span class="name">Aplicativos</span></button>
                          </li>
                          <li class="tab-item" title="Mídias">
                              <button type="button" data-panel="panel-6-inverted"><span class="name">Mídias</span></button>
                          </li>
                      </ul>
                       --}}
                    </nav>
                </div>
                <br>
                <br>
                <div class="">
                    <div class="card no-padding no-border">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab1">
                                <div class="table-responsive">
                                    <!-- Adicione o conteúdo da primeira aba aqui -->
                                    <table class="table table-striped mb-0">
                                        <tbody>
                                        @foreach ($crud->columns() as $column)
                                            @if(isset($column['tab']) && $column['tab'] == 'tab1')
                                            <tr>
                                                <td>
                                                    <strong>{!! $column['label'] !!}:</strong>
                                                </td>
                                                <td>
                                                    @php
                                                        // create a list of paths to column blade views
                                                        // including the configured view_namespaces
                                                        $columnPaths = array_map(function($item) use ($column) {
                                                            return $item.'.'.$column['type'];
                                                        }, $crud->getViewNamespacesFor('columns'));

                                                        // but always fall back to the stock 'text' column
                                                        // if a view doesn't exist
                                                        if (!in_array('crud::columns.text', $columnPaths)) {
                                                            $columnPaths[] = 'crud::columns.text';
                                                        }
                                                    @endphp
                                                    @includeFirst($columnPaths)
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="tab2">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-0">
                                        <tbody>
                                        @foreach ($crud->columns() as $column)
                                            @if(isset($column['tab']) && $column['tab'] == 'tab2')
                                            <tr class="unidades-tabela colunas-label">
                                                <td class="unidades-numero conteudo-texto-td">
                                                 {{--  <strong>{!! $column['label'] !!}:</strong>--}}
                                                </td>
                                                <td class="unidades-numero conteudo-texto">
                                                    @php
                                                        // create a list of paths to column blade views
                                                        // including the configured view_namespaces
                                                        $columnPaths = array_map(function($item) use ($column) {
                                                            return $item.'.'.$column['type'];
                                                        }, $crud->getViewNamespacesFor('columns'));

                                                        // but always fall back to the stock 'text' column
                                                        // if a view doesn't exist
                                                        if (!in_array('crud::columns.text', $columnPaths)) {
                                                            $columnPaths[] = 'crud::columns.text';
                                                        }
                                                    @endphp
                                                    @includeFirst($columnPaths)
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>

                            </div>

                            <div role="tabpanel" class="tab-pane" id="tab3">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-0">
                                        <tbody>
                                        @foreach ($crud->columns() as $column)
                                            @if(isset($column['tab']) && $column['tab'] == 'tab3')
                                                <tr>
                                                    <td>
                                                        @php
                                                        //dd($column);
                                                        if(isset($column['value']) && $column['type'] === 'text'){
                                                            @endphp

                                                            <strong>{!! $column['label'] !!}:</strong>

                                                        @php
                                                            echo $column['value'].'<br>';

                                                        } else {

                                                            $columnPaths = array_map(function($item) use ($column) {
                                                                return $item.'.'.$column['type'];
                                                            }, $crud->getViewNamespacesFor('columns'));


                                                            if (!in_array('crud::columns.text', $columnPaths)) {
                                                                $columnPaths[] = 'crud::columns.text';
                                                            }
                                                        @endphp
                                                            @includeFirst($columnPaths)
                                                        @php
                                                        }
                                                        @endphp
                                                     </td>
                                                </tr>
                                            @endif
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                <!-- Adicione o conteúdo da segunda aba aqui -->
                            </div>
                            <div role="tabpanel" class="tab-pane" id="tab4">

                                <div class="table-responsive">

                                    <table class="table table-striped mb-0">
                                        {{--   <h5 class="text-center">Detalhamento empenhos</h5>--}}
                                      <tbody>
                                      @foreach ($crud->columns() as $column)
                                          @if(isset($column['tab']) && $column['tab'] == 'tab4')
                                              <tr>
                                                  <td>
                                                      @php
                                                          //dd($column);
                                                          if(isset($column['value']) && $column['type'] === 'text'){
                                                      @endphp

                                                      <strong>{!! $column['label'] !!}:</strong>

                                                      @php
                                                          echo $column['value'].'<br>';

                                                      } else {

                                                          $columnPaths = array_map(function($item) use ($column) {
                                                              return $item.'.'.$column['type'];
                                                          }, $crud->getViewNamespacesFor('columns'));


                                                          if (!in_array('crud::columns.text', $columnPaths)) {
                                                              $columnPaths[] = 'crud::columns.text';
                                                          }
                                                      @endphp
                                                      @includeFirst($columnPaths)
                                                      @php
                                                          }
                                                      @endphp
                                                  </td>
                                              </tr>
                                          @endif
                                      @endforeach
                                        </tbody>
                                    </table>

                                </div>
                                <!-- Adicione o conteúdo da segunda aba aqui -->
                            </div>
                            <div role="tabpanel" class="tab-pane" id="tab5">
                                <div class="table-responsive">
                                    <table class="table table-striped mb-0">
                                        <tbody>
                                        @foreach ($crud->columns() as $column)
                                            @if(isset($column['tab']) && $column['tab'] == 'tab5')
                                                <tr>
                                                    <td>
                                                       {{-- <strong>{!! $column['label'] !!}:</strong> --}}
                                                    </td>
                                                    <td>
                                                        @php
                                                            // create a list of paths to column blade views
                                                            // including the configured view_namespaces
                                                            $columnPaths = array_map(function($item) use ($column) {
                                                                return $item.'.'.$column['type'];
                                                            }, $crud->getViewNamespacesFor('columns'));

                                                            // but always fall back to the stock 'text' column
                                                            // if a view doesn't exist
                                                            if (!in_array('crud::columns.text', $columnPaths)) {
                                                                $columnPaths[] = 'crud::columns.text';
                                                            }
                                                        @endphp
                                                        @includeFirst($columnPaths)
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                <!-- Adicione o conteúdo da segunda aba aqui -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
<script>
    $(document).ready(function(){
        $('.nav-tabs a').click(function(){
            $(this).tab('show');
        })
    });
</script>