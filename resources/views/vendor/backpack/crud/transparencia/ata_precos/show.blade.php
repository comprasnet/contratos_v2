@extends(backpack_view('blank'))


@php
  $dadosIcone = json_encode(['icone' => 'fas fa-angle-double-left' , 'url' => $crud->hasAccess('list') ? url($crud->route) : url()->previous()]);
  $defaultBreadcrumbs = [
    trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
    $crud->entity_name_plural => url($crud->route),
    trans('backpack::crud.preview') => false,
	// 'Icone' => $dadosIcone
  ];

  $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp
@section('header')
	<section class="container-fluid d-print-none">
		<div class="container mt-5 p-5" style="box-shadow: 0 0px 9px 8px #dfdede;background: fixed;">
			<div class="br-tab" data-counter="true">
			@php $count = 0; $showMenuButton = false; @endphp
			@foreach ($crud->fields() as $column)
				@if($count == 0)
					<div class="row">
						@endif
						<div class="{{$column['wrapperAttributes']['class']}}">
							<label>{!! $column['label'] !!}:</label>
							<br>
							@if (isset($column['value']) && !empty($column['value'] && !is_array($column['value'])))
								@if (isset($column['span']) && !empty($column['span']))
									<input class="form-control">{!! $column['value'] !!}</input>
								@else
									<label class="form-control">{!! $column['value'] !!}</label>
								@endif
							@endif
							@if (isset($column['value']) && !empty($column['value'] && is_array($column['value'])))
								@include('vendor.backpack.crud.columns.table_show',['value'=>$column['value']] )
							@endif

						</div>
						@php $count++; @endphp
						@if($count == 3)
					</div>

					@php $count =0; @endphp
				@endif
			@endforeach
		</div>


    </section>
	<br>
	<br>
@endsection

@section('content')
	<!--<div class="container mt-5 p-5" style="box-shadow: 0 0px 9px 8px #dfdede;background: fixed;">
		<div class="tab-content">
			<section id="uasgs" class="escondido d-block">
				<div class="tab-panel">
					<p class="p-3">
						<label>{!! $column['label'] !!}:</label>
						@if (isset($column['value']) && !empty($column['value'] && is_array($column['value'])))

							@include('vendor.backpack.crud.columns.table_show',['value'=>$column['value']] )
						@endif

					</p>
				</div>
			</section>

		</div>
	</div>
	</div>



-->

@endsection

