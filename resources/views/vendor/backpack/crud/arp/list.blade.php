@extends(backpack_view('blank'))
<br>
@php
    $defaultBreadcrumbs = [
      trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
      $crud->entity_name_plural => url($crud->route),
      trans('backpack::crud.list') => false,
    ];

    // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
    $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
    $classAtual = $crud->getListContentClass();

    $quantidadeTabela = $crud->one_or_more_table['quantidade_tabela'];
    $idTable = $crud->one_or_more_table['id_table'];
    $routeTable = $crud->one_or_more_table['route_tab'];
    $routeButtonEdit = $crud->one_or_more_table['route_custom_button'] ?? [];
    $permissionEdit = $crud->one_or_more_table['permission_edit'] ?? [];
    $columns = $crud->one_or_more_table['column'];
    $tabTable = $crud->one_or_more_table['tab'] ?? null;

@endphp

@section('content')
    <!-- Default box -->
    <div class="row">
        <div class="{{ $crud->getListContentClass() }}">

            {{-- Se a tela for definida com TAB --}}
            @isset($tabTable)
                <div class="br-tab" data-counter="true">
                    <nav class="tab-nav">
                        <ul>
                            {{-- Exibir a TAB definida na controller --}}
                            @foreach ($tabTable as $key => $tab)
                                @php
                                    $isActive = '';
                                    if ($key == 0) {
                                      $isActive = 'is-active';
                                    }
                                @endphp
                                <li class="tab-item {{ $isActive }}" title="Todos">
                                    <button type="button" data-panel="{{ Str::slug($tab) }}"><span
                                                class="name">{{ $tab }}</span></button>
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                    <div class="tab-content">
                        @endisset

                        @for ($i = 0; $i < $quantidadeTabela; $i++)
                            @isset($tabTable)
                                @php
                                    $active = '';
                                    if ($i == 0) {
                                      $active = 'active';
                                    }
                                @endphp
                                <div class="tab-panel {{ $active }}" id="{{ Str::slug($tabTable[$i]) }}">
                                    @endisset
                                    {{-- Inicio da div dentro da tab --}}
                                    <div>
                                        <div class="table-responsive">
                                            <table
                                                    id="crudTable-{{  Str::slug($idTable[$i])  }}"
                                                    class="bg-white table table-striped table-hover nowrap rounded shadow-xs border-xs mt-2"
                                                    data-responsive-table="{{ (int) $crud->getOperationSetting('responsiveTable') }}"
                                                    data-has-details-row="{{ (int) $crud->getOperationSetting('detailsRow') }}"
                                                    data-has-bulk-actions="{{ (int) $crud->getOperationSetting('bulkActions') }}"
                                                    cellspacing="0"
                                                    style="width: 100%;">
                                                <thead>
                                                <tr>
                                                    {{-- Table columns --}}
                                                    @foreach ($columns[$i]['table_title'] as $tableTitle)
                                                        <th
                                                                {{-- data-orderable="{{ var_export($column['orderable'], true) }}" --}}
                                                                {{-- data-priority="{{ $column['priority'] }}" --}}
                                                                {{-- data-column-name="{{ $column['name'] }}" --}}

                                                                {{-- If it is an export field only, we are done. --}}
                                                                {{-- @if(isset($column['exportOnlyField']) && $column['exportOnlyField'] === true)
                                                                  data-visible="false"
                                                                  data-visible-in-table="false"
                                                                  data-can-be-visible-in-table="false"
                                                                  data-visible-in-modal="false"
                                                                  data-visible-in-export="true"
                                                                  data-force-export="true"
                                                                @else
                                                                  data-visible-in-table="{{var_export($column['visibleInTable'] ?? false)}}"
                                                                  data-visible="{{var_export($column['visibleInTable'] ?? true)}}"
                                                                  data-can-be-visible-in-table="true"
                                                                  data-visible-in-modal="{{var_export($column['visibleInModal'] ?? true)}}"
                                                                  @if(isset($column['visibleInExport']))
                                                                    @if($column['visibleInExport'] === false)
                                                                      data-visible-in-export="false"
                                                                      data-force-export="false"
                                                                    @else
                                                                      data-visible-in-export="true"
                                                                      data-force-export="true"
                                                                    @endif
                                                                  @else
                                                                    data-visible-in-export="true"
                                                                    data-force-export="false"
                                                                  @endif
                                                                @endif --}}
                                                        >
                                                            {!! $tableTitle !!}
                                                            {{-- Bulk checkbox --}}
                                                            {{-- @if($loop->first && $crud->getOperationSetting('bulkActions'))
                                                              {!! View::make('crud::columns.inc.bulk_actions_checkbox')->render() !!}
                                                            @endif
                                                            {!! $column['label'] !!} --}}
                                                        </th>
                                                    @endforeach
                                                    @if ( $crud->buttons()->where('stack', 'line')->count() )
                                                        <th data-orderable="false"
                                                            data-priority="{{ $crud->getActionsColumnPriority() }}"
                                                            data-visible-in-export="false"
                                                        >{{ trans('backpack::crud.actions') }}</th>
                                                    @endif

                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    {{-- Table columns --}}
                                                    @foreach ($columns[$i]['table_title'] as $tableTitle)
                                                        <th>
                                                            {!! $tableTitle !!}
                                                        </th>
                                                    @endforeach
                                                    @if ( $crud->buttons()->where('stack', 'line')->count() )
                                                        <th>{{ trans('backpack::crud.actions') }}</th>
                                                    @endif
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    @isset($tabTable)
                                </div>
                            @endisset
                        @endfor

                    </div>

                    @isset($tabTable)
                </div>
            @endisset

        </div>
        @endsection

        @section('after_styles')
            <!-- DATA TABLES -->
            <link rel="stylesheet" type="text/css"
                  href="{{ asset('packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
            <link rel="stylesheet" type="text/css"
                  href="{{ asset('packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css') }}">
            <link rel="stylesheet" type="text/css"
                  href="{{ asset('packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">

            <!-- CRUD LIST CONTENT - crud_list_styles stack -->
            @stack('crud_list_styles')
        @endsection

        @section('after_scripts')
            @include('crud::inc.arp.datatables_logic')

            <!-- CRUD LIST CONTENT - crud_list_scripts stack -->
    @stack('crud_list_scripts')
@endsection
