<div class="card">
    <div class="card-body row">

        <div class="col-md-4 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_numero_ano"
            bp-field-type="label">
            <div class="br-input">
                <label for="label_numero_ano" class="mb-2">Número/Ano</label>
                <br>
                <span>{{ $crud->arp->getNumeroAno() }}</span>
            </div>
        </div>
        <div class="col-md-4 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_unidade_gerenciadora"
            bp-field-type="label">
            <div class="br-input">
                <label for="label_unidade_gerenciadora" class="mb-2">Unidade Gerenciadora</label>
                <br>
                <span>{{ $crud->arp->getUnidadeOrigem() }}</span>
            </div>
        </div>
        <div class="col-md-4 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_numero_compra"
            bp-field-type="label">
            <div class="br-input">
                <label for="label_numero_compra" class="mb-2">Número da compra/Ano</label>
                <br>
                <span>{{ $crud->arp->getNumeroCompra() }}</span>
            </div>
        </div>
        <div class="col-md-4 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_modalidade_compra"
            bp-field-type="label">
            <div class="br-input">
                <label for="label_modalidade_compra" class="mb-2">Modalidade da compra</label>
                <br>
                <span>{{ $crud->arp->getModalidadeCompra() }}</span>
            </div>
        </div>
        <div class="col-md-4 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_data_assinatura"
            bp-field-type="label">
            <div class="br-input">
                <label for="label_data_assinatura" class="mb-2">Data da Assinatura</label>
                <br>
                <span>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $crud->arp->data_assinatura)->format('d/m/Y') }}</span>
            </div>
        </div>
        <div class="col-md-4 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_data_vigencia_inicial"
            bp-field-type="label">
            <div class="br-input">
                <label for="label_data_vigencia_inicial" class="mb-2">Vigência Inicial</label>
                <br>
                <span>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $crud->arp->vigencia_inicial)->format('d/m/Y') }}</span>
            </div>
        </div>
        <div class="col-md-4 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_data_vigencia_final"
            bp-field-type="label">
            <div class="br-input">
                <label for="label_data_vigencia_final" class="mb-2">Vigência Final</label>
                <br>
                <span>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $crud->arp->vigencia_final)->format('d/m/Y') }}</span>
            </div>
        </div>
        <div class="col-md-4 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_data_valor_total"
            bp-field-type="label">
            <div class="br-input">
                <label for="label_data_valor_total" class="mb-2">Valor Total</label>
                <br>
                <span>{{ "R$ " . number_format($crud->arp->valor_total, 2, ',', '.') }}</span>
            </div>
        </div>
        {{-- @include('crud::inc.show_fields', ['fields' => $crud->fields()]) --}}
    </div>
</div>
