@include('crud::fields.text', $field)

@push('crud_fields_scripts')
    <script type="text/javascript">
        maskCelular($('input[name={{ $field['name'] }}]'))
    </script>
@endpush
