<div class="br-scrim-util foco " id="{{ $field['idModalCustom'] }}" data-scrim="true" data-visible="true" aria-expanded="true">
    <div class="br-modal">
      <div class="br-modal-header">{{ $field['tituloModalCustom'] }}</div>
      <div class="br-modal-body">
        @include('crud::fields.table_detalhe_item')
      </div>
      <div class="br-modal-footer justify-content-center">
        <button class="br-button secondary" type="button" id="fecharModalCustom" data-dismiss="scrimexample">Retomar
        </button>
        @if (isset($field['idBotaoCustomModal']))
            <button class="br-button primary mt-3 mt-sm-0 ml-sm-3" type="button" id="{{ $field['idBotaoCustomModal'] }}">Gravar
            </button>
        @endif
      </div>
    </div>
  </div>

  @push('after_scripts')
  <script>
    $("#fecharModalCustom").click(function(){
        $( "#{{ $field['idModalCustom'] }}" ).removeClass( "active" );
    })
  </script>

  @endpush