@php
    use Illuminate\Support\Facades\Storage;
    use app\Http\Traits\Formatador;

        $field['wrapper'] = $field['wrapper'] ?? $field['wrapperAttributes'] ?? [];
        $field['wrapper']['data-init-function'] = $field['wrapper']['data-init-function'] ?? 'initUploadElementGenerico';
        $field['wrapper']['data-field-name'] = $field['wrapper']['data-field-name'] ?? $field['name'];

        $field['accept'] = $field['accept'] ?? 'application/pdf';
        $field['labelInformativo'] = $field['labelInformativo'] ?? '';
        $field['attributes'] = $field['attributes'] ?? null;
        $field['textoInformativoUsuarioArquivo'] = $field['textoInformativoUsuarioArquivo'] ?? null;

        // Se o tipo será multiple ou único
        $field['multiple'] = $field['multiple'] ?? false;

        // Por padrão o name será criado como array, devido ser repeteable
        $name = "{$field['name']}[]";

        // Se não for único, então coloca o name único
        if (!$field['multiple']) {
            $name = $field['name'];
        }

        # Variável que recebe o registro recuperado no banco
        $arquivo = null;

        # Variável responsável em exibir o campo
        $visible = 'invisible';

        // Se o texto informativo vier com valor, considera tela de rascunho e então
        //  busca as informações no banco de dados com base no ID e a classe model
        if (!empty($field['textoInformativoUsuarioArquivo'])) {
            // Recupera as informações do registro
            $arquivo = App\Models\ArquivoGenerico::where("arquivoable_id", $entry->getKey())
                            ->where("arquivoable_type", $field['textoInformativoUsuarioArquivo']['modelClass'])
                            ->first();
            $visible = 'visible';
        }

        $field['required'] = $field['required'] ?? false;
@endphp

        <!-- text input -->
@include('crud::fields.inc.wrapper_start')

{{-- Exibe o texto informativo para o usuário--}}
{{-- <div class="br-input">
    <label >{!! $field['labelInformativo'] !!}</label>
</div> --}}

{{-- Campos escondidos para formatar os campos do upload --}}
<input type="hidden" id="attributesUploadFileGenerico" data-attributes="{{ json_encode($field['attributes']) }}"/>
<input type="hidden" id="wrapperAttributesUploadFileGenerico"
       data-wrapperattributes="{{ json_encode($field['wrapperAttributes'], JSON_UNESCAPED_UNICODE) }}"/>

<input type="hidden" id="teste" class="upload-button"/>

{{-- Show the file picker on CREATE form. --}}
<span id="{{ $field['name'] }}CampoUploadFileGenerico">

    <div class="br-upload input_file_in_html" id="br-upload-{{ $field['textoInformativoUsuarioArquivo']['idUnico'] }}">
    {{-- Se o arquivo não existir, mas for na tela de edição --}}
        @if (isset($entry))
            {{-- Texto para o usuário --}}
            <label class="upload-label "
                   for="{{ $name }}">
                <span>Envio de arquivo {!! $field['textoInformativoUsuarioArquivo']['textoCampo'] !!}</span>
            </label>
            {{-- Input file --}}
            <input type="file" name="{{ $name }}"
                   id="{{ $name }}"
                   accept="{{  $field['accept'] }}"
                   class="file_input backstrap-file-input upload-input"
                   :attributes
                   onchange="incluirArquivoSelecionado(this, {{ $field['textoInformativoUsuarioArquivo']['idUnico'] }})"
                   data-textounico="{{ $field['textoInformativoUsuarioArquivo']['textoUnico'] }}"
                   data-idunico="{{ $field['textoInformativoUsuarioArquivo']['idUnico'] }}"
                   value="{{ $arquivo['url'] }}">
            {{-- Se não existir o arquivo, então colocamos o upload-list para que o arquivo
                fique em baixo da seleção --}}
            @if (empty($arquivo))
                <div class="upload-list" id="upload-list-{{ $field['textoInformativoUsuarioArquivo']['idUnico'] }}">
                </div>
            @endif
        @endif

        {{-- Se o arquivo existir --}}
        @if (!empty($arquivo))
            {{-- Exibição do nome do arquivo salvo --}}
            <div class="upload-list" id="upload-list-{{ $field['textoInformativoUsuarioArquivo']['idUnico'] }}">
                <div class="br-item d-flex" id="render-item-{{ $field['textoInformativoUsuarioArquivo']['idUnico'] }}">
                    {{-- Exibe o nome do arquivo podendo clicar o anexo --}}
                    <div class="content text-primary-default mr-auto">
                        <a target="_blank" href="{{ url("storage/{$arquivo['url']}") }}">
                            {!! $arquivo['nome'] !!}
                        </a>
                    </div>

                    {{-- Exibe a lixeira para excluir o arquivo --}}
                    <div class="support mr-n2">
                        <span class="mr-1">{{ Formatador::formatBytes(Storage::disk('public')->size($arquivo['url']))  }}</span>
                        <button class="br-button" type="button" circle=""
                                onclick="removeFile({{ $field['textoInformativoUsuarioArquivo']['idUnico'] }}, true)">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>
                </div>
            </div>

            {{-- Campo com o id único para remover, caso seja excluído --}}
            <input type="hidden" name="idUnicoHidden" id="idUnicoHidden"
                   value="{{ $field['textoInformativoUsuarioArquivo']['idUnico'] }}">
            {{-- Campo necessário para verificar se o usuário vai somente remover o arquivo --}}
            <input type="hidden" name="removerArquivoGenerico" id="removerArquivoGenerico" value="false">
            {{-- Campo com o id da tabela arquivo_generico --}}
            <input type="hidden" name="idArquivoGenerico" id="idArquivoGenerico" value="{{ $arquivo->id }}">
        @endif
        </div>

</span>

{{-- HINT --}}
@if (isset($field['hint']))
    <!-- <p class="help-block">{!! $field['hint'] !!}</p> -->
    <span class="feedback danger mt-1" role="alert"><i class="fas fa-times-circle" aria-hidden="true"></i>{!! $field['hint'] !!}</span>
@endif



@include('crud::fields.inc.wrapper_end')


@push('crud_fields_scripts')

    <!-- no scripts -->
    <script>
        // $(document).ready(function() {
        /**
         * Método responsável em montar o campo padrão para poder
         * inserir na tela do usuário
         * */
        function campoInputPadrao() {
            let pt = ''
            //  Se existir um item selecionado, então insere o padding top para afastar os campos
            if (idSelecionado.length > 0) {
                pt = 'pt-3'
            }
            // Recupera os tipos de arquivos válidos
            let accept = '{{$field['accept']}}'

            @if($field['multiple'])
            let name = "{{ $field['name'] }}[:idSelecao]"
            @endif

            @if(!$field['multiple'])
            let name = "{{ $field['name'] }}"
            @endif


            @if($field['required'])
            let required = '<label style="color: red">*</label>'
            @endif

            @if(!$field['required'])
            let required = ''
            @endif

            // Monta o input em HTML para ser incluído na tela
            let inputFile = `
        <div class="br-upload ${pt}" id="br-upload-:idSelecao">
            <label class="upload-label"
                    for="${name}">
                <span>Envio de arquivo :labelInputFile</span> ${required}
            </label>
            <input type="file" name="${name}"
                    accept="${accept}"
                    class="file_input backstrap-file-input upload-input"
                    :attributes
                    onchange="incluirArquivoSelecionado(this, :idSelecao)"
                    data-textounico=":textoUnico">
            <div class="upload-list" id ="upload-list-:idSelecao"></div>
        </div>`

            return inputFile
        }

        var idSelecionado = []

        function uploadTimeout() {
            return new Promise((resolve) => {
                // Colocar aqui um upload para o servidor e retirar o timeout
                return setTimeout(resolve, 3000)
            })
        }

        /**
         * Método responsável em recuperar as informações enviadas pela controller
         * e inserir o input file conforme a seleção do usuário
         *
         */
        function vincularRegistroArquivo(id, texto, textoUnico = '') {
            let uploadList = []
            // Se o id estiver sido selecionado, não faz nada
            if (idSelecionado.includes(id)) {
                return
            }

            // Recupera as informações para montar o campo
            let nameCampo = '{{ $field['name'] }}'
            let attributes = $("#attributesUploadFileGenerico").data('attributes')
            let wrapperAttributes = $("#wrapperAttributesUploadFileGenerico").data('wrapperattributes')

            // Inicia a variável para incluir todo os attributes passados pela Controller
            let attributesHtml = ''

            $.each(attributes, function (key, value) {
                attributesHtml += `${key}='${value}' `
            });

            let classHtml = ''
            $.each(wrapperAttributes, function (key, value) {
                classHtml += `${key}='${value}' `
            });

            // Recupera o HTML do campo
            let inputFile = campoInputPadrao()

            // Substitui os atributos enviados pela controller
            let inputFileFormatadoAttributes = inputFile.replaceAll(':attributes', attributesHtml)

            // Substitiu o id seleção pelo id do registro selecionado
            let inputFileFormatadoId = inputFileFormatadoAttributes.replaceAll(':idSelecao', id)

            let inputFileFormatadoTexto = inputFileFormatadoId.replaceAll(':labelInputFile', texto)

            if (textoUnico) {
                inputFileFormatadoTexto = inputFileFormatadoTexto.replaceAll(':textoUnico', textoUnico)
            }

            let idAreaInputFile = '{{ $field['name'] }}CampoUploadFileGenerico'

            $(`#${idAreaInputFile}`).append(inputFileFormatadoTexto);

            // Insere o id no array para que não possa incluir novamente
            idSelecionado.push(id)

            for (const brUpload of window.document.querySelectorAll('.br-upload')) {
                if (!$(`#${brUpload.id}`).hasClass("input_file_in_html")) {
                    new core.BRUpload('br-upload', brUpload, uploadTimeout)
                    $(`#${brUpload.id}`).addClass('input_file_in_html')
                }
            }
        }

        /**
         * Método responsável em incluir o arquivo dentro do form
         * */
        function incluirArquivoSelecionado(campo, idCampo) {
            // Recupera o HTML inteiro
            const fileList = $(`#upload-list-${idCampo}`)
            const file = campo.files[0]; // Obtém o arquivo selecionado
            const maxSize = 30 * 1024 * 1024; // 30MB em bytes
            if (file && file.size > maxSize) {
                const fileSizeInMB = (file.size / (1024 * 1024)).toFixed(2);
                exibirAlertaNoty('warning-custom', `O arquivo selecionado (${fileSizeInMB} MB) excede o limite de 30 MB.`);

                // Limpa o valor do input
                campo.value = '';

                // Exibe a mensagem de erro
                const fileError = document.getElementById('fileError');
                fileError.style.display = 'block';
                fileError.innerText = `O arquivo selecionado (${fileSizeInMB} MB) excede o limite de 30 MB.`;

                return; // Impede o processo de upload se o arquivo for muito grande
            }
            // Limpa o conteúdo
            fileList.empty()
            // Exibe o loading
            uploadLoading(idCampo, fileList)
            // Exibe o nome do arquivo, tamanho e a lixeira para exclusão
            renderItem(campo.files[0], fileList, idCampo)
            $(`#carregando-${idCampo}`).remove()
        }

        /**
         * Método responsável em exibir o loading ao realizar o upload do arquivo
         * */
        function uploadLoading(idCampo, fileList) {
            const loading = document.createElement('div')
            const carga = document.createElement('span')
            carga.classList.add('cargas')
            carga.innerText = 'Carregando...'
            loading.setAttribute('sm', '')
            loading.setAttribute('id', `carregando-${idCampo}`)
            loading.classList.add('my-3')
            loading.setAttribute('loading', '')
            loading.append(carga)

            fileList.append(loading)
        }

        /**
         * Método responsável em renderizar o arquivo na tela do usuário
         * */
        function renderItem(file, fileList, idCampo) {
            // Se nenhum arquivo for marcado, sai do método
            if (file == undefined) {
                return
            }

            const li = document.createElement('div')
            li.className = 'br-item d-flex'
            fileList.append(li)
            li.innerHTML = ''
            const name = document.createElement('div')
            name.className = 'name'
            li.append(name)
            fileList.append(li)
            const info = document.createElement('div')
            info.className = 'content'
            info.innerHTML = file.name
            const tooltip = document.createElement('div')
            tooltip.classList.add('br-tooltip')
            tooltip.setAttribute('role', 'tooltip')
            tooltip.setAttribute('place', 'top')
            tooltip.setAttribute('info', 'info')
            const textTooltip = document.createElement('span')
            textTooltip.classList.add('text')
            textTooltip.setAttribute('role', 'tooltip')
            textTooltip.innerHTML = file.name
            tooltip.append(textTooltip)
            li.append(info)
            li.append(name)
            li.append(tooltip)
            info.classList.add('text-primary-default', 'mr-auto')
            const del = document.createElement('div')
            del.className = 'support mr-n2'
            const btndel = document.createElement('button')
            const spanSize = document.createElement('span')
            spanSize.className = 'mr-1'
            spanSize.innerHTML = calcSize(file.size)
            del.append(spanSize)
            btndel.className = 'br-button'
            btndel.type = 'button'
            btndel.setAttribute('circle', '')
            btndel.addEventListener(
                'click',
                (event) => {
                    removeFile(idCampo)
                },
                false
            )
            const img = document.createElement('i')
            img.className = 'fa fa-trash'
            btndel.append(img)
            del.append(btndel)
            li.append(del)
            // const tooltipList = []
            // for (const brTooltip of window.document.querySelectorAll('.br-tooltip')) {
            //     tooltipList.push(new core.BRTooltip('br-tooltip', brTooltip))
            // }

            // Seta o id do arquivo selecionado para remover quando apertar no ícone da lixeira
            li.setAttribute('id', `render-item-${idCampo}`)
        }

        /**
         * Formata tamanho do arquivo
         * @private
         * @param {Number} nBytes - quantidade de bytes do arquivo
         */
        function calcSize(nBytes) {
            let sOutput = ''
            for (
                let aMultiples = ['KB', 'MB', 'GB', 'TB'],
                    nMultiple = 0,
                    nApprox = nBytes / 1024;
                nApprox > 1;
                nApprox /= 1024, nMultiple++
            ) {
                sOutput = `${nApprox.toFixed(2)} ${aMultiples[nMultiple]}`
            }
            return sOutput
        }

        /**
         * Método responsável em retirar o arquivo do form
         * */
        function removeFile(idCampo, removerArquivoGenerico = false) {
            // remove o item exibido
            $(`#render-item-${idCampo}`).remove()


            @if($field['multiple'])
            // monta o id do input file
            let id = '{{ $field['name'] }}[' + idCampo + ']'
            @endif

            @if(!$field['multiple'])
            // monta o id do input file
            let id = '{{ $field['name'] }}'
            @endif

            // inicializa o datatransfer para 'limpar' o arquivo enviado
            const fileInput = new ClipboardEvent('').clipboardData || new DataTransfer()
            // insere o datatransfer vazio para 'limpar'
            $(`input[name='${id}']`)[0].files = fileInput.files

            // Se o usuário remover o arquivo, então seta como verdadeiro
            $("#removerArquivoGenerico").val(removerArquivoGenerico)
        }
    </script>
@endpush
