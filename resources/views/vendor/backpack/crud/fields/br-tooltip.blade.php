@php
$id = $id ?? '';
@endphp
<div class="br-tooltip text-justify" role="tooltip" info="info" place="top" id="{{ $id }}"><span class="subtext">{!! $textTooltip !!}</span></div>