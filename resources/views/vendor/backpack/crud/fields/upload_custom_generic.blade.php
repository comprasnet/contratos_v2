@php
    $field['wrapper'] = $field['wrapper'] ?? $field['wrapperAttributes'] ?? [];
    $field['wrapper']['data-init-function'] = $field['wrapper']['data-init-function'] ?? 'bpFieldInitUploadElement';
    $field['wrapper']['data-field-name'] = $field['wrapper']['data-field-name'] ?? $field['name'];

    # campo obrigatório
    $field['required'] = $field['required'] ?? false;

    # extensões aceitas
    $field['accept'] = $field['accept'] ?? '.xlsx,.xls,.ppt, .pptx, .pdf';

    #model
    $connected_entity = new $field['model'];

    $arquivoCriado = false;
    $url = $field['url_arquivo'] ?? 'url';
    $nome = $field['nome_exibir'] ?? 'nome';

    # se for na tela de edição, recupera o único registro
    if (isset($entry)) {
        if (is_array($field['colum_filter'])) {
            $arquivos =  $connected_entity->where($field['colum_filter'])->first();
            $arquivoCriado = true;
        }
    }

$field['attributes']['id'] = $field['attributes']['id'] ?? 'fileInput';
@endphp

@include('crud::fields.inc.wrapper_start')

<div class="br-upload">
    <label class="upload-label" for="single-file">
        <span>{!! $field['label'] !!}</span>
        {{-- @include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => 'Para substituir, selecione um novo arquivo']) --}}
        @if($field['required'])
            @include("vendor.backpack.crud.fields.marcacao_obrigatorio",['textTooltip' => 'Este campo é obrigatório'])
        @endif
    </label>
    <input class="upload-input"
           onchange="$('.existing-file').hide()"
           name="{{ $field['name'] }}"
           value="{{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}"
           type="file"
           accept="{{ $field['accept'] }}"
           data-exists={{ isset($anexo) ? 'true' : 'false' }}
            @include('crud::fields.inc.attributes', ['default_class' => isset($field['value']) && $field['value']!=null ? 'file_input backstrap-file-input' : 'file_input backstrap-file-input'])
    />
    <div class="upload-list"></div>
    <div id="fileError" style="display: none; color: red; margin-top: 10px;"></div>

    <input type="hidden" name="{{ $field['name'] }}_clear"  />
    <input type="hidden" id="arquivoSalvo" value="{{ $arquivoCriado }}">
</div>
{{-- Foi observado que para garantir o correto funcionamento do componente de upload no Design System do governo,
é necessário que haja uma tag irmã ao componente no HTML, ou que a classe '.text-base' esteja presente em alguma parte
da página em que o componente está inserido, e como os campos do sistema são todos possuem o 'wrapper_start' e 'wrapper_end'
 a tag abaixo garante que o upload sempre terá um irmão --}}
<span class="d-none"></span>

@if (isset($arquivos))
    <div class="existing-file" style="max-width: 550px; margin-top: 10px;">
        <span id="row_arquivo_{{ $arquivos->id }}">
            <a target="_blank" href="{{ url('storage/' . ($arquivos->$url)) }}">
            {{ $arquivos->$nome }}
            </a>
            <a href="javascript: void(0)" class="btn btn-light btn-sm float-right file-clear-button" onclick="deletarArquivo('{{ $field['name'] }}', {{ $arquivos->id }})"><i class="fa fa-trash"></i></a>
            @include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => 'Remover arquivo'])
        </span>
    </div>
@endif

@include('crud::fields.inc.wrapper_end')

{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->fieldTypeNotLoaded($field))
    @php
        $crud->markFieldTypeAsLoaded($field);
    @endphp

    @push('crud_fields_styles')
        <style type="text/css">
            .existing-file {
                border: 1px solid rgba(0,40,100,.12);
                border-radius: 5px;
                padding-left: 10px;
                vertical-align: middle;
            }
            .existing-file a {
                padding-top: 5px;
                display: inline-block;
                font-size: 0.9em;
            }
            .backstrap-file {
              position: relative;
              display: inline-block;
              width: 100%;
              height: calc(1.5em + 0.75rem + 2px);
              margin-bottom: 0;
            }

            .backstrap-file-input {
              position: relative;
              z-index: 2;
              width: 100%;
              height: calc(1.5em + 0.75rem + 2px);
              margin: 0;
              opacity: 0;
            }

            .backstrap-file-input:focus ~ .backstrap-file-label {
              border-color: #acc5ea;
              box-shadow: 0 0 0 0rem rgba(70, 127, 208, 0.25);
            }

            .backstrap-file-input:disabled ~ .backstrap-file-label {
              background-color: #e4e7ea;
            }

            .backstrap-file-input:lang(en) ~ .backstrap-file-label::after {
              content: "Browse";
            }

            .backstrap-file-input ~ .backstrap-file-label[data-browse]::after {
              content: attr(data-browse);
            }

            .backstrap-file-label {
              position: absolute;
              top: 0;
              right: 0;
              left: 0;
              z-index: 1;
              height: calc(1.5em + 0.75rem + 2px);
              padding: 0.375rem 0.75rem;
              font-weight: 400;
              line-height: 1.5;
              color: #5c6873;
              background-color: #fff;
              border: 1px solid #e4e7ea;
              border-radius: 0.25rem;
              font-weight: 400!important;
            }

            .backstrap-file-label::after {
              position: absolute;
              top: 0;
              right: 0;
              bottom: 0;
              z-index: 3;
              display: block;
              height: calc(1.5em + 0.75rem);
              padding: 0.375rem 0.75rem;
              line-height: 1.5;
              color: #5c6873;
              content: "Buscar";
              background-color: #f0f3f9;
              border-left: inherit;
              border-radius: 0 0.25rem 0.25rem 0;
            }
        </style>
    @endpush

    @push('crud_fields_scripts')
        <script>
            document.addEventListener('DOMContentLoaded', function () {
                const fileInput = document.getElementById('{{$field['attributes']['id']}}');
                const fileError = document.getElementById('fileError');
                //const fileForm = document.querySelector('form');

                // Valida tamanho do arquivo
                fileInput.addEventListener('input', function (event) {
                    const maxSize = 30 * 1024 * 1024; // 30MB em bytes
                    const file = event.target.files[0]; // Obtém o arquivo selecionado

                    if (file && file.size > maxSize) {
                        const fileSizeInMB = (file.size / (1024 * 1024)).toFixed(2);
                        exibirAlertaNoty('warning-custom', `O arquivo selecionado (${fileSizeInMB} MB) excede o limite de 30 MB.`);
                       // fileError.innerText = `O arquivo selecionado (${fileSizeInMB} MB) excede o limite de 30 MB.`;
                        fileError.style.display = 'block';
                        this.value = ''; // Limpa o valor do input
                    } else if (!file) {
                        // Se o campo de arquivo for limpo, remove a mensagem de erro
                        fileError.style.display = 'none';
                        fileError.innerText = ''; // Remove o texto da mensagem de erro
                    } else {
                        fileError.style.display = 'none'; // Esconde a mensagem de erro se o arquivo for válido
                    }
                });
                //
                // // Permite o envio do formulário, mas exibe o alerta se o erro for relacionado ao tamanho
                // fileForm.addEventListener('submit', function (event) {
                //     const file = fileInput.files[0];
                //     const fileSize = file ? file.size : 0;
                //     const maxSize = 2 * 1024 * 1024; // 2MB em bytes
                //
                //     // Se o arquivo for inválido ou o erro for relacionado ao tamanho
                //     if ((file && fileSize > maxSize) || !file) {
                //         event.preventDefault();  // Impede o envio
                //         if (fileSize > maxSize) {
                //             exibirAlertaNoty('warning-custom', `O arquivo selecionado (
                //             ${(fileSize / (1024 * 1024)).toFixed(2)} MB) excede o limite de 2 MB.`);
                //         } else {
                //             exibirAlertaNoty('warning-custom', "Por favor, selecione um arquivo válido
                //             antes de enviar.");
                //         }
                //     }
                // });
            });
        </script>




        <!-- no scripts -->
        <script>
            function bpFieldInitUploadElement(element) {
                var fileInput = element.find(".file_input");
                var fileClearButton = element.find(".file_clear_button");
                var fieldName = element.attr('data-field-name');
                var inputWrapper = element.find(".backstrap-file");
                var inputLabel = element.find(".backstrap-file-label");

                fileClearButton.click(function(e) {
                    e.preventDefault();
                    $(this).parent().addClass('d-none');

                    fileInput.parent().removeClass('d-none');
                    fileInput.attr("value", "").replaceWith(fileInput.clone(true));

                    // redo the selector, so we can use the same fileInput variable going forward
                    fileInput = element.find(".file_input");

                    // add a hidden input with the same name, so that the setXAttribute method is triggered
                    $("<input type='hidden' name='"+fieldName+"' value=''>").insertAfter(fileInput);
                });

                fileInput.change(function() {
                    var path = $(this).val();
                    var path = path.replace("C:\\fakepath\\", "");
                    inputLabel.html(path);
                    // remove the hidden input, so that the setXAttribute method is no longer triggered
                    $(this).next("input[type=hidden]").remove();
                });

            }

            // Função para inserir o Id do arquivo no campo hidden para enviar ao form
            function deletarArquivo(fieldName, idArquivo) {
                $(`input[name='${fieldName}_clear']`).val(idArquivo);
                $(`#row_arquivo_${idArquivo}`).remove()
                $(`input[name='${fieldName}']`).val();
                $("#arquivoSalvo").val(false)
            }
        </script>
    @endpush
@endif
