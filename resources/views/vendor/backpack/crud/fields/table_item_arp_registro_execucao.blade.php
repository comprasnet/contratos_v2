<div class="col-md-12 pt-5 table-responsive {{ $field['classArea'] }}" element="div" bp-field-wrapper="true"
     bp-field-type="text"
     @php
         if (isset($field['idArea']) && !empty($field['idArea'])) {
     @endphp
     id="{{ $field['idArea'] }}"
        @php } @endphp
>
    <label>Itens da ata</label>
    <table id="table_item_arp_registro_execucao" class="table">
        <thead>
        <th>Fornecedor</th>
        <th>Número</th>
        <th>Descrição</th>
        <th>Grupo</th>
        <th>Quantidade registrada/autorizada</th>
        <th>Quantidade empenhada</th>
        <th>Justificativa</th>
        </thead>
        <tbody id="tbody_item_arp_registro_execucao">
        @foreach ($field['value'] as $linha)
            {!! $linha !!}
        @endforeach
        </tbody>
    </table>
</div>