<!-- number input -->
@php
    $field['required'] = $field['required'] ?? false;
@endphp
@include('crud::fields.inc.wrapper_start')

@include('crud::fields.inc.translatable_icon')

@if(isset($field['prefix']) || isset($field['suffix'])) <div class="input-group"> @endif
    @if(isset($field['prefix'])) <div class="input-group-prepend"><span class="input-group-text">{!! $field['prefix'] !!}</span></div> @endif
    <div class="br-input">
        <label class="{{ $field['class'] ?? null }}" for="{{ $field['name'] }}" >{!! $field['label'] !!}</label>
        @if($field['required'])
            @include("vendor.backpack.crud.fields.marcacao_obrigatorio",['textTooltip' => 'Este campo é obrigatório'])
        @endif
        <input for="{{ $field['name'] }}"  type="number" name="{{ $field['name'] }}"
               value="{{ old_empty_or_null($field['name'], '') ??  $field['value'] ?? $field['default'] ?? '' }}"
                @include('crud::fields.inc.attributes') />

        @if(isset($field['suffix'])) <div class="input-group-append"><span class="input-group-text">{!! $field['suffix'] !!}</span></div> @endif
        @if(isset($field['prefix']) || isset($field['suffix'])) </div> @endif

    @if (isset($field['hint']))
        <p>{!! $field['hint'] !!}</p>
    @endif

</div>

@if(isset($field['suffix'])) <div class="input-group-append"><span class="input-group-text">{!! $field['suffix'] !!}</span></div> @endif
@if(isset($field['prefix']) || isset($field['suffix'])) </div> @endif

@include('crud::fields.inc.wrapper_end')
