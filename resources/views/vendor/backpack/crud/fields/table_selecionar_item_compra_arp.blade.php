<div class="col-md-12 pt-5 table-responsive" element="div" bp-field-wrapper="true" bp-field-type="text"style="display : none;" id="areaItemFornecedor">
    <b><span id="nomeFornecedor"></span></b>
    <table id="table_selecionar_item_compra_arp" class="table">
        <thead>
        <tr>
            <th>
                <input type="checkbox" class="selectAll">
                <i class="fas fa-info-circle" title="Ao marcar o todos, somente os itens da página atual serão selecionados"></i>
            </th>
            <th>Número</th>
            <th>Tipo</th>
            <th>Código</th>
            <th>Descrição</th>
            <th>Classificação do fornecedor</th>
            <th>Quantidade registrada</th>
            <th>Valor Unit.</th>
            <th>Valor Total</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>

<div class="mb-3">
    <a href="#areaItemFornecedor" id="btnInserirItemFornecedor" class="br-button primary large mr-3 menu-item-link-externo br-button-form">Incluir Item</i></a>
    <!-- <div class="br-tooltip" role="tooltip" info="info" place="top"><span class="subtext">Selecionar Item</span></div> -->
</div>
</div>

@push('after_scripts')
<script>
    let languageDt = {
                            "emptyTable":     "Selecionar o fornecedor para exibir a informação",
                            "info":           "{{ trans('backpack::crud.info') }}",
                            "infoEmpty":      "{{ trans('backpack::crud.infoEmpty') }}",
                            "infoFiltered":   "{{ trans('backpack::crud.infoFiltered') }}",
                            "infoPostFix":    "{{ trans('backpack::crud.infoPostFix') }}",
                            "thousands":      "{{ trans('backpack::crud.thousands') }}",
                            "lengthMenu":     "{{ trans('backpack::crud.lengthMenu') }}",
                            "loadingRecords": "{{ trans('backpack::crud.loadingRecords') }}",
                            "processing":     "<img src='{{ asset('packages/backpack/base/img/spinner.svg') }}' alt='{{ trans('backpack::crud.processing') }}'>",
                            "search": "_INPUT_",
                            "searchPlaceholder": "{{ trans('backpack::crud.search') }}...",
                            "zeroRecords":    "{{ trans('backpack::crud.zeroRecords') }}",
                            "paginate": {
                                "first":      "{{ trans('backpack::crud.paginate.first') }}",
                                "last":       "{{ trans('backpack::crud.paginate.last') }}",
                                "next":       ">",
                                "previous":   "<"
                            },
                            "aria": {
                                "sortAscending":  "{{ trans('backpack::crud.aria.sortAscending') }}",
                                "sortDescending": "{{ trans('backpack::crud.aria.sortDescending') }}"
                            }
                        }

    function carregarTabelaItemFornecedor(idFornecedor = null, inicializar = true, unidade_participante = null) {

        if(inicializar) {
            datableModalItem = $('#table_selecionar_item_compra_arp').DataTable({
                language: languageDt
            });
            return
        }
        let numero_ano = $("input[name=numero_ano]").val()
        let unidade_origem_id = $("input[name=unidade_origem_id]").val()
        let modalidade_id = $("select[name='modalidade_id']").val() ?? $("input[name='modalidade_id']").val()
        
        // unidade_participante.push(unidade_origem_id)

        datableModalItem.clear();
        datableModalItem.destroy();
        datableModalItem = $('#table_selecionar_item_compra_arp').
        DataTable({
                    processing: true,
                    serverSide: true,
                    autoWidth: false,
                    ajax: {
                        type: 'POST',
                        url: '{{ url("arp/itemcomprafornecedor") }}',
                        data: {
                            numero_ano: numero_ano,
                            idFornecedor:idFornecedor,
                            idUnidade:unidade_participante,
                            modalidadeId: modalidade_id
                        }
                    },
                    responsive: true,
                    fixedHeader: true,
                    pageLength: 5,
                    aaSorting: [[ 1, "asc" ], [ 2, "asc" ]],
                    lengthMenu: [5, 10, 25, 50, 100],
                    language: languageDt,                    
                    columns: [
                        { data: 'action', name: 'action',  orderable: false, "width": "50px"},
                        {data: 'numero', name: 'numero'},
                        {data: 'descricao', name: 'descricao', },
                        {data: 'codigo_siasg', name: 'codigo_siasg', },
                        {data: 'descricaosimplificada', name: 'descricaosimplificada', },
                        {data: 'classificacao', name: 'classificacao'},
                        {data: 'quantidadehomologadavencedor', name: 'quantidadehomologadavencedor' },
                        {data: 'valor_unitario', name: 'valor_unitario' },
                        {data: 'valor_negociado', name: 'valor_negociado'},
                        {data: 'quantidade_adquirir', name: 'quantidade_adquirir',visible: false, searchable: false,}
                        
                    ],
                }).on( 'draw', function () { checkTodos(".selectAll",false); recuperarItemMarcado() } );

    }

</script>
@endpush