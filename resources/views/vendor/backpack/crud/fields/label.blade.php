<!-- text input -->
@php
$field['date'] = $field['date'] ?? false;

if ($field['date']) {
    $date = new DateTimeImmutable($field['value']);
    $field['value'] = $date->format('d/m/Y');
}

$field['money'] = $field['money'] ?? false;

if ($field['money']) {
    $field['value'] = "R$ ".number_format($field['value'], 2, ',', '.');
}

@endphp
@include('crud::fields.inc.wrapper_start')
    @include('crud::fields.inc.translatable_icon')

    @if(isset($field['prefix']) || isset($field['suffix'])) <div class="input-group"> @endif
        @if(isset($field['prefix'])) <div class="input-group-prepend"><span class="input-group-text">{!! $field['prefix'] !!}</span></div> @endif
        <div class="br-input">
            <label for="{{ $field['name'] }}" class="mb-2" >{!! $field['label'] !!}</label>
            <br >
            <span>{!! $field['value'] !!}</span>
        </div>

@include('crud::fields.inc.wrapper_end')
