<div class="restrito-checkbox">
    <input type="checkbox" name="restrito" id="input_restrito" disabled>
    <label class="font-bold" for="input_restrito">
        Restrito
        <span class="tooltip_restrito">
        <i class="fa fa-info-circle" aria-hidden="true"></i>
        <span class="tooltiptext_restrito">Arquivo restrito não será disponibilizado publicamente</span>
    </span>
    </label>

</div>

<style>
    /* Tooltip container */
    .tooltip_restrito {
        position: relative;
        display: inline-block;
    }

    /* Tooltip text */
    .tooltip_restrito .tooltiptext_restrito {
        visibility: hidden;
        width: 250px;
        background-color: black;
        color: #fff;
        text-align: center;
        padding: 5px 0;
        border-radius: 6px;

        /* Position the tooltip text - see examples below! */
        position: absolute;
        z-index: 1;
    }

    /* Show the tooltip text when you mouse over the tooltip container */
    .tooltip_restrito:hover .tooltiptext_restrito {
        visibility: visible;
    }
</style>