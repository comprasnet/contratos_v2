@php
$field['label_instrucao_tabela'] = $field['label_instrucao_tabela'] ?? 'Selecionar item';
$field['order_table'] = $field['order_table'] ?? "[1,'desc']";

@endphp

<div class="{{ $field['classArea'] }} table-responsive" element="div" bp-field-wrapper="true" bp-field-type="text"  id="{{ $field['idArea'] }}">
    <label>{!! $field['label_instrucao_tabela'] !!}</label>
    <table id="{{ $field['idTable'] }}" class="table">
        <thead>
        <tr>
            {{-- <th><input type="checkbox" class="selectAll"> <i class="fas fa-info-circle" title="Ao marcar o todos, somente a página atual será selecionada"></i></th> --}}
            @foreach ($field['column'] as $item)
                <th>{!! $item !!}</th>
            @endforeach
        </tr>
        </thead>
        @if (isset($field['value']) && !empty($field['value']))
            <tbody>
                @foreach ($field['value'] as $linha)
                <tr>
                    @foreach ($field['column'] as $key => $item)
                        <td>
                            {!! $linha[$key] !!}
                        </td>
                    @endforeach


                </tr>
                @endforeach
            </tbody>
        @endif
    </table>

{{-- <div class="mb-3">
    <a href="#areaItemAdesao" id="btnInserirItemFornecedor" class="br-button primary large mr-3 menu-item-link-externo br-button-form">Incluir Item</i></a>
</div> --}}
</div>

@push('after_scripts')
<script>
    let languageDt = {
                            "emptyTable":     "{{ $field['emptyTable'] }}",
                            "info":           "{{ trans('backpack::crud.info') }}",
                            "infoEmpty":      "{{ trans('backpack::crud.infoEmpty') }}",
                            "infoFiltered":   "{{ trans('backpack::crud.infoFiltered') }}",
                            "infoPostFix":    "{{ trans('backpack::crud.infoPostFix') }}",
                            "thousands":      "{{ trans('backpack::crud.thousands') }}",
                            "lengthMenu":     "{{ trans('backpack::crud.lengthMenu') }}",
                            "loadingRecords": "{{ trans('backpack::crud.loadingRecords') }}",
                            "processing":     "<img src='{{ asset('packages/backpack/base/img/spinner.svg') }}' alt='{{ trans('backpack::crud.processing') }}'>",
                            "search": "_INPUT_",
                            "searchPlaceholder": "{{ trans('backpack::crud.search') }}...",
                            "zeroRecords":    "{{ trans('backpack::crud.zeroRecords') }}",
                            "paginate": {
                                "first":      "{{ trans('backpack::crud.paginate.first') }}",
                                "last":       "{{ trans('backpack::crud.paginate.last') }}",
                                "next":       ">",
                                "previous":   "<"
                            },
                            "aria": {
                                "sortAscending":  "{{ trans('backpack::crud.aria.sortAscending') }}",
                                "sortDescending": "{{ trans('backpack::crud.aria.sortDescending') }}"
                            }
                        }
var datableItem = null

function inserirLinhaItem(dadosFiltro) {
    var tableExists = $.fn.DataTable.isDataTable('#{{ $field['idTable'] }}');

    if (tableExists) {
        // Se o DataTable já existir, destrua-o antes de inicializar um novo
        $('#{{ $field['idTable'] }}').DataTable().destroy();
    }

        datableItem = $('#{{ $field['idTable'] }}').DataTable({
            autoWidth: false,
            responsive: true,
            fixedHeader: true,
            processing: true,
            serverSide: true,
            pageLength: {{ $field['pageLength'] }},
            language: languageDt,
            lengthMenu: [{{ $field['pageLength'] }}],
            order: [{!! $field['order_table'] !!}],
            "ajax": {
                "url": '{{ $field['urlAjax'] }}',
                "type": 'POST',
                data: function(d) {
                    d.numeroAnoCompra = dadosFiltro['numeroAnoCompra']
                    d.modalidadeCompra = dadosFiltro['modalidadeCompra']
                    d.unidadeCompra = dadosFiltro['unidadeCompra']
                    d.numeroAta = dadosFiltro['numeroAta']
                    d.idRemanejamento = dadosFiltro['idRemanejamento']
                    d.draw = d.draw;
                }
            },
            columns: [
                    @foreach ($field['column'] as $key => $item)
                {
                    data: '@php echo $key; @endphp',
                },
                @endforeach
            ]
        });

        $('.dataTables_filter input[type="search"]').css({'width':'680px'});
        $('div.dataTables_wrapper div.dataTables_length select').css("width", "70px")
}
</script>
@endpush