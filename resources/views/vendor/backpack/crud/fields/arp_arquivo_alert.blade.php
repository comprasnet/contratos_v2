<div class="br-message warning arquivo-alert" id="arp_arquivo_alert" role="alert">
    <div class="icon">
        <i class="fas fa-exclamation-triangle fa-lg" aria-hidden="true"></i>
    </div>
    <div class="content">
        <span class="message-title">Atenção.</span>
        <span class="message-body"> A inclusão de um novo arquivo do tipo "Ata de Registro de Preços" irá substituir o
            arquivo existente.</span>
    </div>
</div>
<style>
    .arquivo-alert {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 100%;        
    }
</style>
