@php
    $field['wrapper'] = $field['wrapper'] ?? $field['wrapperAttributes'] ?? [];
    $field['wrapper']['data-init-function'] = $field['wrapper']['data-init-function'] ?? 'bpFieldInitUploadMultipleElement';
    $field['wrapper']['data-field-name'] = $field['wrapper']['data-field-name'] ?? $field['name'];
@endphp

<!-- upload multiple input -->
@include('crud::fields.inc.wrapper_start')
<div class="br-upload">
    <!-- <label>{!! $field['label'] !!}</label> -->
	<label class="upload-label" for="multiple-files">
		<span>{!! $field['label'] !!}</span>
		@if(isset($field['required']) && $field['required'])
			@include("vendor.backpack.crud.fields.marcacao_obrigatorio",['textTooltip' => 'Este campo é obrigatório'])
		@endif
	</label>

    @include('crud::fields.inc.translatable_icon')
	{{-- Show the file picker on CREATE form. --}}
	<input name="{{ $field['name'] }}[]" type="hidden" value="">
	<!-- <div class="backstrap-file mt-2"> -->
		<input
	        type="file"
	        name="{{ $field['name'] }}[]"
			class="upload-input"
			@if (isset($field['accept']))
                accept="{{$field['accept']}}"
            @endif
	        @include('crud::fields.inc.attributes', ['default_class' =>  isset($field['value']) && $field['value']!=null?'file_input backstrap-file-input':'file_input backstrap-file-input'])
	        multiple="multiple"
	    >
		<div class="upload-list"></div>
        <!-- <label class="backstrap-file-label" for="customFile"></label> -->
    <!-- </div> -->

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
	</div>

	{{-- Show the file name and a "Clear" button on EDIT form. --}}
	@if (isset($field['value']))
	@php
		if (is_string($field['value'])) {
			$values = json_decode($field['value'], true) ?? [];
		} else {
			$values = $field['value'];
		}
	@endphp
	@if (count($values))
    <div class="well well-sm existing-file"style="max-width: 550px;">
    	@foreach($values as $key => $file_path)
			@php
				$arrayArquivo = explode("-",$file_path);
				
				$file_path = $arrayArquivo[0];
				$textoArquivo = str_replace("_", " ", $arrayArquivo[1]);
			@endphp
    		<div class="file-preview">
    			@if (isset($field['temporary']))
		            <a target="_blank" href="{{ isset($field['disk'])?asset(\Storage::disk($field['disk'])->temporaryUrl($file_path, Carbon\Carbon::now()->addMinutes($field['temporary']))):asset($file_path) }}">{{ $textoArquivo }}</a>
		        @else
		            <a target="_blank" href="{{ url("storage/{$file_path}") }}">{{ $textoArquivo }}</a>
					{{-- isset($field['disk'])?asset(\Storage::disk($field['disk'])->url($file_path)):asset($file_path) --}}
		        @endif
		    	<a href="#" class="btn btn-light btn-sm float-right file-clear-button"  data-filename="{{ $file_path }}"><i class="fa fa-trash"></i></a>
				@include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => 'Remover arquivo'])
		    	<div class="clearfix"></div>
	    	</div>
    	@endforeach
    </div>
    @endif
    @endif

	@include('crud::fields.inc.wrapper_end')

{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
	@push('crud_fields_styles')
	@loadOnce('upload_field_styles')
	<style type="text/css">
		.existing-file {
			border: 1px solid rgba(0,40,100,.12);
			border-radius: 5px;
			padding-left: 10px;
			vertical-align: middle;
		}
		.existing-file a {
			padding-top: 5px;
			display: inline-block;
			font-size: 0.9em;
		}
		.backstrap-file {
			position: relative;
			display: inline-block;
			width: 100%;
			height: calc(1.5em + 0.75rem + 2px);
			margin-bottom: 0;
		}

		.backstrap-file-input {
			position: relative;
			z-index: 2;
			width: 100%;
			height: calc(1.5em + 0.75rem + 2px);
			margin: 0;
			opacity: 0;
		}

		.backstrap-file-input:focus ~ .backstrap-file-label {
			border-color: #acc5ea;
			box-shadow: 0 0 0 0rem rgba(70, 127, 208, 0.25);
		}

		.backstrap-file-input:disabled ~ .backstrap-file-label {
			background-color: #e4e7ea;
		}

		.backstrap-file-input:lang(en) ~ .backstrap-file-label::after {
			content: "Browse";
		}

		.backstrap-file-input ~ .backstrap-file-label[data-browse]::after {
			content: attr(data-browse);
		}

		.backstrap-file-label {
			position: absolute;
			top: 0;
			right: 0;
			left: 0;
			z-index: 1;
			height: calc(1.5em + 0.75rem + 2px);
			padding: 0.375rem 0.75rem;
			font-weight: 400;
			line-height: 1.5;
			color: #5c6873;
			background-color: #fff;
			border: 1px solid #e4e7ea;
			border-radius: 0.25rem;
			font-weight: 400!important;
		}

		.backstrap-file-label::after {
			position: absolute;
			top: 0;
			right: 0;
			bottom: 0;
			z-index: 3;
			display: block;
			height: calc(1.5em + 0.75rem);
			padding: 0.375rem 0.75rem;
			line-height: 1.5;
			color: #5c6873;
			content: "Browse";
			background-color: #f0f3f9;
			border-left: inherit;
			border-radius: 0 0.25rem 0.25rem 0;
		}
	</style>
	@endLoadOnce
	@endpush
    @push('crud_fields_scripts')
    	@loadOnce('bpFieldInitUploadMultipleElement')
        <script>
        	function bpFieldInitUploadMultipleElement(element) {
        		var fieldName = element.attr('data-field-name');
        		var clearFileButton = element.find(".file-clear-button");
        		var fileInput = element.find("input[type=file]");
        		var inputLabel = element.find("label.backstrap-file-label");

		        clearFileButton.click(function(e) {
		        	e.preventDefault();
		        	var container = $(this).parent().parent();
		        	var parent = $(this).parent();
		        	// remove the filename and button
		        	parent.remove();
		        	// if the file container is empty, remove it
		        	if ($.trim(container.html())=='') {
		        		container.remove();
		        	}
		        	$("<input type='hidden' name='clear_"+fieldName+"[]' value='"+$(this).data('filename')+"'>").insertAfter(fileInput);
		        });

		        fileInput.change(function() {
	                inputLabel.html("Files selected. After save, they will show up above.");
					let selectedFiles = [];

					Array.from($(this)[0].files).forEach(file => {
						selectedFiles.push({name: file.name, type: file.type})
					});

					element.find('input').first().val(JSON.stringify(selectedFiles)).trigger('change');
		        	// remove the hidden input, so that the setXAttribute method is no longer triggered
					$(this).next("input[type=hidden]:not([name='clear_"+fieldName+"[]'])").remove();

					const maxSize = 30 * 1024 * 1024; // 30MB em bytes
					const file = event.target.files[0]; // Obtém o arquivo selecionado

					if (file && file.size > maxSize) {
						const fileSizeInMB = (file.size / (1024 * 1024)).toFixed(2);
						exibirAlertaNoty('warning-custom', `O arquivo selecionado (${fileSizeInMB} MB) excede o limite de 30 MB.`);
						// fileError.innerText = O arquivo selecionado (${fileSizeInMB} MB) excede o limite de 30 MB.;
						fileError.style.display = 'block';
						const that = this;

						function isGreaterThan5MB(sizeString) {
							// Converte a string para um valor em bytes
							const sizeInBytes = convertToBytes(sizeString);

							// 5 MB em bytes
							const fiveMBInBytes = 5 * 1024 * 1024;

							// Verifica se é maior que 5 MB
							return sizeInBytes > fiveMBInBytes;
						}

						function convertToBytes(sizeString) {
							const regex = /^([\d.]+)\s*(KB|MB|GB)$/i;
							const match = sizeString.match(regex);

							if (!match) {
								throw new Error("Formato inválido: " + sizeString);
							}

							const value = parseFloat(match[1]);
							const unit = match[2].toUpperCase();

							// Converte para bytes
							switch (unit) {
								case "KB":
									return value * 1024;
								case "MB":
									return value * 1024 * 1024;
								case "GB":
									return value * 1024 * 1024 * 1024;
								default:
									return 0;
							}
						}
						setTimeout(function() {
							const $buttons = $(that).parent().find('.upload-list .br-button');
							if ($buttons.length) {
								$buttons.each(function() {
									const fileSize = $(this).parent().find('span').text();
									if(isGreaterThan5MB(fileSize)){
										$(this).trigger('click');
									}
								});
							}
						}, 1000);

					}

		        });





				element.find('input').on('CrudField:disable', function(e) {
					element.children('.backstrap-file').find('input').prop('disabled', 'disabled');
					element.children('.existing-file').find('.file-preview').each(function(i, el) {

						let $deleteButton = $(el).find('a.file-clear-button');

						if(deleteButton.length > 0) {
							$deleteButton.on('click.prevent', function(e) {
								e.stopImmediatePropagation();
								return false;
							});
							// make the event we just registered, the first to be triggered
							$._data($deleteButton.get(0), "events").click.reverse();
						}
					});
				});

				element.on('CrudField:enable', function(e) {
					element.children('.backstrap-file').find('input').removeAttr('disabled');
					element.children('.existing-file').find('.file-preview').each(function(i, el) {
						$(el).find('a.file-clear-button').unbind('click.prevent');
					});
				});
        	}
        </script>
        @endLoadOnce
    @endpush
