<div class="col-md-12 pt-5 table-responsive" element="div" bp-field-wrapper="true" bp-field-type="text" style="display : none;" id="{{ $field['idArea'] }}">
    <label>Fornecedor</label>
    <table id="table_fornecedor_compra_arp" class="table">
    <thead>
        <th>Item</th>
        <th>CNPJ</th>
        <th>Fornecedor</th>
        <th>Situação SICAF</th>
    </thead>
    <tbody id="tbody_fornecedor_compra_arp"></tbody>
    </table>
</div>


@push('after_scripts')
<script>
    let languageDtFornecedor = {
                            "emptyTable":     "Selecionar o fornecedor para exibir a informação",
                            "info":           "{{ trans('backpack::crud.info') }}",
                            "infoEmpty":      "{{ trans('backpack::crud.infoEmpty') }}",
                            "infoFiltered":   "{{ trans('backpack::crud.infoFiltered') }}",
                            "infoPostFix":    "{{ trans('backpack::crud.infoPostFix') }}",
                            "thousands":      "{{ trans('backpack::crud.thousands') }}",
                            "lengthMenu":     "{{ trans('backpack::crud.lengthMenu') }}",
                            "loadingRecords": "{{ trans('backpack::crud.loadingRecords') }}",
                            "processing":     "<img src='{{ asset('packages/backpack/base/img/spinner.svg') }}' alt='{{ trans('backpack::crud.processing') }}'>",
                            "search": "_INPUT_",
                            "searchPlaceholder": "{{ trans('backpack::crud.search') }}...",
                            "zeroRecords":    "{{ trans('backpack::crud.zeroRecords') }}",
                            "paginate": {
                                "first":      "{{ trans('backpack::crud.paginate.first') }}",
                                "last":       "{{ trans('backpack::crud.paginate.last') }}",
                                "next":       ">",
                                "previous":   "<"
                            },
                            "aria": {
                                "sortAscending":  "{{ trans('backpack::crud.aria.sortAscending') }}",
                                "sortDescending": "{{ trans('backpack::crud.aria.sortDescending') }}"
                            }
                        }

function inserirLinhaFornecedor(item, inicializar = true) {

    if(inicializar) {
        datableFornecedor = $('#table_fornecedor_compra_arp').DataTable({
            language: languageDtFornecedor
        });
        return
    }

        datableFornecedor.clear();
        datableFornecedor.destroy();

    datableFornecedor = $('#table_fornecedor_compra_arp').DataTable({
        data: item,
        autoWidth: false,
        responsive: true,
        fixedHeader: true,
        pageLength: 5,
        language: languageDtFornecedor,
        lengthMenu: [5, 10, 25, 50, 100],
        columns: [
          { data: "botaoitem",  orderable: false },
          { data: "cpf_cnpj_idgener" },
          { data: "nome" },
          { data: "situacao_sicaf" },

        ]
    });
}

</script>
@endpush