<div class="col-md-4 pt-5-form-br" >
<a id= "btn_validar_arp" class="br-button circle primary br-button-form br-button-cor-icone" tabindex="0">
   <i class="fas fa-plus" id="icone_btn_validar_arp"></i>
</a>
@include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => 'Validar'])
</div>
@push('after_scripts')
   <script>
      {{-- js para possibilitar a tecla space no botão --}}
      document.getElementById('btn_validar_arp').addEventListener('keydown', function(event) {
         if (event.key === ' ' || event.key === 'Enter') {
            event.preventDefault();
            this.click();
         }
      });
   </script>

@endpush