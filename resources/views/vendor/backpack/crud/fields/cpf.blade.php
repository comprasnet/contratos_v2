@include('crud::fields.text', $field)

@push('crud_fields_scripts')
    <script type="text/javascript">
        maskCPF($('input[name={{ $field['name'] }}]'))
    </script>
@endpush
