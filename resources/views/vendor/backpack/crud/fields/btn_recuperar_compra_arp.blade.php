
@php
$textToolTip = isset($field['texttooltip'])? !empty($field['texttooltip'])? $field['texttooltip'] : 'Buscar compra' : 'Buscar compra';
$classArea = $field['classArea'] ?? 'col-md-12';
@endphp
<div class=" {{ $classArea }} pt-6-form-br" >
    <button id= "{{ $field['id'] }}" class="br-button primary mr-3" type="button">
        <i class="las la-search mr-1 mt-1" id="icone_btn_recuperar_compra_arp"></i>
        <span data-value="save_and_back">{{ $field['text'] }}</span>
    </button>

@include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => $field['texttooltip'] ?? 'Buscar compra'])
@if (isset($field['hint']))
<p class="{{ $field['classArea'] }}">{!! $field['hint'] !!}</p>
@endif
    @if(isset($crud->limparAdesao))
        <button class="br-button  mr-3" type="button" onclick="limparCamposAdesao()">
            <span data-value="save_and_back">Limpar</span>
        </button>
    @endif
</div>



