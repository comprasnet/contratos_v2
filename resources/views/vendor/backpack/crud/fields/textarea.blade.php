<!-- text area -->
@php
    $field['required'] = $field['required'] ?? false;
    $field['attributes']['placeholder'] = $field['attributes']['placeholder'] ?? '';

    $quantidadeCaracterDigitado =  $field['attributes']['maxlength'] ?? 0;
@endphp

@include('crud::fields.inc.wrapper_start')
@include('crud::fields.inc.translatable_icon')

@if(isset($field['prefix']) || isset($field['suffix'])) <div class="input-group"> @endif
    @if(isset($field['prefix'])) <div class="input-group-prepend"><span class="input-group-text">{!! $field['prefix'] !!}</span></div> @endif
    <div class="br-textarea">
        <label for="{{ $field['name'] }}" >{!! $field['label'] !!}</label>
        @if($field['required'])
            @include("vendor.backpack.crud.fields.marcacao_obrigatorio",['textTooltip' => 'Este campo é obrigatório'])
        @endif
        <textarea id="{{ $field['name'] }}" placeholder="{{ $field['attributes']['placeholder'] }}" 
                    name="{{ $field['name'] }}" @include('crud::fields.inc.attributes')>{{ old_empty_or_null($field['name'], '') ??  $field['value'] ?? $field['default'] ?? '' }}</textarea>

        @if($quantidadeCaracterDigitado > 0)
            <div class="text-base mt-1">
                <span class="limit" aria-live="polite">Limite máximo de <strong>{{$quantidadeCaracterDigitado}}</strong> caracteres</span>
                <span class="current" aria-live="polite" role="status" id="limitmax"></span>
            </div>
        @endif

        @if($quantidadeCaracterDigitado == 0)
            <div class="text-base mt-1"><span class="characters"><strong>{{$quantidadeCaracterDigitado}}</strong> caracteres digitados</span></div>
        @endif

        @if(isset($field['suffix'])) <div class="input-group-append"><span class="input-group-text">{!! $field['suffix'] !!}</span></div> @endif
        @if(isset($field['prefix']) || isset($field['suffix'])) </div> @endif

    @if (isset($field['hint']))
        <p>{!! $field['hint'] !!}</p>
    @endif

</div>

@if(isset($field['suffix'])) <div class="input-group-append"><span class="input-group-text">{!! $field['suffix'] !!}</span></div> @endif
@if(isset($field['prefix']) || isset($field['suffix'])) </div> @endif

@include('crud::fields.inc.wrapper_end')
