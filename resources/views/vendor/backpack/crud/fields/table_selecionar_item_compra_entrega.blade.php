@php
        $itensEntreguesOsfModel = new \App\Repositories\AutorizacaoExecucao\UsuarioFornecedorAutorizacaoExecucaoEntregaRepository();
        @endphp
<div class="col-md-12 pt-5 table-responsive" element="div" bp-field-wrapper="true" bp-field-type="text" id="itensOsfEntrega">
    <b><span id="nomeFornecedor"></span></b>
    <table id="table_selecionar_item_compra_arp" class="table">
        <h6>Itens Entregues</h6> <!-- Título da Tabela Adicionado -->
        <thead>
        <tr>
            <th>
                <label style="display: block; text-align: center;">
                    <input type="checkbox" class="selectAll" id="selecionar_todos_itens" title="Selecionar todos"/>
                </label>
            </th>
            <th>Tipo item</th>
            <th>Num item compra</th>
            <th>Item</th>
            <th>Especificações Complementares</th>
            <th>Unidade de Fornecimento</th>
            <th>Quantidade Solicitada na OS/F</th>
            <th>Quantidade Informada na Entrega</th>
            <th>Quantidade já Executada na OS/F</th>
            <th>Valor Unitário</th>
            <th>Valor Total da entrega</th>
            <th>Valor Total já Executado da OS/F</th>
        </tr>
        </thead>
        <tbody>
            @foreach($itens as $key=> $item)
                @php
                    $quantidadeInformada = $item->itensEntrega ? $item->itensEntrega->quantidade_informada : 0;

                    $checked = $item->itensEntrega ? 'checked' : '';
                    $disabled = $item->itensEntrega ? '' : 'disabled';

                    $valorTotalEntrega = $quantidadeInformada * str_replace(['.', ','], ['', '.'], $item->valor_unitario);

                    $qtdIntensEntregusOsf = $itensEntreguesOsfModel->retornaQuantidadeJaExecutadaOsfRepository(
                        $aeId,
                        $item->id,
                        request()->id
                    );

                    $valorExecutadoOsf = $qtdIntensEntregusOsf * str_replace(['.', ','], ['', '.'], $item->valor_unitario);

                    $valorTotalOsf = $itensEntreguesOsfModel->retornaValorTotaldaOs($aeId);
                    $valorUnitarioExibicao = str_replace(['.', ','], ['', '.'], $item->valor_unitario);

                @endphp
                <tr>
                    <td><input type="checkbox" name="itens[{{ $key }}][itens_selecionados]" value="{{ $item->id }}" class="item_checkbox" {{ $checked }}/></td>
                    <td class="tipo_item">{{ $item->tipo_item }}</td>
                    <td class="numero_item_compra">{{ $item->numero_item_compra }}</td>
                    <td class="item">{{ $item->item }}</td>
                    <td class="especificacao">{{ $item->especificacao }}</td>
                    <td class="unidade_medida_id">{{ $item->unidadeMedida->nome }}</td>
                    <td class="quantidade_solicitada">{{ $item->getQuantidadeTotal() }}</td>
                    <td><input type="text" name="itens[{{ $key }}][quantidade_informada]" value="{{ $quantidadeInformada }}" class="quantidade_informada" data-key="{{ $key }}" {{ $disabled }} step="0.001" min="0"/></td>
                    <td class="qtd_executada_osf">{{ $qtdIntensEntregusOsf }}</td>
                    {{--<td class="valor_unitario">{{ $item->valor_unitario }}</td>--}}
                    <td class="valor_unitario">{{ 'R$ ' . number_format($valorUnitarioExibicao, 2, ',', '.') }}</td><input type="hidden" name="itens[{{ $key }}][valor_unitario]" value="{{ $item->valor_unitario }}" readonly {{ $disabled }}/></td>
                    <td class="valor_total_entrega_{{ $key }}"><span>{{ $valorTotalEntrega }}</span><input type="hidden" name="itens[{{ $key }}][valor_total_entrega]" value="{{ $valorTotalEntrega }}" readonly {{ $disabled }}/></td>
                    <td class="valor_executado_osf">{{ 'R$ ' . number_format($valorExecutadoOsf, 2, ',', '.') }}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr style="background-color: var(--background-alternative) !important">
                <td colspan="11">Valor Total da OS/F</td>
                <td colspan="1">{{ 'R$ ' . number_format($valorTotalOsf, 2, ',', '.') }}</td>
            </tr>
        </tfoot>
    </table>
</div>

@push('after_scripts')
<script>
        $(document).ready(function(){

            $('td[class^="valor_total_entrega_"] span').each(function() {
                const valorStr = $(this).text().replace('R$', '').trim().replace(',', '.');
                const valor = parseFloat(valorStr);
                if (!isNaN(valor)) {
                    const valorFormatado = formatReal(valor);
                    $(this).text(valorFormatado);
                }
            });

            $('#selecionar_todos_itens').click(function() {
                var checked = this.checked;
                $('.item_checkbox').prop('checked', checked).each(function() {
                    toggleRowInputs($(this), checked);
                });
            });

            $('.item_checkbox').click(function() {
                toggleRowInputs($(this), this.checked);
            });

            function toggleRowInputs(checkbox, enabled) {
                checkbox.closest('tr').find('input').not('.item_checkbox').prop('disabled', !enabled);
            }

            $('.quantidade_informada').change(function() {
                const quantidade_informada = formataQtdInformada($(this).val());
                //alert(quantidade_informada);
                //const quantidade_informada = $(this).val();
                const key = $(this).data('key');
                const row = $(this).closest('tr');
                const valor_unitario = $.floatRound(row.find('input[name="itens[' + key + '][valor_unitario]"]').val(), 2);
               // alert(valor_unitario);
                const resultado = quantidade_informada * valor_unitario;
                $('.valor_total_entrega_' + key + ' span').text(formatReal(resultado));
                $('.valor_total_entrega_' + key + ' input').val(resultado);

                console.log(quantidade_informada, valor_unitario, resultado);
            });

            $('.quantidade_informada').on('input', function() {
                let currentValue = $(this).val();
                // Remove todos os caracteres que não são dígitos, pontos ou vírgulas
                currentValue = currentValue.replace(/[^0-9.,]/g, '');
                // Substitui vírgulas por pontos
                currentValue = currentValue.replace(/,/g, '.');
                // Remove múltiplos pontos e múltiplas vírgulas
                currentValue = currentValue.replace(/(\.|\,)(?=.*(\.|\,))/g, '');
                // Atualiza o valor do campo
                $(this).val(currentValue);
            });

            function formatReal(valor) {
                return 'R$ ' + valor.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, '$1.');
            }

            function formataQtdInformada(valor) {
                let valorFormatado = valor.replace(/,/g, '.');
                let valorNumerico = parseFloat(valorFormatado);
                return valorNumerico;
            }
        });
</script>
@endpush