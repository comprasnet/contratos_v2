<div style="margin-bottom: 10px">
    <label for="objeto">{{$label}}</label>
    @if(strlen($value)>120)
        <div class="hint-text-label">
            <a href="javascript:void(0);">{{ substr($value,0,$max??200) }} ...</a>
            <div class="br-tooltip" role="tooltip" info="info" place="{{$position??'top'}}">
                {{$value}}
            </div>
        </div>
    @else
        <div>{{$value}}</div>
    @endif
</div>
<style>
    .hint-text-label a{
        text-decoration: none !important;
        color: inherit !important;
    }
    .hint-text-label .br-tooltip{
        max-width: 400px !important;
    }
</style>