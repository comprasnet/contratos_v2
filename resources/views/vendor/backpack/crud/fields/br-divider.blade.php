@php
$field['vertical'] = $field['vertical'] ?? null;
$field['prefix_text'] = $field['prefix_text'] ?? null;
$field['sufix_text'] = $field['sufix_text'] ?? null;
$field['texttooltip_prefix'] = $field['texttooltip_prefix'] ?? null;
$field['texttooltip_sufix'] = $field['texttooltip_sufix'] ?? null;
@endphp

@include('crud::fields.inc.wrapper_start')
@include('crud::fields.inc.translatable_icon')

@if(empty($field['vertical']))
    @if(!empty($field['prefix_text']))
        <div>
            <label>{{ $field['prefix_text'] }}</label>
            <i class="fas fa-info-circle"></i>
            @include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => $field['texttooltip_prefix'] ?? ''])
        </div>
    @endif
    <span class="br-divider my-3"></span>
    @if(!empty($field['sufix_text']))
        <div>
            <label>{{ $field['sufix_text'] }}</label>
            <i class="fas fa-info-circle"></i>
            @include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => $field['texttooltip_sufix'] ?? ''])
        </div>
    @endif
@endif

@if(!empty($field['vertical']))
    <div class="d-flex">
        @if(!empty($field['prefix_text']))
            <div>
                {{ $field['prefix_text'] }}
                <i class="fas fa-info-circle"></i>
                @include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => $field['texttooltip_prefix'] ?? ''])
            </div>
        @endif
        <span class="br-divider vertical mx-3"></span>
        @if(!empty($field['sufix_text']))
            <div>
                {{ $field['sufix_text'] }}
                <i class="fas fa-info-circle"></i>
                @include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => $field['texttooltip_sufix'] ?? ''])
            </div>
        @endif
    </div>
@endif

@include('crud::fields.inc.wrapper_end')