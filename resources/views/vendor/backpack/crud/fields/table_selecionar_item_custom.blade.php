@php

$datatable = !isset($field['datatable']) && empty($field['datatable']) ? true : $field['datatable'];
$field['emptyTable'] = !isset($field['emptyTable']) && empty($field['emptyTable']) ? '' : $field['emptyTable'];
$field['pageLength'] = !isset($field['pageLength']) && empty($field['pageLength']) ? '100' : $field['pageLength'];
$field['label'] = !isset($field['label']) && empty($field['label']) ? 'Selecionar item' : $field['label'];

@endphp
<div class="{{ $field['classArea'] }} table-responsive pt-3 ml-3 mr-3" element="div" bp-field-wrapper="true" bp-field-type="text"  id="{{ $field['idArea'] }}">
    <label>{{ $field['label'] }}</label>
    <table id="{{ $field['idTable'] }}" class="table">
        <thead>
        <tr>
            {{-- <th><input type="checkbox" class="selectAll"> <i class="fas fa-info-circle" title="Ao marcar o todos, somente a página atual será selecionada"></i></th> --}}
            @foreach ($field['column'] as $item)
                @if (is_array($item) && isset($item) && !empty($item))
                    @php
                        $item['class'] = !isset($item['class']) && empty($item['class']) ? '' : $item['class'];
                        $item['style'] = !isset($item['style']) && empty($item['style']) ? '' : $item['style'];
                    @endphp
                    <th class="{{ $item['class'] }}" style="{{ $item['style'] }}">{!! $item['column'] !!}</th>
                @else
                    <th>{!! $item !!}</th>
                @endif
                
            @endforeach
        </tr>
        </thead>
        <tbody>
            @if (isset($field['value']) && !empty($field['value']))
                    @foreach ($field['value'] as $linha)
                    
                    <tr>
                        @foreach ($field['column'] as $key => $item)
                        {{-- @php dd($key, $linha); @endphp --}}
                            <td class ="{{ $key }}">
                                {!! $linha[$key] ?? '' !!}
                            </td>
                        @endforeach

                        
                    </tr>
                    @endforeach
            @endif
        </tbody>
    </table>

{{-- <div class="mb-3">
    <a href="#areaItemAdesao" id="btnInserirItemFornecedor" class="br-button primary large mr-3 menu-item-link-externo br-button-form">Incluir Item</i></a>
</div> --}}
</div>

@push('after_scripts')
<script>
    let languageDt_{{ $field['idTable'] }} = {
                            "emptyTable":     "{{ $field['emptyTable'] }}",
                            "info":           "{{ trans('backpack::crud.info') }}",
                            "infoEmpty":      "{{ trans('backpack::crud.infoEmpty') }}",
                            "infoFiltered":   "{{ trans('backpack::crud.infoFiltered') }}",
                            "infoPostFix":    "{{ trans('backpack::crud.infoPostFix') }}",
                            "thousands":      "{{ trans('backpack::crud.thousands') }}",
                            "lengthMenu":     "{{ trans('backpack::crud.lengthMenu') }}",
                            "loadingRecords": "{{ trans('backpack::crud.loadingRecords') }}",
                            "processing":     "<img src='{{ asset('packages/backpack/base/img/spinner.svg') }}' alt='{{ trans('backpack::crud.processing') }}'>",
                            "search": "_INPUT_",
                            "searchPlaceholder": "{{ trans('backpack::crud.search') }}...",
                            "zeroRecords":    "{{ trans('backpack::crud.zeroRecords') }}",
                            "paginate": {
                                "first":      "{{ trans('backpack::crud.paginate.first') }}",
                                "last":       "{{ trans('backpack::crud.paginate.last') }}",
                                "next":       ">",
                                "previous":   "<"
                            },
                            "aria": {
                                "sortAscending":  "{{ trans('backpack::crud.aria.sortAscending') }}",
                                "sortDescending": "{{ trans('backpack::crud.aria.sortDescending') }}"
                            }
                        }
let datableItem_{{ $field['idTable'] }} = null

function inserirLinhaItem(item = null, inicializar = true) {

    if(inicializar) {
        datableItem_{{ $field['idTable'] }} = $('#{{ $field['idTable'] }}').DataTable({
            language: languageDt_{{ $field['idTable'] }}
        });
        return
    }

    datableItem_{{ $field['idTable'] }}.clear();
    datableItem_{{ $field['idTable'] }}.destroy();
    // if (item == null || item == undefined) {
    //     
    
    // }

    datableItem_{{ $field['idTable'] }} = $('#{{ $field['idTable'] }}').DataTable({
        data: item,
        autoWidth: false,
        responsive: true,
        fixedHeader: true,
        pageLength: {{ $field['pageLength'] }},
        language: languageDt_{{ $field['idTable'] }},
        lengthMenu: [{{ $field['pageLength'] }}],
        order: [[1, 'desc']],
        columns: [
            @php
                foreach ($field['column'] as $key => $item) {

            @endphp

                    { data: '@php  echo $key;  @endphp' },
            @php 
                }
            @endphp
        ]
    });

    $('.dataTables_filter input[type="search"]').css(
     {'width':'680px'}
  );

  $('.dataTables_filter input[type="search"]').attr({'placeholder': 'Para filtrar com mais de um valor, separar por espaço'})
}

function checkTodos(idCampo , state) {
    $(idCampo).prop('checked', state)
}

@if($datatable)

    $(document).ready(function () {
    // caso queira o filtro por coluna, descomentar este trecho

    // $('#table_selecionar_item_adesao_arp tfoot th').each(function () {
    //     var title = $(this).text();
    //     if ( title != '') {
    //         $(this).html(`  <div class="br-input">
    //         <div class="input-group">
    //           <div class="input-icon"><i class="fas fa-search" aria-hidden="true"></i>
    //           </div>
    //           <input  type="text"/>
    //         </div>
    //       </div>`);
    //     }
        
    // });

    // filtrar todos os campos separados por virgula
    $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex) {
            var filter = $("#{{ $field['idTable'] }} input").val();
            filter = filter.split(' ');
            for (var f=0;f<filter.length;f++) {
                for (var d=0;d<aData.length;d++) {
                    if (aData[d].indexOf(f)>-1) {
                        return true;
                    }
                }
            }
        }
    )
    })
@endif

</script>
@endpush