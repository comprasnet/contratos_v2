@php
    $options = [];
    $functionType = 'select';

    if ($field['responsaveis']->whereIn('funcao.descricao', ['Gestor', 'Gestor Substituto'])->count() == 0) {
        $functionType = 'alert';
    }

    foreach ($field['responsaveis'] as $responsavel) {
        $options[] = [
            'value' => "{$responsavel->funcao->descres}-{$responsavel->id}",
            'text' => "{$responsavel->funcao->descricao} - {$responsavel->user->name}",
        ];
    }
@endphp

<div id="selectPrepostosResponsaveis" class="form-group text-left d-none">
    <label>Selecione os responsáveis do contrato que deverão assinar o TRD:</label>
    <select
            id="prepostos_responsaveis_ids"
            name="{{ $field['name'] ?? '' }}"
            class="form-control"
            form="mainForm"
            multiple=""
    >
        @foreach($options as $option)
            <option value="{{$option['value']}}">{{$option['text']}}</option>
        @endforeach
    </select>
    <div class="error"></div>
</div>

<script>
    function selectResponsaveis(action) {
        @if($functionType === 'alert')
        swal({
            title: 'Atenção',
            text: 'Para gerar o TRD o contrato deve possuir um Gestor e/ou seu substituto.',
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'Confirmar',
                    className: 'br-button primary mr-3',
                }
            },
        });
        @else
        var selectHtml = $('#selectPrepostosResponsaveis').prop('outerHTML')
            .replace('d-none', '')
            .replace('prepostos_responsaveis_ids', 'modal_prepostos_responsaveis_ids')
            .replace('select_prepostos_responsaveis', 'modal_select_prepostos_responsaveis');

        swal({
            title: 'Signatários',
            content: {
                element: "div",
                attributes: {
                    innerHTML: selectHtml
                },
            },
            buttons: {
                cancel: {
                    text: 'Cancelar',
                    value: null,
                    visible: true,
                    className: 'br-button secondary mr-3',
                    closeModal: true,
                },
                confirm: {
                    text: 'Prosseguir',
                    value: true,
                    visible: true,
                    className: 'br-button primary mr-3',
                    closeModal: false,
                }
            },
        })

        $('.swal-modal .swal-button--confirm').on('click', () => {
            const gestoresSelecionados = $('#modal_prepostos_responsaveis_ids').val().filter((itens) => {
                return itens.indexOf('GESTOR') !== -1
            });

            $('#selectPrepostosResponsaveis .error').empty();

            if (gestoresSelecionados.length === 0) {
                $('#selectPrepostosResponsaveis .error')
                    .append('<p class="invalid-feedback d-block mb-0">Selecione um Gestor e/ou seu substituto</p>');
            }

            if (gestoresSelecionados.length === 0) {
                $('.swal-modal .swal-button--loading').removeClass('swal-button--loading');
            } else {
                action();
            }
        })

        setTimeout(() => {
            $('#modal_prepostos_responsaveis_ids').select2()
        }, 200)
        @endif
    }
</script>

<style>
    .select2-container {
        z-index: 10000000;
    }
    .br-scrim-util.foco {
        z-index: 100000000;
    }
</style>
