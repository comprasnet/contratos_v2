@php
$field['label_instrucao_tabela'] = $field['label_instrucao_tabela'] ?? 'Selecionar item';
$field['order_table'] = $field['order_table'] ?? "[1,'desc']";
@endphp
<div class="{{ $field['classArea'] }} table-responsive" element="div" bp-field-wrapper="true" bp-field-type="text"  id="{{ $field['idArea'] }}">
    <label>{!! $field['label_instrucao_tabela'] !!}</label>
    <table id="{{ $field['idTable'] }}" class="table">
        <thead>
        <tr>
            {{-- <th><input type="checkbox" class="selectAll"> <i class="fas fa-info-circle" title="Ao marcar o todos, somente a página atual será selecionada"></i></th> --}}
            @foreach ($field['column'] as $item)
                <th>{!! $item !!}</th>
            @endforeach
        </tr>
        </thead>
        @if (isset($field['value']) && !empty($field['value']))
            <tbody>
                @foreach ($field['value'] as $linha)
                <tr>
                    @foreach ($field['column'] as $key => $item)
                        <td>
                            {!! $linha[$key] !!}
                        </td>
                    @endforeach


                </tr>
                @endforeach
            </tbody>
        @endif
    </table>

{{-- <div class="mb-3">
    <a href="#areaItemAdesao" id="btnInserirItemFornecedor" class="br-button primary large mr-3 menu-item-link-externo br-button-form">Incluir Item</i></a>
</div> --}}
</div>

@push('after_scripts')
<script>
    let languageDt = {
                            "emptyTable":     "{{ $field['emptyTable'] }}",
                            "info":           "{{ trans('backpack::crud.info') }}",
                            "infoEmpty":      "{{ trans('backpack::crud.infoEmpty') }}",
                            "infoFiltered":   "{{ trans('backpack::crud.infoFiltered') }}",
                            "infoPostFix":    "{{ trans('backpack::crud.infoPostFix') }}",
                            "thousands":      "{{ trans('backpack::crud.thousands') }}",
                            "lengthMenu":     "{{ trans('backpack::crud.lengthMenu') }}",
                            "loadingRecords": "{{ trans('backpack::crud.loadingRecords') }}",
                            "processing":     "<img src='{{ asset('packages/backpack/base/img/spinner.svg') }}' alt='{{ trans('backpack::crud.processing') }}'>",
                            "search": "_INPUT_",
                            "searchPlaceholder": "{{ trans('backpack::crud.search') }}...",
                            "zeroRecords":    "{{ trans('backpack::crud.zeroRecords') }}",
                            "paginate": {
                                "first":      "{{ trans('backpack::crud.paginate.first') }}",
                                "last":       "{{ trans('backpack::crud.paginate.last') }}",
                                "next":       ">",
                                "previous":   "<"
                            },
                            "aria": {
                                "sortAscending":  "{{ trans('backpack::crud.aria.sortAscending') }}",
                                "sortDescending": "{{ trans('backpack::crud.aria.sortDescending') }}"
                            }
                        }
let datableItem = null

function inserirLinhaItem(item = null, inicializar = true) {

    if (!$.fn.DataTable.isDataTable('#{{ $field['idTable'] }}')) {
        datableItem = $('#{{ $field['idTable'] }}').DataTable({
            language: languageDt,
            pageLength: {{ $field['pageLength'] }},
            lengthMenu: [{{ $field['pageLength'] }}]
        })

        $('.dataTables_filter input[type="search"]').css({'width':'580px'});
        $('.dataTables_filter input[type="search"]').attr({'placeholder': 'Para filtrar com mais de um valor, separar por espaço'})
        $('div.dataTables_wrapper div.dataTables_length select').css("width", "70px")

        return
    }

    datableItem.clear();
    datableItem.destroy();
    // if (item == null || item == undefined) {
    //

    // }

    datableItem = $('#{{ $field['idTable'] }}').DataTable({
        data: item,
        autoWidth: false,
        responsive: true,
        fixedHeader: true,
        pageLength: {{ $field['pageLength'] }},
        language: languageDt,
        lengthMenu: [{{ $field['pageLength'] }}],
        order: [{!! $field['order_table'] !!}],
        columns: [
                @foreach ($field['column'] as $key => $item)
            {
                data: '@php echo $key; @endphp',
                render: function(data, type, row) {
                    @if ($key === 'maximoadesao')
                        return data + '<i class="fas fa-info-circle font-weight-bold font-size-22" title="Refere-se a quantidade disponível por item, independente da classificação dos fornecedores do item"></i>';
                    @else
                        return data;
                    @endif
                }
            },
            @endforeach
        ]
    });

    $('.dataTables_filter input[type="search"]').css({'width':'680px'});
    $('.dataTables_filter input[type="search"]').attr({'placeholder': 'Para filtrar com mais de um valor, separar por espaço'})
    $('div.dataTables_wrapper div.dataTables_length select').css("width", "70px")
}

function checkTodos(idCampo , state) {
    $(idCampo).prop('checked', state)
}

{{--$(document).ready(function () {--}}
{{--// caso queira o filtro por coluna, descomentar este trecho--}}

{{--// $('#table_selecionar_item_adesao_arp tfoot th').each(function () {--}}
{{--//     var title = $(this).text();--}}
{{--//     if ( title != '') {--}}
{{--//         $(this).html(`  <div class="br-input">--}}
{{--//         <div class="input-group">--}}
{{--//           <div class="input-icon"><i class="fas fa-search" aria-hidden="true"></i>--}}
{{--//           </div>--}}
{{--//           <input  type="text"/>--}}
{{--//         </div>--}}
{{--//       </div>`);--}}
{{--//     }--}}

{{--// });--}}

{{--// filtrar todos os campos separados por virgula--}}
{{--$.fn.dataTableExt.afnFiltering.push(--}}
{{--    function(oSettings, aData, iDataIndex) {--}}
{{--        var filter = $("#{{ $field['idTable'] }}_filter input").val();--}}
{{--        filter = filter.split(' ');--}}
{{--        for (var f=0;f<filter.length;f++) {--}}
{{--            for (var d=0;d<aData.length;d++) {--}}
{{--                if (aData[d].indexOf(f)>-1) {--}}
{{--                    return true;--}}
{{--                }--}}
{{--            }--}}
{{--        }--}}
{{--     }--}}
{{--  )--}}
{{--})--}}

</script>
@endpush