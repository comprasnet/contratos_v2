<div id="form-criarlocal" class="d-none row">
    @include('crud::fields.text', [
        'field' => [
            'name' => 'descricao',
            'type' => 'textarea',
            'label' => 'Descrição',
            'required' => true,
            'attributes' => ['id' => 'modal_descricao']
        ]
    ])

    @include('crud::fields.cep', [
        'field' => [
            'name' => 'cep',
            'type' => 'text',
            'label' => 'CEP',
            'required' => true,
            'attributes' => ['id' => 'modal_cep'],
            'wrapperAttributes' => ['class' => 'form-group col-md-4']
        ]
    ])
    <div class="loadingSearchCep fa-3x col-md-1" style="display: none">
        <i class="fas fa-spinner fa-pulse"></i>
    </div>
    <div class="offset-md-7"></div>

    @include('crud::fields.text', [
        'field' => [
            'name' => 'logradouro',
            'type' => 'text',
            'label' => 'Logradouro/Nome',
            'required' => true,
            'attributes' => ['id' => 'modal_logradouro', 'readonly' => 'readonly'],
            'wrapperAttributes' => ['class' => 'form-group col-md-9']
        ]
    ])

    @include('crud::fields.text', [
        'field' => [
            'name' => 'numero_endereco',
            'type' => 'text',
            'label' => 'Número',
            'attributes' => ['id' => 'modal_numero_endereco'],
            'wrapperAttributes' => ['class' => 'form-group col-md-3']
        ]
    ])

    @include('crud::fields.text', [
        'field' => [
            'name' => 'bairro',
            'type' => 'text',
            'label' => 'Bairro/Distrito',
            'required' => true,
            'attributes' => ['id' => 'modal_bairro', 'readonly' => 'readonly'],
            'wrapperAttributes' => ['class' => 'form-group col-md-4']
        ],
    ])

    @include('crud::fields.select2_from_array', [
        'field' => [
            'name' => 'municipios_id',
            'type' => 'select2_from_array',
            'label' => 'Localidade/UF',
            'required' => true,
            'value' => null,
            'options' => [],
            'attributes' => ['id' => 'modal_municipios_id', 'readonly' => 'readonly'],
            'wrapperAttributes' => ['class' => 'form-group col-md-4']
        ]
    ])

    @include('crud::fields.text', [
        'field' => [
            'name' => 'complemento',
            'type' => 'text',
            'label' => 'Complemento',
            'attributes' => ['id' => 'modal_complemento'],
            'wrapperAttributes' => ['class' => 'form-group col-md-4']
        ]
    ])
</div>

<div class="d-flex justify-content-center align-items-end form-group col-md-2">
    <button id="btn-criarlocal" class="br-button primary" type="button">
        <i class="fas fa-plus"></i> &nbsp;
        <span data-value="0" data-valuecustom="0" data-namecustom="rascunho">Criar Local</span>
    </button>
</div>

@push('after_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            const btnCrialLocal = $('#btn-criarlocal');
            const formLocalExecucao = $('#form-criarlocal')
                .prop('outerHTML')
                .replace('d-none', '')
                .replaceAll('modal_', '');

            const contratoId = {{ request()->contrato_id }};

            btnCrialLocal.on('click', function () {
                swal({
                    title:  'Criação de Local de Execução',
                    content: {
                        element: "div",
                        attributes: {
                            innerHTML: formLocalExecucao
                        },
                    },
                    className: "swal-criarlocal",
                    buttons: {
                        cancel: {
                            text: 'Cancelar',
                            value: null,
                            visible: true,
                            className: 'br-button secondary mr-3',
                            closeModal: true,
                        },
                        confirm: {
                            text: 'Confirmar',
                            className: 'br-button primary mr-3',
                            closeModal: false
                        },
                    },
                })
            })

            let buscaCep = true;

            $(document).on('keyup', '#cep', () => {
                if ($('#cep').val().length === 9) {
                    if (buscaCep) {
                        buscaCep = false;
                        $.ajax({
                            url: `/busca/cep?cep=${$('#cep').val()}`,
                            type: "GET",
                            dataType: 'json',
                            beforeSend: function () {
                                $('.loadingSearchCep').fadeIn(500)
                            },
                            success: function (data) {
                                $('#logradouro').val(data.endereco);
                                $('#bairro').val(data.bairro);

                                retornaCidadeEstado(`${data.cidade} - ${data.uf}`)

                                if (data.bairro === "") {
                                    $('#bairro').attr('readonly', false)
                                    $('#logradouro').attr('readonly', false)
                                } else {
                                    $('#bairro').attr('readonly', true)
                                    $('#logradouro').attr('readonly', true)
                                }
                            },
                            error: function (data) {
                                $('#cep').val('');
                                exibirAlertaNoty('error-custom', data.responseJSON.error)
                            },
                            complete: function () {
                                $('.loadingSearchCep').fadeOut(500)
                            }
                        })
                    }
                } else {
                    buscaCep = true
                }
            })

            $(document).on('change', '#cep', () => {
                buscaCep = true
            })

            $(document).on('click', '.swal-criarlocal .swal-button--confirm', () => {
                const fieldsRequired = [
                    $('#descricao'),
                    $('#cep'),
                    $('#logradouro'),
                    $('#bairro'),
                    $('#municipios_id'),
                ];
                const fields = [
                    ...fieldsRequired,
                    $('#numero_endereco'),
                    $('#complemento'),
                ];

                const data = {};
                fields.forEach(field => {
                    // Usamos o atributo 'name' como chave e o valor do campo como valor
                    data[field.attr('name')] = field.val();
                });

                $.ajax({
                    url: `/contrato/${contratoId}/local-execucao`,
                    type: 'POST',
                    data,
                    success: function (data) {
                        exibirAlertaNoty('success-custom', 'Local de Execução criado com sucesso');
                        $('#contrato_local_execucao_ids').append(new Option(data.descricao, data.id));
                        swal.close();
                    },
                    error: function (data) {
                        if (data.status === 422) {
                            $('.swal-modal .swal-button--loading').removeClass('swal-button--loading');
                            const errors = data.responseJSON.errors;
                            let alertText = ''
                            Object.keys(errors).forEach((fieldName) => {
                                alertText += errors[fieldName][0] + '<br>'
                            })
                            exibirAlertaNoty('error-custom', alertText);
                        } else if (data.status >= 500) {
                            exibirAlertaNoty('error-custom', 'Erro inesperado ao processar o formulário');
                        }
                    }
                })
            });
        })

        function retornaCidadeEstado(texto) {
            const municipios_id = $("#municipios_id");
            console.log(municipios_id)
            $.ajax({
                url: '/busca/municipio',
                method: 'GET',
                data: {q: texto},
                success: function (response) {
                    municipios_id.empty()
                    municipios_id.append(
                        new Option(response.data[0].nome, response.data[0].id, true, true)
                    );
                    $(municipios_id.next()).find('.select2-selection__placeholder').text(response.data[0].nome);
                },
                error: function () {
                    console.error('Erro ao obter o ID por texto.');
                }
            });
        }
    </script>
@endpush

<style>
    .swal-criarlocal {
        width: 900px;
        max-width: 80%;
    }

    #form-criarlocal .form-group {
        text-align: left;
    }

    #form-criarlocal .select2-container--bootstrap .select2-selection--single {
        height: 42px;
        margin-top: 3px;
        border: 1px solid #888 !important;
    }
</style>
