<div class="br-upload">
    <label class="upload-label" for="single-file">
        <a class="text-tip" href="javascript:void(0);">
            Envio de arquivo da Ata
            <label style="color: red">*</label>
        </a>
        <div class="br-tooltip" role="tooltip" info="info" place="top">
            <span class="text" role="tooltip">Para substituir o arquivo, selecione um novo arquivo</span>
            <span class="subtext"></span>
        </div>
    </label>
    <input class="upload-input" onchange="$('.existing-file').hide()" id="single-file" name="arquivo" type="file" accept=".xlsx,.xls,.ppt, .pptx, .pdf"/>

{{--    <input class="upload-input" id="single-file" name="arquivo" type="file" accept=".xlsx,.xls,.ppt, .pptx, .pdf" />--}}
    <div class="upload-list"></div>
    <div id="fileError" style="color: red; display: none; margin-top: 10px;"></div>
</div>

@if (isset($arquivo))
    <div class="existing-file" style="max-width: 550px; margin-top: 10px;">
        <a target="_blank" href="{{ url('storage/' . ($arquivo->url)) }}">
            {{ $arquivo->tipo->nome }}
        </a>
    </div>
@endif

<style>
    .upload-button {
        max-width: none !important;
    }

    .arquivo-hidden {
        display: none;
    }

    .text-tip {
        text-decoration: none !important;
        color: var(--color);
        font-size: var(--font-size-scale-base);
        font-weight: var(--font-weight-semi-bold);
        line-height: var(--font-line-height-medium);
        margin-bottom: var(--spacing-scale-half);
    }

    .hidden-upload {
        display: none !important;
    }
</style>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        const singleFileInput = document.getElementById('single-file');
        const fileError = document.getElementById('fileError');
        const uploadList = document.querySelector('.upload-list'); // Container da lista de uploads

        singleFileInput.addEventListener('change', function (event) {
            const maxSize = 30 * 1024 * 1024; // 30MB em bytes
            const file = event.target.files[0]; // Obtém o arquivo selecionado

            if (file && file.size > maxSize) {
                const fileSizeInMB = (file.size / (1024 * 1024)).toFixed(2);

                exibirAlertaNoty('warning-custom', `O arquivo selecionado (${fileSizeInMB} MB) excede o limite de 30 MB.`);

                // Exibe a mensagem de erro
                // fileError.innerText = `O arquivo selecionado (${fileSizeInMB} MB) excede o limite de 30 MB.`;
                fileError.style.display = 'block';

                // Limpa o valor do input
                this.value = '';

                // Remove visualmente o arquivo da interface
                if (uploadList) {
                    uploadList.innerHTML = ''; // Apaga o conteúdo visível
                }

                uploadList.classList.add('hidden-upload');
            } else if (!file) {
                fileError.style.display = 'none';
                fileError.innerText = '';
            } else {
                fileError.style.display = 'none';
                uploadList.classList.remove('hidden-upload');
            }
        });
    });
</script>
