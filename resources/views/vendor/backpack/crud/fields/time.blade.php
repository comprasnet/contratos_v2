<!-- html5 time input -->
@include('crud::fields.inc.wrapper_start')
<div class="br-datetimepicker" data-mode="single" data-type="time">
    <div class="br-input has-icon">
        <label>{!! $field['label'] !!}</label>
        @include('crud::fields.inc.translatable_icon')
        <input
                type="time"
                name="{{ $field['name'] }}"
                value="{{ old_empty_or_null($field['name'], '') ??  $field['value'] ?? $field['default'] ?? '' }}"
                data-input="data-input"
                placeholder="hh:mm"
                @include('crud::fields.inc.attributes')
        >

        <button class="br-button circle small" type="button" aria-label="Abrir Timepicker" data-toggle="data-toggle" id="timepicker-input-btn"><i class="fas fa-clock" aria-hidden="true"></i>
        </button>

        {{-- HINT --}}
        @if (isset($field['hint']))
            <p class="help-block">{!! $field['hint'] !!}</p>
        @endif
    </div>
</div>
@include('crud::fields.inc.wrapper_end')
