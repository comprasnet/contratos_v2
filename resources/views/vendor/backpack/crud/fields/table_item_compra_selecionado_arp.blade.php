<div class="col-md-12 pt-5 table-responsive {{ $field['classArea'] }}" element="div" bp-field-wrapper="true" bp-field-type="text"
@php 
if (isset($field['idArea']) && !empty($field['idArea'])) {
@endphp 
id="{{ $field['idArea'] }}"
@php } @endphp
>
    <label>Item Selecionado</label>
    <table id="table_item_compra_selecionado_arp" class="table">
        <thead>
            <th>Remover</th>
            <th>CNPJ</th>
            <th>Fornecedor (Classificação)</th>
            <th>N. Item</th>
            <th>Tipo</th>
            <th>Código</th>
            <th>Descrição</th>
            <th>Quantidade registrada</th>
            <th>Valor Unit.</th>
            <th>Valor total</th>
            <th>Qtd. limite para adesão</th>
            <th>Qtd. limite informada na compra</th>
            <th>Aceita adesão</th>
        </thead>
        <tbody id="tbody_item_compra_selecionado_arp"> 
            @if (!isset($field['value']) && empty($field['value']))
                {{-- <tr>
                    <td colspan="9" style="text-align: center;">Nenhum item selecionado</td>
                </tr> --}}
            @else
                @foreach ($field['value'] as $linha)
                    {!! $linha !!}
                @endforeach
            @endif
        </tbody>
    </table>
</div>