@php
    $contrato_id = \Route::current()->parameter('contrato_id');
    $anexos = old('anexos') ?? [];

    if (empty($anexos) && !empty($field['anexos'])) {
        foreach ($field['anexos'] as $anexo) {
            $anexos[] = [
                'id' => $anexo['id'],
                'nome_arquivo_anexo' => $anexo['nome'],
                'url_arquivo_anexo' => $anexo['url'],
                'descricao_arquivo_anexo' => $anexo['descricao'],
                'remove_file' => 0,
            ];
        }
    }
@endphp
<div id="anexos" class="card-body row pb-0">
    @include(
        'crud::fields.upload_custom',
        [
            'field' => [
                'type'      => 'upload_custom',
                'name'      => 'upload_arquivo_anexo',
                'label'     => $field['label'],
                'accept'    => $field['accept'] ?? 'application/pdf',
                'attributes' => ['id' => 'upload_arquivo_anexo'],
                'wrapperAttributes' => ['class' => 'col-md-4 pt-n1'],
            ],
        ]
    )

    <div class="form-group col-md-8">
        <div class="br-input">
            <label>Descrição</label>
            <input id="descricao_arquivo_anexo" type="text" value="">
        </div>
    </div>

    <div class="form-group col-12">
        <div class="d-flex justify-content-end">
            <button id="uploadAnexo" type="button" class="br-button primary btn-lg" disabled>
                <i class="fas fa-plus"></i> Adicionar Anexo
            </button>
        </div>
    </div>
</div>
<div class="card-body row pt-0">
    <div class="col-sm-12">
        <table class="table dataTable">
            <thead>
                <tr>
                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 375px;">Nome Anexo</th>
                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 244px;">Descrição Anexo</th>
                    <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 248px;">Ação</th>
                </tr>
            </thead>
            <tbody id="table-anexos">
                @empty($anexos)
                    <tr class="row-info"><td class="text-center" colspan="3">Nenhum anexo adicionado</td></tr>
                @else
                    @foreach($anexos as $index => $anexo)
                        @if($anexo['remove_file'])
                        <tr class="row-anexo d-none">
                        @else
                        <tr class="row-anexo">
                        @endif
                            <td>
                                <input class="d-none" type="text" name="anexos[{{$index}}][id]" value="{{$anexo['id'] ?? null }}"  readonly="readonly">
                                <p class="nome_arquivo_anexo_text">{{ $anexo['nome_arquivo_anexo'] }}</p>
                                <input class="d-none nome_arquivo_anexo_value" type="text" name="anexos[{{$index}}][nome_arquivo_anexo]" value="{{$anexo['nome_arquivo_anexo']}}"  readonly="readonly">
                                <input class="d-none url_arquivo_anexo_value" type="text" name="anexos[{{$index}}][url_arquivo_anexo]" value="{{$anexo['url_arquivo_anexo']}}"  readonly="readonly">
                            </td>
                            <td>
                                <p class="descricao_arquivo_anexo_text">{{ $anexo['descricao_arquivo_anexo'] }}</p>
                                <input class="d-none descricao_arquivo_anexo_value" type="text" name="anexos[{{$index}}][descricao_arquivo_anexo]" value="{{$anexo['descricao_arquivo_anexo']}}"  readonly="readonly">
                            </td>
                            <td>
                                <input type="text" class="d-none remove_file_value" name="anexos[{{$index}}][remove_file]" value="{{$anexo['remove_file']}}"  readonly="readonly">
                                <button type="button" class="removeAnexo br-button circle secondary mr-3"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                    @endforeach
                @endempty
            </tbody>


            <tfoot class="d-none">
                <tr class="row-anexo-clone">
                    <td>
                        <input class="d-none" type="text" data-name="arquivoable_id"  readonly="readonly">
                        <p class="nome_arquivo_anexo_text">Arquivo.pdf</p>
                        <input class="d-none nome_arquivo_anexo_value" type="text" data-name="nome_arquivo_anexo"  readonly="readonly">
                        <input class="d-none url_arquivo_anexo_value" type="text" data-name="url_arquivo_anexo"  readonly="readonly">
                    </td>
                    <td>
                        <p class="descricao_arquivo_anexo_text">Descriçao lorem teste teste</p>
                        <input class="d-none descricao_arquivo_anexo_value" type="text" data-name="descricao_arquivo_anexo"  readonly="readonly">
                    </td>
                    <td>
                        <input type="text" class="d-none remove_file_value" value="0" data-name="remove_file"  readonly="readonly">
                        <button type="button" class="removeAnexo br-button circle secondary mr-3"><i class="fas fa-trash"></i></button>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>


@push('after_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            let contratoId = {{ $contrato_id }}

            $('#addAnexos').on('click', function () {
                $('#anexos').slideToggle();
            })

            $('#upload_arquivo_anexo').on('change', function () {
                if (!!$(this).val()) {
                    $('#uploadAnexo').removeAttr('disabled')
                }
            })

            $(document).on('click', '.removeAnexo', function () {
                $(this).closest('.row-anexo').hide();
                const newFile = $(this).siblings('.remove_file_value').val(1);
            })

            $('#uploadAnexo').on('click', function () {
                const descricaoArquivoAnexo = $('#descricao_arquivo_anexo').val();
                const uploadArquivoAnexo = $('#upload_arquivo_anexo')[0].files[0];
                if (!uploadArquivoAnexo) {
                    swal({
                        title: 'Atenção',
                        text: 'Insira um arquivo de anexo',
                        type: 'warning',
                        buttons: {
                            confirm: {
                                text: 'OK',
                                className: 'br-button primary mr-3',
                            }
                        },
                    })
                    $('#uploadAnexo').attr('disabled', 'disabled')
                    return false;
                }

                let formData = new FormData();
                formData.append('upload_arquivo_anexo', uploadArquivoAnexo);

                $.ajax({
                    url: '/autorizacaoexecucao/' + contratoId +'/anexo',
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        $('.row-info').remove();

                        const currentIndex = $('#table-anexos tr').length;
                        const newRowAnexo = $('.row-anexo-clone').clone().removeClass('row-anexo-clone').addClass('row-anexo');
                        $(newRowAnexo).find('input').each(function (i, element) {
                            const name = $(element).data('name');
                            $(element).attr('name', 'anexos[' + currentIndex + '][' + name + ']');
                        })
                        $(newRowAnexo).find('.nome_arquivo_anexo_text').text(response.nome_arquivo_anexo);
                        $(newRowAnexo).find('.nome_arquivo_anexo_value').val(response.nome_arquivo_anexo);
                        $(newRowAnexo).find('.url_arquivo_anexo_value').val(response.url_arquivo_anexo);
                        $(newRowAnexo).find('.descricao_arquivo_anexo_text').text(descricaoArquivoAnexo);
                        $(newRowAnexo).find('.descricao_arquivo_anexo_value').val(descricaoArquivoAnexo);
                        $(newRowAnexo).appendTo('#table-anexos');

                        // zerar campos
                        $('#descricao_arquivo_anexo').val('');
                        $('.upload-list').find('button').click();
                    },
                    error: function (xhr, status, error) {
                        if (xhr.status === 422) {
                            swal({
                                title: 'Atenção',
                                text: xhr.responseJSON.errors.upload_arquivo_anexo[0],
                                type: 'warning',
                                buttons: {
                                    confirm: {
                                        text: 'OK',
                                        className: 'br-button primary mr-3',
                                    }
                                },
                            })
                        }
                    }
                });
            })
        })
    </script>
@endpush

