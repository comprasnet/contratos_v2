@include('crud::fields.text', $field)

@push('crud_fields_scripts')
    <script type="text/javascript">
        $(document).on('click', 'input[name={{ $field['name'] }}]', function() {
            $('input[name={{ $field['name'] }}]').mask("99999-999");
        })
    </script>
@endpush
