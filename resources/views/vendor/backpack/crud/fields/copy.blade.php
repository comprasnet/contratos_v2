<button class="br-button circle primary" type="button" onclick="copyToClipboard(`{{ $text }}`)">
    <i class="fas fa-copy" aria-hidden="true"></i>
</button>
@include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' =>  $textTooltip ])