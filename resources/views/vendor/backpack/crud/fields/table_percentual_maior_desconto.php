<div class="col-md-12 pt-5" element="div" bp-field-wrapper="true" bp-field-type="text" style="display : none;" id="areaDetalheMaiorDesconto">
    <label onclick="recolherAreaDetalheInformacao()">Detalhes sobre o percentual do maior desconto <i class="las la-angle-double-up"></i></label>
    <table id="table_percentual_maior_desconto" class="table">
    <thead>
        <th>Número</th>
        <th>Descrição</th>
        <th>% de Desconto</th>
        <th>Valor Unitário</th>
        <th>Valor Unitário com Desconto</th>
        <th>Valor Total</th>
        <th>Valor Total com Desconto</th>
    </thead>
    <tbody id="tbody_percentual_maior_desconto"> </tbody>
    </table>
</div>
