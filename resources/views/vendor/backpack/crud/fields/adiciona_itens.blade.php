@php

    $contrato_id = \Route::current()->parameter('contrato_id');
    $autorizacaoexecucao_id = \Route::current()->parameter('autorizacaoexecucao_id') ?? -1;

    $itens = old_empty_or_null('autorizacaoexecucaoItens', '') ?? $fields['autorizacaoexecucaoItens']['value'] ?? [];
    $itens = json_encode($itens);

    $displayNone = $field['hidden'] ? 'd-none' : '';

    $isPreposto = session()->get('fornecedor_id') ? 1 : 0;
@endphp


<div class="form-group col-md-6 {{$displayNone}}">
    <div class="scrimutilexemplo">
        <button id="btn-additem" class="br-button primary" type="button">
            <!-- <span class="la la-save" role="presentation" aria-hidden="true"></span> -->
            <i class="fas fa-plus"></i> &nbsp;
            <span data-value="0" data-valuecustom="0" data-namecustom="rascunho">Adicionar Item</span>
        </button>
    </div>
</div>
<div class="col-md-12 table-responsive {{$displayNone}}">
    <table id="table-itens" class="table data-table no-footer dataTable dtr-inline collapsed display nowrap">
    </table>
</div>


<div class="br-scrim-util foco" id="scrimutilexample" data-scrim="true">
    <div class="br-modal">
        <div class="br-modal-header">
            Adicionar Items
        </div>

        <div class="br-modal-body">
            <div class="form-group">
                <select id="contratoitem_id" class="form-control" multiple>
                    <option value="">Todos</option>
                    @foreach ($field['options'] as $item)
                        <option value="{{$item['id']}}"> {{ $item['tipo']['descres'] }} | {{$item['numero_item_compra']}} | {{ $item['item']['descricao'] }} </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="br-modal-footer justify-content-center">
            <button class="br-button secondary" type="button" id="scrimfechar" data-dismiss="scrimexample">Cancelar
            </button>
            <button class="br-button primary mt-3 mt-sm-0 ml-sm-3" id="incluir-item-btn" type="button">Incluir
            </button>
        </div>
    </div>
</div>

<div class="tooltip">
    <i class="fas fa-info-circle"></i>
    <div
        class="br-tooltip text-justify flex-column"
        style="max-width: none; position: absolute; inset: auto auto 0px 0px; transform: translate(1148.89px, -25.5556px); display: unset; z-index: -1; visibility: hidden;"
        role="tooltip"
        info="info"
        place="left"
    >
        <div id="tooltip-quantidade-contratada" class="d-flex flex-wrap align-items-center subtext">
            <b>Qtde. contratada: </b> <span class="value ml-2 mb-0">0</span>
        </div>
        <hr class="m-0 p-0">
        <div id="tooltip-quantidade-solicitada" class="d-flex flex-wrap align-items-center subtext">
            <b>Qtde. solicitada (OS/F Atual): </b> <span class="value ml-2 mb-0">0</span>
        </div>
        <div id="tooltip-quantidade-consumida" class="d-flex flex-wrap align-items-center subtext">
            <b>Qtde. consumida (OS/Fs anteriores): </b> <span class="value ml-2 mb-0">0</span>
        </div>
        <div id="tooltip-quantidade-restante" class="d-flex flex-wrap align-items-center subtext">
            <b>Qtde. disponível: </b> <span class="value ml-2 mb-0">0</span>
        </div>
    </div>
</div>
<style>
    .tooltip {
        display: none;
    }
    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }

    .repeatable-group label {
        display: none !important;
    }

    input[disabled], select[disabled],
    .disable-item {
        border: none;
        background: none !important;
        cursor: default !important;
    }

    .unidade_medida_id label {
        display: inline-block !important;
    }
    .swal-footer {
        text-align: center;
    }
</style>
@push('after_scripts')
    <script type="text/javascript">
        let contratoId = {{ $contrato_id }};
        let autorizacaoexecucao_id = {{ $autorizacaoexecucao_id }};
        let itens = {!! $itens !!};
        const isPreposto = {{ $isPreposto }}
        $(document).ready(function(){

            const datatable = autorizacaoExecucao(contratoId, autorizacaoexecucao_id);
            const selectorsData = $('#data_vigencia_inicio, #data_vigencia_fim').parent();
            const rowsTable = $('#table-itens').find('tbody>tr');
            const isPaginaAlterar = typeof checklist !== 'undefined';

            handleDisabledBtnAdditem();
            if (isPaginaAlterar) {
                checkedAndDisabledSiblings(checklist[2].checkbox)
            }

            selectorsData.on('change', function (e) {
                const rowsTable = $('#table-itens').find('tbody>tr');

                if (isPaginaAlterar) {
                    const dataFim = $(this).val().split('/').reverse().join('-');

                    const repeatElementFiltered = $(datatable.getRepeatElement()).filter(function(i, element) {
                        const dataVigenciaInicio = $(element).find('.periodo_vigencia_inicio').val()

                        return new Date(dataVigenciaInicio) > new Date(dataFim)
                    })

                    if (repeatElementFiltered.length > 0) {
                        swal({
                            title: 'Atenção',
                            text: 'Alterar a vigência da OSF acarretará supressão de itens já inseridos',
                            type: 'warning',
                            buttons: {
                                cancel: {
                                    text: 'Cancelar',
                                    value: null,
                                    visible: true,
                                    className: 'br-button secondary mr-3',
                                    closeModal: true,
                                },
                                confirm: {
                                    text: 'Confirmar',
                                    className: 'br-button primary mr-3',
                                },
                            },
                        }).then((result) => {
                            if (result) {
                                checklist[2].checkbox.val(1)
                                checklist[2].function.call(this, checklist[2].field)
                                checkedAndDisabledSiblings(checklist[2].checkbox)

                                $(repeatElementFiltered).each(function (i, element) {
                                    const row = $(rowsTable).filter((j, row) => $(row).data('uid') === $(element).data('uid'));
                                    datatable.removeRow(row)
                                })

                            } else {
                                $(this).val('')
                            }
                        })
                    }
                } else {

                    if (!checkDataInicioMenorIgualFim()) {
                        swal({
                            title: 'Atenção',
                            text: 'A data da OS/F de vigência início deve ser menor ou igual a data vigência fim',
                            buttons: {
                                confirm: {
                                    text: 'Confirmar',
                                    className: 'br-button primary mr-3',
                                }
                            },
                        })

                        $(this).val('')
                    }

                    //retificação e criação
                    datatable.removeAllRows(rowsTable);
                    handleDisabledBtnAdditem();
                }
            })

            function checkedAndDisabledSiblings(element) {
                if (checklist[0].checkbox.val() == 1 && checklist[2].checkbox.val() == 1) {
                    element.siblings().prop('checked', true).attr('disabled', true)
                }
            }

            function handleDisabledBtnAdditem() {
                if (checkDataPreenchida() || isPaginaAlterar) {
                    $('#btn-additem').removeAttr('disabled');
                } else {
                    $('#btn-additem').attr('disabled', 'disabled');
                }
            }

            let usuarioAlertado = false;

            selectorsData.on('click', function (e) {
                if (usuarioAlertado) return;
                if (datatable.getRepeatElement().length === 0) return;

                if (checkDataPreenchida()) {
                    swal({
                        title: 'Atenção',
                        text: 'Alterar a vigência da OSF acarretará exclusão de itens já inseridos',
                        type: 'warning',
                        buttons: {
                            confirm: {
                                text: 'Ciente',
                                className: 'br-button primary mr-3',
                            }
                        },
                    }).then((result) => {
                        if (result) {
                            usuarioAlertado = true;
                        }
                    });
                }
            })


            function checkDataPreenchida() {
                const dataInicio = $('#data_vigencia_inicio').val();
                const dataFim = $('#data_vigencia_fim').val();

                return !!dataInicio && !!dataFim
            }

            function checkDataInicioMenorIgualFim() {
                if (checkDataPreenchida()) {
                    const dataInicio = $('#data_vigencia_inicio').val().split('/').reverse().join('-');
                    const dataFim = $('#data_vigencia_fim').val().split('/').reverse().join('-');

                    return new Date(dataInicio) <= new Date(dataFim)
                }

                return true;
            }

            $("#contratoitem_id").on("mousedown", function(e) {
                toggleOptions(e, '#contratoitem_id');
            });
        })
    </script>
@endpush
