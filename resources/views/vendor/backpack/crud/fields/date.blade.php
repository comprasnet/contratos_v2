<!-- html5 date input -->

<?php
// if the column has been cast to Carbon or Date (using attribute casting)
// get the value as a date string
if (isset($field['value']) && ($field['value'] instanceof \Carbon\CarbonInterface)) {
    $field['value'] = $field['value']->toDateString();
}
if (!empty($field['value'])) {
  $arrayData = explode('-',$field['value']);
  $field['value'] = "{$arrayData[2]}/$arrayData[1]/$arrayData[0]";
}
?>

@include('crud::fields.inc.wrapper_start')

  <!-- [html-validate-disable input-attributes]-->
  <div class="br-datetimepicker" data-mode="single" data-type="text">
    <div class="br-input has-icon">
      <label for="{{ $field['name'] }}">
        {!! $field['label'] !!}
        @if(isset($field['required']) && $field['required'])
          @include("vendor.backpack.crud.fields.marcacao_obrigatorio",['textTooltip' => 'Este campo é obrigatório'])
        @endif
      </label>
      <input id="{{ $field['name'] }}" name="{{ $field['name'] }}" type="text" placeholder="dd/mm/aaaa" data-input="data-input"
        value="{{ old_empty_or_null($field['name'], '') ??  $field['value'] ?? $field['default'] ?? '' }}"
        @include('crud::fields.inc.attributes')
      />
      <button class="br-button circle small" type="button" aria-label="Abrir Timepicker" data-toggle="data-toggle"  ><i class="fas fa-calendar-alt" aria-hidden="true"></i>
      </button>
    </div>
  </div>

@include('crud::fields.inc.wrapper_end')
