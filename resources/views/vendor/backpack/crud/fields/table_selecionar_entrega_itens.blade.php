@php
    $aeEntregaRepository = new \App\Repositories\AutorizacaoExecucao\UsuarioFornecedorAutorizacaoExecucaoEntregaRepository();
    $valorTotalEntrega = 0;

    $exibeCamposTRP = true;
    if (!$crud->entry || in_array($crud->entry->situacao->descres, ['ae_entrega_status_1', 'ae_entrega_status_9'])) {
        $exibeCamposTRP = false;
    }
@endphp
<div class="col-md-12 pt-5 table-responsive" style="max-width: 80vw" element="div" bp-field-wrapper="true" bp-field-type="text" id="itensOsfEntrega">
    <b><span id="nomeFornecedor"></span></b>
    <table id="table_selecionar_item_compra_arp" class="table dataTable dtr-inline collapsed">
        <h6>Itens Entregues</h6> <!-- Título da Tabela Adicionado -->
        <thead>
            <tr>
                @if ($exibeCamposTRP)
                    <th>#</th>
                @endif
                <th>
                    <label style="display: block; text-align: center;">
                        <input type="checkbox" class="selectAll" id="selecionar_todos_itens" title="Selecionar todos"/>
                    </label>
                </th>
                <th></th>
                <th>Tipo item</th>
                <th>Num item compra</th>
                <th>Item</th>
                <th>Especificações Complementares</th>
                <th>Unidade de Fornecimento</th>
                <th>Quantidade Solicitada na OS/F</th>
                <th>Quantidade Informada na Entrega</th>
                <th>Glosa (R$)</th>
                <th>Valor Unitário</th>
                <th>Valor Total da entrega</th>
            </tr>
        </thead>
        <tbody id="itens_entrega">
            @foreach($itens as $key=> $item)
                @php
                    $quantidadeInformada = old_empty_or_null("itens[$key][quantidade_informada]") ?
                        str_replace(['.', ','], ['', '.'], old_empty_or_null("itens[$key][quantidade_informada]")) : 0;
                    $valorGlosa =  old_empty_or_null("itens[$key][valor_glosa]") ?
                        str_replace(['.', ','], ['', '.'], old_empty_or_null("itens[$key][valor_glosa]")) : 0;
                    if ($exibeCamposTRP) {
                        $mesAnoCompetencia = null;
                        $processoSei = null;
                        $dataInicio = null;
                        $dataFim = null;
                        $horario = null;
                    }

                    $checked = '';
                    $disabled = 'disabled';
                    if ($errors->any()) {
                        $checked = old_empty_or_null("itens[$key][itens_selecionados]") ? 'checked' : '';
                        $disabled = old_empty_or_null("itens[$key][itens_selecionados]") ? '' : 'disabled';
                    }
                    $itemEntregaId = null;

                    if ($item->itensEntrega) {
                        $quantidadeInformada = old_empty_or_null("itens[$key][quantidade_informada]") ?
                            str_replace(['.', ','], ['', '.'], old_empty_or_null("itens[$key][quantidade_informada]")) :
                            $item->itensEntrega->quantidade_informada;
                        $valorGlosa = old_empty_or_null("itens[$key][valor_glosa]") ?
                            str_replace(['.', ','], ['', '.'], old_empty_or_null("itens[$key][valor_glosa]")) :
                            $item->itensEntrega->valor_glosa;
                        if ($exibeCamposTRP) {
                            $mesAnoCompetencia = $item->itensEntrega->mes_ano_competencia;
                            $processoSei = $item->itensEntrega->processo_sei;
                            $dataInicio = $item->itensEntrega->data_inicio;
                            $dataFim = $item->itensEntrega->data_fim;
                            $horario = $item->itensEntrega->horario;
                        }
                        if (!$errors->any()) {
                            $checked = 'checked';
                            $disabled = '';
                        }
                        $itemEntregaId = $item->itensEntrega->id;
                    }

                    $valorTotalItemEntrega = $quantidadeInformada * $item->valor_unitario_sem_formatar - $valorGlosa;

                    $totaisEmAnaliseAteTRP = $aeEntregaRepository->retornaTotaisEmAnaliseAteTRP($item->id, $itemEntregaId);
                    $totaisEmAnaliseAteTRP->quantidade_total = $totaisEmAnaliseAteTRP->quantidade_total ?? '0.00';
                    $valorTotalEmAnaliseAteTRP = $totaisEmAnaliseAteTRP->quantidade_total * $item->valor_unitario_sem_formatar - $totaisEmAnaliseAteTRP->valor_glosa_total;

                    $totaisEmAvaliacaoAteTRD = $aeEntregaRepository->retornaTotaisEmAvaliacaoAteTRD($item->id, $itemEntregaId);
                    $totaisEmAvaliacaoAteTRD->quantidade_total = $totaisEmAvaliacaoAteTRD->quantidade_total ?? '0.00';
                    $valorTotalEmAvaliacaoAteTRD = $totaisEmAvaliacaoAteTRD->quantidade_total * $item->valor_unitario_sem_formatar - $totaisEmAvaliacaoAteTRD->valor_glosa_total;

                    $totaisExecutadosPosTRD = $aeEntregaRepository->retornaTotaisExecutadosPosTRD($item->id, $itemEntregaId);
                    $totaisExecutadosPosTRD->quantidade_total = $totaisExecutadosPosTRD->quantidade_total ?? '0.00';
                    $valorTotalExecutadosPosTRD = $totaisExecutadosPosTRD->quantidade_total * $item->valor_unitario_sem_formatar - $totaisExecutadosPosTRD->valor_glosa_total;

                    $quantidadeSaldoExecutar = $item->getQuantidadeTotalSemFormatar() - $quantidadeInformada - $totaisEmAnaliseAteTRP->quantidade_total - $totaisEmAvaliacaoAteTRD->quantidade_total - $totaisExecutadosPosTRD->quantidade_total;
                    $valorTotalSaldoExecutar = $quantidadeSaldoExecutar * $item->valor_unitario_sem_formatar;

                    $porcentagemSaldoConsumo = ($totaisEmAnaliseAteTRP->quantidade_total + $totaisEmAvaliacaoAteTRD->quantidade_total + $totaisExecutadosPosTRD->quantidade_total + $quantidadeInformada)  * 100 / $item->getQuantidadeTotalSemFormatar();
                    $porcentagemSaldoConsumo = floor($porcentagemSaldoConsumo);

                    $classProgressBar = 'success';
                    if ($porcentagemSaldoConsumo >= 75 && $porcentagemSaldoConsumo < 90) {
                        $classProgressBar = 'warning';
                    } elseif ($porcentagemSaldoConsumo >= 90) {
                        $classProgressBar = 'danger';
                    }

                    $valorGlosa = number_format($valorGlosa, 2, ',', '.');
                    if ($item->tipo_item == 'Serviço') {
                        $quantidadeTotalEmAnaliseAteTRP = str_replace('.', ',', rtrim($totaisEmAnaliseAteTRP->quantidade_total, '0'));
                        $quantidadeTotalEmAnaliseAteTRP = rtrim($quantidadeTotalEmAnaliseAteTRP, ',');

                        $quantidadeTotalEmAvaliacaoAteTRD = str_replace('.', ',', rtrim($totaisEmAvaliacaoAteTRD->quantidade_total, '0'));
                        $quantidadeTotalEmAvaliacaoAteTRD = rtrim($quantidadeTotalEmAvaliacaoAteTRD, ',');

                        $quantidadeTotalExecutadosPosTRD = str_replace('.', ',', rtrim($totaisExecutadosPosTRD->quantidade_total, '0'));
                        $quantidadeTotalExecutadosPosTRD = rtrim($quantidadeTotalExecutadosPosTRD, ',');

                        $quantidadeInformada = str_replace('.', ',', rtrim($quantidadeInformada, '0'));
                        $quantidadeSaldoExecutar = str_replace('.', ',', rtrim($quantidadeSaldoExecutar, '0'));
                    } else {
                        $quantidadeTotalEmAnaliseAteTRP = (int) $totaisEmAnaliseAteTRP->quantidade_total;
                        $quantidadeTotalEmAvaliacaoAteTRD = (int) $totaisEmAvaliacaoAteTRD->quantidade_total;
                        $quantidadeTotalExecutadosPosTRD = (int) $totaisExecutadosPosTRD->quantidade_total;
                        $quantidadeInformada = (int) $quantidadeInformada;
                        $quantidadeSaldoExecutar = (int) $quantidadeSaldoExecutar;
                    }

                @endphp
                <tr>
                    @if($exibeCamposTRP)
                        <td class="dtr-control"></td>
                    @endif
                    <td>
                        <input
                            type="checkbox"
                            name="itens[{{ $key }}][itens_selecionados]"
                            value="{{ $item->id }}"
                            class="item_checkbox"
                            {{ $checked }}
                        />
                    </td>
                    <td>
                        <div id="progress-bar-{{ $key }}" class="progress-saldo d-flex align-items-end {{ $classProgressBar }}" style="text-align: center;">
                            <div>
                                <span class="pct">{{ $porcentagemSaldoConsumo }}%</span>
                                <progress value="{{ $porcentagemSaldoConsumo }}" max="100"></progress>
                            </div>
                            <i class="fas fa-info-circle"></i>
                            <div
                                class="br-tooltip text-justify flex-column"
                                style="max-width: none; position: absolute; inset: auto auto 0px 0px; transform: translate(1148.89px, -25.5556px); display: unset; z-index: -1; visibility: hidden;"
                                role="tooltip"
                                info="info"
                                place="left"
                            >
                                <table class="table-hint">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th class="text-bold">Qtde. Total</th>
                                            <th class="text-bold">Glosa</th>
                                            <th class="text-bold">Valor Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-bold">Qtde. Solicitada (OS/F)</td>
                                            <td>{{ $item->getQuantidadeTotal() }}</td>
                                            <td>---</td>
                                            <td>R$ {{ $item->getTotal()}}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Qtde. Informada Atual</td>
                                            <td id="quantidade_total_informada_{{ $key }}">{{ $quantidadeInformada }}</td>
                                            <td id="valor_glosa_total_entrega_item_{{ $key }}">R$ {{ $valorGlosa }}</td>
                                            <td class="valor_total_entrega_item_{{ $key }}">
                                                <span>R$ {{ number_format($valorTotalItemEntrega, 2, ',', '.') }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Qtde. em Análise (Até TRP)</td>
                                            <td id="quantidade_total_analise_{{ $key }}">
                                                <span>{{ $quantidadeTotalEmAnaliseAteTRP }}</span>
                                                <input
                                                    type="hidden"
                                                    name="itens[{{ $key }}][quantidade_analise]"
                                                    value="{{ $totaisEmAnaliseAteTRP->quantidade_total }}"
                                                    readonly
                                                    {{ $disabled }}
                                                />
                                            </td>
                                            <td>R$ {{ number_format($totaisEmAnaliseAteTRP->valor_glosa_total, 2, ',', '.') }}</td>
                                            <td>R$ {{ number_format($valorTotalEmAnaliseAteTRP, 2, ',', '.') }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Qtde. em Avaliação (Até TRD)</td>
                                            <td>
                                                <span>{{ $quantidadeTotalEmAvaliacaoAteTRD }}</span>
                                                <input
                                                        type="hidden"
                                                        name="itens[{{ $key }}][quantidade_avaliacao]"
                                                        value="{{ $totaisEmAvaliacaoAteTRD->quantidade_total }}"
                                                        readonly
                                                        {{ $disabled }}
                                                />
                                            </td>
                                            <td>R$ {{ number_format($totaisEmAvaliacaoAteTRD->valor_glosa_total, 2, ',', '.') }}</td>
                                            <td>R$ {{ number_format($valorTotalEmAvaliacaoAteTRD, 2, ',', '.') }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-bold">Qtde. Executada (Após TRD)</td>
                                            <td>
                                                <span>{{ $quantidadeTotalExecutadosPosTRD }}</span>
                                                <input
                                                        type="hidden"
                                                        name="itens[{{ $key }}][quantidade_executada]"
                                                        value="{{ $totaisExecutadosPosTRD->quantidade_total }}"
                                                        readonly
                                                        {{ $disabled }}
                                                />
                                            </td>
                                            <td>R$ {{ number_format($totaisExecutadosPosTRD->valor_glosa_total, 2, ',', '.') }}</td>
                                            <td>R$ {{ number_format($valorTotalExecutadosPosTRD, 2, ',', '.') }}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td class="text-bold">Saldo a Executar</td>
                                            <td id="quantidade_saldo_executar_{{ $key }}" class="text-bold">{{ $quantidadeSaldoExecutar }}</td>
                                            <td class="text-bold">---</td>
                                            <td id="valor_total_saldo_executar_{{ $key }}" class="text-bold">R$ {{ number_format($valorTotalSaldoExecutar, 2, ',', '.') }}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </td>
                    <td class="tipo_item">{{ $item->tipo_item }}</td>
                    <td class="numero_item_compra">{{ $item->numero_item_compra }}</td>
                    <td class="item">{{ $item->item }}</td>
                    <td class="especificacao">{{ $item->especificacao }}</td>
                    <td class="unidade_medida_id">{{ $item->unidadeMedida->nome }}</td>
                    <td class="quantidade_solicitada">
                        {{ $item->getQuantidadeTotal() }}
                        <input
                            type="hidden"
                            name="itens[{{ $key }}][quantidade_solicitada]"
                            value="{{ $item->getQuantidadeTotalSemFormatar() }}"
                            {{ $disabled }}
                            readonly
                            min="0"
                        />
                    </td>
                    <td>
                        <input
                            type="text"
                            name="itens[{{ $key }}][quantidade_informada]"
                            value="{{ $quantidadeInformada }}"
                            class="quantidade_informada"
                            data-key="{{ $key }}"
                            {{ $disabled }}
                            min="0"
                        />
                    </td>
                    <td>
                        <input
                            type="text"
                            name="itens[{{ $key }}][valor_glosa]"
                            value="{{ $valorGlosa }}"
                            class="valor_glosa"
                            data-key="{{ $key }}"
                            {{ $disabled }}
                            min="0"
                        />
                    </td>
                    <td class="valor_unitario">
                        {{ 'R$ ' . $item->valor_unitario }}
                        <input
                            type="hidden"
                            name="itens[{{ $key }}][valor_unitario]"
                            value="{{ $item->valor_unitario_sem_formatar }}"
                            readonly
                            {{ $disabled }}
                        />
                    </td>
                    <td class="valor_total_entrega_item_{{ $key }}">
                        <span>{{ 'R$ ' .  number_format($valorTotalItemEntrega, 2, ',', '.') }}</span>
                        <input
                            class="valor_total_entrega_item_input"
                            type="hidden"
                            name="itens[{{ $key }}][valor_total_entrega]"
                            value="{{ $valorTotalItemEntrega }}"
                            readonly
                            {{ $disabled }}
                        />
                    </td>
                </tr>
                @if($exibeCamposTRP)
                    <tr class="d-none">
                        <td colspan="13">
                            <div class="row">
                                @include('crud::fields.date_month',
                                [
                                    'field' => [
                                        'name' => "itens[$key][mes_ano_competencia]",
                                        'label' => 'Competência',
                                        'type' => 'date_month',
                                        'attributes' => [
                                            $disabled => 'disabled'
                                        ],
                                        'wrapperAttributes' => [
                                            'class' => 'col-md-6',
                                        ],
                                        'value' => $mesAnoCompetencia
                                    ]
                                ])
                                @include('crud::fields.text',
                                [
                                    'field' => [
                                        'name' => "itens[$key][processo_sei]",
                                        'label' => 'Processo SEI',
                                        'type' => "text",
                                        'wrapperAttributes' => [
                                            'class' => 'col-md-6',
                                        ],
                                        'attributes' => [
                                            'data-mask' => $crud->model->getProcessoMask(),
                                            'placeholder' => $crud->model->getProcessoMask(),
                                            $disabled => 'disabled'
                                        ],
                                        'value' => $processoSei
                                    ]
                                ])
                                @include('crud::fields.date',
                                [
                                    'field' => [
                                        'name' => "itens[$key][data_inicio]",
                                        'type' => 'date',
                                        'label'=> 'Data Início',
                                        'value' => $dataInicio,
                                        'wrapper' => [
                                            'class' => 'col-md-4',
                                        ],
                                        'attributes' => [
                                            $disabled => 'disabled'
                                        ]
                                    ]
                                ])
                                @include('crud::fields.date',
                                [
                                    'field' => [
                                        'name' => "itens[$key][data_fim]",
                                        'type' => 'date',
                                        'label'=> 'Data Fim',
                                        'value' => $dataFim,
                                        'wrapper' => [
                                            'class' => 'col-md-4',
                                        ],
                                        'attributes' => [
                                            $disabled => 'disabled'
                                        ]
                                    ]
                                ])
                                @include('crud::fields.time',
                                [
                                    'field' => [
                                        'name' => "itens[$key][horario]",
                                        'type' => 'time',
                                        'label'=> 'Horas',
                                        'value' => $horario,
                                        'wrapper' => [
                                            'class' => 'col-md-4',
                                        ],
                                        'attributes' => [
                                            $disabled => 'disabled'
                                        ]
                                    ]
                                ])
                            </div>
                        </td>
                    </tr>
                @endif
            @endforeach
        </tbody>
        <tfoot>
            <tr style="background-color: var(--background-alternative) !important">
                <td colspan="{{ $exibeCamposTRP ? 11 : 10 }}"></td>
                <td><b>Valor Total:</b></td>
                <td colspan="2" id="valor_total_entrega">
                    {{ 'R$ ' . number_format($valorTotalEntrega, 2, ',', '.') }}
                </td>
            </tr>
        </tfoot>
    </table>
</div>

<style>
    .table-hint tr, .table-hint thead, .table-hint th {
        background-color: transparent !important;
    }
    .table-hint td, .table-hint th {
        color: #fff;
        border-color: #fff;
    }
</style>

@push('after_scripts')
<script>
        $(document).ready(function(){
            $('.dtr-control').on('click', function() {
                const trParent = $(this).parent('tr');
                const trDetails = trParent.next();
                trParent.toggleClass('parent');
                trDetails.toggleClass('d-none');
            })

            $('input').on('focusin', function(){
                $(this).data('val', $(this).val());
            });

            $('#selecionar_todos_itens').click(function() {
                var checked = this.checked;
                $('.item_checkbox').prop('checked', checked).each(function() {
                    toggleRowInputs($(this), checked);
                });
                calculaValorTotalEntrega();
            });

            $('.item_checkbox').click(function() {
                toggleRowInputs($(this), this.checked);
                calculaValorTotalEntrega();
            });

            function toggleRowInputs(checkbox, enabled) {
                checkbox.closest('tr').find('input').not('.item_checkbox').prop('disabled', !enabled);
                @if($exibeCamposTRP)
                    checkbox.closest('tr').next().find('input').prop('disabled', !enabled);
                @endif
            }

            $('.quantidade_informada').on('input', function() {
                if ($(this).closest('tr').find('.tipo_item').text() === 'Serviço') {
                    $(this).decimal();
                } else {
                    $(this).integer();
                }
            }).change(async function() {
                const quantidadeInformada = $.ptBRToFloat($(this).val());
                const key = $(this).data('key');
                const valorUnitario = parseFloat($('input[name="itens[' + key + '][valor_unitario]"]').val());
                const valorGlosa = $.floatRound($('input[name="itens[' + key + '][valor_glosa]"]').val(), 2);
                const quantidadeSolicitada = parseFloat($('input[name="itens[' + key + '][quantidade_solicitada]"]').val());
                const quantidadeEmAnalise = parseFloat($('input[name="itens[' + key + '][quantidade_analise]"]').val());
                const quantidadeEmAvaliacaoAteTRD = parseFloat($('input[name="itens[' + key + '][quantidade_avaliacao]"]').val());
                const quantidadeEmExecutadaAposTRD = parseFloat($('input[name="itens[' + key + '][quantidade_executada]"]').val());
                const resultado = quantidadeInformada * valorUnitario - valorGlosa;

                const totalConsumido = quantidadeInformada + quantidadeEmAnalise + quantidadeEmAvaliacaoAteTRD + quantidadeEmExecutadaAposTRD;
                const quantidadeSaldoExecutar = quantidadeSolicitada - totalConsumido;

                let alertText;
                if (quantidadeSolicitada < totalConsumido) {
                    alertText = 'Quantidade informada e em análise não pode ser maior que a quantidade solicitada na OS/F';
                } else if (resultado < 0) {
                    alertText = 'Valor total entrega não pode ser negativo!';
                }

                if (alertText) {
                    alertaUsuarioRetornaValor(alertText, $(this))
                } else {
                    addPorcentagem(key, totalConsumido * 100 / quantidadeSolicitada);
                    $('#quantidade_total_informada_' + key).text(quantidadeInformada);
                    $('#quantidade_saldo_executar_' + key).text(quantidadeSaldoExecutar);
                    $('#valor_total_saldo_executar_' + key).text('R$ ' + formatReal(quantidadeSaldoExecutar * valorUnitario));
                    addValorTotalEntregaItem(key, resultado);
                    calculaValorTotalEntrega();
                }
            });

            $('.valor_glosa').on('input', function () {
                $(this).decimal();
            }).change(async function () {
                let valorGlosa = $.ptBRToFloat($(this).val());
                const key = $(this).data('key');
                const valorUnitario = parseFloat($('input[name="itens[' + key + '][valor_unitario]"]').val());
                const quantidadeInformada = $.ptBRToFloat($('input[name="itens[' + key + '][quantidade_informada]"]').val());
                const valorTotalItemEntrega = quantidadeInformada * valorUnitario;

                if (valorGlosa > valorTotalItemEntrega) {
                    await alertaUsuarioRetornaValor(
                        'Valor da glosa não pode ser maior que o valor total da entrega',
                        $(this)
                    );
                } else {
                    const resultado = valorTotalItemEntrega - valorGlosa;
                    $('#valor_glosa_total_entrega_item_' + key).text(formatReal(valorGlosa));
                    addValorTotalEntregaItem(key, resultado);
                    calculaValorTotalEntrega();
                }
            });

            async function alertaUsuarioRetornaValor(text, element) {
                await swal({
                    title: 'Atenção',
                    text: text,
                    type: 'warning',
                    buttons: {
                        confirm: {
                            text: 'OK',
                            className: 'br-button primary mr-3',
                        }
                    },
                });
                element.val($.ptBRToFloat(element.data('val')));
            }

            function addValorTotalEntregaItem(key, valor) {
                $('.valor_total_entrega_item_' + key + ' span').text(formatReal(valor));
                $('.valor_total_entrega_item_' + key + ' input').val(valor);
            }

            function addPorcentagem(key, porcentagem) {
                const progressSeletor = $('#progress-bar-' + key)
                progressSeletor.find('.pct').text(parseInt(porcentagem) + '%');
                progressSeletor.find('progress').val(parseInt(porcentagem))
                progressSeletor.removeClass('success').removeClass('warning').removeClass('danger');

                progressSeletor.addClass('success');
                if (porcentagem >= 75 && porcentagem < 90) {
                    progressSeletor.addClass('warning');
                } else if (porcentagem >= 90) {
                    progressSeletor.addClass('danger');
                }
            }

            function calculaValorTotalEntrega() {
                const itens = $('#itens_entrega tr');
                let valorTotalEntrega = 0;

                itens.each((key, item) => {
                    const itemSeletor = $(item);

                    if (itemSeletor.find('.item_checkbox').prop('checked')) {
                        valorTotalEntrega += parseFloat(itemSeletor.find('.valor_total_entrega_item_input').val())
                    }
                })

                $('#valor_total_entrega').text(formatReal(valorTotalEntrega));
            }

            function formatReal(valor) {
                return 'R$ ' + valor.toLocaleString("pt-BR", {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2
                });
            }

            calculaValorTotalEntrega();
        });
</script>
@endpush
