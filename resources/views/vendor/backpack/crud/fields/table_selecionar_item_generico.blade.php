@php
$field['label_instrucao_tabela'] = $field['label_instrucao_tabela'] ?? 'Selecionar item';
$field['order_table'] = $field['order_table'] ?? "[1,'desc']";

$field1['name'] = 'input_pesquisar_card';
$field1['type'] = 'search';
$field1['label'] = 'Pesquisar';

$field1['attributes'] = [ 'id'=>'input_pesquisar_table_details', 'placeholder' => 'Pesquisar na tabela'];


$totalLinhaTabela = count($field['value']);
$quantidadeLinhaPorPagina = 2;
$quantidadePagina = $totalLinhaTabela / $quantidadeLinhaPorPagina;

$countLinha = 1;
@endphp

<div class="{{ $field['classArea'] }} table-responsive" element="div"
    bp-field-wrapper="true" bp-field-type="text"  id="{{ $field['idArea'] }}">

    <label>{!! $field['label_instrucao_tabela'] !!}</label>
  {{-- Exibir o campo para filtrar o card --}}
  <div class="row float-right col-md-4">
    @include("vendor.backpack.crud.fields.text", ['field' => $field1])
  </div>
    <table id="{{ $field['idTable'] }}" class="table">
        <thead>
        <tr>
            @if (isset($field['column']['detail_row']))
                <th></th>
            @endif

            @foreach ($field['column'] as $item)
                @if (!is_array($item))
                    <th>{!! $item !!}</th>
                @endif
            @endforeach
        </tr>
        </thead>
        @if (isset($field['value']) && !empty($field['value']))
            <tbody>
                @foreach ($field['value'] as $linha)
                    @php
                        $linha['detail_row'] = $linha['detail_row'] ?? array();
                        $paginacao = (int)ceil($countLinha / $quantidadeLinhaPorPagina);
                        $classPaginacao = "paginacao_{$paginacao}";
                        $hiddenRow = '';

                        if ($paginacao > 1) {
                            $hiddenRow = 'hiddenRow';
                        }
                    @endphp

                    <tr class="{{ $classPaginacao }} {{ $hiddenRow }}">
                        {{-- Se for enviado o array do detail_row então inclui o ícone para expandir --}}
                        @if (count($linha['detail_row']) > 0)
                            <td>
                                <button
                                    class="br-button circle small"
                                    type="button"
                                    onclick="exibirRecolherDetails({{ $linha['id_unico'] }})"
                                    data-target="collapse-1-4-27509">
                                    <i class="fas fa-chevron-down" aria-hidden="true"
                                    id="icon_detail_{{ $linha['id_unico'] }}"></i>
                                </button>
                            </td>
                        @endif

                        {{-- Percorre as colunas --}}
                        @foreach ($field['column'] as $key => $item)
                            @if (!is_array($linha[$key]))
                                <td>
                                    {!! $linha[$key] !!}
                                </td>
                            @endif
                        @endforeach
                    </tr>

                    {{-- Percorre os valores enviados no detail_row --}}
                    @foreach ($linha['detail_row'] as $idUnico => $detailRow)
                        @php
                            $countColumn = count($detailRow);
                            $colMd = 12 / $countColumn;
                        @endphp
                        <tr class="detail_row_{{ $idUnico }} hidden detail_row_custom">
                            @foreach ($detailRow as $key => $detail)
                                @php
                                # Recuperar a configuração enviada no array de colunas informada na Controller
                                $colMdCustom = $field['column']['detail_row'][$key] ?? null;

                                # Se existir um colMd enviado na Controller, então assume esse valor
                                if (!empty($colMdCustom)) {
                                    $colMdCustom = json_decode($colMdCustom, true);
                                    $colMd = $colMdCustom['coldMd'];
                                }

                                @endphp
                                <td colspan="{{ $colMd }}">
                                    {!! $detail !!}
                                </td>
                            @endforeach
                        </tr>
                    @endforeach

                    @php
                        $countLinha ++;
                    @endphp
                @endforeach
            </tbody>
        @endif
    </table>
    {{-- <nav class="br-pagination" aria-label="Paginação de resultados" data-total="{{ $totalLinhaTabela }}" data-current="1">
        <ul>
          <li>
            <button class="br-button circle" type="button" data-previous-page="data-previous-page" aria-label="Página anterior"><i class="fas fa-angle-left" aria-hidden="true"></i>
            </button>
          </li>
            @for ($i = 1; $i <= $quantidadePagina; $i++)
                @php  $paginacaoAtiva = ''; @endphp

                @if ($i == 1)
                    @php  $paginacaoAtiva = 'active'; @endphp
                @endif
                <li><a class="page {{ $paginacaoAtiva }}" href="javascript:void(0)">{{ $i }}</a></li>
            @endfor
          <li>
            <button class="br-button circle" type="button" data-next-page="data-next-page" aria-label="Página seguinte"><i class="fas fa-angle-right" aria-hidden="true"></i>
            </button>
          </li>
        </ul>
      </nav> --}}
<style>
    .hidden {
        display:none;
    }

    /* .hiddenRow {
        display:none;
    } */
</style>
{{-- <div class="mb-3">
    <a href="#areaItemAdesao" id="btnInserirItemFornecedor" class="br-button primary large mr-3 menu-item-link-externo br-button-form">Incluir Item</i></a>
</div> --}}
</div>

@push('after_scripts')
<script>
    let languageDt = {
                            "emptyTable":     "{{ $field['emptyTable'] }}",
                            "info":           "{{ trans('backpack::crud.info') }}",
                            "infoEmpty":      "{{ trans('backpack::crud.infoEmpty') }}",
                            "infoFiltered":   "{{ trans('backpack::crud.infoFiltered') }}",
                            "infoPostFix":    "{{ trans('backpack::crud.infoPostFix') }}",
                            "thousands":      "{{ trans('backpack::crud.thousands') }}",
                            "lengthMenu":     "{{ trans('backpack::crud.lengthMenu') }}",
                            "loadingRecords": "{{ trans('backpack::crud.loadingRecords') }}",
                            "processing":     "<img src='{{ asset('packages/backpack/base/img/spinner.svg') }}' alt='{{ trans('backpack::crud.processing') }}'>",
                            "search": "_INPUT_",
                            "searchPlaceholder": "{{ trans('backpack::crud.search') }}...",
                            "zeroRecords":    "{{ trans('backpack::crud.zeroRecords') }}",
                            "paginate": {
                                "first":      "{{ trans('backpack::crud.paginate.first') }}",
                                "last":       "{{ trans('backpack::crud.paginate.last') }}",
                                "next":       ">",
                                "previous":   "<"
                            },
                            "aria": {
                                "sortAscending":  "{{ trans('backpack::crud.aria.sortAscending') }}",
                                "sortDescending": "{{ trans('backpack::crud.aria.sortDescending') }}"
                            }
                        }
let datableItem = null

function inserirLinhaItem(item = null, inicializar = true) {

    if (!$.fn.DataTable.isDataTable('#{{ $field['idTable'] }}')) {
        datableItem = $('#{{ $field['idTable'] }}').DataTable({
            language: languageDt,
            pageLength: {{ $field['pageLength'] }},
            lengthMenu: [{{ $field['pageLength'] }}],
            order: [{!! $field['order_table'] !!}]
        })

        $('.dataTables_filter input[type="search"]').css({'width':'580px'});
        $('.dataTables_filter input[type="search"]').attr({'placeholder': 'Para filtrar com mais de um valor, separar por espaço'})
        $('div.dataTables_wrapper div.dataTables_length select').css("width", "70px")

        return
    }

    datableItem.clear();
    datableItem.destroy();

    datableItem = $('#{{ $field['idTable'] }}').DataTable({
        data: item,
        autoWidth: false,
        responsive: true,
        fixedHeader: true,
        pageLength: {{ $field['pageLength'] }},
        language: languageDt,
        lengthMenu: [{{ $field['pageLength'] }}],
        order: [{!! $field['order_table'] !!}],
        columns: [
            @foreach ($field['column'] as $key => $item)
            {
                data: '@php echo $key; @endphp'
            },
            @endforeach

        ]
    });

    $('.dataTables_filter input[type="search"]').css({'width':'680px'});
    $('.dataTables_filter input[type="search"]').attr({'placeholder': 'Para filtrar com mais de um valor, separar por espaço'})
    $('div.dataTables_wrapper div.dataTables_length select').css("width", "70px")
}

function checkTodos(idCampo , state) {
    $(idCampo).prop('checked', state)
}

function exibirRecolherDetails (idUnico)
{
    let id = `.detail_row_${idUnico}`
    let idIcon = `#icon_detail_${idUnico}`
    if ($(id).hasClass( "hidden" )) {
        $(id).removeClass('hidden')
        $(id).show()
        $(idIcon).removeClass('fa-chevron-down')
        $(idIcon).addClass('fa-chevron-up')
        return;
    }

    $(id).addClass('hidden')
    $(id).hide()
    $(idIcon).removeClass('fa-chevron-up')
    $(idIcon).addClass('fa-chevron-down')
}
// Método responsável em converter o texto para tornar case insensitive
$.expr[":"].contains = $.expr.createPseudo(function(arg) {
    return function( elem ) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

// Método responsável em filtrar a tabela com base no que o usuário digitou no campo de filtro
$("#input_pesquisar_table_details").keyup(function () {
    // Recuperar as linhas da tabela que não possuem a class hidden
    let rows = $({{ $field['idTable'] }}).find("tr:not(.hidden)").hide();

    if (this.value.length) {
        let data = this.value.split(" ");
        $.each(data, function (i, v) {
            rows.filter(":contains('" + v + "')").show();
        });
        return
    }

    rows.show();
});


$(document).ready(function () {
// caso queira o filtro por coluna, descomentar este trecho

// $('#table_selecionar_item_adesao_arp tfoot th').each(function () {
//     var title = $(this).text();
//     if ( title != '') {
//         $(this).html(`  <div class="br-input">
//         <div class="input-group">
//           <div class="input-icon"><i class="fas fa-search" aria-hidden="true"></i>
//           </div>
//           <input  type="text"/>
//         </div>
//       </div>`);
//     }

// });

// filtrar todos os campos separados por virgula
$.fn.dataTableExt.afnFiltering.push(
    function(oSettings, aData, iDataIndex) {
        var filter = $("#{{ $field['idTable'] }}_filter input").val();
        filter = filter.split(' ');
        for (var f=0;f<filter.length;f++) {
            for (var d=0;d<aData.length;d++) {
                if (aData[d].indexOf(f)>-1) {
                    return true;
                }
            }
        }
     }
  )


    // // Array to track the ids of the details displayed rows
    // var detailRows = [];

    // $("#{{ $field['idTable'] }} tbody").on( 'click', 'tr td:first-child', function () {
    //     var tr = $(this).parents('tr');
    //     var row = datableItem.row( tr );
    //     var idx = $.inArray( tr.attr('id'), detailRows );
    //     console.log(row.data())
    //     let id_unico = row.data().id_unico

    //     if ( row.child.isShown() ) {
    //         tr.removeClass( 'details' );
    //         row.child.hide();

    //         // Remove from the 'open' array
    //         detailRows.splice( idx, 1 );

    //         $(`#btn_expandir_${id_unico}`).removeClass('fa-minus')
    //         $(`#btn_expandir_${id_unico}`).addClass('fa-plus')
    //     }
    //     else {
    //         $(`#btn_expandir_${id_unico}`).removeClass('fa-plus')
    //         $(`#btn_expandir_${id_unico}`).addClass('fa-minus')

    //         tr.addClass( 'details' );
    //         row.child( format( row.data() ) ).show();

    //         // Add to the 'open' array
    //         if ( idx === -1 ) {
    //             detailRows.push( tr.attr('id') );
    //         }
    //     }
    // } );

    // // On each draw, loop over the `detailRows` array and show any child rows
    // datableItem.on( 'draw', function () {
    //     $.each( detailRows, function ( i, id ) {
    //         $('#'+id+' td:first-child').trigger( 'click' );
    //     } );
    // } );

})

// function format ( d ) {
//     return 'Full name: '+d.first_name+' '+d.last_name+'<br>'+
//         'Salary: '+d.salary+'<br>'+
//         'The child row can contain any data you wish, including links, images, inner tables etc.';
// }

</script>
@endpush