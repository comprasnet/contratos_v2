@php
    $arpTipos = \App\Models\ArpArquivoTipo::all();

    $selected = \App\Models\ArpArquivoTipo::where('nome', 'Ata de Registro de Preços')->first()->id;
    if(old('tipo_id')) {
        $selected = old('tipo_id');
    }
@endphp
<div class="br-select w-100" style="max-width: none">
    <div class="br-input">
        <label for="select-simple">Tipo</label>
        <label style="color: red">*</label>
        <input required id="select-simple" type="text" placeholder="Selecione o item"/>
        <button class="br-button" type="button" aria-label="Exibir lista" tabindex="-1" data-trigger="data-trigger"><i class="fas fa-angle-down" aria-hidden="true"></i>
        </button>
    </div>
    <div class="br-list" tabindex="0">
        @foreach($arpTipos as $tipo)
            <div class="br-item {{ $tipo->id == $selected ? 'selected' : '' }}" tabindex="-1" onclick="showArquivoAlert('{{$tipo->nome}}')">
                <div class="br-radio">
                    <input id="{{$tipo->id}}" type="radio" name="tipo_id" value="{{$tipo->id}}"/>
                    <label for="{{$tipo->id}}">{{$tipo->nome}}</label>
                </div>
            </div>
        @endforeach
    </div>
</div>