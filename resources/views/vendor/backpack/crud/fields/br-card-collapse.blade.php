@php
$field['prefix_text'] = $field['label'] ?? '';
$field['quantidade_por_linha'] = $field['quantidade_por_linha'] ?? 3;

$field['col_md'] = (12 / $field['quantidade_por_linha']);

$field1['name'] = 'input_pesquisar_card';
$field1['type'] = 'search';
$field1['label'] = 'Filtrar';

$field1['attributes'] = [ 'id'=>'input_pesquisar_card'];

$contadorMd = 0;
@endphp
{{-- Exibe a linha da divisão entre o label principal e o card  --}}
@include("vendor.backpack.crud.fields.br-divider")

@include('crud::fields.inc.wrapper_start')
@include('crud::fields.inc.translatable_icon')
  {{-- Exibir o campo para filtrar o card --}}
  <div class="row">
    @include("vendor.backpack.crud.fields.text", ['field' => $field1])
  </div>
  <div id="listaCards">
    @foreach ($field['body'] as $key => $body)
      {{-- Percorrer as configurações informadas no Controller --}}
      @foreach ($body['card'] as $key => $card)
        {{-- Abre a div row para incluir is cards --}}
        @if ($contadorMd == 0)
        <div class="row">
        @endif

        {{-- Contador para inserir a div row quando chegar ao doze --}}
        @php
        $contadorMd += $field['col_md'];
        $card['sub-header'] = $card['sub-header'] ?? '';
        $card['body'] = $card['body'] ?? '';
        $card['expansion'] = $card['expansion'] ?? array();
        @endphp

        <div class="  col-md-{{ $field['col_md'] }} col-lg-{{ $field['col_md'] }} card-collapse-filter" id="card-collapse-{{ $key }}">
        <div class="br-card hover" id="card1234-{{ $key }}">
          <div class="card-header">
            <div class="d-flex">
              <div class="ml-3">
                <div class="text-weight-semi-bold text-up-02">{{ $card['header'] }}</div>
                <div>{{ $card['sub-header'] }}</div>
              </div>
            </div>
          </div>
          <div class="card-content">
            <p>{!! $card['body'] !!}</p>
          </div>
          <div class="card-footer">
            <div class="text-right">
              <button class="br-button circle" type="button" aria-label="Expandir o card" data-toggle="collapse"
                      data-target="expanded-{{ $key }}" aria-controls="expanded-{{ $key }}" aria-expanded="false"
                      data-visible="false"><i class="fas fa-chevron-down" aria-hidden="true"></i>
              </button>
            </div>
            <div id="expanded-{{ $key }}" hidden="hidden">
              <div class="br-list mt-3">
                @foreach ($card['expansion'] as $expasion)
                  <div class="br-item">
                    <div class="row">
                      <div class="col">
                        @switch($expasion['type'])
                            @case('label')
                                @include("vendor.backpack.crud.fields.label", ['field' => $expasion])
                                @break
                            @case('custom_html')
                                {!! $expasion['value'] !!}
                                @break
                            @default

                        @endswitch
                      </div>
                    </div>
                  </div>
                  <span class="br-divider"></span>
                @endforeach
              </div>
            </div>
          </div>
        </div>
        </div>

        {{-- Se o contador chegar ao doze, então tem que fechar a div da classe ROW --}}
        @if ($contadorMd == 12)
        </div>
        {{-- Zera o contador para poder abrir outra div ROW --}}
        @php
          $contadorMd = 0;
        @endphp
        @endif


      @endforeach


    @endforeach
  </div>
@include('crud::fields.inc.wrapper_end')

@push('after_scripts')
  <script>
    $(document).ready(function() {
      $("#input_pesquisar_card").on("keyup", function() {
        var textoDigitado = $(this).val().toLowerCase(); // Obter o texto digitado e converter para minúsculas

        let idCardExibir = new Array(0)
        let idCardTodos = new Array(0)

        // Filtrar os cards com base no texto digitado
        $(".card-collapse-filter").filter(function() {
          var cardTexto = $(this).text().toLowerCase(); // Obter o texto do card e converter para minúsculas
          let idCard = $(this).attr('id')
          // Se o texto conter dentro do card, então inclui no array para exibir
          if (cardTexto.indexOf(textoDigitado) > -1) {
            idCardExibir.push(idCard)
          }

          // Popular array de todos para esconder
          idCardTodos.push(idCard)
        }); // Alternar a exibição dos cards filtrados

        // Percorre todos os cards para esconder
        $.each( idCardTodos, function( index, value ){
            $(`#${value}`).addClass('hidden').hide()
        });

        // Percorre somente os cards que serão exibidos
        $.each( idCardExibir, function( index, value ){
            $(`#${value}`).removeClass('hidden').show()
        });
      });
    });
  </script>
@endpush