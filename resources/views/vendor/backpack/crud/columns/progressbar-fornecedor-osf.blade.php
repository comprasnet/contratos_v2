@php
$totalGlossa = 0;

$valorTotalEntregaOs = $entry->getValorTotalEntregaAtiva();
$valorTotalOsf = $entry->getValorTotalSemFormatacao();
$valorTotalGlosaEntregaOs = $entry->getValorTotalGlosaEntregaAtiva();

$percentual = 0;
$dividendo = $valorTotalOsf;

if ($valorTotalGlosaEntregaOs > 0) {
$dividendo = $valorTotalOsf - $valorTotalGlosaEntregaOs;
}

if ($valorTotalEntregaOs > 0) {
$percentual = ($valorTotalEntregaOs / $dividendo) * 100;
}
@endphp

<x-progress-bar :id="'progress-bar-' . $entry->id" :percentual="$percentual" />
