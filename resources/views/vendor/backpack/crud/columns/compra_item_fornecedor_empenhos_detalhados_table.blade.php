@php

    use \Illuminate\Support\Facades\DB;

    $id = $entry->getKey();

    $compraItemFornecedor = \App\Models\CompraItemFornecedor::find($id);

    $compraItem = \App\Models\CompraItem::find($compraItemFornecedor->compra_item_id);

    $fornecedor = \App\Models\Fornecedor::find($compraItemFornecedor->fornecedor_id);

    $empenhos = \Illuminate\Support\Facades\DB::table('compra_item_minuta_empenho')
        ->join('minutaempenhos', 'minutaempenhos.id', 'compra_item_minuta_empenho.minutaempenho_id')
        ->join('compra_items', 'compra_items.id', 'compra_item_minuta_empenho.compra_item_id')
        ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', 'compra_items.id')
        ->join('codigoitens', 'codigoitens.id', 'compra_item_minuta_empenho.operacao_id')
        ->join('codigoitens as situacao', 'situacao.id', 'minutaempenhos.situacao_id')
        ->where('compra_items.id', $compraItem->id)
        ->where('compra_item_fornecedor.fornecedor_id', $fornecedor->id)
        ->where('situacao.descres', 'EMITIDO')
        ->select(DB::raw("DISTINCT minutaempenhos.mensagem_siafi"))
        ->addSelect(DB::raw("SUM( CASE WHEN codigoitens.descres = 'INCLUSAO' THEN compra_item_minuta_empenho.quantidade ELSE 0 END ) AS inclusao"))
        ->addSelect(DB::raw("SUM( CASE WHEN codigoitens.descres = 'REFORCO' THEN compra_item_minuta_empenho.quantidade ELSE 0 END ) AS reforco"))
        ->addSelect(DB::raw("SUM( CASE WHEN codigoitens.descres = 'ANULACAO' THEN -compra_item_minuta_empenho.quantidade ELSE 0 END ) AS anulacao"))
        ->addSelect(DB::raw("SUM(compra_item_minuta_empenho.quantidade) AS qtde_total_empenhada"))
        ->addSelect(DB::raw("SUM(compra_item_minuta_empenho.valor) AS valor_total_empenhado"))
        ->addSelect('compra_item_fornecedor.valor_unitario')
        ->groupBy(['minutaempenhos.mensagem_siafi', 'compra_item_fornecedor.valor_unitario'])
        ->orderBy('minutaempenhos.mensagem_siafi', 'ASC')
        ->get();
@endphp

<span>
    <table class="table table-bordered table-condensed table-striped m-b-0">
		<thead>
			<tr>
                <th>Número do Empenho</th>
                <th>Inclusão</th>
                <th>Reforço</th>
                <th>Anulação</th>
                <th>Quantidade Total</th>
                <th>Valor Unitário</th>
                <th>Valor Total</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($empenhos as $empenho)
                <tr>
                    @php
                        $qtd_total_empenhada = $empenho->inclusao + $empenho->reforco + $empenho->anulacao;
                    @endphp
                    <td>{{$empenho->mensagem_siafi}}</td>
                    <td>{{number_format($empenho->inclusao,0)}}</td>
                    <td>{{number_format($empenho->reforco,0)}}</td>
                    <td>{{number_format($empenho->anulacao,0)}}</td>
                    <td>{{number_format($qtd_total_empenhada,0)}}</td>
                    <td>{{number_format($empenho->valor_unitario,2,',','.')}}</td>
                    <td>{{number_format($empenho->valor_total_empenhado,2,',','.')}}</td>
				</tr>
            @endforeach
		</tbody>
	</table>
</span>
