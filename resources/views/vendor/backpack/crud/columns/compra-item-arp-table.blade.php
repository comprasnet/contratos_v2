@php
    $id = $entry->getKey();
    $value = \Illuminate\Support\Facades\DB::table('compra_items')
			->join('arp', 'arp.compra_id', 'compra_items.compra_id')
    		->join('arp_item', 'arp_item.arp_id', 'arp.id')
    		->join('compra_item_fornecedor', 'compra_item_fornecedor.id', 'arp_item.compra_item_fornecedor_id')
    		->join('unidades', 'unidades.id', 'arp.unidade_origem_id')
    		->join('fornecedores', 'fornecedores.id', 'compra_item_fornecedor.fornecedor_id')
    		//->join('compra_item_unidade', 'compra_item_unidade.compra_item_id', 'compra_item_fornecedor.compra_item_id')
    		->where('compra_items.situacao', true)
    		->where('compra_item_fornecedor.situacao', true)
    		->where('arp.rascunho', false)
    		->where('compra_item_fornecedor.compra_item_id', $id)
    		->select([
                'arp.*',
                'unidades.codigo',
                'fornecedores.nome as nomeFornecedor',
                'fornecedores.cpf_cnpj_idgener',
                'compra_item_fornecedor.valor_unitario',
                'compra_item_fornecedor.quantidade_homologada_vencedor as quantidade',
                //'compra_item_unidade.quantidade_autorizada'
                //'compra_items.qtd_total as quantidade_autorizada'
			])
            ->orderBy('created_at')
            ->groupBy([
                'arp.id',
                'unidades.codigo',
                'fornecedores.nome',
                'fornecedores.cpf_cnpj_idgener',
                'compra_item_fornecedor.valor_unitario',
                'quantidade'
            ])
            ->get();

@endphp

<span>
	<table class="table table-bordered table-condensed table-striped m-b-0">
		<thead>
			<tr>
				<th>Número</th>
				<th>Unidade Gerenciadora</th>
				<th>Fornecedor</th>
				<th>Data assinatura</th>
				<th>Vigência início</th>
				<th>Vigência fim</th>
				<th>Quantidade</th>
				<th>Valor Unitário</th>
				<th>Valor total</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($value as $tableRow)
                <tr>
					<td>{{ $tableRow->numero . '/' . $tableRow->ano }}</td>
					<td>{{ $tableRow->codigo }}</td>
					<td>{{ $tableRow->cpf_cnpj_idgener . ' - ' . $tableRow->nomeFornecedor }}</td>
					<td>{{ $tableRow->data_assinatura ? \Carbon\Carbon::createFromFormat('Y-m-d', $tableRow->data_assinatura)->format('d/m/Y') : '-' }}</td>
					<td>{{ $tableRow->vigencia_inicial ? \Carbon\Carbon::createFromFormat('Y-m-d', $tableRow->vigencia_inicial)->format('d/m/Y') : '-' }}</td>
					<td>{{ $tableRow->vigencia_final ? \Carbon\Carbon::createFromFormat('Y-m-d', $tableRow->vigencia_final)->format('d/m/Y') : '-' }}</td>
					<td>{{ $tableRow->quantidade }}</td>
					<td>{{ number_format($tableRow->valor_unitario, 2, ',', '.') }}</td>
					<td style="text-align: right">{{ number_format($tableRow->quantidade * $tableRow->valor_unitario, 2, ',', '.') }}</td>
				</tr>
            @endforeach
		</tbody>
	</table>
</span>
