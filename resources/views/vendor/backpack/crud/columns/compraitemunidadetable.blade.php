@php
    $id = $entry->getKey();
    $value = \App\Models\CompraItemUnidade::where('compra_item_id',$id)
    		->where('situacao', true)
            ->orderBy('created_at')
            ->get();
    $columns = [
            'codigo_unidade' => 'Unidade',
            'tipo_uasg' => 'Tipo UASG',
            'quantidade_autorizada' => 'Qtd. autorizada',
            "quantidade_saldo" => 'Qtd. Saldo',
        ];
@endphp

<span>
	@if ($value && count($columns))
	<table class="table table-bordered table-condensed table-striped m-b-0">
		<thead>
			<tr>
				@foreach($columns as $tableColumnKey => $tableColumnLabel)
                    <th>{{ $tableColumnLabel }}</th>
                @endforeach
			</tr>
		</thead>
		<tbody>
			@foreach ($value as $tableRow)
                <tr>
				@foreach($columns as $tableColumnKey => $tableColumnLabel)
                        <td>
                            @if($tableColumnKey == 'tipo_uasg')
                                @if($tableRow->tipo_uasg == 'G' or $tableRow->tipo_uasg == '')
                                    Gerenciadora
                                @elseif($tableRow->tipo_uasg == 'P')
                                    Participante
                                @elseif($tableRow->tipo_uasg == 'C')
                                    Carona
                                @endif
                            @elseif($tableColumnKey == 'quantidade_autorizada')
                                {{ number_format($tableRow->quantidade_autorizada,5,',','.') }}
                            @elseif($tableColumnKey == 'quantidade_saldo')
								{{ number_format($tableRow->quantidade_saldo,5,',','.') }}
                            @else
                                {{ $tableRow->{$tableColumnKey} }}
                            @endif
					</td>
                    @endforeach
			</tr>
            @endforeach
		</tbody>
	</table>
    @endif
</span>
