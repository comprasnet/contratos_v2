@php

$colunas = array_keys($column['value']);
$linhas = $column['value'];
$quantidadeLinhas = 0;
if(count($colunas) > 0) {
    $quantidadeLinhas = count($linhas[$colunas[0]]);
}

@endphp
<span>
	@if ($column['value'])
        <table class="table table-bordered table-condensed table-striped m-b-0">
		<thead>
			<tr>
				@foreach($colunas as $tableColumnLabel)
                    <th>{{ str_replace("_"," ",$tableColumnLabel) }}</th>
                @endforeach
			</tr>
		</thead>
		<tbody>
            @for ($i = 0; $i < $quantidadeLinhas; $i++)
                <tr>
                    
                    @foreach($colunas as $key => $coluna)
                    <td> {!! $linhas[$coluna][$i] !!}</td>

                    @endforeach
                </tr>
            @endfor
		</tbody>
	</table>
    @endif
</span>
