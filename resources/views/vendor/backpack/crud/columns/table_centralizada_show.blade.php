@php

$colunas = array_keys($column['value']);
$posicao = $column['posicao'];
//dd($posicao);
$linhas = $column['value'];
$quantidadeLinhas = 0;
if(count($colunas) > 0) {
    $quantidadeLinhas = count($linhas[$colunas[0]]);
}

@endphp
<span>
	@if ($column['value'])
        <table class="table table-bordered table-condensed table-striped m-b-0">
		<thead>
			<tr>
				@foreach($colunas as $tableColumnLabel)
                    <th class="text-center">{{ str_replace("_"," ",$tableColumnLabel) }}</th>
                @endforeach

			</tr>
		</thead>
		<tbody>
            @for ($i = 0; $i < $quantidadeLinhas; $i++)
                <tr>

                    @foreach($colunas as $key => $coluna)
                    @php
                    //dd($linhas[]);
                    @endphp

						<td style="text-align: {{ $posicao[$key]}};"> {!! $linhas[$coluna][$i] !!}</td>

                    @endforeach



                </tr>
            @endfor
		</tbody>
	</table>
    @endif
</span>
