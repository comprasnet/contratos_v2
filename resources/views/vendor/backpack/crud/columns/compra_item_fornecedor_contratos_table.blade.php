@php
    // use App\Models\Contrato;
    use App\Http\Traits\Contrato\ContratoTrait;

    $compraItemId = \Route::current()->parameter('compra_item_id');

    $compraItemFornecedorId = $entry->getKey();

    $contratos = ContratoTrait::recuperarContratoPorItemFornecedor($compraItemId,$compraItemFornecedorId);

    // $contratos = Contrato::join('contratoitens', function ($join) {
    //         $join->on('contratoitens.contrato_id', '=', 'contratos.id')
    //             ->whereNull('contratoitens.deleted_at');
    //     })
    //     ->join('compras', function ($join) {
    //         $join->on('contratos.unidadecompra_id', '=', 'compras.unidade_origem_id')
    //             ->on('contratos.modalidade_id', '=', 'compras.modalidade_id')
    //             ->on('contratos.licitacao_numero', '=', 'compras.numero_ano');
    //     })
    //     ->join('compra_items', function ($join) {
    //         $join->on('compra_items.compra_id', '=', 'compras.id')
    //             ->where('compra_items.situacao', true)
    //             ->whereColumn('compra_items.numero', '=', 'contratoitens.numero_item_compra');
    //     })
    //     ->join('compra_item_fornecedor', function ($join) {
    //         $join->on('compra_item_fornecedor.compra_item_id', '=', 'compra_items.id')
    //             ->where('compra_item_fornecedor.situacao', true)
    //             ->whereColumn('compra_item_fornecedor.fornecedor_id', '=', 'contratos.fornecedor_id');
    //     })
    //     ->join('unidades', 'contratos.unidade_id', '=', 'unidades.id')
    //     ->join('codigoitens', 'codigoitens.id', 'contratoitens.tipo_id')
    //     ->where('compra_items.id', $compraItemId)
    //     ->where('compra_item_fornecedor.id', $compraItemFornecedorId)
    //     ->select([
    //         'contratos.numero',
    //         'contratos.data_assinatura',
    //         'contratos.vigencia_inicio',
    //         'contratos.vigencia_fim',
    //         'contratoitens.*',
    //         'codigoitens.descres',
    //         'unidades.codigo'
    //     ])
    //     ->get();

@endphp

<span>
    <table class="table table-bordered table-condensed table-striped m-b-0">
		<thead>
			<tr>
                <th>Número</th>
                <th>Unidade</th>
                <th>Data Assinatura</th>
                <th>Quantidade</th>
                <th>Data Início</th>
                <th>Data Fim</th>
                <th>Valor Unitário</th>
                <th>Valor Total</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($contratos as $contrato)
                <tr>
                    <td>{{$contrato->numero}}</td>
                    <td>{{$contrato->codigo}}</td>
                    <td>{{$contrato->data_assinatura ? \Carbon\Carbon::createFromFormat('Y-m-d', $contrato->data_assinatura)->format('d/m/Y') : '-'}}</td>
                    <td>{{$contrato->quantidade}}</td>
                    <td>{{$contrato->vigencia_inicio ? \Carbon\Carbon::createFromFormat('Y-m-d', $contrato->vigencia_inicio)->format('d/m/Y') : '-'}}</td>
                    <td>{{$contrato->vigencia_fim ? \Carbon\Carbon::createFromFormat('Y-m-d', $contrato->vigencia_fim)->format('d/m/Y') : '-'}}</td>
                    <td>{{number_format($contrato->valorunitario, 2, ',', '.')}}</td>
                    <td>{{number_format($contrato->valortotal, 2, ',', '.')}}</td>
				</tr>
            @endforeach
		</tbody>
	</table>
</span>
