@php
    $id = $entry->getKey();
    $value = \App\Models\CompraItemFornecedor::where('compra_item_fornecedor.compra_item_id',$id)
    		->where('compra_item_fornecedor.situacao', '=', true)
    		->select([
                'compra_item_fornecedor.*'
			])
            ->orderBy('compra_item_fornecedor.created_at')
            ->groupBy('compra_item_fornecedor.id')
            ->get();

    $compra = \App\Models\CompraItem::find($id);

    $columns = [
            'fornecedor_id' => 'Fornecedor',
            'quantidade_homologada_vencedor' => 'Qtd. Homologada',
            'valor_unitario' => 'Vlr. Unitário',
            'valor_negociado' => 'Vlr. Negociado',
            "quantidade_empenhada" => 'Qtd. Empenhada',
        ];
@endphp

<span>
	@if ($value && count($columns))
        <table class="table table-bordered table-condensed table-striped m-b-0">
		<thead>
			<tr>
				@foreach($columns as $tableColumnKey => $tableColumnLabel)
                    <th>{{ $tableColumnLabel }}</th>
                @endforeach
				<th>Ações</th>
			</tr>
		</thead>
		<tbody>

			@foreach ($value as $tableRow)
                <tr>
					@foreach($columns as $tableColumnKey => $tableColumnLabel)
						<td>
							@if($tableColumnKey == 'quantidade_homologada_vencedor')
								{{ number_format($tableRow->quantidade_homologada_vencedor,5,',','.') }}
							@elseif($tableColumnKey == 'quantidade_empenhada')
								@php
									$quantidade = \Illuminate\Support\Facades\DB::table('compra_item_minuta_empenho')
										->where('compra_item_minuta_empenho.compra_item_id', $id)
										->leftjoin('minutaempenhos', 'minutaempenhos.id', 'compra_item_minuta_empenho.minutaempenho_id')
    									->leftjoin('codigoitens as situacao', 'situacao.id', 'minutaempenhos.situacao_id')
    									->where('situacao.descres', 'EMITIDO')
										->sum('compra_item_minuta_empenho.quantidade');
								@endphp
								{{ number_format($quantidade,5,',','.') }}
							@elseif($tableColumnKey == 'valor_unitario')
								{{ number_format($tableRow->valor_unitario,2,',','.') }}
							@elseif($tableColumnKey == 'valor_negociado')
								{{ number_format($tableRow->valor_negociado,2,',','.') }}
							@elseif($tableColumnKey == 'fornecedor_id')
								{{ $tableRow->fornecedor->cpf_cnpj_idgener . ' - ' . $tableRow->fornecedor->nome }}
							@else
								{{ $tableRow->{$tableColumnKey} }}
							@endif
						</td>
					@endforeach
					<td>
						@php
							$link = "/compras/".$tableRow->compraItens->compra_id."/itens/".$tableRow->compra_item_id."/compra-item-fornecedor/".$tableRow->id."/show";
						@endphp
						<a href="{{$link}}"><i class="fas fa-eye"></i></a>
					</td>
				</tr>
            @endforeach
		</tbody>
	</table>
    @endif
</span>
