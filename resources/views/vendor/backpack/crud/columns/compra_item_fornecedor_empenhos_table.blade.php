@php
    $id = $entry->getKey();

    $compraItemFornecedor = \App\Models\CompraItemFornecedor::find($id);

    $compraItem = \App\Models\CompraItem::find($compraItemFornecedor->compra_item_id);

    $fornecedor = \App\Models\Fornecedor::find($compraItemFornecedor->fornecedor_id);

    $empenhos = \Illuminate\Support\Facades\DB::table('compra_item_minuta_empenho')
        ->join('minutaempenhos', 'minutaempenhos.id', 'compra_item_minuta_empenho.minutaempenho_id')
        ->join('unidades', 'unidades.id', 'minutaempenhos.unidade_id')
        ->join('compra_items', 'compra_items.id', 'compra_item_minuta_empenho.compra_item_id')
        ->join('compra_item_fornecedor', 'compra_item_fornecedor.compra_item_id', 'compra_items.id')
        ->join('codigoitens as situacao', 'situacao.id', 'minutaempenhos.situacao_id')
        ->where('compra_items.id', $compraItem->id)
        ->where('compra_item_fornecedor.fornecedor_id', $fornecedor->id)
        ->where('situacao.descres', 'EMITIDO')
        ->groupBy(
                'minutaempenhos.id',
                'compra_item_fornecedor.valor_unitario',
                'compra_item_minuta_empenho.quantidade',
                'unidades.codigo',
            )
        ->orderBy('minutaempenhos.mensagem_siafi', 'ASC')
        ->select([
                'minutaempenhos.*',
                'compra_item_fornecedor.valor_unitario',
                'compra_item_minuta_empenho.quantidade',
                'unidades.codigo',
            ])
        ->get();
@endphp

<span>
    <table class="table table-bordered table-condensed table-striped m-b-0">
		<thead>
			<tr>
                <th>Número</th>
                <th>Unidade</th>
                <th>Data Emissão</th>
                <th>Descrição</th>
                <th>Processo</th>
                <th>Quantidade</th>
                <th>Valor Unitário</th>
                <th>Valor Total</th>
                <th>Número CIPI</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($empenhos as $empenho)
                <tr>
                    <td>{{$empenho->mensagem_siafi}}</td>
                    <td>{{$empenho->codigo}}</td>
                    @php
                        $data_emissão = '';
                        if($empenho->data_emissao) {
                            $data_emissão = \Carbon\Carbon::createFromFormat('Y-m-d', $empenho->data_emissao)->format('d/m/Y');
                        }
                    @endphp
                    <td>{{$data_emissão}}</td>
                    <td>{{$empenho->descricao}}</td>
                    <td>{{$empenho->processo}}</td>
                    <td>{{number_format($empenho->quantidade,5)}}</td>
                    <td>{{number_format($empenho->valor_unitario,2,',','.')}}</td>
                    <td>{{number_format($empenho->valor_total,2,',','.')}}</td>
                    <td>{{$empenho->numero_cipi}}</td>
				</tr>
            @endforeach
		</tbody>
	</table>
</span>
