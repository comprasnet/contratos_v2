@if(!empty($autorizacoes))
    @foreach($autorizacoes as $autorizacao)
        <a class="d-block mb-2" href="/autorizacaoexecucao/{{$autorizacao->contrato_id}}/{{$autorizacao->id}}/show" target="_blank">
            {{ $autorizacao->numero }} <span class="fas fa-external-link-alt"></span>
        </a>
    @endforeach
@endif
