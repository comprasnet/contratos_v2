@php
        $jsonRetornoPncpDecode = json_decode($entry->retorno_pncp, true);
        $jsonRetornoPncpFormatado = $jsonRetornoPncpDecode ? json_encode(
            $jsonRetornoPncpDecode, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES
            ) : '';

        if (empty($jsonRetornoPncpFormatado)) {
            $jsonRetornoPncpFormatado = $entry->retorno_pncp;
        }

        $jsonEnviadoIncDecode = json_decode($entry->json_enviado_inclusao, true);
        $jsonIncFormatado = $jsonEnviadoIncDecode ? json_encode($jsonEnviadoIncDecode, JSON_PRETTY_PRINT) : '';
        $jsonEnviadoAltDecode = json_decode($entry->json_enviado_alteracao, true);
        $jsonAltFormatado = $jsonEnviadoAltDecode ? json_encode($jsonEnviadoAltDecode, JSON_PRETTY_PRINT) : '';
@endphp

<div class="row">
    <div class="col-md-4 text-bold">Retorno do PNCP</div>
    <div class="col-md-4 text-bold">Json enviado de inclusão</div>
    <div class="col-md-4 text-bold">Json enviado de alteração</div>
</div>
<div class="row">
    <div class="col-md-4" style="white-space: pre-line;overflow-wrap: break-word;overflow-x: scroll;">
        @if($jsonRetornoPncpFormatado)<code style="white-space: pre;">{!! strip_tags($jsonRetornoPncpFormatado) !!}</code>@endif
    </div>
    <div class="col-md-4" style="white-space: pre-line;overflow-wrap: break-word;">
        @if($jsonIncFormatado)<code style="white-space: pre;">{{$jsonIncFormatado}}</code>@endif
    </div>
    <div class="col-md-4" style="white-space: pre-line;overflow-wrap: break-word;">
        @if($jsonAltFormatado)<code style="white-space: pre;">{{$jsonAltFormatado}}</code>@endif
    </div>
</div>