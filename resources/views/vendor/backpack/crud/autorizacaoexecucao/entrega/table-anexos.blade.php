<table class="table table-bordered table-striped m-b-0">
    <thead>
    <tr>
        <th>Nome</th>
        <th>Descrição</th>
        <th>Arquivo</th>
    </tr>
    </thead>
    <tbody>
    @empty($anexos->count())
        <tr>
            <td colspan="8" style="text-align: center">Nenhum item adicionado</td>
        </tr>
    @else
        @foreach($anexos as $anexo)
            <tr>
                <td>{{ $anexo->nome }}</td>
                <td>{{ $anexo->descricao }}</td>
                <td>{!! $anexo->getViewLink() !!}</td>
            </tr>
        @endforeach
    @endempty
    </tbody>
</table>
