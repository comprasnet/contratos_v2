<div class="card">
    <div class="card-body">
        <!-- Section 1 -->
        <div class="row">
            <div class="col-md-3 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_numero_ano" bp-field-type="label">
                <div class="br-input">
                    <label for="label_numero_ano" class="mb-2">Contrato</label>
                    <br>
                    <span>{{ $autorizacaoDeExecucao->contrato->numero }}</span>
                </div>
            </div>
            <div class="col-md-3 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_unidade_gerenciadora" bp-field-type="label">
                <div class="br-input">
                    <label for="label_unidade_gerenciadora" class="mb-2">Fornecedor</label>
                    <br>
                    <span>{{ $autorizacaoDeExecucao->contrato->fornecedor->cpf_cnpj_idgener . ' - ' . $autorizacaoDeExecucao->contrato->fornecedor->nome }}</span>
                </div>
            </div>
            <div class="col-md-3 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_numero_compra" bp-field-type="label">
                <div class="br-input">
                    <label for="label_numero_compra" class="mb-2">Contratante</label>
                    <br>
                    <span>{{ $autorizacaoDeExecucao->contrato->unidade->cnpj . ' - ' . $autorizacaoDeExecucao->contrato->unidade->nomeresumido }}</span>
                </div>
            </div>
            <div class="col-md-3 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_modalidade_compra" bp-field-type="label">
                <div class="br-input">
                    <label for="label_modalidade_compra" class="mb-2">Objeto</label>
                    <br>
                    <span>{{ $autorizacaoDeExecucao->contrato->objeto }}</span>
                </div>
            </div>
        </div>

        <!-- Section 2 -->
        <div class="row">
            <div class="col-md-3 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_data_assinatura" bp-field-type="label">
                <div class="br-input">
                    <label for="label_data_assinatura" class="mb-2">Vigencia Inicial do Contrato</label>
                    <br>
                    <span>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $autorizacaoDeExecucao->contrato->vigencia_inicio)->format('d/m/Y') }}</span>
                </div>
            </div>
            <div class="col-md-3 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_data_vigencia_inicial" bp-field-type="label">
                <div class="br-input">
                    <label for="label_data_vigencia_inicial" class="mb-2">Vigência Final do Contrato</label>
                    <br>
                    <span>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $autorizacaoDeExecucao->contrato->vigencia_fim)->format('d/m/Y') }}</span>
                </div>
            </div>
            <div class="col-md-3 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_data_valor_total" bp-field-type="label">
                <div class="br-input">
                    <label for="label_data_valor_total" class="mb-2">Amparo Legal</label>
                    <br>
                    <span>{{ $autorizacaoDeExecucao->contrato->retornaAmparo() }}</span>
                </div>
            </div>
            <div class="col-md-3 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_data_vigencia_final" bp-field-type="label">
                <div class="br-input">
                    <label for="label_data_vigencia_final" class="mb-2">Número do Processo de Contratação</label>
                    <br>
                    <span>{{ $autorizacaoDeExecucao->contrato->processo }}</span>
                </div>
            </div>
        </div>
        <!-- Section 3: Prepostos e Gestores-->
        <div class="row">
            <div class="col-md-6 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_data_valor_total" bp-field-type="label">
                <div class="br-input">
                    <label for="label_data_valor_total" class="mb-2">Prepostos</label>
                    <br>
                    @if(!empty($autorizacaoDeExecucao->contrato->prepostos))
                        <div class="row">
                            <div class="col-md-6"><strong>Nome</strong></div>
                            <div class="col-md-6"><strong>CPF</strong></div>
                            @foreach($autorizacaoDeExecucao->contrato->prepostos as $preposto)
                                <div class="col-md-6">{{ $preposto->nome }}</div>
                                <div class="col-md-6">{{ '***' . substr($preposto->cpf, 3, 9) . '**' }}</div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-6 pt-3" element="div" bp-field-wrapper="true" bp-field-name="label_data_valor_total" bp-field-type="label">
                <div class="br-input">
                    <label for="label_data_valor_total" class="mb-2">Gestores</label>
                    <br>
                    @if(!empty($autorizacaoDeExecucao->contrato->responsaveis))
                        <div class="row">
                            <div class="col-md-6"><strong>Nome</strong></div>
                            <div class="col-md-6"><strong>Função</strong></div>
                            @foreach($autorizacaoDeExecucao->contrato->responsaveis as $responsavel)
                                <div class="col-md-6">{{ $responsavel->user->name }}</div>
                                <div class="col-md-6">{{ $responsavel->funcao->descricao }}</div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

