@extends(backpack_view('blank'))

<link href="packages/backpack/base/css/padrao_gov/all.min.css" rel="stylesheet" />
<link href="packages/backpack/base/css/padrao_gov/core.min.css" rel="stylesheet" />
<link href="packages/backpack/base/css/padrao_gov/rawline.css" rel="stylesheet" />
<link href="packages/backpack/base/css/padrao_gov/customize.css" rel="stylesheet" />


@php
  $dadosIcone = json_encode(['icone' => 'fas fa-angle-double-left' , 'url' => $crud->hasAccess('list') ? url($crud->route) : url()->previous()]);
  $defaultBreadcrumbs = [
    trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
    $crud->entity_name_plural => url($crud->route),
    trans('backpack::crud.preview') => false,
	// 'Icone' => $dadosIcone
  ];

  // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
  $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;

  //if preenchida aparecerá o texto de observação enviada na controller
  $crud->label_observacao_show = $crud->label_observacao_show ?? null;
@endphp

@section('header')
	<section class="container-fluid d-print-none">
    	<a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
		<h2>
	        {{-- <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span> --}}
	        {{-- <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name !!}.</small> --}}
	        @if ($crud->hasAccess('list'))
	          	<small class="">
					<!-- <button class="br-sign-in small" type="button" onclick=" window.location.href= '{{ url($crud->route) }}'">
						<i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }}
					</button> -->
					{{-- <a href="{{ url($crud->route) }}" class="font-sm">
						<i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }}
						<span>{{ $crud->entity_name_plural }} </span>
					</a> --}}
				</small>
	        @endif
	    </h2>
    </section>
@endsection

@section('content')
<div class="row">
	<div class="{{ $crud->getShowContentClass() }}">

	<!-- Default box -->
	  <div class="">
	  	@if ($crud->model->translationEnabled())
			<div class="row">
				<div class="col-md-12 mb-2">
					<!-- Change translation button group -->
					<div class="btn-group float-right">
					<button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						{{trans('backpack::crud.language')}}: {{ $crud->model->getAvailableLocales()[request()->input('_locale')?request()->input('_locale'):App::getLocale()] }} &nbsp; <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						@foreach ($crud->model->getAvailableLocales() as $key => $locale)
							<a class="dropdown-item" href="{{ url($crud->route.'/'.$entry->getKey().'/show') }}?_locale={{ $key }}">{{ $locale }}</a>
						@endforeach
					</ul>
					</div>
				</div>
			</div>
	    @endif
		@if (!empty($crud->label_observacao_show))
			<div class="br-message warning" role="alert">
				<div class="icon"><i class="fas fa-exclamation-triangle fa-lg" aria-hidden="true"></i>
				</div>
				<div class="content"><span class="message-title">Atenção.</span><span class="message-body"> {!! $crud->label_observacao_show !!}</span></div>
				<div class="close">
				<button class="br-button circle small" type="button" aria-label="Fechar"><i class="fas fa-times" aria-hidden="true"></i>
				</button>
				</div>
			</div>
		@endif
	    <div class="card no-padding no-border">
			<table class="table table-striped mb-0">
		        <tbody>
		        @foreach ($crud->columns() as $column)
		            <tr>
		                <td>
		                    <strong>{!! $column['label'] !!}:</strong>
		                </td>
                        <td>
                        	@php
                        		// create a list of paths to column blade views
                        		// including the configured view_namespaces
                        		$columnPaths = array_map(function($item) use ($column) {
                        			return $item.'.'.$column['type'];
                        		}, $crud->getViewNamespacesFor('columns'));

                        		// but always fall back to the stock 'text' column
                        		// if a view doesn't exist
                        		if (!in_array('crud::columns.text', $columnPaths)) {
                        			$columnPaths[] = 'crud::columns.text';
                        		}
                        	@endphp
													@includeFirst($columnPaths)
                        </td>
		            </tr>
		        @endforeach
				@if ($crud->buttons()->where('stack', 'line')->count())
					{{--<tr>
						<td><strong>{{ trans('backpack::crud.actions') }}</strong></td>
						<td>
							@include('crud::inc.button_stack', ['stack' => 'line'])
						</td>
					</tr>--}}
				@endif
		        </tbody>
			</table>
	    </div><!-- /.box-body -->
	  </div><!-- /.box -->

	</div>
</div>
@endsection
