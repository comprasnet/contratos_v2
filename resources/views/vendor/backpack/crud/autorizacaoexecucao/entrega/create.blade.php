@extends(backpack_view('blank_externo'))


@php
	$dadosIcone = json_encode(['icone' => 'fas fa-angle-double-left' , 'url' => $crud->hasAccess('list') ? url($crud->route) : url()->previous()]);

  $defaultBreadcrumbs = [
    trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
    $crud->entity_name_plural => url($crud->route),
    trans('backpack::crud.add') => false,
	// 'Icone' => $dadosIcone
  ];

  // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
  $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
  $crud->exibir_erro_end = $crud->exibir_erro_end ?? false;
@endphp

@section('header')
	<section class="container-fluid">
	  <h2>
        {{-- <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
        <small>{!! $crud->getSubheading() ?? trans('backpack::crud.add').' '.$crud->entity_name !!}.</small> --}}

        @if ($crud->hasAccess('list'))
          {{-- <small><a href="{{ url($crud->route) }}" class="d-print-none font-sm"><i class="la la-angle-double-{{ config('backpack.base.html_direction') == 'rtl' ? 'right' : 'left' }}"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small> --}}
		  	<!-- <button class="br-button secondary mr-3" type="button" onclick=" window.location.href= '{{ $crud->hasAccess('list') ? url($crud->route) : url()->previous()  }}'">
				<i class="la la-angle-double-{{ config('backpack.base.html_direction') == 'rtl' ? 'right' : 'left' }}"></i> Voltar para &nbsp;
				<span>{{ $crud->entity_name_plural }}</span>
			</button> -->
        @endif
	  </h2>
	</section>
@endsection

@section('content')

<div class="row">
	<div class="{{ $crud->getCreateContentClass() }}">
		@include('crud::autorizacaoexecucao.entrega.cabecalho-list-entrega', ['autorizacaoDeExecucao' => $crud->autorizacaodeexecucao])
		<!-- Default box -->
		@if (!$crud->exibir_erro_end)
			@include('crud::inc.grouped_errors')
		@endif
		  <form method="post"
				id="mainForm"
		  		action="{{ url($crud->route) }}"
				@if ($crud->hasUploadFields('create'))
				enctype="multipart/form-data"
				@endif
		  		>
			  {!! csrf_field() !!}
		      <!-- load the view from the application if it exists, otherwise load the one in the package -->
		      @if(view()->exists('vendor.backpack.crud.form_content'))
		      	@include('vendor.backpack.crud.form_content', [ 'fields' => $crud->fields(), 'action' => 'create' ])
		      @else
		      	@include('crud::form_content', [ 'fields' => $crud->fields(), 'action' => 'create' ])
		      @endif
                <!-- This makes sure that all field assets are loaded. -->
                <div class="d-none" id="parentLoadedAssets">{{ json_encode(Assets::loaded()) }}</div>
	          @include('crud::inc.form_save_buttons')
		  </form>
		@if ($crud->exibir_erro_end)
		  @include('crud::inc.grouped_errors')
	  	@endif
	</div>
</div>

@endsection

