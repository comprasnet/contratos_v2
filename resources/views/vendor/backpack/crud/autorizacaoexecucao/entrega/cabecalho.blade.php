<div class="row ">
    <h2 class="col-md-12 h6 my-0">Dados da Entrega</h2>

    <div class="form-group col-md-4">
        <div class="br-input">
            <label for="numero" class="mb-2">Número/Ano da Entrega</label>
            <br>
            <span>
                {{ $entrega->numero }}
            </span>
        </div>
    </div>
    <div class="form-group col-md-4" element="div" bp-field-wrapper="true" bp-field-name="data_entrega" bp-field-type="label">
        <div class="br-input">
            <label for="data_entrega" class="mb-2">Data da Entrega</label>
            <br>
            <span>
                {{ (new \DateTime($entrega->data_entrega))->format('d/m/Y') }}
            </span>
        </div>
    </div>
    <div class="form-group col-md-4" id="valor_total_label" element="div" bp-field-wrapper="true" bp-field-name="valor_total" bp-field-type="label">
        <div class="br-input">
            <label for="valor_total" class="mb-2">Valor da Entrega</label>
            <br>
            <span>
                {{ $entrega->getValorTotalFormatado() }}
            </span>
        </div>
    </div>
    <div class="form-group col-md-4" id="data_prevista_trp_label" element="div" bp-field-wrapper="true" bp-field-name="data_prevista_trp_label" bp-field-type="label">
        <div class="br-input">
            <label for="data_prevista_trp_label" class="mb-2">Previsão para o TRP</label>
            <br>
            <span>
                {{ (new \DateTime($entrega->data_prevista_trp))->format('d/m/Y') }}
            </span>
        </div>
    </div>
    <div class="form-group col-md-4" id="data_prevista_trd_label" element="div" bp-field-wrapper="true" bp-field-name="data_prevista_trd_label" bp-field-type="label">
        <div class="br-input">
            <label for="data_prevista_trd_label" class="mb-2">Previsão para o TRD</label>
            <br>
            <span>
                {{ (new \DateTime($entrega->data_prevista_trd))->format('d/m/Y') }}
            </span>
        </div>
    </div>
</div>
