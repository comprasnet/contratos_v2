@php
    $actions = !isset($actions) || $actions;
@endphp

<table class="table table-bordered table-striped m-b-0">
    <thead>
        <tr>
            <th>Nº Alteração</th>
            <th>Tipo Alteração</th>
            <th>Data Alteração</th>
            @if($actions)
                <th>Arquivo</th>
            @endif
        </tr>
    </thead>

    <tbody>
    @empty($historicos->count())
        <tr>
            <td colspan="8" style="text-align: center">Nenhuma alteração</td>
        </tr>
    @else
        @foreach($historicos as $historico)
            <tr>
                <td>{{ $historico->getSequencial() }}</td>
                <td>{{ $historico->getTipoAlteracao() }}</td>
                <td>{{ $historico->getDataInicioAlteracao() }}</td>
                @if($actions)
                    <td>{!! $historico->getArquivoViewLink() !!}</td>
                @endif
            </tr>
        @endforeach
    @endempty
    </tbody>
</table>
