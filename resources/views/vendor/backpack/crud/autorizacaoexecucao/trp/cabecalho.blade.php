<div class="row ">
    <h2 class="col-md-12 h6 my-0">Dados da Termo de Recebimento Provisório</h2>

    <div class="form-group col-md-4">
        <div class="br-input">
            <label for="numero" class="mb-2">Número/Ano</label>
            <br>
            <span>
                {{ $trp->numero }}
            </span>
        </div>
    </div>
    <div class="form-group col-md-4" element="div" bp-field-wrapper="true" bp-field-name="data" bp-field-type="label">
        <div class="br-input">
            <label for="data" class="mb-2">Data</label>
            <br>
            <span>
                {{ (new \DateTime($trp->created_at))->format('d/m/Y') }}
            </span>
        </div>
    </div>
    <div class="form-group col-md-4" id="valor_total_label" element="div" bp-field-wrapper="true" bp-field-name="valor_total" bp-field-type="label">
        <div class="br-input">
            <label for="valor_total" class="mb-2">Valor</label>
            <br>
            <span>
                {{ $trp->getValorTotalFormatado() }}
            </span>
        </div>
    </div>
</div>
