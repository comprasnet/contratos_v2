@php
$total = 0;
$hiddenColumns = $hidden ?? [];
$colspanBefore = count(
    array_diff(
        ['periodo', 'tipo_item', 'numero_item_compra', 'item', 'especificacao', 'unidade_medida_id', 'quantidade', 'parcela', 'quantidade_total'],
        $hiddenColumns,
    )
);
$colspanAfter = count(
    array_diff(
        ['horario_execucao', 'subcontratacao', 'numero_demanda_sistema_externo'],
        $hiddenColumns,
    )
);
@endphp

<table class="table table-bordered table-striped m-b-0">
    <thead>
        <tr>
            @if(!in_array('periodo', $hiddenColumns)) <th>{{ $fields['autorizacaoexecucaoItens']['label']['periodo']  }}</th> @endif
            @if(!in_array('tipo_item', $hiddenColumns)) <th>{{ $fields['autorizacaoexecucaoItens']['label']['tipo_item']  }}</th> @endif
            @if(!in_array('numero_item_compra', $hiddenColumns)) <th>{{ $fields['autorizacaoexecucaoItens']['label']['numero_item_compra']  }}</th> @endif
            @if(!in_array('item', $hiddenColumns)) <th>{{ $fields['autorizacaoexecucaoItens']['label']['item']  }}</th> @endif
            @if(!in_array('especificacao', $hiddenColumns)) <th>{{ $fields['autorizacaoexecucaoItens']['label']['especificacao']  }}</th> @endif
            @if(!in_array('unidade_medida_id', $hiddenColumns)) <th>{{ $fields['autorizacaoexecucaoItens']['label']['unidade_medida_id']  }}</th> @endif
            @if(!in_array('quantidade', $hiddenColumns)) <th>{{ $fields['autorizacaoexecucaoItens']['label']['quantidade']  }}</th> @endif
            @if(!in_array('parcela', $hiddenColumns)) <th>{{ $fields['autorizacaoexecucaoItens']['label']['parcela']  }}</th> @endif
            @if(!in_array('quantidade_total', $hiddenColumns)) <th>{{ $fields['autorizacaoexecucaoItens']['label']['quantidade_total']  }}</th> @endif
            @if(!in_array('valor', $hiddenColumns)) <th>{{ $fields['autorizacaoexecucaoItens']['label']['valor_unitario']  }}</th> @endif
            @if(!in_array('valor', $hiddenColumns)) <th>{{ $fields['autorizacaoexecucaoItens']['label']['valor_total']  }}</th> @endif
            @if(!in_array('horario_execucao', $hiddenColumns)) <th>Horário execução</th> @endif
            @if(!in_array('subcontratacao', $hiddenColumns)) <th>{{ $fields['autorizacaoexecucaoItens']['label']['subcontratacao']  }}</th> @endif
            @if(!in_array('numero_demanda_sistema_externo', $hiddenColumns)) <th>{{ $fields['autorizacaoexecucaoItens']['label']['numero_demanda_sistema_externo']  }}</th> @endif
        </tr>
    </thead>

    <tbody>
    @empty($fields['autorizacaoexecucaoItens']['value']->count())
        <tr>
            <td colspan="8" style="text-align: center">Nenhum item adicionado</td>
        </tr>
    @else
        @foreach($fields['autorizacaoexecucaoItens']['value'] as $item)
            <tr>
                 @if(!in_array('periodo', $hiddenColumns)) <td>{{ $item->periodo }}</td> @endif
                 @if(!in_array('tipo_item', $hiddenColumns)) <td>{{ $item->tipo_item }}</td> @endif
                 @if(!in_array('numero_item_compra', $hiddenColumns)) <td>{{ $item->numero_item_compra }}</td> @endif
                 @if(!in_array('item', $hiddenColumns)) <td>{{ $item->item }}</td> @endif
                 @if(!in_array('especificacao', $hiddenColumns)) <td>{{ $item->especificacao }}</td> @endif
                 @if(!in_array('unidade_medida_id', $hiddenColumns)) <td>{{ $item->unidadeMedida ? $item->unidadeMedida->nome : '' }}</td> @endif
                 @if(!in_array('quantidade', $hiddenColumns)) <td>{{ $item->quantidade }}</td> @endif
                 @if(!in_array('parcela', $hiddenColumns)) <td>{{ $item->parcela }}</td> @endif
                 @if(!in_array('quantidade_total', $hiddenColumns)) <td>{{ $item->getQuantidadeTotal() }}</td> @endif
                 @if(!in_array('valor', $hiddenColumns)) <td>R$ {{ $item->valor_unitario }}</td> @endif
                 @if(!in_array('valor', $hiddenColumns)) <td>R$ {{ $item->getTotal() }}</td> @endif
                 @if(!in_array('horario_execucao', $hiddenColumns)) <td><b>Início:</b> {{ $item->horario }} <br><b>Fim:</b> {{ $item->horario_fim }}</td> @endif
                 @if(!in_array('subcontratacao', $hiddenColumns)) <td> @if($item->subcontratada) Sim @else Não @endif</td> @endif
                 @if(!in_array('numero_demanda_sistema_externo', $hiddenColumns)) <td> {{ $item->numero_demanda_sistema_externo }}</td> @endif
            </tr>
            @php $total += (float) $item->getTotalSemFormatar(); @endphp
        @endforeach
    @endempty
    </tbody>
    @if(!empty($fields['autorizacaoexecucaoItens']['value']->count()) && !in_array('valor', $hiddenColumns))
        <tfoot>
            <tr class="bg-gray-cool-3">
                @if($colspanBefore)
                    <td colspan="{{ $colspanBefore }}"></td>
                @endif
                <td class="text-left"><b>Valor total:</b></td>
                <td><b>R$ {{ number_format($total, 2, ',', '.') }}</b></td>
                @if($colspanAfter)
                    <td colspan="{{ $colspanAfter }}"></td>
                @endif
            </tr>
        </tfoot>
    @endempty
</table>
