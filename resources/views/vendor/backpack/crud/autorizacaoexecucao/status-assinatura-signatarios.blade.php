<ul class="list-unstyled">
    @foreach($signtarios as $signtario)
        @php
            $status = $statusAssinatura->where('id', $signtario['status_assinatura_id'])->first();
            $descricao = $status->descricao;

            $classesStatus = 'text-warning fa fa-clock';
            if ($descricao == 'Assinado') {
                $classesStatus = 'text-success fa fa-check';
            } elseif ($descricao == 'Recusado') {
                $classesStatus = 'text-danger fa fa-times';
            }
        @endphp
        <li class="py-1" title="{{$status->descricao}} {{$signtario['data_operacao']}}">
            <span class="{{$classesStatus}}"></span> {{ucwords(mb_strtolower($signtario['nome']))}} - {{$signtario['funcao']}}
        </li>
    @endforeach
</ul>
