<div class="row ">
    <h2 class="col-md-12 h6 my-0">Dados da OS/F</h2>

    <div class="form-group col-md-4">
        <div class="br-input">
            <label for="numero" class="mb-2">Número/Ano da Ordem de Serviço / Fornecimento</label>
            <br>
            <span>
                {{ $osf->numero }}
            </span>
        </div>
    </div>
    <div class="form-group col-md-4" element="div" bp-field-wrapper="true" bp-field-name="tipo_id" bp-field-type="label">
        <div class="br-input">
            <label for="tipo_id" class="mb-2">Tipo</label>
            <br>
            <span>
                {{ $osf->getTipo() }}
            </span>
        </div>
    </div>
    <div class="form-group col-md-4" element="div" bp-field-wrapper="true" bp-field-name="processo" bp-field-type="label">
        <div class="br-input">
            <label for="processo" class="mb-2">Numero do processo SEI</label>
            <br>
            <span>
                {{ $osf->processo }}
            </span>
        </div>
    </div>
    <div class="form-group col-md-4" element="div" bp-field-wrapper="true" bp-field-name="data_assinatura" bp-field-type="label">
        <div class="br-input">
            <label for="data_assinatura" class="mb-2">Data de assinatura</label>
            <br>
            <span>
                {{ (new \DateTime($osf->data_assinatura))->format('d/m/Y') }}
            </span>
        </div>
    </div>
    <div class="form-group col-md-4" id="data_vigencia_inicio_label" element="div" bp-field-wrapper="true" bp-field-name="data_vigencia_inicio" bp-field-type="label">
        <div class="br-input">
            <label for="data_vigencia_inicio" class="mb-2">Vigência início</label>
            <br>
            <span>
                {{ (new \DateTime($osf->data_vigencia_inicio))->format('d/m/Y') }}
            </span>
        </div>
    </div>
    <div class="form-group col-md-4" id="data_vigencia_fim_label" element="div" bp-field-wrapper="true" bp-field-name="data_vigencia_fim_label" bp-field-type="label">
        <div class="br-input">
            <label for="data_vigencia_fim_label" class="mb-2">Vigência fim</label>
            <br>
            <span>
                {{ (new \DateTime($osf->data_vigencia_fim))->format('d/m/Y') }}
            </span>
        </div>
    </div>
    <div class="form-group col-md-4" element="div" bp-field-wrapper="true" bp-field-name="numero_sistema_origem" bp-field-type="label">
        <div class="br-input">
            <label for="numero_sistema_origem" class="mb-2">Número da ordem de serviço no sistema de origem</label>
            <br>
            <span>
                {{ $osf->numero_sistema_origem }}
            </span>
        </div>
    </div>
</div>
