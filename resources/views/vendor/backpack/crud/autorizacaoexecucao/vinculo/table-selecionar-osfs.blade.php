<table class="table dataTable dtr-inline">
    <thead>
        <tr>
            <th>
                <input type="checkbox" id="selecionar_todos_vinculos" title="Selecionar todos"/>
            </th>
            <th>Situação</th>
            <th>Número/Ano</th>
            <th>Vigência Início</th>
            <th>Vigência Fim</th>
            <th>Valor Total</th>
            <th>Evolução</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
    @empty(count($osfs))
        <tr>
            <td colspan="8" style="text-align: center">Nenhum item adicionado</td>
        </tr>
    @else
        @foreach($osfs as $osf)
            @php
                $checked = '';
                $disabled = '';
                if (request()->autorizacaoexecucao_id == $osf->id) {
                    $checked = 'checked';
                    $disabled = 'disabled';
                }
            @endphp
            <tr>
                <td>
                    <input
                        type="checkbox"
                        name="vinculos_selecionados[]"
                        value="{{ $osf->id }}"
                        class="vinculo_checkbox"
                        {{ $checked }}
                        {{ $disabled }}
                    />
                </td>
                <td>{{ $osf->getSituacaoChipComponent() }}</td>
                <td>{{ $osf->numero }}</td>
                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $osf->data_vigencia_inicio)->format('d/m/Y') }} </td>
                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $osf->data_vigencia_fim)->format('d/m/Y') }}</td>
                <td>{{ $osf->getValorTotal() }}</td>
                <td>
                    @include('crud::columns.progressbar-fornecedor-osf', ['entry' => $osf])
                </td>
                <td>
                    <a target="_blank" href="/autorizacaoexecucao/{{$osf->contrato_id}}/{{$osf->id}}/show">
                        <i class="fas fa-eye"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    @endempty
    </tbody>
</table>

@push('after_scripts')
    <script>
        $(document).ready(function () {
            $('#selecionar_todos_vinculos').click(function () {
                $('.vinculo_checkbox:not(:disabled)').prop('checked', $(this).prop('checked'));
            })
        })
    </script>
@endpush
