@php
    $display = empty($disabled) ? 'block' : 'none';
@endphp
<div id="vinculo-{{ $value }}" class="position-relative vinculo mt-3" style="display: {{ $display }}">
    <input type="hidden" name="vinculos[{{$key}}][id]" value="{{$value}}" {{ $disabled }}>
    <a
            class="remove-vinculo br-button circle secondary small position-absolute"
            href="javascript:void(0)"
            style="z-index: 10; right: 21px"
            title="Remover"
    >
        <i class="fas fa-trash"></i>
    </a>

    {{ $tableSelecionarVinculoItens }}
    <hr>
</div>

@once
    @push('after_scripts')
        <script src="/assets/js/numberField.js"></script>
        <script>
            $(document).ready(function () {
                $('.dtr-control').on('click', function() {
                    const trParent = $(this).parent('tr');
                    const trDetails = trParent.next();
                    trParent.toggleClass('parent');
                    trDetails.toggleClass('d-none');
                })

                $('.table-selecionar-vinculo-itens input').on('focusin', function(){
                    $(this).data('val', $(this).val());
                });

                $('.selecionar-todos-vinculos-itens').click(function () {
                    var checked = this.checked;
                    const tableVinculoItens = $(this).closest('.table-selecionar-vinculo-itens');
                    tableVinculoItens.find('.vinculo_item_checkbox')
                        .prop('checked', $(this).prop('checked'))
                        .each(function () {
                            toggleRowInputs($(this), checked)
                        })
                })

                $('.vinculo_item_checkbox').click(function() {
                    toggleRowInputs($(this), this.checked);
                });

                $('.quantidade_informada').on('input', function() {
                    const { tipoItem } = getValuesRow($(this).closest('tr'));
                    if (tipoItem === 'Serviço') {
                        $(this).decimal();
                    } else {
                        $(this).integer();
                    }
                }).change(function() {
                    const tableRow = $(this).closest('tr').prev();
                    const tableRowDetails = $(this).closest('tr');
                    const {
                        quantidadeContratada,
                        quantidadeOSF,
                        quantidadeSolicitada,
                        quantidadeInformada
                    } = getValuesRow(tableRowDetails);
                    const quantidadeTotalConsumida = parseFloat($(tableRow).find('.quantidade_total_consumida').val() ?? 0);
                    const percentual =  (quantidadeInformada + quantidadeTotalConsumida) * 100 / quantidadeSolicitada;

                    let mensagemAlert = null;
                    let resetValores = false;
                    if (!addValorTotalEntregaItem(tableRowDetails)) {
                        mensagemAlert = 'Valor total não pode ser negativo!';
                        resetValores = true
                    } else if (quantidadeInformada > quantidadeContratada) {
                        mensagemAlert = 'Existem Itens usando mais de 100% do valor Contratado!';
                    } else if (quantidadeInformada > quantidadeOSF) {
                        mensagemAlert = 'Existem Itens usando mais de 100% do valor OS/F!';
                    } else if (percentual > 100) {
                        mensagemAlert = 'Existem Itens usando mais de 100% do valor Solicitado!';
                    } else if (quantidadeInformada === 0) {
                        $(this).val(quantidadeInformada);
                    }

                    if (!!mensagemAlert) {
                        swal({
                            title: 'Atenção',
                            text: mensagemAlert,
                            type: 'warning',
                            buttons: {
                                confirm: {
                                    text: 'Ciente',
                                    className: 'br-button primary mr-3',
                                }
                            },
                        });

                        if (resetValores) {
                            $(this).val(($(this).data('val')));
                        }
                    }

                    if (!resetValores) {
                        const progressBar = $(tableRow).find('.progress-saldo');
                        $(progressBar).find('.pct').text($.floatBrRound(percentual, 2) + '%');
                        $(progressBar).find('progress').val(percentual).trigger('change');

                        const tooltipQuantidadeInformada = $(tableRow).find('.tooltip-quantidade-informada .value');
                        $(tooltipQuantidadeInformada).text(quantidadeInformada);

                        const tooltipQuantidadeDisponivel = $(tableRow).find('.tooltip-quantidade-disponivel .value');

                        const factor = 10000000
                        const quantidadeDisponivelConsumo = (quantidadeSolicitada * factor - quantidadeInformada * factor - quantidadeTotalConsumida * factor) / factor;
                        $(tableRow).find('.quantidade_disponivel_consumo').val(quantidadeDisponivelConsumo);
                        $(tooltipQuantidadeDisponivel).text(quantidadeDisponivelConsumo);

                        recalculaValorTotalVinculo();
                    }
                });

                $('.valor_glosa').on('input', function() {
                    $(this).decimal();
                }).change(function() {
                    if (!addValorTotalEntregaItem($(this).closest('tr'))) {
                        swal({
                            title: 'Atenção',
                            text: 'Valor total não pode ser negativo!',
                            type: 'warning',
                            buttons: {
                                confirm: {
                                    text: 'OK',
                                    className: 'br-button primary mr-3',
                                }
                            },
                        })
                        $(this).val(($(this).data('val')));
                    } else {
                        if (!$(this).val()) {
                            $(this).val(0);
                        }
                        recalculaValorTotalVinculo();
                    }
                });

                function toggleRowInputs(checkbox, enabled) {
                    checkbox.closest('tr').find('input').not('.vinculo_item_checkbox').prop('disabled', !enabled);
                    checkbox.closest('tr').next().find('input').not('.vinculo_item_checkbox').prop('disabled', !enabled);
                    recalculaValorTotalVinculo();
                }

                function addValorTotalEntregaItem(tableRow) {
                    const { quantidadeInformada, valorUnitario, valorGlosa } = getValuesRow(tableRow);
                    const resultado = quantidadeInformada * valorUnitario - valorGlosa;

                    if (resultado >= 0) {
                        $(tableRow).find('.valor_total_vinculo_item span').text('R$ ' + $.floatBrRound(resultado));
                        $(tableRow).find('.valor_total_vinculo_item_input').val(resultado);

                        return true;
                    }

                    return false;
                }

                function getValuesRow(tableRow) {
                    const tipoItem = $(tableRow).find('.tipo_item span').text();
                    const quantidadeContratada = $.ptBRToFloat($(tableRow).find('.quantidade_contratada span').text());
                    let quantidadeOSF = $.ptBRToFloat($(tableRow).find('.quantidade_osf span').text());
                    const quantidadeSolicitada = $.ptBRToFloat($(tableRow).find('.quantidade_solicitada span').text());
                    const quantidadeSolicitadaFormatado = $(tableRow).find('.quantidade_solicitada span').text();
                    const quantidadeInformada = $.ptBRToFloat($(tableRow).find('.quantidade_informada').val());
                    const quantidadeInformadaFormatado = $(tableRow).find('.quantidade_informada').val();
                    const valorUnitarioText = $(tableRow).find('.valor_unitario span').text();
                    const valorUnitario = parseFloat(valorUnitarioText.replace('R$', '').replace(',', '.').trim());
                    const valorUnitarioFormatado = $(tableRow).find('.valor_unitario span').text();
                    const valorGlosa = $.floatRound($(tableRow).find('.valor_glosa').val() ?? 0, 2);
                    const valorGlosaFormatado = $(tableRow).find('.valor_glosa').val() ?? 0;

                    if (quantidadeOSF === 0) {
                        quantidadeOSF = quantidadeSolicitada;
                    }

                    return {
                        tipoItem,
                        quantidadeContratada,
                        quantidadeOSF,
                        quantidadeSolicitada,
                        quantidadeSolicitadaFormatado,
                        quantidadeInformada,
                        quantidadeInformadaFormatado,
                        valorUnitario,
                        valorUnitarioFormatado,
                        valorGlosa,
                        valorGlosaFormatado
                    }
                }
            })
        </script>
    @endpush
    @push('after_styles')
        <style>
            .table-selecionar-vinculo-itens th, .table-selecionar-vinculo-itens td {
                align-content: center;
            }
        </style>
    @endpush
@endonce
