@php
    $osfIds = [];
    $itensAdicionados = collect();
    if ($crud->entry) {
        $osfIds = $crud->entry->autorizacoes->pluck('id')->toArray();
        $itensAdicionados = $crud->entry->itens;
    }
@endphp
<x-adicionar-vinculos
    title="Lista de OS/F para Composição da Entregas"
    textButton="Adicionar OS/F"
>
    <x-slot name="tableSelecionarVinculo">
        @include('crud::autorizacaoexecucao.vinculo.table-selecionar-osfs', ['osfs' => $opcoes_vinculos])
    </x-slot>

    <x-slot name="adicionarVinculoItens">
        @foreach($opcoes_vinculos as $key => $opcao_vinculo)
            @php
                $display = 'none';
                $disabled = 'disabled';
                if (in_array($opcao_vinculo->id, $osfIds) || old_empty_or_null("vinculos.$key.id") == $opcao_vinculo->id) {
                    $display = 'block';
                    $disabled = '';
                }
            @endphp
            <x-adicionar-vinculo-itens
                :value="$opcao_vinculo->id"
                :key="$key"
                :disabled="$disabled"
            >
                <x-slot name="tableSelecionarVinculoItens">
                    @include('crud::autorizacaoexecucao.cabecalho-osf', ['osf' => $opcao_vinculo])
                    @include(
                        'crud::autorizacaoexecucao.vinculo.table-selecionar-osf-itens',
                        [
                            'keyVinculo' => $key,
                            'itens' => $opcao_vinculo->autorizacaoexecucaoItens,
                            'itensAdicionados' => $itensAdicionados,
                            'disabledVinculo' => $disabled
                        ]
                    )
                </x-slot>
            </x-adicionar-vinculo-itens>
        @endforeach
    </x-slot>
</x-adicionar-vinculos>
