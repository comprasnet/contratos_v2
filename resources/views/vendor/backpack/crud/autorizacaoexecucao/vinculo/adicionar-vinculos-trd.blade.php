@php
    $trpIds = [];
    $itensAdicionados = collect();
    if ($crud->entry) {
        $trpIds = $crud->entry->termosRecebimentoProvisorio->pluck('id')->toArray();
        $itensAdicionados = $crud->entry->itens;
    }
@endphp
<x-adicionar-vinculos
    title="Lista de TRP para Composição do TRD"
    textButton="Adicionar TRP"
>
    <x-slot name="tableSelecionarVinculo">
        @include('crud::autorizacaoexecucao.vinculo.table-selecionar-trps', ['trps' => $opcoes_vinculos])
    </x-slot>

    <x-slot name="adicionarVinculoItens">
        @foreach($opcoes_vinculos as $key => $vinculo)
            @php
                $display = 'none';
                $disabled = 'disabled';
                if (in_array($vinculo->id, $trpIds) || old_empty_or_null("vinculos.$key.id") == $vinculo->id) {
                    $display = 'block';
                    $disabled = '';
                }
                $aeItens = $vinculo->autorizacaoItens;
                $itensTRP = $vinculo->itens;
            @endphp
            <x-adicionar-vinculo-itens
                :value="$vinculo->id"
                :key="$key"
                :disabled="$disabled"
            >
                <x-slot name="tableSelecionarVinculoItens">
                    @include(
                        'crud::autorizacaoexecucao.vinculo.table-selecionar-trp-itens',
                        [
                            'keyVinculo' => $key,
                            'vinculo' => $vinculo,
                            'itens' => $aeItens,
                            'itensTRP' => $itensTRP,
                            'itensAdicionados' => $itensAdicionados,
                            'disabledVinculo' => $disabled
                        ]
                    )
                </x-slot>
            </x-adicionar-vinculo-itens>
        @endforeach
    </x-slot>
</x-adicionar-vinculos>
