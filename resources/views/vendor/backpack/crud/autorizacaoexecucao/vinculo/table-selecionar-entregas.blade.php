<table class="table dataTable dtr-inline">
    <thead>
    <tr>
        <th>
            <input type="checkbox" id="selecionar_todos_vinculos" title="Selecionar todos"/>
        </th>
        <th>Situação</th>
        <th>Número</th>
        <th>OS/Fs</th>
        <th>Valor Total</th>
        <th>Ações</th>
    </tr>
    </thead>
    <tbody>
    @empty(count($entregas))
        <tr>
            <td colspan="8" style="text-align: center">Nenhum item adicionado</td>
        </tr>
    @else
        @foreach($entregas as $entrega)
            @php
                $checked = '';
                $disabled = '';
                if (request()->entrega_id == $entrega->id) {
                    $checked = 'checked';
                    $disabled = 'disabled';
                }
            @endphp
            <tr>
                <td>
                    <input
                            type="checkbox"
                            name="vinculos_selecionados[]"
                            value="{{ $entrega->id }}"
                            class="vinculo_checkbox"
                            {{ $checked }}
                            {{ $disabled }}
                    />
                </td>
                <td>{{ $entrega->getSituacaoChipComponent() }}</td>
                <td>{{ $entrega->numero }}</td>
                <td>{{ $entrega->getRedirectAutorizacoesComponent() }}</td>
                <td>{{ $entrega->getValorTotalFormatado() }}</td>
                <td>
                    <a target="_blank" href="/entrega/{{$entrega->contrato_id}}/{{$entrega->id}}/show">
                        <i class="fas fa-eye"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    @endempty
    </tbody>
</table>

@push('after_scripts')
    <script>
        $(document).ready(function () {
            $('#selecionar_todos_vinculos').click(function () {
                $('.vinculo_checkbox:not(:disabled)').prop('checked', $(this).prop('checked'));
            })
        })
    </script>
@endpush
