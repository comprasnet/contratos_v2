<div class="br-accordion" single="single">
    <div class="item" active>
        <button class="header" type="button" aria-controls="selecionar-vinculos">
            <span class="icon">
                <i class="fas fa-angle-down" aria-hidden="true"></i>
            </span>
            <span class="title">
                {{ $title }}
            </span>
        </button>
    </div>
    <div class="content px-2" id="selecionar-vinculos">
        {{ $tableSelecionarVinculo }}

        @error('vinculos')
        <p class="invalid-feedback d-block" role="alert">{{ $message }}</p>
        @enderror
        <div class="my-2">
            <button id="adicionar-vinculos" class="br-button primary" type="button">
                <i class="fas fa-plus"></i> {{ $textButton }}
            </button>
        </div>
    </div>
</div>

{{ $adicionarVinculoItens }}

<div class="d-flex justify-content-end p-3" style="background-color: #c1c1c1">
    <p class="h6">
        <strong>Valor Total:</strong>
        <span id="valor_total"></span>
        <input type="hidden" name="valor_total" id="valor_total_input">
    </p>
</div>

@once
    @push('after_scripts')
        <script>
            function recalculaValorTotalVinculo() {
                let valorTotal = 0
                $('.vinculo_item_checkbox:checked:not(:disabled)').each((index, input) => {
                    valorTotal += parseFloat($(input).closest('tr').next().find('.valor_total_vinculo_item_input').val())
                });
                $('#valor_total').text('R$ ' + $.floatBrRound(valorTotal, 2));
                $('#valor_total_input').val(valorTotal);
            }

            $(document).ready(function () {
                $('#adicionar-vinculos').click(function () {
                    if ($('.vinculo_checkbox:checked').length >= 1) {
                        $('.vinculo_checkbox:checked').each(function () {
                            const vinculoId = $(this).val();
                            const vinculo = $('#vinculo-' + vinculoId);
                            vinculo.slideDown();
                            vinculo.children('input').each((index, input) => input.removeAttribute('disabled'))
                            vinculo.find('input.vinculo_item_checkbox, .vinculo_field').each((index, input) => input.removeAttribute('disabled'))
                        })

                        $('#selecionar_todos_vinculos').prop('checked', false);
                        $('.vinculo_checkbox:not(:disabled)').prop('checked', false);

                    } else {
                        swal({
                            title: "Atenção",
                            text: "Selecione ao menos um vínculo para adicionar!",
                            buttons: {
                                confirm: {
                                    text: 'Confirmar',
                                    className: 'br-button primary',
                                },
                            }
                        })
                    }
                });

                $('.remove-vinculo').click(function () {
                    swal({
                        title: "Atenção",
                        text: "Deseja remover este vínculo? Caso confirme os itens inseridos serão excluídos",
                        buttons: {
                            cancel: {
                                text: 'Cancelar',
                                value: null,
                                visible: true,
                                className: 'br-button secondary mr-3',
                            },
                            confirm: {
                                text: 'Confirmar',
                                className: 'br-button primary',
                            },
                        },
                    }).then((value) => {
                        if (value) {
                            $(this).parent().slideUp();
                            $(this).parent().find('input, .vinculo_field').each((index, input) => $(input).attr('disabled', 'disabled'))
                        }
                        recalculaValorTotalVinculo();
                    })
                });

                recalculaValorTotalVinculo();
            });
        </script>
    @endpush
@endonce
