@php
    $valorTotalVinculo = 0;
@endphp
<table class="table table-bordered table-striped m-b-0">
    <thead>
    <tr>
        <th>OS/F</th>
        <th>N.º item</th>
        <th>Item</th>
        <th>Tipo item</th>
        <th>Unidade de Fornecimento</th>
        <th>Valor Unitário</th>
        <th>Quantidade Informada</th>
        <th>Glosa</th>
        <th>Valor Total</th>
    </tr>
    </thead>
    <tbody>
    @empty($itens)
        <tr>
            <td colspan="8" style="text-align: center">Nenhum item adicionado</td>
        </tr>
    @else
        @foreach ($itens as $item)
            @php
                $valorTotalItem = ($item->pivot->quantidade_informada * $item->valor_unitario_sem_formatar) - $item->pivot->valor_glosa;
                $valorTotalVinculo += $valorTotalItem;
            @endphp
            <tr>
                <td>
                    @include(
                        'crud::columns.redirect-autorizacoes',
                        [
                            'autorizacoes' => collect([$item->autorizacaoexecucao])
                        ]
                    )
                </td>
                <td>{{ $item->numero_item_compra }}</td>
                <td>{{ $item->item }}</td>
                <td>{{ $item->tipo_item }}</td>
                <td>{{ $item->unidadeMedida ? $item->unidadeMedida->nome : '' }}</td>
                <td>R$ {{ $item->valor_unitario }}</td>
                <td>{{ rtrim($item->pivot->quantidade_informada, '0') }}</td>
                <td>R$ {{ number_format($item->pivot->valor_glosa, 2, ',', '.') }}</td>
                <td>R$ {{ number_format($valorTotalItem, 2, ',', '.') }}</td>
            </tr>
        @endforeach
    @endempty
    </tbody>
    <tfoot>
    <tr class="bg-gray-cool-3">
        <td colspan="8" class="text-right"><b>Valor total:</b></td>
        <td><b>R$ {{ number_format($valorTotalVinculo, 2, ',', '.') }}</b></td>
    </tr>
    </tfoot>
</table>
