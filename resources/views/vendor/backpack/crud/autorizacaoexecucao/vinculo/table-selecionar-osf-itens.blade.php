@php
    $entregaItemRepository = new \App\Repositories\EntregaTrpTrd\EntregaItemRepository();
@endphp
<table class="table-selecionar-vinculo-itens table dataTable dtr-inline collapsed">
    <thead>
    <tr>
        <th>#</th>
        <th>
            <input type="checkbox" class="selecionar-todos-vinculos-itens" title="Selecionar todos"/>
        </th>
        <th>N.º item</th>
        <th>Item</th>
        <th>Consumo</th>
    </tr>
    </thead>
    <tbody>
    @foreach($itens as $keyItem => $item)
        @php
            $itemAdicionado = $itensAdicionados->count() ? $itensAdicionados->where('itemable_id', $item->id)->first() : false;
            $checked = '';
            $disabled = $disabledVinculo ? 'disabled' : '';
            $disabledFields = 'disabled';
            $quantidadeContratada = str_replace('.', ',', (float) $item->saldohistoricoitem->quantidade);
            $quantidadeSolicitada = str_replace('.', ',', (float) $item->quantidade);
            $quantidadeInformada = old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.quantidade_informada") ?? 0;
            $valorTotalItemEntrega = 0;
            $valorGlosa = old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.valor_glosa") ?? 0;
            if ($itemAdicionado || old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.id") == $item->id) {
                $checked = 'checked';
                $disabled = '';
                $disabledFields = '';
                $quantidadeInformada = old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.quantidade_informada") ??
                    str_replace('.', ',', rtrim($itemAdicionado->quantidade_informada, 0));
                $valorGlosa = old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.valor_glosa") ??
                    str_replace('.', ',', $itemAdicionado->valor_glosa);
            }

            $quantidadeInformadaSemFormatar = str_replace(['.', ','], ['', '.'], $quantidadeInformada);

            $valorTotalItemEntrega = $quantidadeInformadaSemFormatar *
                $item->valor_unitario_sem_formatar -
                str_replace(['.', ','], ['', '.'], $valorGlosa);

            $quantidadeTotalConsumida = (float) $entregaItemRepository->getQuantidadeTotalConsumida(
                $item->id,
                \App\Models\AutorizacaoexecucaoItens::class,
                request()->id ?? 0
            );
            $porcentagemSaldoConsumo = ($quantidadeInformadaSemFormatar + $quantidadeTotalConsumida)  * 100 / $item->getQuantidadeTotalSemFormatar();
            $porcentagemSaldoConsumo = floor($porcentagemSaldoConsumo);
            $quantidadeDisponivelConsumo = $item->getQuantidadeTotalSemFormatar() - ($quantidadeTotalConsumida + $quantidadeInformadaSemFormatar);
        @endphp
        <tr>
            <td class="dtr-control"></td>
            <td>
                <input
                        type="checkbox"
                        name="vinculos[{{$keyVinculo}}][itens][{{$keyItem}}][id]"
                        class="vinculo_item_checkbox"
                        value="{{ $item->id }}"
                        {{ $checked }}
                        {{ $disabled }}
                />
            </td>
            <td>{{ $item->numero_item_compra }}</td>
            <td>{{ $item->item }}</td>
            <td>
                <input class="quantidade_total_consumida" type="hidden" value="{{ $quantidadeTotalConsumida }}"
                       readonly/>
                <input
                        class="quantidade_disponivel_consumo"
                        name="vinculos[{{ $keyVinculo }}][itens][{{ $keyItem }}][quantidade_disponivel_consumo]"
                        type="hidden"
                        value="{{ $quantidadeDisponivelConsumo }}"
                        readonly
                        {{ $disabledFields }}
                />
                <x-progress-bar :percentual="$porcentagemSaldoConsumo">
                    <x-slot name='tooltip'>
                        <div
                                class="br-tooltip text-justify flex-column"
                                role="tooltip"
                                info="info"
                                place="left"
                        >
                            <div class="tooltip-quantidade-solicitada d-flex flex-wrap align-items-center subtext">
                                <b>Qtde. solicitada: </b> <span
                                        class="value ml-2 mb-0">{{ $item->getQuantidadeTotal() }}</span>
                            </div>
                            <hr class="m-0 p-0">
                            <div class="tooltip-quantidade-informada d-flex flex-wrap align-items-center subtext">
                                <b>Qtde. informada (Atual): </b> <span
                                        class="value ml-2 mb-0">{{ $quantidadeInformada }}</span>
                            </div>
                            <div class="tooltip-quantidade-consumida d-flex flex-wrap align-items-center subtext">
                                <b>Qtde. consumida (Anteriores): </b> <span
                                        class="value ml-2 mb-0">{{ $quantidadeTotalConsumida }}</span>
                            </div>
                            <div class="tooltip-quantidade-disponivel d-flex flex-wrap align-items-center subtext">
                                <b>Qtde. disponível: </b> <span
                                        class="value ml-2 mb-0">{{ $quantidadeDisponivelConsumo }}</span>
                            </div>
                        </div>
                    </x-slot>
                </x-progress-bar>
            </td>
        </tr>
        <tr class="d-none">
            <td colspan="5">
                <div class="row mb-2">
                    @include(
                        'crud::fields.label',
                        [
                            'field' => [
                                'label' => 'Tipo Item',
                                'type' => 'label',
                                'name' => 'label_tipo_item',
                                'value' => $item->tipo_item,
                                'wrapperAttributes' => [
                                    'class' => 'tipo_item col-md-3',
                                ]
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.label',
                        [
                            'field' => [
                                'label' => 'Unidade de Fornecimento',
                                'type' => 'label',
                                'name' => 'label_unidade_fornecimento',
                                'value' => $item->unidadeMedida->nome,
                                'wrapperAttributes' => [
                                    'class' => 'col-md-3',
                                ],
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.label',
                        [
                            'field' => [
                                'label' => 'Quantidade Contratada',
                                'type' => 'label',
                                'name' => 'label_quantidade_contratada',
                                'value' => $quantidadeContratada,
                                'wrapperAttributes' => [
                                    'class' => 'quantidade_contratada col-md-3',
                                ],
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.label',
                        [
                            'field' => [
                                'label' => 'Quantidade Solicitada (OS/F)',
                                'type' => 'label',
                                'name' => 'label_quantidade_solicitada',
                                'value' => $item->getQuantidadeTotal(),
                                'wrapperAttributes' => [
                                    'class' => 'quantidade_solicitada col-md-3',
                                ],
                            ]
                        ]
                    )
                </div>
                <div class="row">
                    @include(
                        'crud::fields.text',
                        [
                            'field' => [
                                'label' => 'Quantidade Informada',
                                'name' => "vinculos[$keyVinculo][itens][$keyItem][quantidade_informada]",
                                'type' => 'text',
                                'value' => $quantidadeInformada,
                                'wrapperAttributes' => [
                                    'class' => 'quantidade_solicitada col-md-3',
                                ],
                                'attributes' => array_merge(
                                    [
                                        'style' => 'min-width: 100px;',
                                        'class' => 'quantidade_informada',
                                        'data-key' => $keyItem,
                                    ],
                                    (function () use ($disabledFields) : array  {
                                        if (!empty($disabledFields)) {
                                            return ['disabled' => 'disabled'];
                                        }
                                        return [];
                                    })()
                                )
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.text',
                        [
                            'field' => [
                                'label' => 'Valor Glosa',
                                'name' => "vinculos[$keyVinculo][itens][$keyItem][valor_glosa]",
                                'type' => 'text',
                                'value' => $valorGlosa,
                                'wrapperAttributes' => [
                                    'class' => 'quantidade_solicitada col-md-3',
                                ],
                                'attributes' => array_merge(
                                    [
                                        'class' => "valor_glosa",
                                        'data-key' => $keyItem
                                    ],
                                    (function () use ($disabledFields) : array  {
                                        if (!empty($disabledFields)) {
                                            return ['disabled' => 'disabled'];
                                        }
                                        return [];
                                    })()
                                )
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.label',
                        [
                            'field' => [
                                'label' => 'Valor Unitário',
                                'type' => 'label',
                                'name' => 'label_valor_unitario',
                                'value' => $item->valor_unitario,
                                'wrapperAttributes' => [
                                    'class' => 'valor_unitario col-md-3',
                                ],
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.label',
                        [
                            'field' => [
                                'label' => 'Valor Total',
                                'type' => 'label',
                                'name' => 'label_valor_total',
                                'value' => number_format($valorTotalItemEntrega, 2, ',', '.'),
                                'wrapperAttributes' => [
                                    'class' => 'valor_total_vinculo_item col-md-3',
                                ],
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.hidden',
                        [
                            'field' => [
                                'label' => null,
                                'type' => 'hidden',
                                'name' => 'label_valor_total',
                                'value' => $valorTotalItemEntrega,
                                'attributes' => [
                                    'disabled' => 'disabled',
                                    'class' => 'valor_total_vinculo_item_input'
                                ]
                            ]
                        ]
                    )
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
