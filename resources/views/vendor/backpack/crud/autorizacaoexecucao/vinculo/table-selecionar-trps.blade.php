<table class="table dataTable dtr-inline">
    <thead>
    <tr>
        <th>
            <input type="checkbox" id="selecionar_todos_vinculos" title="Selecionar todos"/>
        </th>
        <th>Situação</th>
        <th>Número</th>
        <th>OS/Fs</th>
        <th>Valor Total</th>
        <th>Ações</th>
    </tr>
    </thead>
    <tbody>
    @empty(count($trps))
        <tr>
            <td colspan="8" style="text-align: center">Nenhum item adicionado</td>
        </tr>
    @else
        @foreach($trps as $trp)
            @php
                $checked = '';
                $disabled = '';
                if (request()->trp_id == $trp->id) {
                    $checked = 'checked';
                    $disabled = 'disabled';
                }
            @endphp
            <tr>
                <td>
                    <input
                            type="checkbox"
                            name="vinculos_selecionados[]"
                            value="{{ $trp->id }}"
                            class="vinculo_checkbox"
                            {{ $checked }}
                            {{ $disabled }}
                    />
                </td>
                <td>{{ $trp->getSituacaoChipComponent() }}</td>
                <td>{{ $trp->numero }}</td>
                <td>{{ $trp->getRedirectAutorizacoesComponent() }}</td>
                <td>{{ $trp->getValorTotalFormatado() }}</td>
                <td>
                    <a target="_blank" href="/trp/{{$trp->contrato_id}}/{{$trp->id}}/show">
                        <i class="fas fa-eye"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    @endempty
    </tbody>
</table>

@push('after_scripts')
    <script>
        $(document).ready(function () {
            $('#selecionar_todos_vinculos').click(function () {
                $('.vinculo_checkbox:not(:disabled)').prop('checked', $(this).prop('checked'));
            })
        })
    </script>
@endpush
