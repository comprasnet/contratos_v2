@php
    $entregaIds = [];
    $itensAdicionados = collect();
    if ($crud->entry) {
        $entregaIds = $crud->entry->entregas->pluck('id')->toArray();
        $itensAdicionados = $crud->entry->itens;
    }
@endphp
<x-adicionar-vinculos
    title="Lista de Entregas para Composição do TRP"
    textButton="Adicionar Entrega"
>
    <x-slot name="tableSelecionarVinculo">
        @include('crud::autorizacaoexecucao.vinculo.table-selecionar-entregas', ['entregas' => $opcoes_vinculos])
    </x-slot>

    <x-slot name="adicionarVinculoItens">
        @foreach($opcoes_vinculos as $key => $vinculo)
            @php
                $display = 'none';
                $disabled = 'disabled';
                if (in_array($vinculo->id, $entregaIds) || old_empty_or_null("vinculos.$key.id") == $vinculo->id) {
                    $display = 'block';
                    $disabled = '';
                }
            @endphp
            <x-adicionar-vinculo-itens
                :value="$vinculo->id"
                :key="$key"
                :disabled="$disabled"
            >
                <x-slot name="tableSelecionarVinculoItens">
                    @include(
                        'crud::autorizacaoexecucao.vinculo.table-selecionar-entrega-itens',
                        [
                            'keyVinculo' => $key,
                            'vinculo' => $vinculo,
                            'itens' => $vinculo->autorizacaoItens,
                            'itensAdicionados' => $itensAdicionados,
                            'disabledVinculo' => $disabled
                        ]
                    )
                </x-slot>
            </x-adicionar-vinculo-itens>
        @endforeach
    </x-slot>
</x-adicionar-vinculos>
