@php
    $trpItemRepository = new \App\Repositories\EntregaTrpTrd\TermoRecebimentoProvisorioItemRepository();
@endphp

@include('crud::autorizacaoexecucao.entrega.cabecalho', ['entrega' => $vinculo])

<div class="d-flex">
    @include(
        'crud::fields.select2_from_array',
        [
            'field' => [
                'label' => 'Locais de Execução para Entrega',
                'name' => "vinculos[$keyVinculo][locais_execucao]",
                'type' => 'select2_from_array',
                'model' => 'App\Models\ContratoLocalExecucao',
                'value' => $vinculo->locaisExecucao()->pluck('id')->toArray(),
                'options' => $vinculo->contrato->locaisExecucao()->pluck('descricao', 'id')->toArray(),
                'allows_multiple' => true,
                'wrapperAttributes' => ['class' => 'form-group col-md-6 pl-0 pr-1'],
                'attributes' => array_merge(
                    [
                        'class' => 'vinculo_field locais_execucao',
                        'data-key' => $keyVinculo,
                    ],
                    (function () use ($disabledVinculo) : array  {
                        if (!empty($disabledVinculo)) {
                            return ['disabled' => 'disabled'];
                        }
                        return [];
                    })()
                )
            ]
        ]
    )

    @include(
        'crud::fields.date_month',
        [
            'field' => [
                'label' => 'Mês/Ano de Referência',
                'name' => "vinculos[$keyVinculo][mes_ano_referencia]",
                'type' => 'date_month',
                'value' => $vinculo->mes_ano_referencia,
                'wrapperAttributes' => ['class' => 'form-group col-md-6 pl-1 pr-0'],
                'required' => true,
                'attributes' => array_merge(
                    [
                        'class' => 'vinculo_field mes_ano_referencia',
                        'data-key' => $keyVinculo,
                    ],
                    (function () use ($disabledVinculo) : array  {
                        if (!empty($disabledVinculo)) {
                            return ['disabled' => 'disabled'];
                        }
                        return [];
                    })()
                )
            ]
        ]
    )
</div>

<table class="table-selecionar-vinculo-itens table dataTable dtr-inline collapsed">
    <thead>
    <tr>
        <th>#</th>
        <th>
            <input type="checkbox" class="selecionar-todos-vinculos-itens" title="Selecionar todos"/>
        </th>
        <th>OS/F</th>
        <th>N.º item</th>
        <th>Item</th>
        <th>Consumo</th>
    </tr>
    </thead>
    <tbody>
    @foreach($itens as $keyItem => $item)
        @php
            $itemAdicionado = $itensAdicionados->count() ?
                $itensAdicionados
                    ->where('itemable_id', $item->id)
                    ->where('entrega_item_id', $item->pivot->id)
                    ->first() :
                false;
            $checked = '';
            $disabled = $disabledVinculo ? 'disabled' : '';
            $disabledFields = 'disabled';
            $quantidadeContratada = str_replace('.', ',',(float) $item->saldohistoricoitem->quantidade);
            $quantidadeOSF = str_replace('.', ',',(float) ($item->getQuantidadeTotal()));
            $quantidadeSolicitada = str_replace('.', ',',(float) $item->pivot->quantidade_informada);
            $quantidadeInformada = old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.quantidade_informada") ?? 0;
            $valorGlosa = old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.valor_glosa") ?? 0;
            $mesAnoCompetencia = old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.mes_ano_competencia");
            $processoSei = old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.processo_sei");
            $dataInicio = old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.data_inicio");
            if ($dataInicio) {
                $dataInicio = \Carbon\Carbon::createFromFormat('d/m/Y', $dataInicio)->format('Y-m-d');
            }
            $dataFim = old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.data_fim");
            if ($dataFim) {
                $dataFim = \Carbon\Carbon::createFromFormat('d/m/Y', $dataFim)->format('Y-m-d');
            }
            $horario = old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.horario");

            if ($itemAdicionado || old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.id") == $item->id) {
                $checked = 'checked';
                $disabled = '';
                $disabledFields = '';
                $quantidadeInformada = old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.quantidade_informada") ??
                    str_replace('.', ',', rtrim($itemAdicionado->quantidade_informada, 0));
                $valorGlosa = old_empty_or_null("vinculos.$keyVinculo.itens.$keyItem.valor_glosa") ??
                    str_replace('.', ',', $itemAdicionado->valor_glosa);
                $mesAnoCompetencia = $mesAnoCompetencia ?? $itemAdicionado->mes_ano_competencia;
                $processoSei = $processoSei ?? $itemAdicionado->processo_sei;
                $dataInicio = $dataInicio ?? $itemAdicionado->data_inicio;
                $dataFim = $dataFim ?? $itemAdicionado->data_fim;
                $horario = $horario ?? $itemAdicionado->horario;
            }

            $quantidadeSolicitadaSemFormatar = str_replace(['.', ','], ['', '.'], $quantidadeSolicitada);
            $quantidadeInformadaSemFormatar = str_replace(['.', ','], ['', '.'], $quantidadeInformada);
            $valorGlosaSemFormatar = str_replace(['.', ','], ['', '.'], $valorGlosa);

            $valorTotalItem = $quantidadeInformadaSemFormatar * $item->valor_unitario_sem_formatar -
                $valorGlosaSemFormatar;

            $quantidadeTotalConsumida = (float) $trpItemRepository->getQuantidadeTotalConsumida(
                $item->id,
                \App\Models\AutorizacaoexecucaoItens::class,
                $item->pivot->id,
                request()->id ?? 0
            );

            $porcentagemSaldoConsumo = ($quantidadeInformadaSemFormatar + $quantidadeTotalConsumida)  * 100 / $quantidadeSolicitadaSemFormatar;
            $porcentagemSaldoConsumo = floor($porcentagemSaldoConsumo);
            $quantidadeDisponivelConsumo = $quantidadeSolicitadaSemFormatar - ($quantidadeTotalConsumida + $quantidadeInformadaSemFormatar);
        @endphp
        <tr>
            <td class="dtr-control"></td>
            <td>
                <input
                        type="checkbox"
                        name="vinculos[{{$keyVinculo}}][itens][{{$keyItem}}][id]"
                        class="vinculo_item_checkbox"
                        value="{{ $item->id }}"
                        {{ $checked }}
                        {{ $disabled }}
                />
                <input
                        type="hidden"
                        name="vinculos[{{$keyVinculo}}][itens][{{$keyItem}}][entrega_item_id]"
                        value="{{ $item->pivot->id }}"
                        {{ $disabledFields }}
                >
            </td>
            <td>

                @include(
                    'crud::columns.redirect-autorizacoes',
                    [
                        'autorizacoes' => collect([$item->autorizacaoexecucao])
                    ]
                )
            </td>
            <td>{{ $item->numero_item_compra }} </td>
            <td>{{ $item->item }}</td>
            <td>
                <input class="quantidade_total_consumida" type="hidden" value="{{ $quantidadeTotalConsumida }}"
                       readonly/>
                <x-progress-bar :percentual="$porcentagemSaldoConsumo">
                    <x-slot name='tooltip'>
                        <div
                                class="br-tooltip text-justify flex-column"
                                role="tooltip"
                                info="info"
                                place="left"
                        >
                            <div class="tooltip-quantidade-solicitada d-flex flex-wrap align-items-center subtext">
                                <b>Qtde. Entregue: </b> <span
                                        class="value ml-2 mb-0">{{ $quantidadeSolicitada }}</span>
                            </div>
                            <hr class="m-0 p-0">
                            <div class="tooltip-quantidade-informada d-flex flex-wrap align-items-center subtext">
                                <b>Qtde. informada (Atual): </b> <span
                                        class="value ml-2 mb-0">{{ $quantidadeInformada }}</span>
                            </div>
                            <div class="tooltip-quantidade-consumida d-flex flex-wrap align-items-center subtext">
                                <b>Qtde. consumida (Anteriores): </b> <span
                                        class="value ml-2 mb-0">{{ $quantidadeTotalConsumida }}</span>
                            </div>
                            <div class="tooltip-quantidade-disponivel d-flex flex-wrap align-items-center subtext">
                                <b>Qtde. disponível: </b> <span
                                        class="value ml-2 mb-0">{{ $quantidadeDisponivelConsumo }}</span>
                            </div>
                        </div>
                    </x-slot>
                </x-progress-bar>
            </td>
        </tr>
        <tr class="d-none">
            <td colspan="6">
                <div class="row mb-2">
                    @include(
                        'crud::fields.label',
                        [
                            'field' => [
                                'label' => 'Tipo Item',
                                'type' => 'label',
                                'name' => 'label_tipo_item',
                                'value' => $item->tipo_item,
                                'wrapperAttributes' => [
                                    'class' => 'tipo_item col-md-12 mb-2',
                                ]
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.label',
                        [
                            'field' => [
                                'label' => 'Unidade de Fornecimento',
                                'type' => 'label',
                                'name' => 'label_unidade_fornecimento',
                                'value' => $item->unidadeMedida->nome,
                                'wrapperAttributes' => [
                                    'class' => 'col-md-3',
                                ],
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.label',
                        [
                            'field' => [
                                'label' => 'Quantidade Contratada',
                                'type' => 'label',
                                'name' => 'label_quantidade_contratada',
                                'value' => $quantidadeContratada,
                                'wrapperAttributes' => [
                                    'class' => 'quantidade_contratada col-md-3',
                                ],
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.label',
                        [
                            'field' => [
                                'label' => 'Quantidade OS/F',
                                'type' => 'label',
                                'name' => 'label_quantidade_osf',
                                'value' => $quantidadeOSF,
                                'wrapperAttributes' => [
                                    'class' => 'quantidade_osf col-md-3',
                                ],
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.label',
                        [
                            'field' => [
                                'label' => 'Quantidade Solicitada (Entregue)',
                                'type' => 'label',
                                'name' => 'label_quantidade_solicitada',
                                'value' => $quantidadeSolicitada,
                                'wrapperAttributes' => [
                                    'class' => 'quantidade_solicitada col-md-3',
                                ],
                            ]
                        ]
                    )
                </div>
                <div class="row mb-2">
                    @include(
                        'crud::fields.text',
                        [
                            'field' => [
                                'label' => 'Quantidade Informada',
                                'name' => "vinculos[$keyVinculo][itens][$keyItem][quantidade_informada]",
                                'type' => 'text',
                                'value' => $quantidadeInformada,
                                'required' => true,
                                'wrapperAttributes' => [
                                    'class' => 'col-md-3',
                                ],
                                'attributes' => array_merge(
                                    [
                                        'class' => 'quantidade_informada',
                                        'data-key' => $keyItem,
                                    ],
                                    (function () use ($disabledFields) : array  {
                                        if (!empty($disabledFields)) {
                                            return ['disabled' => 'disabled'];
                                        }
                                        return [];
                                    })()
                                )
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.text',
                        [
                            'field' => [
                                'label' => 'Valor Glosa',
                                'name' => "vinculos[$keyVinculo][itens][$keyItem][valor_glosa]",
                                'type' => 'text',
                                'required' => true,
                                'value' => $valorGlosa,
                                'wrapperAttributes' => [
                                    'class' => 'col-md-3',
                                ],
                                'attributes' => array_merge(
                                    [
                                        'class' => "valor_glosa",
                                        'data-key' => $keyItem
                                    ],
                                    (function () use ($disabledFields) : array  {
                                        if (!empty($disabledFields)) {
                                            return ['disabled' => 'disabled'];
                                        }
                                        return [];
                                    })()
                                )
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.label',
                        [
                            'field' => [
                                'label' => 'Valor Unitário',
                                'type' => 'label',
                                'name' => 'label_valor_unitario',
                                'value' => 'R$ ' . $item->valor_unitario,
                                'wrapperAttributes' => [
                                    'class' => 'valor_unitario col-md-3',
                                ],
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.label',
                        [
                            'field' => [
                                'label' => 'Valor Total',
                                'type' => 'label',
                                'name' => 'label_valor_total',
                                'value' => 'R$ ' . number_format($valorTotalItem, 2, ',', '.'),
                                'wrapperAttributes' => [
                                    'class' => 'valor_total_vinculo_item col-md-3',
                                ],
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.hidden',
                        [
                            'field' => [
                                'label' => null,
                                'type' => 'hidden',
                                'name' => 'label_valor_total',
                                'value' => $valorTotalItem,
                                'attributes' => [
                                    'disabled' => 'disabled',
                                    'class' => 'valor_total_vinculo_item_input'
                                ]
                            ]
                        ]
                    )
                </div>
                <div class="row mb-2">
                    @include(
                        'crud::fields.date_month',
                        [
                            'field' => [
                                'label' => 'Mês/Ano de Referência',
                                'name' => "vinculos[$keyVinculo][itens][$keyItem][mes_ano_competencia]",
                                'type' => 'date_month',
                                'value' => $mesAnoCompetencia,
                                'wrapperAttributes' => ['class' => 'form-group col-md-3'],
                                'attributes' => (function () use ($disabledFields) : array  {
                                    if (!empty($disabledFields)) {
                                        return ['disabled' => 'disabled'];
                                    }
                                    return [];
                                })()
                            ]
                        ]
                    )

                    @include(
                        'crud::fields.text',
                        [
                            'field' => [
                                'label' => 'Processo SEI',
                                'name' => "vinculos[$keyVinculo][itens][$keyItem][processo_sei]",
                                'type' => "text",
                                'wrapperAttributes' => [
                                    'class' => 'col-md-3',
                                ],
                                'attributes' => [
                                    'data-mask' => $crud->model->getProcessoMask(),
                                    'placeholder' => $crud->model->getProcessoMask(),
                                    $disabledFields => 'disabled'
                                ],
                                'value' => $processoSei
                            ]
                        ]
                    )
                    @include(
                        'crud::fields.date',
                        [
                            'field' => [
                                'name' => "vinculos[$keyVinculo][itens][$keyItem][data_inicio]",
                                'type' => 'date',
                                'label'=> 'Data Início',
                                'value' => $dataInicio,
                                'wrapper' => [
                                    'class' => 'col-md-3',
                                ],
                                'attributes' => [
                                    $disabledFields => 'disabled'
                                ]
                            ]
                        ]
                    )
                    @include(
                        'crud::fields.date',
                        [
                            'field' => [
                                'name' => "vinculos[$keyVinculo][itens][$keyItem][data_fim]",
                                'type' => 'date',
                                'label'=> 'Data Fim',
                                'value' => $dataFim,
                                'wrapper' => [
                                    'class' => 'col-md-3',
                                ],
                                'attributes' => [
                                    $disabledFields => 'disabled'
                                ]
                            ]
                        ]
                    )
                    @include(
                        'crud::fields.time',
                        [
                            'field' => [
                                'name' => "vinculos[$keyVinculo][itens][$keyItem][horario]",
                                'type' => 'time',
                                'label'=> 'Horas',
                                'value' => $horario,
                                'wrapper' => [
                                    'class' => 'col-md-3',
                                ],
                                'attributes' => [
                                    $disabledFields => 'disabled'
                                ]
                            ]
                        ]
                    )
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
