@if ($crud->hasAccess('create'))
	{{-- <a href="{{ url($crud->route.'/create') }}" class="btn btn-primary" data-style="zoom-in"><span class="ladda-label"><i class="la la-plus"></i> {{ trans('backpack::crud.add') }} {{ $crud->entity_name }}</span></a> --}}
    <button class="br-button primary mr-3" type="button" onclick=" window.location.href= '{{ url($crud->route.'/create') }}'">
        <i class="fas fa-plus"></i> {{ $crud->prefix_text_button_redirect_create ?? 'Adicionar ' }} {{ $crud->text_button_redirect_create ?? '' }}
    </button>
@endif
