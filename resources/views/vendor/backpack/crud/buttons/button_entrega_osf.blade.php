@if (in_array($entry->getSituacao(), ['Concluído', 'Em execução', 'Vigência Expirada']))
    @include('crud::buttons.button_entrega', ['link' => '/entrega/' . $entry->contrato_id  . '/' . $entry->id])
@endif
