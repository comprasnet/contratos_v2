@php
    $rascunho = $crud->model->find($entry->getKey())->rascunho;
@endphp

@if ($rascunho)
    <a href="{{ url("{$crud->route}/{$entry->getKey()}/edit") }} "
       class="btn btn-link"
       title="Analisar"
    >
        <i class="fas fa-edit"></i>
    </a>
@endif