@if(in_array($entry->situacao->descres, ['ae_entrega_status_8', 'ae_entrega_status_12']))
    @php
        $trd = \App\Models\AutorizacaoExecucaoEntregaTRD::find($entry->id);
        $aeEntregaSignatarioService = new \App\Services\AutorizacaoExecucao\AutorizacaoExecucaoEntregaSignatarioService($trd);
        $canAssinar = $aeEntregaSignatarioService->verifyUsuarioCanAssinar();
    @endphp
    @if ($canAssinar)
        @php
            $title = 'Assinar TRD';
            $class = 'btn btn-sm btn-link';

            if (isset($prependIcon) && $prependIcon) {
                $class .= ' btn-block text-left';
            }
        @endphp
        <a
            style="text-decoration: none"
            class="{{ $class }}"
            href="{{ url($crud->route.'/trd/'.$entry->getKey().'/assinatura/create') }}"
            title="{{ $title }}"
        >
            <i class="fa fa-signature"></i>

            @if (isset($prependIcon) && $prependIcon)
                {{ $title }}
            @endif
        </a>
    @endif
@endif
