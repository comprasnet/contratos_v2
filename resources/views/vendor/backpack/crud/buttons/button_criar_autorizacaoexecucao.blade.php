@if ($crud->hasAccess('create'))
    @php
//        $external = $button->name === 'create_external' ? true : false;

        //temporario
        $external = $button->name === 'create' ? true : false;

        $prefix_text_button_redirect_create = $external ? 'Informar' : 'Criar';
        $textTooltip = $external ? 'Registro de Ordem de Serviço / Fornecimento gerada em sistema externo' : 'Criação de uma nova Ordem de Serviço / Fornecimento';
    @endphp

    <button class="br-button primary mr-3" type="button" onclick=" window.location.href= '{{ url($crud->route.'/create?external=' . $external) }}'">
        <i class="fas fa-plus"></i> {{ $prefix_text_button_redirect_create }} OS/F
    </button>


    <div class="br-tooltip text-justify" role="tooltip" info="info" place="bottom">
        <span>{{ $textTooltip }}</span>
    </div>
@endif
