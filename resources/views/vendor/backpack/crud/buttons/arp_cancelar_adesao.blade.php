@php
    $situacoesHabilitar = ['Aceita', 'Enviada para aceitação', 'Aceito Parcial','Aguardando Aceitação do Fornecedor',
    'Aceita Parcialmente pelo Fornecedor'];
@endphp
@if (in_array($entry->getSituacao(), $situacoesHabilitar))
<a href="javascript:void(0)" onclick="cancelarAdesao({{$entry->id}}, `{{$entry->getNumeroSolicitacao()}}`, 'adesao/cancelaradesao')"
   class="btn btn-xs btn-link" ><i class="fas fa-ban"></i></a>
   @include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => 'Cancelar adesão'])
@endif