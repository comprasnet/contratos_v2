@if(in_array($entry->situacao->descres, ['trp_status_3', 'trp_status_4']))
    @php
    $title = 'Download do TRP';
    $class = 'btn btn-sm btn-link';

    if (isset($prependIcon) && $prependIcon) {
        $class .= ' btn-block text-left';
    }
    @endphp
    <a
        style="text-decoration: none"
        class="{{$class}}"
        type="button"
        href="{{ url('storage/' .  $entry->getArquivo()->url) }}"
        target="_blank"
        title="Download do TRP"
    >
        <span
            class="br-button primary"
            style="font-size: 11px; height: 22px; padding: 0 9px; color: white"
        >
            TRP
        </span>

        @if(isset($prependIcon) && $prependIcon)
            {{ $title }}
        @endif
    </a>
@endif
