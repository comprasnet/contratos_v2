@if ($entry->unidade_origem_id == session('user_ug_id') && $entry->tipo->descricao == 'Ata de Registro de Preços')
    <a href="/arp/item/{{$entry->id}}/arquivos" class="btn btn-x btn-link" ><i
                class="fa fa-file"></i></a>

@include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => 'Arquivos'])
@endif