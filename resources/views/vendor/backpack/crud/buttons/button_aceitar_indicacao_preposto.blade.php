@if(
    $entry->status_indicacao_id != null
    &&
    $entry->statusIndicacao->descres == 'sit_indica_prepost_1'
)
    <a
            class="btn btn-sm btn-link"
            href="{{ route('aceita.indicacao.preposto', ['contrato_id' => request()->contrato_id, 'contratopreposto_id' => $entry->id, 'statusaceitacao' => 'sit_indica_prepost_2']) }}"
            title="Aceitar indicação"
            data-contratoprepostoid = "{{ $entry->id }}"
            data-situacaoindicacao = "sit_indica_prepost_3"
    >
        <i class="fas fa-check-circle"></i>
    </a>
@endif
