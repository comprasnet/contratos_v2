<a
        class="btn btn-sm btn-link download-pdf"
        href="//{{$entry->linkInstrumentoDeCobrancaTransparenciaV1()}}"
        target="_blank"
>
    <i class="fas fa-solid fa-link"></i>
    @include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => 'Ordem de pagamento da faturas no órgão'])
</a>


