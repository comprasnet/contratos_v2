@if(
    !in_array($entry->situacao->descres,
    [
        'ae_entrega_status_1',
        'ae_entrega_status_2',
        'ae_entrega_status_3',
        'ae_entrega_status_5',
        'ae_entrega_status_6',
        'ae_entrega_status_9',
    ])
)
    <a
        class="btn btn-sm btn-link download-pdf"
        href="{{ url('storage/' .  $entry->getArquivoTRP()->url) }}"
        title="Baixar PDF do TRP"
        target="_blank"
    >
        <span class="badge badge-pill badge-primary">TRP</span>
    </a>
@endif
