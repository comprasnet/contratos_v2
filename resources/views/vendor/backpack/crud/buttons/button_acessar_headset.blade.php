<button class="br-button secondary large mr-3 mt-3 circle " type="button" id="btn_acessar_suporte" target="_blank">
    <i class="fas fa-headphones"></i>
</button>

@push('script')
    <script type="text/javascript">
        $(document).ready(function($) {
            $('#btn_acessar_suporte').click(function () {
                window.open("https://portaldeservicos.gestao.gov.br/", '_blank');
            })
        })
    </script>
@endpush