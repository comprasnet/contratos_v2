@if(in_array($entry->situacao->descres, [
    'ae_entrega_status_12',
    'ae_entrega_status_7'
    ])
    )
    <a
        class="btn btn-sm btn-link download-pdf"
        href="{{ url('storage/' .  $entry->getArquivoTRD()->url) }}"
        title="Baixar PDF do TRD"
        target="_blank"
    >
        <span class="badge badge-pill badge-primary">TRD</span>
    </a>
@endif
