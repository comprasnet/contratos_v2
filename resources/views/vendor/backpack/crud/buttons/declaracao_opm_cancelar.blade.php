@php
    $situacoesHabilitar = ['Ativa'];
@endphp

@if (in_array($entry->getSituacao(), $situacoesHabilitar))
    <a href="javascript:void(0)" onclick="cancelarOpm({{$entry->id}}, `{{$entry->getNumeroSolicitacao()}}`, '{{ url("declaracaoopm/" . request()->contrato_id . "/cancelaropm") }}')"
       class="btn btn-xs btn-link" title="Cancelar declaração"><i class="fas fa-ban"></i></a>
@endif
