@php
$situacaoAtual = $entry->read();
$title = 'Marcar como lido';
$icone = 'far fa-bell';

if($situacaoAtual) {
    $title = 'Marcar como não lido';
    $icone = 'far fa-bell-slash';
}
@endphp

<a href="javascript:void(0)" class="btn btn-x btn-link" title="{{$title}}" onclick="alterarSituacaoNotificacao('{{$entry->id}}', {{$situacaoAtual}})">
    <i class="{{$icone}}"></i>
</a>