@if(in_array($entry->situacao->descres, ['trd_status_2', 'trd_status_4', 'trd_status_5']))
    @php
    $title = 'Download do TRD';
    $class = 'btn btn-sm btn-link';

    if (isset($prependIcon) && $prependIcon) {
        $class .= ' btn-block text-left';
    }
    @endphp
    <a
        style="text-decoration: none"
        class="{{$class}}"
        type="button"
        href="{{ url('storage/' .  $entry->getArquivo()->url) }}"
        target="_blank"
        title="Download do TRD"
    >
        <span
            class="br-button primary"
            style="font-size: 11px; height: 22px; padding: 0 9px; color: white"
        >
            TRD
        </span>

        @if(isset($prependIcon) && $prependIcon)
            {{ $title }}
        @endif
    </a>
@endif
