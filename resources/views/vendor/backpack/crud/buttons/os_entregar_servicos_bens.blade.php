@php
$situacoesHabilitar = ['Concluído', 'Em execução', 'Vigência Expirada'];
$title = 'Entregas';
$class = 'btn btn-sm btn-link';
if (isset($prependIcon) && $prependIcon) {
    $class .= ' btn-block text-left';
}
@endphp
@if (in_array($entry->getSituacao(), $situacoesHabilitar))
    <a
        style="text-decoration: none"
        href="{{$crud->urlAutorizacaoExecucaoEntrega ?? $crud->route}}/{{$entry->id}}/entrega"
        class="{{$class}}"
        title="{{ $title }}"
    >
        <i class="fas fa-clipboard-list"></i>
        @if(isset($prependIcon) && $prependIcon)
            {{ $title }}
        @endif
    </a>
@endif
