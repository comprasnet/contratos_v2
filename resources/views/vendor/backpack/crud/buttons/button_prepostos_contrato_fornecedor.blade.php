@php
$url = "/fornecedor/contrato/informacoesprepostos";
 @endphp
<a href="javascript:void(0)"
   class="btn btn-sm btn-link"
   onclick="exibirPrepostos({{$entry->id}}, '{{$url}}')"
   title="Exibir os Prepostos do Contrato"><i class="fas fa-user-circle" aria-hidden="true"></i></a>
