@if(
    ($entry->situacao->descres == 'ae_status_2' || $entry->situacao->descres == 'ae_status_8') &&
    $entry->originado_sistema_externo
)
    @php
        $title = 'Retificar';
        $class = 'btn btn-sm btn-link';

        if (isset($prependIcon) && $prependIcon) {
            $class .= ' btn-block text-left';
        }
    @endphp

    <a
       style="text-decoration: none"
       class="{{$class}}"
       href="{{ url($crud->route.'/'.$entry->getKey().'/retificar') }}"
       title="{{$title}}"
    >
        <i class="fas fa-redo"></i>
        @if(isset($prependIcon) && $prependIcon)
            {{ $title }}
        @endif
    </a>
@endif
