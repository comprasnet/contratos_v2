@php
$title = 'Gerar PDF';
$class = 'btn btn-sm btn-link';

if (isset($prependIcon) && $prependIcon) {
    $class .= ' btn-block text-left';
}
@endphp

@if($entry->situacao->descres == 'ae_status_1' && $entry->originado_sistema_externo == 1)
    <a
        style="text-decoration: none"
        class="{{$class}} download-pdf"
        href="#"
        data-url="{{ url($crud->route.'/'.$entry->getKey().'/gerar-pdf') }}"
        title="{{$title}}"
    >
        <i class="fas fa-file-pdf"></i>
        @if(isset($prependIcon) && $prependIcon)
            {{ $title }}
        @endif
    </a>

    {{-- Garante que a informação é adicionada no html apenas uma vez --}}
    @loadOnce
        @include(
            'crud::fields.modal_select_signatarios',
            [
                'field' => [
                    'prepostos' => $crud->prepostosResponsaveis['prepostos'],
                    'responsaveis' => $crud->prepostosResponsaveis['responsaveis'],
                ]
            ]
        )
    @endLoadOnce
@endif
