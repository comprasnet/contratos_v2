@php
    $linkPncp = $entry->getLinkPNCP() ?? null;
@endphp

<a
        href="{{$linkPncp ?? '#'}}"
        class="btn btn-x btn-link {{!$linkPncp ? 'disabled' : ''}}"
        role='tooltip' 
        info='info'
        target="_blank"
>
    <i class="fa fa-globe-americas"></i>
    @include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => 'PNCP'])
</a>