<button id="transparencia" class="br-sign-in secondary mb-5 w-100 button-custom-login" type="button">
    Transparência
</button>

@push('script')
    <script type="text/javascript">
        $(document).ready(function($) {
            $('#transparencia').click(function () {
                window.location.href = "/transparencia";
            })
        })
    </script>
@endpush