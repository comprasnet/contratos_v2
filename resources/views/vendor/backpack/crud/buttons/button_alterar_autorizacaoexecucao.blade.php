@php
    $title = 'Alterar';
    $class = 'btn btn-sm btn-link';

    if (isset($prependIcon) && $prependIcon) {
        $class .= ' btn-block text-left';
    }
@endphp

@if($entry->situacao->descres == 'ae_status_2' || $entry->situacao->descres == 'ae_status_8')
    <a
       style="text-decoration: none"
       class="{{ $class }}"
       href="{{ url($crud->route.'/'.$entry->getKey().'/alterar') }}"
       title="{{$title}}"
    >
        <i class="fas fa-file-signature"></i>
        @if(isset($prependIcon) && $prependIcon)
            {{ $title }}
        @endif
    </a>
@endif
