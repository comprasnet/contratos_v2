@php
$autorizacaoExecucaoModel = new \App\Models\AutorizacaoExecucao();
$autorizacaoExecucao = $autorizacaoExecucaoModel->findOrFail($entry->getKey());
$arquivoAssinado = $autorizacaoExecucao->getArquivo();
@endphp
@if($arquivoAssinado)
    <a
            class="btn btn-sm btn-link download-pdf"
            href="{{ url('storage/' .  $autorizacaoExecucao->getArquivo()->url) }}"
            target="_blank"
            title="Gerar PDF"
    >
        <i class="fas fa-file-pdf"></i>
    </a>
@endif

