@php
    $urlPrefix = session()->get('fornecedor_id') ? '/fornecedor' : '' ;
@endphp

@if ($entry->aplicavel_decreto)
    <a href="{{ url($urlPrefix . '/declaracaoopm/' . $entry->getKey()) }} " class="btn btn-link" title="Declaração OPM">
        <i class="fas fa-venus" style="font-size: 20px !important;"></i>
    </a>
@endif