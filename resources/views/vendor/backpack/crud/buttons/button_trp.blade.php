@php
    $title = 'Elaborar Recebimento Provisório';
    $class = 'btn btn-sm btn-link';
    if (isset($prependIcon) && $prependIcon) {
        $class .= ' btn-block text-left';
    }
@endphp

<a
    style="text-decoration: none"
    class="{{ $class }}"
    type="button"
    href="{{ $link }}"
    title="{{ $title }}"
>
    <span
        class="br-button primary"
        style="font-size: 11px; height: 22px; padding: 0 9px; color: white"
    >
        TRP
    </span>

    @if (isset($prependIcon) && $prependIcon)
        {{ $title }}
    @endif
</a>
