@php
    $situacoesHabilitar = ['Em Análise'];
@endphp
@if (in_array($entry->retornaTextoSituacao(), $situacoesHabilitar))
<a href="javascript:void(0)" onclick="cancelarEntrega({{$entry->id}}, `{{$entry->getFormattedSequencial()}}`, 'cancelar')"
   class="btn btn-xs btn-link" title="Cancelar entrega"><i class="fas fa-ban"></i></a>
@endif