@if(session()->has('fornecedor_id'))
    @if(!$crud->menuAcessoRapido)
        @if(in_array($entry->situacao->descres, ['ae_status_2', 'ae_status_5', 'ae_status_8']) && !$entry->originado_sistema_externo)
            @php
                $aeSignatarioService = new \App\Services\AutorizacaoExecucao\AutorizacaoExecucaoSignatarioService($entry);
                $canAssinar = $aeSignatarioService->verifyUsuarioCanAssinar();
            @endphp
            @if ($canAssinar)
                <a
                        class="btn btn-sm btn-link"
                        href="{{ url($crud->route.'/'.$entry->getKey().'/assinatura/create') }}"
                        title="Assinar"
                >
                    <i class="fa fa-signature"></i>
                </a>
            @endif
        @endif
    @else
        @if(in_array($entry->situacao->descres, ['ae_status_2', 'ae_status_5', 'ae_status_8']) && !$entry->originado_sistema_externo)
            @php
                $aeSignatarioService = new \App\Services\AutorizacaoExecucao\AutorizacaoExecucaoSignatarioService($entry);
                $canAssinar = $aeSignatarioService->verifyUsuarioCanAssinar();
            @endphp
            @if ($canAssinar)
                <a
                        class="btn btn-sm btn-link"
                        href="{{ url($crud->route . '/' . $entry->contrato_id . '/' .$entry->getKey().'/assinatura/create') }}"
                        title="Assinar"
                >
                    <i class="fa fa-signature"></i>
                </a>
            @endif
        @endif
    @endif
@else
    @if(in_array($entry->situacao->descres, ['ae_status_2', 'ae_status_5', 'ae_status_8']) && !$entry->originado_sistema_externo)
        @php
            if ($entry instanceof \App\Models\AutorizacaoexecucaoHistorico) {
                $aeSignatarioService = new \App\Services\AutorizacaoExecucao\AutorizacaoExecucaoHistoricoAlterarSignatarioService($entry);
            } else {
                $aeSignatarioService = new \App\Services\AutorizacaoExecucao\AutorizacaoExecucaoSignatarioService($entry);
            }
            $canAssinar = $aeSignatarioService->verifyUsuarioCanAssinar();
            $title = 'Assinar ou Recusar Assinatura';
            $class = 'btn btn-sm btn-link';

            if (isset($prependIcon) && $prependIcon) {
                $class .= ' btn-block text-left';
            }
        @endphp
        @if ($canAssinar)
            <a
                style="text-decoration: none"
                class="{{$class}}"
                href="{{ url($crud->route.'/'.$entry->getKey().'/assinatura/create') }}"
                title="{{$title}}"
            >
                <i class="fa fa-signature"></i>
                @if(isset($prependIcon) && $prependIcon)
                    {{ $title }}
                @endif
            </a>
        @endif
    @endif
@endif
