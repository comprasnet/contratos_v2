@php
$textoTooltip = 'Visualizar PDF';
$exibirTooltipPadraoGov = $tooltipPadraoGov ?? false;
$idAta = $id ?? $entry->id;
@endphp
<a href="{{ route('visualizar.pdf', ['id' => $idAta, 'tipo' => 'visualizar']) }}"
   target="_blank" data-arp-id="{{ $idAta }}"
   id="visualizar-pdf-{{ $idAta }}"

   @if(!$exibirTooltipPadraoGov)
       title = '{{$textoTooltip}}'
   @endif
>
    <i class="fas fa-eye" style="font-size: 22px !important;"></i>
</a>

@if($exibirTooltipPadraoGov)
<div class="br-tooltip text-justify" role="tooltip" info="info" place="top"
     id="{{ $idAta }}">
    <span class="subtext">{{$textoTooltip}}</span>
</div>
@endif