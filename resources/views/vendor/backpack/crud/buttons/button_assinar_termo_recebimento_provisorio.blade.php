@if(in_array($entry->situacao->descres, ['trp_status_2', 'trp_status_3']))
    @php
        $modelDTO = new \App\Services\ModelSignatario\ModelDTO($entry);
        $trpAssinador = new \App\Services\ModelSignatario\TermoRecebimentoProvisorioAssinador($modelDTO);
        $canAssinar = $trpAssinador->verifyUsuarioCanAssinar();
    @endphp
    @if ($canAssinar)
        @php
            $title = 'Assinar TRP';
            $class = 'btn btn-sm btn-link';

            if (isset($prependIcon) && $prependIcon) {
                $class .= ' btn-block text-left';
            }
        @endphp
        <a
            style="text-decoration: none"
            class="{{ $class }}"
            href="{{ url($crud->route.'/'.$entry->getKey().'/assinatura/create') }}"
            title="{{ $title }}"
        >
            <i class="fa fa-signature"></i>
            @if(isset($prependIcon) && $prependIcon)
                {{ $title }}
            @endif
        </a>
    @endif
@endif
