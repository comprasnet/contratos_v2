@php
$title = 'Duplicar';

if ($button && !empty($button->title)) {
    $title = $button->title;
}
@endphp
<a
   class="btn btn-sm btn-link"
   href="{{ url($crud->route.'/'.$entry->getKey().'/duplicar') }}"
   title="{{ $title }}"
>
    <i class="fas fa-copy"></i>
</a>
