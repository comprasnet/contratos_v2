@if ($entry->getLinkPNCP() == null)

    <a class="btn btn-x btn-link disabled">
        <i class="fa fa-globe-americas"></i>
    </a>

@else

    <a href="{{$entry->getLinkPNCP()}}" class="btn btn-x btn-link" title="PNCP" target="_blank">
        <i class="fa fa-globe-americas"></i>
    </a>


@endif
