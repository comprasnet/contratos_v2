@php
    $title = 'Prepostos';
    $class = 'btn btn-sm btn-link';
    if (isset($prependIcon) && $prependIcon) {
        $class .= ' btn-block text-left';
    }
    $contratoResponsavelRepository = new \App\Repositories\Contrato\ContratoResponsavelRepository($entry->id);
@endphp

@if ($contratoResponsavelRepository->isUsuarioFiscalContrato() || $contratoResponsavelRepository->isUsuarioGestorContrato())
    <a
        style="text-decoration: none"
        href="{{ url('/contrato/' . $entry->getKey()) . '/prepostos' }} "
        class="{{ $class }}"
        title="{{ $title }}"
    >
        <i class="fas fa-user-circle" style="font-size: 20px !important;"></i>
        @if(isset($prependIcon) && $prependIcon)
            {{ $title }}
        @endif
    </a>
@endif
