@if(in_array($entry->situacao->descres, [
        'anuencia_status_1'
        ])
    )
    <a href="{{ url('fornecedor/arp/adesao/analisar/'.$entry->getKey().'/edit') }}" class="btn btn-link" title="Analisar">
        <i class="fas fa-business-time"></i>
    </a>
@endif