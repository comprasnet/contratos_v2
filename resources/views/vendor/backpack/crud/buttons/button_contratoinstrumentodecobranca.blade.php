@php
    $urlPrefix = session()->get('fornecedor_id') ? '/fornecedor' : '' ;
@endphp

{{--@can('autorizacaoexecucao_visualizar')--}}
    <a href="{{ url($urlPrefix . '/contratoinstrumentodecobranca/' . $entry->getKey()) }} "
       class="btn btn-link"
       title="Instrumento de Cobrança"
    >
        <i class="fas fa-wallet"></i>
    </a>
{{--@endcan--}}
