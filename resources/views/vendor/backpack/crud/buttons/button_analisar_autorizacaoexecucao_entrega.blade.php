@if($entry->situacao->descres === 'ae_entrega_status_1')
    @php
        $title = 'Analisar Entrega';
        $class = 'btn btn-sm btn-link';

        if (isset($prependIcon) && $prependIcon) {
            $class .= ' btn-block text-left';
        }
    @endphp
    <a
       style="text-decoration: none"
       class="{{ $class }}"
       href="{{ url($crud->route.'/'.$entry->getKey().'/analisar/create') }}"
       title="{{ $title }}"
    >
        <i class="fas fa-check-square"></i>

        @if (isset($prependIcon) && $prependIcon)
            {{ $title }}
        @endif
    </a>
@endif
