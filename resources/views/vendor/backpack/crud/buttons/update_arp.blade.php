@if ($crud->hasAccess('update') && $entry->rascunho)
	@if (!$crud->model->translationEnabled())
	@php
		# Parâmetro criado para que possamos customizar a url caso necessário
		$url = $crud->url_button_custom ?? url($crud->route.'/'.$entry->getKey().'/edit');
	@endphp
	<!-- Single edit button -->
	<a href="{{ $url }}" class="btn btn-link" ><i class="fas fa-edit"></i> </a>
	@include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => trans('backpack::crud.edit')])

	@else

	<!-- Edit button group -->
	<div class="btn-group">
	  <a href="{{ $url }}" class="btn btn-link pr-0" title="{{ trans('backpack::crud.edit') }}"><i class="fas fa-edit"></i></a>
	  <a class="btn btn-link dropdown-toggle text-primary pl-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    <span class="caret"></span>
	  </a>
	  <ul class="dropdown-menu dropdown-menu-right">
  	    <li class="dropdown-header">{{ trans('backpack::crud.edit_translations') }}:</li>
	  	@foreach ($crud->model->getAvailableLocales() as $key => $locale)
		  	<a class="dropdown-item" href="{{ $url }}?locale={{ $key }}">{{ $locale }}</a>
	  	@endforeach
	  </ul>
	</div>

	@endif
@endif
