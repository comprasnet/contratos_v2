@php
$title = 'Entregas NOVO';
$class = 'btn btn-sm btn-link';
if (isset($prependIcon) && $prependIcon) {
    $class .= ' btn-block text-left';
}
@endphp
<a
    style="text-decoration: none"
    href="{{ $link }}"
    class="{{ $class }}"
    title="{{ $title }}"
>
    <i class="fas fa-clipboard-list"></i>
    @if(isset($prependIcon) && $prependIcon)
        {{ $title }}
    @endif
</a>
