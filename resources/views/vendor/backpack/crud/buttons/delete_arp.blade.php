@if ($crud->hasAccess('delete') && $entry->rascunho)
	<a href="javascript:void(0)" onclick="deleteEntry(this)" data-route="{{ url($crud->route.'/'.$entry->getKey()) }}" class="btn btn-link" data-button-type="delete" title="{{ trans('backpack::crud.delete') }}"><i class="fas fa-trash"></i> </a>
@endif

{{-- Button Javascript --}}
{{-- - used right away in AJAX operations (ex: List) --}}
{{-- - pushed to the end of the page, after jQuery is loaded, for non-AJAX operations (ex: Show) --}}
@push('after_scripts') @if (request()->ajax()) @endpush @endif

<script>

	if (typeof deleteEntry != 'function') {
	  $("[data-button-type=delete]").unbind('click');

	  function deleteEntry(button) {
		// ask for confirmation before deleting an item
		// e.preventDefault();
		var button = $(button);
		var route = button.attr('data-route');
		var row = $("#crudTable a[data-route='"+route+"']").closest('tr');


	   var customDiv = document.createElement('div');
	   customDiv.classList.add('br-modal-body', 'text-left');
	   customDiv.innerHTML = "<p> {!! trans('backpack::crud.delete_confirm') !!}  </p>"


		swal({
		  title: "{!! trans('backpack::base.warning') !!}",
			content: customDiv,
			//icon: "warning",
		  buttons: {
		  	cancel: {
			  text: "{!! trans('backpack::crud.cancel') !!}",
			  value: null,
			  visible: true,
			  className: "br-button secondary mr-3",
			  closeModal: true,
			},
		  	delete: {
			  text: "{!! trans('backpack::crud.delete') !!}",
			  value: true,
			  visible: true,
			  className: "br-button danger mr-3",
			}
		  },
		}).then((value) => {
			if (value) {
				$.ajax({
			      url: route,
			      type: 'DELETE',
			      success: function(result) {
			          if (result == 1) {
			          	  // Show a success notification bubble
							exibirAlertaNoty('success-custom', "{!! trans('backpack::crud.delete_confirmation_message') !!}")
			            //   new Noty({
		                //     type: "success",
		                //     text: "{!! '<strong>'.trans('backpack::crud.delete_confirmation_title').'</strong><br>'.trans('backpack::crud.delete_confirmation_message') !!}"
		                //   }).show();

			              // Hide the modal, if any
			             // $('.modal').modal('hide');

			              // Remove the details row, if it is open
			              if (row.hasClass("shown")) {
			                  row.next().remove();
			              }

			              // Remove the row from the datatable
			              row.remove();
			          } else if (result == 2) {
						  swal({
							  title: "{!! trans('backpack::crud.delete_confirmation_not_title') !!}",
							  text: "Arquivo do tipo Ata de registro de preços não pode ser excluído. Você pode substituí-lo adicionando outro arquivo do mesmo tipo.",
							  icon: "error",
							  timer: 8000,
							  buttons: false,
						  });
					  } else {
			              // if the result is an array, it means
			              // we have notification bubbles to show
			          	  if (result instanceof Object) {
			          	  	// trigger one or more bubble notifications
			          	  	Object.entries(result).forEach(function(entry, index) {
			          	  	  var type = entry[0];
			          	  	  entry[1].forEach(function(message, i) {
								exibirAlertaNoty(`${type}-custom`, "{!! trans('backpack::crud.delete_confirmation_message') !!}")
					          	//   new Noty({
				                //     type: type,
				                //     text: message
				                //   }).show();
			          	  	  });
			          	  	});
			          	  } else {// Show an error alert
				              swal({
				              	title: "{!! trans('backpack::crud.delete_confirmation_not_title') !!}",
	                            text: "{!! trans('backpack::crud.delete_confirmation_not_message') !!}",
				              	icon: "error",
				              	timer: 4000,
				              	buttons: false,
				              });
			          	  }
			          }
			      },
			      error: function(result) {
			          // Show an alert with the result
			          swal({
		              	title: "{!! trans('backpack::crud.delete_confirmation_not_title') !!}",
                        text: "{!! trans('backpack::crud.delete_confirmation_not_message') !!}",
		              	icon: "error",
		              	timer: 4000,
		              	buttons: false,
		              });
			      }
			  });
			}
		});

      }
	}

	// make it so that the function above is run after each DataTable draw event
	// crud.addFunctionToDataTablesDrawEventQueue('deleteEntry');
</script>
<style>
	/*Estilização modal sweet alert padrao gov*/
	button.swal-button.swal-button--delete.br-button.danger.mr-3:hover{
		background-color: #ff2e11;
	}
	button.swal-button.swal-button--cancel.br-button.secondary.mr-3:hover {
		background-color: #e8e8e8;
	}
	.swal-footer {
		text-align: center;
	}
	.swal-modal {
		width: 400px;
	}
	.swal-title {
		color: rgb(0 0 0 / 71%);
		padding: 0px 37px;
		font-size: 21px;
		text-align: left;
	}
</style>
@if (!request()->ajax()) @endpush @endif
