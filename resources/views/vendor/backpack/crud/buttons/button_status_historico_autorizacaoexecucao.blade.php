@if($entry->situacao->descres != 'ae_status_1')
    <a
       class="btn btn-sm btn-link"
       href="{{ url($crud->route.'/'.$entry->getKey().'/status-historico') }}"
    >
        <i class="fas fa-history"></i>
        @include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => 'Visualizar histórico da OS/F'])
    </a>
@endif
