@component('crud::buttons.more.base')
    <li>@include('crud::buttons.button_prepostos', ['prependIcon' => true])</li>
    @if (Config::get("app.app_amb") != 'Ambiente Produção')
        <li>@include('crud::buttons.button_entrega_contrato', ['prependIcon' => true])</li>
        <li>@include('crud::buttons.button_trp', ['prependIcon' => true, 'link' => '/trp/' . $entry->getKey()])</li>
        <li>@include('crud::buttons.button_trd', ['prependIcon' => true, 'link' => '/trd/' . $entry->getKey()])</li>
    @endif
@endcomponent
