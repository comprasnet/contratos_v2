@if($entry->situacao->descres != 'ae_entrega_status_9')
    @component('crud::buttons.more.base')
        <li>@include('crud::buttons.button_pdf_trp', ['prependIcon' => true])</li>
        <li>@include('crud::buttons.button_assinar_trp', ['prependIcon' => true])</li>
        <li>@include('crud::buttons.button_pdf_trd', ['prependIcon' => true])</li>
        <li>@include('crud::buttons.button_assinar_trd', ['prependIcon' => true])</li>
        @if ($crud->isUsuarioResponsavelContrato)
            <li>@include('crud::buttons.button_analisar_autorizacaoexecucao_entrega', ['prependIcon' => true])</li>
            <li>@include('crud::buttons.button_gerar_trp_trd_autorizacaoexecucao_entrega', ['prependIcon' => true])</li>
        @endif
    @endcomponent
@endif
