@component('crud::buttons.more.base')
    <li>@include('crud::buttons.button_pdf_autorizacaoexecucao', ['prependIcon' => true])</li>
    @can('autorizacaoexecucao_inserir')
        <li>@include('crud::buttons.button_assinar_autorizacaoexecucao', ['prependIcon' => true])</li>
        <li>@include('crud::buttons.button_alterar_autorizacaoexecucao', ['prependIcon' => true])</li>
        <li>@include('crud::buttons.os_entregar_servicos_bens', ['prependIcon' => true])</li>
        @if(Config::get("app.app_amb") != 'Ambiente Produção')
            <li>@include('crud::buttons.button_entrega_osf', ['prependIcon' => true])</li>
        @endif
        <li>@include('crud::buttons.button_retificar_autorizacaoexecucao', ['prependIcon' => true])</li>
    @endcan
@endcomponent
