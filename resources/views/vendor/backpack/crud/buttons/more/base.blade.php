<div class="btn-group">
    <button type="button"

        class="btn btn-link"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
        title="Mais"
    >
        <i class="fa fa-chevron-down" style="font-size: 20px !important;"></i>
    </button>

    <ul class="dropdown-menu dropdown-menu-right">
        {{ $slot }}
    </ul>
</div>

<style>
    button[aria-expanded="false"] .fa-chevron-down{
        transform: rotate(0deg);
        transition: transform 0.2s linear;
    }

    button[aria-expanded="true"] .fa-chevron-down{
        transform: rotate(180deg);
        transition: transform 0.2s linear;
    }
</style>
