@if($entry->situacao->descres != 'entrega_status_1')
    @component('crud::buttons.more.base')
        @if ($crud->isUsuarioResponsavelContrato)
            <li>@include('crud::buttons.button_analisar_autorizacaoexecucao_entrega', ['prependIcon' => true])</li>
            <li>@include('crud::buttons.button_gerar_trp_trd_autorizacaoexecucao_entrega', ['prependIcon' => true])</li>
        @endif
    @endcomponent
@endif
