@php
    $icon = (!$entry->restrito) ? 'fa-lock' : 'fa-lock-open';
    $button_text = ($entry->restrito) ? 'Alterar o status do arquivo para público.' : 'Alterar o status do arquivo para restrito.';
    $url = '/arp/'.$entry->arp_id.'/arp-arquivo/'.$entry->id.'/alterar-privacidade';
@endphp

@if ($entry->tipo->nome != 'Ata de Registro de Preços')
    <a
            href="{{$url}}"
            class="btn btn-xs btn-link"
            title="{{$button_text}}"
            onclick="exibirAlertaEnvioCustomizado('Salvando Dados e Atualizando PNCP')"
    >
        <i class="fa {{$icon}}"></i>
    </a>
@endif