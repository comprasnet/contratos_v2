@php
    use Carbon\Carbon;
    $situacoesHabilitar = ['Aceita', 'Enviada para aceitação'];
    $dataAtual = Carbon::now();
    $diferencaEmDias = $dataAtual->diffInDays($entry->data_aprovacao_analise);
@endphp
@if (in_array($entry->getSituacao(), $situacoesHabilitar) && $diferencaEmDias > 90)
<a href="#" onclick="cancelarAdesao({{$entry->id}}, `{{$entry->getNumeroSolicitacao()}}`, 'cancelaradesao')"
   class="btn btn-xs btn-link"><i class="fas fa-ban"></i></a>
   @include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => 'Cancelar adesão'])
@endif