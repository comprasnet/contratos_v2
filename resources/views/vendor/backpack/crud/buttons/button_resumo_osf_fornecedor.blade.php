@php
$url = "{$crud->route}/informacoesquadroresumo";
 @endphp
<a href="javascript:void(0)"
   onclick="exibirInformacoesOSF({{$entry->id}}, '{{$url}}', '{{$entry->getContrato()}}', '{{$entry->numero}}')"
   class="btn btn-xs btn-link" title="Exibir informações sobre a OS/F" style="text-decoration: none"><i class="fas fa-info"></i></a>
