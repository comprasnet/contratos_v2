@php
    $urlPrefix = session()->get('fornecedor_id') ? '/fornecedor' : '' ;
@endphp

@can('autorizacaoexecucao_visualizar')
    <a href="{{ url($urlPrefix . '/autorizacaoexecucao/' . $entry->getKey()) }} " class="btn btn-link" ><i class="fas fa-file-invoice"></i></a>
    @include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => 'Ordem de Serviço / Fornecimento'])
@endcan
