@php
    $acao = null;
    if ($entry->situacao->descres === 'ae_entrega_status_6' || $entry->situacao->descres === 'ae_entrega_status_2') {
        $acao = 'trp';
        $title = 'Elaborar Recebimento Provisório';
    } elseif ($entry->situacao->descres === 'ae_entrega_status_10' || $entry->situacao->descres === 'ae_entrega_status_11') {
        $acao = 'trd';
        $title = 'Elaborar Recebimento Definitivo';
    }
    $class = 'btn btn-sm btn-link';
    if (isset($prependIcon) && $prependIcon) {
        $class .= ' btn-block text-left';
    }
@endphp

@if($acao)
    <a
        style="text-decoration: none"
        class="{{ $class }}"
        type="button"
        href="{{url($crud->route.'/' . $acao . '/'.$entry->getKey().'/edit')}}"
        title="{{ $title }}"
    >
        <span
            class="br-button primary"
            style="font-size: 11px; height: 22px; padding: 0 9px; color: white"
        >
            {{ strtoupper($acao) }}
        </span>

        @if (isset($prependIcon) && $prependIcon)
            {{ $title }}
        @endif
    </a>
@endif
