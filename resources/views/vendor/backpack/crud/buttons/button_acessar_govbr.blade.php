<button class="br-sign-in primary mb-5 button-custom-login" type="button" id="acessogov" >
    Entrar com&nbsp;<span class="text-black">gov.br</span>
</button>

@push('script')
    <script type="text/javascript">
        $(document).ready(function($) {
            $('#acessogov').click(function () {
                window.location.href = "{{ route('acessogov.autorizacao') }}";
            })
        })
    </script>
@endpush