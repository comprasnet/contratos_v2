@if(in_array($entry->situacao->descres, ['trd_status_2', 'trd_status_4']))
    @php
        $modelDTO = new \App\Services\ModelSignatario\ModelDTO($entry);
        $trdAssinador = new \App\Services\ModelSignatario\TermoRecebimentoDefinitivoAssinador($modelDTO);
        $canAssinar = $trdAssinador->verifyUsuarioCanAssinar();
    @endphp
    @if ($canAssinar)
        @php
            $title = 'Assinar TRD';
            $class = 'btn btn-sm btn-link';

            if (isset($prependIcon) && $prependIcon) {
                $class .= ' btn-block text-left';
            }
        @endphp
        <a
            style="text-decoration: none"
            class="{{ $class }}"
            href="{{ url($crud->route.'/'.$entry->getKey().'/assinatura/create') }}"
            title="{{ $title }}"
        >
            <i class="fa fa-signature"></i>
            @if(isset($prependIcon) && $prependIcon)
                {{ $title }}
            @endif
        </a>
    @endif
@endif
