@if(in_array($entry->situacao->descres, [
        'anuencia_status_2',
        'anuencia_status_3'
        ]) ||  $entry->getSituacao() == 'Enviada para aceitação'
    )
    <a href="{{ url('arp/adesao/analisar/'.$entry->getKey().'/edit') }} " class="btn btn-link" title="Analisar">
        <i class="fas fa-edit"></i>
    </a>
@endif