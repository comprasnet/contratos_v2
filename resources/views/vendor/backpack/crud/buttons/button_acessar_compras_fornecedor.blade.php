<button id="acesso-fornecedor" class="br-sign-in secondary mb-5 w-100 button-custom-login" type="button">
    Acesso Fornecedor
</button>

@push('script')
    <script type="text/javascript">
        $(document).ready(function($) {
            $('#acesso-fornecedor').click(function () {
                window.location.href = "https://www.comprasnet.gov.br/seguro/loginPortal.asp";
            })
        })
    </script>
@endpush