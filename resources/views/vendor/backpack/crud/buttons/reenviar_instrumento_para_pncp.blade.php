@php
    $href = route('reenviar-pncp', [$entry->id]);
    $permitirReenvio = $entry->allowSendToPNCP();
    $endpointsPNCP = array_keys(config('api.pncp.endpoints'));


$booArquivoRestrito = false;

if (!empty($entry->enviaDadosPncp->pncpable)) {
    $booArquivoRestrito = ($entry->log_name === 'inserir_arquivo_ata' &&  $entry->enviaDadosPncp->pncpable->restrito) ?? false;
}
@endphp

{{----------------------------------------------------
    caso a acao enviada esteja no array de endpoints
    caso permitirReenvio seja verdadeiro
    caso o registro nao tenha sido excluído
    caso quando arquivo que a privacidade seja publica
-----------------------------------------------------}}
@if(in_array($entry->log_name, $endpointsPNCP) && $permitirReenvio && $entry->deleted_at === null && !$booArquivoRestrito)
    <a
            href="{{$href}}"
            class="btn btn-link"
            title="reenviar para PNCP"
            style="padding: 0"
            onclick="exibirAlertaEnvioCustomizado('Reenviando para PNCP')"
    >
        <x-pncp-reenviar-icon/>
    </a>
@endif

@if(in_array($entry->log_name, $endpointsPNCP) && $permitirReenvio && $entry->deleted_at === null && $booArquivoRestrito)
    <a
            href="#"
            class="btn btn-xs btn-link"
            title="Arquivo restrito"
    >
        <i class="fa fa-lock"></i>
    </a>
@endif



