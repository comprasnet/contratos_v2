@if ($crud->hasAccess('create'))
    @php
        $external = $button->name === 'create_external' ? 1 : 0;

        $buttonText = $external ? 'Informar Recebimento' : 'Comunicar Recebimento';
        $textTooltip = $external ?
            'Informar quando ocorreram recebimentos de parte, ou total, do objeto desta Ordem de Serviço / Fornecimento
                em outros sistemas, inclusive inserindo Termos de Recebimento Provisório e/ou Definitivo, assim como
                outros documentos relacionados.' :
            'Comunicar recebimento de parte, ou total, do objeto desta Ordem de Serviço / Fornecimento para continuação
                do fluxo de recebimento dentro do sistema.';
    @endphp

    <button class="br-button primary mr-3" type="button" onclick=" window.location.href= '{{ url($crud->route.'/create?external=' . $external) }}'">
        <i class="fas fa-plus"></i> {{ $buttonText }}
    </button>


    @if($textTooltip)
        <div class="br-tooltip text-justify" role="tooltip" info="info" place="bottom">
            <span>{{ $textTooltip }}</span>
        </div>
    @endif
@endif
