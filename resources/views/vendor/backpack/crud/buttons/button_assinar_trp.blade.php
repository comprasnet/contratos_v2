@if(
    !in_array($entry->situacao->descres,
    [
        'ae_entrega_status_1',
        'ae_entrega_status_2',
        'ae_entrega_status_3',
        'ae_entrega_status_5',
        'ae_entrega_status_6',
        'ae_entrega_status_9',
    ])
)
    @php
        $trp = \App\Models\AutorizacaoExecucaoEntregaTRP::find($entry->id);
        $aeEntregaSignatarioService = new \App\Services\AutorizacaoExecucao\AutorizacaoExecucaoEntregaSignatarioService($trp);
        $canAssinar = $aeEntregaSignatarioService->verifyUsuarioCanAssinar();
    @endphp
    @if ($canAssinar)
        @php
            $title = 'Assinar TRP';
            $class = 'btn btn-sm btn-link';

            if (isset($prependIcon) && $prependIcon) {
                $class .= ' btn-block text-left';
            }
        @endphp
        <a
            style="text-decoration: none"
            class="{{ $class }}"
            href="{{ url($crud->route.'/trp/'.$entry->getKey().'/assinatura/create') }}"
            title="{{ $title }}"
        >
            <i class="fa fa-signature"></i>
            @if(isset($prependIcon) && $prependIcon)
                {{ $title }}
            @endif
        </a>
    @endif
@endif
