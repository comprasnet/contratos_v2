@if(
    $entry->situacao->descres == 'ae_status_2' ||
    $entry->situacao->descres == 'ae_status_4' ||
    $entry->situacao->descres == 'ae_status_8'
)
    @include('crud::buttons.duplicar', $entry)
@endif
