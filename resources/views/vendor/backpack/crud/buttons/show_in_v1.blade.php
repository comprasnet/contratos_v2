<a
    href="{{ $entry->getLinkShowInV1() }}"
    class="btn btn-link"
    target="_blank"
>
    <i class="fas fa-eye"></i>
    @include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => trans('backpack::crud.preview')])
</a>
