@if(in_array($entry->situacao->descres, ['ae_entrega_status_8', 'ae_entrega_status_12', 'ae_entrega_status_7']))
    @php
        $title = 'Download do TRD';
        $class = 'btn btn-sm btn-link';

        if (isset($prependIcon) && $prependIcon) {
            $class .= ' btn-block text-left';
        }
    @endphp
    <a
        style="text-decoration: none"
        class="{{$class}}"
        type="button"
        href="{{ url('storage/' .  $entry->getArquivoTRD()->url) }}"
        target="_blank"
        title="{{$title}}"
    >
        <span
            class="br-button primary"
            style="font-size: 11px; height: 22px; padding: 0 9px; color: white"
        >
            TRD
        </span>

        @if (isset($prependIcon) && $prependIcon)
            {{ $title }}
        @endif
    </a>
@endif
