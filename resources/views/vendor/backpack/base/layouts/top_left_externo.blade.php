<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}" dir="{{ config('backpack.base.html_direction') }}">

<head>
  @include(backpack_view('inc.head'))
</head>

<body class="{{ config('backpack.base.body_class') }}">
  @include(backpack_view('inc.main_header_externo'))

  <div class="app-body">
    @include(backpack_view('inc.sidebar_externo'))

    <main class=" flex-fill mb-8" id="main">
      <div class="container-lg ">
        <div class="row">
          <div class="col mb-5">
            <div class="main-content pl-sm-3 mt-4" id="main-content">

              @yield('before_breadcrumbs_widgets')

              @includeWhen(isset($breadcrumbs), backpack_view('inc.breadcrumbs'))

              @yield('after_breadcrumbs_widgets')

              @yield('header')

              <div class="container-fluid animated fadeIn">

                @yield('before_content_widgets')
      
                @yield('content')
                
                @yield('after_content_widgets')
      
              </div>

            </div>
          </div>
        </div>

      </div>
    </main>

    {{-- <main class="main pt-2">

       @yield('before_breadcrumbs_widgets')

       @includeWhen(isset($breadcrumbs), backpack_view('inc.breadcrumbs'))

       @yield('after_breadcrumbs_widgets')

       @yield('header')

        <div class="container-fluid animated fadeIn">

          @yield('before_content_widgets')

          @yield('content')
          
          @yield('after_content_widgets')

        </div>

    </main> --}}

  </div><!-- ./app-body -->

  <footer class="{{ config('backpack.base.footer_class') }}">
    @include(backpack_view('inc.footer'))
  </footer>

  @yield('before_scripts')
  @stack('before_scripts')

  @include(backpack_view('inc.scripts'))

  @yield('after_scripts')
  @stack('after_scripts')
</body>
</html>