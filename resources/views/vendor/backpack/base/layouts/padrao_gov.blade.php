<html lang="pt-BR">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Contratos.gov.br</title>
    <!-- Fonte Rawline-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/rawline.css"/>
    <!-- Fonte Raleway-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900&amp;display=swap"/>
    <!-- Design System de Governo-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/core.min.css"/>
    <!-- Fontawesome-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/all.min.css"/>
    <link rel="icon" type="image/x-icon" href="{{ config('backpack.base.project_somente_logo_ico') }}">
  </head>
  <body>
    <div class="template-base"> 
      <header class="br-header mb-4" id="header" data-sticky="data-sticky">
        <div class="container-lg">
          
          @yield('header')

        </div>
      </header>

      <main class=" flex-fill mb-8" id="main">
        <div class="container-lg ">
          <div class="row">
            <div class="col mb-5">
              <div class="main-content pl-sm-3 mt-4" id="main-content">

                @yield('content')

              </div>
            </div>
          </div>

        </div>
      </main>


      @yield('footer')
    </div>
  </body>
  
  <script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/core-init.js"></script>
  <script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/jquery-3.6.1.min.js"></script>
  <script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/jquery.mask.js"></script>

  @yield('after_scripts')
  @stack('after_scripts')

</html>

{{-- <!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{ config('backpack.base.html_direction') }}">
<head>
    @include(backpack_view('inc.head'))
</head>
<body class="app flex-row align-items-center">

  @yield('header')

  <div class="container">
  @yield('content')
  </div>

  <footer class="app-footer sticky-footer">
    @include('backpack::inc.footer')
  </footer>

  @yield('before_scripts')
  @stack('before_scripts')

  @include(backpack_view('inc.scripts'))

  @yield('after_scripts')
  @stack('after_scripts')
 teste 22
</body>
</html> --}}

