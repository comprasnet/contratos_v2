@php
$controller = new $widget['controller'];
//todo issue 435 customizar para os quadrados dos vencimentos
 @endphp
<div class="w-50 mb-3">
    <div class="br-list border rounded ml-2" style="position: relative" role="list">
        <div class="card-header {{ $widget['name'] }}-list-header">
            <div class="row">
                <div class="col">
                    <div class="font-sm text-gray-{{ $widget['name'] }}">
                        {{ $widget['titulo_cabecalho'] }}</div>
                </div>

            </div>
        </div>
        <span class="br-divider"></span>
        <div class="col"  align="center">
            @include(backpack_view('inc.widgets'), [ 'widgets' => app('widgets')->where('group', 'content')->toArray() ]);
        </div>
    </div>
</div>