@php use App\Services\Pncp\Arp\SolicitacoesService; @endphp
@php
    $analises = SolicitacoesService::getNumberSolicitacaoAdesaoFornecedor();
@endphp
<style>
    a:hover {
        background: none !important;
    }

    .text-acesso-rapido-item {
        font-size: 15px;
        color: #333333;
    }

    .text-acesso-rapido-subitem {
        color: #565C65;
    }
</style>
<div class="flex text-center w-100">
    <span style="font-size: 22px;">Acesso Rápido</span>
    <p style="font-size: 14px;">Selecione uma das opções abaixo</p>
</div>
<div class="m-6 row w-100 text-center icones-adesao">
    <div class="col w-100">
        <a href="/fornecedor/contrato">
            <img class="mb-2" src="/img/gestao_icon.png" style="height: 65px;">
        </a>
        <br>
        <a href="/fornecedor/contrato" style="text-decoration: none;">
            <span class="text-acesso-rapido-item">Contratos</span>
            <p class="font-xs text-acesso-rapido-subitem">Consulta aos contratos do fornecedor</p>
        </a>
    </div>
    <div class="col w-100">
        <a href="/fornecedor/autorizacaoexecucao">
            <i class="fas fa-file-invoice fa-5x mb-2" style="color: #295aa1;" aria-hidden="true">
            </i>
        </a>
        <br>
        <a href="/fornecedor/autorizacaoexecucao" style="text-decoration: none;">
            <span class="text-acesso-rapido-item">Ordens de Serviço / Fornecimento</span>
            <p class="font-xs text-acesso-rapido-subitem">Consulta às Ordens de Serviço do Fornecedor</p>
        </a>
    </div>
    <div class="col w-100">
        <a href="/fornecedor/contratoinstrumentodecobranca">
            <i class="fas fa-wallet fa-5x mb-2" style="color: #295aa1;" aria-hidden="true">
            </i>
        </a>
        <br>
        <a href="/fornecedor/contratoinstrumentodecobranca" style="text-decoration: none;">
            <span class="text-acesso-rapido-item">Instrumentos de Cobrança</span>
            <p class="font-xs text-acesso-rapido-subitem">
                Consulta às notas fiscais, faturas, recibos, boletos bancários, etc.
            </p>
        </a>
    </div>
    <div class="col w-100">
        <a href="/fornecedor/arp">
            <i class="fas fa-file-invoice-dollar fa-5x mb-2" style="color: #295aa1;" aria-hidden="true">
            </i>
        </a>
        <br>
        <a href="/fornecedor/arp" style="text-decoration: none;">
            <span class="text-acesso-rapido-item">Atas de Registro de Preços</span>
            <p class="font-xs text-acesso-rapido-subitem">Consulta às atas de registro de preços do fornecedor</p>
        </a>
    </div>
    <div class="col w-100">
        <a href="/fornecedor/arp/adesao/analisar">
            <img class="mb-2" src="/img/analise_adesao.png" style="height: 65px;">
            @if($analises)
                <div class="badge analise-{{$analises->status}}" aria-label="{{$analises->descricao}}"
                         style="margin-left: 10px;">

                    <a href="javascript:void(0);">{{$analises->qnt}}</a>
                    <div class="br-tooltip  analise-{{$analises->status}}" role="tooltip" info="info" place="top">
                        <span class="text" role="tooltip">{{ mb_substr($analises->descricao, 0, 20) }}</span>
                    </div>
                </div>
            @endif
        </a>
        <br>
        <a href="/fornecedor/arp/adesao/analisar" style="text-decoration: none;">
            <span class="text-acesso-rapido-item">Analisar Adesão</span>
            <p class="font-xs text-acesso-rapido-subitem">Analisar solicitação de adesão a ata</p>
        </a>
    </div>
</div>
