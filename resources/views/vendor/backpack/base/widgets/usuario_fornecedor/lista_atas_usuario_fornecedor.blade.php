<style>
    .card-header {
        background-color: #e6e6e7;
        font-weight: bold;
    }

    .color-icon {
        color: #2a5aa1;
    }

    .text-gray-{{ $widget['name'] }} {
        color: #333333;
    }
</style>
<div class="w-50">
    <div class="br-list border rounded ml-2" style="position: relative" role="list">
        <div class="card-header {{ $widget['name'] }}-list-header">
            <div class="row">
                <div class="col">
                    <div class="font-sm text-gray-{{ $widget['name'] }}">{{ $widget['titulo_cabecalho'] }}</div>
                </div>
                <div class="col">
                    <div  class="dataTables_filter">
                        <input type="search" class="form-control" placeholder="{{ $widget['placeholder_search'] }}"
                               id="search-{{ $widget['name'] }}" aria-controls="crudTable" control-id="ControlID-7">
                    </div>
                </div>
            </div>
        </div>
        <span class="br-divider"></span>
        <div class="loading loading-table-{{ $widget['name'] }} medium"></div>
        <div class="list-{{ $widget['name'] }}-dashboard">
        </div>
        <nav id="pagination-{{ $widget['name'] }}-dashboard"></nav>
    </div>
    <div id="item-model" style="display: none">
        {{--<div class="row align-items-center">--}}
            {{--<div class="col-auto">

                <i class="fas fa-heartbeat" aria-hidden="true"></i>

            </div>--}}
            {{--<div class="col">--}}
                <a  class="text-decoration-none">
                    <span class="font-lg color-icon"></span>
                    <p class="font-xs"  style="margin-bottom: -1.5em"></p>
                </a>
           {{-- </div>--}}
           {{-- <div class="col-auto {{ $widget['name'] }}-status"></div>--}}
       {{-- </div>--}}
    </div>
</div>