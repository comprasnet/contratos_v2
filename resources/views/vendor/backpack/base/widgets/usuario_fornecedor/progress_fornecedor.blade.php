@php
$controller = new $widget['controller'];
$count = 0;
$dataProgress = $controller->setup()['progress'];
$qtdProgress = count($dataProgress);
$qtdMd = (int) (12/$qtdProgress);
@endphp

<div class="w-50 mb-3">
    <div class="br-list border rounded ml-2" style="position: relative" role="list">
        <div class="card-header {{ $widget['name'] }}-list-header">
            <div class="row">
                <div class="col">
                    <div class="font-sm text-gray-{{ $widget['name'] }}">
                        {{ $widget['titulo_cabecalho'] }}</div>
                </div>

            </div>
        </div>
        <span class="br-divider"></span>
        <div class="col"  align="center">
            @foreach($dataProgress as $progress)
                @if ($count == 0)
                    <div class="row">
                @endif
                        <div class="col-md-{{$qtdMd}} {{$progress['wrapperClass'] ?? ''}} ">
                            @if (isset($progress['redirect']))
                            <a href="{{$progress['redirect']}}" style=" text-decoration: none; color: var(--color)">
                            @endif
                            <div class="{{ $progress['class'] ?? 'card text-white bg-primary' }}" style="{{$progress['style'] ?? ''}}">
                                <div class="card-body">
                                    @if (isset($widget['value']))
                                        <div class="text-value">{!! $widget['value'] !!}</div>
                                    @endif

                                    @if (isset($progress['description']))
                                            <div><b>{!! $progress['description'] !!}</b></div>
                                    @endif

                                    @if (isset($progress['progress']))
                                        <div class="progress progress-white progress-xs my-2">
                                            <div class="{{ $progress['progressClass'] ?? 'progress-bar bg-info' }}" role="progressbar" style="width: {{ $progress['progress']  }}%" aria-valuenow="{{ $progress['progress']  }}" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    @endif

                                    @if (isset($progress['hint']))
                                        <small class="" style="color: var(--color)">{!! $progress['hint'] !!}</small>
                                    @endif
                                </div>

                                @if (isset($progress['footer_link']))
                                    <div class="card-footer px-3 py-2">
                                        <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="{{ $progress['footer_link'] ?? '#' }}"><span class="small font-weight-bold">{{ $progress['footer_text'] ?? 'View more' }}</span><i class="la la-angle-right"></i></a>
                                    </div>
                                @endif
                            </div>
                            @if (isset($progress['redirect']))
                            </a>
                            @endif
                        </div>

                @php $count ++; @endphp

                @if($count == 0)
                    </div>
                @endif

                @if($count == $qtdProgress)
                    </div>
                @endif

                @if($count == 6)
                    </div>
                    @php $count =0; @endphp
                @endif

            @endforeach
        </div>
    </div>
</div>