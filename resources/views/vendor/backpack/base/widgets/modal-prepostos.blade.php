<style>
    .fontepequena {
        font-size: 12px;
    }
    .br-modal-footer .br-button {
        margin: 0 8px;
    }
</style>
<div class="br-scrim-util foco" id="{{$widget['id']}}" data-scrim="true">
    <div class="br-modal large">
        <div class="br-modal-header"></div>
        <div class="br-modal-body">
            <div class="br-textarea">
                <table class="table-hint">
                    <tbody id="prepostos_tbody">
                    <!-- Os dados dos prepostos serão inseridos aqui via JavaScript -->
                    <tr>
                        <td class="fontepequena">Carregando...</td>
                        <td class="fontepequena">Carregando...</td>
                    </tr>
                    </tbody>
                </table>
                <div id="pagination" class="pagination-container">
                    <!-- A paginação será renderizada aqui -->
                </div>
            </div>
        </div>
        <div class="br-modal-footer justify-content-center">
            <button class="br-button secondary" type="button" id="btn_fechar_modal_preposto"
                    data-dismiss="scrimexample">Voltar
            </button>
            @if(session()->get('tipo_acesso') == 'Administrador')
            <button class="br-button primary" type="button" id="btn_indicar_preposto" data-contrato-id="">
                Indicar Preposto
            </button>
            @endif
        </div>
    </div>
</div>
