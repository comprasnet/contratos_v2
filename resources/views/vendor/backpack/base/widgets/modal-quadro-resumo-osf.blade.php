<style>
    .fontepequena {
        font-size: 12px;
    }
</style>
<div class="br-scrim-util foco" id="{{$widget['id']}}" data-scrim="true">
    <div class="br-modal large">
        <div class="br-modal-header"></div>
        <div class="br-modal-body">
            <div class="br-textarea">
                <table class="table-hint">
                    <thead>
                    <tr>
                        <th></th>
                        <th class="text-bold">Qtde. Total</th>
                        <th class="text-bold">Glosa</th>
                        <th class="text-bold">Valor Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-bold">Qtde. Solicitada (OS/F)</td>
                        <td class="fontepequena"><span id="qtd_solicitada_osf"></span></td>
                        <td style="text-decoration: line-through;color: black;" class="fontepequena">
                           {{-- R$ <span id="qtd_solicitada_valor_glosa_osf"></span>--}}
                            {{--<span></span>--}}
                            <span>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </span>
                        </td>
                        <td class="fontepequena">R$ <span id="qtd_solicitada_valor_total_osf"></span></td>
                    </tr>
                    <tr>
                        <td class="text-bold">Qtde. em Análise (Até TRP)</td>
                        <td class="fontepequena"><span id="qtd_total_ate_trp_osf"></span></td>
                        <td class="fontepequena">R$ <span id="valor_glosa_ate_trp_osf"></span></td>
                        <td class="fontepequena">R$ <span id="valor_total_ate_trp_osf"></span></td>
                    </tr>
                    <tr>
                        <td class="text-bold">Qtde. em Avaliação (Até TRD)</td>
                        <td class="fontepequena"><span id="qtd_total_ate_trd_osf"></span></td>
                        <td class="fontepequena">R$ <span id="valor_glosa_ate_trd_osf"></span></td>
                        <td class="fontepequena">R$ <span id="valor_total_ate_trd_osf"></span></td>
                    </tr>
                    <tr>
                        <td class="text-bold">Qtde. Executada (Após TRD)</td>
                        <td class="fontepequena"><span id="qtd_total_apos_trd_osf"></span></td>
                        <td class="fontepequena">R$ <span id="valor_glosa_apos_trd_osf"></span></td>
                        <td class="fontepequena">R$ <span id="valor_total_apos_trd_osf"></span></td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td class="text-bold">Saldo a Executar</td>
                        <td class="text-bold fontepequena"><span id="qtd_total_saldo_executar"></span></td>
                        <td class="text-bold fontepequena" style="text-decoration: line-through;color: black;">
                           {{-- R$ <span id="valor_glosa_saldo_executar"></span>--}}
                            <span>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </span>
                        </td>
                        <td class="text-bold fontepequena">R$ <span id="valor_total_saldo_executar"></span></td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
        <div class="br-modal-footer justify-content-center">
            <button class="br-button secondary" type="button" id="btn_fechar_modal_cancelamento"
                    data-dismiss="scrimexample">Voltar
            </button>
        </div>
    </div>
</div>