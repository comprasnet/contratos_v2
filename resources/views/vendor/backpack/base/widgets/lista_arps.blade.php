@php
    $unidadeSession = \App\Repositories\ArpsDashboardRepository::unidadeSession();
    $isTransparencia = \Illuminate\Support\Facades\Request::is('transparencia');

@endphp

<style>
    .card-header {
        background-color: #e6e6e7;
        font-weight: bold;
    }

    .color-icon {
        color: #2a5aa1;
    }

    .text-gray-arp {
        color: #333333;
    }
</style>
<div class="br-list border rounded w-50 pb-2" style="height: fit-content; position: relative" role="list">
    <div class="card-header arp-list-header">
        <div class="row">
            <div class="col">
                <div class="font-sm text-gray-arp">Lista de Atas de Registro de Preços</div>
            </div>
            <div class="col">
                @if (!$isTransparencia)
                    <div class="dataTables_filter">
                        <input type="search" class="form-control" placeholder="Pesquisar Número da Ata/Ano" id="search-ata" aria-controls="crudTable" control-id="ControlID-7">
                    </div>
                @endif
            </div>
        </div>
    </div>
    <span class="br-divider"></span>
    <div class="loading loading-table medium"></div>
    <div class="list-arp-dashboard" data-unidade="{{$unidadeSession?$unidadeSession->id:""}}">
    </div>
    <nav id="pagination-arp-dashboard"></nav>
</div>
<div id="item-model" style="display: none">
    <div class="row align-items-center">
        <div class="col-auto">
            {{--                <i class="fas fa-heartbeat" aria-hidden="true"></i>--}}
        </div>
        <div class="col">
            <a  class="text-decoration-none">
                <span class="font-lg color-icon"></span>
                <p class="font-xs"></p>
            </a>
        </div>
        <div class="col-auto arp-status"></div>
    </div>
</div>