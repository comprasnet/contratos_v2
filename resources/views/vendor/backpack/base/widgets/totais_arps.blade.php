@php use App\Services\Pncp\Arp\DashboardArpService; @endphp
@php
    $arp = DashboardArpService::getRepositoryByRequest();
    $arps_total = $arp->getValorTotalArp();
    $uasgs = $arp->getUasgs();
    $isTransparencia = \Illuminate\Support\Facades\Request::is('transparencia');

@endphp
<style>
    .arp-valor-contratado {
        font-size: 25px;
        font-weight: bold;
        color: #2a5aa1;
    }

    .header-background-totais {
        background-color: #2a5aa1 !important;
    }
</style>
<div class="w-100">
    <div class="br-card">
        <div class="card-header p-0 pb-2 pt-2 header-background-totais">
            <div class="flex text-center align-center">
                <div class="text-center">
                    <span class="text-weight-semi-bold text-up-02 text-white">Atas de Registro de Preços</span>
                    <p class="font-sm text-white mb-0">Conforme filtro</p>
                </div>
                @if (!$isTransparencia)
                    <div class="float-right ml-auto mr-2 scrimutilexemplo2" style="margin-top: -44px;">
                        <button class="br-button circle" type="button">
                            <i class="fa fa-filter text-white" aria-hidden="true"></i>
                        </button>
                    </div>
                @endif
            </div>
        </div>
        <div class="card-content text-center">
            <span class="arp-valor-contratado">{{'R$ ' . number_format($arps_total,2,',','.')}}</span>
            <p class="font-sm">Valor Total Registrado</p>
        </div>
    </div>
</div>

<div class="br-scrim-util foco" id="scrimutilexample2" data-scrim="true">
    <div class="br-modal w-100">
        <div class="br-modal-header">Filtrar atas de registro de preços</div>
        <form action="" method="GET">
            <input type="hidden" name="has_filter" value="1">
            <div class="br-modal-body">
                <div class="row">
                    <div class="br-datetimepicker col-md-6" data-mode="single" data-type="text">
                        <div class="br-input has-icon">
                            <label for="vigencia_inicial">Vigência Inicial</label>
                            <input id="vigencia_inicial" type="text" name="vigencia_inicial" placeholder="dd/mm/aaaa"
                                   data-input="data-input" value="{{$arp->getVigenciaInicial()}}"/>
                            <button class="br-button circle small" type="button" aria-label="Abrir Timepicker"
                                    data-toggle="data-toggle" id="simples-input-btn"><i class="fas fa-calendar-alt"
                                                                                        aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                    <div class="br-datetimepicker col-md-6" data-mode="single" data-type="text">
                        <div class="br-input has-icon">
                            <label for="vigencia_final">Vigência Final</label>
                            <input id="vigencia_final" type="text" name="vigencia_final" placeholder="dd/mm/aaaa"
                                   data-input="data-input" value="{{$arp->getVigenciaFinal()}}"/>
                            <button class="br-button circle small" type="button" aria-label="Abrir Timepicker"
                                    data-toggle="data-toggle" id="simples-input-btn"><i class="fas fa-calendar-alt"
                                                                                        aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="br-select w-100" style="max-width: none;">
                    <div class="br-input large input-button w-100">
                        <label for="input-search-large">Unidade</label>
                        <select name="uasg[]" class="select2" data-ajax="true" data-placeholder="Selecione uma UASG"
                                style="width: 100%;" id="uasg" multiple="multiple">
                        </select>
                    </div>
                </div>

                <div class="br-input mt-2">
                    <label for="numero_ano">Número da compra/Ano</label>
                    <div class="input-group">
                        <input name="numero_ano" id="numero_ano" type="text" placeholder="Número/Ano da Compra"
                               value="{{$arp->getNumeroAno()}}"/>
                    </div>
                </div>

            </div>
            <div class="br-modal-footer justify-content-center">
                <button class="br-button secondary" type="button" id="scrimfechar" data-dismiss="scrimexample">Cancelar
                </button>
                <button class="br-button primary mt-3 mt-sm-0 ml-sm-3" type="submit">Filtrar
                </button>
            </div>
        </form>
    </div>
</div>

@section('after_scripts')

    <script>
        $(document).ready(function () {

            $('#numero_ano').mask('99999/9999');

            $('#uasg').select2({
                ajax: {
                    url: '/arp/get-uasgs',
                    dataType: 'json',
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.codigo + ' ' + item.nome,
                                    id: item.id
                                }
                            })
                        };
                    },
                    cache: true
                },
                multiple: true
            });

            <?php if (!empty($uasgs)) { ?>
            var selectedOptions = [];
                <?php foreach ($uasgs as $uasgAux){ ?>
            selectedOptions.push({{$uasgAux}})
            <?php } ?>

            var uasgsSelect = $('#uasg');
            $.ajax({
                type: 'GET',
                url: '/arp/get-selected-uasgs',
                data: {
                    items: selectedOptions,
                },
            }).then(function (data) {

                data.forEach(updateSelect);

                function updateSelect(item) {
                    var option = new Option(item.text, item.id, true, true);
                    uasgsSelect.append(option).trigger('change');

                    // manually trigger the `select2:select` event
                    uasgsSelect.trigger({
                        type: 'select2:select',
                        params: {
                            data: item
                        }
                    });
                }
            });
            <?php } ?>
        });
    </script>

@endsection