<div class="br-scrim-util foco" id="{{$widget['id']}}" data-scrim="true" data-visible="true" aria-expanded="true">
    <div class="br-modal">
        <div class="br-modal-header"></div>
        <div class="br-modal-body">
            <div class="br-textarea">
                <label for="justificativa_cancelamento">A unidade solicitante afirmou que se trata de uma execução
                    descentralizada de programa ou projeto federal com recursos financeiros provenientes de
                    transferências voluntárias da União para Estados, municípios e Entidades da administração pública
                    federal integrantes dos Orçamentos Fiscal e da Seguridade Social da União e a Organizações da
                    Sociedade Civil (OSC), conforme inciso I, § 2ºdo Art. 32 do Decreto nº 11.462 de 31/03/2023 e que a
                    aceitação dessa solicitação de adesão não levará em consideração o saldo máximo de adesão do
                    artigo citado.
                </label>
            </div>
        </div>
        <div class="br-modal-footer justify-content-center">
            <button class="br-button secondary" type="button" id="btn_voltar_listagem_adesao"
                    data-dismiss="scrimexample">Voltar
            </button>
            <button class="br-button primary mt-3 mt-sm-0 ml-sm-3"
                    type="button" id="btn_ciente_projeto_ente_federal">Ciente
            </button>
        </div>
    </div>
</div>
@push('after_scripts')
    <script>
        $("#btn_voltar_listagem_adesao").click(function() {
            window.location.href='../'
        });

        $("#btn_ciente_projeto_ente_federal").click(function() {
            $("#{{$widget['id']}}").removeClass("active");
        });
    </script>
@endpush
