@includeWhen(!empty($widget['wrapper']), 'backpack::widgets.inc.wrapper_start')

<div>
    @if (isset($widget['subtitle']))
        <h2 class="h3 flex align-center">
            {{ $widget['subtitle'] }}
            @if (isset($widget['hint']))
                <i id="subtitle-hint" class="primary fas fa-info-circle"></i>
            @endif
        </h2>

        @if (isset($widget['hint']))
            <div class="br-tooltip text-justify" role="tooltip" info="info" place="right"
                 id="subtitle-hint">
                <span>{{ $widget['hint'] }}</span>
            </div>
        @endif
    @endif
</div>

@includeWhen(!empty($widget['wrapper']), 'backpack::widgets.inc.wrapper_end')
