@php use App\Services\Pncp\Arp\SolicitacoesService; @endphp
@php
    $solicitacoes = SolicitacoesService::getNumberSolicitacao(true);
    $analises = SolicitacoesService::getNumberSolicitacao();
@endphp
<style>
    a:hover {
        background: none !important;
    }

    .text-acesso-rapido-item {
        font-size: 15px;
        color: #333333;
    }

    .text-acesso-rapido-subitem {
        color: #565C65;
    }
</style>
<div class="flex text-center w-100">
    <span style="font-size: 22px;">Acesso Rápido</span>
    <p style="font-size: 14px;">Selecione uma das opções abaixo</p>
</div>
<div class="m-6 row w-100 text-center icones-adesao">
    @can('gestaodeatas_V2_acessar')
        <div class="col w-100">
            <a href="/arp?situacao=%5B%22Ata+de+Registro+de+Preços%22%5D">
                <i class="fas fa-file-invoice-dollar fa-5x mb-2" style="color: #295aa1;" aria-hidden="true"></i>
            </a>
            <br>
            <a href="/arp?situacao=%5B%22Ata+de+Registro+de+Preços%22%5D" style="text-decoration: none;">
                <span class="text-acesso-rapido-item">Atas de Registro de Preços</span>
                <p class="font-xs text-acesso-rapido-subitem">Consulta a Atas de Registro de Preços da unidade</p>
            </a>
        </div>
    @endcan
    <div class="col w-100">
        <a href="/compras">
            <i class="fa fa-cart-arrow-down fa-5x mb-2" style="color: #295aa1;" aria-hidden="true"></i>
        </a>
        <br>
        <a href="/compras" style="text-decoration: none;">
            <span class="text-acesso-rapido-item">Compras</span>
            <p class="font-xs text-acesso-rapido-subitem">Consulta ao Saldo de Compras</p>
        </a>
    </div>
    <div class="col w-100">
        <a href="autenticarautomatica/gescon-contrato?situacao=[1]">
{{--        <a href="{{str_replace('appredirect', 'login', env('URL_CONTRATO_VERSAO_UM'))}}">--}}
            <img class="mb-2" src="/img/gestao_icon.png" style="height: 68px;">
        </a>
        <br>
        <a href="autenticarautomatica/gescon-contrato?situacao=[1]" style="text-decoration: none;">
{{--        <a href="{{str_replace('appredirect', 'login', env('URL_CONTRATO_VERSAO_UM'))}}" style="text-decoration: none;">--}}
            <span class="text-acesso-rapido-item">Gestão Contratual</span>
            <p class="font-xs text-acesso-rapido-subitem">Módulos de Gestão Contratual, incluindo aspectos orçamentários
                e financeiros</p>
        </a>
    </div>
    @can('gestaodeatas_V2_acessar')
        <div class="col w-100">
            <a href="/arp/adesao">
                <img class="mb-2" src="/img/solicitar_adesao.png" style="height: 65px;">
            </a>

            @if($solicitacoes)
                <div class="mb-3 badge solicitacao-{{$solicitacoes->status}}" aria-label="{{$solicitacoes->descricao}}">
                    <a href="javascript:void(0);">{{$solicitacoes->qnt}}</a>
                    <div class="br-tooltip  solicitacao-{{$solicitacoes->status}}" role="tooltip" info="info" place="top">
                        <span class="text" role="tooltip">{{$solicitacoes->descricao}}</span>
                    </div>
                </div>
            @endif
            <br>
            <a href="/arp/adesao" style="text-decoration: none;">

                <span class="text-acesso-rapido-item">Solicitar Adesão</span>
                <p class="font-xs text-acesso-rapido-subitem">Solicitar adesão a ata</p>
            </a>
        </div>
        <div class="col w-100">
            <a href="/arp/adesao/analisar">
                <img class="mb-2" src="/img/analise_adesao.png" style="height: 65px;">
                @if($analises)
                    <div class="badge analise-{{$analises->status}}" aria-label="{{$analises->descricao}}"
                         style="margin-left: 10px;">

                        <a href="javascript:void(0);">{{$analises->qnt}}</a>
                        <div class="br-tooltip  analise-{{$analises->status}}" role="tooltip" info="info" place="top">
                            <span class="text" role="tooltip">{{$analises->descricao}}</span>
                        </div>
                    </div>
                @endif
            </a>
            <br>
            <a href="/arp/adesao/analisar" style="text-decoration: none;">
                <span class="text-acesso-rapido-item">Analisar Adesão</span>
                <p class="font-xs text-acesso-rapido-subitem">Analisar solicitação de adesão a ata</p>
            </a>
        </div>
    @endcan
    @can('fiscalizacao_V2_acessar')
        <div class="col w-100">
            <a href="/meus-contratos?situacao=[1]">
                <img class="mb-2" src="/img/gestao_fiscalizacao_contratos.png" alt="icone fiscalização e gestão de contratos" style="height: 69px;" />
            </a>
            <br>
            <a href="/meus-contratos?situacao=[1]" style="text-decoration: none;">
                <span class="text-acesso-rapido-item">Fiscalização e Gestão de Contratos</span>
                <p class="font-xs text-acesso-rapido-subitem">Consulta de Fiscalização e Gestão de Contratos</p>
            </a>
        </div>
    @endcan
</div>
