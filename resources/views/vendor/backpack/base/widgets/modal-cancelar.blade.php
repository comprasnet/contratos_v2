<div class="br-scrim-util foco" id="{{$widget['id']}}" data-scrim="true">
    <div class="br-modal">
        <div class="br-modal-header"></div>
        <div class="br-modal-body">
            <div class="br-textarea">
                <label for="justificativa_cancelamento">Justificativa para o cancelamento</label>
                <textarea id="justificativa_cancelamento"
                          placeholder="Preencher o motivo pelo qual será cancelada"
                          maxlength="250"
                          style="height: 200px"></textarea>
                <div class="text-base mt-1">
                    <span class="limit">Limite máximo de <strong>250</strong> caracteres</span>
                    <span class="current"></span>
                </div>
            </div>
        </div>
        <div class="br-modal-footer justify-content-center">
            <button class="br-button secondary" type="button" id="btn_fechar_modal_cancelamento"
                    data-dismiss="scrimexample">Voltar
            </button>
            <button class="br-button primary mt-3 mt-sm-0 ml-sm-3"
                    type="button" id="btn_realizar_cancelamento">Realizar cancelamento
            </button>
        </div>
    </div>
</div>