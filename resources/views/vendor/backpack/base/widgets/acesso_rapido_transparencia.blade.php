<style>
    a:hover {
        background: none !important;
    }

    .text-acesso-rapido-item {
        font-size: 15px;
        color: #333333;
    }

    .text-acesso-rapido-subitem {
        color: #565C65;
    }
</style>
<div class="flex text-center w-100">
    <span style="font-size: 22px;">Acesso Rápido</span>
    <p style="font-size: 14px;">Selecione uma das opções abaixo</p>
</div>

<div class="m-6 row w-100 text-center">
    <div class="col w-100">
        <a href="{{ route('transparencia.arp')  }}">
            <i class="fas fa-file-invoice-dollar fa-5x mb-2" style="color: #295aa1;" aria-hidden="true"></i>
        </a>
        <br>
        <a href="{{ route('transparencia.arp')  }}" style="text-decoration: none;">
            <span class="text-acesso-rapido-item">Consultar Atas</span>
            <p class="font-xs text-acesso-rapido-subitem">Consulta a Atas de Registro de Preços</p>
        </a>
    </div>
    <div class="col w-100">
        <a href="{{ '/transparencia/arp-item' }}">
            <img class="mb-2" src="/img/consultar-item.png" style="height: 65px;">
        </a>
        <br>
        <a href="{{ '/transparencia/arp-item' }}" style="text-decoration: none;">
            <span class="text-acesso-rapido-item">Consultar Itens de Atas</span>
            <p class="font-xs text-acesso-rapido-subitem">Consulta de Itens de Atas de Registro de Preços</p>
        </a>
    </div>
    <div class="col w-100">
        {{--<a href="autenticarautomatica/gescon-contrato?situacao=[1]">--}}
        <a href="https://contratos.comprasnet.gov.br/transparencia" target="_blank">
            <img class="mb-2" src="/img/gestao_icon.png" style="height: 65px;">
        </a>
        <br>
        {{--        <a href="autenticarautomatica/gescon-contrato?situacao=[1]" style="text-decoration: none;">--}}
        <a href="https://contratos.comprasnet.gov.br/transparencia" target="_blank" style="text-decoration: none;">
            <span class="text-acesso-rapido-item">Gestão Contratual</span>
            <p class="font-xs text-acesso-rapido-subitem">Módulos de Gestão Contratual, incluindo aspectos
                orçamentários e financeiros</p>
        </a>
    </div>
    <div class="col w-100">
        <a href="/transparencia/compras">
            <i class="fa fa-cart-arrow-down fa-5x mb-2" style="color: #295aa1;" aria-hidden="true"></i>
        </a>
        <br>
        <a href="/transparencia/compras" style="text-decoration: none;">
            <span class="text-acesso-rapido-item">Compras</span>
            <p class="font-xs text-acesso-rapido-subitem">Consulta ao Saldo de Compras</p>
        </a>
    </div>
</div>