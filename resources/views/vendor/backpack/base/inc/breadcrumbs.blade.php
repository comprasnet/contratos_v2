{{-- @if (config('backpack.base.breadcrumbs') && isset($breadcrumbs) && is_array($breadcrumbs) && count($breadcrumbs))
	<nav aria-label="breadcrumb" class="d-none d-lg-block">
	  <ol class="breadcrumb bg-transparent p-0 {{ config('backpack.base.html_direction') == 'rtl' ? 'justify-content-start' : 'justify-content-end' }}">
	  	@foreach ($breadcrumbs as $label => $link)
	  		@if ($link)
			    <li class="breadcrumb-item text-capitalize"><a href="{{ $link }}">{{ $label }}</a></li>
	  		@else
			    <li class="breadcrumb-item text-capitalize active" aria-current="page">{{ $label }}</li>
	  		@endif
	  	@endforeach
	  </ol>
	</nav>
@endif --}}
@php
	/*$logadopeloCompras = session()->get('acesso_pelo_compras');
    if($logadopeloCompras) {
	    return;
	}*/
@endphp
 @if (config('backpack.base.breadcrumbs') && isset($breadcrumbs) && is_array($breadcrumbs) && count($breadcrumbs))

<div class="br-breadcrumb">
	<ul class="crumb-list">
		@php
            $buttonVoltar = '';
		@endphp
		@foreach ($breadcrumbs as $label => $link)

			@if($label == 'Icone')
				@php
                        $dadosIcone = json_decode($link,true);

                        $label = '<a  href= "'.$dadosIcone['url'].'"> Voltar</a>';
                        $link = false;
				@endphp
			@endif

			@if ($link)

				@if($label == 'Admin')
					<li class="crumb home"><a class="br-button circle" href="{{$link}}"><span class="sr-only">Página inicial</span><i class="fas fa-home"></i></a></li>
				@else
					<li class="crumb "><i class="icon fas fa-chevron-right f100"></i><a href="{{ $link }}">{{ $label }}</a></li>
				@endif

			@else

				@if($label !== 'Inicio')
					<li class="crumb" data-active="active"><i class="icon fas fa-chevron-right"></i> <span> {!! $label !!} </span> </li>
				@endif

			@endif

		@endforeach
	</ul>
  </div>

@endif