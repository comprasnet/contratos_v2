@php 
$titulo = null;
$subTitulo = null;
if(isset($crud)) {
  $titulo = $crud->getHeading() ?? $crud->entity_name_plural;
  $subTitulo = $crud->getSubheading().' '.$crud->entity_name ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name;
}
$uasgUsuario = session('user_ug') ?? false;
$logadoComoFornecedor = session()->get('fornecedor_id') ?? false;
$cnpjCpfFornecedor = session()->get('fornecedor_cpf_cnpj_idgener') ?? false;
$tipoAcesso = session()->get('tipo_acesso') ?? null;
$nomeFornecedor = session()->get('nomeRazaoSocialFornecedor') ?? null;
@endphp

<header class="br-header mb-4" id="header" data-sticky="data-sticky">
  <div class="container-lg">
    <div class="header-top">
      <div class="header-logo">
        <div class="header-menu-trigger" id="header-navigation">
          <!-- <button class="br-button small circle" type="button" aria-label="Menu" data-toggle="menu" data-target="#main-navigation" id="navigation"><i class='nav-icon la la-bars'></i>
          </button> -->
        </div>
        <img
                style="-webkit-transform: scale(1.0);-moz-transform: scale(1.0);-o-transform: scale(1.0);
                -ms-transform: scale(1.0);
                transform: scale(1.0);"
                class="m-0" src="{{ config('backpack.base.project_logo') }}"
                alt="{!! env('APP_NAME') !!}"
        />
        <span class="br-divider vertical mx-2"></span>
        <div class="header-sign">
          <div class="header-title">
            @if(!$logadoComoFornecedor)
              @if(backpack_user() && $uasgUsuario)
                <div class="header-title">
                  {{ backpack_user()->name }} - UASG: {{$uasgUsuario}}
                </div>
              @endif
            @endif
            @if($logadoComoFornecedor)
              @if(backpack_user() && !$uasgUsuario)
                <div class="header-title">
                  {{ backpack_user()->name }}
                </div>
                <div class="header-title">
                  Identificador Fornecedor: {{$cnpjCpfFornecedor}} - {{$nomeFornecedor}}
                </div>
                <div class="header-title">
                    Tipo acesso: {{$tipoAcesso}}
                </div>
              @endif
              @if(backpack_user() && $uasgUsuario)
                <div class="header-title">
                  {{ backpack_user()->name }} - UASG: {{$uasgUsuario}}
                </div>
                <div class="header-title">
                  Identificador Fornecedor: {{$cnpjCpfFornecedor}} - {{$nomeFornecedor}}
                </div>
                <div class="header-title">
                  Tipo acesso: {{$tipoAcesso}}
                </div>
              @endif
            @endif
          </div>
        </div>
      </div>
      <div class="header-actions">
        <span class="br-divider vertical mx-half mx-sm-1">
            @include(backpack_view('inc.area_notificacao_fornecedor'))
        </span>
        <div class="header-login">
          <div class="header-sign-in">
          </div>
          <div class="header-avatar"></div>
          <div class="header-info">
            @if(backpack_user())
              <button class="br-button circle" type="button" onclick="window.location.href='{{ backpack_url('logout') }}'">
                <i class="fas fa-sign-out-alt"></i>
              </button>
            @else
              <button class="br-sign-in circle mt-3 mt-sm-0 ml-sm-3" type="button" onclick="window.location.href='{{ backpack_url('login') }}'">
                <i class="fas fa-user" aria-hidden="true">

                </i>
              </button>
            @endif
          </div>
        </div>
      </div>
    </div>
    <div class="header-bottom">
      <div class="header-menu">
        <div class="header-menu-trigger" id="header-navigation">
          <button class="br-button small circle" type="button" aria-label="Menu" data-toggle="menu" data-target="#main-navigation" id="navigation"><i class="fas fa-bars" aria-hidden="true"></i>
          </button>
        </div>
        <div class="header-info">
          <div class="header-title">{!! $titulo !!}</div>
          <div class="header-subtitle">{!!  $subTitulo !!}</div>
           <div class="header-title">{!! isset($filter['titulo']) ? $filter['titulo'] : '' !!}</div>
           <div class="header-subtitle">{!! isset($filter['subTitulo']) ? $filter['subTitulo'] : '' !!}</div>
        </div>
      </div>
    </div>
  </div>
</header>