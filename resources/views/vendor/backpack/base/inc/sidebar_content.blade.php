{{-- Exemplo de Menu com quatro níveis --}}

{{-- <div class="menu-folder"><a class="menu-item" href="javascript: void(0)"><span class="icon"><i class="fas fa-bell" aria-hidden="true"></i></span><span class="content">Nível 1</span></a>
    <ul>
      <li><a class="menu-item" href="javascript: void(0)"><span class="icon"><i class="fas fa-heart" aria-hidden="true"></i></span><span class="content">Nível 2</span></a></li>
      <li><a class="menu-item" href="javascript: void(0)"><span class="icon"><i class="fas fa-address-book" aria-hidden="true"></i></span><span class="content">Nível 2</span></a>
        <ul>
          <li><a class="menu-item" href="javascript: void(0)"><span class="icon"><i class="fas fa-book" aria-hidden="true"></i></span><span class="content">Nível 3</span></a></li>
          <li><a class="menu-item" href="javascript: void(0)"><span class="icon"><i class="fas fa-tree" aria-hidden="true"></i></span><span class="content">Nível 3</span></a>
            <ul>
              <li><a class="menu-item" href="javascript: void(0)"><span class="content">Nível 4</span></a></li>
              <li><a class="menu-item" href="javascript: void(0)"><span class="content">Nível 4</span></a></li>
              <li><a class="menu-item" href="javascript: void(0)"><span class="content">Nível 4</span></a></li>
            </ul>
          </li>
          <li><a class="menu-item" href="javascript: void(0)"><span class="icon"><i class="fas fa-moon" aria-hidden="true"></i></span><span class="content">Nível 3</span></a></li>
        </ul>
      </li>
      <li><a class="menu-item" href="javascript: void(0)"><span class="icon"><i class="fas fa-archive" aria-hidden="true"></i></span><span class="content">Nível 2</span></a></li>
    </ul>
  </div> --}}

<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
{{-- <a class="menu-item divider" href="{{ backpack_url('dashboard') }}"> <span class="icon"> <i class='nav-icon la la-home'></i> </span><span class="content">Início</span></a>

<div class="menu-folder"><a class="menu-item" href="javascript: void(0)"><span class="icon"><i class="nav-icon la la-shopping-cart" aria-hidden="true"></i></span><span class="content">Compras</span></a>
  <ul>
    <li><a class="menu-item" href="{{ backpack_url('compras') }}"><span class="icon"><i class="nav-icon la la-search" aria-hidden="true"></i></span><span class="content">Visualizar</span></a></li>
    <li><a class="menu-item" href="{{ backpack_url('compras') }}/create"><span class="icon"><i class="nav-icon la la-plus" aria-hidden="true"></i></span><span class="content">Cadastrar</span></a></li>
  </ul>
</div> --}}

{{-- <a class="menu-item divider" href="{{ backpack_url('compras') }}"> <span class="icon"> <i class='nav-icon la la-shopping-cart'></i> </span><span class="content">Compras</span></a> --}}

{{-- <li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> Início</a></li> --}}

{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('codigos') }}'><i class='nav-icon la la-question'></i> Codigos</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('codigo') }}'><i class='nav-icon la la-question'></i> Codigos</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('codigo-item') }}'><i class='nav-icon la la-question'></i> Codigo items</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('estado') }}'><i class='nav-icon la la-question'></i> Estados</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('municipio') }}'><i class='nav-icon la la-question'></i> Municipios</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('unidade') }}'><i class='nav-icon la la-question'></i> Unidades</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('nsu') }}'><i class='nav-icon la la-question'></i> Nsus</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('fornecedor') }}'><i class='nav-icon la la-question'></i> Fornecedors</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('nfe') }}'><i class='nav-icon la la-question'></i> Nves</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('nfe-item') }}'><i class='nav-icon la la-question'></i> Nfe items</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('consulta-nfe') }}'><i class='nav-icon la la-question'></i> Consulta nves</a></li>--}}

{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('compras') }}'><i class='nav-icon la la-sign-out-alt'></i> Compras</a></li> --}}


<?php
$menus = config('menu.menu');
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

function converterTextoNumeroMenu(?int $numeroMenu) {
  switch ($numeroMenu) {
    case 1:
      return 'um';
    break;

    case 2:
      return 'dois';
    break;

    case 3:
      return 'tres';
    break;

  }
}

foreach($menus as $modulo => $menu) {

  $nivelMenuTexto = converterTextoNumeroMenu($menu[':nivelMenu']);
  $menu[':nivelMenuTexto']  = $nivelMenuTexto;
  $estruturaMenu = config("menu.pattern.structure.{$menu[':nivelMenuTexto']}");
  $menu[':permissoesCrud'] = empty(config("security.permission_block_crud.{$modulo}"))? [] : config("security.permission_block_crud.{$modulo}");

  if(!empty($estruturaMenu)) {
    $estruturaMenu = substituirCampos($estruturaMenu, $menu);
  }

  echo $estruturaMenu;
}

function substituirCampos(string $estruturaMenu, array $menu) {
  $menu[':nivelMenuTexto'] = Str::ucfirst($menu[':nivelMenuTexto']);

  $iconeMenu = config("menu.pattern.structure.iconeMenu").$menu[':nivelMenuTexto'];
  $textoMenu = config("menu.pattern.structure.textoMenu").$menu[':nivelMenuTexto'];
  $rota = config("menu.pattern.structure.rota").$menu[':nivelMenuTexto'];
  $permissao = config("menu.pattern.structure.permissao").$menu[':nivelMenuTexto'];
  $target = config("menu.pattern.structure.target").$menu[':nivelMenuTexto'];
  $itemMenuDois = config("menu.pattern.structure.itemMenu");

  $paginaDeAcesso = explode("/",$menu[$rota]);
  $paginaDeAcesso = end($paginaDeAcesso);

  if($menu[$permissao] != -1) {
    if ((backpack_user() && !backpack_user()->can($menu[$permissao])) || in_array($paginaDeAcesso,$menu[':permissoesCrud'])) {
      return "";
    }
  }

    // Verificação para permissaoTres
    if (isset($menu[':permissaoTres'])) {
        if (!backpack_user()->can($menu[':permissaoTres'])) {
            return "";
        }
    }

  // exit;


  if(isset($menu[$iconeMenu])) {
    $estruturaMenu = str_replace($iconeMenu,$menu[$iconeMenu],$estruturaMenu);
  }

  if(isset($menu[$textoMenu])) {
    $estruturaMenu = str_replace($textoMenu,$menu[$textoMenu],$estruturaMenu);
  }

  if(isset($menu[$rota])) {
    $estruturaMenu = str_replace($rota,backpack_url($menu[$rota]),$estruturaMenu);
  }

  if(isset($menu[$target])) {
    $menu[$target] = "target='{$menu[$target]}'";
    $estruturaMenu = str_replace($target,$menu[$target],$estruturaMenu);
  }

  $itemMenu = "itemMenuNivel{$menu[':nivelMenuTexto']}";
  $itemMenuNiveis = '';
    $ambiente = config('app.app_amb');
    if (isset($menu[':bloquearAmbiente'])) {
        if (in_array($ambiente, $menu[':bloquearAmbiente'])) {
            return '';
        }
    }

    if(isset($menu[$itemMenu])) {

      $menuAnterior = $menu[':nivelMenu'] - 1;
      $nivelAnteriorMenuTexto = Str::ucfirst(converterTextoNumeroMenu($menuAnterior));

      $iconeMenuAnterior = config("menu.pattern.structure.iconeMenu").$nivelAnteriorMenuTexto;
      $textoMenuAnterior = config("menu.pattern.structure.textoMenu").$nivelAnteriorMenuTexto;
      $rotaAnterior = config("menu.pattern.structure.rota").$nivelAnteriorMenuTexto;
      $permissaoAnterior = config("menu.pattern.structure.permissao").$nivelAnteriorMenuTexto;
      // $targetAnterior = config("menu.pattern.structure.target").$nivelAnteriorMenuTexto;

      if(isset($menu[$iconeMenuAnterior])) {
        $estruturaMenu = str_replace($iconeMenuAnterior,$menu[$iconeMenuAnterior],$estruturaMenu);
      }

      if(isset($menu[$textoMenuAnterior])) {
        $estruturaMenu = str_replace($textoMenuAnterior,$menu[$textoMenuAnterior],$estruturaMenu);
      }

      if(isset($menu[$rotaAnterior])) {
        $estruturaMenu = str_replace($rotaAnterior,$menu[$rotaAnterior],$estruturaMenu);
      }
      // print_r($menu);
      // if(isset($menu[$targetAnterior])) {
      //   $menu[$targetAnterior] = "target='{$menu[$targetAnterior]}'";
      //   $estruturaMenu = str_replace(":targetDois",$menu[$targetAnterior],$estruturaMenu);
      // }

      $estruturaMenuNivelDois = $menu[$itemMenu];
      $itemMenu = "itemMenu{$menu[':nivelMenuTexto']}";
      $itemTres = '';

      foreach($estruturaMenuNivelDois as $key => $nivelDois) {
        if ($nivelDois[':nivelMenu'] == 2 ) {
            if (isset($nivelDois[':bloquearAmbiente'])){
                if (in_array($ambiente, $nivelDois[':bloquearAmbiente'])) {
                    continue;
                }
            }
          // echo $target.'arua';
          $estruturaItemMenu = config("menu.pattern.structure.{$itemMenu}");
          $estruturaItemMenu = str_replace($iconeMenu,$nivelDois[$iconeMenu],$estruturaItemMenu);
          $estruturaItemMenu = str_replace($textoMenu,$nivelDois[$textoMenu],$estruturaItemMenu);
          $estruturaItemMenu = str_replace($rota,backpack_url($nivelDois[$rota]),$estruturaItemMenu);

          if(isset($nivelDois[$target])) {
            $targetNivelDois = "target='{$nivelDois[$target]}'";
            $estruturaItemMenu = str_replace($target,$targetNivelDois,$estruturaItemMenu);
          }

          $paginaDeAcesso = explode("/",$nivelDois[$rota]);
          $paginaDeAcesso = end($paginaDeAcesso);

          if($nivelDois[$permissao] != -1) {

            if (backpack_user()->can($nivelDois[$permissao]) && !in_array($paginaDeAcesso,$menu[':permissoesCrud'])
                || backpack_user()->can('V2_acessar_desenvolvedor_log')) {
              $itemMenuNiveis .= $estruturaItemMenu;
            }
          } else {
              if(!in_array($paginaDeAcesso,$menu[':permissoesCrud'])) {
                $itemMenuNiveis .= $estruturaItemMenu;
              }
          }
        }

        if ($nivelDois[':nivelMenu'] == 3 ) {
          $nivelTres = Str::ucfirst(converterTextoNumeroMenu($nivelDois[':nivelMenu']));
          $itemMenuNivelTres = "itemMenu{$nivelTres}";
          $estruturaItemMenuTres = config("menu.pattern.structure.{$itemMenuNivelTres}");
          $iconeMenuTres = config("menu.pattern.structure.iconeMenu").$nivelTres;
          $textoMenuTres = config("menu.pattern.structure.textoMenu").$nivelTres;
          $rotaTres = config("menu.pattern.structure.rota").$nivelTres;

          $estruturaItemMenuTres = str_replace(':textoMenuTres',$nivelDois[$textoMenuTres],$estruturaItemMenuTres);

          $estruturaItemMenuTres = str_replace(':iconeMenuTres',$nivelDois[$iconeMenuTres],$estruturaItemMenuTres);
          $itemMenuNiveis .=$estruturaItemMenuTres;

          foreach ($nivelDois['itemMenuNivelTres'] as $nivelTres) {
            $itemTres .=  '<li><a class="menu-item" href="'.backpack_url($nivelTres[$rotaTres]).'"><span class="content">'.$nivelTres[$textoMenuTres].'</span></a></li>';
          }
        }


      }

      $itemMenuNiveis = str_replace(':itemMenuTres',$itemTres,$itemMenuNiveis);

      $estruturaMenu = str_replace($itemMenuDois,$itemMenuNiveis,$estruturaMenu);
    }

    return $estruturaMenu;

}