<!-- <div class="modal fade" id="modalTrocaUASG" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Alterar UASG</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
            <div class="modal-body">
                <label>Informar o número da UASG</label> <br>
                <input type="text" id="novauasgusuario">
            </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Retornar</button>
          <button type="button" class="btn btn-primary" id="btnAlterarUasg">Alterar Uasg</button>
        </div>
        
      </div>
    </div>
  </div> -->

  <div class="br-scrim-util foco" id="scrimutilexample" data-scrim="true">
  <div class="br-modal">
    <div class="br-modal-header">Alterar Unidade</div>
    <div class="br-modal-body">
      <p>Informar o número da Unidade para alteração</p>
      <div class="br-input">
        <input type="text" id="novauasgusuario" data-mask ='000000'>
      </div>
      
    </div>
    <div class="br-modal-footer justify-content-center">
      <button class="br-button secondary" type="button" id="scrimfechar" data-dismiss="scrimexample">Cancelar
      </button>
      <button class="br-button primary mt-3 mt-sm-0 ml-sm-3" type="button" id="btnAlterarUasg">Alterar
      </button>
    </div>
  </div>
</div>