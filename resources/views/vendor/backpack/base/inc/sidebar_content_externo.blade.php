<?php
$menus = config('menu.menu_externo');

$rotaInicio = session()->get('tipo_acesso') == 'Administrador' ?
    'fornecedor/inicio/administrador' : 'fornecedor/inicio/preposto';

$menus['iniciofornecedor'][':rotaUm'] = $rotaInicio;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

function converterTextoNumeroMenu(?int $numeroMenu) {
  switch ($numeroMenu) {
    case 1:
      return 'um';
    break;

    case 2:
      return 'dois';
    break;

    case 3:
      return 'tres';
    break;

  }
}

foreach($menus as $modulo => $menu) {

  $nivelMenuTexto = converterTextoNumeroMenu($menu[':nivelMenu']);
  $menu[':nivelMenuTexto']  = $nivelMenuTexto;
  $estruturaMenu = config("menu.pattern.structure.{$menu[':nivelMenuTexto']}");
  $menu[':permissoesCrud'] = empty(config("security.permission_block_crud.{$modulo}"))? [] : config("security.permission_block_crud.{$modulo}");

  if(!empty($estruturaMenu)) {
    $estruturaMenu = substituirCampos($estruturaMenu, $menu);
  }

  echo $estruturaMenu;
}

function substituirCampos(string $estruturaMenu, array $menu) {
  $menu[':nivelMenuTexto'] = Str::ucfirst($menu[':nivelMenuTexto']);

  $iconeMenu = config("menu.pattern.structure.iconeMenu").$menu[':nivelMenuTexto'];
  $textoMenu = config("menu.pattern.structure.textoMenu").$menu[':nivelMenuTexto'];
  $rota = config("menu.pattern.structure.rota").$menu[':nivelMenuTexto'];
  $permissao = config("menu.pattern.structure.permissao").$menu[':nivelMenuTexto'];
  $target = config("menu.pattern.structure.target").$menu[':nivelMenuTexto'];
  $itemMenuDois = config("menu.pattern.structure.itemMenu");

  $paginaDeAcesso = explode("/",$menu[$rota]);
  $paginaDeAcesso = end($paginaDeAcesso);

  if($menu[$permissao] != -1) {
    if ((backpack_user() && !backpack_user()->can($menu[$permissao])) || in_array($paginaDeAcesso,$menu[':permissoesCrud'])) {
      return "";
    }
  }

  // exit;


  if(isset($menu[$iconeMenu])) {
    $estruturaMenu = str_replace($iconeMenu,$menu[$iconeMenu],$estruturaMenu);
  }

  if(isset($menu[$textoMenu])) {
    $estruturaMenu = str_replace($textoMenu,$menu[$textoMenu],$estruturaMenu);
  }

  if(isset($menu[$rota])) {
    $estruturaMenu = str_replace($rota,backpack_url($menu[$rota]),$estruturaMenu);
  }
  
  if(isset($menu[$target])) {
    $menu[$target] = "target='{$menu[$target]}'";
    $estruturaMenu = str_replace($target,$menu[$target],$estruturaMenu);
  }
  
  $itemMenu = "itemMenuNivel{$menu[':nivelMenuTexto']}";
  $itemMenuNiveis = '';

    if(isset($menu[$itemMenu])) {

      $menuAnterior = $menu[':nivelMenu'] - 1;
      $nivelAnteriorMenuTexto = Str::ucfirst(converterTextoNumeroMenu($menuAnterior));

      $iconeMenuAnterior = config("menu.pattern.structure.iconeMenu").$nivelAnteriorMenuTexto;
      $textoMenuAnterior = config("menu.pattern.structure.textoMenu").$nivelAnteriorMenuTexto;
      $rotaAnterior = config("menu.pattern.structure.rota").$nivelAnteriorMenuTexto;
      $permissaoAnterior = config("menu.pattern.structure.permissao").$nivelAnteriorMenuTexto;
      // $targetAnterior = config("menu.pattern.structure.target").$nivelAnteriorMenuTexto;

      if(isset($menu[$iconeMenuAnterior])) {
        $estruturaMenu = str_replace($iconeMenuAnterior,$menu[$iconeMenuAnterior],$estruturaMenu);
      }

      if(isset($menu[$textoMenuAnterior])) {
        $estruturaMenu = str_replace($textoMenuAnterior,$menu[$textoMenuAnterior],$estruturaMenu);
      }

      if(isset($menu[$rotaAnterior])) {
        $estruturaMenu = str_replace($rotaAnterior,$menu[$rotaAnterior],$estruturaMenu);
      }
      // print_r($menu);
      // if(isset($menu[$targetAnterior])) {
      //   $menu[$targetAnterior] = "target='{$menu[$targetAnterior]}'";
      //   $estruturaMenu = str_replace(":targetDois",$menu[$targetAnterior],$estruturaMenu);
      // }

      $estruturaMenuNivelDois = $menu[$itemMenu];
      $itemMenu = "itemMenu{$menu[':nivelMenuTexto']}";
      $itemTres = '';
      $ambiente = config('app.app_amb');

      foreach($estruturaMenuNivelDois as $key => $nivelDois) {
        if ($nivelDois[':nivelMenu'] == 2 ) {
            if (isset($nivelDois[':bloquearAmbiente'])){
                if (in_array($ambiente, $nivelDois[':bloquearAmbiente'])) {
                    continue;
                }
            }
          // echo $target.'arua';
          $estruturaItemMenu = config("menu.pattern.structure.{$itemMenu}");
          $estruturaItemMenu = str_replace($iconeMenu,$nivelDois[$iconeMenu],$estruturaItemMenu);
          $estruturaItemMenu = str_replace($textoMenu,$nivelDois[$textoMenu],$estruturaItemMenu);
          $estruturaItemMenu = str_replace($rota,backpack_url($nivelDois[$rota]),$estruturaItemMenu);

          if(isset($nivelDois[$target])) {
            $targetNivelDois = "target='{$nivelDois[$target]}'";
            $estruturaItemMenu = str_replace($target,$targetNivelDois,$estruturaItemMenu);
          }

          $paginaDeAcesso = explode("/",$nivelDois[$rota]);
          $paginaDeAcesso = end($paginaDeAcesso);

          if($nivelDois[$permissao] != -1) {

            if (backpack_user()->can($nivelDois[$permissao]) && !in_array($paginaDeAcesso,$menu[':permissoesCrud'])) {
              $itemMenuNiveis .= $estruturaItemMenu;
            }
          } else {
              if(!in_array($paginaDeAcesso,$menu[':permissoesCrud'])) {
                $itemMenuNiveis .= $estruturaItemMenu;
              }
          }
        }

        if ($nivelDois[':nivelMenu'] == 3 ) {
          $nivelTres = Str::ucfirst(converterTextoNumeroMenu($nivelDois[':nivelMenu']));
          $itemMenuNivelTres = "itemMenu{$nivelTres}";
          $estruturaItemMenuTres = config("menu.pattern.structure.{$itemMenuNivelTres}");
          $iconeMenuTres = config("menu.pattern.structure.iconeMenu").$nivelTres;
          $textoMenuTres = config("menu.pattern.structure.textoMenu").$nivelTres;
          $rotaTres = config("menu.pattern.structure.rota").$nivelTres;

          $estruturaItemMenuTres = str_replace(':textoMenuTres',$nivelDois[$textoMenuTres],$estruturaItemMenuTres);

          $estruturaItemMenuTres = str_replace(':iconeMenuTres',$nivelDois[$iconeMenuTres],$estruturaItemMenuTres);
          $itemMenuNiveis .=$estruturaItemMenuTres;

          foreach ($nivelDois['itemMenuNivelTres'] as $nivelTres) {
            $itemTres .=  '<li><a class="menu-item" href="'.backpack_url($nivelTres[$rotaTres]).'"><span class="content">'.$nivelTres[$textoMenuTres].'</span></a></li>';
          }
        }


      }

      $itemMenuNiveis = str_replace(':itemMenuTres',$itemTres,$itemMenuNiveis);

      $estruturaMenu = str_replace($itemMenuDois,$itemMenuNiveis,$estruturaMenu);
    }

    return $estruturaMenu;

}