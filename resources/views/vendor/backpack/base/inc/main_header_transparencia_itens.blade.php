@php
  $titulo = null;
  $subTitulo = null;
  if(isset($crud)) {
    $titulo = $crud->getHeading() ?? $crud->entity_name_plural;
    $subTitulo = $crud->getSubheading().' '.$crud->entity_name ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name;
  }
  $uasgUsuario = session('user_ug');
@endphp


<header class="br-header mb-4" id="header" data-sticky="data-sticky">
  <div class="container-lg">
    <div class="header-top">
      <div class="header-logo">

        <div class="header-menu-trigger" id="header-navigation">
          <!-- <button class="br-button small circle" type="button" aria-label="Menu" data-toggle="menu" data-target="#main-navigation" id="navigation"><i class='nav-icon la la-bars'></i>
          </button> -->
        </div>

        <img
                style="-webkit-transform: scale(1.0);-moz-transform: scale(1.0);-o-transform: scale(1.0);
                -ms-transform: scale(1.0);
                transform: scale(1.0);"
                class="m-0" src="{{ config('backpack.base.project_logo') }}"
                alt="{!! env('APP_NAME') !!}"
        />
        <span class="br-divider vertical mx-2"></span>

        <div class="header-sign">
          <div class="header-title">

          </div>

        </div>

      </div>
      <div class="header-actions">

        <span class="br-divider vertical mx-half mx-sm-1"></span>

        <div class="header-login">
          <div class="header-sign-in">

          </div>
          <div class="header-avatar"></div>

         {{--
          <div class="header-info">
            <div class="scrimutilexemplo" style="display: contents">
              <button class="br-button circle" type="button"><i class="fas fa-exchange-alt"></i> </button>
              <div class="br-tooltip" role="tooltip" info="info" place="top"><span class="subtext">Alterar Unidade do usuário </span></div>
            </div>
            <button class="br-button circle" type="button" onclick="window.location.href='{{ route('backpack.account.info') }}'"><i class="fas fa-key"></i> </button>
            <div class="br-tooltip" role="tooltip" info="info" place="top"><span class="subtext">Alterar senha</span></div>
            <button class="br-button circle" type="button" onclick="window.location.href='{{ backpack_url('logout') }}'"><i class="fas fa-sign-out-alt"></i> </button>
            <div class="br-tooltip" role="tooltip" info="info" place="top"><span class="subtext">Sair do Sistema</span></div>
          </div>
          --}}


        </div>

      </div>
    </div>

    <div class="header-bottom">

      <div class="header-menu">
        <div class="header-menu-trigger" id="header-navigation">
          <button class="br-button small circle" type="button" aria-label="Menu" data-toggle="menu" data-target="#main-navigation" id="navigation"><i class="fas fa-bars" aria-hidden="true"></i>
          </button>
        </div>
        <div class="header-info">
          <div class="header-title">Consultar Atas de Registro de Preços Por Itens</div>
         {{-- <div class="header-subtitle">{!! $subTitulo !!}</div> --}}
        </div>
      </div>
    </div>

  </div>
</header>