@if (config('backpack.base.scripts') && count(config('backpack.base.scripts')))
    @foreach (config('backpack.base.scripts') as $path)
        <script type="text/javascript" src="{{ asset($path).'?v='.config('backpack.base.cachebusting_string') }}"></script>
    @endforeach
@endif

@if (config('backpack.base.mix_scripts') && count(config('backpack.base.mix_scripts')))
    @foreach (config('backpack.base.mix_scripts') as $path => $manifest)
        <script type="text/javascript" src="{{ mix($path, $manifest) }}"></script>
    @endforeach
@endif

@include('backpack::inc.alerts')
@include('backpack::inc.modal_uasg')
@include('backpack::inc.loading')

<input type="hidden" id="message_erro_gov_br" value="{{ url('message_gov_br/error.html') }}"/>
<input type="hidden" id="message_warning_gov_br" value="{{ url('message_gov_br/warning.html') }}"/>
<input type="hidden" id="message_success_gov_br" value="{{ url('message_gov_br/success.html') }}"/>
<input type="hidden" id="message_info_gov_br" value="{{ url('message_gov_br/info.html') }}"/>

<!-- page script -->
<script type="text/javascript">
    $('#novauasgusuario').mask('999999')

    // To make Pace works on Ajax calls
    $(document).ajaxStart(function() { Pace.restart(); });

    // polyfill for `startsWith` from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/startsWith
    if (!String.prototype.startsWith) {
        Object.defineProperty(String.prototype, 'startsWith', {
            value: function(search, rawPos) {
                var pos = rawPos > 0 ? rawPos|0 : 0;
                return this.substring(pos, pos + search.length) === search;
            }
        });
    }



    // polyfill for entries and keys from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/entries#polyfill
    if (!Object.keys) Object.keys = function(o) {
        if (o !== Object(o))
            throw new TypeError('Object.keys called on a non-object');
        var k=[],p;
        for (p in o) if (Object.prototype.hasOwnProperty.call(o,p)) k.push(p);
        return k;
    }

    if (!Object.entries) {
        Object.entries = function( obj ){
            var ownProps = Object.keys( obj ),
                i = ownProps.length,
                resArray = new Array(i); // preallocate the Array
            while (i--)
                resArray[i] = [ownProps[i], obj[ownProps[i]]];
            return resArray;
        };
    }

    // Ajax calls should always have the CSRF token attached to them, otherwise they won't work
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    {{-- Enable deep link to tab --}}
    var activeTab = $('[href="' + location.hash.replace("#", "#tab_") + '"]');
    location.hash && activeTab && activeTab.tab('show');
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        location.hash = e.target.hash.replace("#tab_", "#");
    });


    function maiuscula(z){
        v = z.value.toUpperCase();
        z.value = v;
    }

    function minusculo(z){
        v = z.value.toLowerCase();
        z.value = v;
    }

    function maskCNPJ(element) {
        $(element).mask("99999999999999");
    }

    function maskCPF(element) {
        $(element).mask("999.999.999-99");
    }

    function maskTelefone(element) {
        $(element).mask("(99) 9999-9999");
    }

    function maskCelular(element) {
        $(element).mask("(99) 99999-9999");
    }

    function maskUG(element) {
        $(element).mask("999999");
    }

    function maskIDGener(element) {
        $(element).mask("EX9999999");
    }

    function maskEmpenho(element) {
        $(element).mask("9999NE999999");
    }

    function maskContrato(element) {
        $(element).mask("99999/9999");
    }

// Melhoria no modal de loading
    $("#btnAlterarUasg").click(function(){
        let novauasgusuario = $("#novauasgusuario").val()
        $.blockUI({ message: $('#loadingContratos') });
        $.post( '{{ url('alteraruasgusuario') }}', {novauasgusuario : novauasgusuario}).done(function( response ) {

            let data = JSON.parse(JSON.stringify(response));
            if(data.code === 200) {
                exibirAlertaNoty('success-custom', data.mensagem);
                setTimeout(function(){
                    window.location.href='/'
                },  700);
                $('#scrimutilexample').hide(); // oculta o modal scrimutilexample
            }

            if(data.code === 203 || data.code === 204) {
                $('#scrimutilexample .br-modal-header').html(data.mensagem);
            }
            $.unblockUI();
        });
    })

 {{--
        $("#btnAlterarUasg").click(function(){
            let novauasgusuario = $("#novauasgusuario").val()

            $.post( '{{ url('alteraruasgusuario') }}', {novauasgusuario : novauasgusuario}).done(function( response ) {

            let data = JSON.parse(JSON.stringify(response));
            if(data.code === 200) {
                $('#scrimutilexample .br-modal-body').html(
                    '<div class="loading medium"></div>'
                );
                $('#scrimutilexample .br-modal-header').html('Carregando...');
                $('#scrimutilexample .br-modal-footer').html('');

                setTimeout(function(){
                    window.location.reload()
                },  700);
            }

            if(data.code === 203 || data.code === 204) {
                $('#scrimutilexample .br-modal-header').html(data.mensagem);
            }

        });
    })
--}}

    function montarHtml(options) {
        html = '<div class="my-custom-template noty_body">';

        let url = options.url
        let pagina = recuperarHtml(url)

        replaceTitle = pagina.replace(":title", options.title)
        replaceMessage = replaceTitle.replace(":body", options.text)

        html +=replaceMessage
        html += '<div>';

        return html
    }

    function recuperarHtml(url) {
        let pagina = null
        $.ajax({
            url: url,
            async: false,
            success: function (html) {
                pagina = html;
            }
        });
        return pagina
    }

    function opcoesNoty() {
        Noty.overrideDefaults({
            callbacks: {
            onTemplate: function() {
                if (this.options.type === 'warning-custom') {
                    this.barDom.innerHTML = montarHtml(this.options)
                }

                if (this.options.type === 'error-custom') {
                    this.barDom.innerHTML = montarHtml(this.options)
                }

                if (this.options.type === 'success-custom') {
                    this.barDom.innerHTML = montarHtml(this.options)
                }

                if (this.options.type === 'info-custom') {
                    this.barDom.innerHTML = montarHtml(this.options)
                }
            }
            }
        })
    }

    function exibirAlertaNoty(type, text, title = '', timeout = 5000) {
        opcoesNoty()

        let titleMessage = title
        let textMessage = text
        let urlMessage = null
        if( titleMessage == '') {
            switch (type) {
                case 'error-custom':
                    titleMessage = 'Erro ao executar a ação'
                    urlMessage = $("#message_erro_gov_br").val()
                break
                case 'warning-custom':
                    titleMessage = 'Atenção'
                    urlMessage = $("#message_warning_gov_br").val()
                break
                case 'success-custom':
                    titleMessage = 'Sucesso'
                    urlMessage = $("#message_success_gov_br").val()
                break
                case 'info-custom':
                    titleMessage = 'Informação'
                    urlMessage = $("#message_info_gov_br").val()
                break

            }
        }
        new Noty({
            layout:'top',
            title: titleMessage,
            text: textMessage,
            type: type,
            timeout: timeout,
            url: urlMessage,
            closeWith: ['click']

        }).show();
    }

    function exibirDescricaoCompleta(descricaoCompleta, descricaoResumida, idItem) {

        let idBotaoClicado = `botaoDescricaoCompletaCompra_${idItem}`
        let idTexto = `textoDescricaoCompletaCompra_${idItem}`
        let tipoBotao = $(`#${idBotaoClicado}`).data()

        let iconeBotao = 'fas fa-plus'
        let novoTipo = 'resumido'
        let novoConteudo = descricaoResumida

        if(tipoBotao.tipo == 'resumido') {
            iconeBotao = "fas fa-minus"
            novoTipo = 'detalhado'
            novoConteudo = descricaoCompleta
        }

        $( `#${idBotaoClicado}` ).html(`<i class="${ iconeBotao }"></i>`);
        $(`#${idBotaoClicado}`).data('tipo',novoTipo);
        $(`#${idTexto}`).html(novoConteudo)

    }

    function copyToClipboard(element) {
        let $temp = $("<input>");
        $("body").append($temp);
        $temp.val(element).select();
        document.execCommand("copy");
        $temp.remove();

        new Noty({ type: 'success', text: 'Texto copiado com sucesso!' }).show()
    }

    function loadingDados(classElement, message)
    {
        $('.' + classElement).css({'display' : 'flex', 'flex-direction' : 'column', 'justify-content' : 'center'});
        $('.' + classElement).html(
            '<div class="legend pb-5x" style="align-self: center" >' + message + ' </div>' +
            '<div class="loading medium"></div>'
        );
    }

    function formatarDataComparacao(data) {
        let arrayData = data.split('/')
        let dataFormatada = `${arrayData[2]}/${arrayData[1]}/${arrayData[0]}`
        return new Date(dataFormatada).getTime()
    }

</script>
