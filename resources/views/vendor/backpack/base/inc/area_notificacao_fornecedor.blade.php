@php
    $user = backpack_user();
    $notifacoes = $user->notifications()->whereNull('read_at');
    $quantidadeNotificacao = $notifacoes->count();

    if ($quantidadeNotificacao == 0) {
        $quantidadeNotificacao = '';
    }
@endphp
<style>
    .br-button {
        position: relative; /* Necessário para posicionamento absoluto dentro do botão */
    }

    .notification-tag {
        position: absolute;
        top: 12px; /* Ajuste conforme necessário */
        right: 12px; /* Ajuste conforme necessário */
        transform: translate(50%, -50%); /* Centraliza a tag no canto superior direito */

        height: 16px; /* Ajuste conforme necessário */
        width: 16px; /* Ajuste conforme necessário */
        padding: 0; /* Remove o padding */
        font-size: 10px; /* Ajuste conforme necessário */
        line-height: 16px; /* Centraliza o texto verticalmente */
        display: flex;
        align-items: center;
        justify-content: center;
    }

    /* Estilização do botão */
    .dropbtn {
        background-color: #4CAF50;
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;
        cursor: pointer;
    }

    /* Container do dropdown */
    .dropdown {
        position: relative;
        display: inline-block;
    }

    /* Conteúdo do dropdown (escondido por padrão) */
    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 250px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
        right: 30px;
    }

    /* Links dentro do dropdown */
    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    /* Mudar a cor do link ao passar o mouse */
    .dropdown-content a:hover {
        background-color: #ccc;
    }

    /* Classe que mostra o dropdown */
    .show {
        display: block;
    }
</style>
<div class="dropdown large">
    <button class="br-button circle mt-2" onclick="toggleDropdown()">
        <i class="far fa-bell" aria-hidden="true" style="font-size: 30px !important;"></i>
                @if (!empty($quantidadeNotificacao))
                    <div class="d-flex notification-tag">
                <span class="br-tag count bg-danger small">
                    <span aria-hidden="true" style="margin-bottom: 3px;">{{$quantidadeNotificacao}}</span>
                </span>
                    </div>
                @endif

    </button>
    @include("vendor.backpack.crud.fields.br-tooltip",['textTooltip' => 'Acessar as notificações'])
    <div id="myDropdown" class="dropdown-content">
        @foreach($notifacoes->take(3)->get() as $notification)
            @php
                $data = (object) $notification->data;
            @endphp
            <a class="br-item" href="/fornecedor/notificacao/{{$notification->id}}" role="menuitem">{{$data->data['texto_lista_suspesa']}}</a>
            <input type="hidden" id="situacaoNotificacaoAcessoRapido_{{$notification->id}}"
                   data-id="{{$notification->id}}">
        @endforeach
        <a class="br-item" href="/fornecedor/notificacao" role="menuitem">Todas as notificações</a>
    </div>
</div>

<script>
    function toggleDropdown() {
        document.getElementById("myDropdown").classList.toggle("show");
    }
</script>