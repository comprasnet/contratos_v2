{{-- @if (config('backpack.base.show_powered_by') || config('backpack.base.developer_link'))
    <div class="text-muted ml-auto mr-auto">
      @if (config('backpack.base.developer_link') && config('backpack.base.developer_name'))
      Copyright © {{ date('Y') }} <a target="_blank" rel="noopener" href="{{ config('backpack.base.developer_link') }}">{{ config('backpack.base.developer_name') }}</a> - {{ config('app.server_node') }} | v. {{  config('app.app_version') }}.
      @endif
      @if (config('backpack.base.show_powered_by'))
      {{ trans('backpack::base.powered_by') }} <a target="_blank" rel="noopener" href="http://backpackforlaravel.com?ref=panel_footer_link">Backpack for Laravel</a>.
      @endif
    </div>
@endif --}}


    <footer class=" br-footer pt-3" id="footer">
        <div class="container-lg">
        <div class="info">
            <div class="text-down-01 text-medium pb-3">
                &nbsp;Copyright &copy; {{ date('Y') }} 
                <strong><a href="#">{{ env('APP_NAME') }}</a> </strong>- Todos direitos reservados. Software Livre (GPL)
            </div>
        </div>
        </div>
    </footer>