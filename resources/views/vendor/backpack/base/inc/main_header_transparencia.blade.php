@php
    $titulo = null;
    $subTitulo = null;
    if(isset($crud)) {
      $titulo = $crud->getHeading() ?? $crud->entity_name_plural;
      $subTitulo = $crud->getSubheading().' '.$crud->entity_name ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name;
    }
    $uasgUsuario = session('user_ug');
@endphp


<div class="header-top">
    <div class="header-logo">

        <div class="header-menu-trigger" id="header-navigation">
            <!-- <button class="br-button small circle" type="button" aria-label="Menu" data-toggle="menu" data-target="#main-navigation" id="navigation"><i class='nav-icon la la-bars'></i>
            </button> -->
        </div>

        <img
                style="-webkit-transform: scale(1.0);-moz-transform: scale(1.0);-o-transform: scale(1.0);
                -ms-transform: scale(1.0);
                transform: scale(1.0);"
                class="m-0" src="{{ config('backpack.base.project_logo') }}"
                alt="{!! env('APP_NAME') !!}"
        />
        <span class="br-divider vertical mx-2"></span>

        <div class="header-sign">
            @if(backpack_user() && $uasgUsuario)
                <div class="header-title">
                    {{ backpack_user()->name }} - UASG: {{$uasgUsuario}}
                </div>
            @endif
        </div>

    </div>
    <div class="header-actions">

        <span class="br-divider vertical mx-half mx-sm-1"></span>

        <div class="header-login">
            <div class="header-sign-in">

            </div>
            <div class="header-avatar"></div>

            <div class="header-info">


                @if(backpack_user())
                    <div class="scrimutilexemplo" style="display: contents">
                        <button class="br-button circle" type="button"><i class="fas fa-exchange-alt"></i></button>
                        <div class="br-tooltip" role="tooltip" info="info" place="top"><span class="subtext">Alterar Unidade do usuário </span>
                        </div>
                    </div>

                    <button class="br-button circle" type="button"
                            onclick="window.location.href='{{ route('backpack.account.info') }}'"><i class="fas fa-key"></i>
                    </button>

                    <div class="br-tooltip" role="tooltip" info="info" place="top">
                        <span class="subtext">Alterar senha</span>
                    </div>

                    <button class="br-button circle" type="button" onclick="window.location.href='{{ backpack_url('logout') }}'">
                        <i class="fas fa-sign-out-alt"></i>
                    </button>
                @else
                    <button class="br-sign-in circle mt-3 mt-sm-0 ml-sm-3" type="button" onclick="window.location.href='{{ backpack_url('login') }}'">
                        <i class="fas fa-user" aria-hidden="true">

                        </i>
                    </button>
                @endif
            </div>


        </div>

    </div>
</div>

<div class="header-bottom">

    <div class="header-menu">
        <div class="header-menu-trigger" id="header-navigation">
            <button class="br-button small circle" type="button" aria-label="Menu" data-toggle="menu"
                    data-target="#main-navigation" id="navigation"><i class="fas fa-bars" aria-hidden="true"></i>
            </button>
        </div>
        <div class="header-info">
            <div class="header-title">{!! $titulo !!}</div>
            <div class="header-subtitle">{!! $subTitulo !!}</div>
        </div>
    </div>
</div>

