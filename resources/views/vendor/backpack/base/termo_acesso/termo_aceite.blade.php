<html lang="pt-BR">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Início :: Contratos.gov</title>
    <!-- Fonte Rawline-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/rawline.css" />
    <!-- Fonte Raleway-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900&amp;display=swap" />
    <!-- Design System de Governo-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/core.min.css" />
    <!-- Fontawesome-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/all.min.css" />

    <link rel="stylesheet" href="layout_login/css/style.css">

</head>

<body>
    <div class="d-lg-flex half">
        <div class="contents order-2 order-md-2">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-7">

                        <form class="col-md-12 " role="form" method="POST" action="{{ route('aceita.termo') }}">
                            <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response" data-sitekey="{{ env('RECAPTCHA_SITE_KEY') }}">
                            {!! csrf_field() !!}
                            <div align="center">
                                <img src="{{ config('backpack.base.project_logo') }}" width="300px" alt="{!! env('APP_NAME') !!}">
                            </div>

                            <div class="w-100 fade-in">
                                <p style="font-size: 20px; text-align: center;">Prezado(a) Usuário(a),</p>
                                <div style="font-size: 19px; text-align: justify">Informamos que, a partir de 1º de novembro de 2024, o acesso no Sistema Contratos.gov.br será realizado exclusivamente por meio de login com conta de nível prata ou ouro do Gov.br.
                                    Se ainda não possui uma conta no Gov.br, recomendamos que selecione a opção <b>"Entrar com Gov.br"</b> e siga as instruções disponíveis no site. Caso já tenha uma conta, sugerimos que comece a utilizá-la imediatamente para facilitar sua adaptação ao novo método de autenticação.
                                </div>
                            </div>
                            <hr class="m-t-30" />

                            <div class="text-center">
                                <br>
                                <button class="br-sign-in primary" type="submit">
                                    Ciente
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

<style>
    body {
        margin: 0;
        height: 100vh;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .fade-in {
        opacity: 0;
        animation: fadeIn 0.5s forwards;
    }

    @keyframes fadeIn {
        to {
            opacity: 1;
        }
    }
</style>

</html>