

<div class="br-menu" id="main-navigation">
    <div class="menu-container">
        <div class="menu-panel">
            <div class="menu-header">
                <div class="menu-title">
                    <img src="{{ config('backpack.base.project_somente_logo') }}" alt="{{ env('APP_NAME') }}"/><span>{{ env('APP_NAME') }}</span>
                </div>
                <div class="menu-close">
                    <button class="br-button circle" type="button" aria-label="Fechar o menu" data-dismiss="menu"><i class="las la-times"></i>
                    </button>
                </div>
            </div>

            <nav class="menu-body">
                @if(backpack_user())
                    <a class="menu-item divider" href="{{ route('transparencia') }}">
                        <span class="icon"> <i class="fas fa-home"></i> </span>
                        <span class="content">Início</span>
                    </a>
                @else
                    <a class="menu-item divider" href="/">
                        <span class="icon"> <i class="fas fa-home"></i> </span>
                        <span class="content">Início</span>
                    </a>
                @endif

                <a class="menu-item divider" href="{{ route('transparencia.arp') }}">
                    <span class="icon"> <i class="fas fa-file-invoice-dollar"></i> </span>
                    <span class="content">Consultar Atas</span>
                </a>

                <a class="menu-item divider" href="/transparencia/compras">
                    <span class="icon"> <i class="fas fa-file-invoice-dollar"></i> </span>
                    <span class="content">Consultar Compras</span>
                </a>

                <a class="menu-item divider" href="/transparencia/arp-item">
                    <span class="icon"> <img class="mb-2" src="/img/consultar-item.png" style="height: 17px; margin-left: 2px"> </span>
                    <span class="content">Consultar Atas por Item</span>
                </a>

                <a class="menu-item divider" href="https://contratos.comprasnet.gov.br/transparencia" target="_blank">
                    <span class="icon"> <img class="mb-2" src="/img/gestao_icon.png" style="height: 17px; margin-left: 2px"">  </span>
                    <span class="content">Gestão Contratual</span>
                </a>
            </nav>

            <div class="menu-footer">
                <div class="menu-links">
                    <a href="https://www.gov.br/compras/pt-br." class="menu-item-link-externo" target="_blank"><i class="fas fa-external-link-square-alt" aria-hidden="true"></i> <span class="mr-1">Portal de Compras</span></a>
                    <a href="https://www.gov.br/pncp/pt-br" class="menu-item-link-externo" target="_blank"><i class="fas fa-external-link-square-alt" aria-hidden="true"></i> <span class="mr-1">PNCP</span></a>
                    <a href="https://gitlab.com/comprasnet/contratos/-/wikis/uploads/27ed2940e223c6c3bdf753f0357d2965/Manual_Contratos.gov.br_-_Gest%C3%A3o_de_Atas_-_vers%C3%A3o_2.0.0.pdf" class="menu-item-link-externo" target="_blank"><i class="fas fa-external-link-square-alt" aria-hidden="true"></i> <span class="mr-1">Manual</span></a>
                    <a href="https://www.comprasnet.gov.br/seguro/loginPortal.asp" class="menu-item-link-externo" target="_blank"><i class="fas fa-external-link-square-alt" aria-hidden="true"></i> <span class="mr-1"> Área de Trabalho</span></a>
                </div>

                <div class="menu-info">
                    <div class="text-center text-down-01">
                        @include("backpack::inc.ambiente_utilizado")
                    </div>
                </div>
            </div>
        </div>

        <div class="menu-scrim" data-dismiss="menu" tabindex="0"></div>
    </div>
</div>


<!-- Left side column. contains the sidebar -->
{{-- <div class="{{ config('backpack.base.sidebar_class') }}"> --}}
<!-- sidebar: style can be found in sidebar.less -->
{{-- <nav class="sidebar-nav overflow-hidden"> --}}
<!-- sidebar menu: : style can be found in sidebar.less -->
{{-- <ul class="nav"> --}}
<!-- <li class="nav-title">{{ trans('backpack::base.administration') }}</li> -->
<!-- ================================================ -->
<!-- ==== Recommended place for admin menu items ==== -->
<!-- ================================================ -->

{{-- @include(backpack_view('inc.sidebar_content')) --}}

<!-- ======================================= -->
<!-- <li class="divider"></li> -->
<!-- <li class="nav-title">Entries</li> -->
{{-- </ul> --}}
{{-- </nav> --}}
<!-- /.sidebar -->
{{-- </div> --}}


@push('before_scripts')
    <script type="text/javascript">
        // Save default sidebar class
        let sidebarClass = (document.body.className.match(/sidebar-(sm|md|lg|xl)-show/) || ['sidebar-lg-show'])[0];
        let sidebarTransition = function(value) {
            document.querySelector('.app-body > .sidebar').style.transition = value || '';
        };

        // Recover sidebar state
        let sessionState = sessionStorage.getItem('sidebar-collapsed');
        if (sessionState) {

            // disable the transition animation temporarily, so that if you're browsing across
            // pages with the sidebar closed, the sidebar does not flicker into the view
            sidebarTransition("none");
            document.body.classList.toggle(sidebarClass, sessionState === '1');

            // re-enable the transition, so that if the user clicks the hamburger menu, it does have a nice transition
            setTimeout(sidebarTransition, 100);
        }
    </script>
@endpush

@push('after_scripts')
    <script>
        // Store sidebar state
        document.querySelectorAll('.sidebar-toggler').forEach(function(toggler) {
            toggler.addEventListener('click', function() {
                sessionStorage.setItem('sidebar-collapsed', Number(!document.body.classList.contains(sidebarClass)))
                // wait for the sidebar animation to end (250ms) and then update the table headers because datatables uses a cached version
                // and dont update this values if there are dom changes after the table is draw. The sidebar toggling makes
                // the table change width, so the headers need to be adjusted accordingly.
                setTimeout(function() {
                    if(typeof crud !== "undefined" && crud.table) {
                        crud.table.fixedHeader.adjust();
                    }
                }, 300);
            })
        });
        // Set active state on menu element
        var full_url = "{{ Request::fullUrl() }}";
        var $navLinks = $(".sidebar-nav li a, .app-header li a");

        // First look for an exact match including the search string
        var $curentPageLink = $navLinks.filter(
            function() { return $(this).attr('href') === full_url; }
        );

        // If not found, look for the link that starts with the url
        if(!$curentPageLink.length > 0){
            $curentPageLink = $navLinks.filter( function() {
                if ($(this).attr('href').startsWith(full_url)) {
                    return true;
                }

                if (full_url.startsWith($(this).attr('href'))) {
                    return true;
                }

                return false;
            });
        }

        // for the found links that can be considered current, make sure
        // - the parent item is open
        $curentPageLink.parents('li').addClass('open');
        // - the actual element is active
        $curentPageLink.each(function() {
            $(this).addClass('active');
        });
    </script>
@endpush
