@extends(backpack_view('base.blank'))

@php
    $widgets['after_content'][] = [
          'type' => 'div',
          'class' => 'row',
          'content' => [ // widgets
               [
                   'type'     => 'view',
                   'view'     => 'vendor.backpack.base.widgets.acesso_rapido_transparencia',
               ],
               [
                   'type'     => 'view',
                   'view'     => 'vendor.backpack.base.widgets.totais_arps',
               ],
               [
                   'type'     => 'view',
                   'view'     => 'vendor.backpack.base.widgets.lista_arps',
               ],
               [
                'type' => 'chart',
                'wrapperClass' => 'w-50',
                'class' => 'border rounded ml-2',
                'controller' => App\Http\Controllers\Admin\Charts\ArpTotalChartController::class,
                'content' => [
                    'header' => 'Atas de registro de preços por tipo de item (%)',
                ]
            ],
          ]
    ];
@endphp

@section('content')
    @include(backpack_view('inc.widgets'), [ 'widgets' => app('widgets')->where('group', 'content')->toArray() ])
@endsection
