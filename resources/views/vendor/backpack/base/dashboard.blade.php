@php
$content = [ // widgets
    [
        'type'     => 'view',
        'view'     => 'vendor.backpack.base.widgets.acesso_rapido',
    ],
];

if(auth()->user()->can('gestaodeatas_V2_acessar')) {
    $content[] = [
        'type'     => 'view',
        'view'     => 'vendor.backpack.base.widgets.totais_arps',
    ];
    $content[] = [
        'type'     => 'view',
        'view'     => 'vendor.backpack.base.widgets.lista_arps',
        'unidade' => $unidade,
    ];

    $content[] = [
        'type' => 'chart',
        'wrapperClass' => 'w-50',
        'class' => 'border rounded ml-2',
        'controller' => \App\Http\Controllers\Admin\Charts\ArpTotalChartController::class,
        'content' => [
            'header' => 'Atas de registro de preços por tipo de item (%)',
        ]
    ];
}

@endphp

@extends(backpack_view('blank'))

@php
    //$widgets['before_content'][] = [
        // 'type'        => 'jumbotron',
        // 'heading'     => trans('backpack::base.welcome'),
        // 'content'     => trans('backpack::base.use_sidebar'),
        // 'button_link' => backpack_url('logout'),
        // 'button_text' => trans('backpack::base.logout'),
    //];

    $widgets['after_content'][] = [
          'type' => 'div',
          'class' => 'row',
          'content' => $content
    ];
@endphp

@section('content')
{{--    <img src="{{ config('backpack.base.project_logo') }}" alt="{!! env('APP_NAME') !!}"  width="1500px"/>--}}
    @include(backpack_view('inc.widgets'), [ 'widgets' => app('widgets')->where('group', 'content')->toArray() ])
@endsection