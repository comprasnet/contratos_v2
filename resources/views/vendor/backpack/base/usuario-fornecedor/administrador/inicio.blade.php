@extends(backpack_view('layouts.plain'))

@section('after_styles')
    <style>
        body {
            margin: 0; /* Remove margens padrão do corpo */
            padding: 0; /* Remove padding padrão do corpo */
        }

        header {
            background-image: url("{{ asset('img/logo.png') }}");
            background-repeat: no-repeat;
            background-position: center;
            background-size: 50%; /* Reduz o tamanho do logotipo em 50% */
            height: 200px; /* Altura do cabeçalho, ajuste conforme necessário */
            text-align: center;
        }

        .content-container {
            text-align: center;
            margin-top: 20px; /* Ajuste conforme necessário */
        }

        h1 {
            color: black; /* Cor do texto, ajuste conforme necessário */
        }
    </style>
@endsection

@section('content')
    <header></header>
    <div class="content-container">
        <h1>
            Seja bem-vindo!
        </h1>
    </div>
@endsection
