<html lang="pt-BR">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Compras.gov.br - Contratos</title>
    <!-- Fonte Rawline-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/rawline.css"/>
    <!-- Fonte Raleway-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900&amp;display=swap"/>
    <!-- Design System de Governo-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/core.min.css"/>
    <!-- Fontawesome-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/all.min.css"/>

    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet"> -->

    <!-- <link rel="stylesheet" href="layout_login/fonts/icomoon/style.css"> -->

    <!-- <link rel="stylesheet" href="layout_login/css/owl.carousel.min.css"> -->

    <!-- Style -->
    <link rel="stylesheet" href="../../layout_login/css/style.css">
    <link rel="icon" type="image/x-icon" href="{{ config('backpack.base.project_somente_logo_ico') }}">
</head>
<body>

<div class="d-lg-flex half">
    <div class="bg order-1 order-md-1" style="background-image: url('{{ config('backpack.base.project_logo_lateral') }}');"></div>
    <div class="contents order-2 order-md-2">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-md-7">
                    <fieldset>
                        <form name="cadastrar-usuario-fornecedor" id="cadastrar-usuario-fornecedor" class="col-md-12 " role="form" method="POST" action="{{ route('usuario.fornecedor.create') }}">
                            {!! csrf_field() !!}
                          {{--  <input type="hidden" name="tipoUsuario" value="{{ $tipoUsuario }}">
                            <input type="hidden" name="identificadorUsuario" value="{{ $identificadorUsuario }}">
                            <input type="hidden" name="nome" value="{{ $nome }}">
                            <input type="hidden" name="login" value="{{ $login }}">
                            <input type="hidden" name="tipoPessoaFornecedor" value="{{ $tipoPessoaFornecedor }}">
                            <input type="hidden" name="administrador" value="{{ $administrador }}">
                            <input type="hidden" name="identificadorFornecedor" value="{{ $identificadorFornecedor }}">
                            <input type="hidden" name="nomeRazaoSocialFornecedor" value="{{ $nomeRazaoSocialFornecedor }}">--}}
                            <div align="center">
                                <img src="{{ config('backpack.base.project_logo') }}" width="300px" alt="{!! env('APP_NAME') !!}">
                            </div>
                            {{--<div class=" mb-5 text-center">
                                <span class="icon-external-link"></span>
                                <div class="scrimutilexemplo" style="display: contents">
                                    <{!! config('app.app_amb') !!}
                                    <div class="br-tooltip" role="tooltip" info="info" place="right"><span class="subtext">{{ config('app.server_node') }} | v: {{  config('app.app_version') }}</span></div>
                                </div>
                            </div>--}}

                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <div class="br-input">
                                        <label for="identificador_fornecedor">Identificador Fornecedor</label>
                                        <input type="text" placeholder="Digite o CPF/CNPJ/IDGENERICO do Fornecedor"  name="identificador_fornecedor" id="identificador_fornecedor">
                                            <span class="invalid-feedback" style="color: red">
                                    </div>
                                </div>
                            </div>

                            <div class="text-center">
                                <br>
                                <button class="br-sign-in primary " type="submit">
                                    <i class="fas fa-user" aria-hidden="true"></i>Cadastrar
                                </button>
                            </div>

                        </form>


                    </fieldset>


                </div>
            </div>
        </div>
    </div>


</div>



</body>

<script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/core-init.js"></script>
<script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/jquery-3.6.1.min.js"></script>
<script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/jquery.mask.js"></script>


<script type="text/javascript">
    $( document ).ready(function($) {
        const phoneOptions = {
            "onKeyPress": (val, e, field, options) =>
                field.mask(
                    (val) => val.replace(/\D/g, '').length === 11 ? "(00) 00000-0000" : "(00) 0000-00009",
                    options)
        };
        $('#{{ $telefone }}').mask("(00) 00000-0000", phoneOptions);
    });
</script>

</html>