@php
    $content = [ // widgets
        [
            'type'     => 'view',
            'view'     => 'vendor.backpack.base.widgets.usuario_fornecedor.acesso_rapido_fornecedor',
        ],
    ];
        if(
        auth()->user()->can('acessar_dashboard_usuario_fornecedor')
        ||
        auth()->user()->can('perfilfornecedor_consultar')
        ) {
            // Vencimentos
          $content[] = [
            'type'     => 'view',
            'view'     => 'vendor.backpack.base.widgets.usuario_fornecedor.progress_fornecedor',
            'controller' => \App\Http\Controllers\Admin\Charts\ContratoFornecedorChartProgressController::class,
            'titulo_cabecalho' => 'Contratos',
            'name' => 'progress_fornecedor-contrato'
          ];

          $content[] = [
            'type'     => 'view',
            'view'     => 'vendor.backpack.base.widgets.usuario_fornecedor.progress_fornecedor',
            'controller' => \App\Http\Controllers\Admin\Charts\AtaFornecedorChartProgressController::class,
            'titulo_cabecalho' => 'Atas de registro de preços',
            'name' => 'progress_fornecedor-ata'
          ];

           // Gráfico de pizza
           $content[] = [
            'type' => 'chart',
            'wrapperClass' => 'w-50 mb-3',
            'class' => 'border rounded ml-2',
            'controller' => \App\Http\Controllers\Admin\Charts\ContratoFornecedorTotalChartController::class,
            'content' => [
                'header' => 'Contrato(s) vigente(s) do fornecedor por unidade',
                ]
           ];

          $content[] = [
            'type' => 'chart',
            'wrapperClass' => 'w-50 mb-3',
            'class' => 'border rounded ml-2',
            'controller' => \App\Http\Controllers\Admin\Charts\ArpFornecedorTotalChartController::class,
            'content' => [
                'header' => 'Ata(s) de registro de preços vigente(s) do fornecedor por unidade',
                ]
          ];

          // Valor total
          $content[] = [
                'type'     => 'view',
                'view'     => 'vendor.backpack.base.widgets.usuario_fornecedor.valor_total_contratado_fornecedor',
                'controller' =>
                \App\Http\Controllers\Admin\Charts\ContratoFornecedorTotalContratadoChartController::class,
                'titulo_cabecalho' => 'Valor total contratado',
                'name' => 'total-contratado_contratos'
          ];

          $content[] = [
            'type'     => 'view',
            'view'     => 'vendor.backpack.base.widgets.usuario_fornecedor.valor_total_contratado_fornecedor',
            'controller' =>
            \App\Http\Controllers\Admin\Charts\ContratoFornecedorTotalContratadoAtasChartController::class,
            'titulo_cabecalho' => 'Valor total registrado em atas de registro de preços',
            'name' => 'total-contratado_contratos'
          ];

          // Listagens
          $content[] = [
            'type'     => 'view',
            'view'     => 'vendor.backpack.base.widgets.usuario_fornecedor.lista_contratos_usuario_fornecedor',
            'titulo_cabecalho' => 'Contrato(s) vigente(s) do fornecedor',
            'placeholder_search' => 'Digite o nº do contrato',
            'name' => 'contrato-fornecedor'
          ];

          $content[] = [
            'type'     => 'view',
            'view'     => 'vendor.backpack.base.widgets.usuario_fornecedor.lista_atas_usuario_fornecedor',
           'titulo_cabecalho' => 'Ata(s) vigente(s) do fornecedor',
            'placeholder_search' => 'Digite o nº da ata',
            'name' => 'ata-fornecedor'
          ];
    }

@endphp

@extends(backpack_view('blank_externo'))

@php
    $widgets['after_content'][] = [
          'type' => 'div',
          'class' => 'row',
          'content' => $content
    ];
@endphp

@section('content')
    {{--    <img src="{{ config('backpack.base.project_logo') }}" alt="{!! env('APP_NAME') !!}"  width="1500px"/>--}}
    @include(backpack_view('inc.widgets'), [ 'widgets' => app('widgets')->where('group', 'content')->toArray() ])
@endsection