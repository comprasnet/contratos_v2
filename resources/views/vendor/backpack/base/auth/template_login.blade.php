<html lang="pt-BR">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Contratos.gov.br</title>
    <!-- Fonte Rawline-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/rawline.css"/>
    <!-- Fonte Raleway-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900&amp;display=swap"/>
    <!-- Design System de Governo-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/core.min.css"/>
    <!-- Fontawesome-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/all.min.css"/>
    <!-- Style -->
    <link rel="stylesheet" href="layout_login/css/style.css">
    <link rel="icon" type="image/x-icon" href="{{ config('backpack.base.project_somente_logo_ico') }}">

    @yield('header_adicional')

</head>
<body>
<div class="d-lg-flex half">
    <div class="bg order-1 order-md-1" style="background-image: url('{{ config('backpack.base.project_logo_lateral') }}');"></div>
    <div class="contents order-2 order-md-2">
        <div class="container">
            <div style="text-align: right;">
                @yield('area_botoes_superior_direito')
            </div>
            <div class="row align-items-center justify-content-center">
                <div class="col-md-7">
                    <fieldset>
                        <form class="col-md-12 " role="form" method="POST" action="{{ route('backpack.auth.login') }}">
                            {!! csrf_field() !!}
                            <div align="center">
                                <img src="{{ config('backpack.base.project_logo') }}" width="900px" alt="{!! env('APP_NAME') !!}">
                            </div>

                            <div class="mb-5" style="text-align: right;font-size: 16px">
                                Nova versão
                            </div>

                            @yield('area_formulario')

                            <div class="row mt-5 d-flex justify-content-center">
                                @yield('area_botoes_formulario')
                            </div>
                        </form>
                    </fieldset>
                </div>
            </div>
            <div class="footer-text">
                <div >
                    {!! config('app.app_amb') !!}
                    <br>
                    <span>
                          {{ config('app.server_node') }} | v: {{  config('app.app_version') }}
                      </span>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/core-init.js"></script>
<script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/jquery-3.6.1.min.js"></script>
<script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/jquery.mask.js"></script>

@yield('script')
@stack('script')
</body>
</html>