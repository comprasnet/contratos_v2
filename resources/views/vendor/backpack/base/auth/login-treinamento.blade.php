@extends(backpack_view('auth.template_login'))

@section('header_adicional')
    <script src="https://www.google.com/recaptcha/api.js?render={{ env('RECAPTCHA_SITE_KEY') }}"></script>
    <style>
        .half, .half .container > .row {
            height: 87vh;
        }
    </style>
@endsection

@section('area_botoes_superior_direito')
    @include('vendor.backpack.crud.buttons.button_acessar_headset')
@endsection

@section('area_formulario')
    <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response" data-sitekey="{{ env('RECAPTCHA_SITE_KEY') }}">
    <div class="row">
        <div class="col-md-12 mb-3">
            <div class="br-input">
                <label for="{{ $username }}">CPF</label>
                <input type="text" placeholder="Digite o login de acesso"  name="{{ $username }}" value="{{ old($username) }}" id="{{ $username }}">
                @if ($errors->has($username))
                    <span class="invalid-feedback" style="color: red">
                                        <strong>{!! $errors->first($username) !!}</strong>
                                    </span>
                @endif
                @if ($errors->has('erro_usuario_compras'))
                    <span class="invalid-feedback" style="color: red">
                                        <strong>{!! $errors->first('erro_usuario_compras') !!}</strong>
                                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-12 mb-3">
            <div class="br-input input-button">
                <label for="input-password">Senha</label>
                <input id="input-password" type="password" placeholder="Digite a senha" name="password">
                @if ($errors->has('password'))
                    <span class="invalid-feedback"  style="color: red">
                                        <strong>{!! $errors->first('password') !!}</strong>
                                    </span>
                @endif

                <button class="br-button" type="button" aria-label="Mostrar senha">
                    <i class="fas fa-eye" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    </div>

@endsection

@section('area_botoes_formulario')
    @include('vendor.backpack.crud.buttons.button_acessar_sistema')
    <br>
    <br>
    <br>
    <br>
    @include('vendor.backpack.crud.buttons.button_acessar_transparencia')
@endsection

<script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/jquery-3.6.1.min.js"></script>
<script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/jquery.mask.js"></script>

<script type="text/javascript">
    $(document).ready(function($) {
        $('#{{ $username }}').mask('999.999.999-99');

        grecaptcha.ready(function () {
            grecaptcha.execute('6LdUa3YoAAAAAGPRF2IyPNt1DNQ8z0zrnnXZCh2n', {action: 'submit'}).then(function (token) {
                // Preencha o campo g-recaptcha-response
                $('#g-recaptcha-response').val(token);
            });
        });

    });
</script>
