@extends(backpack_view('auth.template_login'))

@section('header_adicional')
    <style>
        .half, .half .container > .row {
          height: 87vh;
        }
    </style>
@endsection

@section('area_botoes_superior_direito')
    @include('vendor.backpack.crud.buttons.button_acessar_headset')
@endsection

@section('area_botoes_formulario')
    @include('vendor.backpack.crud.buttons.button_acessar_govbr')
    @include('vendor.backpack.crud.buttons.button_acessar_transparencia')
    @include('vendor.backpack.crud.buttons.button_acessar_compras_fornecedor')

    <div class="col-md-12 mb-3">
        <div class="form-group{{ session('error_nivel') ? ' has-error' : '' }}">
            <div>
                @if (session('error_nivel'))
                    <p style="color: red; text-align: justify">
                        {!! session('error_msg') !!}
                    </p>
                @endif
            </div>
        </div>
    </div>
@endsection