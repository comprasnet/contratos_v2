<html lang="pt-BR">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Contratos.gov.br</title>
    <!-- Fonte Rawline-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/rawline.css"/>
    <!-- Fonte Raleway-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900&amp;display=swap"/>
    <!-- Design System de Governo-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/core.min.css"/>
    <!-- Fontawesome-->
    <link rel="stylesheet" href="{{ asset('packages/backpack/base/css/padrao_gov') }}/all.min.css"/>

    <!-- Style -->
    <link rel="stylesheet" href="layout_login/css/style-nativo.css">
    <link rel="icon" type="image/x-icon" href="{{ config('backpack.base.project_somente_logo_ico') }}">
    <script src="https://www.google.com/recaptcha/api.js?render={{ env('RECAPTCHA_SITE_KEY') }}"></script>

</head>
<body>

<div class="d-lg-flex half">
    <div class="bg order-1 order-md-1" style="background-image: url('{{ config('backpack.base.project_logo_lateral') }}');"></div>
    <div class="contents order-2 order-md-2">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-md-7">
                    <fieldset >
                        <form class="col-md-12 " role="form" method="POST" action="{{ route('backpack.auth.login') }}" id="recaptcha-form">
                            <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response" data-sitekey="{{ env('RECAPTCHA_SITE_KEY') }}">
                            {!! csrf_field() !!}
                            <div align="center">
                                <img src="{{ config('backpack.base.project_logo') }}" width="300px" alt="{!! env('APP_NAME') !!}">
                            </div>

                            <div class=" mb-5 text-center">
                                <span class="icon-external-link"></span>
                                <div class="scrimutilexemplo" style="display: contents">
                                    {!! config('app.app_amb') !!}
                                    <div class="br-tooltip" role="tooltip" info="info" place="right"><span class="subtext">{{ config('app.server_node') }} | v: {{  config('app.app_version') }}</span></div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-12 mb-3">
                                    <div class="br-input">
                                        <label for="{{ $username }}">CPF</label>
                                        <input type="text" placeholder="Digite o login de acesso"  name="{{ $username }}" value="{{ old($username) }}" id="{{ $username }}">
                                        @if ($errors->has($username))
                                            <span class="invalid-feedback" style="color: red">
                                    <strong>{{ $errors->first($username) }}</strong>
                                </span>
                                        @endif
                                        @if ($errors->has('erro_usuario_compras'))
                                            <span class="invalid-feedback" style="color: red">
                                    <strong>{{ $errors->first('erro_usuario_compras') }}</strong>
                                </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <div class="br-input input-button">
                                        <label for="input-password">Senha</label>
                                        <input id="input-password" type="password" placeholder="Digite a senha" name="password">
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback"  style="color: red">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                        @endif

                                        <button class="br-button" type="button" aria-label="Mostrar senha"><i class="fas fa-eye" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <div class="form-group{{ session('error_nivel') ? ' has-error' : '' }}">
                                        <div>
                                            @if (session('error_nivel'))
                                                <p style="color: red; text-align: justify">
                                                    {!! session('error_msg') !!}
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-center">
                                <br>
                                <button class="br-sign-in primary entrar-teste" type="submit">
                                    <i class="fas fa-user" aria-hidden="true"></i>Entrar
                                </button>
                                <button  class="br-sign-in " type="button" id="acessogov">
                                    Entrar com&nbsp;<img src="https://www.gov.br/++theme++padrao_govbr/img/govbr-colorido-b.png" alt="gov.br"/>
                                </button>
                            </div>

                            <br>

                            <div class="text-center">
                                <button id="transparencia" class="br-sign-in " type="button" id="acessogov">
                                    Transparência
                                </button>
                            </div>

                        </form>


                    </fieldset>


                </div>
            </div>
        </div>
    </div>


</div>



</body>

<script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/core-init.js"></script>
<script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/jquery-3.6.1.min.js"></script>
<script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/jquery.mask.js"></script>


<script type="text/javascript">
    $(document).ready(function($) {
        $('#{{ $username }}').mask('999.999.999-99');

        $('#acessogov').click(function () {
            window.location.href = "{{ route('acessogov.autorizacao') }}";
        })

        $('#transparencia').click(function () {
            window.location.href = "/transparencia";
        })

        grecaptcha.ready(function () {
            grecaptcha.execute('6LdUa3YoAAAAAGPRF2IyPNt1DNQ8z0zrnnXZCh2n', {action: 'submit'}).then(function (token) {
                // Preencha o campo g-recaptcha-response
                $('#g-recaptcha-response').val(token);
            });
        });

    });
</script>

</html>