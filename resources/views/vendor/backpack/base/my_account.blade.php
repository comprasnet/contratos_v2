@extends(backpack_view('blank'))

@section('after_styles')
    <style media="screen">
        .backpack-profile-form .required::after {
            content: ' *';
            color: red;
        }
    </style>
@endsection

@php
  $breadcrumbs = [
      trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
      trans('backpack::base.my_account') => false,
  ];
@endphp

@section('header')
    <section class="content-header">
        <div class="container-fluid mb-3">
            <h1>{{ trans('backpack::base.my_account') }}</h1>
        </div>
    </section>

    @if (session('error'))
        <div class="br-message danger" role="alert">
            <div class="icon"><i class="fas fa-times-circle fa-lg" aria-hidden="true"></i>
            </div>
            <div class="content"><span class="message-title">Erro ao executar à ação.</span><span class="message-body">{{ session('error') }}</span></div>
            <div class="close">
                <button class="br-button circle small" type="button" aria-label="Fechar"><i class="fas fa-times" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    @endif

 <div class="my-custom-template noty_body">
    @if ($errors->count())
        <div class="br-message danger" role="alert">
            <div class="icon"><i class="fas fa-times-circle fa-lg" aria-hidden="true"></i>
            </div>
            <div class="content"><span class="message-title">Erro ao executar à ação.</span>
                <div class="mb-1">
                    @foreach ($errors->all() as $e)
                        <li><span class="message-body">{{ $e }}</span></li>
                    @endforeach
                </div>
                <span class="message-body">{{ session('error') }}</span></div>
            <div class="close">
                <button class="br-button circle small" type="button" aria-label="Fechar"><i class="fas fa-times" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    @endif
 </div>
@endsection

@section('content')
    <div class="row">

        @if (session('success'))
            <div class="br-message success" role="alert">
                <div class="icon"><i class="fas fa-check-circle fa-lg" aria-hidden="true"></i>
                </div>
                <div class="content"><span class="message-title">Sucesso.</span><span class="message-body"> {{ session('success') }}</span></div>
                <div class="close">
                    <button class="br-button circle small" type="button" aria-label="Fechar"><i class="fas fa-times" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="alert alert-success">

                </div>
            </div>
        @endif

        {{-- UPDATE INFO FORM --}}
        <div class="col-lg-8">
            <form class="form" action="{{ route('backpack.account.info.store') }}" method="post">
                {!! csrf_field() !!}
                <div class="card padding-10">
                    <div class="card-header">
                        {{ trans('backpack::base.update_account_info') }}
                    </div>
                    <div class="card-body backpack-profile-form bold-labels">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                @php
                                    $label = trans('backpack::base.name');
                                    $field = 'name';
                                @endphp
                                <label class="required">{{ $label }}</label>
                                <input required class="form-control" type="text" name="{{ $field }}" value="{{ old($field) ? old($field) : $user->$field }}">
                            </div>

                            <div class="col-md-6 form-group">
                                @php
                                    $label = config('backpack.base.authentication_column_name');
                                    $field = backpack_authentication_column();
                                @endphp
                                <label class="">{{ $label }}</label>
                                <input class="form-control" type="{{ backpack_authentication_column()=='email'?'email':'text' }}" name="{{ $field }}" value="{{ old($field) ? old($field) : $user->$field }}" {{ $field == 'email' ? '' : 'readonly' }}>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer  p-3">
                        <button type="submit" class="br-button primary mr-3"><i class="la la-save"></i> {{ trans('backpack::base.save') }}</button>
                        <a href="{{ backpack_url() }}" class="br-button">{{ trans('backpack::base.cancel') }}</a>
                    </div>
                </div>
            </form>
        </div>
        {{-- CHANGE PASSWORD FORM --}}
        <div class="col-lg-8">
            <form class="form" action="{{ route('backpack.account.password') }}" method="post">

                {!! csrf_field() !!}

                <div class="card padding-10">

                    <div class="card-header">
                        {{ trans('backpack::base.change_password') }}
                    </div>

                    <div class="card-body backpack-profile-form bold-labels">
                        <div class="row">
                            <div class="col-md-4 form-group">
                                @php
                                    $label = trans('backpack::base.old_password');
                                    $field = 'old_password';
                                @endphp
                                <label class="required">{{ $label }}</label>
                                <input autocomplete="new-password" required class="form-control" type="password" name="{{ $field }}" id="{{ $field }}" value="">
                            </div>

                            <div class="col-md-4 form-group">
                                @php
                                    $label = trans('backpack::base.new_password');
                                    $field = 'new_password';
                                @endphp
                                <label class="required">{{ $label }}</label>
                                <input autocomplete="new-password" required class="form-control" type="password" name="{{ $field }}" id="{{ $field }}" value="">
                            </div>

                            <div class="col-md-4 form-group">
                                @php
                                    $label = trans('backpack::base.confirm_password');
                                    $field = 'confirm_password';
                                @endphp
                                <label class="required">{{ $label }}</label>
                                <input autocomplete="new-password" required class="form-control" type="password" name="{{ $field }}" id="{{ $field }}" value="">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-3">
                        <button type="submit" class="br-button primary mr-3"><i class="la la-save"></i> {{ trans('backpack::base.save') }}</button>
                        <a href="{{ backpack_url() }}" class="br-button">{{ trans('backpack::base.cancel') }}</a>
                    </div>

                </div>

            </form>
        </div>

    </div>

@endsection
