@include('components_v2.cabecalho_relatorio')

<div class="divisoria"></div>
<br>
<body>
<div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">INFORMAÇÕES DO CONTRATO</h3>

    <!-- 1 linha de colunas compras -->
    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Número do Contrato:</div>
            <div class="info-label">Amparo Legal:</div>

        </div>
        <div class="info-row">
            <div class="info-content">nº&nbsp; {{ $items->numero_contrato }}</div>
            <div class="info-content">{{ $items->amparo_legal }}</div>
        </div>
    </div>
    <br>

    <!-- 2 linha de colunas compras -->
    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Vigência Inicial:</div>
            <div class="info-label">Vigência Final:</div>
        </div>
        <div class="info-row">

            <div class="info-content">{{ $items->vigencia_inicial }}</div>
            <div class="info-content">{{ $items->vigencia_final }}</div>
        </div>
    </div>
    <br>

    <!-- 3 linha de colunas compras -->
    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Número do processo de contratação:</div>
            <div class="info-label">Contratante:</div>

        </div>
        <div class="info-row">
            <div class="info-content">{{ $items->processo }}</div>
            <div class="info-content">{{ $items->contratante }}</div>

        </div>
    </div>



    @if ($items instanceof \Illuminate\Support\Collection && $items->isNotEmpty())
        <div class="info-table">
            <div class="info-row">
                <div class="info-label">Fornecedor(es):</div>
            </div>
            @foreach ($items as $fornecedor)
                <div class="info-row">
                    <div class="info-content">
                        {{ $fornecedor->fornecedor }}
                    </div>
                </div>
            @endforeach
        </div>
        <br>
    @elseif (!empty($items))
        <div class="info-table">
            <div class="info-row">
                <div class="info-label">Fornecedor:</div>
            </div>
            <div class="info-row">
                <div class="info-content">
                    {{ $items->fornecedor }}
                </div>
            </div>
        </div>
        <br>
    @endif


    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Objeto:</div>
            <div class="info-label"></div>
        </div>
        <div class="info-row">
            <div class="info-content">{{ $items->objeto }}</div>
            <div class="info-content"></div>
        </div>
    </div>
    <br>
    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Preposto:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{!! $items->prepostos !!}</div>
        </div>
    </div>
    <br>
    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Gestores:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{!! $items->gestores !!}</div>
        </div>
    </div>

</div>
<br>
<div style="page-break-before: always;"></div>
<div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.6cm;padding-bottom: 0cm;margin-bottom: 0px;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">INFORMAÇÕES DA DECLARAÇÃO DO ORGANISMO DE POLÍTICAS PARA MULHERES</h3>
    <!-- Primeira linha de colunas -->
    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Número do Contrato:</div>
            <div class="info-label">Número/ano da declaração:</div>
        </div>
        <div class="info-row">
            <div class="info-content-primeira-linha p-0">nº&nbsp;{{ $items->numero_contrato }}</div>
            <div class="info-contentc p-0" style="width: 25%; display: table-cell;padding-top: 0 ;">{{ $dadosOpm->sequencial}}</div>

        </div>
    </div>
    <br>
    <br>
    <!-- Segunda linha de colunas -->
    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Quantidade de vagas para mulheres em situação de violência doméstica e familiar </div>
            <div class="info-label">Quantidade de mulheres em situação de violência doméstica e familiar contratadas</div>

        </div>
        <div class="info-row">
            <div class="info-content">{{ $dadosOpm->quantidade_familiar }}</div>
            <div class="info-content">{{ $dadosOpm->quantidade_contratadas }}</div>


        </div>
    </div>
    <br>
    <!-- Terceira linha de colunas -->
    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Data da assinatura:</div>
            <div class="info-label">A empresa contratada está cumprindo o percentual mínimo de 8%</div>
        </div>
        <div class="info-row">
            <div class="info-content"> {{ \Carbon\Carbon::parse($dadosOpm->data_assinatura)->format('d-m-Y') }}</div>
            <div class="info-content">
                {{ $dadosOpm->percentual_minimo ? 'Sim' : 'Não' }}
            </div>
        </div>
    </div>
    <br>

    <!-- Quarta linha de colunas -->
        <div class="info-table">
            <div class="info-row mt-2">
                <div class="info-label">Situação:</div>
                @if (!empty($dadosOpm->justificativa_cancelamento))
                    <div class="info-label">Justificativa cancelamento</div>
                @endif
            </div>
            <div class="info-row">
                <div class="info-content">{{$dadosOpm->getSituacao()}}</div>
                <div class="info-content">
                    {{ $dadosOpm->justificativa_cancelamento}}
                </div>
            </div>
        </div>
    <br>
    <!-- Quinta linha de colunas -->
    @if (!empty($dadosOpm->getUsuarioCancelamento()) && !empty($dadosOpm->data_cancelamento))
        <div class="info-table">
            <div class="info-row">
                <div class="info-label">Usuário responsável pelo cancelamento:</div>
                <div class="info-label">Data do cancelamento:</div>
            </div>
            <div class="info-row">

                <div class="info-content">{{$dadosOpm->getUsuarioCancelamento() ?? null}}</div>
                <div class="info-content">
                    {{ \Carbon\Carbon::parse($dadosOpm->data_cancelamento)->format('d-m-Y') }}
                </div>

            </div>
        </div>

    @endif
<br>
    <!-- sexta linha de colunas -->
    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Declaração criada por: </div>
            <div class="info-label">Data de criação: </div>
        </div>
        <div class="info-row">
            <div class="info-content">{{$dadosOpm->getUsuario() }}</div>
            <div class="info-content"> {{ \Carbon\Carbon::parse($dadosOpm->created_at)->format('d-m-Y') }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Responsável do Contrato: </div>
        </div>
        <div class="info-row">
            <div class="info-content">{{ $responsavel }}</div>
        </div>
    </div>
    <br>
</div>


<br><br>

<style>
    table.table thead {
        background-color: #F0F0F0;
        color: #1c466b !important;
    }


</style>
{{--<div style="page-break-before: always;"></div>--}}

<div class="rodape text-center" style="top:88%;">
    @include('components_v2.rodape_relatorio')
</div>

<style>
    table.table thead,
    table.table tfoot {
        background-color: #F0F0F0;
        color: #1c466b !important;
    }
</style>
</body>
