@php
    $total = 0;
    $countTopicos = 0;
@endphp

<body>
@include('components_v2.cabecalho_relatorio', ['portrait' => true])
<table>
    <tr style="margin-top: -20px">
        <td width="120"></td>
        <td style="font-weight: 400 !important;">
            Número da Alteração: {{ $autorizacaoexecucaoHistorico->getSequencial() }} -
            Data da Alteração: {{ \Carbon\Carbon::now()->format('d/m/Y') }}
        </td>
    </tr>
</table>
<div class="divisoria"></div>
<br>
{{-- add class    --}}
<div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.6cm;padding-bottom: 0cm;margin-bottom: 0px;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - INFORMAÇÕES DO CONTRATO</h3>
    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%;">{{ $fields['contrato']['label'] }}:</div>
            <div class="info-label" style="width: 50%;">{{ $fields['fornecedor']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%;">{{ $fields['contrato']['value'] }}</div>
            <div class="info-content" style="width: 50%;">{{ $fields['fornecedor']['value'] }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%">{{ $fields['contratante']['label'] }}:</div>
            <div class="info-label">{{ $fields['vigencia_inicial_label']['label'] }}:</div>
            <div class="info-label">{{ $fields['vigencia_final_label']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%">{{ $fields['contratante']['value'] }}</div>
            <div class="info-content">{{ $fields['vigencia_inicial_label']['value'] }}</div>
            <div class="info-content">{{ $fields['vigencia_final_label']['value'] }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%">{{ $fields['amparo_legal']['label'] }}:</div>
            <div class="info-label" style="width: 50%">{{ $fields['contrato_processo']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%">{{ $fields['amparo_legal']['value'] }}</div>
            <div class="info-content" style="width: 50%">{{ $fields['contrato_processo']['value'] }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%">{{ $fields['preposto']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%">{!! $fields['preposto']['value'] !!}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%">{{ $fields['gestores']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%">{!! $fields['gestores']['value'] !!}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 100%">{{ $fields['restrito']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content text-justify" style="width: 100%">{{ $fields['restrito']['value'] }}</div>
        </div>
    </div>
</div>
<div class="page-break-always"></div>

<div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - INFORMAÇÕES DA ORDEM DE SERVIÇO</h3>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label">{{ $fields['processo']['label'] }}:</div>
            <div class="info-label">{{ $fields['tipo_id']['label'] }}:</div>
            <div class="info-label">{{ $fields['numero']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{{ $fields['processo']['value'] }}</div>
            <div class="info-content">{{ $fields['tipo_id']['value']['descricao'] }}</div>
            <div class="info-content">{{ $fields['numero']['value'] }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Data de Início da Alteração:</div>
            <div class="info-label">{{ $fields['data_vigencia_inicio']['label'] }}:</div>
            <div class="info-label">{{ $fields['data_vigencia_fim']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{{ $autorizacaoexecucaoHistorico->getDataInicioAlteracao() }}</div>
            <div class="info-content">{{ $fields['data_vigencia_inicio']['value']['formated'] }}</div>
            <div class="info-content">{{ $fields['data_vigencia_fim']['value']['formated'] }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label">{{ $fields['numero_sistema_origem']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{{ $fields['numero_sistema_origem']['value'] }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label">{{ $fields['unidaderequisitante_ids']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{!! $fields['unidaderequisitante_ids']['value']['unidades_descricao'] !!}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label">{{ $fields['empenhos']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{!! $fields['empenhos']['value']['empenhos_descricao'] !!}</div>
        </div>
    </div>
    <br>
</div>
<br>

<div class="page-break-avoid" style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - ALTERAÇÕES REALIZADAS</h3>

    @if ($autorizacaoexecucaoHistorico->verificaTipoAlteracao('vigencia'))
        @php
            $antesDepoisVigenciaFim = $autorizacaoexecucaoHistorico->getAntesDepoisColumnTable('data_vigencia_fim');
        @endphp
        <table class="table table-bordered table-striped m-b-0">
            <thead>
                <tr>
                    <th colspan="2" style="text-align: center">{{ $fields['data_vigencia_fim']['label'] }}</th>
                </tr>
                <tr>
                    <td style="width: 50%" >Antes</td>
                    <td style="width: 50%">Depois</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="width: 50%">{{ $antesDepoisVigenciaFim['Antes'][0] }}</td>
                    <td style="width: 50%">{{ $antesDepoisVigenciaFim['Depois'][0] }}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <br>
    @endif

    @if ($autorizacaoexecucaoHistorico->verificaTipoAlteracao('itens'))
        <div class="resize-font">
            @include('crud::autorizacaoexecucao.table-itens', ['hidden' => $hiddenColumnsItens])
        </div>

        @if($fields['autorizacaoexecucaoItens']['value']->count())
            <p class="text-justify">
                O Valor Total da presente Ordem de Serviço / Fornecimento é de R$ {{ number_format($valorTotal, 2, ',', '.') }} ({{ $valorPorExtenso }}).
            </p>
        @endif
        <br>
        <br>
    @endif

    @if ($autorizacaoexecucaoHistorico->verificaTipoAlteracao('extingir'))
        <p>Alteração de extinguir OS/F na data de {{ $autorizacaoexecucaoHistorico->getDataExtincao() }}</p>
    @endif
</div>

<div class="page-break-avoid" style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - HISTÓRICO DAS ALTERAÇÕES ANTERIORES</h3>

    @include(
        'crud::autorizacaoexecucao.table-historico-alteracoes',
        ['historicos' => $historicoAlteracoes, 'actions' => false]
    )
</div>

<div class="text-justify page-break-avoid" style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - {{ $fields['informacoes_complementares']['label'] }}</h3>

    <div class="padroniza-tinymce">
        {!! str_replace("\r\n", '<br>', $fields['informacoes_complementares']['value']) !!}
    </div>
</div>
<br>

<div class="page-break-avoid text-justify" style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - AUTORIZAÇÃO</h3>

    <p>
        Autorizamos a execução dos serviços constantes do Contrato Administrativo nº {{ $fields['contrato']['value'] }},
        conforme detalhamento que consta no quadro de especificação do(s) produto(s)/serviço(s) e volumes desta
        {{ $fields['tipo_id']['value']['descricao'] }}.
    </p>
</div>
<br>


<div style="padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 2cm;">
    <div class="w-100 assign-session">
    @foreach($assinantes as $assinante)
        <div class="page-break-avoid">
            <i>Documento assinado eletronicamente</i>
            <p>
                {{ $assinante->user ? $assinante->user->name : $assinante->nome }}<br>
                {{ $assinante->funcao ? $assinante->funcao->descricao : 'Preposto' }}<br>
            </p>
        </div>
    @endforeach
    </div>
</div>

<div class="rodape text-center" style="top:88%;">
    @include('components_v2.rodape_relatorio')
</div>

<style>
    table.table thead,
    table.table tfoot
    {
        background-color: #F0F0F0;
        color: #1c466b !important;
    }
    div.resize-font > table.table td,
    div.resize-font > table.table thead,
    div.resize-font > table.table tfoot {
        /* Aumenta a fonte da table confome o número de colúnas ocultas */
        font-size: {{ 6 + count($hiddenColumnsItens) }}px  !important;
    }
    .assign-session {
        width: 100%;
        font-size: 14px;
        text-align: center;
        margin-top: 30px;
    }
    .text-justify {
        text-align: justify;
    }
    .page-break-avoid {
        page-break-inside: avoid;
    }
    .page-break-always {
        page-break-before: always;
    }

    .padroniza-tinymce * {
        font-family: 'Rawline', sans-serif;
        font-size: 16px !important;
        text-align: justify !important;
    }
</style>
</body>
