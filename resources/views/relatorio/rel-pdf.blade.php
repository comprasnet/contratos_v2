@include('components_v2.cabecalho_relatorio')

<div class="divisoria"></div>
<br>
<body>
<!-- Primeira linha de colunas -->
<div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.6cm;padding-bottom: 0cm;margin-bottom: 0px;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">INFORMAÇÕES DA ATA</h3>
    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Ata:</div>
            <div class="info-label">Última Atualização:</div>
            <div class="info-label">Link da ata no PNCP:</div>

        </div>
        <div class="info-row">
            <div class="info-content-primeira-linha p-0">nº&nbsp;{{$arpDados->arp_numero}}</div>
            <div class="info-contentc  p-0"
                 style="width: 25%; display: table-cell;padding-top: 0 ;">{{$arpDados->updated_at->format('d/m/Y')}}</div>
            <div class="info-content">
                <a href="<?php echo $linkPncp; ?>" style="text-decoration: none; color: #000;">
                    <?php
                    // Texto a ser exibido
                    $substrings = str_split($linkPncp, 35);
                    // Exibe as substrings em linhas separadas
                    foreach ($substrings as $substring) {
                        echo $substring . "<br>";
                    }
                    ?>
                </a>
            </div>


        </div>
    </div>
    <br>

    <!-- Segunda linha de colunas -->
    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Vigência</div>
            <div class="info-label">Órgão:</div>
            <div class="info-label">Unidade gerenciadora:</div>

        </div>
        <div class="info-row">
            <div class="info-content">de {{date("d/m/Y", strtotime($arpDados->arp_vigencia_inicial))}}
                a {{date("d/m/Y", strtotime($arpDados->arp_vigencia_final))}}</div>
            <div class="info-content">
                {{$arpDados->orgao_nome}}
            </div>
            <div class="info-content-intermediario p-0"
                 style="padding-top: 0 !important; padding-bottom: 0px !important; margin-bottom: -20px !important;">
                {{$arpDados->unidade_gerenciadora}}
            </div>

        </div>
    </div>
    <br>

    <!-- terceira linha de colunas -->
    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Valor Contratado:</div>
            <div class="info-label"></div>

        </div>
        <div class="info-row">
            <div class="info-content">R$ {{number_format($valorTotal,2,',','.')}}</div>
            <div class="info-content"></div>
            <div class="info-content"></div>
        </div>
    </div>
    <br>
    <br>
    <!-- quarta linha de colunas -->
    <table class="table">
        <thead>
        <tr>
            <th>Fornecedor</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($dadosFornecedores as $fornecedor)
                <tr>
                    <td>{{$fornecedor}}</td>
                </tr>
            @endforeach
        </tbody>
    
    </table>
    <br>
    <br>
    <!-- quarta linha de colunas -->
    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Objeto:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{{$arpDados->objeto}}</div>
        </div>
    </div>
    <br>
</div>
<br>
<div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">INFORMAÇÕES DA COMPRA</h3>

    <!-- 1 linha de colunas  compras -->
    <div class="info-table">
        <div class="info-row">

            <div class="info-label">Número da compra / Ano:</div>
            <div class="info-label">Modalidade da compra:</div>
            <div class="info-label">Data da assinatura:</div>

        </div>
        <div class="info-row">
            <div class="info-content">{{ $numeroCompra }}</div>
            <div class="info-content">{{$modalidadeCompra }}</div>
            <div class="info-content">{{date("d/m/Y", strtotime($arpDados->data_assinatura))}}</div>
        </div>
    </div>

</div>
<br><br>

@if ($arpAlteracao->isNotEmpty())
    <h3 class="titulo-ata" style="font-weight: 500 !important;">ALTERAÇÕES DA ATA</h3>

    <table class="table">
        <thead>
        <tr>
            <th>Tipo de Alteração</th>
            <th>Objeto da alteração</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($arpAlteracao as $alteracao)
            <tr>
                <td>{{ $alteracao->status_descricao }}</td>
                <td>
                    @if ($alteracao->status_descricao == 'Cancelamento de item(ns)')
                        {{ $alteracao->motivo_cancelamento_descricao }}
                    @else
                        {{ $alteracao->objeto_descricao }}
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif




<br><br>


<h3 class="titulo-ata" style="font-weight: 500 !important;">ITENS DA ATA</h3>


<table class="table">
    <thead>
    <tr>
        <th>Número</th>
        <th>Item</th>
        <th>Aceita Adesão</th>
        <th>Qtd. Limite Adesão</th>
        <th>Qtd. Limite Informado na Compra</th>
        <th>Código</th>
        <th>Tipo</th>
        <th>Qtd. Homologada</th>
    </tr>
    </thead>
    <tbody>
    @foreach($items as $item)
        <tr>
            <td>{{ $item->numero }}</td>
            <td>{{ $item->descricaodetalhada }}</td>
            <td>{{ $item->aceita_adesao }}</td>
            <td>{{ (int)$item->maximo_adesao }}</td>
            <td>{{ (int)$item->maximo_adesao_informado_compra }}</td>
            <td>{{ $item->codigo_item }}</td>
            <td>{{ $item->tipo_item }}</td>

            <td>{{number_format($item->quantidade_homologada, 4, ',', '.')}}</td>
        </tr>
    @endforeach
    </tbody>

</table>



    <style>
    table.table thead {
        background-color: #F0F0F0;
        color: #1c466b !important;
    }


</style>
<div style="page-break-before: always;"></div>


<div class="info-conteudo-detalhamento" style="width: 100% !important;min-height: 800px;">
    @php
        $lastItem = null; // Variável para acompanhar o último item
        $lastFornecedor = null; // Variável para acompanhar o último fornecedor
        use Carbon\Carbon;
    @endphp

    @foreach($arpDetalhamento  as $key=>$detalhamentoItem)
        @if($lastItem === null || $lastItem->numero !== $detalhamentoItem->numero)
            <!-- Iniciar uma nova tabela para o novo item -->

            <div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.2cm;padding-bottom: 0.2cm;margin-bottom: 0px;">
                <h3 class="titulo-ata" style="font-weight: 500 !important;">Detalhamento do
                    item {{$detalhamentoItem->numero}} </h3>
                <table class="info-table-detalhamento">
                    <tr>
                        <th class="info-title">Descrição detalhada:</th>
                        <td class="info-content-detalhamento">{{$detalhamentoItem->descricaodetalhada}}</td>
                    </tr>
                    <tr>
                        <th class="info-title">Código do item:</th>
                        <td class="info-content-detalhamento">{{$detalhamentoItem->codigo_item}}</td>
                    </tr>
                    <tr>
                        <th class="info-title">Tipo do item:</th>
                        <td class="info-content-detalhamento">{{$detalhamentoItem->tipo_item}}</td>
                    </tr>
                    <tr>
                        <th class="info-title">Quantidade homologada:</th>
                        <td class="info-content-detalhamento">{{ number_format($detalhamentoItem->quantidade_homologada, 4, ',', '.') }}</td>
                    </tr>


                    <tr>
                        <th class="info-title">Vigência inicial:</th>
                        <td class="info-content-detalhamento">{{date("d/m/Y", strtotime($detalhamentoItem->ata_vigencia_inicio))}}</td>
                    </tr>
                    <tr>
                        <th class="info-title">Vigência final:</th>
                        <td class="info-content-detalhamento">{{date("d/m/Y", strtotime($detalhamentoItem->ata_vigencia_fim))}}</td>
                    </tr>
                </table>
            </div>
        @endif


        <!-- Tabela de Fornecedores -->
        @php
            $fornecedoresDoItem = collect($arpFornecedor)->where('numero', $detalhamentoItem->numero);
        @endphp

        @if($fornecedoresDoItem->count() > 0 && ($lastFornecedor === null || $lastFornecedor->numero !== $detalhamentoItem->numero))
            <h3 class="titulo-ata" style="font-weight: 700 !important;">Fornecedor(es)
                item {{$detalhamentoItem->numero}}:</h3>
            <table class="table">
                <thead>
                <tr>
                    <th>Classificação</th>
                    <th>CNPJ</th>
                    <th>Fornecedor</th>
                    <th>Qtd. total</th>
                    <th>Valor unitário</th>
                </tr>
                </thead>
                <tbody>
                @foreach($fornecedoresDoItem as $detalhamentoFornecedor)
                    <tr>
                        <td>{{$detalhamentoFornecedor->classificacao}}</td>
                        <td>{{$detalhamentoFornecedor->cpf_cnpj_idgener}}</td>
                        <td>{{$detalhamentoFornecedor->nome}}</td>

                        <td>{{number_format($detalhamentoFornecedor->quantidade_homologada_vencedor, 4, ',', '.')}}</td>
                        <td>{{$detalhamentoFornecedor->valor_unitario}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        @endif
        <br>
        <!-- Tabela de Unidades -->
        @php
            $unidadesDoItem = collect($arpUnidade)->where('numero', $detalhamentoItem->numero);
        @endphp
        @if($unidadesDoItem->count() > 0 && ($lastFornecedor === null || $lastFornecedor->numero !== $detalhamentoItem->numero))
            <h3 class="titulo-ata" style="font-weight: 700 !important;">Unidade(s)
                item {{$detalhamentoItem->numero}}</h3>
            <table class="table">
                <thead>
                <tr>
                    <th>Código</th>
                    <th>Unidade</th>
                    <th>Tipo da unidade</th>
                    <th>Qtd. registrada</th>
                    <th>Qtd. disponível para remanejamento/empenho</th>
                </tr>
                </thead>
                <tbody>
                @foreach($unidadesDoItem as $detalhamentoUnidade)
                    <tr>
                        <td>{{$detalhamentoUnidade->codigo}}</td>
                        <td>{{$detalhamentoUnidade->nomeresumido}}</td>
                        <td>{{$detalhamentoUnidade->tipo_uasg}}</td>
                        <td>{{$detalhamentoUnidade->quantidade_autorizada}}</td>
                        <td>{{$detalhamentoUnidade->quantidade_saldo}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif


        <br>
        <!-- Tabela de adesao -->
        @php
            $adesaoDoItem = collect($adesaoTotalizador)->where('numero', $detalhamentoItem->numero);
        @endphp
        @if($adesaoDoItem->count() > 0 && ($lastFornecedor === null || $lastFornecedor->numero !== $detalhamentoItem->numero))

            <div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.2cm;padding-bottom: 0.4cm;margin-bottom: 0px;">
                <h3 class="titulo-ata" style="font-weight: 700 !important;">Adesões(s)
                    item {{$detalhamentoItem->numero}}</h3>
                <table class="info-table-detalhamento-adesao">
                    @foreach($adesaoDoItem as $detalhamentoAdesao)
                        <tr>
                            <th class="info-title-adesao">
                                Qtd. máxima para adesão
                            </th>
                            <td class="info-content-detalhamento-adesao">
                                {{$detalhamentoAdesao->maximo_adesao}}
                            </td>

                        </tr>
                        <tr>
                            <th class="info-title-adesao">
                                Qtd. disponivel para adesão:
                            </th>
                            <td class="info-content-detalhamento-adesao">
                                {{$detalhamentoAdesao->quantidade_disponivel_adesao}}
                            </td>
                        </tr>
                        <tr>
                            <th class="info-title-adesao">
                                Quantidade aguardando análise:
                            </th>
                            <td class="info-content-detalhamento-adesao">
                                {{$detalhamentoAdesao->quantidade_aguardando_analise}}
                            </td>
                        </tr>
                        <tr>
                            <th class="info-title-adesao">
                                Aceita adesão
                            </th>
                            <td class="info-content-detalhamento-adesao">
                                {{$detalhamentoAdesao->aceita_adesao}}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>

        @endif
        <!-- Tabela de adesao aprovadas-->
        @php
            $adesaoAprovadaDoItem = collect($adesaoAprovadas)->where('numero', $detalhamentoItem->numero);
        @endphp
        @if($adesaoAprovadaDoItem->count() > 0 && ($lastFornecedor === null || $lastFornecedor->numero !== $detalhamentoItem->numero))
            <h4 class="titulo-adesao">Solicitações aprovadas:</h4>
            <table class="table">
                <thead>
                <tr>
                    <th>
                        Unidade
                    </th>
                    <th>
                        Data aprovação análise
                    </th>
                    <th>
                        Qtd. aprovada da adesão
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($adesaoAprovadaDoItem as $detalhamentoAdesaoAprovada)
                    <tr>
                        <td>
                            {{$detalhamentoAdesaoAprovada->unidade_solicitacao}}
                        </td>
                        <td>
                            {{$detalhamentoAdesaoAprovada->data_aprovacao_analise}}
                        </td>
                        <td>
                            {{$detalhamentoAdesaoAprovada->quantidade_aprovada}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
        <br>
        <!-- Tabela de adesao aprovadasssssssssss-->

        @php
            $adesaoAnaliseDoItem = collect($adesaoAnalise)->where('numero', $detalhamentoItem->numero);
        @endphp
        @if($adesaoAnaliseDoItem->count() > 0 && ($lastFornecedor === null || $lastFornecedor->numero !== $detalhamentoItem->numero))
            <h4 class="titulo-adesao">Solicitações aguardando análise:</h4>
            <table class="table">
                <thead>
                <tr>
                    <th>
                        Unidade
                    </th>
                    <th>
                        Data/hora da solicitação
                    </th>
                    <th>
                        Quantidade aguardando análise
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($adesaoAnaliseDoItem as $detalhamentoAdesaoAnalisada)
                    <tr>
                        <td>
                            {{$detalhamentoAdesaoAnalisada->unidade_solicitacao}}
                        </td>
                        <td>
                            {{ $detalhamentoAdesaoAnalisada->data_hora_solicitacao }}

                        </td>
                        <td>
                            {{$detalhamentoAdesaoAnalisada->quantidade_solicitada}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif

        <!-- Tabela de totalizador empenho -->
        @php
            $somaResultadosEmpenhoDoItem = collect($somaResultadosEmpenho);
            $somaResultadosSaldoEmpenhoDoItem = collect($somaResultadosSaldo);
        @endphp
        @if($somaResultadosEmpenhoDoItem->count() > 0 && ($lastFornecedor === null || $lastFornecedor->numero !== $detalhamentoItem->numero))

            <div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.2cm;padding-bottom: 0.4cm;margin-bottom: 0px;">
                <h3 class="titulo-ata" style="font-weight: 700 !important;">Empenho(s)
                    item {{$detalhamentoItem->numero}}</h3>
                <table class="info-table-detalhamento-adesao">
                    @foreach($somaResultadosEmpenhoDoItem as $detalhamentoSomaEmpenhoDoItem)
                        <tr>
                            <th class="info-title-adesao">
                                Qtd. Registrada / Autorizada:
                            </th>
                            <td class="info-content-detalhamento-adesao">
                                {{ number_format($detalhamentoSomaEmpenhoDoItem, 5, '.', '') }}
                            </td>
                        </tr>
                    @endforeach
                        @foreach($somaResultadosSaldoEmpenhoDoItem as $detalhamentoSomaSaldoEmpenhoDoItem)
                            <tr>
                                <th class="info-title-adesao">
                                    Saldo para Empenho
                                </th>
                                <td class="info-content-detalhamento-adesao">
                                    {{ number_format($detalhamentoSomaSaldoEmpenhoDoItem, 5, '.', '') }}
                                </td>
                            </tr>
                        @endforeach
                </table>
            </div>
        @endif
        <!-- Tabela de empenhos unidade-->
        @php
            $empenhoUnidadeTotalizadorDoItem = collect($totalizadorMinutaEmpenho)->where('numero', $detalhamentoItem->numero);
        @endphp
        @if($empenhoUnidadeTotalizadorDoItem->count() > 0 && ($lastFornecedor === null || $lastFornecedor->numero !== $detalhamentoItem->numero))
            <table class="table">
                <thead>
                <tr>
                    <th>
                        Unidade
                    </th>
                    <th>
                        Tipo
                    </th>
                    <th>
                        Quantidade registrada
                    </th>
                    <th>
                        Quantidade empenhada
                    </th>
                    <th>
                        Saldo para empenho
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($empenhoUnidadeTotalizadorDoItem as $detalhamentoEmpenhoUnidade)
                    <tr>
                        <td>
                            {{$detalhamentoEmpenhoUnidade->unidade}}
                        </td>
                        <td>
                            {{$detalhamentoEmpenhoUnidade->tipo_uasg}}
                        </td>
                        <td>
                            {{$detalhamentoEmpenhoUnidade->quantidade_autorizada}}
                        </td>
                        <td>
                            {{$detalhamentoEmpenhoUnidade->quantidade_empenhada}}
                        </td>
                        <td>
                            {{$detalhamentoEmpenhoUnidade->saldo_para_empenho}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
        <br><br>
        <!-- Tabela de minuta empenho-->
        @php
            $minutasEmpenhoPorUnidadeDoItem = collect($minutasEmpenhoPorUnidade)->where('numero', $detalhamentoItem->numero);
        @endphp
        @if($minutasEmpenhoPorUnidadeDoItem->count() > 0 && ($lastFornecedor === null || $lastFornecedor->numero !== $detalhamentoItem->numero))

            <table class="table">
                <thead>
                <tr>
                    <th>
                        Número de empenho
                    </th>
                    <th>
                        Unidade
                    </th>
                    <th>
                        Fornecedor
                    </th>
                    <th>
                        Data do empenho
                    </th>
                    <th>
                        Quantidade incluída
                    </th>
                    <th>
                        Reforço
                    </th>
                    <th>
                        Anulação
                    </th>
                    <th>
                        Quantidade empenhada
                    </th>
                    <th>
                        Valor
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($minutasEmpenhoPorUnidadeDoItem as $detalhamentoMinutaEmpenho)
                    <tr>
                        <td>
                            {{$detalhamentoMinutaEmpenho->mensagem_siafi}}
                        </td>
                        <td>
                            {{$detalhamentoMinutaEmpenho->unidade}}
                        </td>
                        <td>
                            {{$detalhamentoMinutaEmpenho->fornecedor}}
                        </td>
                        <td>
                            {{$detalhamentoMinutaEmpenho->data_emissao}}
                        </td>
                        <td>
                            {{$detalhamentoMinutaEmpenho->quantidade_inclusao}}
                        </td>
                        <td>
                            {{$detalhamentoMinutaEmpenho->quantidade_reforco}}
                        </td>
                        <td>
                            {{$detalhamentoMinutaEmpenho->quantidade_anulacao}}
                        </td>
                        <td>
                            {{$detalhamentoMinutaEmpenho->quantidade_empenhada}}
                        </td>
                        <td>
                            {{$detalhamentoMinutaEmpenho->valor}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
        <br>
            <!-- Tabela de Contratos -->
            @php
                $contratosDoItem = collect($arpContratos)->where('numero_item_compra', $detalhamentoItem->numero);
            @endphp
            @if($contratosDoItem->count() > 0 && ($lastFornecedor === null || $lastFornecedor->numero !== $detalhamentoItem->numero))
                <h3 class="titulo-ata" style="font-weight: 700 !important;">Contrato(s)
                    item {{$detalhamentoItem->numero}}:</h3>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Número do contrato</th>
                        <th>Unidade</th>
                        <th>Fornecedor</th>
                        <th>Qtd. contratada</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contratosDoItem as $detalhamentoContratos)
                        <tr>
                            <td>{{$detalhamentoContratos->numero}}</td>
                            <td>{{$detalhamentoContratos->unidade}}</td>
                            <td>{{$detalhamentoContratos->fornecedor_contrato}}</td>
                            <td>{{number_format($detalhamentoContratos->quantidade_contratada, 4, ',', '.')}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        @php

            $lastItem = $detalhamentoItem;
            $lastFornecedor = $detalhamentoItem;
        @endphp
        @if(count($arpDetalhamento)-1 > $key)
            <div style="page-break-before: always;"></div>
        @endif

    @endforeach
</div>

<div class="rodape text-center" style="top:88%;">
    @include('components_v2.rodape_relatorio')
</div>
</body>
