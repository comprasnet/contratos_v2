@php
    $total = 0;
    $countTopicos = 0;
    $itensEntregues = $termoRecebimento->entregaItens->load('itens');
@endphp

<body>
@include('components_v2.cabecalho_relatorio', ['portrait' => true])
<div class="divisoria"></div>
<br>
{{-- add class    --}}
<div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.6cm;padding-bottom: 0cm;margin-bottom: 0px;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - TERMO DE RECEBIMENTO PROVISÓRIO</h3>
    <div>
        {!! str_replace("\r\n", '<br>', $termoRecebimento->introducao_trp) !!}
    </div>
    <br>

    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - INFORMAÇÕES DO CONTRATO</h3>
    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%;">{{ $fields['contrato']['label'] }}:</div>
            <div class="info-label" style="width: 50%;">{{ $fields['fornecedor']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%;">{{ $fields['contrato']['value'] }}</div>
            <div class="info-content" style="width: 50%;">{{ $fields['fornecedor']['value'] }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%">{{ $fields['contratante']['label'] }}:</div>
            <div class="info-label">{{ $fields['vigencia_inicial_label']['label'] }}:</div>
            <div class="info-label">{{ $fields['vigencia_final_label']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%">{{ $fields['contratante']['value'] }}</div>
            <div class="info-content">{{ $fields['vigencia_inicial_label']['value'] }}</div>
            <div class="info-content">{{ $fields['vigencia_final_label']['value'] }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%">{{ $fields['amparo_legal']['label'] }}:</div>
            <div class="info-label" style="width: 50%">{{ $fields['contrato_processo']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%">{{ $fields['amparo_legal']['value'] }}</div>
            <div class="info-content" style="width: 50%">{{ $fields['contrato_processo']['value'] }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%">{{ $fields['preposto']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%">{!! $fields['preposto']['value'] !!}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%">{{ $fields['gestores']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%">{!! $fields['gestores']['value'] !!}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 100%">{{ $fields['restrito']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content text-justify" style="width: 100%">{{ $fields['restrito']['value'] }}</div>
        </div>
    </div>
</div>
<div class="page-break-always"></div>

<div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - INFORMAÇÕES DA ORDEM DE SERVIÇO</h3>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label">{{ $fields['processo']['label'] }}:</div>
            <div class="info-label">{{ $fields['tipo_id']['label'] }}:</div>
            <div class="info-label">{{ $fields['numero']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{{ $fields['processo']['value'] }}</div>
            <div class="info-content">{{ $fields['tipo_id']['value']['descricao'] }}</div>
            <div class="info-content">{{ $fields['numero']['value'] }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label">{{ $fields['data_assinatura']['label'] }}:</div>
            <div class="info-label">{{ $fields['data_vigencia_inicio']['label'] }}:</div>
            <div class="info-label">{{ $fields['data_vigencia_fim']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{{ $fields['data_assinatura']['value']['formated'] }}</div>
            <div class="info-content">{{ $fields['data_vigencia_inicio']['value']['formated'] }}</div>
            <div class="info-content">{{ $fields['data_vigencia_fim']['value']['formated'] }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label">{{ $fields['numero_sistema_origem']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{{ $fields['numero_sistema_origem']['value'] }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label">{{ $fields['unidaderequisitante_ids']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{!! $fields['unidaderequisitante_ids']['value']['unidades_descricao'] !!}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label">{{ $fields['empenhos']['label'] }}:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{!! $fields['empenhos']['value']['empenhos_descricao'] !!}</div>
        </div>
    </div>
    <br>
</div>
<br>

<div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - ITENS ENTREGUES</h3>

    <table class="table table-bordered table-striped m-b-0">
        <thead>
        <tr>
            <th>Tipo item</th>
            <th>Num item compra</th>
            <th>Item</th>
            <th>Unidade de Fornecimento</th>
            <th>Quantidade Solicitada na OS/F</th>
            <th>Quantidade Informada na Entrega</th>
            <th>Glosa (R$)</th>
            <th>Valor Unitário</th>
            <th>Valor Total da entrega</th>
        </tr>
        </thead>
        <tbody>
        @foreach($itensEntregues as $itemEntregue)
            @php
                $valorTotalItemEntrega = $itemEntregue->quantidade_informada * $itemEntregue->itens->valor_unitario_sem_formatar - $itemEntregue->valor_glosa;
                $total += $valorTotalItemEntrega;
            @endphp
            <tr>
                <td>{{ $itemEntregue->itens->tipo_item }}</td>
                <td>{{ $itemEntregue->itens->numero_item_compra }}</td>
                <td>{{ $itemEntregue->itens->item }}</td>
                <td>{{ $itemEntregue->itens->unidadeMedida->nome }}</td>
                <td>{{ $itemEntregue->itens->getQuantidadeTotal() }}</td>
                <td>{{ str_replace('.', ',', (float) $itemEntregue->quantidade_informada) }}</td>
                <td>{{ str_replace('.', ',', (float) $itemEntregue->valor_glosa) }}</td>
                <td>{{ $itemEntregue->itens->valor_unitario }}</td>
                <td>{{ number_format($valorTotalItemEntrega, 2, ',', '.') }}</td>
            </tr>
            @if(
                !empty($itemEntregue->mes_ano_competencia) ||
                !empty($itemEntregue->processo_sei) ||
                !empty($itemEntregue->data_inicio) ||
                !empty($itemEntregue->data_fim) ||
                !empty($itemEntregue->horario)
            )
                <tr>
                    <td colspan="9">
                        @if(!empty($itemEntregue->mes_ano_competencia))
                            <p style="display: inline-block; margin: 3px 5px"><b>Competência:</b> {{ \Carbon\Carbon::parse($itemEntregue->mes_ano_competencia)->format('m/Y') }}</p>
                        @endif
                        @if(!empty($itemEntregue->processo_sei))
                            <p style="display: inline-block; margin: 3px 5px"><b>Processo SEI:</b> {{ $itemEntregue->processo_sei }}</p>
                        @endif
                        @if(!empty($itemEntregue->data_inicio))
                            <p style="display: inline-block; margin: 3px 5px"><b>Data Início:</b> {{ \Carbon\Carbon::parse($itemEntregue->data_inicio)->format('d/m/Y') }}</p>
                        @endif
                        @if(!empty($itemEntregue->data_fim))
                            <p style="display: inline-block; margin: 3px 5px"><b>Data Fim:</b> {{ \Carbon\Carbon::parse($itemEntregue->data_fim)->format('d/m/Y') }}</p>
                        @endif
                        @if(!empty($itemEntregue->horario))
                            <p style="display: inline-block; margin: 3px 5px"><b>Horário:</b> {{ $itemEntregue->horario }}</p>
                        @endif
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>


    <p class="text-justify">
        O Valor Total da entrega do presente Termo de Recebimento Provisório é de R$ {{ number_format($total, 2, ',', '.') }}
        ({{$termoRecebimento->valorPorExtenso($total)}}).
    </p>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Locais de Execução para Entrega:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{{ $termoRecebimento->getLocaisExecucao() }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label">Mês/Ano de Referência:</div>
        </div>
        <div class="info-row">
            <div class="info-content">{{ $termoRecebimento->getMesAnoReferencia() }}</div>
        </div>
    </div>
</div>
<br>

<div class="text-justify page-break-avoid" style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - INFORMAÇÕES COMPLEMENTARES</h3>

    <div>
        {!! str_replace("\r\n", '<br>', $termoRecebimento->informacoes_complementares_trp) !!}
    </div>
</div>
<br>

@if ($anexos->count() > 0)
    <div class="text-justify page-break-avoid" style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
        <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - ANEXOS</h3>

        <table class="table table-bordered table-striped m-b-0">
            <thead>
                <tr>
                    <th>Arquivo</th>
                    <th>Descrição</th>
                </tr>
            </thead>
            <tbody>
                @foreach($anexos as $anexo)
                    <tr>
                        <td>
                            <a href="{{url('storage/' . $anexo->url)}}" target="_blank">{{ $anexo->nome }}</a>
                        </td>
                        <td>{{ $anexo->descricao }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <br>
@endif

<div class="page-break-avoid text-justify" style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - RECEBIMENTO PROVISÓRIO</h3>

    <p>
        Por este instrumento, atestamos, que o objeto descrito no item 4 do presente documento, correspondentes à
        entrega {{$termoRecebimento->getFormattedSequencial()}} da OS/F nº {{$fields['numero']['value']}} do Contrato
        {{$fields['contrato']['value']}}, foram recebidos provisoriamente na presente data e serão objeto de avaliação
        por parte da CONTRATANTE quanto à adequação da entrega às condições contratuais, de acordo com os Critérios de
        Aceitação previamente definidos no Modelo de Gestão do contrato.
    </p>

    <p>
        Ressaltamos que o recebimento definitivo destes serviços/produtos ocorrerá após a verificação dos requisitos e
        demais condições contratuais, desde que não se observem inconformidades ou divergências quanto às especificações
        constantes do Termo de Referência e do Contrato acima identificado que ensejem correções por parte da
        <b>CONTRATADA</b>.
    </p>
</div>
<br>


<div style="padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 2cm;">
    <div class="w-100 assign-session">
    @foreach($assinantes as $assinante)
        <div class="page-break-avoid">
            <i>Documento assinado eletronicamente</i>
            <p>
                {{ $assinante->user ? $assinante->user->name : $assinante->nome }}<br>
                {{ $assinante->funcao ? $assinante->funcao->descricao : 'Preposto' }}<br>
            </p>
        </div>
    @endforeach
    </div>
</div>

<div class="rodape text-center" style="top:88%;">
    @include('components_v2.rodape_relatorio')
</div>

<style>
    table.table thead,
    table.table tfoot
    {
        background-color: #F0F0F0;
        color: #1c466b !important;
    }
    table.table td,
    table.table thead,
    table.table tfoot {
        font-size: 13px  !important;
    }
    .assign-session {
        width: 100%;
        font-size: 14px;
        text-align: center;
        margin-top: 30px;
    }
    .text-justify {
        text-align: justify;
    }
    .page-break-avoid {
        page-break-inside: avoid;
    }
    .page-break-always {
        page-break-before: always;
    }

    .padroniza-tinymce * {
        font-family: 'Rawline', sans-serif;
        font-size: 16px !important;
        text-align: justify !important;
    }
</style>
</body>
