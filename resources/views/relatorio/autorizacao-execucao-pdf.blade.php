<body>
    @include('components_v2.cabecalho_relatorio')
    <div class="divisoria"></div>
    <br>
    {{-- add class    --}}
    <div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.6cm;padding-bottom: 0cm;margin-bottom: 0px;">
        <h3 class="titulo-ata" style="font-weight: 500 !important;">INFORMAÇÕES DO CONTRATO</h3>
        <div class="info-table">
            <div class="info-row">
                <div class="info-label" style="width: 50%;">{{ $fields['contrato']['label'] }}:</div>
                <div class="info-label" style="width: 50%;">{{ $fields['fornecedor']['label'] }}:</div>
            </div>
            <div class="info-row">
                <div class="info-content" style="width: 50%;">{{ $fields['contrato']['value'] }}</div>
                <div class="info-content" style="width: 50%;">{{ $fields['fornecedor']['value'] }}</div>
            </div>
        </div>
        <br>

        <div class="info-table">
            <div class="info-row">
                <div class="info-label" style="width: 50%">{{ $fields['contratante']['label'] }}:</div>
                <div class="info-label">{{ $fields['vigencia_inicial_label']['label'] }}:</div>
                <div class="info-label">{{ $fields['vigencia_final_label']['label'] }}:</div>
            </div>
            <div class="info-row">
                <div class="info-content" style="width: 50%">{{ $fields['contratante']['value'] }}</div>
                <div class="info-content">{{ $fields['vigencia_inicial_label']['value'] }}</div>
                <div class="info-content">{{ $fields['vigencia_final_label']['value'] }}</div>
            </div>
        </div>
        <br>

        <div class="info-table">
            <div class="info-row">
                <div class="info-label" style="width: 50%">{{ $fields['amparo_legal']['label'] }}:</div>
                <div class="info-label" style="width: 50%">{{ $fields['contrato_processo']['label'] }}:</div>
            </div>
            <div class="info-row">
                <div class="info-content" style="width: 50%">{{ $fields['amparo_legal']['value'] }}</div>
                <div class="info-content" style="width: 50%">{{ $fields['contrato_processo']['value'] }}</div>
            </div>
        </div>
        <br>

        <div class="info-table">
            <div class="info-row">
                <div class="info-label" style="width: 50%">{{ $fields['preposto']['label'] }}:</div>
                <div class="info-label" style="width: 50%">{{ $fields['gestores']['label'] }}:</div>
            </div>
            <div class="info-row">
                <div class="info-content" style="width: 50%">{!! $fields['preposto']['value'] !!}</div>
                <div class="info-content" style="width: 50%">{!! $fields['gestores']['value'] !!}</div>
            </div>
        </div>
        <br>

        <div class="info-table">
            <div class="info-row">
                <div class="info-label" style="width: 100%">{{ $fields['restrito']['label'] }}:</div>
            </div>
            <div class="info-row">
                <div class="info-content" style="width: 100%">{{ $fields['restrito']['value'] }}</div>
            </div>
        </div>
    </div>
    <br>

    <div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
        <h3 class="titulo-ata" style="font-weight: 500 !important;">INFORMAÇÕES DA AUTORIZAÇÃO DE EXECUÇÃO</h3>

        <div class="info-table">
            <div class="info-row">
                <div class="info-label">{{ $fields['processo']['label'] }}:</div>
                <div class="info-label">{{ $fields['tipo_id']['label'] }}:</div>
                <div class="info-label">{{ $fields['numero']['label'] }}:</div>
            </div>
            <div class="info-row">
                <div class="info-content">{{ $fields['processo']['value'] }}</div>
                <div class="info-content">{{ $fields['tipo_id']['value']['descricao'] }}</div>
                <div class="info-content">{{ $fields['numero']['value'] }}</div>
            </div>
        </div>
        <br>

        <div class="info-table">
            <div class="info-row">
                <div class="info-label">{{ $fields['data_assinatura']['label'] }}:</div>
                <div class="info-label">{{ $fields['data_vigencia_inicio']['label'] }}:</div>
                <div class="info-label">{{ $fields['data_vigencia_fim']['label'] }}:</div>
            </div>
            <div class="info-row">
                <div class="info-content">{{ $fields['data_assinatura']['value']['formated'] }}</div>
                <div class="info-content">{{ $fields['data_vigencia_inicio']['value']['formated'] }}</div>
                <div class="info-content">{{ $fields['data_vigencia_fim']['value']['formated'] }}</div>
            </div>
        </div>
        <br>

        <div class="info-table">
            <div class="info-row">
                <div class="info-label">{{ $fields['numero_sistema_origem']['label'] }}:</div>
            </div>
            <div class="info-row">
                <div class="info-content">{{ $fields['numero_sistema_origem']['value'] }}</div>
            </div>
        </div>
        <br>

        <div class="info-table">
            <div class="info-row">
                <div class="info-label">{{ $fields['unidaderequisitante_ids']['label'] }}:</div>
            </div>
            <div class="info-row">
                <div class="info-content">{!! $fields['unidaderequisitante_ids']['value']['unidades_descricao'] !!}</div>
            </div>
        </div>
        <br>

        <div class="info-table">
            <div class="info-row">
                <div class="info-label">{{ $fields['informacoes_complementares']['label'] }}:</div>
            </div>
            <div class="info-row">
                <div class="info-content">{{ $fields['informacoes_complementares']['value'] }}</div>
            </div>
        </div>
        <br>

        <div class="info-table">
            <div class="info-row">
                <div class="info-label">{{ $fields['empenhos']['label'] }}:</div>
            </div>
            <div class="info-row">
                <div class="info-content">{!! $fields['empenhos']['value']['empenhos_descricao'] !!}</div>
            </div>
        </div>
        <br>
    </div>
    <br>

    <div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
        <h3 class="titulo-ata" style="font-weight: 500 !important;">ITENS DA AUTORIZAÇÃO DE EXECUÇÃO</h3>

        @include('crud::autorizacaoexecucao.table-itens')
    </div>


    <div class="rodape text-center" style="top:88%;">
        @include('components_v2.rodape_relatorio')
    </div>

    <style>
        table.table thead,
        table.table tfoot {
            background-color: #F0F0F0;
            color: #1c466b !important;
        }
    </style>
</body>
