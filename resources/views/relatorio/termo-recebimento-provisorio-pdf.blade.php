@php
    $total = 0;
    $countTopicos = 0;
@endphp

<body>
@include('components_v2.cabecalho_relatorio', ['portrait' => true])
<div class="divisoria"></div>
<br>
{{-- add class    --}}
<div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.6cm;padding-bottom: 0cm;margin-bottom: 0px;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - TERMO DE RECEBIMENTO PROVISÓRIO</h3>
    <div>
        {!! str_replace("\r\n", '<br>', $termoRecebimento->introducao) !!}
    </div>
    <br>

    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - INFORMAÇÕES DO CONTRATO</h3>
    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%;">Contrato:</div>
            <div class="info-label" style="width: 50%;">Fornecedor:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%;">{{ $contrato->numero }}</div>
            <div class="info-content" style="width: 50%;">
                {{ $contrato->fornecedor->cpf_cnpj_idgener }} - {{ $contrato->fornecedor->nome }}
            </div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%">Contratante:</div>
            <div class="info-label">Vigência Inicial:</div>
            <div class="info-label">Vigência Final:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%">{{ $contratante }}</div>
            <div class="info-content">{{ (new \DateTime($contrato->vigencia_inicio))->format('d/m/Y') }}</div>
            <div class="info-content">
                {{ $contrato->vigencia_fim ? (new \DateTime($contrato->vigencia_fim))->format('d/m/Y') : 'Indeterminado' }}
            </div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%">Amparo Legal:</div>
            <div class="info-label" style="width: 50%">Número do processo de contratação:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%">{{ $contrato->retornaAmparo() }}</div>
            <div class="info-content" style="width: 50%">{{ $contrato->processo }}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%">Preposto:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%">{!! $prepostos !!}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 50%">Gestores:</div>
        </div>
        <div class="info-row">
            <div class="info-content" style="width: 50%">{!! $gestores !!}</div>
        </div>
    </div>
    <br>

    <div class="info-table">
        <div class="info-row">
            <div class="info-label" style="width: 100%">Objeto:</div>
        </div>
        <div class="info-row">
            <div class="info-content text-justify" style="width: 100%">{{ $contrato->objeto }}</div>
        </div>
    </div>
</div>
<div class="page-break-always"></div>

<div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - ENTREGAS</h3>

    @foreach($entregas as $entrega)
        <p><strong>DADOS ENTREGA</strong></p>

        <div class="info-table">
            <div class="info-row">
                <div class="info-label">Número/Ano da Entrega:</div>
                <div class="info-label">Data da Entrega:</div>
                <div class="info-label">Valor da Entrega:</div>
            </div>
            <div class="info-row">
                <div class="info-content">{{ $entrega->numero }}</div>
                <div class="info-content">{{ (new DateTime($entrega->data_entrega))->format('d/m/Y') }}</div>
                <div class="info-content">{{ 'R$ ' .$entrega->valor_total }}</div>
            </div>
        </div>
        <br>

        <div class="info-table">
            <div class="info-row">
                <div class="info-label">Previsão para o TRP:</div>
                <div class="info-label">Previsão para o TRD:</div>
                <div class="info-label">Mês/Ano de Referência:</div>
            </div>
            <div class="info-row">
                <div class="info-content">{{ (new DateTime($entrega->data_prevista_trp))->format('d/m/Y') }}</div>
                <div class="info-content">{{ (new DateTime($entrega->data_prevista_trd))->format('d/m/Y') }}</div>
                <div class="info-content">{{ (new DateTime($entrega->mes_ano_referencia))->format('d/m/Y') }}</div>
            </div>
        </div>
        <br>

        <div class="info-table">
            <div class="info-row">
                <div class="info-label">Locais de Execução para Entrega:</div>
            </div>
            <div class="info-row">
                <div class="info-content">{{ $entrega->locaisExecucao()->pluck('descricao')->implode(', ') }}</div>
            </div>
        </div>

        {{--        <div style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">--}}
        <div class="info-table">
            <p><strong>ITENS </strong></p>

            <table class="table table-bordered table-striped m-b-0">
                <thead>
                <tr>
                    <th>OS/Fs</th>
                    <th>Tipo item</th>
                    <th>N.º item</th>
                    <th>Item</th>
                    <th>Unidade de Fornecimento</th>
                </tr>
                </thead>
                <tbody>
                @foreach($itens->where('entregaItem.entrega_id', $entrega->id) as $item)
                    @php
                        $itemInfo = $item->getAutorizacaoExecucaoItem();
                        $valorTotalItem = $item->quantidade_informada * $itemInfo->valor_unitario_sem_formatar - $item->valor_glosa;
                        $total += $valorTotalItem;
                    @endphp
                    <tr>
                        <td>{{ $itemInfo->autorizacaoexecucao->numero }}</td>
                        <td>{{ $itemInfo->tipo_item }}</td>
                        <td>{{ $itemInfo->numero_item_compra }}</td>
                        <td>{{ $itemInfo->item }}</td>
                        <td>{{ $itemInfo->unidadeMedida->nome }}</td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <p style="display: inline-block; margin: 3px 5px"><b>Quantidade Contratada:</b> {{ str_replace('.', ',', (float) $itemInfo->saldohistoricoitem->quantidade) }} </p>
                            <p style="display: inline-block; margin: 3px 5px"><b>Quantidade OS/F:</b> {{ $itemInfo->getQuantidadeTotal() }} </p>
                            <p style="display: inline-block; margin: 3px 5px"><b>Quantidade Solicitada na Entrega:</b> {{ str_replace('.', ',', (float) $item->entregaItem->quantidade_informada) }} </p>
                            <p style="display: inline-block; margin: 3px 5px"><b>Quantidade Informada:</b> {{ str_replace('.', ',', (float) $item->quantidade_informada)}} </p>
                            <p style="display: inline-block; margin: 3px 5px"><b>Valor Glosa:</b> {{  'R$ '. number_format($item->valor_glosa, 2, ',', '.') }} </p>
                            <p style="display: inline-block; margin: 3px 5px"><b>Valor Unitário:</b> {{ 'R$ '. $itemInfo->valor_unitario }} </p>
                            <p style="display: inline-block; margin: 3px 5px"><b>Valor Total:</b> {{  'R$ '. number_format($valorTotalItem, 2, ',', '.') }}</p>


                            @if(!empty($item->mes_ano_competencia))
                                <p style="display: inline-block; margin: 3px 5px"><b>Competência:</b> {{ \Carbon\Carbon::parse($item->mes_ano_competencia)->format('m/Y') }}</p>
                            @endif
                            @if(!empty($item->processo_sei))
                                <p style="display: inline-block; margin: 3px 5px"><b>Processo SEI:</b> {{ $item->processo_sei }}</p>
                            @endif
                            @if(!empty($item->data_inicio))
                                <p style="display: inline-block; margin: 3px 5px"><b>Data Início:</b> {{ \Carbon\Carbon::parse($item->data_inicio)->format('d/m/Y') }}</p>
                            @endif
                            @if(!empty($item->data_fim))
                                <p style="display: inline-block; margin: 3px 5px"><b>Data Fim:</b> {{ \Carbon\Carbon::parse($item->data_fim)->format('d/m/Y') }}</p>
                            @endif
                            @if(!empty($item->horario))
                                <p style="display: inline-block; margin: 3px 5px"><b>Horário:</b> {{ $item->horario }}</p>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>


            <p class="text-justify">
                O Valor Total da entrega do presente Termo de Recebimento Provisório é de R$ {{ number_format($total, 2, ',', '.') }}
                ({{$termoRecebimento->valorPorExtenso($total)}}).
            </p>
        </div>
        <br>
    @endforeach
</div>
<br>

@if ($termoRecebimento->informacoes_complementares)
    <div class="text-justify page-break-avoid" style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
        <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - INFORMAÇÕES COMPLEMENTARES</h3>

        <div>
            {!! str_replace("\r\n", '<br>', $termoRecebimento->informacoes_complementares) !!}
        </div>
    </div>
    <br>
@endif

@if ($anexos->count() > 0)
    <div class="text-justify page-break-avoid" style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
        <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - ANEXOS</h3>

        <table class="table table-bordered table-striped m-b-0">
            <thead>
            <tr>
                <th>Número Entrega</th>
                <th>Arquivo</th>
                <th>Descrição</th>
            </tr>
            </thead>
            <tbody>
            @foreach($anexos as $anexo)
                <tr>
                    <td>{{ $anexo->numero }}</td>
                    <td>
                        <a href="{{url('storage/' . $anexo->url)}}" target="_blank">{{ $anexo->nome }}</a>
                    </td>
                    <td>{{ $anexo->descricao }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <br>
@endif

<div class="page-break-avoid text-justify" style="background: #F8F8F8;padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 0.5cm;">
    <h3 class="titulo-ata" style="font-weight: 500 !important;">{{++$countTopicos}} - RECEBIMENTO PROVISÓRIO</h3>

    <p>
        Por este instrumento, atestamos, que o objeto descrito no item 4 do presente documento, correspondentes à
        entrega {{$termoRecebimento->numero}} do Contrato {{ $contrato->numero }}, foram recebidos provisoriamente na
        presente data e serão objeto de avaliação por parte da CONTRATANTE quanto à adequação da entrega às condições
        contratuais, de acordo com os Critérios de Aceitação previamente definidos no Modelo de Gestão do contrato.
    </p>

    <p>
        Ressaltamos que o recebimento definitivo destes serviços/produtos ocorrerá após a verificação dos requisitos e
        demais condições contratuais, desde que não se observem inconformidades ou divergências quanto às especificações
        constantes do Termo de Referência e do Contrato acima identificado que ensejem correções por parte da
        <b>CONTRATADA</b>.
    </p>
</div>
<br>


<div style="padding-left: 0.6cm;padding-right: 0.6cm;padding-top: 0.3cm;padding-bottom: 2cm;">
    <div class="w-100 assign-session">
        @foreach($assinantes as $assinante)
            <div class="page-break-avoid">
                <i>Documento assinado eletronicamente</i>
                <p>
                    {{ $assinante->user ? $assinante->user->name : $assinante->nome }}<br>
                    {{ $assinante->funcao ? $assinante->funcao->descricao : 'Preposto' }}<br>
                </p>
            </div>
        @endforeach
    </div>
</div>

<div class="rodape text-center" style="top:88%;">
    @include('components_v2.rodape_relatorio')
</div>

<style>
    table.table thead,
    table.table tfoot
    {
        background-color: #F0F0F0;
        color: #1c466b !important;
    }
    table.table td,
    table.table thead,
    table.table tfoot {
        font-size: 13px  !important;
    }
    .assign-session {
        width: 100%;
        font-size: 14px;
        text-align: center;
        margin-top: 30px;
    }
    .text-justify {
        text-align: justify;
    }
    .page-break-avoid {
        page-break-inside: avoid;
    }
    .page-break-always {
        page-break-before: always;
    }

    .padroniza-tinymce * {
        font-family: 'Rawline', sans-serif;
        font-size: 16px !important;
        text-align: justify !important;
    }
</style>
</body>
