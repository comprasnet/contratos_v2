@extends(backpack_view('layouts.top_left_transparencia'))

@php

    $breadcrumbs = [
        trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'transparencia'),
        trans('Visualizar') => false,
    ];

@endphp
<br>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<link rel="stylesheet" href="{{ asset('packages/select2/dist/css/') }}/select2.css"/>
<script src="{{ asset('packages/select2/dist/js/') }}/select2.js" defer></script>
<link href="{{ asset('packages/backpack/base/css') }}/bundle.css" rel="stylesheet"/>
<link href="{{ asset('packages/backpack/base/css') }}/blue-bundle.css" rel="stylesheet"/>
<link href="{{ asset('packages/backpack/base/css') }}/padrao_gov/all.min.css" rel="stylesheet"/>
<link href="{{ asset('packages/backpack/base/css') }}/padrao_gov/core.min.css" rel="stylesheet"/>
<link href="{{ asset('packages/backpack/base/css') }}/padrao_gov/rawline.css" rel="stylesheet"/>
<link href="{{ asset('packages/backpack/base/css') }}/padrao_gov/customize.css" rel="stylesheet"/>
<script src="{{ asset('packages/backpack/base/js/') }}/bundle.js"></script>
<script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/core-init.js"></script>
<script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/jquery-3.6.1.min.js"></script>
<script src="{{ asset('packages/backpack/base/js/padrao_gov/') }}/jquery.mask.js"></script>

<link rel="stylesheet" href="{{ asset('packages/select2/dist/css/') }}/select2.css"/>

<script src="{{ asset('assets/js/admin/forms/filtro_ata.js') }} "></script>

<link rel="stylesheet" href="{{ asset('packages/backpack/base/css/filtro_ata/filtro_ata.css?nc=5') }}"/>
@section('after_styles')
    <!-- DATA TABLES -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>


    <!-- CRUD LIST CONTENT - crud_list_styles stack -->
    @stack('crud_list_styles')
@endsection
@section('after_scripts')
    {{--    @include('crud::inc.datatables_logic') --}}
    <script type="text/javascript" src="{{ asset('packages/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('packages/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}">
    </script>
    <script type="text/javascript"
            src="{{ asset('packages/datatables.net-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script type="text/javascript"
            src="{{ asset('packages/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('packages/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('packages/datatables.net-fixedheader-bs4/js/fixedHeader.bootstrap4.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('packages/datatables.net/js/ellipses.js') }}"></script>



    <script>
        $(function() {

            let form = $('#form-arp-item'); // modal_ata_itens.blade.php

            let searchParams = new URLSearchParams(window.location.search);

            let hasFilters = form.serializeArray().some(function (item) {
                return item.value.trim() !== '' && item.name !== 'status';
            }) || searchParams.getAll('unidades[]').length > 0 || searchParams.getAll('fornecedorAta[]').length > 0;



            let loggedUserId = {!! auth()->user()->id !!};
            let searchTimeout;
            if(hasFilters) {
                $('#itens').DataTable({
                    searchDelay: 2000,
                    language: {
                        "sEmptyTable": "Nenhum registro encontrado",
                        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "_MENU_ Resultados por página",
                        "sLoadingRecords": "Carregando...",

                        "sProcessing":     "<img src='{{ asset('packages/backpack/base/img/spinner.svg') }}' alt='{{ trans('backpack::crud.processing') }}'>",
                        "sZeroRecords": "Nenhum registro encontrado",
                        "sSearch": "Pesquisar",
                        "oPaginate": {
                            "sNext": "Próximo",
                            "sPrevious": "Anterior",
                            "sFirst": "Primeiro",
                            "sLast": "Último"
                        },
                        "oAria": {
                            "sSortAscending": ": Ordenar colunas de forma ascendente",
                            "sSortDescending": ": Ordenar colunas de forma descendente"
                        }
                    },
                    processing: true,
                    serverSide: true,
                    searching: true,

                    ajax: function(data, callback, settings) {
                        clearTimeout(searchTimeout);

                        let searchValue = data.search.value.trim();
                        // Sim é para sempre cair nesse if, a ideia é o primeiro digito aguardar 2 segundos

                        if (searchValue.length >= 0) {

                            let camposFiltro = form.serializeArray();


                            // Obtém os valores da consulta na URL para 'fornecedorAta'
                            let searchParams = new URLSearchParams(window.location.search);
                            let fornecedorAtaValues = searchParams.getAll('fornecedorAta[]');

                            // Verifica se existem valores para 'fornecedorAta' e adiciona ao camposFiltro como um array
                            if (fornecedorAtaValues.length > 0) {
                                camposFiltro.push({
                                    name: 'fornecedor_ata',
                                    value: fornecedorAtaValues
                                });
                            }

                            // Obtém os valores da consulta na URL para 'unidades'
                            let unidadesValues = searchParams.getAll('unidades[]');

                            // Verifica se existem valores para 'unidades' e adiciona ao camposFiltro como um array
                            if (unidadesValues.length > 0) {
                                camposFiltro.push({
                                    name: 'unidades',
                                    value: unidadesValues
                                });
                            }

                            // Adiciona o formData ao objeto de dados
                            data.camposFiltro = camposFiltro;
                            data.user_id = loggedUserId;



                            searchTimeout = setTimeout(() => {
                                $.ajax({
                                    "url": "{!! route('transparencia.arp-item.ajax') !!}",
                                    "type": "POST",
                                    "data": data,
                                    "success": function(response) {
                                        callback(response);
                                    },
                                    error: function (xhr, status, error) {
                                        var message = 'Ocorreu um erro inesperado. Por favor, tente novamente.';
                                        if (xhr.responseJSON && xhr.responseJSON.error) {
                                            message = xhr.responseJSON.error;
                                        }
                                        exibirAlertaNoty('info-custom', message);
                                    }
                                });
                            }, 2000);
                        }
                    },
                    {{-- ajax: '{!! route('transparencia.arp-item.ajax') !!}', --}}
                    columns: [{
                        data: 'numero',
                        name: 'numero'
                    },
                        {
                            data: 'unidade_gerenciadora',
                            name: 'unidade_gerenciadora'
                        },
                        {
                            data: 'numero_item_compra',
                            name: 'numero_item_compra'
                        },
                        {
                            data: 'codigo_pdm',
                            name: 'codigo_pdm'
                        },
                        {
                            data: 'descricaodetalhada',
                            name: 'descricaodetalhada'
                        },
                        {
                            data: 'unidade_federacao',
                            name: 'unidade_federacao'
                        },
                        {
                            data: 'fornecedor',
                            name: 'fornecedor'
                        },
                        {
                            data: 'quantidade_registrada',
                            name: 'quantidade_registrada'
                        },
                        {
                            data: 'saldo_adesao',
                            name: 'saldo_adesao'
                        },
                        {
                            data: 'vigencia_inicial',
                            name: 'vigencia_inicial',
                            render: function (data) {
                                var date = moment(data);
                                return date.format('DD/MM/YYYY');
                            }
                        },
                        {
                            data: 'vigencia_final',
                            name: 'vigencia_final',
                            render: function (data) {
                                var date = moment(data);
                                return date.format('DD/MM/YYYY');
                            }
                        },
                        {
                            data: 'acao',
                            name: 'acao',
                            render: function (data, type, row, meta) {
                                var link = '/transparencia/arpshow/itens/' + row.id_item + '/' + row
                                    .id_ata_rp + '/show';
                                return '<a href="' + link + '">' + data + '</a>';
                            }
                        }
                        /*{ data: 'name', name: 'name' },
                        { data: 'email', name: 'email' },
                        { data: 'created_at', name: 'created_at' },
                        { data: 'updated_at', name: 'updated_at' }*/
                    ]
                });
            }else{
                exibirAlertaNoty('info-custom', 'Para listar itens, preencha o campo pesquisar ou os campos de busca avançada');
            }
        });

    </script>


    <script>
        // Obtém o parâmetro "status" da URL
        const urlParams = new URLSearchParams(window.location.search);
        const status = urlParams.get('status');

        // Se o parâmetro "status" estiver presente, seleciona o radio button correspondente
        if (status) {
            const statusRadio = document.querySelector(`input[name="status"][value="${status}"]`);
            if (statusRadio) {
                statusRadio.checked = true;
            }
        }
    </script>
    <script>
        function limparCampos() {
            var form = document.getElementById("form-arp-item");
            var inputs = form.getElementsByTagName("input");
            var selects = form.getElementsByTagName("select");
            for (var i = 0; i < inputs.length; i++) {
                inputs[i].value = "";
            }
            for (var i = 0; i < selects.length; i++) {
                selects[i].selectedIndex = -1;
            }
        }
    </script>

    <!-- CRUD LIST CONTENT - crud_list_scripts stack -->
    {{--    @stack('crud_list_scripts') --}}
@endsection
@section('content')
    @include('components_v2.filtro_auto_busca_ata_items')
    <br><br><br>

    <div class="margin-top-50 text-center">
        <h3>Resultados</h3>
    </div>
    <div class="table-responsive">
        <table id="itens" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Número da ata</th>
                <th>Unidade gerenciadora</th>
                <th>Número do item</th>
                <th>Código PDM</th>
                <th>Descrição do item</th>
                <th>Unidade federação</th>
                <th>Fornecedor</th>
                <th>Quantidade registrada</th>
                <th>Saldo para adesões</th>
                <th>Início vigência</th>
                <th>Fim vigência</th>
                <th>Ação</th>
            </tr>
            </thead>
        </table>
    </div>
    <br>
    @include('components_v2.modal_ata_itens', ['esferas' => $esferas])
@endsection
<!-- /.content-header -->
