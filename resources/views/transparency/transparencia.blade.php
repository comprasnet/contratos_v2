
@extends(backpack_view('layouts.top_left'))
@php
    $breadcrumbs = [
        trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'transparencia'),
        trans('Visualizar') => false,
        // 'Icone' => $dadosIcone
    ];
@endphp

@section('content')

{{--    @include('components_v2.filtro_auto_busca_ata')--}}
    <br><br><br>

    <div class="margin-top-50 text-center"><h3> Resultados</h3></div>

    @include('components_v2.resultado_filtro_ata')
    <br>
    @include('components_v2.modal_ata')




@endsection
@push('after_scripts')


    <script src="/assets/js/admin/forms/arp_pdf.js">
    </script>
@endpush
<script>
    function limparCampos() {
        var form = document.getElementById("form-arp");
        var inputs = form.getElementsByTagName("input");
        var selects = form.getElementsByTagName("select");
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].value = "";
        }
        for (var i = 0; i < selects.length; i++) {
            selects[i].selectedIndex = -1;
        }
    }

</script>

<!-- /.content-header -->


