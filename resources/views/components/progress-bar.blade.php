@php
$classProgressBar = 'success';
if ($percentual >= 75 && $percentual < 90) {
    $classProgressBar = 'warning';
} elseif ($percentual >= 90) {
    $classProgressBar = 'danger';
}
@endphp

@once
    @push('crud_fields_styles')
        <link rel="stylesheet" href="/assets/css/autorizacaoexecucao/progress-bar.css">
    @endpush

    @push('after_scripts')
        <script>
            $(document).ready(function () {
                $('.progress-saldo progress').on('change', function () {
                    const percentual = $(this).val()
                    let classProgressBar = 'success';
                    if (percentual >= 75 && percentual < 90) {
                        classProgressBar = 'warning';
                    } else if (percentual >= 90) {
                        classProgressBar = 'danger';
                    }

                    $(this).closest('.progress-saldo').removeClass('success')
                        .removeClass('warning')
                        .removeClass('danger')
                        .addClass(classProgressBar)
                })
            })
        </script>
    @endpush
@endonce

<div {{ !empty($id) ? "id='$id'" : null }} class="progress-saldo d-flex align-items-end {{ $classProgressBar }}" style="text-align: center;">
    <div>
        <span class="pct">{{ number_format($percentual, 2, ',', '.') }}%</span>
        <progress value="{{ $percentual }}" max="100"></progress>
    </div>
    @isset($tooltip)
        <i class="fas fa-info-circle"></i>

        {{ $tooltip }}
    @endisset
</div>
