<br>
<br>
<script>
    function atualizaStatus(status) {
        document.getElementById('status_hidden').value = status.value;
    }
</script>
<section class="grid">
    <form action="/transparencia/arp-item" method="GET" id="form_simples">
        <div class="row">
            <div class="col-lg-6 mt-3">
                <div class="form-group">
                    <div class="br-input">
                        <label for="palavra_chave">Palavra-chave</label>
                        <input onchange="document.getElementById('palavra_chave_hidden').value = this.value" type="text" class="form-control" id="palavra_chave" name="palavra_chave" placeholder="Digite uma unidade gerencidora, número da compra, numero da ata, código do item, descrição do item, ou órgão para pesquisar" value="{{$filter['palavra_chave']}}">
                    </div>
                </div>
            </div>
            <div class="col-lg-6  mt-3">
                <div class="form-group">
                    <label for="status">Status:</label>
                    <div class="form-check">
                        <input onchange="atualizaStatus(this)" class="form-check-input" type="radio" name="status" id="vigente" value="vigente" checked>
                        <label class="form-check-label" for="vigente">
                            Vigente
                        </label>
                    </div>
                    <div class="form-check">
                        <input onchange="atualizaStatus(this)" class="form-check-input" type="radio" name="status" id="todos" value="todos">
                        <label class="form-check-label" for="todos">
                            Todos
                        </label>
                    </div>
                    <div class="form-check">
                        <input onchange="atualizaStatus(this)" class="form-check-input" type="radio" name="status" id="nao_vigente" value="nao_vigente">
                        <label class="form-check-label" for="nao_vigente">
                            Não vigentes
                        </label>
                    </div>

                </div>
            </div>

            <div class="col-lg-6 mt-3">
                <div class="d-flex flex-wrap">
                    <button class="br-button mr-3 mb-3" type="button" onclick="document.getElementById('palavra_chave').value=''">
                        <span data-value="save_and_back">Limpar</span>
                    </button>

                    <button class="br-button primary mr-3 mb-3" type="submit">
                        <span class="la la-search" role="presentation" aria-hidden="true" style="vertical-align: middle;"></span>
                        &nbsp;
                        <span data-value="save_and_back" onclick="$('form_simples').submit()">Pesquisar</span>
                    </button>

                    <button type="button" class="br-button primary mb-3" data-toggle="modal" data-target="#filtroModal">
                        <span data-value="save_and_back">Busca avançada</span>
                    </button>
                </div>
            </div>
        </div>
    </form>
</section>