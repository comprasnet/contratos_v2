<div class="divisoria-rodape"></div>
<table>
    <th>
        <td class="logo-rodape">
            <img src="data:image/png;base64,{{base64_encode(file_get_contents(public_path('img/logo_relatorio.png')))}}"
                 alt="logo"
                 width="90"
            >
        </td>

        <td class="col-md-6 d-flex" style="padding-left: 15px;padding-top: 15px !important;">
            <div>
                <h3 class="titulo-relatorio-rodape" style="font-weight: 400 !important;">
                    @if(isset($titulo))
                        {{ $titulo }}
                        @if(isset($subtitulo))
                            <br>
                            {{ $subtitulo }}
                        @endif
                    @else
                        Relatório Ata de Registro de Preços
                    @endif
                </h3>
                <p class="subtitulo-rodape">
                    @if(isset($unidadeGerenciadora))
                        {{ $unidadeGerenciadora }}
                    @else
                        Unidade Gerenciadora {{$arpDados->unidade_gerenciadora_titulo}}
                    @endif
                </p>
                {{--<p class="subtitulo-rodape">Relatório Gerado em {{ \Carbon\Carbon::now()->format('d/m/Y H:i:s') }}</p>--}
                 {{-- <p class="texto-baixo-rodape">SOLICITAÇÃO DE TITULAÇÃO DE ASSENTAMENTO</p>--}}
            </div>
        </td>
    </th>
</table>

<style>
    /* Estilos para o rodapé */
    .rodape {
        text-align: center;
        position: absolute;
        bottom: 0;
        width: 100%;
        padding: 10px 0;
    }

    .logo-rodape {
        margin-top: 95px;
        vertical-align: middle;
    }

    .divisoria-rodape {
        width: 100%;
        height: 2px; /* Altura da borda */
        background-color: #CCC; /* Cor da borda */
        margin-top: 0px; /* Espaçamento acima da borda */
    }

    .titulo-relatorio-rodape {
        margin-bottom: 5px;
        line-height: 1.2;
        font-size: 16px;
    }

    .subtitulo-rodape {
        margin-top: 0;
        margin-bottom: 5px;
        opacity: 0.7;

        font-size: 14px;
    }

    .texto-baixo-rodape {
        margin-top: 0;
        font-weight: 400;
        position: relative;
        padding-bottom: 20px;
        font-size: 13px;
    }
</style>
