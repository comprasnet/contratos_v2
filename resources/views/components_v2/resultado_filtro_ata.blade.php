<script>
    var usarRecaptcha = @json(env('HABILITAR_RECAPTCHA'));

    function changeOrder(val) {

        let queryString = window.location.search;  // get url parameters
        let params = new URLSearchParams(queryString);  // create url search params object
        params.delete('order');  // delete order parameter if it exists, in case you change the dropdown more than once
        params.append('order', val); // add selected order
        document.location.href = "?" + params.toString(); // refresh the page with new url

    }

</script>
@if(env('HABILITAR_RECAPTCHA'))
    <script src="https://www.google.com/recaptcha/api.js?render={{ env('RECAPTCHA_SITE_KEY') }}"></script>
@endif

{{--<div class="col-lg-3 offset-9 d-flex">--}}
{{--    <h7 class="tile-filtro-results mt-2 mr-2">Ordenar: </h7>--}}
{{--    <select class="form-control"  id="order" name="order" onchange="changeOrder(this.value)">--}}
{{--        <option style="display:none" {{ $orderBy == 'inicio' ? 'selected' : '' }} value="inicio"></option>--}}
{{--        <option {{ $orderBy == 'recente' ? 'selected' : '' }} value="recente">Mais Recentes</option>--}}
{{--        <option {{ $orderBy == 'antigo' ? 'selected' : '' }} value="antigo">Mais Antigos</option>--}}
{{--    </select>--}}
{{--</div>--}}

<br><br><br>
@if(!empty($arps))
    @forelse($arps as $arp)
        <div class="number-ata">
            <nav class="d-flex">
                <ul class="col-lg-8 value-filtros">
                    <li class="titulo ata-filtro">
                        <strong>Ata &nbsp; </strong>
                        nº&nbsp; {{$arp->numero}}/{{$arp->ano}}
                    </li>
                    <li class="titulo atualizacao">
                        <strong>Última Atualização {{$arp->updated_at->format('d/m/Y')}}</strong>
                    </li>

                    <li class="titulo link-pncp">
                        <strong>Link da ata no PNCP: </strong>
                        <a href="{{$arp->getLinkPNCP()}}" target="_blank">{{$arp->getLinkPNCP()}}</a>
                    </li>
                    <li class="titulo link-pncp">
                        <strong>Link da compra no PNCP: </strong>
                        <a href="{{$arp->getLinkCompraPncp()}}" target="_blank">{{$arp->getLinkCompraPncp()}}</a>
                    </li>
                    <li class="titulo unid-gereneciadora">
                        <strong>
                            Unidade gerenciadora: &nbsp;
                        </strong>
                        {{$arp->getUnidadeOrigem()}}
                    </li>

                    <li class="titulo vigencia">
                        <strong>
                            Órgão:&nbsp;
                        </strong>
                        {{$arp->unidades->getOrgao()}}

                    </li>

                    <li class="titulo vigencia">
                        <strong>
                            Vigência:&nbsp;
                        </strong>
                        de {{date("d/m/Y", strtotime($arp->vigencia_inicial))}} a {{date("d/m/Y", strtotime($arp->vigencia_final))}}
                    </li>

                    <li class="titulo objetivo">
                        <strong>
                            Objeto: &nbsp;
                        </strong>
                        {{$arp->objeto}}
                    </li>
                    <li class="titulo unid-gereneciadora">
                        <strong>
                            Fornecedor: &nbsp;
                        </strong>
                        @foreach ($arp->getFornecedorPorAtaTransparencia() as $fornecedor)
                            {{$fornecedor}}<br>
                        @endforeach
                    </li>

                </ul>
                <ul class="mt-5 col-lg-5 value-filtros">
                    <li class="titulo valor-global">
                        <strong>
                            Valor  Contratado: &nbsp;
                        </strong>
                        R$ {{number_format($arp->valor_total,2,',','.')}}
                    </li>
                    <li class="col-lg-5 offset-7 titulo mt-5">
                        <a href="/transparencia/arpshow/{{$arp->id}}/show">
                            <i class="fas fa-chevron-right"></i>
                        </a>
                        <div class="br-tooltip text-justify" role="tooltip" info="info" place="top"
                             id="{{ $arp->id }}">
                            <span class="subtext">Acessar</span>
                        </div>

                    </li>

                    <form action="/transparencia/disparar-job-pdf/{{$arp->id}}" method="POST" id="recaptcha-form">
                        @csrf
                        <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response" data-sitekey="{{ env('RECAPTCHA_SITE_KEY') }}">

                        <li class="col-lg-5 offset-7 titulo mt-5">

                            @if ($arp->arpPdf && $arp->arpPdf->gerado && Storage::disk('local_root')->exists($arp->arpPdf->caminho_pdf) && $arp->arpPdf->created_at->diffInHours() < 24)
                                <div class="ml-0">
                                    <!-- Botão de download -->
                                    <a href="{{ route('visualizar.pdf', ['id' => $arp->id,'tipo' =>'download']) }}"
                                       id="download-pdf-{{ $arp->id }}">
                                        <i class="fas fa-download" style="font-size: 22px !important;"></i>
                                    </a>
                                    <div class="br-tooltip text-justify" role="tooltip" info="info" place="top"
                                         id="{{ $arp->id }}">
                                        <span class="subtext">Download</span>
                                    </div>
                                </div>
                                <div class="ml-0 mt-5">
                                    <!-- Botão de visualização -->
                                    <a href="{{ route('visualizar.pdf', ['id' => $arp->id, 'tipo' => 'visualizar']) }}"
                                       target="_blank" data-arp-id="{{ $arp->id }}"
                                       id="visualizar-pdf-{{ $arp->id }}">
                                        <i class="fas fa-eye" style="font-size: 22px !important;"></i>
                                    </a>
                                    <div class="br-tooltip text-justify" role="tooltip" info="info" place="top"
                                         id="{{ $arp->id }}">
                                        <span class="subtext">Visualizar PDF</span>
                                    </div>
                                </div>
                            @else



                                <a href="#" class="ml-0 gerar-pdf pdf-spinner" data-arp-id="{{ $arp->id }}"
                                   data-pdf-url="{{ route('visualizar.pdf', ['id' => $arp->id,'tipo' =>'visualizar']) }}">
                                    <i class="fas fa-file-pdf" style="font-size: 22px !important;"></i>
                                </a>

                                <div class="br-tooltip text-justify" role="tooltip" info="info" place="top"
                                     id="{{ $arp->id }}">
                                    <span class="subtext">Gerar Pdf</span>
                                </div>
                            @endif
                        </li>

                    </form>


                </ul>
            </nav>
        </div>
        <br>
    @empty
        <div class="number-ata">
            <span>Nenhum registro encontrado...</span>
        </div>
    @endforelse
    @if ($arps instanceof \Illuminate\Pagination\LengthAwarePaginator)
        <ul class="br-pagination large mt-5" aria-label="Paginação de resultados" data-total="{{ $arps->total() }}" data-current="{{ $arps->currentPage() }}">
            <ul>
                @if($arps->previousPageUrl())
                    <li>
                        <a class="br-button circle" aria-label="Primeira página" href="{{ $arps->currentUrl }}&page=1">
                            <i class="fas fa-angle-left" aria-hidden="true"></i>
                        </a>
                    </li>
                    <li><a class="page" href="{{ $arps->currentUrl }}&page={{ $arps->currentPage() - 1 }}">{{ $arps->currentPage() - 1 }}</a></li>
                @endif
                <li><a class="page active" href="#">{{ $arps->currentPage() }}</a></li>
                @if($arps->nextPageUrl())
                        <li><a class="page" href="{{ $arps->currentUrl }}&page={{ $arps->currentPage() + 1 }}">{{ $arps->currentPage() + 1 }}</a></li>
                        <li>
                        <a class="br-button circle" aria-label="Página seguinte" href="{{ $arps->currentUrl}}&page={{$arps->lastPage()}}">
                            <i class="fas fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </li>
                @endif
            </ul>
        </ul>
    @endif

@else
    <div class="number-ata">
        <span>Nenhum registro encontrado. Favor realizar a busca avançada</span>
    </div>
@endif


<script>

</script>

{{--
<!-- Modal com reCAPTCHA -->
<div class="modal fade br-scrim-util foco" id="recaptchaModal" tabindex="-1" role="dialog" aria-labelledby="recaptchaModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="recaptchaModalLabel">Validação reCAPTCHA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Coloque o código do reCAPTCHA aqui -->
                <div id="recaptcha-container">
                    @if(env('HABILITAR_RECAPTCHA'))
                        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                        <div class="g-recaptcha" data-sitekey="6LfP0XooAAAAAA__Ha0AtqEz7hUdW0KGpybabat3"></div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button  class="br-button secondary" type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <!-- Botão para continuar após a validação reCAPTCHA -->
                <button type="button" class="br-button primary continuar-apos-recaptcha">Continuar</button>
            </div>
        </div>
    </div>
</div>
--}}
