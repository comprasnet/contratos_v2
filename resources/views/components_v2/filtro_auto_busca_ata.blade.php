<br>
<br>
<section class="grid">
    <form action="/transparencia/arp" method="GET" id="form_simples">
       <div class="row">
            <div class="col-lg-6 mt-3">
                <div class="form-group">
                    <div class="br-input">
                        <label for="palavra_chave">Palavra-chave</label>
                        <input onchange="document.getElementById('palavra_chave_hidden').value = this.value" type="text" class="form-control" id="palavra_chave" name="palavra_chave"  placeholder="Digite uma unidade gerencidora, número da compra, numero da ata ou órgão para pesquisar" value="{{$filter['palavra_chave']}}">
                    </div>
                </div>
            </div>
           <div class="col-lg-6  mt-3">
               <div class="form-group">
                   <label for="status">Status:</label>
                   <div class="form-check">
                       <input class="form-check-input" type="radio" name="status" id="vigente" value="vigente">
                       <label class="form-check-label" for="vigente">
                           Vigente
                       </label>
                   </div>
                   <div class="form-check">
                       <input class="form-check-input" type="radio" name="status" id="todos" value="todos" checked>
                       <label class="form-check-label" for="todos">
                           Todos
                       </label>
                   </div>
                   <div class="form-check">
                       <input class="form-check-input" type="radio" name="status" id="nao_vigente" value="nao_vigente">
                       <label class="form-check-label" for="nao_vigente">
                           Não vigentes
                       </label>
                   </div>

               </div>
           </div>

            <div class="col-lg-6"></div>
            <div class="col-lg-6 mt-5">
                <button class="br-button mr-3" type="button" onclick="document.getElementById('palavra_chave').value=''">
                    <span data-value="save_and_back">Limpar</span>
                </button>

                <button class="br-button primary mr-3" type="submit">
                    <span class="la la-search" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value="save_and_back" onclick="$('form_simples').submit()">Pesquisar</span>
                </button>

                <button type="button" class="br-button primary mr-3"  data-toggle="modal" data-target="#filtroModal">
                    <span data-value="save_and_back">Busca avançada</span>
                </button>
            </div>
         </div>
    </form>
</section>
<script>
    // Obtém o parâmetro "status" da URL
    const urlParams = new URLSearchParams(window.location.search);
    const status = urlParams.get('status');

    // Se o parâmetro "status" estiver presente, seleciona o radio button correspondente
    if (status) {
        const statusRadio = document.querySelector(`input[name="status"][value="${status}"]`);
        if (statusRadio) {
            statusRadio.checked = true;
        }
    }

</script>