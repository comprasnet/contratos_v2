<script>
    function limparCampos() {
        var form = document.getElementById("form-arp-item");
        var inputs = form.getElementsByTagName("input");
        var selects = form.getElementsByTagName("select");
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].value = "";
        }
        for (var i = 0; i < selects.length; i++) {
            selects[i].selectedIndex = -1;
        }
    }
</script>


<div class="modal fade" id="filtroModal" tabindex="-1" role="dialog" aria-labelledby="filtroModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content shadow">
            <div class="modal-body">
                <section class="grid-filtro-lateral-v2">
                    <h4 class="font-weight-bold ml-4">Filtros</h4>
                    <form action="/transparencia/arp-item" method="GET" id="form-arp-item">
                        <div class="row">
                            <input type="hidden" id="palavra_chave_hidden" name="palavra_chave"
                                   value="{{ $filter['palavra_chave'] }}">
                            <input type="hidden" id="status_hidden" name="status"
                                   value="{{ $filter['status'] }}">
                            <input name="codigoUnidade" id="codigoUnidade" type="hidden"
                                   value="{{ $filter['codigo_unidade'] }}" />
                            <input name="modalidadeCompra" id="modalidadeCompra" type="hidden"
                                   value="{{ $filter['modalidade_compra'] }}" />
                            <div class="col-lg-4 form-group">
                                <div class="br-datetimepicker" data-mode="single" data-type="text">
                                    <div class="br-input has-icon">
                                        <label for="simples-input">Data de vigência inicial da ata</label>
                                        <input name="dataInicio" id="dataInicio" type="text" placeholder="dd/mm/aaaa"
                                               data-input="data-input" value="{{ $filter['data_vigencia_inicio'] }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="br-datetimepicker" data-mode="single" data-type="text">
                                    <div class="br-input has-icon">
                                        <label for="simples-input">Data de vigência final da ata</label>
                                        <input name="dataFim" id="dataFim" type="text" placeholder="dd/mm/aaaa"
                                               data-input="data-input" value="{{ $filter['data_vigencia_fim'] }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="br-input">
                                    <label for="input-icon numeroAta">Número da ata</label>
                                    <div class="input-group">
                                        <input name="numeroAta" id="input-icon numeroAta" type="number"
                                               placeholder="Número da Ata" value="{{ $filter['numero_ata'] }}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 form-group">
                                <div class="br-input">
                                    <label for="input-icon numeroCompra">Número da compra</label>
                                    <div class="input-group">
                                        <input name="numeroCompra" id="input-icon numeroCompra" type="number"
                                               placeholder="Número da Compra" value="{{ $filter['numero_compra'] }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="br-input">
                                    <label for="input-icon anoCompra">Ano da compra</label>
                                    <div class="input-group">
                                        <input name="anoCompra" id="input-icon anoCompra" type="number"
                                               placeholder="Ano da Compra" value="{{ $filter['ano_compra'] }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="br-input large input-button">
                                    <label for="input-search-large">Unidade gerenciadora</label>
                                    <div class="input-group">
                                        <select name="unidades[]" class="select2" data-ajax="true"
                                                data-placeholder="Selecione uma unidade gerenciadora"
                                                style="width: 100%;" id="unidades_gerenciadoras" multiple="multiple">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 form-group">
                                <div class="br-input">
                                    <label for="input-icon codigoItem">Código do item</label>
                                    <div class="input-group">
                                        <input name="codigoItem" id="input-icon codigoItem" type="number"
                                               placeholder="Código do item" value="{{ $filter['codigo_item'] }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="br-input">
                                    <label for="input-icon descricaoItem">Descrição do item</label>
                                    <div class="input-group">
                                        <input name="descricaoItem" id="input-icon descricaoItem" type="text"
                                               placeholder="Descrição do item"
                                               value="{{ $filter['descricao_item'] }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="br-input">
                                    <label for="input-icon numeroItemCompra">Número do item</label>
                                    <div class="input-group">
                                        <input name="numeroItemCompra" id="input-icon numeroItemCompra"
                                               type="text" placeholder="Número do item"
                                               value="{{ $filter['numero_item_compra'] }}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 form-group">
                                <div class="br-input">
                                    <label for="input-icon esfera">Esfera</label>
                                    <div class="input-group">
                                        <select name="esfera" id="select-esfera" class="form-control"
                                                style="margin-top: 4px; height: 40px">
                                            <option value=""></option>
                                            @foreach ($esferas as $esfera)
                                                <option value="{{ $esfera }}"
                                                        @if ($filter['esfera'] == $esfera) selected @endif>
                                                    {{ $esfera }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 form-group">
                                <div class="br-input">
                                    <label for="input-icon uf">Unidade da federação</label>
                                    <div class="input-group">
                                        <input name="uf" id="input-icon uf" type="text"
                                               placeholder="Unidade da federação" value="{{ $filter['uf'] }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="br-input">
                                    <label for="input-icon municipio">Município</label>
                                    <div class="input-group">
                                        <input name="municipio" id="input-icon municipio" type="text"
                                               placeholder="Município" value="{{ $filter['municipio'] }}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 form-group">
                                <div class="br-input">
                                    <label for="input-icon codigoPdm">Código PDM</label>
                                    <div class="input-group">
                                        <input name="codigoPdm" id="input-icon codigoPdm" type="number"
                                               placeholder="Código PDM"
                                               value="{{ $filter['codigo_pdm'] }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="br-input">
                                    <label for="input-icon descricaoPdm">Descrição PDM</label>
                                    <div class="input-group">
                                        <input name="descricaoPdm" id="input-icon descricaoPdm" type="text"
                                               placeholder="Descrição PDM" value="{{ $filter['descricao_pdm'] }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="br-input large input-button">
                                    <label for="input-search-large fornecedorAta">Fornecedor da ata</label>
                                    <div class="input-group">
                                        <select name="fornecedorAta[]" class="select2" data-ajax="true"
                                                data-placeholder="Selecione um fornecedor da ata" style="width: 100%;"
                                                id="fornecedor_ata" multiple="multiple">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 form-group">
                                <div class="btn-group" role="group">
                                    <button class="br-button primary mr-3" type="submit">
                                        <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                                        <span data-value="save_and_back">Aplicar</span>
                                    </button>
                                    <button class="br-button mr-3" type="button" onclick="limparCampos()">
                                        <span data-value="save_and_back">Limpar</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</div>