<div class="modal fade" id="filtroModal" tabindex="-1" role="dialog" aria-labelledby="filtroModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content shadow">

            <div class="modal-body">
                <section class="grid-filtro-lateral-v2">
                    <h4 class="font-weight-bold ml-4">Filtros</h4>
                    <form action="/transparencia/arp" method="GET"  id="form-arp">
                        <div class="d-flex">
                            <input type="hidden" id="palavra_chave_hidden" name="palavra_chave" value="{{ $filter['palavra_chave'] }}">
                            <input type="hidden" id="status_hidden" name="status" value="{{ $filter['status'] }}">

                            <div class="col-lg-6 form-group">
                                <div class="br-datetimepicker" data-mode="single" data-type="text">
                                    <div class="br-input has-icon">
                                        <label for="simples-input">Data de vigência da ata (inicio)</label>
                                        <input name="dataInicio" id="dataInicio" type="text" placeholder="dd/mm/aaaa" data-input="data-input" value="{{ $filter['data_vigencia_inicio'] }}"/>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 form-group">
                                <div class="br-datetimepicker" data-mode="single" data-type="text">
                                    <div class="br-input has-icon">
                                        <label for="simples-input">Data de vigência da ata (fim)</label>
                                        <input name="dataFim" id="dataFim" type="text" placeholder="dd/mm/aaaa" data-input="data-input" value="{{ $filter['data_vigencia_fim'] }}"/>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="col-lg-6 form-group">
                                <div class="br-input">
                                    <label for="input-icon numeroAta">Número da ata</label>

                                    <div class="input-group">
                                        <input name="numeroAta" id="input-icon numeroAta" type="number" placeholder="Número da Ata" value="{{ $filter['numero_ata'] }}"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <div class="br-input">
                                    <label for="input-icon anoCompra">Ano da ata</label>
                                    <div class="input-group">
                                        <input name="anoAta" id="input-icon anoAta" type="number" placeholder="Ano da Ata" value="{{ $filter['ano_ata'] }}"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="d-flex">
                            <div class="col-lg-6 form-group">
                                <div class="br-input">
                                    <label for="input-icon numeroCompra">Número da compra</label>
                                    <div class="input-group">
                                        <input name="numeroCompra" id="input-icon numeroCompra" type="number" placeholder="Número da Compra" value="{{ $filter['numero_compra'] }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 form-group">
                                <div class="br-input">
                                    <label for="input-icon anoCompra">Ano da compra</label>
                                    <div class="input-group">
                                        <input name="anoCompra" id="input-icon anoCompra" type="number" placeholder="Ano da Compra" value="{{ $filter['ano_compra'] }}"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="d-flex">
                            <div class="col-lg-6 form-group">
                                <div class="br-select" multiple="multiple">
                                    <div class="br-input">
                                        <label for="select-multtiple">Modalidade da compra</label>
                                        <input name="modalidadeLicitacao[]" id="select-multtiple" type="text" placeholder="Selecione os itens"/>
                                        <button class="br-button" type="button" aria-label="Exibir lista" tabindex="-1" data-trigger="data-trigger"><i class="fas fa-angle-down" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <div class="br-list" tabindex="0">
                                        <div class="br-item highlighted" data-all="data-all" tabindex="-1">
                                            <div class="br-checkbox">
                                                <input id="cb" name="cb" type="checkbox"/>
                                                <label for="cb">Selecionar todos</label>
                                            </div>
                                        </div>
                                        <div class="br-item" tabindex="-1">
                                            <div class="br-checkbox">
                                                <input id="concorrencia" name="concorrencia" type="checkbox"/>
                                                <label for="concorrencia">Concorrência</label>
                                            </div>
                                        </div>
                                        <div class="br-item" tabindex="-1">
                                            <div class="br-checkbox">
                                                <input id="dispensa" name="dispensa" type="checkbox"/>
                                                <label for="dispensa">Dispensa</label>
                                            </div>
                                        </div>
                                        <div class="br-item" tabindex="-1">
                                            <div class="br-checkbox">
                                                <input id="inexigibilidade" name="inexigibilidade" type="checkbox"/>
                                                <label for="inexigibilidade">Inexigibilidade</label>
                                            </div>
                                        </div>
                                        <div class="br-item" tabindex="-1">
                                            <div class="br-checkbox">
                                                <input id="pregao" name="pregao" type="checkbox"/>
                                                <label for="pregao">Pregão</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 form-group">
                                    <div class="br-input large input-button">
                                        <label for="input-search-large">Unidade da federação</label>
                                        <select name="unidadeFederacao" class="select2" data-ajax="true"  style="width: 100%;" id="unidades_federacao" multiple="multiple" data-placeholder="Selecione uma unidade federação">
                                        </select>
                                    </div>
                                </div>
                        </div>

                        <div class="d-flex">
                            <div class="col-lg-6 form-group">
                                <div class="br-input large input-button">
                                    <label for="input-search-large">Órgão gerenciador</label>
                                    <select name="orgaosGerenciadores[]" class="select2" data-ajax="true" data-placeholder="Selecione um orgão gerenciador" style="width: 100%;" id="orgao_gerenciador" multiple="multiple">
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-6 form-group">
                                <div class="br-input large input-button">
                                    <label for="input-search-large">Unidade gerenciadora</label>
                                    <select name="unidadesGerenciadoras[]" class="select2" data-ajax="true" data-placeholder="Selecione uma unidade gerenciadora" style="width: 100%;" id="unidades_gerenciadoras" multiple="multiple">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="col-lg-6 form-group">
                                <div class="br-input large input-button">
                                    <label for="input-search-large">Órgão participante</label>
                                    <select name="orgaosParticipantes[]" class="select2" data-ajax="true" data-placeholder="Selecione um orgão participante" style="width: 100%;" id="orgao_participante" multiple="multiple">
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 form-group">
                                <div class="br-input large input-button">
                                    <label for="input-search-large">Unidade participante</label>
                                    <select name="unidadesParticipantes[]" class="select2" data-ajax="true" data-placeholder="Selecione uma unidade participante" style="width: 100%;" id="unidades_participante" multiple="multiple">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="col-lg-12 form-group">
                                <div class="br-input large input-button">
                                    <label for="input-search-large">Fornecedor da ata</label>
                                    <select name="fornecedorAta[]" class="select2" data-ajax="true" data-placeholder="Selecione um fornecedor da ata" style="width: 100%;" id="fornecedor_ata" multiple="multiple">
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-6 btn-group" role="group">
                            <button class="br-button primary mr-3" type="submit" >
                                <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                                <span data-value="save_and_back">Aplicar</span>
                            </button>
                            <button class="br-button mr-3" type="button" onclick="limparCampos()">
                                <span data-value="save_and_back">Limpar</span>
                            </button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</div>