<head>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">


    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900&amp;display=swap"/>



    <style>
        .logo {
            margin-top: 95px;
            vertical-align: middle;
        }

        body {
            font-family: 'Rawline', sans-serif;
        }

        .titulo-rel {
            margin-bottom: 0px;
            line-height: 1.2;

        }

        @page {
            margin: 1.2cm;
        }


        .subtitulo {
            margin-top: 0;
            margin-bottom: 2px;
            opacity: 0.7;
            font-family: 'Rawline', sans-serif;
            font-size: 18px;
        }

        .texto-baixo {
            margin-top: 0;
            font-weight: 500;
            position: relative;
            padding-bottom: 20px;
            margin-bottom: 0px;
        }

        .divisoria {
            width: 100%;
            height: 2px; /* Altura da borda */
            background-color: #CCC; /* Cor da borda */
            margin-top: 3px; /* Espaçamento acima da borda */
        }

        .info-table {
            display: table;
            width: 100%;
            border-collapse: collapse;
            table-layout: fixed !important;

        }

        .info-row {
            display: table-row;
        }

        .info-label,
        .info-content {
            width: 25%;
            display: table-cell;
            padding-top: 0;
            padding-bottom: 1px;
            vertical-align: middle;

        }


        .info-label {
            font-weight: bold;
            text-align: left;
        }

        .info-content {
            text-align: left;
        }


        .table {
            border-collapse: collapse;
            width: 100%;
            border: 1px solid #ddd;
        }

        .table th, .table td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }



        .link-pncp {
            white-space: pre-wrap !important;
        }

        .info-table-detalhamento {
            display: table;
            width: 100%;
        }

        .info-row-detalhamento {
            display: table-row;
        }

        .info-title {
            text-align: left;
            margin-left: 10px !important;
            padding: 6px;
            vertical-align: top;
            font-weight: 400 !important;
            font-size: 18px !important;
        }
        .titulo-ata {

            font-size: 20px;
            font-style: normal;
            line-height: normal;
            text-transform: uppercase;
            text-align: end;
            text-align: left !important;
        }



        .info-title-adesao {
            text-align: left;
            margin-left: 10px !important;
            padding: 6px;
            vertical-align: top;
            font-weight: 400 !important;
            font-size: 18px !important;
        }


    </style>
</head>
<body>
<table style="margin-top:-4%;">
    <th>
    <td class="logo">
        <img src="data:image/png;base64,{{base64_encode(file_get_contents(public_path('img/logo_relatorio.png')))}}"
             alt="logo"
             width="130"
        >
    </td>
    <td class="col-md-6 d-flex" style="padding-left: 27px;">
        <div>
            <h2 class="titulo-rel" style="font-weight: 400 !important;">
                @if(isset($titulo))
                    {{ $titulo }}
                    @if(isset($subtitulo))
                        <br>
                        {{ $subtitulo }}
                    @endif
                @else
                    Relatório Ata de Registro de Preços
                @endif
            </h2>


            <p class="subtitulo">
                @if(isset($unidadeGerenciadora))
                    {{ $unidadeGerenciadora }}
                @else
                    Unidade Gerenciadora {{$arpDados->unidade_gerenciadora_titulo}}
                @endif
            </p>
        </div>
    </td>
    </th>
</table>
