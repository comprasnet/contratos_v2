@extends('errors.layout')

@php
  $error_number = 403;
@endphp

@section('title')
  Permissão negada.
@endsection

@section('description')
  @php
    $default_error_message = "Acesso negado - O usuário fornecedor deve acessar os sistema através do login no ambiente do sistema Compras.gov.br";
  @endphp
  {!! isset($exception)? ($exception->getMessage()?e($exception->getMessage()):$default_error_message): $default_error_message !!}
  <a href='https://www.comprasnet.gov.br/seguro/loginPortal.asp'>https://www.comprasnet.gov.br/seguro/loginPortal.asp</a>
@endsection
