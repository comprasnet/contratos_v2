<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'namespace'  => 'App\Http\Controllers\Api',
], function () {
    Route::get('amparolegalfiltercontratos', 'AmparoLegalController@amparolegalfiltercontratos');
    Route::get('unidadefiltercontratos', 'UnidadeController@unidadesfiltercontratos');
});

Route::group([
    'prefix' => 'contratosv2',
    'namespace' => 'App\Http\Controllers\Api',
], function () {
    Route::post('login', 'AuthController@login');

    Route::group([
        'prefix' => 'novodivulgacao',
        'namespace' => 'NovoDivulgacaoCompra',
    ], function () {
        Route::get('contratacao', 'NovoDivulgacaoCompraController@getContratacao');
        Route::get('item', 'NovoDivulgacaoCompraController@getItem');
        Route::get('fornecedorunidade', 'NovoDivulgacaoCompraController@getFornecedorUnidade');
    });

    Route::group([
        'prefix' => 'compras'
    ], function () {
        Route::put('sisrp', 'ComprasController@atualizarCompraV1');
    });
    Route::group([
        'prefix' => 'contratacao_pncp',
    ], function () {
        Route::get('compra', 'PNCPCompraController@getContratacao');
    });
});
