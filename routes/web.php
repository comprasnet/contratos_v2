<?php

use App\Http\Controllers\Transparencia\AtaPrecosController;
use App\Http\Controllers\Transparencia\ItemAtaPrecosController;
use App\Http\Controllers\FornecedorCompras\UsuarioFornecedorLoginController;
use App\Http\Controllers\FornecedorCompras\UsuarioFornecedorCrudController;
use App\Http\Controllers\Transparencia\DetalhamentoAtaPrecosCrudController;
use App\Http\Controllers\Transparencia\TransparanciaController;
use App\Http\Controllers\Relatorio\RelatorioTransparenciaController;
use Illuminate\Support\Facades\Route;
use App\Http\Traits\NfeOrg;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Arp\ArpCrudController;

use Dompdf\Dompdf;

//rotas para perfil fornecedor login
Route::group([
    'prefix' => 'fornecedor',
    'namespace'  => 'App\Http\Controllers\FornecedorCompras',
], function () {

    Route::get('/login/administrador', [UsuarioFornecedorLoginController::class, 'indexAdministrador'])
        ->name('fornecedor.administrador');
    Route::get('/login/preposto', [UsuarioFornecedorLoginController::class, 'indexPreposto'])
        ->name('fornecedor.preposto');
    Route::post('/create-user-fornecedor', [UsuarioFornecedorLoginController::class, 'salvarUsuarioFornecedor'])
        ->name('usuario.fornecedor.create');

    //'administrador' e 'preposto' inicio
    Route::middleware([
            'web',
            'check.administrador.fornecedor:perfilfornecedor_consultar,acessar_dashboard_usuario_fornecedor'
    ])->group(function () {
        Route::get('/inicio/administrador', [UsuarioFornecedorCrudController::class, 'adminInicio'])
            ->name('fornecedor.administrador.inicio');
    });

    Route::middleware([
        'web',
        'check.administrador.fornecedor:perfilfornecedor_consultar,acessar_dashboard_usuario_fornecedor'
    ])->group(function () {
        Route::get('/inicio/preposto', [UsuarioFornecedorCrudController::class, 'prepostoInicio'])
            ->name('fornecedor.preposto.inicio');
        Route::get(
            '/inicio/preposto/contratos',
            [UsuarioFornecedorCrudController::class, 'buscarContratosFornecedor']
        )->name('fornecedor.preposto.contratos');
        Route::get(
            '/inicio/preposto/atas',
            [UsuarioFornecedorCrudController::class, 'buscarAtasFornecedor']
        )->name('fornecedor.preposto.atas');
    });
});

//    Route::group(['middleware' => ['web', 'auth']], function () {
//        Route::get('/transparencia/arp-item', [ItemAtaPrecosController::class, 'index'])
//        ->name('transparencia.arp-item');
//        Route::get(
//            '/transparencia/arp-item/{codigo_unidade}/{modalidade_compra}/{numero_ano_compra}/{numero_item_compra}',
//            [ItemAtaPrecosController::class, 'listItensComBaseNaCompra']
//        );
//    });

// rota transparencia

//    Route::get('/arp/filtro', [AtaPrecosController::class, 'index'])->name('transparency.transparencia-filtro');

//# Rota para trazer os itens de atas, baseado nos dados da compra
//
    Route::get('/arp/ajax-options', [AtaPrecosController::class, 'ajaxOptions']);
    Route::get('/arp/ajax-unidade-options', [AtaPrecosController::class, 'getUnidades']);
    Route::get('/arp/ajax-orgaos-options', [AtaPrecosController::class, 'getOrgaos']);
    Route::get('/arp/ajax-estados-options', [AtaPrecosController::class, 'getEstados']);
    Route::get('/arp/ajax-fornecedor-options', [AtaPrecosController::class, 'getFornecedores']);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    Route::get('/arp/get-uasgs', 'App\Http\Controllers\Arp\ArpCrudController@getUasgs');
    Route::get('/arp/get-selected-uasgs', 'App\Http\Controllers\Arp\ArpCrudController@getSelectedUasgs');
    Route::get('/arp/filter-arps', 'App\Http\Controllers\Arp\ArpCrudController@filterUasgs');

    Route::group(
        [
        'namespace' => 'App\Http\Controllers',
        'middleware' => [
            'permission:V2_acessar|V2_acessar_desenvolvedor',
            config('backpack.base.web_middleware', 'web'),
        ],
        'prefix' => config('backpack.base.route_prefix'),
        ],
        function () {

            Route::get('logout', 'Auth\LoginController@logout')->name('backpack.auth.logout');
            Route::post('logout', 'Auth\LoginController@logout');

            Route::get('inicio', 'AdminController@inicio')
                ->middleware('check.block.fornecedor')
                ->middleware('ugprimaria')
                ->name('backpack.inicio');

            Route::get('/', 'AdminController@redirect')->name('backpack');
            Route::get('/dashboard', 'AdminController@redirect')->name('backpack');
            Route::get('/dashboard/list-arp', 'AdminController@listArpDashboard');
            Route::get('/dashboard/unidade', 'AdminController@getUnidadeSession');

            Route::get(
                'autenticarautomatica/{url}',
                'AdminController@autenticarUsuarioAutomatico'
            )->name('inicio.autenticarautomatico');

                Route::get(
                    '/arp/{arp_id}/arp-arquivo/{arp_arquivo_id}/alterar-privacidade',
                    'Arp\ArpArquivoCrudController@alterarPrivacidade'
                )->name('arp-arquivo-alterar-privacidade');
        }
    );

    Route::group([
    'prefix' => 'acessogov',
    'namespace' => 'App\Http\Controllers\Acessogov',
    ], function () {
        Route::get('/autorizacao', 'LoginAcessoGov@autorizacao')->name('acessogov.autorizacao');
        Route::get('/tokenacesso', 'LoginAcessoGov@tokenAcesso')->name('acessogov.tokenacesso');
    });

    Route::get('/unidadeuasg', '\App\Http\Controllers\UnidadeCrudController@unidadeUasg');

    Route::get('/layoutpadraogov', '\App\Http\Controllers\AdminController@layoutpadraogov');

    Route::get('appredirect/{key}/{url}', function ($key, $url) {
        $idKey = base64_decode($key);

        $user = User::where('id', $idKey)->where("situacao", true)->first();

        if ($user) {
            backpack_auth()->login($user);
            $urlCompleta = \Request::getRequestUri();

            return redirect("/{$url}");
        }
    })->withoutMiddleware('permission:V2_acessar');
