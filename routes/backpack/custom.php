<?php

use App\Http\Controllers\Relatorio\RelatorioTransparenciaController;
use App\Http\Controllers\Transparencia\AtaPrecosController;
use App\Http\Controllers\Transparencia\DetalhamentoAtaPrecosCrudController;
use App\Http\Controllers\Transparencia\ItemAtaPrecosController;
use App\Http\Controllers\Transparencia\TransparenciaController;
use App\Http\Controllers\Transparencia\ArpCrudController;
use App\Http\Controllers\Transparencia\TransparenciaAtaPrecosController;
use App\Http\Controllers\FornecedorCompras\AlterarFornecedorCrudController;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Response;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::get('/contratos', function () {
    return redirect()->to(env('URL_CONTRATO_VERSAO_UM'));
})->name('contratos');

Route::group([
    'prefix' => 'transparencia',
    'namespace' => 'App\Http\Controllers\Transparencia',
], function () {
    Route::get('/v1', function () {
        return redirect()->to('https://contratos.comprasnet.gov.br/transparencia');
    })->name('transparencia-v1');

//    Route::get('/', [TransparenciaController::class, 'index'])
//        ->name('transparencia')
//        ->middleware('cache.response');


    //teste rotas bruno VM
//    Route::post(
//        '/transparencia/arp-item',
//        [ItemAtaPrecosController::class, 'ajax']
//    )->name(
//        'transparencia.arp-item.ajax'
//    );
//
//    Route::post(
//        '/disparar-job-pdf/{arpId}',
//        [
//            RelatorioTransparenciaController::class,
//            'dispararJobPDF'
//        ]
//    )->name('disparar.job.pdf');
//
//    Route::get(
//        '/gerar-pdf/{id}',
//        [
//            RelatorioTransparenciaController::class,
//            'generatePDF'
//        ]
//    )->name('gerar.pdf');
//    Route::get(
//        '/visualizar-pdf/{id}/{tipo}',
//        [
//            RelatorioTransparenciaController::class,
//            'visualizarPDF'
//        ]
//    )->name('visualizar.pdf');
//
//    Route::get(
//        '/check-status-pdf/{arpId}',
//        [
//            RelatorioTransparenciaController::class,
//            'checkStatusPDF'
//        ]
//    );

//    Route::get('/arp', [AtaPrecosController::class, 'index'])->name('transparencia.arp');

//    Route::get('/arp/{id}/show', [DetalhamentoAtaPrecosCrudController::class, 'setupShowOperation']);
//
//    Route::crud('/arpshow', 'DetalhamentoAtaPrecosCrudController');
//    Route::crud('/arpshow/itens/{numero_item}', 'AtaPrecosDetalhesItemsCrudController');
//
//    # Rota que busca lista de Atas a ser exibida na página principal (Tanto do transparêndia quanto da área logada)
//    Route::get('/dashboard/list-arp', [TransparenciaController::class, 'listArpDashboard']);
//
//    Route::crud(
//        '/arpshow/{compra_id}/itens/{compra_item_id}/compra-item-fornecedor',
//        'AtaPrecosDetalhesItemFornecedorCrudController'
//    );

//    Route::group(['middleware' => ['web', 'auth']], function () {
//        Route::get(
//            '/compras/ajax-unidades-options',
//            'Compras\ComprasCrudController@unidadesOptions'
//        )->name('unidade_id');
//        Route::get(
//            '/compras/ajax-modalidade-options',
//            'Compras\ComprasCrudController@modalidadeOptions'
//        )->name('modalidade_id');
//        Route::get(
//            '/compras/ajax-compra-lei-options',
//            'Compras\ComprasCrudController@compraLeiOptions'
//        )->name('lei');
//        Route::crud('/compras', 'Compras\ComprasCrudController');
//        Route::crud('/compras/{compra_id}/itens', 'Compras\CompraItemsCrudController');
//        Route::crud(
//            '/compras/{compra_id}/itens/{compra_item_id}/compra-item-fornecedor',
//            'Compras\CompraItemFornecedorCrudController'
//        );
//    });
});


Route::group([
    'prefix'     => config('backpack.base.route_prefix', ''),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin'),
        (array) 'check.block.fornecedor',
        (array) 'check.permissions',
        (array) 'permission:V2_acessar'
    ),
    'namespace' => 'App\Http\Controllers',
], function () {
    Route::group(['middleware' => 'ugprimaria'], function () {
        Route::group([
            'prefix' => 'fetch',
            'namespace' => 'Fetch',
        ], function () {
        });

        Route::get('busca/cep', 'BuscaEnderecoController@buscaCep')->name('busca.cep');
        Route::get('busca/municipio', 'BuscaEnderecoController@buscaMunicipio')->name('busca.municipio');

        Route::crud('meus-contratos', 'MeusContratosCrudController');

        Route::group([
            'prefix' => 'contrato/{contrato_id}',
            'namespace' => 'Contrato'
        ], function () {
            Route::crud('/prepostos', 'ContratoPrepostoCrudController');
            Route::get(
                '/prepostos/{contratopreposto_id}/{statusaceitacao}',
                'ContratoPrepostoCrudController@aceitarIndicacaoPreposto'
            )->name('aceita.indicacao.preposto');
            Route::post('/local-execucao', 'ContratoLocalExecucaoCrudController@store');
        });

        Route::group([
            'prefix' => 'compras',
            'namespace' => 'Arp',
        ], function () {
            Route::get('/ajax-unidades-options', 'ComprasCrudController@unidadesOptions')->name('unidade_id');
            Route::get('/ajax-modalidade-options', 'ComprasCrudController@modalidadeOptions')->name('modalidade_id');
            Route::get('/ajax-compra-lei-options', 'ComprasCrudController@compraLeiOptions')->name('lei');
            Route::crud('/', 'ComprasCrudController');
            Route::crud('/{compra_id}/itens', 'CompraItemsCrudController');
            Route::crud('arp', 'ArpCrudController');
            Route::crud(
                '/{compra_id}/itens/{compra_item_id}/compra-item-fornecedor',
                'CompraItemFornecedorCrudController'
            );
        });

        Route::group([
            'prefix' => 'assinagov'
        ], function () {
            Route::get('/autorizacao', 'AssinaGovController@autorizacao')->name('assinagov.autorizacao');
            Route::get('/token', 'AssinaGovController@token')->name('assinagov.token');
        });

        Route::group([
            'prefix' => 'autorizacaoexecucao/{contrato_id}',
        ], function () {
            Route::crud('/', 'AutorizacaoExecucao\AutorizacaoExecucaoCrudController');
            Route::post('/anexo', 'AutorizacaoExecucao\AutorizacaoExecucaoCrudController@anexo')
                ->name('autorizacaoexecucao.anexo');
            Route::get('empenhos', 'AutorizacaoExecucao\AutorizacaoExecucaoCrudController@empenhos');
            Route::get('unidades', 'AutorizacaoExecucao\AutorizacaoExecucaoCrudController@unidades');
            Route::get('contratoitem', 'AutorizacaoExecucao\AutorizacaoExecucaoCrudController@contratoItem');
            Route::get(
                'saldohistoricoitem-periodo',
                'AutorizacaoExecucao\AutorizacaoExecucaoCrudController@saldoHistoricoItemPorPeriodo'
            )->name('autorizacaoexecucao.saldoHistoricoItemPorPeriodo');
            Route::get(
                'saldohistoricoitem',
                'AutorizacaoExecucao\AutorizacaoExecucaoCrudController@saldoHistoricoItem'
            )->name('autorizacaoexecucao.saldoHistoricoItem');

            Route::group(['prefix' => '{autorizacaoexecucao_id}'], function () {
                Route::crud('entrega', 'AutorizacaoExecucao\AutorizacaoExecucaoEntregaCrudController');
                Route::get(
                    'entrega/{entrega_id}/duplicar',
                    'AutorizacaoExecucao\AutorizacaoExecucaoEntregaCrudController@duplicar'
                )->name('autoracaoexecucao.entrega.duplicar');
                Route::crud(
                    'entrega/{entrega_id}/analisar',
                    'AutorizacaoExecucao\AutorizacaoExecucaoEntregaAnalisarCrudController'
                );
                Route::crud('entrega/trp', 'AutorizacaoExecucao\AutorizacaoExecucaoEntregaTRPCrudController');
                Route::crud('entrega/trd', 'AutorizacaoExecucao\AutorizacaoExecucaoEntregaTRDCrudController');
                Route::get(
                    'entrega/trp/{entrega_id}/ajustar',
                    'AutorizacaoExecucao\AutorizacaoExecucaoEntregaTRPCrudController@ajustar'
                )->name('trp.ajustar');
                Route::get(
                    'entrega/trd/{entrega_id}/ajustar',
                    'AutorizacaoExecucao\AutorizacaoExecucaoEntregaTRDCrudController@ajustar'
                )->name('trd.ajustar');
                Route::crud(
                    'entrega/trp/{entrega_id}/assinatura',
                    'AutorizacaoExecucao\AutorizacaoExecucaoEntregaTRPAssinaturaCrudController'
                );
                Route::get(
                    'entrega/trp/{entrega_id}/assinatura/assinar',
                    'AutorizacaoExecucao\AutorizacaoExecucaoEntregaTRPAssinaturaCrudController@assinar'
                )->name('trp.assinar');
                Route::crud(
                    'entrega/trd/{entrega_id}/assinatura',
                    'AutorizacaoExecucao\AutorizacaoExecucaoEntregaTRDAssinaturaCrudController'
                );
                Route::get(
                    'entrega/trd/{entrega_id}/assinatura/assinar',
                    'AutorizacaoExecucao\AutorizacaoExecucaoEntregaTRDAssinaturaCrudController@assinar'
                )->name('trd.assinar');
                Route::crud('retificar', 'AutorizacaoExecucao\AutorizacaoExecucaoRetificarCrudController');
                Route::crud('alterar', 'AutorizacaoExecucao\AutorizacaoExecucaoAlterarCrudController');
                Route::crud(
                    'alterar/{autorizacaoexecucao_historico_id}/assinatura',
                    'AutorizacaoExecucao\AutorizacaoExecucaoAlterarAssinaturaCrudController'
                );
                Route::get(
                    'alterar/{autorizacaoexecucao_historico_id}/assinatura/assinar',
                    'AutorizacaoExecucao\AutorizacaoExecucaoAlterarAssinaturaCrudController@assinar'
                )->name('autorizacaoexecucao-alterar.assinatura.assinar');
                Route::get(
                    'alterar/{autorizacaoexecucao_historico_id}/assinatura/recusar',
                    'AutorizacaoExecucao\AutorizacaoExecucaoAlterarAssinaturaCrudController@recusar'
                )->name('autorizacaoexecucao-alterar.assinatura.recusar');

                Route::crud('assinatura', 'AutorizacaoExecucao\AutorizacaoExecucaoAssinaturaCrudController');
                Route::get(
                    'assinatura/assinar',
                    'AutorizacaoExecucao\AutorizacaoExecucaoAssinaturaCrudController@assinar'
                )->name('autorizacaoexecucao.assinatura.assinar');
                Route::get(
                    'assinatura/recusar',
                    'AutorizacaoExecucao\AutorizacaoExecucaoAssinaturaCrudController@recusar'
                )->name('autorizacaoexecucao.assinatura.recusar');
                Route::get('duplicar', 'AutorizacaoExecucao\AutorizacaoExecucaoCrudController@duplicar');
                Route::get('gerar-pdf', 'AutorizacaoExecucao\AutorizacaoExecucaoCrudController@gerarPDF');
                Route::crud(
                    'status-historico',
                    'AutorizacaoExecucao\AutorizacaoExecucaoStatusHistoricoCrudController'
                );
            });
            Route::get('usado', 'AutorizacaoExecucao\AutorizacaoExecucaoCrudController@quantidadeUsada');
            Route::get('utilizado', 'AutorizacaoExecucao\AutorizacaoExecucaoCrudController@itensUtilizados');
            Route::crud('saldo-utilizado', 'AutorizacaoExecucao\AutorizacaoexecucaoSaldoUtilizadoCrudController');
            Route::get(
                'contrato-item/valor/{item}',
                'AutorizacaoExecucao\AutorizacaoexecucaoSaldoUtilizadoCrudController@getValorItem'
            );
            Route::get(
                'contrato-item/quantidade/{item}',
                'AutorizacaoExecucao\AutorizacaoexecucaoSaldoUtilizadoCrudController@getQuantidadeItem'
            );
            Route::get(
                'informacoesquadroresumo/{autorizacao_execucao_id}',
                'AutorizacaoExecucao\AutorizacaoExecucaoCrudController@informacoesQuadroResumo'
            );
        });

        Route::group(['prefix' => 'entrega/{contrato_id}'], function () {
            Route::crud('/', 'EntregaCrudController');
            Route::get(
                '/{entrega_id}/duplicar',
                'EntregaCrudController@duplicar'
            )->name('entrega.duplicar');
            Route::crud('/{autorizacaoexecucao_id}', 'EntregaCrudController');
            Route::get(
                '/{autorizacaoexecucao_id}/{entrega_id}/duplicar',
                'EntregaCrudController@duplicar'
            )->name('entrega.duplicar');
        });

        Route::group(['prefix' => 'trp/{contrato_id}'], function () {
            Route::crud('/', 'TermoRecebimentoProvisorioCrudController');
            Route::crud('/{id}/assinatura', 'TermoRecebimentoProvisorioAssinaturaCrudController');
            Route::get('/{id}/assinatura/assinar', 'TermoRecebimentoProvisorioAssinaturaCrudController@assinar');
            Route::get('/{id}/ajustar', 'TermoRecebimentoProvisorioCrudController@ajustar');

            Route::crud('/{entrega_id}', 'TermoRecebimentoProvisorioCrudController');
            Route::crud('/{entrega_id}/{id}/assinatura', 'TermoRecebimentoProvisorioAssinaturaCrudController');
            Route::get(
                '/{entrega_id}/{id}/assinatura/assinar',
                'TermoRecebimentoProvisorioAssinaturaCrudController@assinar'
            );
            Route::get('/{entrega_id}/{id}/ajustar', 'TermoRecebimentoProvisorioCrudController@ajustar');
        });

        Route::group(['prefix' => 'trd/{contrato_id}'], function () {
            Route::crud('/', 'TermoRecebimentoDefinitivoCrudController');
            Route::crud('/{id}/assinatura', 'TermoRecebimentoDefinitivoAssinaturaCrudController');
            Route::get('/{id}/assinatura/assinar', 'TermoRecebimentoDefinitivoAssinaturaCrudController@assinar');
            Route::get('/{id}/ajustar', 'TermoRecebimentoDefinitivoCrudController@ajustar');

            Route::crud('/{trp_id}', 'TermoRecebimentoDefinitivoCrudController');
            Route::crud('/{trp_id}/{id}/assinatura', 'TermoRecebimentoDefinitivoAssinaturaCrudController');
            Route::get(
                '/{trp_id}/{id}/assinatura/assinar',
                'TermoRecebimentoDefinitivoAssinaturaCrudController@assinar'
            );
            Route::get('/{trp_id}/{id}/ajustar', 'TermoRecebimentoDefinitivoCrudController@ajustar');
        });

        Route::group([
            'prefix' => 'declaracaoopm/{contrato_id}',
        ], function () {
            Route::post('cancelaropm', 'DeclaracaoOpmCrudController@cancelarDeclaracao')->name('cancelaropm');
            Route::crud('/', 'DeclaracaoOpmCrudController');
            Route::get('/gerar-pdf/{id}', 'DeclaracaoOpmCrudController@gerarPDF')->name('gerarpdf');
            Route::get('/download-pdf/{id}', 'DeclaracaoOpmCrudController@buscarPDF')->name('buscarPDF');
        });

        Route::get('amparo-legal/searchByAtoNormativo', 'AmparoLegalCrudController@searchByAtoNormativo');

        Route::group([
            'prefix' => 'arp',
            'namespace' => 'Arp',
            'excluded_middleware' => ['ugprimaria'],
        ], function () {
            Route::crud('/', 'ArpCrudController');

            Route::post('validarcompra', 'ArpCrudController@validarCompra');
            Route::post('itemcomprafornecedor', 'ArpCrudController@itemCompraFornecedor');
            Route::get('gestores', 'ArpCrudController@gestores');
            Route::get('autoridadesignataria', 'ArpCrudController@autoridadeSignataria');
            Route::post('validarnumeroata', 'ArpCrudController@validarNumeroAta');

            Route::get('/visualizar-pdf/{id}/{tipo}', 'ArpCrudController@visualizarArpPDF')
                ->name('visualizar.arp.pdf');

            Route::post('/disparar-job-pdf/{arp_id}', 'ArpCrudController@dispararJobArpPDF')
                ->name('disparar.arp.job.pdf');

            Route::get('/gerar-pdf/{id}', 'ArpCrudController@generateArpPDF')
                ->name('gerar.arp.pdf');

            Route::get('/check-status-pdf/{arp_id}', 'ArpCrudController@checkStatusArpPDF');

            Route::group([
                'prefix' => 'alteracaoarp',
            ], function () {
                Route::crud('/{arp_id}', 'ArpAlteracaoCrudController');
                Route::post(
                    '{arp_id}/{arp_alteracao_id}/itemalteracao',
                    'ArpAlteracaoCrudController@exibirItemTipoAlteracao'
                );
                Route::post(
                    '{arp_id}/{arp_alteracao_id}/fornecedoralteracao',
                    'ArpAlteracaoCrudController@exibirFornecedorAlteracao'
                );
                Route::get('{arp_id}/pesquisarempenho/{arp_item_id}', 'ArpAlteracaoCrudController@pesquisarEmpenho');
                Route::post('{arp_id}/pesquisarcnpjfornecedor', 'ArpAlteracaoCrudController@pesquisarCnpjFornecedor');
            });
            Route::get('exibirclassificacaofornecedor', 'ArpCrudController@exibirClassificacaoFornecedor');
            Route::get('exibirdetalheitem', 'ArpCrudController@exibirDetalheItem');
            Route::post('validarcompracadastro', 'ArpCrudController@validarCompraCadastro');

            Route::group([
                'prefix' => 'execucao'
            ], function () {
                Route::crud('/{arp_id}', 'ArpExecucaoCrudController');
                Route::post('{arp_id}/validaform', 'ArpExecucaoCrudController@validaFormularioAjax');
            });

            Route::group([
                'prefix' => 'remanejamento',
            ], function () {
                Route::crud('/', 'ArpRemanejamentoCrudController');
                Route::get('numeroata', 'ArpRemanejamentoCrudController@numeroAta');
                Route::get('numerocompraata/{idUnidadeCompra}', 'ArpRemanejamentoCrudController@numeroCompraAta');
                Route::get(
                    'modalidadecompraata/{idUnidadeCompra}',
                    'ArpRemanejamentoCrudController@modalidadeCompraAta'
                );

                Route::get('unidadecompraata', 'ArpRemanejamentoCrudController@unidadeCompraAta');
                Route::post('buscaritemremanejamento', 'ArpRemanejamentoCrudController@buscarItemRemanejamento');
                Route::post(
                    'validarquantitativosolicitado',
                    'ArpRemanejamentoCrudController@validarQuantitativoSolicitado'
                );
                Route::post(
                    'cancelarremanejamento',
                    'ArpRemanejamentoCrudController@cancelarRemanejamento'
                );

                Route::group([
                    'prefix' => 'analisar',
                ], function () {
                    // Etapa 1
                    Route::crud('/', 'ArpRemanejamentoAnaliseUnidadeItemCrudController');

                    // Rota para exibir o remanejamento para a Unidade Gerenciadora do item
                    Route::post(
                        '/gerenciadoradonaitem',
                        'ArpRemanejamentoAnaliseUnidadeItemCrudController@remanejamentoGerenciadoraDonaItem'
                    );

                    // Rota para exibir o remanejamento para a Unidade Gerenciadora da ata
                    Route::post(
                        '/remanejamentogerenciadoraata',
                        'ArpRemanejamentoAnaliseUnidadeItemCrudController@remanejamentoGerenciadoraAta'
                    );

                    // Etapa 2
                    Route::crud('/gerenciadoraata', 'ArpRemanejamentoAnaliseGerenciadoraAtaCrudController');
                });
            });

            Route::group([
                'prefix' => 'item',
            ], function () {
                Route::crud('/{arp_id}', 'ArpItemCrudController');
                Route::post('{arp_id}/itemcomprafornecedor', 'ArpCrudController@itemCompraFornecedor');
            });

            Route::group([
                'prefix' => 'item/{arp_id}/',
            ], function () {
                Route::crud('arquivos', 'ArpArquivoCrudController');
            });

            Route::group([
                'prefix' => 'retificar',
            ], function () {
                Route::crud('/{arp_id}', 'ArpRetificarCrudController');
                Route::get(
                    '{arp_id}/verificar-vinculos-item/{item_id}',
                    'ArpRetificarCrudController@verificarVinculosItem'
                );
            });

            Route::group([
                'prefix' => 'adesao',
            ], function () {
                Route::crud('/', 'AdesaoCrudController');
                Route::get('unidade', 'AdesaoCrudController@unidade');
                Route::get('fornecedor', 'AdesaoCrudController@fornecedor');
                Route::get('itens', 'AdesaoCrudController@itensAtaAdesao');
                //  Route::get('numeroAtaAdesao', 'AdesaoCrudController@numeroAtaAdesao');
                Route::get('/{unidade_id}/compra', 'AdesaoCrudController@compra');
                Route::get('/{unidade_id}/numeroAtaAdesao', 'AdesaoCrudController@numeroAtaAdesao');
                Route::get('/{unidade_id}/{numero_compra}/modalidade', 'AdesaoCrudController@modalidade');
                Route::get(
                    '/{unidade_id}/{numero_compra}/{modalidade}/fornecedor',
                    'AdesaoCrudController@fornecedor'
                );
                Route::post('buscaritem', 'AdesaoCrudController@buscarItem');
                Route::crud('/item/{id_adesao}', 'AdesaoItemCrudController');
                Route::crud('/analisar', 'AnalisarAdesaoCrudController');
                Route::get(
                    'unidadegerenciadoraexcecao/{id_unidade_gerenciadora}',
                    'AdesaoCrudController@unidadeGerenciadoraExcecao'
                );
                Route::post('cancelaradesao', 'AdesaoCrudController@cancelarAdesao');
                Route::post(
                    'validarquantitativo',
                    'AdesaoCrudController@validarQuantitativoSolicitacaoCarona'
                );
                Route::get(
                    'exibircampoexecucaodescentralizadaprogramaprojetofederal/{unidade_gerenciadora_compra_id}',
                    'AdesaoCrudController@exibirCampoExecucaoDescentralizadaProgramaProjetoFederal'
                );
            });
        });

        Route::group([
            'prefix' => 'admin',
            'namespace' => 'Admin',
        ], function () {
            Route::crud('envia-dados-pncp', 'EnviaDadosPncpCrudController');
            Route::crud('envia-dados-pncp-historico/{envia_dados_pncp_id}', 'EnviaDadosPncpHistoricoCrudController');
        });

        Route::get(
            '{envia_pncp_historico_id}/reenviar-pncp',
            'Admin\EnviaDadosPncpHistoricoCrudController@reSendToPNCP'
        )->name('reenviar-pncp');

        Route::get(
            'autenticarautomatica/{url}',
            'AdminController@autenticarUsuarioAutomatico'
        )->name('inicio.autenticarautomatico');

        Route::get('appredirect/{key}/{url}', function ($key) {

            $idKey = base64_decode($key);

            $user = User::where('id', $idKey)->where("situacao", true)->first();

            if ($user) {
                backpack_auth()->login($user);
                $urlCompleta = \Request::getRequestUri();
                $url = explode("/", $urlCompleta)[3];
                $url = str_replace("-", "/", $url);

                return redirect("/{$url}");
            }
        })->withoutMiddleware('permission:V2_acessar');
        Route::post('alteraruasgusuario', 'AdminController@alterarUasgUsuario');
        Route::get('infocontratos', 'AdminController@infoContratos');
    });

    Route::group([
        'middleware' => [
            'check.administrador.fornecedor:trocar_fornecedor_sessao,
            acessar_dashboard_usuario_fornecedor,V2_acessar'],
        'prefix' => 'fornecedor',
        'namespace'  => 'FornecedorCompras',
    ], function () {
            Route::group([
                'namespace' => 'Arp'
            ], function () {
                Route::crud('/arp/adesao/analisar', 'UsuarioFornecedorAnalisarAdesaoCrudController');
            });

        Route::get(
            'autorizacaoexecucao/informacoesquadroresumo/{autorizacao_execucao_id}',
            'UsuarioFornecedorAutorizacaoExecucaoCrudController@informacoesQuadroResumo'
        );
        Route::get(
            'contrato/informacoesprepostos/{contrato_id}',
            'UsuarioFornecedorContratosCrudController@informacoesPrepostos'
        );
        Route::group([
            'prefix' => '/contrato/{contrato_id}/prepostos',
        ], function () {
            Route::crud('/', 'UsuarioFornecedorContratoPrepostoCrudController'); // CRUD padrão
            Route::get(
                '/indicar',
                function ($contrato_id) {
                    return response()->json([
                        'message' => 'Rota fake funcionando para contrato ID: ' . $contrato_id,
                    ]);
                }
            )->name('fornecedor.contrato.prepostos.indicar');
        });
        Route::crud('/alterar-fornecedor', 'AlterarFornecedorCrudController');
        Route::crud('/contrato', 'UsuarioFornecedorContratosCrudController');
        Route::crud('/arp', 'ArpFornecedorCrudController');
         Route::group([
            'prefix' => '/autorizacaoexecucao/{contrato_id}',
         ], function () {
            Route::crud('/', 'UsuarioFornecedorAutorizacaoExecucaoCrudController');
            Route::crud(
                '/{autorizacaoexecucao_id}/assinatura',
                'UsuarioFornecedorAutorizacaoExecucaoAssinaturaCrudController'
            );
            Route::get(
                '/{autorizacaoexecucao_id}/assinatura/assinar',
                'UsuarioFornecedorAutorizacaoExecucaoAssinaturaCrudController@assinar'
            )->name('autorizacaoexecucao.assinatura.preposto.assinar');
            Route::get(
                'informacoesquadroresumo/{autorizacao_execucao_id}',
                'UsuarioFornecedorAutorizacaoExecucaoCrudController@informacoesQuadroResumo'
            );
         });
        Route::group([
            'prefix' => '/autorizacaoexecucao',
        ], function () {
            Route::crud('/', 'UsuarioFornecedorAutorizacaoExecucaoCrudController');
            Route::get(
                '{fornecedor_id}/unidadesfornecedor',
                'UsuarioFornecedorAutorizacaoExecucaoCrudController@unidadesFornecedor'
            );
            Route::get(
                '{contrato_id}/{fornecedor_id}/unidadesfornecedor',
                'UsuarioFornecedorAutorizacaoExecucaoCrudController@unidadesFornecedor'
            );
            Route::get(
                '{fornecedor_id}/empenhosfornecedor',
                'UsuarioFornecedorAutorizacaoExecucaoCrudController@empenhosFornecedor'
            );
            Route::get(
                '{contrato_id}/{fornecedor_id}/empenhosfornecedor',
                'UsuarioFornecedorAutorizacaoExecucaoCrudController@empenhosFornecedor'
            );
            Route::group([
                'prefix' => '{autorizacaoexecucao_id}/entrega',
            ], function () {
                Route::crud('/', 'UsuarioFornecedorAutorizacaoExecucaoEntregaCrudController');
                Route::post('/anexo', 'UsuarioFornecedorAutorizacaoExecucaoEntregaCrudController@anexo');

                Route::get(
                    '{entrega_id}/duplicar',
                    'UsuarioFornecedorAutorizacaoExecucaoEntregaCrudController@duplicar'
                );

                Route::post(
                    '{entrega_id}/cancelar',
                    'UsuarioFornecedorAutorizacaoExecucaoEntregaCrudController@cancelarEntrega'
                );
            });
        });

        Route::group([
            'prefix' => 'assinagov',
            'namespace' => '\App\Http\Controllers',
        ], function () {
            Route::get('/autorizacao', 'AssinaGovController@autorizacao')
                ->name('fornecedor.assinagov.autorizacao');
            Route::post('/autorizacao', 'AssinaGovController@autorizacao');
            Route::get('/token', 'AssinaGovController@token')
                ->name('fornecedor.assinagov.token');
        });

        Route::group([
            'prefix' => '/contratoinstrumentodecobranca/{contrato_id}',
        ], function () {
            Route::crud('/', 'UsuarioFornecedorContratoInstrumentoDeCobrancaCrudController');
        });
        Route::group([
            'prefix' => '/contratoinstrumentodecobranca',
        ], function () {
            Route::crud('/', 'UsuarioFornecedorContratoInstrumentoDeCobrancaCrudController');
        });
    });
});

//rotas para perfil fornecedor login
Route::group([
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin'),
        (array) 'permission:V2_acessar'
    ),
    'prefix' => 'fornecedor',
    'namespace'  => 'App\Http\Controllers\FornecedorCompras',
], function () {

    Route::get('/login/administrador', [UsuarioFornecedorLoginController::class, 'indexAdministrador'])
        ->name('fornecedor.administrador');
    Route::get('/login/preposto', [UsuarioFornecedorLoginController::class, 'indexPreposto'])
        ->name('fornecedor.preposto');
    Route::post('/create-user-fornecedor', [UsuarioFornecedorLoginController::class, 'salvarUsuarioFornecedor'])
        ->name('usuario.fornecedor.create');

    //'administrador' e 'preposto' inicio
    Route::middleware([
        'web',
        'check.administrador.fornecedor:perfilfornecedor_consultar,acessar_dashboard_usuario_fornecedor'
    ])->group(function () {
        Route::get('/inicio/administrador', [UsuarioFornecedorCrudController::class, 'adminInicio'])
            ->name('fornecedor.administrador.inicio');
    });

    Route::middleware([
        'web',
        'check.administrador.fornecedor:perfilfornecedor_consultar,acessar_dashboard_usuario_fornecedor'
    ])->group(function () {
        Route::get('/inicio/preposto', [UsuarioFornecedorCrudController::class, 'prepostoInicio'])
            ->name('fornecedor.preposto.inicio');
    });
});

Route::group([
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin'),
        (array) 'permission:V2_acessar'
    ),
    'prefix' => 'fornecedor/notificacao',
    'namespace'  => 'App\Http\Controllers',
], function () {
    Route::crud('/', 'NotificacoesFornecedorCrudController');
    Route::get('/{id}', 'NotificacoesFornecedorCrudController@acessarNotificao');
    Route::put('/alterarsituacao/{id}', 'NotificacoesFornecedorCrudController@alterarSituacao');
    Route::get('/alterarsituacaogeral/{alt_sit}', 'NotificacoesFornecedorCrudController@alterarSituacaoGeral');
});
