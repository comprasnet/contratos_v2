#!/bin/sh
php -f /var/www/html/contratos_v2/artisan optimize:clear
php -f /var/www/html/contratos_v2/artisan l5-swagger:generate

git fetch --all

sudo service cron start

# apachectl -D FOREGROUND
/usr/sbin/apache2ctl -D FOREGROUND
