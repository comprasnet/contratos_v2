#!/bin/bash
php -v
if [ $? -eq 0 ]
then
  if [[ $(sudo update-alternatives --list php) != *"php7.3"* ]]
  then
      echo "Desinstale todas as versões do PHP do seu WSL"
#      echo 'sudo apt-get purge `dpkg -l | grep php| awk '"'{print "'$'"2}'"' |tr "\n" " "`'
      exit 1
  fi
fi
sudo chown -R $(echo $USER):$(id -ng) vendor/
#sudo apt-get install -y software-properties-common
#sudo add-apt-repository -y ppa:ondrej/php
sudo apt-get update
sudo apt-get -y install lsb-release ca-certificates curl
sudo curl -sSLo /usr/share/keyrings/deb.sury.org-php.gpg https://packages.sury.org/php/apt.gpg
sudo sh -c 'echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
sudo apt-get update
sudo apt-get install -y php7.3 php7.3-curl php7.3-xml php7.3-gd php7.3-zip
mv composer.json composer.json.bkp
sed '/post-autoload-dump/,/],/d' composer.json.bkp > composer.json
php composer.phar install
mv composer.json.bkp composer.json
git config core.hooksPath .githooks