#!/bin/sh
php -f /var/www/html/contratos_v2/artisan migrate --force
php -f /var/www/html/contratos_v2/artisan clear-compiled 
php -f /var/www/html/contratos_v2/artisan optimize:clear

chmod -R ugo+rw /var/www/html/contratos_v2/storage
chmod -R ugo+rw /var/www/html/contratos_v2/bootstrap/cache

service cron start

/usr/sbin/apache2ctl -D FOREGROUND
