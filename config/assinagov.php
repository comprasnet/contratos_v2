<?php

return [
    'host' => env('ASSINAGOV_HOST'),
    'client_id' => env('ASSINAGOV_CLIENT_ID'),
    'secret' => env('ASSINAGOV_SECRET'),
    'api_assinatura' => env('ASSINAGOV_API_ASSINATURA'),
];
