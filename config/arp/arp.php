<?php

return [
    'tipo_inicial' => [
        'Ata de Registro de Preços',
        'Termo de Aditivo',
        'Termo de Apostilamento',
        'Termo de Extinção'
    ],
    'ano_minimo_arp' => 2023,
    'status_adesao_inicial' => [
        'Enviada para aceitação',
        'Aceita',
        'Cancelada',
        'Em elaboração',
        'Negada',
        // 'Estornada',
        'Aceito Parcial',
        'Ativa',
        'Item Não Avaliado',
        'Aceitar',
        'Aceitar Parcialmente',
        'Negar'
    ],
    'status_historico' => [
        'Ata Inicial',
        'Vigência',
        'Valor(es) registrado(s)',
        'Cancelamento de item(ns)',
        'Informativo',
        'Retificar'
    ],
    'status_remanejamento' =>
    [
        'Cancelado parcialmente',
        'Analisado pela unidade gerenciadora da ata',
        'Aguardando aceitação da unidade participante e gerenciadora',
        'Aguardando aceitação da unidade participante',
        'Aguardando aceitação da unidade gerenciadora',
        'Negado pela unidade participante',
        'Negado pela unidade gerenciadora da ata'
    ],
    'tipo_arquivo_pncp' =>
    [
        "Ata de Registro de Preços",
        "Termo Aditivo",
        "Termo de Apostilamento",
        "Termo de Extinção",
        "Outros","Alteração"
    ],
    'nome_queue' => 'compra_gestao_ata_v2',
    'nome_queue_relatorio' => 'relatorio_transparencia_gestao_ata_v2',
    'nome_queue_novo_divulgacao' => 'contratacao_novo_dc_gestao_ata_v2',
    'nome_queue_alteracao_arp_quantitativo' => 'alterar_vigencia_quantitativo_gestao_ata_v2',
    'resultado_lei' =>   '/MP1221/i'
];
