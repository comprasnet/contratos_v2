<?php
    // $rotaInicio = session()->get('tipo_acesso') == 'administrador'
    //     ? 'fornecedor/inicio/administrador'
    //     : 'fornecedor/inicio/preposto';

    return [
        'iniciofornecedor'  =>
            [
                //':rotaUm' => $rotaInicio,
                ':rotaUm' => 'fornecedor/inicio/administrador',
                ':iconeMenuUm' => 'fas fa-home',
                ':textoMenuUm' => 'Início',
                ':permissaoUm' => -1,
                ':nivelMenu' => 1
            ],
        'gestaocontratualfornecedor' =>
            [
                ':rotaDois' => 'javascript: void(0)',
                ':iconeMenuUm' => 'fas fa-file-contract',
                ':textoMenuUm' => 'Gestão Contratual',
                ':permissaoDois' => -1,
                ':nivelMenu' => 2,
                'itemMenuNivelDois' =>
                    [
                        'contratos2' =>
                            [
                                ':rotaDois' => 'fornecedor/contrato' ,
                                ':iconeMenuDois' => 'fas fa-file-contract',
                                ':textoMenuDois' => 'Contratos',
                                ':permissaoDois' => -1,
                                ':nivelMenu' => 2,
                            ],
                        'ordemdeservicofornecedor' =>
                            [
                                ':rotaDois' => 'fornecedor/autorizacaoexecucao' ,
                                ':iconeMenuDois' => 'fas fa-file-invoice',
                                ':textoMenuDois' => 'Ordens de Serviço / Fornecimento',
                                ':permissaoDois' => -1,
                                ':nivelMenu' => 2,
                            ],
                        'instrumentodecobrancafornecedor' =>
                            [
                                ':rotaDois' => 'fornecedor/contratoinstrumentodecobranca' ,
                                ':iconeMenuDois' => 'fas fa-wallet',
                                ':textoMenuDois' => 'Instrumentos de Cobrança',
                                ':permissaoDois' => -1,
                                ':nivelMenu' => 2,
                            ]
                    ]
            ],
       /* 'gestaocontratualfornecedor' =>
            [
                ':rotaUm' => 'fornecedor/contrato',
                ':iconeMenuUm' => 'fas fa-file-contract',
                ':textoMenuUm' => 'Contratos',
                ':permissaoUm' => -1,
                ':nivelMenu' => 1,
            ],*/
         'arpfornecedor'  =>
            [
                ':rotaUm' => 'fornecedor/arp',
                ':iconeMenuUm' => 'fas fa-file-invoice-dollar',
                ':textoMenuUm' => 'Gestão de Atas',
                ':permissaoUm' => -1 ,
                ':nivelMenu' => 1
            ],

        'transparenciafornecedor' =>
            [
                ':rotaDois' => 'javascript: void(0)',
                ':iconeMenuUm' => 'fas fa-file-contract',
                ':textoMenuUm' => 'Transparência',
                ':permissaoDois' => -1,
                ':nivelMenu' => 2,
                'itemMenuNivelDois' =>
                    [
                        'transparencia-v1' =>
                            [
                                ':rotaDois' => 'transparencia/v1' ,
                                ':iconeMenuDois' => 'fas fa-file-contract',
                                ':textoMenuDois' => 'Versão Original',
                                ':permissaoDois' => -1,
                                ':nivelMenu' => 2,
                                ':targetDois' => "_blank"
                            ],
                        'transparencia-v2' =>
                            [
                                ':rotaDois' => '/transparencia' ,
                                ':iconeMenuDois' => 'fas fa-file-contract',
                                ':textoMenuDois' => 'Nova versão',
                                ':permissaoDois' => -1,
                                ':nivelMenu' => 2,
                                ':targetDois' => "_blank"
                            ]
                    ]
            ],
    ];
