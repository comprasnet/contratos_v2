<?php

$uri = $_SERVER['REQUEST_URI'] ?? null;
# Se a navegação for pelo transparência
if (strpos($uri, '/transp') !== false) {
    return [
        'inicio'  =>
            [
                ':rotaUm' => 'transparencia',
                ':iconeMenuUm' => 'fas fa-home',
                ':textoMenuUm' => 'Início',
                ':permissaoUm' => -1,
                ':nivelMenu' => 1
            ],
        'arp'  =>
            [
                ':rotaUm' => 'transparencia/arp',
                ':iconeMenuUm' => 'fas fa-file-invoice-dollar',
                ':textoMenuUm' => 'Consultar Atas',
                ':permissaoUm' => -1 ,
                ':nivelMenu' => 1
            ],
        'compras'  =>
            [
                ':rotaUm' => 'transparencia/compras',
                ':iconeMenuUm' => 'fas fa-file-invoice-dollar',
                ':textoMenuUm' => 'Consultar Compras',
                ':permissaoUm' => -1,
                ':nivelMenu' => 1
            ],
        'arp-item'  =>
            [
                ':rotaUm' => 'transparencia/arp-item',
                ':iconeMenuUm' => 'fas fa-file-invoice-dollar',
                ':textoMenuUm' => 'Consultar Itens de Atas',
                ':permissaoUm' => -1 ,
                ':nivelMenu' => 1
            ],
        'gestaocontratual'  =>
            [
                ':rotaUm' => 'transparencia/v1',
                ':iconeMenuUm' => 'fas fa-file-contract',
                ':textoMenuUm' => 'Gestão Contratual',
                ':permissaoUm' => -1 ,
                ':nivelMenu' => 1
            ],
    ];
}

# Se a navegação for por dentro do sistema ao logar
return [
    # Item do menu Início
    'inicio'  =>
        [
            ':rotaUm' => 'dashboard' ,
            ':iconeMenuUm' => 'fas fa-home',
            ':textoMenuUm' => 'Início',
            ':permissaoUm' => -1,
            ':nivelMenu' => 1,
            ':targetUm' => "_blank"
        ],
    # Item do menu Contrato
    'contrato'  =>
        [
            ':rotaUm' => 'autenticarautomatica/gescon-contrato?situacao=[1]',
            ':iconeMenuUm' => 'fas fa-file-contract',
            ':textoMenuUm' => 'Gestão contratual',
            ':permissaoUm' => -1,
            ':nivelMenu' => 1
        ],
    # Item do menu Compras
    'compras' =>
        [
            ':rotaDois' => 'javascript: void(0)',
            ':iconeMenuUm' => 'fas fa-shopping-cart',
            ':textoMenuUm' => 'Compras',
            ':permissaoDois' => -1,
            ':nivelMenu' => 2,
                'itemMenuNivelDois' =>
                    [
                        'visualizar' =>
                            [
                                ':rotaDois' => 'compras' ,
                                ':iconeMenuDois' => 'fas fa-search',
                                ':textoMenuDois' => 'Visualizar',
                                ':permissaoDois' => -1,
                                ':nivelMenu' => 2
                            ]
                    ]
                    ],
    # Item do menu Fiscalização e Gestão de Contratos
    'fiscalizacao_gestaodecontratos'  =>
        [
            ':rotaUm' => 'meus-contratos?situacao=[1]',
            ':iconeMenuUm' => 'fas fa-tasks',
            ':textoMenuUm' => 'Fiscalização e Gestão de Contratos',
            ':permissaoUm' => 'fiscalizacao_V2_acessar',
            ':nivelMenu' => 1
        ],
    # Item do menu Gestão de Atas
    'arp'  =>
        [
            ':rotaDois' => 'javascript: void(0)',
            ':iconeMenuUm' => 'fas fa-file-invoice-dollar',
            ':textoMenuUm' => 'Gestão de Atas',
            ':permissaoUm' => 'gestaodeatas_V2_acessar',
            ':permissaoDois' => 'gestaodeatas_V2_acessar',
            ':nivelMenu' => 2,
                'itemMenuNivelDois' =>
                    [
                        'visualizar' =>
                            [
                                ':rotaDois' => 'arp',
                                ':iconeMenuDois' => 'fas fa-file-invoice-dollar',
                                ':textoMenuDois' => 'Ata de Registro de Preços',
                                ':permissaoDois' => -1,
                                ':nivelMenu' => 2
                            ],
                        'remanejamento' =>
                            [
                                ':rotaDois' => 'arp/remanejamento',
                                ':iconeMenuDois' => 'fas fa-plus',
                                ':textoMenuDois' => 'Solicitar Remanejamento',
                                ':permissaoDois' => -1,
                                ':nivelMenu' => 2
                            ],
                        'remanejamento_analisar' =>
                            [
                                ':rotaDois' => 'arp/remanejamento/analisar',
                                ':iconeMenuDois' => 'fas fa-diagnoses',
                                ':textoMenuDois' => 'Analisar Solicitação de Remanejamento',
                                ':permissaoDois' => -1,
                                ':nivelMenu' => 2
                            ],
                        'adesao_visualizar' =>
                            [
                                ':rotaDois' => 'arp/adesao',
                                ':iconeMenuDois' => 'fas fa-file-medical',
                                ':textoMenuDois' => 'Solicitar adesão',
                                ':permissaoDois' => -1,
                                ':nivelMenu' => 2
                            ],
                        'adesao_analisar' =>
                            [
                                ':rotaDois' => 'arp/adesao/analisar',
                                ':iconeMenuDois' => 'fas fa-diagnoses',
                                ':textoMenuDois' => 'Analisar solicitação de adesão',
                                ':permissaoDois' => -1,
                                ':nivelMenu' => 2
                            ],
                    ]
                ],
    'admin'  =>
        [
            ':rotaDois' => 'javascript: void(0)',
            ':iconeMenuUm' => 'fas fa-cogs',
            ':textoMenuUm' => 'Administração',
            ':permissaoUm' => 'admin_pncpv2_visualizar',
            ':permissaoDois' => 'admin_pncpv2_visualizar' ,
            ':nivelMenu' => 2,
                'itemMenuNivelDois' =>
                    [
                        'visualizar' =>
                            [
                                ':rotaDois' => 'admin/envia-dados-pncp',
                                ':iconeMenuDois' => 'fas fa-globe-americas',
                                ':textoMenuDois' => 'PNCP',
                                ':permissaoDois' => 'admin_pncpv2_visualizar',
                                ':nivelMenu' => 2
                            ],
                        'log' =>
                            [
                                ':rotaDois' => 'log',
                                ':iconeMenuDois' => 'fas fa-home',
                                ':textoMenuDois' => 'Log',
                                ':permissaoDois' => 'log_visualizar',
                                ':permissaoTres' => 'V2_acessar_desenvolvedor_log',
                                ':nivelMenu' => 2
                            ],
                        'phpinfo' =>
                            [
                                ':rotaDois' => 'infocontratos',
                                ':iconeMenuDois' => 'fab fa-php',
                                ':textoMenuDois' => 'PHP info',
                                ':permissaoDois' => 'log_visualizar',
                                ':nivelMenu' => 2,
                                ':targetDois' => "_blank",
                                ':bloquearAmbiente' => ['Ambiente Produção']
                            ]
                    ]
            ],
     'fornecedor-compras'  =>
        [
            //':rotaUm' => 'fornecedor-sessao/alterar-fornecedor/create',
            ':rotaUm' => 'fornecedor/alterar-fornecedor/create',
            ':iconeMenuUm' => 'fas fa-file-contract',
            ':textoMenuUm' => 'Fornecedor compras.gov',
            ':permissaoUm' => 'perfilfornecedor_consultar',
            ':nivelMenu' => 1
            //':bloquearAmbiente' => ['Ambiente Produção']
        ]
];

/**
 *
 *  Exemplo para nivel tres de menu
 *   'remanejamento' =>
 *   [
 *       ':rotaTres' => 'javascript: void(0)',
 *       ':iconeMenuTres' => 'fas fa-exchange-alt',
 *       ':textoMenuTres' => 'Remanejamento',
 *       ':permissaoTres' => -1,
 *       ':nivelMenu' => 3,
 *       'itemMenuNivelTres' =>
 *           [
 *               'visualizar' =>
 *                   [
 *                       ':rotaTres' => 'arp/remanejamento',
 *                       ':iconeMenuTres' => 'fas fa-plus',
 *                       ':textoMenuTres' => 'Solicitar Remanejamento',
 *                       ':permissaoTres' => -1
 *                   ],
 *               'analisarRemanejamento' =>
 *                   [
 *                       ':rotaTres' => 'arp/remanejamento/analisar',
 *                       ':iconeMenuTres' => 'fas fa-diagnoses',
 *                       ':textoMenuTres' => 'Analisar Solicitação de Remanejamento',
 *                       ':permissaoTres' => -1
 *                   ]
 *           ]
 *   ]
 *
 */
