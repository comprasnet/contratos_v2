<?php

return [
    //parametros para substituir pelos valores recuperados
    'iconeMenu' => ":iconeMenu",
    'textoMenu' => ":textoMenu",
    'rota' => ":rota",
    'permissao' => ":permissao",
    'itemMenu' => ":itemMenu",
    'target' => ":target",

    //Nivel de acesso do menu
    'um' => '<a class="menu-item divider" href=":rotaUm" :targetUm> <span class="icon"> <i class=":iconeMenuUm"></i> </span><span class="content">:textoMenuUm</span></a>',
    'dois' => '<div class="menu-folder">
                                <a class="menu-item" href="javascript: void(0)">
                                    <span class="icon">
                                        <i class=":iconeMenuUm" aria-hidden="true"></i>
                                    </span>
                                    <span class="content">
                                        :textoMenuUm
                                    </span>
                                </a>
                                <ul>
                                    :itemMenu
                                </ul>
                            </div>',

    //item do menu para o nivel de acesso
    'itemMenuDois' => ' <li>
                            <a class="menu-item" href=":rotaDois" :targetDois>
                                <span class="icon">
                                    <i class=":iconeMenuDois" aria-hidden="true"></i>
                                </span>
                                <span class="content">
                                    :textoMenuDois
                                </span>
                            </a>
                        </li>',


    'itemMenuTres' => ' <li>
                            <a class="menu-item" href="javascript: void(0)">
                                <span class="icon"><i class=":iconeMenuTres" aria-hidden="true"></i>
                                </span><span class="content">:textoMenuTres</span>
                            </a>
                        <ul>
                          :itemMenuTres
                        </ul>
                      </li>'
];
