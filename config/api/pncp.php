<?php

return [
    'base_uri' => env('API_PNCP_URL'),
    'endpoints' => [
        'inserir_ata' => 'v1/orgaos/{cnpj}/compras/{anoCompra}/{sequencialCompra}/atas',
        'retificar_ata' => 'v1/orgaos/{cnpj}/compras/{anoCompra}/{sequencialCompra}/atas/{sequencialAta}',
        'cancelar_ata' => 'v1/orgaos/{cnpj}/compras/{anoCompra}/{sequencialCompra}/atas/{sequencialAta}',
        'inserir_arquivo_ata'=> 'v1/orgaos/{cnpj}/compras/{anoCompra}/{sequencialCompra}/atas/{sequencialAta}/arquivos',
        'deletar_arquivo_ata' =>
        'v1/orgaos/{cnpj}/compras/{anoCompra}/{sequencialCompra}/atas/{sequencialAta}/arquivos/{sequencialDocumento}',
        'todas_atas' => 'v1/orgaos/{cnpj}/compras/{anoCompra}/{sequencialCompra}/atas',
        'recuperar_ata' => 'v1/orgaos/{cnpj}/compras/{anoCompra}/{sequencialCompra}/atas/{sequencialAta}',
        'recuperar_arquivo_ata' => 'v1/orgaos/{cnpj}/compras/{anoCompra}/{sequencialCompra}/atas/'.
                                    '{sequencialAta}/arquivos/{sequencialDocumento}',
    ],
    'fluxo_situacao' =>
    [
        'INCPEN'    => 'NUMSEQPEN',
        'NUMSEQPEN' => 'ARQPEN',
        'ARQPEN'    => 'SUCESSO',
        'RETPEN'    => 'SUCESSO',
        'INCARQ'    => 'SUCESSO',
        'ATUARQ'    => 'SUCESSO',
        'DELARQ'    => 'SUCESSO',
        'EXCPEN'    => 'SUCESSO',
        'ASNPEN'    => 'SUCESSO',
        'SUCESSO'   => 'SUCESSO',
    ]
];
