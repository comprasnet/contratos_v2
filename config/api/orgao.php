<?php

return [
    'url' => ENV('API_HOST_UNIDADES'),
    'servico' => [
        'orgaoPorCodigo' => 'v1/orgao/{numero}'
    ]
];
