<?php

return [
    'host' => env('API_HOST_USUARIO_FORNECEDOR', ''),
    'sessao_usuario' => 'internal/v2/sessao/fornecedor/{compras_id}/ip/{ip}',
];
