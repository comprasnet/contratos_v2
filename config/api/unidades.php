<?php

return [
    'url' => ENV('API_HOST_UNIDADES'),
    'servico' => [
        'uasg' => [
            'uasgPorCodigo' => 'v1/uasg/{numero}'
        ]
    ]
];
