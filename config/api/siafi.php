<?php
return [
    'prod' => [
        'url_consulta' => 'https://servicos-siafi.tesouro.gov.br/siafi:exercicio/services/tabelas/consultarTabelasAdministrativas?wsdl',
        'url_cpr' => 'https://servicos-siafi.tesouro.gov.br/siafi:exercicio/services/cpr/manterContasPagarReceber?wsdl',
        'url_orcamentario' => 'https://servicos-siafi.tesouro.gov.br/siafi:exercicio/services/orcamentario/manterOrcamentario?wsdl',
    ],
    'hom' => [
        'url_consulta' => 'https://homextservicos-siafi.tesouro.gov.br/siafi:exerciciohe/services/tabelas/consultarTabelasAdministrativas?wsdl',
        'url_cpr' => 'https://homextservicos-siafi.tesouro.gov.br/siafi:exerciciohe/services/cpr/manterContasPagarReceber?wsdl',
        'url_orcamentario' => 'https://homextservicos-siafi.tesouro.gov.br/siafi:exerciciohe/services/orcamentario/manterOrcamentario?wsdl',
    ],
    'soap_security' => 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
    'soap_utility'  => 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd',
    'soap_password' => 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'
];