![Logo](public/img/logo_mail.png)
## CONTRATOS.GOV.BR V2- Gestão Administrativa para Órgãos Públicos - GESTÃO DE ATAS DE REGISTRO DE PREÇOS

O [Contratos v2 - Gestão de Atas de Registro de Preços](https://contratos.sistema.gov.br/login) é o módulo Gestão de Atas é uma solução integrante do sistema [Contratos](https://gitlab.com/comprasnet/contratos), foi desenvolvido pelo Ministério da Gestão e da Inovação em Serviços Públicos (MGI) e possibilita aos órgãos e entidades da administração pública direta, autárquica e fundacional, incluindo as empresas estatais, estados e municípios, realizar de forma integrada a gestão das atas de registro de preços com amparo legal na Lei nº 14.133/2021, além de divulgar as informações e suas eventuais alterações no PNCP, em atendimento a citada lei.


A ferramenta promove a gestão de atas de registro de preços, conectando as unidades, divulgando e agregando transparência às atas de registro de preços e ações relacionadas, aprimorando as condições gerenciais envolvidas.

**Quem pode utilizar:**

Órgãos e entidades da administração pública federal direta, autárquica e fundacional, bem como as empresas estatais; e estados e municípios.


**Modelo de oferta do módulo:**

Disponibilizado de forma centralizada como parte do sistema Contratos, evitando a manutenção de outros sistemas afins, de forma isolada, estabelecendo medidas de eficiência organizacional para o aprimoramento da administração pública.

## Tecnologia Utilizada

* PHP 7.3
* Laravel 8.75
* Backpack  5.2.1
* Composer 2.3.4
* Postgres 12

A ferramenta é desenvolvida em PHP, utilizando  Framework Laravel versão 8.75.*

Essa ferramenta é Gratuita, e cada Instituição Pública poderá utilizá-la sem limites.
 
Caso o órgão queira implementar nova funcionalidade, pedimos que disponibilize esta para que outras instituições possa utilizar.

## Licença

A licença dessa ferramenta é [“Licença Pública Geral GNU (GPLv3)”](https://www.gnu.org/licenses/gpl-3.0.pt-br.html). Pedimos que toda implementação seja disponibilizada para a comunidade.

## Versões, Requisitos, Instalação e Configuração


[Ambiente de Desenvolvimento](https://gitlab.com/comprasnet/contratos/-/wikis/Ambiente-de-Desenvolvimento)

## Ajuda

Conheça nossa [Wiki](https://gitlab.com/comprasnet/contratos/-/wikis/home)!

## Atualização e Versões

As atualizações podem ser acompanhadas pelas [Versões](https://gitlab.com/comprasnet/contratos/-/tags)

