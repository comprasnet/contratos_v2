#!/bin/bash

vendor/bin/phpcbf --runtime-set testVersion $1 $2

if [ $? -eq 1 ]
then
    git config user.name "Contratos Gitlab Runner"
    git config user.email "contratos.gitlabrunner@gitlab.com"
    git remote add gitlab_origin https://oauth2:$3@gitlab.com/$4.git
    git commit -am "Correção Linter"
    git push gitlab_origin HEAD:$5
fi
