#!/bin/bash

vendor/bin/phpcs --runtime-set testVersion $1 $2

if [ $? -eq 0 ]
then
    if [ $8 -ne 15183971 ]
    then
        body="**LINTER SUCCESS**"
        curl --request POST --header "PRIVATE-TOKEN: $3" "https://gitlab.com/api/v4/projects/$4/merge_requests/$6/notes?body=${body// /%20}"
    fi
    exit 0
else
    if [ $8 -ne 15183971 ]
    then
        body="**LINTER ERROR** @$5 Verificar relatorio do linter em $7/artifacts/download?file_type=archive"
        curl --request POST --header "PRIVATE-TOKEN: $3" "https://gitlab.com/api/v4/projects/$4/merge_requests/$6/notes?body=${body// /%20}"
    fi
    exit 1
fi
