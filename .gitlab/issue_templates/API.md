## Solicitação de API (Evolução de Sistema)

<!--OS COMENTÁRIOS NÃO APARECERÃO NA ISSUE
No Título coloque no início entre colchetes o módulo ou fluxo a que se refere Ex. [API]. -->

## Origem
<!-- Forneça links para chamados relacionados, reuniões, e-mails ou outras ocasiões que tenham inspirado esta solicitação de melhoria -->

## Descrição
<!-- Descreva detalhadamente a melhoria desejada, seguindo a estrutura de "Como - Quero - Para". Utilize imagens, se necessário, para tornar a análise e a solução mais compreensíveis -->

**Como**

**Quero**

**Para** 

## Critérios de Aceitação

1. **Solicitação** <! Forneça a solicitação (Criar API, Editar API) -->

1.1 **Nome da API** <!-- Informe o nome da API -->

1.2 **Método para API** <!-- Método HTTP para utilizar na API (GET (Consulta) , POST (Envia), PUT (Atualiza), DELETE (Exclui)) -->

1.3 **URI do recurso** <!-- Endpoint para API (/api/v1/{funcionalidade}/{tipo do método}) -->

1.4 **Dados de Entrada** <!-- Quais dados serão recebido na API -->

| Id| Campo         | Tipo                         | Obrigatório      | Descrição          |
|---|---------------|------------------------------|------------------|--------------------|
| 1 | <!-- Nome --> | <!-- Tipo (String) -->       | <!-- Sim/Não --> | <!-- Descrição --> |
| 2 | <!-- Nome --> | <!-- Tipo (Integer) -->      | <!-- Sim/Não --> | <!-- Descrição --> |
| 3 | <!-- Nome --> | <!-- Tipo (Array) -->        | <!-- Sim/Não --> | <!-- Descrição --> |
|3.1| <!-- Nome --> | <!-- Tipo (String) -->       | <!-- Sim/Não --> | <!-- Descrição --> |
| 4 | <!-- Nome --> | <!-- Tipo (Number (15,2) --> | <!-- Sim/Não --> | <!-- Descrição --> |
| 5 | <!-- Nome --> | <!-- Tipo (Date (8) -->      | <!-- Sim/Não --> | <!-- Descrição --> |
| 6 | <!-- Nome --> | <!-- Tipo (Objeto) -->       | <!-- Sim/Não --> | <!-- Descrição --> |
|6.1| <!-- Nome --> | <!-- Tipo (Boolean) -->      | <!-- Sim/Não --> | <!-- Descrição --> |

1.5 **Dados de Retorno** <!-- Quais dados serão enviados pela API -->

| Id| Campo         | Tipo                         | Obrigatório      | Descrição          |
|---|---------------|------------------------------|------------------|--------------------|
| 1 | <!-- Nome --> | <!-- Tipo (String) -->       | <!-- Sim/Não --> | <!-- Descrição --> |
| 2 | <!-- Nome --> | <!-- Tipo (Integer) -->      | <!-- Sim/Não --> | <!-- Descrição --> |
| 3 | <!-- Nome --> | <!-- Tipo (Array) -->        | <!-- Sim/Não --> | <!-- Descrição --> |
|3.1| <!-- Nome --> | <!-- Tipo (String) -->       | <!-- Sim/Não --> | <!-- Descrição --> |
| 4 | <!-- Nome --> | <!-- Tipo (Number (15,2) --> | <!-- Sim/Não --> | <!-- Descrição --> |
| 5 | <!-- Nome --> | <!-- Tipo (Date (8) -->      | <!-- Sim/Não --> | <!-- Descrição --> |
| 6 | <!-- Nome --> | <!-- Tipo (Objeto) -->       | <!-- Sim/Não --> | <!-- Descrição --> |
|6.1| <!-- Nome --> | <!-- Tipo (Boolean) -->      | <!-- Sim/Não --> | <!-- Descrição --> |

1.6 **Mensagens de Retorno** <!-- Quais mensagens de retorno a API vai ter para tratamento dos dados recebidos -->

| Mensagem                                  | Tipo                  | Descrição          |
|-------------------------------------------|-----------------------|--------------------|
|<!-- Mensagem que será retornada na API -->| <!-- Sucesso/Erro --> | <!-- Descrição --> |
|<!-- Mensagem que será retornada na API -->| <!-- Sucesso/Erro --> | <!-- Descrição --> |
|<!-- Mensagem que será retornada na API -->| <!-- Sucesso/Erro --> | <!-- Descrição --> |

2. **Atualizar documentação da API no Swagger** <! Se houver criação ou alteração deve atualizar o swagger -->

## Observações para o Desenvolvedor

* Observação 1: <!-- Adicione observações específicas para os desenvolvedores -->
* Observação 2: <!-- Outras observações relevantes -->

## Roteiro de Testes

**Dado que** <!-- Descreva a condição inicial para os testes -->

**Quando** <!-- Descreva a ação ou evento que será testado -->

**Então** <!-- Descreva o resultado esperado após a ação ou evento -->

<!--NÃO REMOVER AS LINHAS A SEGUIR-->
/label ~"tipo::Melhoria"