jQuery.InsertTableToDataTable = (options) => {
    let opt = $.extend({
        table: 'table',
        repeatElement: '.repeatable-element',
        repeatableButton: '.add-repeatable-element-button',
        fields: [],
        initialData: [],
        columns: [],
        detail: true,
        hasRemove: true,
        removeMessage: 'Deseja remover esse registro?',
        select2: false,
        onDetailTableItem: function(){},
        onCreateRow:function(colls){
            return colls;
        },
        onAddedRow: function () {},
        onPreRemove: function () {
            return true;
        },
        onPosRemove: function () {
        },
        onCreateItem: function () {
        },
        onAddTableItem: function () {
        },
        onRemoveItem: function () {
        }
    }, options);
    let that = this;


    $(opt.repeatableButton).hide();
    $(opt.repeatElement).hide();



    if (opt.detail) {
        opt.columns = [{
            class: 'dtr-control',
            orderable: false,
            data: null,
            defaultContent: '',
        }].concat(opt.columns);
    }

    if (opt.hasRemove) {
        opt.columns.push({
            class: 'remove-control',
            orderable: false,
        })
    }


    let detailItem = (td) => {
        let tr = $(td).closest('tr');
        let row = that.insertTable.row(tr);

        if (row.child.isShown()) {
            tr.removeClass('parent');
            tr.removeClass('details');
            row.child.hide();

        } else {
            tr.addClass('parent');
            tr.addClass('details');
            let uid = tr.data('uid');
            let element = $('.repeatable-element[data-uid="' + uid + '"]');
            let htmlForm = element.html();
            row.child("<div class='row'  data-uid='" + uid + "'>" + htmlForm + "</div>", 'no-padding').show();
            setByElement(element, tr.next());
            opt.onDetailTableItem(tr.next());
        }
    }

    let setByElement = (elementRepeater, trDEtails) => {
        let itemParent,item;
        for(let i = 0; i < opt.fields.length; i++) {
            item = $(trDEtails).find('.'+opt.fields[i]);
            if(['INPUT','TEXTAREA','SELECT'].includes(item[0].tagName)){
                item.parents('[bp-field-name]').remove()
            }else{
                item.remove();
            }
        }

        $(trDEtails).find('input,select').each(function () {
            let name = $(this).attr('name');
            $(this).removeAttr('name');
            $(this).attr('data-name', name);
            $(this).val(elementRepeater.find('[name="' + name + '"]').val());
        })
        $(trDEtails).find('input,select').bind('keyup keydown blur', function () {
            let name = $(this).data('name');
            elementRepeater.find('[name="' + name + '"]').val($(this).val());
        })
        $(trDEtails).find('.delete-element').remove();
    }

    let addLastInsertField = () => {
        let repeatElementObj = $(opt.repeatElement).last();
        return addField(repeatElementObj)
    }
    let addField = (repeatElementObj) => {

        let arrayColls = [];

        let itemObj, label;

        if (opt.detail) {
            arrayColls = [''];
        }

        for (let item of opt.fields) {
            arrayColls.push(
                getHtmlFieldAndRemoveByClass(item)
            );
        }
        arrayColls = opt.onCreateRow(arrayColls);

        if (opt.hasRemove) {
            let removeBtn = $('<button type="button"/>');
            let icon = $('<i/>');
            icon.addClass('fas fa-trash');

            removeBtn.addClass('br-button circle secondary mr-3');
            removeBtn.append(icon)
            arrayColls.push(removeBtn[0].outerHTML);
        }
        let rowAdded = that.insertTable.row.add(arrayColls).draw().node();

        let uid = Math.random().toString(16).slice(2);
        let insertedTr = $(opt.table).find('tbody tr').last();
        if (opt.select2) {
            insertedTr.find('select').each(function () {
                $(this).select2({
                    minimumResultsForSearch: Infinity
                })
            })
        }
        insertedTr.data('uid', uid);

        insertedTr.find('.dtr-control').on('click', function () {
            detailItem(this);
        });

        insertedTr.find('.remove-control').on('click', function () {
            if (confirm(opt.removeMessage)) {
                let tr = $(this).closest('tr');
                that.removeRow(tr);
            }
        });
        rowAddedTrigger(rowAdded);

        repeatElementObj.hide();
        repeatElementObj.attr('data-uid', uid);
        return rowAdded;
    }

    let rowAddedTrigger = (rowAdded)=>{
        $(rowAdded).find('input,select,textarea').each(function(){
            let name = $(this).attr('data-name');
            $('[name="' + name + '"]').val($(this).val());
            $(this).on('change keyup',function(){
                $('[name="' + name + '"]').val($(this).val());
            });
        })
        opt.onAddedRow(rowAdded);
    }

    let getHtmlFieldAndRemoveByClass = (item) => {
        let repeatElementObj = $(opt.repeatElement).last();
        let itemObj = repeatElementObj.find('.' + item);

        itemObj = getOnlyFieldIfSelect(itemObj);

        let html = itemObj[0].outerHTML;
        let htmlObj = $(html);
        let htmlName = $(htmlObj).attr('name');
        htmlObj.removeAttr('name');
        htmlObj.attr('data-name', htmlName);
        html = htmlObj[0].outerHTML;
        return html;
    }

    let getOnlyFieldIfSelect = (itemObj) => {
        let select = itemObj.find("select");
        if (select.length > 0) {
            select.show();
            select.removeClass('select2-hidden-accessible');
            select.addClass('form-control');
            return select;
        }
        return itemObj;
    }

    let removeFormGroupField = (itemObj) => {
        if (!itemObj.hasClass('form-group')) {
            removeFormGroupField(itemObj.parent());
        }
        itemObj.remove();
    }

    let dataTableOpts = {
        data: opt.initialData,
        columns: opt.columns,
        paging: false,
        searching: false,
        ordering: false,
        language:{
            "zeroRecords": "Nenhum item adicionado",
            "info": "Mostrando _TOTAL_ itens",
            "infoEmpty": "Sem registros adicionados"
        }
    };


    this.insertTable = $(opt.table).DataTable(dataTableOpts);
    this.setLoading = (loading) => {
        this.insertTable.processing(loading);
    }

    this.addItem = () => {
        $(opt.repeatableButton).trigger('click');

        const elementCreated = $(opt.repeatElement).last();
        const index = $(opt.repeatElement).length - 1;
        const repeatableName = elementCreated.attr('data-repeatable-identifier');

        setTimeout(function () {
            elementCreated.children().each(function(i, element) {
                $(element).find('input, select, textarea').each(function(i, el){
                    reindexElementsNames(el, index, repeatableName);
                })
            })
        }, 500)

        let repeatElementObj = $(opt.repeatElement).last();
        opt.onCreateItem(repeatElementObj, this);
        let rowadded = addLastInsertField();
        opt.onAddTableItem(this);
        return rowadded;
    }

    this.removeAllRows = (rowsTable) => {
        rowsTable.each(function (i, tr) {
            that.removeRow($(tr))
        })
    }

    this.removeRow = (tr) => {
        let uid = tr.data('uid');
        let element = $('.repeatable-element[data-uid="' + uid + '"]');

        let row = that.insertTable.row(tr);
        if (opt.onPreRemove(element)) {
            element.remove();
            row.remove().draw();
            opt.onPosRemove();
        }
    }

    this.loadAddedRepeaters = (onAddItem) => {
        let totalRepeater = $(opt.repeatElement).length;
        let counterRepeater = 0
        $(opt.repeatElement).each(function(){
            counterRepeater++;
            let rowadded = addField($(this));
            if(onAddItem){
                onAddItem(rowadded,$(this),counterRepeater===1,counterRepeater===totalRepeater,counterRepeater-1);
            }
        });
        $(opt.repeatableButton).trigger('click');
        $(opt.repeatElement).last().hide();
        setTimeout(() =>{
            $(opt.repeatElement).last().find('.delete-element').trigger('click')
        },1000);
    }

    this.getRepeatElement = () => {
        return $(opt.repeatElement)
    }


    return this;
}
