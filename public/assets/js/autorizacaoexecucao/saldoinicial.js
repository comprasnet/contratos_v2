(async function () {

    const contratoId = $('.contrato-id').val();
    const fieldVigenciaInicio = $('.vigencia_inicio');
    const fieldVigenciaFim = $('.vigencia_fim');
    const fieldQuantidade = $('.quantidade input');
    const fieldValorTotal = $('.valor-total input');
    const fieldValorUnitario = $('.valor-unitario input');


    $.blockUI({message: $("#loadingContratos")});

    //retorna o tipo do item de contrato
    const getTipoContratoItem = async () => {
        let contratoItem = $('.contrato-item select').val();
        if (contratoItem && contratoItem !== '') {

            let response = await fetch('/autorizacaoexecucao/' + contratoId + '/contrato-item/tipo/' + contratoItem);
            let json = await response.json();
            toggleByTipo(json.tipo);
        }
    }

    //retorna o valor do item de contrato
    const getValor = async () => {
        let contratoItem = $('.contrato-item select').val();
        if (contratoItem && contratoItem !== '') {

            let response = await fetch('/autorizacaoexecucao/' + contratoId + '/contrato-item/valor/' + contratoItem);
            let json = await response.json();
            return parseFloat(json.valor.toString());
        }
        return 0;
    }

    //exibe ou esconde os campos conforme o tipo
    const toggleByTipo = (tipo) => {
        if (tipo === 'MATERIAL') {
            fieldVigenciaInicio.show();
            fieldVigenciaFim.show();
            return;
        }
        fieldVigenciaInicio.hide();
        fieldVigenciaFim.hide();
        fieldVigenciaInicio.find('input').val('');
        fieldVigenciaFim.find('input').val('');
    }

    const changeStepQuantidadeByTipo = (tipo) => {
        if (tipo === 'MATERIAL') {
            fieldQuantidade.attr('step', '0.00000000000000001')
            return;
        }
        fieldQuantidade.attr('step', '0')
    }

    const calculateValorTotal = async () => {
        let valorUnitario = $('.valor-unitario input').val();
        let quantidade = parseFloat(fieldQuantidade.val());
        let total = valorUnitario * quantidade;

        if (isNaN(total)) {
            total = '';
        }
        fieldValorTotal.val(total);
    }

    const setValorUnitario = async (sobreescrever=true) => {
        if(!sobreescrever && fieldValorUnitario.val()!==''){
            return;
        }
        let valorUnitario = await getValor();
        fieldValorUnitario.val(valorUnitario);
    }


    $(async function () {
        $('.contrato-item select').change(async function () {
            $.blockUI({message: $("#loadingContratos")});
            await getTipoContratoItem();
            await setValorUnitario();
            $.unblockUI();
        });
        [fieldQuantidade,fieldValorUnitario].forEach(element => element.on('change keyup',function(){
            calculateValorTotal();
        }))

        await setValorUnitario(false);
        await calculateValorTotal();
        await getTipoContratoItem();
        $.unblockUI();
    })
})();


(function () {
    let stripZeros = (str) => {
        return str.replace(/(^0+(?=\d))|(,?0+$)/g, '');
    }

    $('#crudTable').on('draw.dt', function () {
        $('#crudTable').find('tbody tr').each(function () {
            let span = $(this).find('td').eq(0).find('span');
            let value = span.html().trim();
            span.html(stripZeros(value));
        })
    });
})()