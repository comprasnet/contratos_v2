(async function () {

    const contratoId = $('.contrato-id').val();
    const tipoItem = $('.tipo_item').val();
    const fieldQuantidade = $('.quantidade input');
    const fieldQuantidadeDisponivel = $('.quantidade_disponivel input')
    if (tipoItem === 'SERVIÇO') {
        fieldQuantidade.decimal();
    } else {
        fieldQuantidade.integer()
    }
    const fieldValorTotal = $('.valor-total input');
    const fieldValorUnitario = $('.valor-unitario input');

    $.blockUI({message: $("#loadingContratos")});

    //retorna o valor do item de contrato
    const getValor = async () => {
        let contratoItem = $('.contrato-item select').val();
        if (contratoItem && contratoItem !== '') {

            let response = await fetch('/autorizacaoexecucao/' + contratoId + '/contrato-item/valor/' + contratoItem);
            let json = await response.json();
            return parseFloat(json.valor.toString());
        }
        return 0;
    }

    const getQuantidade = async () => {
        let contratoItem = $('.contrato-item select').val();
        if (contratoItem && contratoItem !== '') {

            let response = await fetch('/autorizacaoexecucao/' + contratoId + '/contrato-item/quantidade/' + contratoItem);
            let json = await response.json();
            return parseFloat(json.quantidade.toString());
        }
        return 0;
    }

    const calculateValorTotal = async () => {
        let valorUnitario = $('.valor-unitario input').val();
        let quantidade = $.ptBRToFloat(fieldQuantidade.val());
        let total = valorUnitario * quantidade;

        if (isNaN(total)) {
            total = '';
        } else {
            total = total.toFixed(2);
        }
        fieldValorTotal.val(total);
    }

    const validaQuantidade = () => {
        const quantidade = $.ptBRToFloat(fieldQuantidade.val());
        const quantidadeDisponivel = $.ptBRToFloat(fieldQuantidadeDisponivel.val());

        if (quantidade > quantidadeDisponivel) {
            exibirAlertaNoty('warning-custom', 'Quantidade inserida é maior que a quantidade disponível no contrato!');
        }
    }

    const setValorUnitario = async (sobreescrever=true) => {
        if(!sobreescrever && fieldValorUnitario.val()!==''){
            return;
        }
        let valorUnitario = await getValor();
        fieldValorUnitario.val(valorUnitario.toFixed(2));
    }

    $(async function () {
        $('.contrato-item select').change(async function () {
            $.blockUI({message: $("#loadingContratos")});
            await setValorUnitario();
            fieldQuantidadeDisponivel.val(await getQuantidade());
            $.unblockUI();
        });

        [fieldQuantidade, fieldValorUnitario].forEach(element => element.on('keyup change',function(){
                calculateValorTotal();
                if (element === fieldQuantidade) {
                    validaQuantidade();
                }
            })
        );

        await setValorUnitario(false);
        await calculateValorTotal();
        fieldQuantidadeDisponivel.val(await getQuantidade());
        $.unblockUI();
    })
})();
