function exibirInformacoesOSF(autorizacaoExecucaoId, url, numeroContrato, numeroOsf) {
    $("#modalquadroresumo").addClass( "active" );
    $("#modalquadroresumo .br-modal-header").text( `Quadro resumo OS/F ${numeroOsf} do contrato ${numeroContrato}`);

    limparCampo()

    $.ajax({
        type: 'GET',
        url: `${url}/${autorizacaoExecucaoId}`,
        success: function (response) {
            //Qtde. Solicitada (OS/F)
            $("#qtd_solicitada_osf").html(response.qtd_solicitaca_osf.qtd_total)
            $("#qtd_solicitada_valor_glosa_osf").html(response.qtd_solicitaca_osf.valor_glosa)
            $("#qtd_solicitada_valor_total_osf").html(response.qtd_solicitaca_osf.valor_total)

            //Qtde. em Análise (Até TRP)
            $("#qtd_total_ate_trp_osf").html(response.qtd_em_analise_ate_trp.qtd_total)
            $("#valor_glosa_ate_trp_osf").html(response.qtd_em_analise_ate_trp.valor_glosa)
            $("#valor_total_ate_trp_osf").html(response.qtd_em_analise_ate_trp.valor_total)

            //Qtde. em Avaliação (Até TRD)
            $("#qtd_total_ate_trd_osf").html(response.qtd_em_analise_ate_trd.qtd_total)
            $("#valor_glosa_ate_trd_osf").html(response.qtd_em_analise_ate_trd.valor_glosa)
            $("#valor_total_ate_trd_osf").html(response.qtd_em_analise_ate_trd.valor_total)

            //Qtde. Executada (Após TRD)
            $("#qtd_total_apos_trd_osf").html(response.qtd_em_analise_apos_trd.qtd_total)
            $("#valor_glosa_apos_trd_osf").html(response.qtd_em_analise_apos_trd.valor_glosa)
            $("#valor_total_apos_trd_osf").html(response.qtd_em_analise_apos_trd.valor_total)

            //Saldo a Executar
            $("#qtd_total_saldo_executar").html(response.saldo_executar.qtd_total)
            $("#valor_glosa_saldo_executar").html(response.saldo_executar.valor_glosa)
            $("#valor_total_saldo_executar").html(response.saldo_executar.valor_total)
        },
        error: function () {
            exibirAlertaNoty('error-custom', 'Erro ao carregar as informações')
        }
    })
}

$("#btn_fechar_modal_cancelamento").click(function() {
    $("#modalquadroresumo").removeClass( "active" );
    $("#justificativa_cancelamento").val('')
})

function limparCampo() {

    let campos = [
        'qtd_solicitada_osf',
        'qtd_solicitada_valor_glosa_osf',
        'qtd_solicitada_valor_total_osf',
        'qtd_total_ate_trp_osf',
        'valor_glosa_ate_trp_osf',
        'valor_total_ate_trp_osf',
        'qtd_total_ate_trd_osf',
        'valor_glosa_ate_trd_osf',
        'valor_total_ate_trd_osf',
        'qtd_total_saldo_executar',
        'valor_glosa_saldo_executar',
        'valor_total_saldo_executar'
    ]

    let carregando = 'Carregando.'
    $.each(campos, function(index, value) {
        $(`#${value}`).html(carregando)
    });
}