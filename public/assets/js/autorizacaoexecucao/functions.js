const toggleOptions = function (e, id) {
    e.preventDefault();

    // Toggle selection only if clicked with the mouse
    if (e.which === 1) { // Check if left mouse button (1) is clicked
        let option = $(e.target);
        let allOptions = $(`${id} option`);

        if (option.val() === '') {
            // Toggle em todas opções quando clica na opção todos
            allOptions.prop("selected", !option.prop("selected"));
        } else {
            // Garante que opção 'Todos' fica desmarcada caso selecione uma opção individual
            allOptions.first().prop('selected', false)
            option.prop("selected", !option.prop("selected"));
        }
    }
}

$(document).on('click', '.download-pdf', function (e) {
    selectResponsaveis(() => {
        const queryString  = $.param({
            type: 'assinatura',
            assinantes: $('#modal_prepostos_responsaveis_ids').val()
        });
        window.location.href = $(this).data('url') + '?' + queryString;

        setTimeout(function () {
            swal.close()
        }, 4000);
    })
})
