let idCancelamento = null;
let urlCancelamento = null;

function cancelarOpm(opmId, numeroDeclaracao, urlCancelamentoParam) {
    let idCancelamento = opmId;
    let urlCancelamento = urlCancelamentoParam;

    $("#modalcancelaropm").addClass("active");
    $("#modalcancelaropm .br-modal-header").text(`Cancelar opm ${numeroDeclaracao}`);

    $("#btn_realizar_cancelamento").click(function() {

        let justificativaCancelamento = $("#justificativa_cancelamento").val();

        if (justificativaCancelamento === '') {
            exibirAlertaNoty('warning-custom', 'Informe uma justificativa para realizar o cancelamento');
            return;
        }
        $.blockUI({ message: $('#loadingContratos') });

        $.post(urlCancelamento, { opmId: idCancelamento, justificativaCancelamento })
            .done(function(response) {
                exibirAlertaNoty(`${response.type}-custom`, response.message);

                if (response.type === 'success') {
                    $('#crudTable').DataTable().ajax.reload();
                    $.unblockUI();
                    $("#modalcancelaropm").removeClass("active");
                }
            })
            .catch((error) => {
                $.unblockUI();
                let objetoErro = error.responseJSON.errors;
                Object.keys(objetoErro).forEach(function(chave) {
                    exibirAlertaNoty('error-custom', objetoErro[chave]);
                });
            });
    });
}
$("#btn_fechar_modal_cancelamento").click(function() {
    $("#modalcancelaropm").removeClass("active");
    $("#justificativa_cancelamento").val('');
});

let isDraft = false;
// Variável responsável em bloquear o envio do form
$(".button-custom").click(function(event) {
    // Obtenha os valores dos campos
    let quantidadeFamiliar = $("#quantidade_familiar").val();
    let quantidadeContratadas = $("#quantidade_contratadas").val();
    let percentualMinimo = $("#percentual_minimo").val();
    let dataAssinatura = $("#data_assinatura_opm").val();
    let sequencial = $("#sequencial-input").val();
    let contratoResponsavel = $("#contrato_responsavel_id").val();
    isDraft = $(this).attr('id') === 'rascunho';

    let dataParts = dataAssinatura.split('/');
    let dataAssinaturaDate = new Date(dataParts[2], dataParts[1] - 1, dataParts[0]);
    let hoje = new Date();
    let dataInicioDecreto = new Date(2023, 2, 30);
    // 30 de março de 2023 (Mês é 2 porque é zero-indexado)



    let fileInput = document.getElementById('anexo_emitida_orgao');
    let anexoEnviado = fileInput.files.length > 0 || $('#anexo_emitida_orgao').data('exists');
    let fileUrl = fileInput.getAttribute('data-url');


console.log(fileUrl)
    $.blockUI({ message: $('#loadingContratos') });

    if(!isDraft){
        if (dataAssinaturaDate > hoje) {
            exibirAlertaNoty('warning-custom', 'Não é possível inserir uma data da declaração com a data futura.');
            bloquearSubmit = true;
            $.unblockUI();
            return;
        }
        if (dataAssinaturaDate < dataInicioDecreto) {
            exibirAlertaNoty('warning-custom', 'Não é possível inserir uma data da declaração anterior à data de início da vigência do Decreto 11.430/2023.');
            bloquearSubmit = true;
            $.unblockUI();
            return;
        }


        if (quantidadeFamiliar == "") {
            exibirAlertaNoty('warning-custom', 'Informe a quantidade de vagas para mulheres em situação de violência doméstica e familiar');
            bloquearSubmit = true
            $.unblockUI();
            return
        }

        if (quantidadeContratadas == "") {
            exibirAlertaNoty('warning-custom', 'Informe a quantidade de mulheres em situação de violência doméstica e familiar contratadas');
            bloquearSubmit = true
            $.unblockUI();
            return
        }
        if (percentualMinimo == "") {
            exibirAlertaNoty('warning-custom', 'Responda se a empresa contratada está cumprindo o percentual mínimo de 8% ');
            bloquearSubmit = true
            $.unblockUI();
            return
        }

        if (dataAssinatura == "") {
            exibirAlertaNoty('warning-custom', 'Informe a Data da declaração');
            bloquearSubmit = true
            $.unblockUI();
            return
        }
        if (!anexoEnviado && !fileUrl) {
            exibirAlertaNoty('warning-custom', 'Anexar a declaração emitida pelo órgão');
            bloquearSubmit = true
            $.unblockUI();
            return
        }



        if (contratoResponsavel == "") {
            exibirAlertaNoty('warning-custom', 'Informe o responsavel pela assinatura da declaração');
            bloquearSubmit = true
            $.unblockUI();
            return
        }
        if (sequencial == "" ) {
            exibirAlertaNoty('warning-custom', 'Informe o numero da declaração');
            bloquearSubmit = true
            $.unblockUI();
            return
        }
    }
      bloquearSubmit = false
});


// $(document).ready(function() {
//     $('#quantidade_familiar, #quantidade_contratadas').on('input', function() {
//         this.value = this.value.replace(/[^0-9]/g, '');
//     });
//
//     // Impede a inserção de caracteres não numéricos durante a digitação
//     $('#quantidade_familiar, #quantidade_contratadas').on('keydown', function(e) {
//         if (!((e.keyCode >= 48 && e.keyCode <= 57) || // números na linha superior do teclado
//             (e.keyCode >= 96 && e.keyCode <= 105) || // números no teclado numérico
//             e.keyCode === 8 || // backspace
//             e.keyCode === 46 || // delete
//             e.keyCode === 37 || // seta para a esquerda
//             e.keyCode === 39 || // seta para a direita
//             e.keyCode === 9)) { // tab
//             e.preventDefault();
//         }
//     });
//
//     var lastValue = ''; // Variável para armazenar o último valor do campo
//
//     $('#sequencial-input').on('focus', function() {
//         // Armazena o valor atual do campo quando o foco é recebido
//         lastValue = $(this).val().trim();
//     });
//
//     $('#sequencial-input').on('blur', function() {
//
//         var value = $(this).val().trim();
//
//         // Verifica se o valor tem menos de 5 caracteres
//         if (value.length < 5) {
//             // Completa com zeros à esquerda até 5 dígitos
//             value = value.padStart(5, '0');
//         }
//
//         // Verifica se o valor é composto apenas por zeros
//         if (/^0+$/.test(value)) {
//             // Substitui o valor por '00001' se for composto apenas por zeros
//             value = '00001';
//         }
//
//         // Verifica se o valor é diferente do último valor armazenado
//         if (value !== lastValue) {
//             // Atualiza o valor no campo apenas se houver alteração
//             $(this).val(value);
//         }
//
//     });
// });










