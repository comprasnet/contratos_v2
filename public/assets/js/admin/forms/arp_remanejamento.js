let quantidadeSolicitadaInserida = []
let quantidadeSolicitadaInseridaPorItem = []
let currentLocation = window.location;
let urlBuscaItem = $("#url_buscar_item_remanejamento").val()
// Inicia a variável para verificar se existe algum valor preenchido
var quantidadeLancada = []
var quantidadeAtaLancada = []
let dadosDataSetInputQuantidadeSolicitada = []

// Se a tela for edição, então inclui o id da ata para não deixar incluir o
// campo input file novamente
if (currentLocation.pathname.includes("edit")) {
    let idUnicoUpdate = $("#idUnicoHidden").val()

    // variável idSelecionado está sendo criada no arquivo upload_file_generico
    idSelecionado.push(parseInt(idUnicoUpdate))
    quantidadeLancada.push(parseInt(idUnicoUpdate))
    $("#qtd_ata_selecionada").val(quantidadeLancada.length)
    $(document).ready(function () {
        filtrarItem()
    });
}

// Método responsável em filtrar os itens
function filtrarItem() {
    // Recupera os valores que foram preenchidos no filtro
    let dadosFormFiltro = montarDadosBuscarItem()

    let todosFiltrosEmBranco = todosValoresNulosOuIndefinidos(dadosFormFiltro)

    if (todosFiltrosEmBranco) {
        exibirAlertaNoty('warning-custom', 'Selecionar ao menos um item dos filtros para realizar a busca')
        return
    }

    inserirLinhaItem(dadosFormFiltro)
    if (!currentLocation.pathname.includes("edit")) {
        $(".br-upload").each(function () {
            $(this).remove()
        })

        quantidadeSolicitadaInserida = []
        quantidadeSolicitadaInseridaPorItem = []
        quantidadeLancada = []
        quantidadeAtaLancada = []
    }
}


// Ação do clique do botão para buscar os itens para o remanejamento
$("#btn_recuperar_item_remanejamento").click(function () {
    filtrarItem()
})

// Método responsável em recuperar as informações informadas pelo usuário para
// enviar na busca
function montarDadosBuscarItem() {
    let numeroAnoCompra = $('select[name="numero_compra"]').val()
    let modalidadeCompra = $('select[name="modalidade_compra"]').val()
    let unidadeCompra = $('select[name="unidade_compra"]').val()
    let numeroAta = $('select[name="numero_ata"]').val()
    let idRemanejamento = $('input[name="id"]').val()

    return {numeroAnoCompra, modalidadeCompra, unidadeCompra, numeroAta, idRemanejamento}
}

// Método necessário para iniciar a tabela com o datatable
// inserirLinhaItem()

function validarQuantidadeSolicitada(
    urlValidarLancamento,
    idUnidadeSolicitante,
    idItemUnidadeCeder,
    compraItemId,
    somatorioCampoItem
) {
    let bloquearLancamento = null
    // Verificar se o valor digitado pode ser solicitado para o remanejamento
    $.ajax({
        url: urlValidarLancamento,
        type: "POST",
        async: false,
        data: {idUnidadeSolicitante, idItemUnidadeCeder, compraItemId, somatorioCampoItem},
        success: function (response) {
            bloquearLancamento = response
        }
    });

    return bloquearLancamento
}

$(document).ready(function () {
    function contadorIdAta(idAta, idItemUnidade, removerAta = false) {
        // Se não existir a ata selecionada, então criamos o array
        if (quantidadeAtaLancada[idAta] === undefined) {
            quantidadeAtaLancada[idAta] = []
            quantidadeAtaLancada[idAta][idItemUnidade] = 1
            return
        }

        // Se o usuário apagar o valor digitado então remove o item da variável de controle da ata
        if (removerAta) {
            quantidadeAtaLancada[idAta].splice(idItemUnidade, 1)
            // quantidadeAtaLancada[idAta][idItemUnidade] = 0
            return
        }

        // if (quantidadeAtaLancada[idAta] === 0) {
        // quantidadeAtaLancada[idAta][idItemUnidade] = 1
        // }
        // quantidadeAtaLancada[idAta] += 1
    }

    // Se perder o foco do campo digitado
    $(document).on('blur', '.quantidade_solicitada', function () {
        let idAta = $(this).data('idata')
        let idItemUnidade = $(this).data('iditemunidade')
        let idUnidadeSolicitante = $(this).data('unidadesolicitanteid')
        let valorDigitado = 0

        if ($(this).val() != '') {
            valorDigitado = parseFloat($(this).val())

            // O usuário não pode solicitar uma quantidade fracionada
            if (!Number.isInteger(valorDigitado)) {
                exibirAlertaNoty('error-custom', 'A quantidade solicitada não pode ser valor fracionado');
                $(this).val('')
                return;
            }
        }

        let idItemFornecedor = $(this).data('iditemfornecedor')
        let compraItemId = $(this).data('compraitemid')

        // Método dentro do arquivo upload_file_generico

        if (quantidadeSolicitadaInserida[idItemUnidade] == undefined) {
            quantidadeSolicitadaInserida[idItemUnidade] = []
        }

        // Arquivo(s)
        if (valorDigitado > 0) {
                if (quantidadeSolicitadaInseridaPorItem[compraItemId] != undefined) {
                    if (quantidadeSolicitadaInserida[idItemUnidade][idAta] != undefined) {
                        quantidadeSolicitadaInseridaPorItem[compraItemId] -= quantidadeSolicitadaInserida[idItemUnidade][idAta];
                    }
                    quantidadeSolicitadaInseridaPorItem[compraItemId] += valorDigitado;
                }
                quantidadeSolicitadaInserida[idItemUnidade][idAta] = valorDigitado;


                if (quantidadeSolicitadaInseridaPorItem[compraItemId] == undefined) {
                    quantidadeSolicitadaInseridaPorItem[compraItemId] = valorDigitado;
                }

            // Recuperar a URL principal para complementar com o id da unidade solicitante
            // e o do item para validar o lançamento do usuário
            let urlValidarLancamento = $(this).data('urlvalidarlancamento')

            let somatorioCampoItem = 0
            let classQuantItem = quantidadeSolicitadaInseridaPorItem[compraItemId]

            if (classQuantItem != undefined) {
                somatorioCampoItem = parseFloat(classQuantItem)
            }

            let retornoValidacaoLancamento = validarQuantidadeSolicitada(
                urlValidarLancamento,
                idUnidadeSolicitante,
                $(this).data('itemprincipal'),
                $(this).data('compraitemid'),
                somatorioCampoItem
            )

            // Se o retorno for verdadeiro, então o usuário não pode lançar a quantidade solicitada
            if (retornoValidacaoLancamento.bloqueiaLancamento) {
                let mensagem =
                    `A quantidade solicitada do item ${retornoValidacaoLancamento.itemCompleto} <br>
                da unidade ${retornoValidacaoLancamento.unidadeOrigemItem},
                ultrapassa a quantidade máxima permitida para unidade não participante. <br>
                A quantidade disponível para este item é de ${retornoValidacaoLancamento.quantidadeValidacao}`

                exibirAlertaNoty('error-custom', mensagem);
                $(this).val('')
                if (quantidadeSolicitadaInseridaPorItem[compraItemId] != undefined) {
                    if (quantidadeSolicitadaInserida[idItemUnidade][idAta] != undefined) {
                        quantidadeSolicitadaInseridaPorItem[compraItemId] -= quantidadeSolicitadaInserida[idItemUnidade][idAta];
                        quantidadeSolicitadaInserida[idItemUnidade][idAta] = undefined;
                    }
                }
                dadosDataSetInputQuantidadeSolicitada[idItemUnidade] = null
                return
            }

            if (!currentLocation.pathname.includes("edit")) {
                vincularRegistroArquivo(
                    idAta,
                    'do remanejamento da ata ' + $(this).data('numeroata'),
                    $(this).data('numeroata')
                )
                quantidadeLancada.push(idAta)
                contadorIdAta(idAta, idItemUnidade)
            }

            dadosDataSetInputQuantidadeSolicitada[idItemUnidade] = $(this).data();
        }

        // Se o usuário inserir o valor zero ou limpar o campo então retira do array
        if ((valorDigitado == 0 || valorDigitado == '') && quantidadeSolicitadaInserida[idItemUnidade][idAta] != undefined) {
            // Realiza o grep para retirar o valor id da ata no array
            quantidadeLancada = jQuery.grep(quantidadeLancada, function (value) {
                return value != idAta;
            });

            contadorIdAta(idAta, idItemUnidade, true)
            // Recuperar todos os itens da Ata selecionada
            const count = quantidadeAtaLancada[idAta].filter(function (key, item) {
                return item
            });

            // Se não tiver nenhum item na Ata selecionada, então retira do array de item selecionado
            // e o usuário não consegue ver o campo para selecionar o campo
            if (count.length == 0 && !currentLocation.pathname.includes("edit")) {
                idSelecionado.splice(idSelecionado.indexOf(), 1);
                // Remove da tela do usuário o input file vinculada a ata
                $(`#br-upload-${idAta}`).remove()
            }

            if (quantidadeSolicitadaInserida[idItemUnidade] == undefined) {
                quantidadeSolicitadaInserida[idItemUnidade] = []
            }
            // Remover a quantidade do item
            if (quantidadeSolicitadaInserida[idItemUnidade][idAta] != undefined) {
                quantidadeSolicitadaInseridaPorItem[compraItemId] -= quantidadeSolicitadaInserida[idItemUnidade][idAta];

                if (quantidadeSolicitadaInseridaPorItem[compraItemId] == 0) {
                    quantidadeSolicitadaInseridaPorItem[compraItemId] = undefined
                }
            }
            quantidadeSolicitadaInserida[idItemUnidade][idAta] = undefined;
            dadosDataSetInputQuantidadeSolicitada[idItemUnidade] = null
        }
        // Incluir a quantidade de atas que foram selecionadas
        $("#qtd_ata_selecionada").val(quantidadeLancada.length)
    })
})

/**
 * Método responsável em verificar se o input file estão com arquivo
 */
function verificarArquivosRemanejamento() {
    let retornoValidacao = []
    let opcaoSelecionadaCampoRemanejamentoEntreUnidadeEstadoDfMunicipioDistinto =
        $("input[name=remanejamento_entre_unidade_estado_df_municipio_distinto]:checked").val()

    if (opcaoSelecionadaCampoRemanejamentoEntreUnidadeEstadoDfMunicipioDistinto == 0) {
        return []
    }

    // Recupera todos os campos input file vinculados com a ata
    $('.upload-input').each(function () {
        let inputFile = $(this)
        let campoFile = inputFile[0].files.length

        // Se o idunico do campo conter dentro da variável de controle do arquivo
        // então sai do método
        // Se nenhum arquivo for selecionado para a ata, então bloqueia o envio para o form
        if (campoFile == 0 && !idSelecionado.includes(inputFile.data('idunico'))) {
            let mensagem = `Favor selecionar um arquivo para a ata ` + inputFile.data('textounico') + `.<br>`
            retornoValidacao.push(mensagem)
        }
    })

    return retornoValidacao;
}

//Sobreescreveu o metodo para fazer a validacao nas datas
$(".button-custom").click(function (e) {
    e.preventDefault()

    let idBotao = $(this).attr("id")

    let opcaoSelecionadaCampoRemanejamentoEntreUnidadeEstadoDfMunicipioDistinto =
        $("input[name=remanejamento_entre_unidade_estado_df_municipio_distinto]:checked").val()

    let opcaoSelecionadaCampoFornecedorDeAcordoNovoLocal =
        $("input[name=fornecedor_de_acordo_novo_local]:checked").val()

    if (opcaoSelecionadaCampoRemanejamentoEntreUnidadeEstadoDfMunicipioDistinto === undefined) {
        exibirAlertaNoty(
            'error-custom',
            'Selecione uma opção para o campo ' +
            '"o remanejamento está sendo feito entre unidades de estado, distrito federal ou município distintos."'
        );

        bloquearSubmit = true
        return
    }


    if (opcaoSelecionadaCampoRemanejamentoEntreUnidadeEstadoDfMunicipioDistinto == 1 &&
        (opcaoSelecionadaCampoFornecedorDeAcordoNovoLocal == 0 ||
            opcaoSelecionadaCampoFornecedorDeAcordoNovoLocal === undefined)
    ) {
        exibirAlertaNoty(
            'error-custom',
            'Para remanejamento entre unidades de estado, distrito federal ou município distintos, ' +
            'é  necessário que o fornecedor beneficiário da ata esteja de acordo.'
        );

        bloquearSubmit = true
        return
    }

    // Retira a obrigação dos campos
    inserirRemoverCampoRequired("input[name=fornecedor_de_acordo_novo_local]", false)
    inserirRemoverCampoRequired("input[name=remanejamento_entre_unidade_estado_df_municipio_distinto]", false)

    // Se o botão clicado pelo usuário for para criar o remanejamento de forma
    // direta, então inclui as validação de campos obrigatório
    if (idBotao == 'adicionar') {
        if (opcaoSelecionadaCampoRemanejamentoEntreUnidadeEstadoDfMunicipioDistinto == 1){
            inserirRemoverCampoRequired(
                "input[name=remanejamento_entre_unidade_estado_df_municipio_distinto]",
                true
            )
        }

        if (opcaoSelecionadaCampoRemanejamentoEntreUnidadeEstadoDfMunicipioDistinto == 1) {
            inserirRemoverCampoRequired("input[name=fornecedor_de_acordo_novo_local]", true)
        }

        let arquivoNãoInformado = verificarArquivosRemanejamento()
        // Se existir mensagem, porque existe input file sem arquivo
        if (arquivoNãoInformado.length > 0) {
            let mensagemNoty = ''
            arquivoNãoInformado.forEach(function (mensagem) {
                mensagemNoty += mensagem
            })
            exibirAlertaNoty('error-custom', mensagemNoty);
            bloquearSubmit = true
            return
        }
    }

    // Se não for digitado nenhum valor para o remanejamento, então bloqueia
    if (quantidadeLancada.length == 0) {
        exibirAlertaNoty('error-custom', 'Preencha ao menos um item para solicitar o remanejamento.');
        bloquearSubmit = true
        return
    }

    let formValidation = $(this).closest('form')[0]

    let remanejamentoEntreUnidadeEstadoCount = 0
    let fornecedorDeAcordoNovoLocalCount = 0
    // Método responsável em percorrer os campos obrigatórios para exibir o alerta
    formValidation.forEach(function (item) {
        // Se a mensagem for preenchida, então o campo obrigatório não foi
        // informada nenhuma informação
        if (item.validationMessage != "") {
            if (item.name == 'remanejamento_entre_unidade_estado_df_municipio_distinto' &&
                remanejamentoEntreUnidadeEstadoCount == 0) {
                exibirAlertaNoty(
                    'error-custom',
                    `O campo O remanejamento está sendo feito entre unidades
                    de estado, distrito federal ou município distintos não
                    está marcado`
                )
                remanejamentoEntreUnidadeEstadoCount++;
            }

            if (item.name == 'fornecedor_de_acordo_novo_local' && fornecedorDeAcordoNovoLocalCount == 0) {
                exibirAlertaNoty(
                    'error-custom',
                    `O campo O fornecedor beneficiário da ata de registro de
                        preços está de acordo com o fornecimento no novo local
                        de entrega não está marcado`
                )
                fornecedorDeAcordoNovoLocalCount++;
            }

            bloquearSubmit = true
            return
        }
    })

    bloquearSubmit = false

    let container = document.getElementById("mainForm");

    if(currentLocation.pathname.includes("edit")) {
        $(".quantidade_solicitada").each(function () {
            let dadosDataSet = $(this).data()
            let compraItemUnidadeId = dadosDataSet.iditemunidade
            let  quantidade = $(this).val()

            if (quantidade != "") {
                criarCampo(container, `quantidade_solicitada[${compraItemUnidadeId}]`,quantidade)
                criarCampo(container, `ata_solicitada_por_compra_item_unidade[${compraItemUnidadeId}]`, dadosDataSet.idata)
                criarCampo(container, `item_principal_solicitado[${compraItemUnidadeId}]`,dadosDataSet.itemprincipal)
            }

            $(this).remove()
        })
        return
    }

    $(".quantidade_solicitada").each(function () {
        $(this).remove()
    })
    quantidadeSolicitadaInserida.forEach(function (idAta, compraItemUnidadeId) {
        idAta.forEach(function(quantidade, idAtaSelecionada) {
            if (quantidade != undefined) {
                let dadosDataSet = dadosDataSetInputQuantidadeSolicitada[compraItemUnidadeId]

                if (dadosDataSet == undefined) {
                    return
                }

                criarCampo(container, `quantidade_solicitada[${compraItemUnidadeId}]`,quantidade)
                criarCampo(container, `ata_solicitada_por_compra_item_unidade[${compraItemUnidadeId}]`, dadosDataSet.idata)
                criarCampo(container, `item_principal_solicitado[${compraItemUnidadeId}]`,dadosDataSet.itemprincipal)
            }
        })
    })
})

/**
 * Método responsável em inserir se o campo está obrigatório
 */

function inserirRemoverCampoRequired(idCampo, campoObrigatorio) {
    $(idCampo).prop('required', campoObrigatorio)
}

/**
 * Método responsável quando o usuário seleciona uma opção para o campo
 * O remanejamento está sendo feito entre unidades de estado, distrito federal ou município distintos?
 */
function marcacaoCampoRemanejamentoEntreUnidadeEstadoDfMunicipioDistinto(campo) {
    // Se o campo for marcado como Sim, então a próxima marcação será obrigatória
    if (campo.value === "1") {
        $('input[name=fornecedor_de_acordo_novo_local]').prop({readonly: true, disabled: false});
        inserirRemoverCampoRequired("input[name=fornecedor_de_acordo_novo_local]", true)
        return
    }

    inserirRemoverCampoRequired("input[name=fornecedor_de_acordo_novo_local]", false)
    $('input[name=fornecedor_de_acordo_novo_local]').prop({readonly: false, disabled: true, checked: false});
}

/**
 * Método responsável em recuperar o click do botão para expandir as colunas
 */
$('#table_selecionar_item_arp_remajenamento tbody').on('click', 'td.dtr-control', function () {
    var tr = $(this).closest('tr');
    var row = datableItem.row(tr);
    let colunasDatatable = row.data()

    // Verifica se a ação vai ser recolher as colunas
    if (row.child.isShown()) {
        // Seta o valor do campo padrão sem nenhum valor
        colunasDatatable.quantidadesolicitada = colunasDatatable.campovazioquantidadesolicitada
    } else {
        // Seta o valor do campo padrão sem nenhum valor
        colunasDatatable.campovazioquantidadesolicitada = colunasDatatable.quantidadesolicitada

        // Recupera o valor inserido pelo usuário
        let valorInserido = quantidadeSolicitadaInserida[colunasDatatable.id_item_unidade][colunasDatatable.idata]

        // Se achar o valor inserido
        if (valorInserido != undefined) {
            // Substitui o valor padrão pelo inserido
            novoCampoComValor = colunasDatatable.quantidadesolicitada.replace('value=""', `value='${valorInserido}'`)

            // Se existir valor lançado, então monta o value com base no salvo em banco
            // e substitui pelo digitado
            if (typeof colunasDatatable['valor_salvo_rascunho'] !== 'undefined') {
                let valorFormat = `value="${colunasDatatable.valor_salvo_rascunho}"`
                novoCampoComValor =
                    colunasDatatable.quantidadesolicitada.replace(valorFormat, `value='${valorInserido}'`)
            }

            // Insere o novo campo com o valor setado
            colunasDatatable.quantidadesolicitada = novoCampoComValor
            return
        }

        // Se não tiver valor inserido, insere o valor do campo vazio
        if (valorInserido == undefined) {
            colunasDatatable.quantidadesolicitada = colunasDatatable.campovazioquantidadesolicitada
            return
        }
    }
});

/**
 * Método responsável em cancelar o remanejamento quando estiver sido solicitado
 *
 */
function cancelarRemanejamento(idRemanejamento) {
    $.ajax({
        url: 'remanejamento/cancelarremanejamento',
        type: "POST",
        data: {idRemanejamento},
        success: function (response) {

            if (response.sucesso) {
                $('#crudTable').DataTable().ajax.reload();
                exibirAlertaNoty('success-custom', response.mensagem)
            }
        }
    });
}
function habilitarDesabilitarCamposCompra(habilitar) {

    if (habilitar) {
        $('#numero_compra').prop('disabled', false);
        $('#modalidade_compra').prop('disabled', false);
        return
    }

    $('#numero_compra').prop('disabled', true).val(null).trigger('change');
    $('#modalidade_compra').prop('disabled', true).val(null).trigger('change');
}


$('#unidade_compra').on('select2:clear', function () {
    habilitarDesabilitarCamposCompra(false)
})

$('#unidade_compra').on('select2:select', function (e) {
    habilitarDesabilitarCamposCompra(true)
    let data = e.params.data
    let idUnidadeCompra = data.id
    carregarSelect('numero_compra', `numerocompraata/${idUnidadeCompra}`, 'numero_ano')
    carregarSelect('modalidade_compra', `modalidadecompraata/${idUnidadeCompra}`, 'modalidade_compra')
})

function carregarSelect(name, url, text) {
    $(`[name='${name}']`).select2({
        theme: 'bootstrap',
        multiple: false,
        allowClear: true,
        ajax: {
            url: url,
            type: 'GET',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                var data = {
                    q: params.term, // search term
                    page: params.page, // pagination
                };

                return data;
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                let paginate = false;

                if (data.data) {
                    paginate = data.current_page < data.last_page;
                    data = data.data;
                }

                return {
                    results: $.map(data, function (item) {
                        let texto = eval(`item.${text}`)
                        return {
                            text: texto,
                            id: item.id,
                        }
                    }),
                    pagination: {
                        more: paginate,
                    }
                };
            },
            cache: true

        },
    });
}

function todosValoresNulosOuIndefinidos(obj) {
    return Object.values(obj).every(value => value === null || value === undefined);
}

$('#table_ajax_item_remanejamento_arp').on('draw.dt', function () {
    $(this).find('tbody tr').each(function () {
        var tr = $(this).closest('tr');
        var row = datableItem.row(tr);
        let colunasDatatable = row.data()
        let valorSalvoMemoria = false

        if (colunasDatatable == undefined) {
            return;
        }

        // quantidadeSolicitadaInseridaPorItem[colunasDatatable.compra_item_id] = 0
        if (colunasDatatable.itemFornecedorCarona.length > 0 ) {
            colunasDatatable.itemFornecedorCarona.forEach(function(linha, index) {

                if (quantidadeSolicitadaInserida[linha.compra_item_unidade_id] != undefined && !currentLocation.pathname.includes("edit")) {
                    let quantidadeSalvaFornecedor = quantidadeSolicitadaInserida[linha.compra_item_unidade_id][linha.idata]
                    if (quantidadeSalvaFornecedor != undefined) {
                        linha.valor_rascunho = quantidadeSalvaFornecedor
                        colunasDatatable.itemFornecedorCarona[index] = linha
                        valorSalvoMemoria = true
                        return
                    }
                }

                if (linha.valor_rascunho != "" && currentLocation.pathname.includes("edit")) {
                    row.child(format(colunasDatatable)).show();
                    tr.addClass('shown');
                    if (quantidadeSolicitadaInserida[linha.compra_item_unidade_id] == undefined) {
                        quantidadeSolicitadaInserida[linha.compra_item_unidade_id] = []
                        quantidadeSolicitadaInserida[linha.compra_item_unidade_id][linha.idata] = linha.valor_rascunho

                        if (quantidadeSolicitadaInseridaPorItem[colunasDatatable.compra_item_id] != undefined) {
                            quantidadeSolicitadaInseridaPorItem[colunasDatatable.compra_item_id] += parseFloat(linha.valor_rascunho)
                        }

                        if (quantidadeSolicitadaInseridaPorItem[colunasDatatable.compra_item_id] == undefined) {
                            quantidadeSolicitadaInseridaPorItem[colunasDatatable.compra_item_id] = parseFloat(linha.valor_rascunho)
                        }
                    }

                    dadosDataSetInputQuantidadeSolicitada[linha.compra_item_unidade_id] =
                        $(`#quantidade_solicitada_${linha.compra_item_unidade_id}_${linha.idata}`).data()
                }
            })

            if (valorSalvoMemoria) {
                row.child(format(colunasDatatable)).show();
                tr.addClass('shown');
            }
        }

        if (colunasDatatable.itemFornecedorCarona.length > 0 ) {
            colunasDatatable.itemFornecedorCarona.forEach(function(linha, index) {

                if (quantidadeSolicitadaInserida[linha.compra_item_unidade_id] != undefined && !currentLocation.pathname.includes("edit")) {
                    let quantidadeSalvaFornecedor = quantidadeSolicitadaInserida[linha.compra_item_unidade_id][linha.idata]
                    if (quantidadeSalvaFornecedor != undefined) {
                        linha.valor_rascunho = quantidadeSalvaFornecedor
                        colunasDatatable.itemFornecedorCarona[index] = linha
                        valorSalvoMemoria = true
                        return
                    }
                }

                if (linha.valor_rascunho != "" && currentLocation.pathname.includes("edit")) {
                    row.child(format(colunasDatatable)).show();
                    tr.addClass('shown');
                    if (quantidadeSolicitadaInserida[linha.compra_item_unidade_id] == undefined) {
                        quantidadeSolicitadaInserida[linha.compra_item_unidade_id] = []
                        quantidadeSolicitadaInserida[linha.compra_item_unidade_id][linha.idata] = linha.valor_rascunho

                        if (quantidadeSolicitadaInseridaPorItem[colunasDatatable.compra_item_id] != undefined) {
                            quantidadeSolicitadaInseridaPorItem[colunasDatatable.compra_item_id] += parseFloat(linha.valor_rascunho)
                        }

                        if (quantidadeSolicitadaInseridaPorItem[colunasDatatable.compra_item_id] == undefined) {
                            quantidadeSolicitadaInseridaPorItem[colunasDatatable.compra_item_id] = parseFloat(linha.valor_rascunho)
                        }
                    }

                    dadosDataSetInputQuantidadeSolicitada[linha.compra_item_unidade_id] =
                        $(`#quantidade_solicitada_${linha.compra_item_unidade_id}_${linha.idata}`).data()
                }
            })

            if (valorSalvoMemoria) {
                row.child(format(colunasDatatable)).show();
                tr.addClass('shown');
            }
        }

        let idItemUnidade = colunasDatatable.id_item_unidade
        let idata = colunasDatatable.idata

        if (colunasDatatable.valor_rascunho != '') {
            if (quantidadeSolicitadaInserida[idItemUnidade] == undefined) {
                quantidadeSolicitadaInserida[idItemUnidade] =[]
            }

            quantidadeSolicitadaInserida[idItemUnidade][idata] = colunasDatatable.valor_rascunho
        }

        let idCampoQuantidadeSolicitada = `quantidade_solicitada_${idItemUnidade}_${idata}`

        if (quantidadeSolicitadaInserida[idItemUnidade] == undefined) {
            return
        }

        let quantidadeLancadaItem = quantidadeSolicitadaInserida[idItemUnidade][idata]

        if (quantidadeLancadaItem  != undefined) {
            $(`#${idCampoQuantidadeSolicitada}`).val(quantidadeLancadaItem)
            return;
        }
    })
})

$('#table_ajax_item_remanejamento_arp').on('click', 'a.details-link', function () {
    var tr = $(this).closest('tr');
    var row = datableItem.row(tr);
    let colunasDatatable = row.data()

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('shown');
    } else {
        row.child(format(colunasDatatable)).show();
        tr.addClass('shown');
    }
});

// Função para formatar os detalhes em linhas separadas
function format(d) {
    table = ` <table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                    <thead>
                      <tr>
                            <th>CNPJ Fornecedor</th>
                            <th>Nome</th>
                            <th>Quantidade</th>
                        </tr>  
                    </thead>`;
    table +=`<tbody>`;
    let numeroAta = d.numero_ata
    let idAta = d.idata
    d.itemFornecedorCarona.forEach(function(linha) {
        let idItemUnidade = linha.compra_item_unidade_id

        let valorInserido = linha.valor_rascunho ?? undefined

        if (valorInserido == undefined) {
            valorInserido = ''
        }
        var meuObjeto = [{idAta, idUnidadeCeder: d.id_item_unidade }];
        let t = JSON.stringify(meuObjeto)
        table += `<tr>`

        table += `<td> ${linha.cpf_cnpj_idgener}</td>`
        table += `<td> ${linha.nome}</td>`
        table += `<td> 
                        <div class='br-input'>
                             <input
                                 type='hidden'
                                 id='ata_selecionada_[${idItemUnidade}]'
                                 value='${numeroAta}'
                             >
                             <input
                                 type='hidden'
                                 name='id_ata_fornecedor[${d.id_item_unidade}]'
                                 value='${idAta}'
                             >
                            <input  type='number'  
                                    min='0'
                                    class='quantidade_solicitada'
                                    id='quantidade_solicitada_${idItemUnidade}_${idAta}'
                                    name='quantidade_solicitada[${d.id_item_unidade}][${idItemUnidade}]'
                                    min='0'
                                    step='${d.step}'
                                    data-iditemunidade='${idItemUnidade}'
                                    data-numeroata='${numeroAta}'
                                    data-idata='${idAta}'
                                    data-unidadesolicitanteid = '${d.unidade_solicitante_id}'
                                    data-urlvalidarlancamento = '${d.url}'
                                    data-compraitemid = '${d.compra_item_id}'
                                    data-itemprincipal = '${d.id_item_unidade}'
                                    value = '${valorInserido}'
                                    style='width: 30%;'/>

                           
                        </div>
                 </td>`

        table += `</tr>`
    })

    table +=`</tbody>`;

    table += `  </table>`

    return table
}

function criarCampo(container, name, value) {
    let novoInput = document.createElement("input");
    // Define o tipo do input como texto
    novoInput.setAttribute("type", "hidden");
    novoInput.setAttribute("name", name);
    novoInput.setAttribute("value", value);

    container.appendChild(novoInput);
}