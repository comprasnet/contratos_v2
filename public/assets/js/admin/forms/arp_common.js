/********************************************************************/
/*** Arquivo que armazena funções em comum para as páginas da ata ***/
/********************************************************************/

function campoEmBranco(nomeCampo) {
    if (nomeCampo == '' || nomeCampo == undefined) {
        return true
    }
    return false
}

// Responsável em montar a modal para exibir a classificação de todos os fornecedores do item
function exibirClassificacaoFornecedor(idItemFornecedor, url, numeroItem) {

    // Habilita define o tamanho da modal e inclui a função para exibir na tela    
    $(".br-modal").css("max-width", "100%");
    $("#modalArp").addClass("active");
    $("#modalArp .br-modal-header").html(`Detalhes da classificação dos fornecedores do item ${numeroItem}`)

    // Monta o cabeçalho da tabela dentro da modal
    $("#table_detalhe_item #thead_detalhe_item").empty()
    let cabecalho = `   <th>Classificação</th>
                        <th>CNPJ</th>
                        <th>Nome</th>
                        <th>Quantidade homologada</th>
                        <th>Valor unitário</th>`

    $("#table_detalhe_item #thead_detalhe_item").append(cabecalho);

    // Limpa a linha da tabela
    $("#table_detalhe_item #tbody_detalhe_item").empty()

    // Método para recuperar as informações
    $.get(url, { idItemFornecedor: idItemFornecedor }).done(function (response) {
        // Percorre o array de fornecedores
        $.each(response, (index, item) => {
            // Monta a linha da tabela
            let linha = `   <tr>
                                <td>${item.classificacao}</td>
                                <td>${item.cpf_cnpj_idgener}</td>
                                <td>${item.nome}</td>
                                <td>${item.quantidade_homologada_vencedor}</td>
                                <td>${item.valor_unitario}</td>
                            </tr>`
            $("#table_detalhe_item #tbody_detalhe_item").append(linha);
        })
    })

}

// Método responsável em exibir as informações dos participantes para o item
function exibirDetalheItem(compraId, userId, compraItemId, url) {
    // Recupera as informações das unidades para o item
    $.get(url, { compraId, userId, compraItemId }).done(function (response) {

        // Propriedades para exibir a modal
        $(".br-modal").css("max-width", "100%");
        $("#modalArp").addClass("active");
        $("#modalArp .br-modal-header").html('Unidades')
        // Remove as informações anteriores
        $("#loadingProgress").remove()
        $("#labelMensagem").remove()

        // Limpa o cabeçalho da tabela
        $("#table_detalhe_item #thead_detalhe_item").empty()

        // Limpa as linhas da tabela
        $("#table_detalhe_item #tbody_detalhe_item").empty()

        // Se o JOB ainda estiver sendo processado, não será exibido a modal
        if (response.code == 423) {
            if (response.percentual != undefined) {
                let loadingProgress = montarLoadingCarregar(response)
                $("#modalArp .br-modal-body").append(loadingProgress)
            }
            let labelMensagem = `<span id="labelMensagem">${response.message}<span>`
            $("#modalArp .br-modal-body").append(labelMensagem)

            // Tempo para fechar a modal quando estiver em processo de importação dos participantes
            // Por padrão, deixamos cinco segundos
            setTimeout(function () { $("#modalArp").removeClass("active"); }, 10000);

            return
        }

        // Monta o cabeçalho
        let cabecalho = `<th>Unidade</th>
                        <th>Tipo</th>
                        <th>Quantidade homologada</th>`

        // Adiciona o cabeçalho na tabela
        $("#table_detalhe_item #thead_detalhe_item").append(cabecalho);

        // Percorre os itens enviados e adiciona as linhas de cada unidade
        $.each(response.data, (index, item) => {
            let linha = `<tr>
                            <td>${item.unidade}</td>
                            <td>${item.tipo}</td>
                            <td>${item.quantidade}</td>
                        </tr>`
            $("#table_detalhe_item #tbody_detalhe_item").append(linha);
        })
    })

}

// Função responsável em exibir a modal com as informações do maior desconto do item
function exibirDetalheMaiorDesconto(percentual_maior_desconto, numero, descricaosimplificada,
    valor_unitario, valor_unitario_desconto, valor_negociado,
    valor_negociado_desconto) {
    // Habilita define o tamanho da modal e inclui a função para exibir na tela
    $(".br-modal").css("max-width", "100%");
    $("#modalArp").addClass("active");
    $("#modalArp .br-modal-header").html('Detalhes sobre o percentual do maior desconto')

    // Monta o cabeçalho da tabela dentro da modal
    $("#table_detalhe_item #thead_detalhe_item").empty()
    let cabecalho =
        `<th>Número</th>
        <th>Descrição</th>
        <th>% de Desconto</th>
        <th>Valor Unitário</th>
        <th>Valor Unitário com Desconto</th>
        <th>Valor Total</th>
        <th>Valor Total com Desconto</th>`

    $("#table_detalhe_item #thead_detalhe_item").append(cabecalho);

    // Monta a linha da tabela
    let linha =
        `<tr>
        <td>${numero}</td>
        <td>${descricaosimplificada}</td>
        <td>${percentual_maior_desconto}</td>
        <td>${valor_unitario}</td>
        <td>${valor_unitario_desconto}</td>
        <td>${valor_negociado}</td>
        <td>${valor_negociado_desconto}</td>
        </tr>`

    $("#table_detalhe_item #tbody_detalhe_item").empty()
    $("#table_detalhe_item #tbody_detalhe_item").append(linha);
}


/***************************************************************/
/*** Funções para verificar e formatar o campo número da ata ***/
/***************************************************************/

// Adicione o manipulador de eventos blur para o campo do número da ata
$("input[name=numero], input[name=numero_alteracao], input[name=ano],input[name=ano_ata]").blur(function () {


    let campo = $(this).attr('name');
    let anoAta = $("input[name=ano_ata]").val();
    let numeroAta = $("input[name=numero_alteracao]").val();
    let numeroArp = $(this).val();
    let unidadeGerenciadora = $("input[name=unidade_origem_id]").val();
    let anoAtaDigitado = $("input[name=ano]").val();

    if(campo === 'numero' || campo ==='numero_alteracao') {

        let resultadoValidacao = validarNumeroAta(numeroAta);
        if (resultadoValidacao !== true) {
            exibirAlertaNoty('error-custom', resultadoValidacao);
            bloquearSubmit = true;
            $(this).val(''); // Limpar o campo do número da ata
        } else {
            numeroArp = adicionarZeroEsquerda(numeroArp);
            $(this).val(numeroArp);
        }
    }

    // Se todos os campos estiverem preenchidos, envia para o ajax
    if (!campoEmBranco(numeroAta)) {
        let urlValidarNumeroAta = $("#url_buscar_arp").val()
        $.post(
            urlValidarNumeroAta,
            { numeroAta: numeroAta, anoAta: anoAta, unidadeGerenciadora: unidadeGerenciadora }
        ).done(function (response) {
            if (!response && anoAtaDigitado === anoAta) {
                mensagemAlertaCampoEmBranco =
                    'Existe ata com o mesmo número e ano registrada para essa unidade ' +
                    'ou número informado é o mesmo desta ata. Para prosseguir, corrija as informações digitadas.'
                exibirAlertaNoty('warning-custom', mensagemAlertaCampoEmBranco)
            }
            return false;
        })
    }

});

function adicionarZeroEsquerda(numero) {
    if (numero == '') {
        return
    }

    numero = ("0000" + numero).slice(-5);
    return numero;
}

function validarNumeroAta(numeroAta) {
    if (/^0+$/.test(numeroAta) || numeroAta === '00000') {
        return 'O Número da Ata não pode ser composto apenas por zeros.';
    }
    return true; // Número da ata válido
}


// Permite a digitação somente de números no campo quantidade solicitada,
// impedindo valores fracionados e/ou negativos

/******************************************************************/
/*** Rotinas para impedir digitação de caracteres não numéricos ***/
/******************************************************************/

// Ao digitar, impedir que o usuário digite caracteres não numéricos
$(document).on('keypress', '.nao_fracionado', function (e) {
    var inputValue = String.fromCharCode(e.which);

    if (!/^\d$/.test(inputValue)) {
        e.preventDefault();
    }
});

// Se o campo tem valor antigo com caracteres não numéricos, retira os caracteres não numéricos
$(document).on('keydown', '.nao_fracionado', function () {
    var valor = $(this).val();

    if (/[^0-9]/.test(valor)) {
        valor = valor.replace(/[^0-9]/g, '');
        exibirAlertaNoty(
            'warning-custom',
            'O valor informado continha caracteres alfanuméricos que foram removidos. Favor conferir.');
    }

    $(this).val(valor);
});

// Ao tentar colar (ctrl+v) um valor não numérico, impede e exibe mensagem
$(document).on('paste', '.nao_fracionado', function (e) {
    var clipboardData = e.originalEvent.clipboardData || window.clipboardData;
    var pastedData = clipboardData.getData('text');

    if (/[^0-9]/.test(pastedData)) {
        e.preventDefault();
        exibirAlertaNoty('warning-custom', 'O valor contém caracteres alfanuméricos. Favor conferir.'
        );
    }
});


/*$('.inputValorNumericoFracionado5Casas').maskMoney({
    thousands: '',
    decimal: ',',
    precision: 5,
    //align: 'right',
    allowZero: true
});*/

$(document).on('keypress', '.inputValorNumericoFracionado5Casas', function (e) {
    var inputValue = String.fromCharCode(e.which);

    // Permite somente números, vírgula e a tecla backspace
    if (!/^\d|,$/.test(inputValue) && e.which !== 8) {
        e.preventDefault();
    }
});

$(document).on('focus', '.inputValorNumericoFracionado5Casas', function (e) {
    $(this).select();
});