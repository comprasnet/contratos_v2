
// Armazena o ID (arp_item) dos itens a serem removidos
let itensRemover = []

/**
 * Verifica se um item tem vínculos.
 * @param {number} arpId - O ID da ATA.
 * @param {number} itemId - O ID do item.
 * @returns {boolean} Retorna verdadeiro se o item tiver vínculos, caso contrário, retorna falso.
 */
async function itemTemVinculos(arpId, itemId) {
    try {
        const response = await fetch(
            `/arp/retificar/${arpId}/verificar-vinculos-item/${itemId}`,
            {
                method: 'GET'
            }
        );
        if (!response.ok) {
            throw new Error();
        }
        const data = await response.json();
        if (data.length > 0) {
            exibirAlertaNoty('warning-custom', data, '', false);
            return true;
        }
        return false;
    } catch (error) {
        exibirAlertaNoty(
            'error-custom',
            'Ocorreu um erro ao tentar buscar as minutas de empenho e/ou os contratos vinculados ao item',
            '', // título
            false
        );
        return false;
    }
}

async function removerLinhaItemSelecionado(arpId, itemId) {

    if (itensRemover.length == ($('#quantidade_de_itens').val() - 1)) {
        exibirAlertaNoty('warning-custom', 'Não é possível remover todos os itens da ata.', '', false);
        return false;
    }

    const temVinculos = await itemTemVinculos(arpId, itemId);
    if (temVinculos) {
        return false;
    }
    itensRemover.push(itemId); // Adiciona o código do item no array de removidos
    $(`#linha_${itemId}`).remove(); // Remove visualmente a linha da tabela
}

$('form').submit(function (e) {
    // Ao enviar, junta os códigos de itens a serem removidos em uma string
    $('#itens_remover').val(itensRemover.join(','));
});