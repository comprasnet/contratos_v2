$(document).ready(function () {

    // Ao sair do campo quantidade empenhada
    $(document).on('blur', '.quantidade_empenhada', function () {

        // Obtém o valor do campo e converte para número
        let valor = parseFloat($(this).val().replace(',', '.'));

        // A linha (tr) contém atributos com valores úteis
        let linha = $(this).closest('tr').attr('id');

        // Obtém os valores dos atributos 'item_id' e 'grupo'
        let item_id = $('tr#' + linha).attr('item_id');
        let grupo = $('tr#' + linha).attr('grupo');

        // Se o item estiver em um grupo (ou seja, grupo for diferente de 00000)
        //if (grupo != '00000') {

            // Obtém o textarea
            let justificativa = $('textarea#texto_justificativa_item_isolado_' + item_id);

            // Se informou valor, não é preciso justificativa: desabilita o campo
            if (valor > 0) {
                justificativa.val('');
                justificativa.prop('disabled', true);
            } else {
                justificativa.prop('disabled', false);
            }
        //}
    });

    $('form').submit(async function (e) {

        // Evita o envio padrão do formulário
        e.preventDefault();

        let arpId = $('input[name="arp_id"]').val();
        let dadosFormulario = new FormData(this);

        let url = window.location.origin + '/arp/execucao/' + arpId + '/validaform';

        try {
            // Obter o token CSRF da meta tag
            let token = $('meta[name="csrf-token"]').attr('content');

            const response = await fetch(url, {
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': token // Inclui o token CSRF nos cabeçalhos da solicitação
                },
                body: dadosFormulario
            });

            if (!response.ok) {
                throw new Error();
            }

            const responseData = await response.json(); // Se a resposta for em JSON

            if (responseData.status == 'error') {
                exibirAlertaNoty('error-custom', responseData.message, '', false);
                $('.br-button.primary').removeAttr('disabled');
                return false;
            }

        } catch (error) {
            errorMessage = error.message ? error.message : 'Ocorreu um erro ao enviar as informações.';

            exibirAlertaNoty('error-custom', errorMessage, '', false);

            return false;
        }


        $(this).unbind('submit').submit();
    });

    $('.br-button.secondary[onclick]').click(function () {

        let arpId = $('input[name="arp_id"]').val();
        let url = window.location.origin + '/arp'

        window.location.href = url;
    });
});
