let unidade_id = null
let compra = null
let modalidade_id = null
let fornecedor_id = null
let arp_id = null
let numeroAtaAdesao = null; // numeroAtaAdesao e atribua um valor inicial
let arp_item = null;

let currentLocation = window.location;
let quantidadeSolicitadaInserida = []
let dadosDataSetInputQuantidadeSolicitada = []
let dadosItemPreenchido = []
let controleQuantidadeItem = []

exibirEsconderBotao()

function limparCamposAdesao() {
    $('#unidade_gerenciadora_id select').val(null).trigger('change');
    $('#compra_fake select').val(null).trigger('change').prop('disabled', true);
    $('#modalidade_fake').val(null).trigger('change').prop('disabled', true);
    // $('#arp_id select').val(null).trigger('change');
    $('#arp_id select').val(null).trigger('change').prop('disabled', true);
    $('#fornecedor_id select').val(null).trigger('change');
    $('#arp_item select').val(null).trigger('change');

    // Limpar o campo "Número da Compra/Ano" explicitamente
    $('#numero_compra').val('');
    $('#modalidade_fake').val('');

    // Limpar os campos select2
    if ($('#compra_fake select').data('select2')) {
        $('#compra_fake select').select2('destroy');
    }

    if ($('#modalidade_fake select').data('select2')) {
        $('#modalidade_fake select').select2('destroy');
    }

    if ($('#arp_id select').data('select2')) {
        $('#arp_id select').select2('destroy');
    }


    // $('#arp_id select').select2({ theme: 'bootstrap', disabled: true });
    $('#modalidade_fake').select2({theme: 'bootstrap', disabled: true});

    // Campos de exceção
    $("#areaAquisicaoEmergencialMedicamento").addClass('invisible')
    $('input[name=execucao_descentralizada_programa_projeto_federal]').val([0])
}

if(currentLocation.pathname.includes("edit")) {
    atualizaArrayDeInputsQuantidadeSolicitadaSubmit()
}
function atualizaArrayDeInputsQuantidadeSolicitadaSubmit() {
    var container = document.getElementById("mainForm");

    if(currentLocation.pathname.includes("edit")) {
        $(".quantidade_solicitada").each(function () {
            let dataSet = $(this).data()
            let index = dataSet.id
            quantidadeSolicitadaInserida[index] = $(this).val()
            dadosDataSetInputQuantidadeSolicitada[index] = dataSet;
            // criarCampo(container, `quantidade_solicitada[${index}]`, $(this).val())
            criarCampo(container, `item_solicitacao_lancamento[${index}]`,index)
            criarCampo(container, `grupo_compra_quantidade_solicitada[${index}]`,dataSet.grupocompra)
        })
        return
    }

    $(".quantidade_solicitada").each(function () {
        $(this).remove()
    })

    Object.keys(quantidadeSolicitadaInserida).forEach(function(index) {
        let dadosDatasetInput = dadosDataSetInputQuantidadeSolicitada[index]
        if (dadosDatasetInput == null) {
            return
        }

        criarCampo(container, `quantidade_solicitada[${index}]`, quantidadeSolicitadaInserida[index])
        criarCampo(container, `item_solicitacao_lancamento[${index}]`,index)
        criarCampo(container, `grupo_compra_quantidade_solicitada[${index}]`,dadosDatasetInput.grupocompra)
    });
}

function criarCampo(container, name, value) {
    let novoInput = document.createElement("input");
    // Define o tipo do input como texto
    novoInput.setAttribute("type", "hidden");
    novoInput.setAttribute("name", name);
    novoInput.setAttribute("value", value);

    container.appendChild(novoInput);
}

$(document).ready(function () {
    // Desabilitar campos inicialmente
    $('#compra_fake select').prop('disabled', true);
    $('#arp_id select').prop('disabled', true);
    $('#modalidade_fake').prop('disabled', true);

    $('#unidade_gerenciadora_id select').on('select2:select', function (e) {
        // Habilitar campos modalidade_fake e compra_fake após a seleção da unidade
        $('#compra_fake select').prop('disabled', false).val(null).trigger('change');
        $('#arp_id select').prop('disabled', false).val(null).trigger('change');
        $('#modalidade_fake').prop('disabled', false).val(null).trigger('change');
        exibirCampoExecucaoDescentralizadaProgramaProjetoFederal(e.params.data)
    });

    $('#btn_adesao_limpa').click(function () {
        limparCamposAdesao();
    });

    $('#unidade_gerenciadora_id select').on('select2:clear', function () {

        // Desabilitar campos modalidade_fake e compra_fake após a limpeza da unidade_gerenciadora_id
        $('#compra_fake select').prop('disabled', true).val(null).trigger('change');
        $('#modalidade_fake').prop('disabled', true).val(null).trigger('change');
        $('#arp_id select').prop('disabled', true).val(null).trigger('change');
        $("#areaAquisicaoEmergencialMedicamento").addClass('invisible');

        $("#execucao_descentralizada_programa_projeto_federal").removeClass('d-block')
            .addClass('d-none')
        $(".areaExecucaoDescentralizadaProgramaProjetoFederal").removeClass('d-block').addClass('d-none')
    });

    // Manipulador de evento para select2:clear em outros campos
    $('#compra_fake select, #modalidade_fake select, #arp_id select').on('select2:clear', function () {
        // Desabilitar todos os campos exceto o campo atual
        if ($(this).is('#unidade_gerenciadora_id select')) {
            // Se o campo sendo limpo for unidade_gerenciadora_id, não desabilitar esse campo
            return;
        }

    });

    function obterItensSelecionadosDoGrupo(grupo) {
        var itensDoGrupo = [];
        $('input.select-checkbox:checked').each(function () {
            var numeroItem = $(this).data('numero');
            var numeroCompra = $(this).data('numerocompras');
            var unidadeItem = $(this).data('unidade-id');
            var grupoCompraItem = $(this).data('grupocompra');
            itensDoGrupo.push({
                numeroItem: numeroItem,
                numeroCompra: numeroCompra,
                unidadeItem: unidadeItem,
                grupoCompraItem: grupoCompraItem
            });
        });

        // Ordena os números dos itens em ordem crescente
        itensDoGrupo.sort(function (a, b) {
            return a.numeroItem - b.numeroItem;
        });

        return itensDoGrupo;
    }


    function escondeJustificativa() {
        $('.justificativa-item-isolado').hide();
        $('.texto-justificativa-item-isolado').hide();
        $('.campo-upload').hide();
        // remove os campos obrigatórios
        $('.justificativa-item-isolado input').prop('required', false);
        $('.texto-justificativa-item-isolado textarea').prop('required', false);
        $('.campo-upload input').prop('required', false);
        bloqueiaAnexoIsolado = false
    }

    function mostraJustificativa() {
        $('.justificativa-item-isolado').show();
        $('.texto-justificativa-item-isolado').show();
        $('.campo-upload').show();
        // Tornar os campos obrigatórios
        $('.texto-justificativa-item-isolado textarea').prop('required', true);
        $('.campo-upload input').prop('required', true);
        bloqueiaAnexoIsolado = true
    }

    // click na aba justificativa e anexos
    $('button[data-panel="tab_justificativa-e-anexos"]').click(function () {
        // Executa a verificação ao carregar a página
        toggleButtons();

        // Adiciona o evento ao mudar a seleção do radio button
        $('input[name="consulta_aceitacao_fornecedor"]').on('change', toggleButtons);

        var frequencia = {};

        Object.keys(quantidadeSolicitadaInserida).forEach(function(index) {
            if (dadosDataSetInputQuantidadeSolicitada[index] == null) {
                return;
            }

            let grupoCompra = dadosDataSetInputQuantidadeSolicitada[index].grupocompra
                if (frequencia[grupoCompra]) {
                    // Incrementar a contagem
                    frequencia[grupoCompra]++;
                    return
                }

                // Inicializar a contagem como 1
                frequencia[grupoCompra] = 1;
        });
        // Calcular o comprimento da frequência
        var comprimento = Object.keys(frequencia).length;
        var chaves = Object.keys(frequencia);
        let index = chaves.indexOf('00000');
        let contGrupoZero = false;
        if (index > -1) {
            comprimento = comprimento - 1;
            contGrupoZero = true;
            chaves.splice(index, 1);
        }

        if (comprimento == 1) {
            if ($('#count_grupo_' + chaves[0]).val() == Object.entries(frequencia)[0][1]) {
                // Regra: se selecionar todos os itens do mesmo grupo deve ocultar o campo
                // Ocultar os campos de justificativa
                escondeJustificativa();

                return;
            }

            if (Object.entries(frequencia)[0][1] >= 1 && (chaves[0] == '' || chaves[0] == '00000')) {
                // Regra: se selecionar todos os itens do mesmo grupo deve ocultar o campo
                // Ocultar os campos de justificativa
                escondeJustificativa();

                return;
            }
            // Ou se selecionar somente 1 item de um grupo (com exceção do grupo zero) deve mostrar o campo
            // Mostrar os campos de justificativa
            for (const grupo of chaves) {
                var itensDoGrupo = obterItensSelecionadosDoGrupo(grupo);
                mostrarAlertaItensGrupo(grupo, itensDoGrupo);
            }
            mostraJustificativa();
            return;
        }
        // regra se selecionar todos os itens do mesmo grupo 00000 deve ocultar o campo ou
        // selecionar unico item do grupo zero, oculta o campo
        if (comprimento == 0 && contGrupoZero) {
            // Ocultar os campos de justificativa
            escondeJustificativa();
            return;
        }
        // regra se selecionar todos os itens do mesmo grupo, mas selecionar itens de outros grupos
        // (com execeção do grupo zero) deve mostrar o campo
        if (comprimento > 1) {
            // Mostrar os campos de justificativa
            for (const grupo of chaves) {
                var itensDoGrupo = obterItensSelecionadosDoGrupo(grupo);
                mostrarAlertaItensGrupo(grupo, itensDoGrupo);
            }

            mostraJustificativa();
            return;
        }
    });

    function mostrarAlertaItensGrupo(grupo, itens) {
        if (grupo === '00000') {
            return;
        }
        if (itens.length === 0) {
            // Caso não haja itens no grupo, não exibir alerta
            return;
        }
        // Filtra os itens pertencentes ao grupo específico
        var itensDoGrupo = itens.filter(function (item) {
            return item.grupoCompraItem === grupo;
        });

        // Ordena os números dos itens em ordem crescente
        itensDoGrupo.sort(function (a, b) {
            return a.numeroItem - b.numeroItem;
        });
        // Formatação do número da compra e unidade para o formato "00349/2023"
        var numeroCompraFormatado = itens[0].numeroCompra ? itens[0].numeroCompra.replace("-", "/") : "";
        var unidadeFormatada = itens[0].unidadeItem.replace(" - ", " ");
        // Monta a mensagem com os números dos itens do grupo
        var numerosItens = itensDoGrupo.map(function (item) {
            return item.numeroItem;
        });
        // Define "Item" ou "Itens" com base na quantidade de itens do grupo específico
        var palavraItemGrupoEspecifico = (numerosItens.length === 1) ? "do Item" : "dos Itens";
        var palavraItemGrupoEspecificoItens = (numerosItens.length === 1) ? "Item" : "Itens";
        var palavraItemGrupo = (numerosItens.length === 1) ? "pertencente" : "pertencentes";
        var palavraItemIsolado = (numerosItens.length === 1) ? "selecionado isolado" : "selecionados isolados";

        // Monta a mensagem final com a compra, unidade e números dos itens do grupo
        var mensagem = `Insira justificativa  ${palavraItemGrupoEspecifico.toLowerCase()} ${palavraItemGrupo.toLowerCase()}
        ao grupo ${grupo}, da compra ${numeroCompraFormatado}, da unidade ${unidadeFormatada}, ${palavraItemIsolado.toLowerCase()}
        do grupo:<br>${palavraItemGrupoEspecificoItens}: ${numerosItens.join(", ")}`;

        exibirAlertaNoty('info-custom', mensagem, false, false);
    }
    function toggleButtons() {
        // Verifica se há algum radio button selecionado
        const consultaAceitacao = $('input[name="consulta_aceitacao_fornecedor"]:checked').val();
        const adicionarButton = $('#adicionar');
        const enviarFornecedorButton = $('#enviar_fornecedor');
        const aceitaçãoFieldWrapper = $('[data-field-name="aceitacao"]');


        if (consultaAceitacao == '0') {
            adicionarButton.hide();
            enviarFornecedorButton.show();
            aceitaçãoFieldWrapper.hide();
        } else {
            adicionarButton.show();
            enviarFornecedorButton.hide();
            aceitaçãoFieldWrapper.show();
        }
    }

});

var campoObj = true;

function exibirEsconderBotao(exibir = false) {
    if (exibir) {
        // $("#rascunho").show()
        // $("#adicionar").show()
        $(".button-custom").show()
        return
    }
    // $("#rascunho").hide()
    // $("#adicionar").hide()
    $(".button-custom").hide()
}

// inserirLinhaItem()

// Método responsável em carregar o select2 na tela para o usuário
function carregarSelect(name, url, text, additionalData, tipoPesquisa, numeroAtaAdesao) {
    $(`[name='${name}']`).select2({
        theme: 'bootstrap',
        multiple: false,
        allowClear: true,
        ajax: {
            url: url,
            type: 'GET',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                var data = {
                    q: params.term, // search term
                    page: params.page, // pagination
                };
                if (additionalData) {
                    Object.assign(data, additionalData);
                }
                if (tipoPesquisa === 'fornecedor') {
                    data.fornecedor = true;
                }
                if (numeroAtaAdesao) {
                    data.numeroAtaAdesao = numeroAtaAdesao;
                }
                return data;
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                let paginate = false;

                if (data.data) {
                    paginate = data.current_page < data.last_page;
                    data = data.data;
                }

                return {
                    results: $.map(data, function (item) {
                        let texto = eval(`item.${text}`)
                        return {
                            text: texto,
                            id: item.id,
                        }
                    }),
                    pagination: {
                        more: paginate,
                    }
                };
            },
            cache: true

        },
    });
}

// Quando o fornecedor for selecionado
$('#fornecedor_id').on('select2:select', function (e) {
    var data = e.params.data;
    fornecedor_id = data.id
    carregarSelect('compra_fake', `${fornecedor_id}`, 'fornecedor_arp_adesao')
});
$('#arp_item').on('select2:select', function (e) {
    var data = e.params.data;
    arp_item = data.id
    carregarSelect('compra_fake', `${arp_item}`, 'catmat_descricao')

});

// Quando a ata for selecionada
$('#arp_id').on('select2:select', function (e) {
    var data = e.params.data;
    if (numeroAtaAdesao) {
        // carregarSelect('arp_id', numeroAtaAdesao, 'ata_arp_adesao', null, 'numeroAtaAdesao', data.id);
        carregarSelect('arp_id', `${unidade_id}/numeroAtaAdesao`, 'ata_arp_adesao', null, 'numeroAtaAdesao', data.id);

    }
});

// Quando a unidade gerenciadora for selecionada
$('#unidade_gerenciadora_id').on('select2:select', function (e) {
    var data = e.params.data;
    unidade_id = data.id
    carregarSelect('compra_fake', `${unidade_id}/compra`, 'numero_ano_adesao_arp')

    carregarSelect('arp_id', `${unidade_id}/numeroAtaAdesao`, 'ata_arp_adesao', null, 'numeroAtaAdesao');


    let urlBuscaUnidadeGerenciadoraExecacao = $("#url_buscar_unidade_gerenciadora_excecao").val()
    urlBuscaUnidadeGerenciadoraExecacao += `/${unidade_id}`

    // Verifica se a unidade gerenciadora pertence ao orgão do Ministério da saúde (36000)
    $.get(urlBuscaUnidadeGerenciadoraExecacao).done(function (response) {

        // Se pertencer, então exibe o campo
        if (response) {
            $("#areaAquisicaoEmergencialMedicamento").removeClass('invisible')
            return
        }

        $("#areaAquisicaoEmergencialMedicamento").addClass('invisible')
    })
});

// Quando a compra for selecionada
$('#compra_fake').on('select2:select', function (e) {
    var data = e.params.data;
    compra = data.text.replace("/", "-");
    //carregarSelect('modalidade_fake', `${unidade_id}/${compra}/modalidade`, 'compra')

    $.get(`${unidade_id}/${compra}/modalidade`).done(function (response) {
        $("[name='modalidade_fake']").select2({
            theme: "bootstrap",
            data: response,
            allowClear: true // Adicione essa opção

        })
    })

    // Insere o ID da compra para o cadastro
    $("#compra_id").val(data.id)

    // Insere o número da compra
    $("#numero_compra").val(compra)

});

// Quando a modalidade for selecionada
$('#modalidade_fake').on('select2:select', function (e) {
    var data = e.params.data;
    modalidade_id = data.id
    // carregarSelect('fornecedor_fake', `${unidade_id}/${compra}/${modalidade_id}/fornecedor`, 'fornecedor_arp_adesao')

    $.get(`${unidade_id}/${compra}/${modalidade_id}/fornecedor`).done(function (response) {

        $("[name='fornecedor_fake']").select2({
            theme: "bootstrap",
            data: response,
            allowClear: true // Adicione essa opção
        })
    })
});

// Clique do botão para exibir os itens para adesão, conforme o filtro
$("#btn_recuperar_compra_arp_adesao").click(function (e) {
    $(this).prop('disabled', true);
    // $.blockUI({ message: $('#loadingContratos') });
    $("#areaItemAdesao").show()
    let form = $("form").serialize()

    let unidadeGerenciadora =  $('select[name="unidade_gerenciadora_id"]').val();
    let compraFake =  $('select[name="compra_fake"]').val();
    let modalidadeFake =  $('select[name="modalidade_fake"]').val();
    let ataId =  $('select[name="arp_id"]').val();
    let itemAtaId =  $('select[name="arp_item"]').val();
    let fornecedorId =  $('select[name="fornecedor_id"]').val();

    if (unidadeGerenciadora == null && compraFake == null && ataId == null &&
        itemAtaId == null && fornecedorId == null && modalidadeFake == '') {
        exibirAlertaNoty('warning-custom', 'Selecionar ao menos um item dos filtros para realizar a busca')
        return
    }

    let unidadePertenceEsfera = $('input[name=execucao_descentralizada_programa_projeto_federal]:checked').val()
    let aquisicaoEmergencialMedicamento = $('input[name=aquisicao_emergencial_medicamento_material]:checked').val()
    let ataEnfrentandoImpactoDecorrenteCalamidadePublica =
        $('input[name=ata_enfrentando_impacto_decorrente_calamidade_publica]:checked').val()

    inserirLinhaItem(
        unidadeGerenciadora,
        compraFake,
        ataId,
        itemAtaId,
        fornecedorId,
        modalidadeFake,
        unidadePertenceEsfera,
        aquisicaoEmergencialMedicamento,
        ataEnfrentandoImpactoDecorrenteCalamidadePublica
    )

    dadosDataSetInputQuantidadeSolicitada = []
    quantidadeSolicitadaInserida = []
    dadosItemPreenchido = []
    controleQuantidadeItem = []

    datableItem.on('xhr', function() {
        $("#btn_recuperar_compra_arp_adesao").prop('disabled', false);
    });

})
let bloqueiaAnexoIsolado = true
// Método responsável em limpar os campos quando for digitado um valor
// superior ao permitido
function limparInformacaoQuantidadeSolicitada(idItem) {
    quantidadeSolicitadaInserida[idItem] = undefined;
    dadosDataSetInputQuantidadeSolicitada[idItem] = undefined;
    $(`#chk_${idItem}`).prop('checked', false);
    $(`#feedback_${idItem}`).hide();
    $(`#valornegociado_${idItem}`).html('');
    $(`span#valornegociado_${idItem}`).html('');
    // $(`input[name='quantidade_solicitada[${idItem}]']`).val('')
    // var quantidadeCampo = $(`input#quantidade_solicitada_${idItem}`);
    // quantidadeCampo.val('').trigger('input');
}

// Método responsável em exibir a mensagem para a quantidade solicitada
// quando o usuário digitar o valor no campo
function quantidadeLancada(campo) {
    let unidadePertenceEsfera = $('input[name=execucao_descentralizada_programa_projeto_federal]:checked').val() ??
        $('input[name=execucao_descentralizada_programa_projeto_federal]').val()

    let aquisicaoEmergencialMedicamento = $('input[name=aquisicao_emergencial_medicamento_material]:checked').val() ??
        $('input[name=aquisicao_emergencial_medicamento_material]').val()

    if (campo.value == '') {
        adicionarRemoverQuantidadeItemCompra(
            campo.dataset.compraId,
            campo.dataset.compraItemId,
            quantidadeSolicitadaInserida[campo.dataset.id] ?? 0,
            campo.dataset.id,
            campo.value,
            false
        )

        validarQuantidadeCarona(campo.dataset, unidadePertenceEsfera, aquisicaoEmergencialMedicamento)

        limparInformacaoQuantidadeSolicitada(campo.dataset.id)

        quantidadeSolicitadaInserida[campo.dataset.id] = campo.value;
        dadosDataSetInputQuantidadeSolicitada[campo.dataset.id] = campo.dataset;

        return;
    }

    dadosDataSetInputQuantidadeSolicitada[campo.dataset.id] = campo.dataset;

    var unidadeCodigo = campo.getAttribute('data-unidadeadesao');
    var numero = unidadeCodigo.replace(/\D/g, '');

    var compraLei = campo.getAttribute('data-lei');


    // Recupera o valor do valor unitário
    let valorUnitario = $(`#valorunitario_${campo.dataset.id}`).text();

    // Substitui os valores para poder realizar o cálculo com o valor digitado pelo usuário
    valorUnitario = valorUnitario.replace(".", "").replace(",", ".");

    // Realiza o cálculo do valor total do item para aquela adesão
    let valorTotalAdesao = valorUnitario * parseFloat(campo.value);
    $(`td #valornegociado_${campo.dataset.id}`).html(valorTotalAdesao.toLocaleString('pt-br', {
        style: 'currency',
        currency: 'BRL'
    }));

    // Valor do saldo na coluna QUantidade Disponível para Adesão
    let maximoAdesao = parseFloat(campo.dataset.maximoadesao)

    // Valor calculado dos 50% da coluna Quantidade Registrada
    let maximoCinquantaPorCento = parseFloat(campo.dataset.maximocinquentaporcento)

    // Inicia o máximo de lançamento com a quantidade dos 50% da coluna Quantidade Registrada
    let maximoLancamento = maximoCinquantaPorCento

    campo.addEventListener('input', function () {
        if (campo.value === '') {
            $(`span#valornegociado_${campo.dataset.id}`).html('');
            $(`#chk_${campo.dataset.id}`).prop('checked', false);

        }
    });

    if (itemPertenceCentralDeCompras(numero, compraLei)) {
        $(`#chk_${campo.dataset.id}`).prop('checked', true);
        $(`#feedback_${campo.dataset.id}`).hide();
        quantidadeSolicitadaInserida[campo.dataset.id] = campo.value;
        return false;
    }
        if (unidadePertenceEsfera == false && aquisicaoEmergencialMedicamento == false) {
            // Se o máximo de adesão for menor que os 50% então exibe o valor do máximo de adesão
            if (maximoAdesao < maximoCinquantaPorCento) {
                maximoLancamento = maximoAdesao
            }

            if (parseFloat(campo.value) > maximoLancamento) {
                // Formata o número para o padrão português
                maximoLancamento = formatnumberPTBR(maximoLancamento)

                // Exibe o alerta
                exibirAlertaNoty('warning-custom', `O valor máximo permitido para este item é ${maximoLancamento}`);
                limparInformacaoQuantidadeSolicitada(campo.dataset.id)
                adicionarRemoverQuantidadeItemCompra(
                    campo.dataset.compraId,
                    campo.dataset.compraItemId,
                    quantidadeSolicitadaInserida[campo.dataset.id] ?? 0,
                    campo.dataset.id,
                    parseFloat(campo.value),
                    false
                )
                return;
            }
        }

        // Se o campo de aquisição ou a unidade pertence a esfera marcado como 'SIM'
        if ((unidadePertenceEsfera == true || unidadePertenceEsfera == undefined) ||
            (aquisicaoEmergencialMedicamento == true || aquisicaoEmergencialMedicamento == undefined)) {
            // Se o usuário marcar em algum campo de exeção o SIM e o valor digitado for maior que os 50%
            // da coluna Quantidade Registrada
            if ((unidadePertenceEsfera == true || aquisicaoEmergencialMedicamento == true)) {
                $(`#chk_${campo.dataset.id}`).prop('checked', true);
                $(`#feedback_${campo.dataset.id}`).hide();

                maximoLancamento = maximoCinquantaPorCento
                let incremetarQuantidade = true
                if (parseFloat(campo.value) > maximoLancamento) {
                    // Formata o número para o padrão português
                    maximoLancamento = formatnumberPTBR(maximoLancamento)
                    // Exibe o alerta
                    exibirAlertaNoty(
                        'warning-custom',
                        `O valor máximo permitido para este item é ${maximoLancamento}`
                    );
                    limparInformacaoQuantidadeSolicitada(campo.dataset.id)
                    incremetarQuantidade = false
                }

                adicionarRemoverQuantidadeItemCompra(
                    campo.dataset.compraId,
                    campo.dataset.compraItemId,
                    quantidadeSolicitadaInserida[campo.dataset.id] ?? 0,
                    campo.dataset.id,
                    parseFloat(campo.value),
                    incremetarQuantidade)

                validarQuantidadeCarona(campo.dataset, unidadePertenceEsfera, aquisicaoEmergencialMedicamento)
                // Insere o valor no array para salvar o valor inserido
                quantidadeSolicitadaInserida[campo.dataset.id] = campo.value;
                return;
            }

            // Se o valor digitado for maior que o permitido,  e todos estiverem com a opção Não,
            // então exibe uma mensagem para o usuário
            if (
                (parseFloat(campo.value) > maximoCinquantaPorCento || parseFloat(campo.value) > maximoAdesao) &&
                ((unidadePertenceEsfera == false || unidadePertenceEsfera == undefined) &&
                    (aquisicaoEmergencialMedicamento == false || aquisicaoEmergencialMedicamento == undefined))
            ) {
                // Se o valor for negativo, então exibe que não existe saldo
                if (maximoAdesao < 0) {
                    // Exibe o alerta
                    exibirAlertaNoty('warning-custom', `Não existe saldo para utilizar neste item`);
                    // campo.value = ''; // Limpar o valor digitado
                    $(`#chk_${campo.dataset.id}`).prop('checked', false); // Desmarcar o checkbox
                    $(`span#valornegociado_${campo.dataset.id}`).html(''); // Limpar o campo valornegociado
                    adicionarRemoverQuantidadeItemCompra(
                        campo.dataset.compraId,
                        campo.dataset.compraItemId,
                        quantidadeSolicitadaInserida[campo.dataset.id] ?? 0,
                        campo.dataset.id,
                        parseFloat(campo.value),
                        false
                    )
                    return;
                }

                if (maximoLancamento > maximoAdesao) {
                    maximoLancamento = maximoAdesao
                }

                // Se a quantidade disponivel for menor que a digitada pelo usuário
                // então exibe no alerta o valor do campo quantidade disponível adesão
                if (parseFloat(campo.value) > maximoLancamento) {
                    // Formata o número para o padrão português
                    maximoLancamento = formatnumberPTBR(maximoLancamento)

                    // Exibe o alerta
                    exibirAlertaNoty(
                        'warning-custom',
                        `O valor máximo permitido para este item é ${maximoLancamento}`);

                    limparInformacaoQuantidadeSolicitada(campo.dataset.id)
                    adicionarRemoverQuantidadeItemCompra(
                        campo.dataset.compraId,
                        campo.dataset.compraItemId,
                        quantidadeSolicitadaInserida[campo.dataset.id] ?? 0,
                        campo.dataset.id,
                        parseFloat(campo.value),
                        false
                    )
                    return;
                }
            }
        }

        $(`#chk_${campo.dataset.id}`).prop('checked', true);
        $(`#feedback_${campo.dataset.id}`).hide();

        // Marcar o checkbox automaticamente após informar um valor e clicar fora da caixa
        let valida = quantidadeSelecionada(document.getElementById(`chk_${campo.dataset.id}`), campo.dataset.id);
        // limpa os valores do input e span
        if (valida == false) {
            $(`span#valornegociado_${campo.dataset.id}`).html('');
            var quantidadeCampo = $(`input#quantidade_solicitada_${campo.dataset.id}`);
            quantidadeCampo.val('').trigger('input');
        }

        adicionarRemoverQuantidadeItemCompra(
            campo.dataset.compraId,
            campo.dataset.compraItemId,
            quantidadeSolicitadaInserida[campo.dataset.id] ?? 0,
            campo.dataset.id,
            parseFloat(campo.value))

    validarQuantidadeCarona(campo.dataset, unidadePertenceEsfera, aquisicaoEmergencialMedicamento)

    // Insere o valor no array para salvar o valor inserido
    quantidadeSolicitadaInserida[campo.dataset.id] = campo.value;
}

function formatnumberPTBR(numero) {
    return new Intl.NumberFormat('pt-BR').format(numero)
}

function quantidadeSelecionada(checkbox, idCheck) {
    let unidadesGerenciadorasSelecionadas =[]

    Object.keys(quantidadeSolicitadaInserida).forEach(function(index) {
        if (dadosDataSetInputQuantidadeSolicitada[index] == null) {
            return
        }

        let dados = {}
        dados ['unidadeId'] = dadosDataSetInputQuantidadeSolicitada[index].unidadeadesao
        dados['compraId'] = dadosDataSetInputQuantidadeSolicitada[index].compraId
        unidadesGerenciadorasSelecionadas.push(dados)
    })

    var unidadeId = $(checkbox).data('unidade-id');
    var compraId = $(checkbox).data('compra-id');

    var unidadesDiferentes = unidadesGerenciadorasSelecionadas.filter(function (unidade) {
        return unidade.unidadeId !== unidadeId || unidade.compraId != compraId;
    });

    if (checkbox.checked && unidadesDiferentes.length > 0) {
        checkbox.checked = false;

        // Remover os valores selecionados das variáveis de controle
        removeElement(dadosDataSetInputQuantidadeSolicitada, checkbox.dataset.id);
        removeElement(quantidadeSolicitadaInserida, checkbox.dataset.id);

        // delete dadosDataSetInputQuantidadeSolicitada[checkbox.dataset.id]
        // delete quantidadeSolicitadaInserida[checkbox.dataset.id]

        if (unidadesDiferentes.some(function (unidade) {
            return unidade.compraId !== compraId;
        })) {
            exibirAlertaNoty('error-custom', 'Atenção, não é permitida a inclusão de mais de uma Compra por Solicitação.');
            return false;
        }

        exibirAlertaNoty('error-custom', 'Atenção, não é permitida a inclusão de mais de uma Unidade Gerenciadora por Solicitação.');
        return false;
    }

    validarUnidadesGerenciadoras();
}

function validarUnidadesGerenciadoras() {
    var checkboxesSelecionados = $('[id^="chk_"]:checked');
    var unidadesGerenciadorasSelecionadas = checkboxesSelecionados.map(function () {
        return $(this).data('unidade-id');
    }).get();

    var uniqueUnits = [...new Set(unidadesGerenciadorasSelecionadas)];

    $('[id^="chk_"]').each(function () {
        var unidadeId = $(this).data('id');
        var quantidadeCampo = $(`input[name='quantidade_solicitada[${unidadeId}]']`);
        var feedback = $(`#feedback_${unidadeId}`);

        if (uniqueUnits.length > 1 || (uniqueUnits.length === 1 && !checkboxesSelecionados.is(':checked'))) {
            quantidadeCampo.attr('disabled', true);
            feedback.hide();
        } else {
            quantidadeCampo.attr('disabled', false);
            feedback.show();
        }
    });
}

function habilitarLancamento(itemSelecionado) {
    if (itemSelecionado.checked) {
        $(`input[name='quantidade_solicitada[${itemSelecionado.value}]']`).attr("disabled", false)
        $(`#feedback_${itemSelecionado.value}`).show()
        return
    }
    $(`input[name='quantidade_solicitada[${itemSelecionado.value}]']`).attr("disabled", true)
    $(`#feedback_${itemSelecionado.value}`).hide()
}

$(".selectAll").on("click", function (e) {
    marcartodos()
});

function marcartodos() {
    state = $(".selectAll").is(":checked")
    cols = datableItem.column(0).nodes(), state

    for (i = 0; i < cols.length; i += 1) {
        idItem = cols[i].querySelector("input[type='checkbox']")
        idItem.checked = state
        habilitarLancamento(idItem)
    }
}

var bloquearSubmit = true

//Sobreescreveu o metodo para fazer a validacao nas datas
$(".button-custom").click(function (e) {
    e.preventDefault()
    let buttonId = $(this).attr("id");



    let todosValoresNull = quantidadeSolicitadaInserida.every(function(element) {
        return element === undefined;
    });

    if (quantidadeSolicitadaInserida.length == 0 || todosValoresNull) {
        exibirAlertaNoty('error-custom', 'Preencha ao menos um item e verifique se a quantidade informada pode ser solicitada')
        return;
    }

    let id = $(this).attr("id")
    let nameHidden = $(`#${id} span`).data("namecustom")
    let valueHidden = $(`#${id} span`).data("valuecustom")

    let enviarFornecedor = $('input[name="consulta_aceitacao_fornecedor"]:checked').val();
    let rascunho = $("input[name='rascunho']").val();


    if (valueHidden == 1 && enviarFornecedor === "0") {
        valueHidden = 0;
    }


    if (rascunho == 1 && buttonId === 'rascunho') {
        valueHidden = 1;
    }


    if (valueHidden == 1 && enviarFornecedor === 1
        && buttonId === "enviar_fornecedor"
       ) {
        valueHidden = 0;
    }

    bloquearSubmit = false
    $("#justificativa").prop('required', false);

    $("#demonstracao").prop('required', false);

    $("#aceitacao").prop('required', false);

    $("#item_isolado").prop('required', false);


    $("textarea[name=texto_justificativa]").prop('required', false);
    $("textarea[name=texto_justificativa_item_isolado]").prop('required', false);
    $("input[name=demonstracao_valores_registrados]").prop('required', false)
    $("input[name=consulta_aceitacao_fornecedor]").prop('required', false)
    if (valueHidden == 0) {
        let anexoJustificativa = document.getElementById("justificativa").files.length

        let anexoDemonstracao = document.getElementById("demonstracao").files.length

        let anexoAceitacao = document.getElementById("aceitacao").files.length

        let bdAnexoJustificativa = $("#qtd_justificativa").val()

        let bdAnexoDemonstracao = $("#qtd_demonstracao").val()

        let bdAnexoAceitacao = $("#qtd_aceitacao").val()

        // let anexoItemIsoladoSelecionado = document.getElementById("item_isolado").files.length;


        if (anexoJustificativa == 0 && (bdAnexoJustificativa == 0 || bdAnexoJustificativa == undefined)) {
            $("#justificativa").prop('required', true);
            exibirAlertaNoty('error-custom', `Selecione um anexo para a justificativa`)
            return;
        }

        if (anexoDemonstracao == 0 && (bdAnexoDemonstracao == 0 || bdAnexoDemonstracao == undefined)) {
            $("#demonstracao").prop('required', true);
            exibirAlertaNoty('error-custom', `Selecione um anexo para a demonstração`)
            return;
        }


        if (enviarFornecedor === "1") {
            if (anexoAceitacao == 0 && (bdAnexoAceitacao == 0 || bdAnexoAceitacao == undefined)) {
                $("#aceitacao").prop('required', true);
                exibirAlertaNoty('error-custom', `Selecione um anexo para a aceitação`);
                return;
            }
        }

        let marcacaoUsuarioEnviarForm = $('input[name=execucao_descentralizada_programa_projeto_federal]:checked').val()

        if (marcacaoUsuarioEnviarForm == 1) {
            let anexoComprovacaoExecucaoDescentralizadaProgramaProjetoFed = document.
                        getElementsByName("anexo_comprovacao_execucao_descentralizada_programa_projeto_fed")[0].
                            files.length
            if (anexoComprovacaoExecucaoDescentralizadaProgramaProjetoFed == 0) {
                exibirAlertaNoty('error-custom', `Selecione um anexo para o campo anexo da comprovação`)
                bloquearSubmit = true
                return;
            }
            $("textarea[name=justificativa_anexo_comprovacao_execucao_descentralizada_progra]").prop('required', true);
        }


        $("textarea[name=texto_justificativa]").prop('required', true);
        if (bloqueiaAnexoIsolado === true) {
            $("textarea[name=texto_justificativa_item_isolado]").prop('required', true);
        }

        $("input[name=demonstracao_valores_registrados]").prop('required', true);
        $("input[name=consulta_aceitacao_fornecedor]").prop('required', true);
    }

    let formValidation = $(this).closest('form')[0]

    if (formValidation.checkValidity()) {

        $('#hidden_custom').attr('name', nameHidden);
        $('#hidden_custom').attr('value', valueHidden);

        var saveActions = $('#saveActions'),
            crudForm = saveActions.parents('form'),
            saveActionField = $('[name="_save_action"]');

        crudForm.submit(function (event) {
            window.removeEventListener('beforeunload', preventUnload);
            $("button[type=submit]").prop('disabled', true);
        });

        atualizaArrayDeInputsQuantidadeSolicitadaSubmit()
        crudForm.submit();
    }

    // Método responsável em percorrer os campos obrigatórios para exibir o alerta
    formValidation.forEach(function (item) {

        if (item.validationMessage != "") {

            if (item.className == 'quantidade_solicitada nao_fracionado') {
                let mensagem = item.validationMessage.replace('Insira ', 'insira ')
                exibirAlertaNoty('error-custom', `No campo Quantidade solicitada, ${mensagem}`)
            }

            if (item.name == 'processo_adesao') {
                exibirAlertaNoty('error-custom', 'Preencha o campo Processo Adesão')
            }

            if (item.name == 'texto_justificativa') {
                exibirAlertaNoty('error-custom', 'Preencha o campo Justificativa da vantagem da adesão')
            }
            if (item.name == 'texto_justificativa_item_isolado') {
                exibirAlertaNoty('error-custom', 'Preencha o campo Justificativa de item isolado pertencente a um grupo')
            }

            if (item.name == 'justificativa_anexo_comprovacao_execucao_descentralizada_progra') {
                exibirAlertaNoty('error-custom', 'Preencha o campo Justificativa da comprovação da execução descentralizada de programa ou projeto federal')
            }


            if (item.id == 'demonstracao_valores_registrados_0') {
                exibirAlertaNoty(
                    'error-custom',
                    `Selecione uma opção para o item
                    "Foi realizada demonstração de que os valores registrados estão compatíveis
                    com os valores praticados pelo mercado,
                    nos termos da Lei 14.133/2021 (Art.23 e Art. 86, §2º, inc. II)?"`
                )
            }

            if (item.id == 'consulta_aceitacao_fornecedor_0') {
                exibirAlertaNoty(
                    'error-custom',
                    `Selecione uma opção para o item
                    "Houve prévia consulta e aceitação do fornecedor,
                    nos termos da Lei 14.133/2021 (Art. 86, §2º, inc. III)?'`
                )
            }
        }
    });
});

function preventUnload(event) {
    if (initData !== getFormData()) {
        // Cancel the event as stated by the standard.
        event.preventDefault();
        // Older browsers supported custom message
        event.returnValue = '';
    }
}

function acaoUltimaGuia(guiaAtual, ultimaGuia) {
    if (guiaAtual == ultimaGuia) {
        exibirEsconderBotao(true)
        return
    }

    exibirEsconderBotao()
}

/**
 * Método responsável em recuperar o click do botão para expandir as colunas
 */
$('#table_selecionar_item_adesao_arp').on('click', 'tbody td.dtr-control', function () {
    var tr = $(this).closest('tr');
    var row = datableItem.row(tr);
    let colunasDatatable = row.data()

    // Recupera o valor inserido pelo usuário
    let valorInserido = quantidadeSolicitadaInserida[colunasDatatable.id]

    // Verifica se a ação vai ser recolher as colunas
    if (!row.child.isShown()) {
        // Seta o valor do campo padrão sem nenhum valor
        colunasDatatable.campovazioquantidadesolicitada = colunasDatatable.quantidadesolicitada

        // Se achar o valor inserido
        if (valorInserido != undefined) {
            // Substitui o valor padrão pelo inserido
            novoCampoComValor = colunasDatatable.quantidadesolicitada.replace(/value='(.*?)'/, `value='${valorInserido}'`)
        }

        if (valorInserido == undefined) {
            novoCampoComValor = colunasDatatable.quantidadesolicitada.replace(/value='(.*?)'/, `value=''`)
        }

        colunasDatatable.quantidadesolicitada = novoCampoComValor
    }

    // Altera o valor para que entre na regra de validação dos campos
    $(`#quantidade_solicitada_${colunasDatatable.id}`).val(valorInserido).change();
});

function analisarCampoQuantidadeExcecao(campo) {
    let campoPreenchidoQuantidadeSolicitada = []

    $("td .quantidade_solicitada").each(function () {
        if ($(this).val() != '') {
            if (campo.value == 0) {
                let valorCampo = $(this).val()
                let maximocinquentaporcento = $(this).data('maximocinquentaporcento')
                let maximoadesao = $(this).data('maximoadesao')

                if (parseFloat(valorCampo) > maximocinquentaporcento || parseFloat(valorCampo) > maximoadesao) {
                    let idItem = $(this).data('id')
                    let numeroItem = $(this).data('numero')
                    let compraItem = $(this).data('numerocompras')
                    let unidadeItem = $(this).data('unidadeadesao')

                    maximoLancamento = maximocinquentaporcento
                    if (parseFloat(valorCampo) > campo.dataset.maximoadesao) {
                        maximoLancamento = maximoadesao
                    }

                    // Formata o número para o padrão português
                    maximoLancamento = formatnumberPTBR(maximoLancamento)

                    let mensagem =
                        `O valor máximo para o item ${numeroItem}
                    da compra ${compraItem} e unidade ${unidadeItem}
                    é ${maximoLancamento}.<br>`

                    campoPreenchidoQuantidadeSolicitada.push(mensagem)
                    limparInformacaoQuantidadeSolicitada(idItem)
                }
            }


        }
    });

    if (campoPreenchidoQuantidadeSolicitada.length > 0) {
        let mensagemAlerta = 'A(s) quantidade(s) solicitada(s) ultrapassa(m) o valor máximo:<br>'
        // Percorre as mensagem dos itens que tiveram os valores superiores ao máximo
        $.each(campoPreenchidoQuantidadeSolicitada, function (key, value) {
            mensagemAlerta += value
        });

        exibirAlertaNoty('warning-custom', mensagemAlerta)
    }
}

$('#table_selecionar_item_adesao_arp').on('draw.dt', function () {
    $(this).find('tbody tr').each(function () {
        var tr = $(this).closest('tr');
        var row = datableItem.row(tr);
        let colunasDatatable = row.data()

        if (colunasDatatable === undefined) {
            return;
        }

        let pertenceCentralDeCompras =
            itemPertenceCentralDeCompras(colunasDatatable.codigo_unidade_gerenciadora, colunasDatatable.lei)

        if (pertenceCentralDeCompras) {
            datableItem.column(14).visible(false);
        }

        // Verifique se o checkbox está marcado
        var checked = $(this).find('td:eq(0) input[type="checkbox"]');
        if (checked.length == 0) {
            return
        }

        let itemSelecionado = quantidadeSolicitadaInserida[checked[0].dataset.id]

        if (itemSelecionado != undefined) {
            checked.prop('checked', true)

            // Seta o valor do campo padrão sem nenhum valor
            colunasDatatable.campovazioquantidadesolicitada = colunasDatatable.quantidadesolicitada

            // Se achar o valor inserido
            if (itemSelecionado != undefined) {
                // Substitui o valor padrão pelo inserido
                novoCampoComValor = colunasDatatable.quantidadesolicitada.replace(/value='(.*?)'/, `value='${itemSelecionado}'`)
            }

            if (itemSelecionado == undefined) {
                novoCampoComValor = colunasDatatable.quantidadesolicitada.replace(/value='(.*?)'/, `value=''`)
            }

            colunasDatatable.quantidadesolicitada = novoCampoComValor


            // Altera o valor para que entre na regra de validação dos campos
            $(`#quantidade_solicitada_${colunasDatatable.id}`).val(itemSelecionado).change();
        }
    });
});

function removeElement(array, index) {
    if (index >= 0 && index < array.length) {
        array[index] = null; // Ou qualquer outro valor que você queira usar para indicar a remoção
    }
}

function itemPertenceCentralDeCompras(codigoUnidade, compraLei) {
    var match = compraLei.match(/MP1221/i);
    var resultado = match ? match[0] : null;

    if (codigoUnidade === "201057" && resultado !== null) {
        return true
    }

    return false
}

function adicionarRemoverQuantidadeItemCompra(
    compraId,
    numeroItem,
    quantidadeAtual,
    arpItemId,
    quantidadeDigitada,
    adicionar = true
) {
    if (isNaN(quantidadeDigitada)) {
        return;
    }

    if (quantidadeDigitada == '') {
        quantidadeDigitada = 0
    }

    // let ultimaQuantidadeLancada = 0
    // if (controleQuantidadeItem[arpItemId] != undefined) {
    //     ultimaQuantidadeLancada = controleQuantidadeItem[arpItemId]
    //
    //     if (quantidadeAtual == quantidadeDigitada && adicionar) {
    //         return
    //     }
    //
    //     // if (dadosItemPreenchido[compraId] != undefined &&
    //     //     dadosItemPreenchido[compraId][numeroItem] != undefined
    //     // ) {
    //     //     console.log('linha 1167', dadosItemPreenchido[compraId][numeroItem])
    //     //     console.log('linha 1168', ultimaQuantidadeLancada)
    //     //     dadosItemPreenchido[compraId][numeroItem] -= ultimaQuantidadeLancada
    //     // }
    // }

    // console.log('ultimaQuantidadeLancada', ultimaQuantidadeLancada)

    // if (dadosItemPreenchido[compraId] != undefined) {
    //     if (adicionar) {
    //         dadosItemPreenchido[compraId][numeroItem] += parseFloat(quantidade)
    //     }
    //
    //     if (!adicionar) {
    //         dadosItemPreenchido[compraId][numeroItem] -= parseFloat(quantidade)
    //     }
    // }
    //
    // if (dadosItemPreenchido[compraId] == undefined && adicionar) {
    //     dadosItemPreenchido[compraId] = []
    //     dadosItemPreenchido[compraId][numeroItem] = parseFloat(quantidade)
    // }
    //

    if (controleQuantidadeItem[arpItemId] != undefined) {
        controleQuantidadeItem[arpItemId] -= quantidadeAtual;
        controleQuantidadeItem[arpItemId] += parseFloat(quantidadeDigitada);
    }

    if (controleQuantidadeItem[arpItemId] == undefined) {
        controleQuantidadeItem[arpItemId] = [];

        if(quantidadeDigitada != '') {
            controleQuantidadeItem[arpItemId] =  parseFloat(quantidadeDigitada);
        }
    }

    if (dadosItemPreenchido[compraId] != undefined) {
        if (adicionar) {
            if (dadosItemPreenchido[compraId][numeroItem] > 0 && quantidadeDigitada > 0) {
                dadosItemPreenchido[compraId][numeroItem] -= quantidadeAtual
            }

            dadosItemPreenchido[compraId][numeroItem] += controleQuantidadeItem[arpItemId]
        }

        if (!adicionar) {
            if (parseFloat(quantidadeDigitada) == 0) {
                quantidadeDigitada = quantidadeAtual
            }

            dadosItemPreenchido[compraId][numeroItem] -= parseFloat(quantidadeDigitada)
        }
    }

    if (dadosItemPreenchido[compraId] == undefined && adicionar) {
        dadosItemPreenchido[compraId] = []
        dadosItemPreenchido[compraId][numeroItem] = controleQuantidadeItem[arpItemId]
    }
}

function validarQuantidadeCarona(dataset, unidadePertenceEsfera = 0, aquisicaoEmergencialMedicamento = 0) {
    if (dadosItemPreenchido[dataset.compraId] == undefined) {
        return;
    }
    dados = {
        quantidadeTotalItem: dadosItemPreenchido[dataset.compraId][dataset.compraItemId] || 0,
        compraItemId: dataset.compraItemId,
        maximoAdesao: dataset.maximoadesao,
        compraId: dataset.compraId,
        compraItemFornecedorId: dataset.compraItemFornecedorId,
        fornecedorId: dataset.fornecedorId,
        quantidadeHomologadaVencedor: dataset.quantidadeHomologadaVencedor,
        unidadePertenceEsfera: unidadePertenceEsfera ?? false,
        aquisicaoEmergencialMedicamento: aquisicaoEmergencialMedicamento ?? false
    }

    if (dados.quantidadeTotalItem == 0) {
        return
    }

    $.post( '/arp/adesao/validarquantitativo', dados).done(function( response ) {
        if (response.code == 500) {
            exibirAlertaNoty(`${response.type}-custom`, response.message)
            // limparInformacaoQuantidadeSolicitada(dataset.id)

            let idItem = dataset.id
            dadosItemPreenchido[dataset.compraId][dataset.compraItemId] -= quantidadeSolicitadaInserida[idItem]
            dadosDataSetInputQuantidadeSolicitada[idItem] = undefined;

            $(`#chk_${idItem}`).prop('checked', false);
            $(`#feedback_${idItem}`).hide();
            $(`#valornegociado_${idItem}`).html('');
            $(`span#valornegociado_${idItem}`).html('');
        }
    }) .catch((error) => {
        exibirAlertaNoty(`error-custom`, error)
    })
}

function exibirCampoExecucaoDescentralizadaProgramaProjetoFederal(data) {
    let url = $("#url_exibir_campo_execucao_descentralizada_programa_projeto_federal").val();

    $("#execucao_descentralizada_programa_projeto_federal").removeClass('d-block')
        .addClass('d-none')
    $(".areaExecucaoDescentralizadaProgramaProjetoFederal").removeClass('d-block').addClass('d-none')

    $.ajax({
        url: `${url}/${data.id}`,
        type: "GET",
        success: function (response) {
            if (response) {
                $("#execucao_descentralizada_programa_projeto_federal").removeClass('d-none')
                    .addClass('d-block')
            }
        }
    });
}

function exibirCampoAnexoComprovacao(data) {
    let marcacaoUsuario = $('input[name=execucao_descentralizada_programa_projeto_federal]:checked').val()

    if (marcacaoUsuario == 0) {
        $(".areaExecucaoDescentralizadaProgramaProjetoFederal").removeClass('d-block').addClass('d-none')
        $('input[name=execucao_descentralizada_programa_projeto_federal]').prop('required', false)
        return
    }

    let textoMarcacaoUsuario= 'Não'
    if (marcacaoUsuario == 1) {
        textoMarcacaoUsuario = 'Sim'
    }

    let span =
        document.querySelector('#marcacao_usuario_execucao_descentralizada_programa_projeto_federal span');

    span.textContent = textoMarcacaoUsuario;

    $(".areaExecucaoDescentralizadaProgramaProjetoFederal").removeClass('d-none').addClass('d-block')

    $('input[name=execucao_descentralizada_programa_projeto_federal]').prop('required', true)
}