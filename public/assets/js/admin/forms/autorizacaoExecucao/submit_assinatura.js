// variável global consultar arquivo form_content.blade.php:50
bloquearSubmit = true;
let alertSubmitItensNegativo = false;
let alertEmpenhosNegativo = false;

$('.button-custom:not(#assinatura,#assinar,#recusar_assinatura,#ajustar_trp,#ajustar_trd,.ajax)').on('click', function () {
    if (bloquearSubmit) {
        bloquearSubmit = false;
        $(this).trigger('click');
    }
})

$('#assinatura').on('click', function () {
    const button = this;
    alertSaldoNegativo()
        .then((result) => {
            if (result) {
                selectResponsaveis(() => {
                    bloquearSubmit = false;
                    $(button).trigger('click');
                })
            }
        })
});

$('#assinar').on('click', function () {
    const button = this;

    alertSaldoNegativo()
        .then((result) => {
            if (result) {
                const pathArquivo = $('iframe.pdf').attr('src').replace('/storage', '').split('?')[0];
                const redirectSuccess = window.location.href.replace('/create', '/assinar');
                const redirectError = window.location.href;
                const paginaAssinatura = $('.pagina_assinatura span').text().trim();
                const posicaoXAssinatura = $('.posicao_x_assinatura span').text().trim();
                const posicaoYAssinatura = $('.posicao_y_assinatura span').text().trim();

                assinarGov(pathArquivo, redirectSuccess, redirectError, paginaAssinatura, posicaoXAssinatura,
                    posicaoYAssinatura)
            }
        })
})

$('#recusar_assinatura').on('click', function () {
    const textareaHtml = '<label for="motivo_recusa">Descrição dos Motivos da Recusa</label>' +
        '<textarea class="col-12 p-2"  id="motivo_recusa" name="motivo_recusa" rows="3" placeholder="Motivo da recusa"></textarea>';

    swal({
        title: 'Motivo da recusa',
        content: {
            element: "div",
            attributes: {
                innerHTML: textareaHtml
            },
        },
        buttons: {
            cancel: {
                text: 'Cancelar',
                value: null,
                visible: true,
                className: 'br-button secondary mr-3',
                closeModal: true,
            },
            confirm: {
                text: 'Prosseguir',
                value: true,
                visible: true,
                className: 'br-button primary mr-3',
                closeModal: false,
            }
        },
    }).then((value) => {
        const motivoRecusa = $('#motivo_recusa').val()
        if (value) {
            if (motivoRecusa.length > 0) {
                window.location.href = window.location.href.replace(
                    '/create',
                    '/recusar?recusar_assinatura=true&motivo_recusa=' + motivoRecusa
                );
            } else {
                swal({
                    title: 'Atenção',
                    text: 'Por favor, informe uma descrição para o motivo da recusa',
                    buttons: {
                        confirm: {
                            text: 'Confirmar',
                            value: true,
                            visible: true,
                            className: 'br-button primary mr-3',
                            closeModal: true,
                        }
                    },
                });
            }
        }
    })
});

$('#ajustar_trp, #ajustar_trd').on('click', function () {
    let ajustarTermo = 'TRP';
    if (this.id === 'ajustar_trd') {
        ajustarTermo = 'TRD';
    }
    swal({
        title: 'Atenção',
        text: `Tem certeza que deseja ajustar o ${ajustarTermo}?`,
        buttons: {
            cancel: {
                text: 'Cancelar',
                value: null,
                visible: true,
                className: 'br-button secondary mr-3',
                closeModal: true,
            },
            confirm: {
                text: 'Confirmar',
                value: true,
                visible: true,
                className: 'br-button primary mr-3',
                closeModal: false,
            }
        },
    }).then((value) => {
        if (value) {
            window.location.href = window.location.href.replace(
                '/assinatura/create',
                '/ajustar'
            );
        }
    })
});

function assinarGov(
    pathArquivo,
    redirectSuccess = null,
    redirectError = null,
    paginaAssinatura = null,
    posicaoXAssinatura = null,
    posicaoYAssinatura = null
) {
    location.href = '/assinagov/autorizacao?pathArquivo=' + pathArquivo +
        '&redirectSuccess=' + redirectSuccess +
        '&redirectError=' + redirectError +
        '&pagina_assinatura=' + paginaAssinatura +
        '&posicao_x_assinatura=' + posicaoXAssinatura +
        '&posicao_y_assinatura=' + posicaoYAssinatura;
}

let alertSaldoNegativo = () => {
    const mensagem = getMensagemSaldoNegativo(alertSubmitItensNegativo, alertEmpenhosNegativo)

    if (mensagem && bloquearSubmit) {
        return swal({
            title: 'Atenção',
            text: mensagem,
            icon: 'warning',
            buttons: ['Cancelar', 'Confirmar'],
            dangerMode: true,
        })
    }

    return new Promise((resolve, reject) => {
        resolve(bloquearSubmit);
    });
}

let getMensagemSaldoNegativo = (alertSubmitItensNegativo, alertEmpenhosNegativo = false) => {
    let result = false;

    if (alertSubmitItensNegativo && alertEmpenhosNegativo) {
        result = 'O Saldo dos item(ns) e dos empenho(s) dessa OS/F estão negativos, deseja prosseguir mesmo assim?';
    } else if (alertSubmitItensNegativo) {
        result = 'O Saldo dos item(ns) dessa OS/F estão negativos, deseja prosseguir mesmo assim?';
    } else if (alertEmpenhosNegativo) {
        result = 'O Saldo dos empenho(s) dessa OS/F estão negativos, deseja prosseguir mesmo assim?';
    }

    return result;
}
