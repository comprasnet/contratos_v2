$(document).ready(function () {

    // variável global consultar arquivo form_content.blade.php:50
    bloquearSubmit = true;

    let exibirAlertaExtingir = true;
    let exibirAlertaSaldoNegativo = true;

    $('#alterar_disabled').click(function (event) {
        swal({
            title: 'Atenção',
            text: 'Não é possível enviar para assinatura, pois já existe outra alteração aguardando assinatura',
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'Ciente',
                    className: 'br-button primary mr-3',
                }
            },
        })
    });


    $('#alterar').click(function (event) {
        let valorExtingir = $('input[name="tipo_alteracao[extingir]"]').val();
        let valorItens = $('input[name="tipo_alteracao[itens]"]').val();

        if (valorExtingir === '1' && exibirAlertaExtingir) {
            swal({
                title: 'Atenção',
                text: 'Deseja confirmar a extinção da Ordem de Serviço / Fornecimento?',
                type: 'warning',
                buttons: ['Cancelar', 'Extingir'],
                dangerMode: true,
            }).then((result) => {
                if (result) {
                    exibirAlertaExtingir = false
                    $('#alterar').trigger('click');
                }
            });
        } else if (valorItens === '1' && exibirAlertaSaldoNegativo) {
            alertSaldoNegativo()
                .then((result) => {
                    if (result) {
                        exibirAlertaSaldoNegativo = false;
                        $('#alterar').trigger('click');
                    }
                });
        } else {
            selectResponsaveis(() => {
                submitAjax.call(this, 0);
            });
        }
    });

    $('#rascunho').click(function (event) {
        submitAjax.call(this, 1);
    });


    function submitAjax(rascunho = 1) {
        let form = this.closest('form')
        let action = $(form).prop('action');
        var formData = new FormData(form);
        formData.append('rascunho', rascunho);

        $.ajax({
            url: action, // Substitua pelo caminho correto
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.redirect) {
                    window.location.href = response.redirect
                }
            },
            error: function (xhr, status, error) {
                if (xhr.status === 422) {
                    var errors = xhr.responseJSON.errors;
                    var errorMessage = '';

                    $.each(errors, function (key, value) {
                        errorMessage += value[0] + '<br>';
                    });

                    exibirAlertaNoty('error-custom', errorMessage)
                    $('.swal-overlay').removeClass('swal-overlay--show-modal'); // fecha modal seleção de signtários
                } else if (xhr.status === 403) {
                    exibirAlertaNoty('error-custom', 'Sem permissão para alterar esta Ordem de Serviço / Fornecimento.')
                } else {
                    exibirAlertaNoty('error-custom', 'Erro inesperado ao processar o formulário.')
                }
            }
        });
    }
});

const checklist = [
    {
        'checkbox': $('input[name="tipo_alteracao[vigencia]"]'),
        'field': $('#data_vigencia_fim'),
        'function': disableField,
    },
    {
        'checkbox': $('input[name="tipo_alteracao[extingir]"]'),
        'field': $('#data_extincao'),
        'function': disableField,
    },
    {
        'checkbox': $('input[name="tipo_alteracao[itens]"]'),
        'field': $('.table-responsive'),
        'function': disableItensField
    }
];

checklist.forEach(item => {
    item.function.call(item.checkbox, item.field);
    item.checkbox.change(function () {
        if ($(this).attr('name').includes('extingir')) {
            disableChecklistAndFields(this, checklist.filter((item, key) => key !== 1));
        }
        item.function.call(this, item.field);
    })
})

function disableChecklistAndFields(checkboxExtingir, checklists) {
    checklists.forEach(function (checklist) {
        if ($(checkboxExtingir).val() === '1') {
            $(checklist.checkbox).val('0');
            $(checklist.checkbox).siblings('input').prop('checked', false).prop("disabled", true);
            checklist.function.call(checklist.checkbox, checklist.field);
        } else {
            $(checklist.checkbox).siblings('input').prop("disabled", false);
        }
    })
}

function disableField(field) {
    if ($(this).val() === '0') {
        field.prop("disabled", true);
        field.closest('.form-group').fadeOut(250);
    } else {
        field.prop("disabled", false);
        field.closest('.form-group').fadeIn(250);
    }
}

function disableItensField(field) {
    if ($(this).val() === '0') {
        field.fadeOut(250);
        field.prev('div').fadeOut(250);
    } else {
        field.fadeIn(250);
        field.prev('div').fadeIn(250);
    }
}
