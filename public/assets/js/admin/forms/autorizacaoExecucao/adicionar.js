let autorizacaoExecucao = (contratoId, autorizacaoexecucaoId) => {

    let adicionarItensautorizacaoExecucao = (contratoId) => {


        /**
         * Ação executada ao carregar os detalhes da tabela
         * @param line
         */
        let onDetailTableItem = (line) => {
            autorizacaoExecucaoHelper.input.createDecimal($(line).find('.valor_unitario'));
            autorizacaoExecucaoHelper.input.createDecimal($(line).find('.valor_total'));

            let quantidadeItem = $(line).find('.quantidade_item').val();
            let numeroParcelas = $(line).find('.numero_parcelas').val();
            let quantidadeEntregue = $(line).find('.quantidade_itens_entrega')
            $(quantidadeEntregue).val(parseFloat($(quantidadeEntregue).val()))

            autorizacaoExecucaoHelper.tooltip(
                $(line).find('.quantidade_contratada').siblings().append('<i class="fas fa-info-circle"></i>'),
                'Esse valor é resultado da própria Quantidade Contratada (' + quantidadeItem + ') vezes número de parcelas do Item (' + numeroParcelas + ')'
            )

            if ($(line).prev().find('.tipo_item').val() == 'Serviço') {
                autorizacaoExecucaoHelper.input.createDecimal($(line).find('.quantidade'));
            } else {
                autorizacaoExecucaoHelper.input.createInteger($(line).find('.quantidade'));
            }

            //faz a totalizacão dos dados quando os inputs são alterados
            autorizacaoExecucaoEventInputTotalizar(line);

            //seta novamente o padrão de horas nos campos devidoss
            autorizacaoExecucaoHelper.input.setTimeInAllTimesInElement(line);
        }


        /**
         * Transforma o repeatable em um dataTables
         * @type {*|jQuery}
         */
        dataTable = $.InsertTableToDataTable({
            table: '.data-table',
            initialData: [],
            fixedWidth: '20%',
            fields: [
                'periodo',
                'tipo_item',
                'numero_item_compra',
                'item'
            ],
            columns: [
                {title: "Período", width: "30%"},
                {title: "Tipo Item", width: "20%"},
                {title: "Núm. item Compra", width: "20%"},
                {title: "Item", width: "25%"},
                {title: "", width: "10%"},
            ],
            onDetailTableItem: onDetailTableItem,
            onCreateRow: function (row) {
                let progress = autorizacaoExecucaoCreateProgressbar();
                row.push(progress)
                return row;
            },
            onAddedRow: function (row) {
                tooltipList = [];
                for (const brTooltip of $(row).find('.br-tooltip:not(.utilities)')) {
                    tooltipList.push(new core.BRTooltip('br-tooltip', brTooltip))
                }
            },
            onAddTableItem: function (row) {
                autorizacaoExecucaoCounterBarraSaldo()
            },
            onPreRemove: function (element) {
                if ($(element).find('.quantidade_itens_entrega').val() > 0) {
                    swal({
                        title: 'Atenção',
                        text: 'Não é possível excluir itens que já tenham entregas',
                        type: 'warning',
                        buttons: {
                            confirm: {
                                text: 'Ciente',
                                className: 'br-button primary mr-3',
                            }
                        },
                    })
                    return false;
                }
                return true;
            },
            onPosRemove: function () {
                autorizacaoExecucaoCounterBarraSaldo()
            }
        });

        autorizacaoExecucaoSetSaldoUtilizado(contratoId);

        autorizacaoExecucaoSetSaldoUsado().then(function () {
            /**
             * Carrega os reapetables que já vem padrão na página
             */
            dataTable.loadAddedRepeaters((line, repeatable, start, finish, counter) => {
                let itemId = repeatable.find('.contratoitem_id').val();
                let contratohistoricoId = repeatable.find('.contratohistorico_id').val();
                let saldohistoricoitensId = repeatable.find('.saldohistoricoitens_id').val();
                let periodo = repeatable.find('.periodo').val();

                if (start) {
                    autorizacaoExecucaoHelper.startLoading();
                }
                //Carrega os dados do contrato
                contratoApi.saldoHistoricoItem(
                    contratoId,
                    contratohistoricoId,
                    saldohistoricoitensId,
                    (data) => {
                        //Adiciona os valores de cada campo
                        $(line).find('.periodo').last().val(!!periodo ? periodo : data.periodo);
                        $(line).find('.numero_item_compra').last().val(data.numero_item_compra);
                        $(line).find('.tipo_item').last().val(data.tipo);
                        $(line).find('.item').last().val(data.catmatser_item);

                        autorizacaoExecucaoHelper.text.addTolltip($(line).find('.item'), data.descricao)
                        console.log($(repeatable).find('.quantidade_itens_entrega').last().val());

                        $(repeatable).find('.quantidade_item').last().val(data.quantidade_item);
                        $(repeatable).find('.numero_parcelas').last().val(data.numero_parcelas);
                        $(repeatable).find('.quantidade_contratada').last().val(data.quantidade_item * data.numero_parcelas);
                        $(repeatable).find('.valor_unitario_contratado').last().val(autorizacaoExecucaoHelper.formatPtBr(parseFloat(data.valor_unitario)));

                        let unidadeSelected;
                        if (itens[counter] && itens[counter].unidade_medida_id) {
                            unidadeSelected = itens[counter].unidade_medida_id;
                        }

                        autorizacaoExecucaoUnidadesMedidaSet(data.unidades_medida, line, unidadeSelected);

                        if (finish) {
                            autorizacaoExecucaoHelper.stopLoading();
                        }
                    }
                );
            });

            autorizacaoExecucaoCounterBarraSaldo();
        });

        $('#data_vigencia_fim').on('change', function () {
            if ($('div[bp-field-name="vigencia_final_label"]').find('span').text().trim() == 'Indeterminado') {
                // verifica se o ano mudou para evitar requests desnecessários ao servidor
                let lines = $('.data-table tbody tr');

                lines.each(function () {
                    let uid = $(this).data('uid');
                    if (uid) {
                        let repeatable = autorizacaoExecucaoHelper.getRepeatableByLine($(this));
                        contratoApi.contratoitem(contratoId, $(repeatable).find('.saldohistoricoitens_id').val(), (data) => {
                            $(repeatable).find('.quantidade_item').val(data.quantidade_item);
                            $(repeatable).find('.numero_parcelas').val(data.numero_parcelas);
                            $(repeatable).find('.quantidade_contratada').val(data.quantidade_item * data.numero_parcelas);

                            // atualiza a quantidade contratada e o tooltip do detalhe caso esteja presente
                            let quantidadeContratada = $(this).next('tr.no-padding').find('.quantidade_contratada')
                            if (quantidadeContratada.length) {
                                $(quantidadeContratada).val(data.quantidade_item * data.numero_parcelas);
                                autorizacaoExecucaoHelper.tooltip(
                                    $(quantidadeContratada).siblings(),
                                    'Esse valor é resultado da própria Quantidade Contratada (' + data.quantidade_item + ') vezes número de parcelas do Item (' + data.numero_parcelas + ')'
                                )
                            }

                            autorizacaoExecucaoCounterBarraSaldo();
                        })
                    }

                })
            }

        });

        autorizacaoExecucaoBtnModalAddItem(contratoId);

        return dataTable;
    }

    /**
     * Retorna se um item foi usado mais de 100%
     * @returns {boolean}
     */
    let maisCemPorCentoUsadosItensCheck = () => {
        let maisCemPorCentoUsado = false;
        $('.progress-saldo progress').each(function () {
            let val = parseFloat($(this).attr('value'));
            if (val > 100) {
                maisCemPorCentoUsado = true;
            }
        })
        alertSubmitItensNegativo = maisCemPorCentoUsado;
        return maisCemPorCentoUsado;
    }

    /**
     * Executa a ação para abrir o modal
     */
    let autorizacaoExecucaoBtnModalAddItem = (contratoId) => {
        $('#incluir-item-btn').click(async function () {
            //Recupera o item de contrato selecionado para adicionar a lista
            let contratoItemInput = $('#contratoitem_id');
            let contratoItemId = contratoItemInput.val();

            autorizacaoExecucaoHelper.startLoading();

            if (contratoItemId[0] === '') {
                contratoItemId.shift();
            }

            for (let itemContrato of contratoItemId) {
                autorizacaoExecucaoAddItem(contratoId, itemContrato, contratoItemId.length);
            }

            if (contratoItemId.length === 0) {
                autorizacaoExecucaoHelper.stopLoading();
            }

            $("#contratoitem_id option").prop('selected', false);

            $('#scrimfechar').trigger('click');
        });
    }

    let autorizacaoExecucaoAddItem = (contratoId, itemId, qtdItensSelecionados) => {
        contratoApi.saldoHistoricoItemPorPeriodo(contratoId, itemId, function (data) {
            if (autorizacaoExecucaoLoaderCounter.total === 0) {
                const numeroPeriodos = data.length;
                autorizacaoExecucaoLoaderCounter.total = qtdItensSelecionados * numeroPeriodos
            }
            for (let item of data) {
                let lineAdded = dataTable.addItem();
                let repeatable = autorizacaoExecucaoHelper.getRepeatableByLine(lineAdded);

                $(repeatable).find('.contratohistorico_id').val(item.contratohistorico_id)
                $(repeatable).find('.saldohistoricoitens_id').val(item.saldohistoricoitens_id);
                $(repeatable).find('.contratoitem_id').val(item.contratoitem_id);
                $(repeatable).find('.periodo_vigencia_inicio').val(item.periodo_vigencia_inicio);
                $(repeatable).find('.periodo_vigencia_fim').val(item.periodo_vigencia_fim);

                autorizacaoExecucaoPreencheDataByApi(lineAdded, item);
                autorizacaoExecucaoHelper.text.addTolltip($(lineAdded).find('.item'), item.descricao);
                autorizacaoExecucaoLoaderCounter.increase();
            }
        })
    }

    let autorizacaoExecucaoPreencheDataByApi = function (lineAdded, data) {
        let repeatable = autorizacaoExecucaoHelper.getRepeatableByLine(lineAdded);

        //Seta os valores nos campos da linha adicionada
        $(lineAdded).find('.item').val(data.catmatser_item)
        $(lineAdded).find('.numero_item_compra').val(data.numero_item_compra);
        $(lineAdded).find('.tipo_item').val(data.tipo);
        $(lineAdded).find('.periodo').val(data.periodo);


        let valorUnitario = autorizacaoExecucaoHelper.formatPtBr(parseFloat(data.valor_unitario), 2, 17)
        $(repeatable).find('.valor_unitario').val(valorUnitario);
        $(repeatable).find('.valor_total').val(0);
        $(repeatable).find('.quantidade_item').last().val(data.quantidade_item);
        $(repeatable).find('.numero_parcelas').last().val(data.numero_parcelas);
        $(repeatable).find('.quantidade_contratada').last().val(data.quantidade_item * data.numero_parcelas);
        $(repeatable).find('.valor_unitario_contratado').last().val(valorUnitario);
        $(repeatable).find('.quantidade_itens_entrega').last().val(0);

        autorizacaoExecucaoUnidadesMedidaSet(data.unidades_medida, lineAdded);
    }

    /**
     * Seta as unidades de medidas carregadas no item
     * @param data
     * @param line
     */
    let autorizacaoExecucaoUnidadesMedidaSet = (data, line, defaultSelected) => {
        let repetable = autorizacaoExecucaoHelper.getRepeatableByLine(line);
        let unidadesMedidas = data;
        let unidadesMedidaElementor = $(repetable).find('.unidade_medida_select');
        let option;

        //cria o select apartir desses dados
        autorizacaoExecucaoHelper.select.addArrayToSelectOption(
            unidadesMedidas, 'id', 'nome', unidadesMedidaElementor
        )

        //salva na própria linha nos dados ocultos para que não seja preciso carregar  por ajax todas as vezes
        $(line).data('unidades-medida', unidadesMedidas);
        if (defaultSelected) {
            unidadesMedidaElementor.val(defaultSelected);
        }

        autorizacaoExecucaoHelper.select.selectFistIfHasOneItem(unidadesMedidaElementor);
    }


    /**
     * Faz a totalização dos dados
     * @param line
     */
    let autorizacaoExecucaoEventInputTotalizar = (line) => {
        const repeatable = autorizacaoExecucaoHelper.getRepeatableByLine($(line).prev());
        let totalizador = {
            quantidade: 0,
            parcela: 0,
            valorUnitario: 0,
            total: 0,
            atualizarCampos: function(campoAlterado) {

                let quantidadeTotal = this.quantidade * this.parcela;

                if (Number.isNaN(quantidadeTotal)) {
                    quantidadeTotal = 0;
                }

                const tipoItem = $(line).prev().find('.tipo_item').val()

                if (campoAlterado === 'total' && quantidadeTotal > 0) {
                    this.valorUnitario = this.total / quantidadeTotal;
                    $(line).find('.valor_unitario').val($.floatBrRound(this.valorUnitario))
                    $(repeatable).find('.valor_unitario').val($.floatBrRound(this.valorUnitario))
                } else if (campoAlterado === 'total' && tipoItem === 'Serviço' && this.parcela > 0 && this.valorUnitario > 0) {
                    this.quantidade = ( this.total / this.valorUnitario ) / this.parcela
                    quantidadeTotal = this.quantidade * this.parcela;
                    $(line).find('.quantidade').val($.floatBrRound(this.quantidade, 4, 4))
                    $(repeatable).find('.quantidade').val($.floatBrRound(this.quantidade, 4, 4))
                } else {
                    this.total = quantidadeTotal * this.valorUnitario;

                    $(line).find('.valor_total').val($.floatBrRound(this.total))
                }

                quantidadeTotal = $.floatRound(quantidadeTotal, 4);
                $(line).find('.quantidade_total').val(quantidadeTotal)
            }
        }

        const quantidadeAtual = $(line).find('.quantidade').val();
        const parcelaAtual = $(line).find('.parcela').val();
        const valorUnitarioAtual = $(line).find('.valor_unitario').val();

        totalizador.quantidade = $.ptBRToFloat(quantidadeAtual);
        totalizador.parcela = $.ptBRToFloat(parcelaAtual);
        totalizador.valorUnitario = $.ptBRToFloat(valorUnitarioAtual);
        totalizador.atualizarCampos('valorUnitario');

        $(line).find('.quantidade,.parcela,.valor_unitario,.valor_total').change(function () {
            totalizador[$(this).data('item')] = $.ptBRToFloat($(this).val());
            totalizador.atualizarCampos($(this).data('item'));
            if ($(this).data('item') === 'quantidade' || $(this).data('item') === 'parcela') {
                autorizacaoExecucaoCounterBarraSaldo();
            }
        })
    };

    let autorizacaoExecucaoLoaderCounter = {
        total: 0,
        current: 0,
        increase: () => {
            autorizacaoExecucaoLoaderCounter.current++;
            if (autorizacaoExecucaoLoaderCounter.current === autorizacaoExecucaoLoaderCounter.total) {
                autorizacaoExecucaoHelper.stopLoading();
                autorizacaoExecucaoLoaderCounter.total = 0;
                autorizacaoExecucaoLoaderCounter.current = 0;
            }
        }
    }

    let autorizacaoExecucaoCreateProgressbar = () => {
        let div = $('<div/>');
        let span = $('<span class="pct"/>');
        let progress = $("<progress/>");

        let tooltip = $('.tooltip').html();

        span.text('0%');
        progress.val(0);
        progress.attr('max', 100);

        div.append(span);
        div.append(progress);
        div.append(tooltip);
        div.css({
            'text-align': 'center'
        })
        div.addClass('progress-saldo')


        return div[0].outerHTML;
    }

    let autorizacaoExecucaoSaldoUsado = {};
    let autorizacaoExecucaoSaldoUtilizado = {};

    let autorizacaoExecucaoSetSaldoUsado = async function () {

        let getSaldoJaUsado = async () => {
            const data = {
                autorizacaoexecucao_id: autorizacaoexecucaoId
            };

            const searchParams = new URLSearchParams(data);


            let response = await fetch('/autorizacaoexecucao/' + contratoId + '/usado?' + searchParams);
            return await response.json();
        }
        let itensSaldoUsado = await getSaldoJaUsado();
        autorizacaoExecucaoSaldoUsado = {};
        for (let i = 0; i < itensSaldoUsado.length; i++) {
            if (!autorizacaoExecucaoSaldoUsado[itensSaldoUsado[i].saldohistoricoitens_id]) {
                autorizacaoExecucaoSaldoUsado[itensSaldoUsado[i].saldohistoricoitens_id] = 0;
            }
            autorizacaoExecucaoSaldoUsado[itensSaldoUsado[i].saldohistoricoitens_id] += parseFloat(itensSaldoUsado[i].quantidade_total);
        }
        return autorizacaoExecucaoSaldoUsado;
    }

    let autorizacaoExecucaoSetSaldoUtilizado = async function (contrato) {
        let getSaldoJaUtilizado = async (id) => {
            let response = await fetch('/autorizacaoexecucao/' + contratoId + '/utilizado');
            return await response.json();
        }

        let itensSaldoUtilizado = await getSaldoJaUtilizado(contrato);

        autorizacaoExecucaoSaldoUtilizado = {};
        for (let i = 0; i < itensSaldoUtilizado.length; i++) {
            autorizacaoExecucaoSaldoUtilizado[itensSaldoUtilizado[i].contratoitens_id] = $.ptBRToFloat(itensSaldoUtilizado[i].quantidade);
        }
        return autorizacaoExecucaoSaldoUtilizado;
    }

    let cienciaSaldo = false;

    /**
     * FAz o calculo de todas as barras de saldo
     */
    let autorizacaoExecucaoCounterBarraSaldo = () => {

        let saldos = {};
        let lines = $('.data-table tbody tr');

        let countSaldo = () => {
            lines.each(function () {
                let uid = $(this).data('uid');
                if (uid) {
                    let repeatable = autorizacaoExecucaoHelper.getRepeatableByLine($(this));
                    let id = $(repeatable)
                        .find('.saldohistoricoitens_id')
                        .val();
                    let contratoitem_id = $(repeatable).find('.contratoitem_id').val();

                    let quantidadeContratada = $(repeatable).find('.quantidade_contratada').val();
                    let quantidade = $(repeatable).find('.quantidade').val();
                    let parcela = $(repeatable).find('.parcela').val();
                    if (!saldos[id]) {
                        saldos[id] = {
                            total: $.ptBRToFloat(quantidadeContratada),
                            usado: 0,
                            solicitada: 0,
                        }
                    }
                    saldos[id].solicitada += ($.ptBRToFloat(quantidade) * parcela);
                    if (autorizacaoExecucaoSaldoUsado[id] || autorizacaoExecucaoSaldoUtilizado[contratoitem_id]) {
                        const saldoUsado = autorizacaoExecucaoSaldoUsado[id] ?? 0;
                        const saldoUtilizado = autorizacaoExecucaoSaldoUtilizado[contratoitem_id] ?? 0;
                        saldos[id].usado = saldoUsado  + saldoUtilizado;
                    }
                }
            })
            lines.each(function () {
                let uid = $(this).data('uid');
                if (uid) {
                    let repeatable = autorizacaoExecucaoHelper.getRepeatableByLine($(this));
                    let id = $(repeatable)
                        .find('.saldohistoricoitens_id')
                        .val();
                    if (!saldos[id]) {
                        saldos[id] = {
                            total: 100,
                            usado: 0,
                            solicitada: 0,
                        }
                    }
                    let pctBase = ((saldos[id].solicitada + saldos[id].usado) * 100) / saldos[id].total;
                    let pct = Math.ceil(pctBase);
                    let progress = $(this).find("progress");
                    let progressSaldo = $(this).find(".progress-saldo")
                    let saldoRestante = saldos[id].total - saldos[id].usado - saldos[id].solicitada

                    progressSaldo.find('#tooltip-quantidade-contratada .value').text(saldos[id].total);
                    progressSaldo.find('#tooltip-quantidade-consumida .value').text(saldos[id].usado);
                    progressSaldo.find('#tooltip-quantidade-solicitada .value').text(saldos[id].solicitada);
                    progressSaldo.find('#tooltip-quantidade-restante .value').text(saldoRestante);

                    progressSaldo.removeClass('success')
                        .removeClass('warning')
                        .removeClass('danger');

                    if (isNaN(pct)) {
                        pct = 0;
                    }

                    if (pct <= 74) {
                        progressSaldo.addClass('success')
                    } else if (pct >= 75 && pct < 90) {
                        progressSaldo.addClass('warning')
                    } else {
                        progressSaldo.addClass('danger')
                    }

                    progress.val(pct);
                    progress.attr('max', 100);
                    progressSaldo.find('span.pct').html(pct + '%');
                }
            })
        }

        setTimeout(function () {
            countSaldo();
            autorizacaoExecucaoHelper.orderByNumeroItemCompra();

            if (maisCemPorCentoUsadosItensCheck() && !cienciaSaldo && !isPreposto) {
                swal({
                    title: 'Atenção',
                    text: 'Existem itens usando mais de 100% do valor contratado',
                    type: 'warning',
                    buttons: {
                        confirm: {
                            text: 'Ciente',
                            className: 'br-button primary mr-3',
                        }
                    },
                }).then((result) => {
                    if (result) {
                        cienciaSaldo = true;
                    }
                })
            }
        }, 1500);
    }
//funções auxiliares para  adicionar itens na autirização de execução
    let autorizacaoExecucaoHelper = {


        /**
         *  Retorna o elemento do repeatable referente a linha da tabela
         * @param line
         * @returns {jQuery|HTMLElement|*}
         */
        getRepeatableByLine: (line) => {
            return $('.repeatable-element[data-uid="' + $(line).data('uid') + '"]')
        },

        /**
         * Retorna o elemento do campo referente ao campo que está no repeatable
         * @param element
         * @returns {jQuery|HTMLElement|*}
         */
        getElementInRepeatable: (element) => {
            return $('[name="' + element.data('name') + '"]')
        },


        /**
         * Convert para moeda padrão BR
         * @param valor
         * @returns {string}
         */
        formatPtBr: (valor, digits = 2, maxDigits = 2) => {
            return valor.toLocaleString("pt-BR", {
                minimumFractionDigits: digits,
                maximumFractionDigits: maxDigits
            });
        },

        select: {

            /**
             * Cria um option para select baseado na chaveValor
             * @param value
             * @param label
             * @returns {jQuery|HTMLElement|*}
             */
            createOption: (value, label) => {
                let option = $('<option/>');
                option.attr('value', value);
                option.text(label);
                return option;
            },
            /**
             * Adiciona options em um select através de chave=>value
             * @param value
             * @param label
             * @param select
             */
            addSelectOption: function (value, label, select) {
                let optionItem = this.createOption(value, label);
                $(select).append(optionItem);
            },
            /**
             * Item a item de um array insere em um option conforme o keylabel e keyvalue passado
             * @param itens
             * @param keyValue
             * @param keyLabel
             * @param select
             */
            addArrayToSelectOption: function (itens, keyValue, keyLabel, select) {
                for (let i = 0; i < itens.length; i++) {
                    this.addSelectOption(
                        itens[i][keyValue],
                        itens[i][keyLabel],
                        select
                    );
                }
            },

            /**
             * Seleciona o primeiro item do select caso exista apenas um item com valor
             * @param select
             */
            selectFistIfHasOneItem: function (select) {
                let values = this.selectValuesToArray(select, false);
                if (values.length === 1) {
                    select.val(values[0].key);
                }
            },


            /**
             * Retorna os valores de um select em um array
             * @param select
             * @param includeEmpty caso queira incluir valores vazios
             * @returns {*[]}
             */
            selectValuesToArray: (select, includeEmpty = true) => {
                let valuesOfSelect = [];
                $(select).find('option').each(function () {
                    let val = $(this).val();
                    let label = $(this).text();

                    if (includeEmpty || val !== '') {
                        valuesOfSelect.push({
                            key: val,
                            label: label
                        });
                    }
                })
                return valuesOfSelect;
            }
        },

        /**
         * Cria um tooltip
         * @param field
         * @param text
         * @param place
         */
        tooltip: (field, text, place = 'top') => {
            const config = {
                activator: $(field)[0],
                place: 'top',
                textTooltip: text,
            }

            new core.Tooltip(config)
        },
        input: {
            /**
             * Cria mascara de moeda no padrão PT/BR
             * @param input
             */
            createDecimal: function (input) {
                $(input).decimal();
            },
            createInteger: function (input) {
                $(input).integer();
            },
            /**
             * Seta o estilo de time em fotos os flatpickr com a mascara de tempo
             * @param element
             */
            setTimeInAllTimesInElement: (element) => {
                $(element).find('.flatpickr-input').each(function () {
                    const field = $(this);
                    let option = {};
                    if (field.data('mask') == '00:00') {
                        option.enableTime = true;
                        option.noCalendar = true;
                        option.dateFormat = 'H:i';
                    }
                    let fp = flatpickr(this, option);

                    // força a atualização do campo input dentro dos detalhes da table
                    fp.config.onClose.push(function (selectedDates, dateStr, instance) {
                        field.keydown();
                    })

                    let button = $(this).parent().find('button');
                    if (button) {
                        button.click(function () {
                            fp.open();
                        })
                    }
                })
            }
        },
        text: {
            addTolltip: (field, textTolltip) => {
                const parent = $(field).parent()
                    .addClass('d-flex align-items-center')
                    .append('<i class="fas fa-info-circle"></i>')
                autorizacaoExecucaoHelper.tooltip($(parent).find('.fas'), textTolltip);
            }
        },
        orderByNumeroItemCompra: () => {
            let tabela = $('#DataTables_Table_0');
            let linhas = tabela.find('tr:gt(0)').toArray().sort((a, b) => {
                let inputA = $(a).find(".numero_item_compra");
                let inputB = $(b).find(".numero_item_compra")
                let numeroA = parseInt($(inputA).val());
                let numeroB = parseInt($(inputB).val());

                return numeroA - numeroB;
            });

            for (var i = 0; i < linhas.length; i++) {
                tabela.append(linhas[i]);
            }
        },
        startLoading: () => {
            $.blockUI({message: $("#loadingContratos")});
        },
        stopLoading: () => {
            $.unblockUI();
        },
    }

    let contratoApi = {
        saldoHistoricoItemPorPeriodo: (contratoId, itemId, callback) => {
            let vigenciaInicio = null;
            vigenciaInicio = $('#data_vigencia_inicio_label').find('span').text().trim();
            if (!vigenciaInicio) {
                vigenciaInicio = $('#data_vigencia_inicio').val()
            }
            vigenciaInicio = vigenciaInicio.split('/').reverse().join('-')

            let vigenciaFim = null;
            vigenciaFim = $('#data_vigencia_fim').val();
            if (!vigenciaFim) {
                vigenciaFim = $('#data_vigencia_fim_label').find('span').text().trim()
            }
            vigenciaFim = vigenciaFim.split('/').reverse().join('-')

            $.get(`/autorizacaoexecucao/${contratoId}/saldohistoricoitem-periodo?id=${itemId}&vigencia_inicio=${vigenciaInicio}&vigencia_fim=${vigenciaFim}`, function (data) {
                callback(data);
            });
        },
        saldoHistoricoItem: (contratoId, contratohistoricoId, saldohistoricoitensId, callback) => {
            $.get(`/autorizacaoexecucao/${contratoId}/saldohistoricoitem?contratohistorico_id=${contratohistoricoId}&saldohistoricoitens_id=${saldohistoricoitensId}`, function (data) {
                callback(data);
            })
        }
    }
    return adicionarItensautorizacaoExecucao(contratoId);
}
