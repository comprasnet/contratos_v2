/**
 * Método responsável para exibir a modal em tela
 */
function exibirModalUsuario(tituloModal) {
    // Configurações para tamanho e exibir a modal
    $(".br-modal").css("max-width", "100%");
    $("#modalInformacoesDados").addClass("active");

    // Incluir o título da modal conforme o botão clicado
    $("#modalInformacoesDados .br-modal-header").html(tituloModal)

    // Limpar o HTML e insere a tabela padrão
    $("#modalInformacoesDados .br-modal-body").empty()
}


/**
 * Método responsável em exibir a modal com as informações para o usuário
 *
 */
function montarDadosModal(cabecalhoTabela, linhaTabela, idTableModal, idTableTheadModal, idTableTbodyModa) {
    // Limpar o HTML e inserir a tabela padrão
    $(`#${idTableModal} #${idTableTheadModal}`).empty()

    // Insere o cabeçalho conforme o botão clicado pelo usuário
    $(`#${idTableModal} #${idTableTheadModal}`).append(cabecalhoTabela);

    // Limpar o HTML e insere a tabela padrão
    $(`#${idTableModal} #${idTableTbodyModa}`).empty()

    // Insere a linha conforme o botão clicado pelo usuário
    $(`#${idTableModal} #${idTableTbodyModa}`).append(linhaTabela);
}

/**
 * Montar o HTML da tabela para exibir na modal
 */
function montarTableHtmlModal(idTableModal, theadTableModal, tbodyTableModal, tituloTable) {
    let tableInformacao =
        `
    <div class="br-table" data-search="data-search" data-selection="data-selection" data-collapse="data-collapse" data-random="data-random">
    <div class="table-header">
    <div class="top-bar">
      <div class="table-title">${tituloTable}</div>
      </div>
      </div>
    <table id="${idTableModal}" class="table">
        <thead id="${theadTableModal}"></thead>
        <tbody id="${tbodyTableModal}"></tbody>
     </table>
     </div>`

    $("#modalInformacoesDados .br-modal-body").append(tableInformacao)
}

/**
 * Método responsável em exibir as informações gerais sobre o item do remanejamento
 */
function exibirInformacaoGeralItem(campo) {
    // Informações da compra
    let numeroCompra = campo.dataset.numeroCompra
    let modalidade = campo.dataset.modalidade
    let unidadeGerenciadoraCompra = campo.dataset.unidadeGerenciadoraCompra

    let idTableModalCompra = 'table_compra_modal'
    let idTableTheadModalCompra = 'thead_table_compra_modal'
    let idTableTbodyModalCompra = 'tbody_table_compra_modal'

    // Informações da ata
    let numeroAta = campo.dataset.numeroAta
    let vigenciaAta = campo.dataset.vigenciaAta
    let unidadeOrigemAta = campo.dataset.unidadeOrigemAta
    let valorTotal = campo.dataset.valorTotal

    let idTableModalAta = 'table_ata_modal'
    let idTableTheadModalAta = 'thead_table_ata_modal'
    let idTableTbodyModalAta = 'tbody_table_ata_modal'

    // Informações do usuário solicitante
    let nomeUsuario = campo.dataset.nomeUsuario
    let email = campo.dataset.email
    let cpfEscondido = campo.dataset.cpfEscondido

    // Informações sobre o número do item e unidade de origem do item
    let numeroItem = campo.dataset.numeroItem
    let unidadeOrigemItem = campo.dataset.unidadeOrigemItem


    let idTableModalUsuarioSolicitante = 'table_usuario_solicitante_modal'
    let idTableTheadModalUsuarioSolicitante = 'thead_table_usuario_solicitante_modal'
    let idTableTbodyModalUsuarioSolicitante = 'tbody_table_usuario_solicitante_modal'


    let tituloModal =
        `Informações gerais sobre o item ${numeroItem} e da unidade de origem ${unidadeOrigemItem}`

    // Exibe a modal para o usuário
    exibirModalUsuario(tituloModal)

    // Insere a tabela na modal
    montarTableHtmlModal(
        idTableModalCompra,
        idTableTheadModalCompra,
        idTableTbodyModalCompra,
        'Informações sobre a compra'
    )
    // Insere a linha na tabela referente as informações da compra
    exibirDadosCompra(
        numeroCompra,
        modalidade,
        unidadeGerenciadoraCompra,
        idTableModalCompra,
        idTableTheadModalCompra,
        idTableTbodyModalCompra
    )

    // Insere a tabela na modal
    montarTableHtmlModal(idTableModalAta, idTableTheadModalAta, idTableTbodyModalAta, 'Informações sobre a ata')
    // Insere a linha na tabela referente as informações da ata
    exibirDadosAta(
        numeroAta,
        vigenciaAta,
        unidadeOrigemAta,
        valorTotal,
        idTableModalAta,
        idTableTheadModalAta,
        idTableTbodyModalAta
    )

    // Insere a tabela na modal
    montarTableHtmlModal(
        idTableModalUsuarioSolicitante,
        idTableTheadModalUsuarioSolicitante,
        idTableTbodyModalUsuarioSolicitante,
        'Informações sobre o usuário solicitante'
    )

    // Insere a linha na tabela referente as informações do usuário solicitante
    exibirDadosUsuarioSolicitante(
        nomeUsuario,
        email,
        cpfEscondido,
        idTableModalUsuarioSolicitante,
        idTableTheadModalUsuarioSolicitante,
        idTableTbodyModalUsuarioSolicitante
    )
}

/**
 * Método responsável em exibir as informações da compra para o usuário
 *
 */
function exibirDadosCompra(numeroCompra, modalidade, unidadeGerenciadoraCompra, idTable, thead, tbody) {
    let cabecalho = `<th>Número da compra</th>
                    <th>Modalidade</th>
                    <th>Unidade da compra</th>`
    let linha = `<tr>
                    <td>${numeroCompra}</td>
                    <td>${modalidade}</td>
                    <td>${unidadeGerenciadoraCompra}</td>
                </tr>`

    montarDadosModal(cabecalho, linha, idTable, thead, tbody)
}

/**
 * Método responsável em exibir as informações da ata para o usuário
 */
function exibirDadosAta(numeroAta, vigenciaAta, unidadeOrigemAta, valorTotal, idTable, thead, tbody) {
    let cabecalho = `<th>Número da ata</th>
                    <th>Vigência</th>
                    <th>Unidade de origem da ata</th>
                    <th>Valor total da ata</th>`

    let linha = `<tr>
                    <td>${numeroAta}</td>
                    <td>${vigenciaAta}</td>
                    <td>${unidadeOrigemAta}</td>
                    <td>${valorTotal}</td>
                </tr>`

    montarDadosModal(cabecalho, linha, idTable, thead, tbody)
}

/**
 * Método responsável em exibir as informações do usuário solicitante do remanejamento
 */

function exibirDadosUsuarioSolicitante(nomeUsuario, email, cpfEscondido, idTable, thead, tbody) {
    let cabecalho = `<th>Solicitante</th>
                    <th>E-mail</th>
                    <th>CPF</th>`

    let linha = `<tr>
                    <td>${nomeUsuario}</td>
                    <td>${email}</td>
                    <td>${cpfEscondido}</td>
                </tr>`

    montarDadosModal(cabecalho, linha, idTable, thead, tbody)
}

/**
 * Método responsável em alterar o estado do campo justicativa e motivo
 */
function alterarEstadoCampo(idTextArea, desabilitar) {
    if (desabilitar) {
        $(idTextArea).val(''); // Clear the textarea content

        $(idTextArea).prop('required', false);
        $(idTextArea).prop('disabled', true);

        return
    }

    $(idTextArea).prop('disabled', false);
    $(idTextArea).prop('required', true);
}

/**
 * Método responsável em alterar o status do textarea da justificativa e motivo
 * @param {*} campo
 * @returns
 */
function alterarEstadoJustificativa(campo) {
    let dataSet = campo.dataset
    let id = dataSet.idItem
    let status = dataSet.status
    let quantidade = parseInt(dataSet.quantidadeAutorizada)


    let idCampoQuantidadeAprovada = `input[name='quantidade_analisada[${id}]']`

    let idTextArea = `#textarea-${id}`

    let readonly = false
    $(idCampoQuantidadeAprovada).attr('readonly', readonly);
    let idCampoQuantidadeSolicitada = `#quantidade_solicitada_analise_${id}`;
    let quantidadeSolicitada = $(idCampoQuantidadeSolicitada).val();


    if (status == 'Aceitar') {
        $(idCampoQuantidadeAprovada).attr('readonly', true);
        $(idCampoQuantidadeAprovada).val(quantidadeSolicitada).trigger('input');
        return;
    }


    if (status != 'Aceitar') {
        // Altera o estado do campo para habilitado e obrigatório
        alterarEstadoCampo(idTextArea)
        $(idCampoQuantidadeAprovada).prop('required', true);
        // Se o usuário selecionar a situação do item para negar, então setamos o valor zero
        // e não deixa o usuário alterar
        if (status == 'Negar') {
            $(idCampoQuantidadeAprovada).val('0').trigger('input');
            readonly = true
        }

        $(idCampoQuantidadeAprovada).attr('readonly', readonly);

        return
    }
    // Altera o estado do campo para desabilitado e não obrigatório
    alterarEstadoCampo(idTextArea, true)

    $(idCampoQuantidadeAprovada).prop('required', false);
    $(idCampoQuantidadeAprovada).val(null);
}

let arrayRadioChecado = []
let checado = false
let valueChecado = null

/**
 * Método responsável em retirar a marcação do usuário no radio
 * @param {*} campo
 */
function removerMarcacaoAtual(campo) {
    // Se o ID não existir no array, então criamos
    if (arrayRadioChecado[campo.id] == undefined) {
        let dadosCampo = {checked: false}
        arrayRadioChecado[campo.id] = dadosCampo
    }

    let checado = true

    // Se o item estiver marcado, então marcamos como false para poder
    // retirar a marcação e alterar o valor da variável de controle
    if (arrayRadioChecado[campo.id].checked == true) {
        checado = false
    }

    campo.checked = checado
    arrayRadioChecado[campo.id].checked = campo.checked

    // Recupera o id único do campo
    let id = campo.dataset.idItem
    let idTextArea = `#textarea-${id}`
    // Altera o estado do campo para desabilitado e não obrigatório
    alterarEstadoCampo(idTextArea, true)
}

exibirTodosDetail()

/**
 * Método responsável em deixar todos os details abertos
 */
function exibirTodosDetail() {
    $(".detail_row_custom").each(function () {
        // Recuperar a primeira classe da linha do detail row
        // Devido ter o id do ícone
        let classRow = $(this)[0].classList[0].split('_')

        // Retornar somenter a última posição do array
        let last = [...classRow].pop();

        // Remover a classe para deixar aberto para o usuário
        $(this).removeClass('hidden')

        // Alterar o ícone como se o detail estivesse recolhido
        let idIcon = `#icon_detail_${last}`
        $(idIcon).removeClass('fa-chevron-down')
        $(idIcon).addClass('fa-chevron-up')
    })
}