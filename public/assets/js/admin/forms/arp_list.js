$(document).on('click', '.gerar-pdf', function(event) {
    event.preventDefault()

    var arpId = $(this).data('arp-id');
    var $gerarPdfButton = $(this);

    var that = $(this);
    that.data('load', 1);

    $.ajax({
        type: 'POST',
        url: '/arp/disparar-job-pdf/' + arpId,
        data: { arpId: arpId },
        dataType: 'json',
        success: function (response) {
            if (response.success) {
                gerarPDF(response);
            }
        },
        error: function () {
            handleErroRequisicaoPDF();
        }
    });

    function gerarPDF(response) {
        var alertTimer;

        if ($gerarPdfButton.hasClass('pdf-gerado')) {
            window.location.href = $gerarPdfButton.data('pdf-url');
        } else if (!$gerarPdfButton.hasClass('pdf-em-processo-de-geracao')) {
            $gerarPdfButton.addClass('pdf-em-processo-de-geracao').prop('disabled', true);
            exibirAlertaNoty('info-custom', 'O PDF está sendo gerado e ficará disponível quando for concluído.<br>' +
                'O PDF é gerado apenas uma vez por dia, eventuais atualizações realizadas após o ' +
                'horário de geração do PDF estarão disponíveis no próximo dia.');

            $gerarPdfButton.find('i').removeClass('fa-file-pdf').addClass('fa-spinner fa-spin');

            alertTimer = setTimeout(function () {
                exibirAlertaNoty('warning-custom', 'A geração do PDF está demorando mais que o ' +
                    'esperado. Por favor, volte mais tarde.');
            }, 15000);

            if (response.success) {
                var intervalId = setInterval(function () {
                    $.ajax({
                        type: 'GET',
                        url: '/arp/check-status-pdf/' + arpId,
                        dataType: 'json',
                        success: function (statusResponse) {
                            if (statusResponse.gerado) {
                                clearInterval(intervalId);
                                clearTimeout(alertTimer);
                                $gerarPdfButton.removeClass('pdf-em-processo-de-geracao pdf-spinner').addClass('pdf-gerado');
                                exibirAlertaNoty('success-custom', 'PDF gerado com sucesso!');
                                $gerarPdfButton.find('i').removeClass('fa-spinner fa-spin').addClass('fas fa-download');

                                //  botão de visualização
                                var $visualizarPdfButton = $('<a class="btn btn-sm btn-link" href="/arp/visualizar-pdf/' + arpId + '/visualizar" title="Visualizar PDF" target="_blank"><i class="fa fa-book-open"></i></a>');
                                $gerarPdfButton.after($visualizarPdfButton);

                                // Atualizar URL do botão de download
                                $gerarPdfButton.data('pdf-url', '/arp/visualizar-pdf/' + arpId + '/download');

                                // Adicionar evento de clique ao botão de download
                                $gerarPdfButton.on('click', function(event) {
                                    event.preventDefault(); // Impedir o comportamento padrão do link
                                    window.location.href = $(this).data('pdf-url');
                                });

                                // Adicionar evento de clique ao ícone de download
                                $visualizarPdfButton.on('click', function(event) {
                                    event.preventDefault(); // Impedir o comportamento padrão do link
                                    window.location.href = $(this).attr('href');
                                });


                                // Iniciar o download automaticamente
                                window.location.href = response.pdfUrl;

                                that.data('load', 0);
                            } else if (statusResponse.erro) {
                                clearInterval(intervalId);
                                $gerarPdfButton.removeClass('pdf-em-processo-de-geracao pdf-spinner').prop('disabled', false);
                                exibirAlertaNoty('error-custom', 'Ocorreu um erro ao gerar o PDF.');
                                $gerarPdfButton.find('i').removeClass('fa-spinner fa-spin').addClass('fa-file-pdf');
                            }
                        },
                        error: function () {
                            that.data('load', 0);
                            alert('Ocorreu um erro ao iniciar a geração do PDF.');
                        }
                    });
                }, 5000);
            } else {
                $gerarPdfButton.removeClass('pdf-em-processo-de-geracao pdf-spinner').prop('disabled', false);
                that.data('load', 0);
                clearTimeout(alertTimer);
                alert('Ocorreu um erro ao iniciar a geração do PDF.');
            }
        }
    }

    function handleErroRequisicaoPDF() {
        $gerarPdfButton.removeClass('pdf-em-processo-de-geracao pdf-spinner').prop('disabled', false);
        alert('Ocorreu um erro ao iniciar a geração do PDF.');
    }

});
