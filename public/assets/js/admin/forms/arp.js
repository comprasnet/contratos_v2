let datableModalItem = null
var selected = []
var selectedData = []
let datableFornecedor = null
let currentLocation = window.location;

// Se a tela for edição, exibe os botões ao final da tela
if (currentLocation.pathname.includes("edit")) {
    exibirEsconderBotao(true)
} else {
    exibirEsconderBotao()
}

// Método responsávem em exibir ou esconder os botões na tela para o usuário
function exibirEsconderBotao(exibir = false) {


    if (exibir) {
        $("#rascunho").show()
        $("#adicionar").show()
        return
    }
    $("#rascunho").hide()
    $("#adicionar").hide()
}

// Função responsável em adicionar ou remover as classes para as ações do botões
function mudarBotaoAcaoRealizada(idCampo, sucesso, classRemove, classAdd) {

    if(sucesso) {
        $( idCampo ).removeClass( classRemove ).addClass( classAdd )
        return
    }

    $( idCampo ).removeClass( classRemove ).addClass( classAdd )
}

// Função responsável em exibir a parte dos campos da ata e informações da compra
function exibirAreaAtaCompra(esconder = false) {
    // Se for para esconder, é que o botão de verificar ata foi clicado
    if(esconder) {
        $(".areaDadosCompra").show()
        $(".areaDadosAta").show()
        mudarBotaoAcaoRealizada("#btn_validar_arp",true,"btn-info","btn-success")
        mudarBotaoAcaoRealizada("#icone_btn_validar_arp",true,"fas fa-plus","fas fa-check")

        return ;
    }

    $(".areaDadosCompra").hide()
    $(".areaDadosAta").hide()
    $(".areaItemSelecionado").hide()
    mudarBotaoAcaoRealizada("#btn_validar_arp",true,"btn-success","btn-info")
    mudarBotaoAcaoRealizada("#icone_btn_validar_arp",true,"fas fa-check","fas fa-plus")
}

exibirAreaAtaCompra()


$('#btn_validar_arp').click(function(e){
    $(".areaItemSelecionado").hide()

    e.preventDefault()

    let campoAno = $("input[name=ano]")

    let min = campoAno.attr('min')
    let max = campoAno.attr('max')
    let anoAta = campoAno.val()

    let mensagemAlertaCampoEmBranco = null

    // Se o ano for menor que o digitado
    if(parseInt(anoAta) < parseInt(min)) {
        mensagemAlertaCampoEmBranco = `O ano digitado está menor que o mínimo de ${min}`

        exibirAlertaNoty('warning-custom',mensagemAlertaCampoEmBranco)
        mudarBotaoAcaoRealizada("#btn_validar_arp",false,"btn-success","btn-info")
        mudarBotaoAcaoRealizada("#icone_btn_validar_arp",false,"fas fa-check","fas fa-search")

        return
    }

    // Se o ano for maior que o digitado
    if(parseInt(anoAta) > parseInt(max)) {
        mensagemAlertaCampoEmBranco = `O ano digitado está maior que o máximo de ${max}`

        exibirAlertaNoty('warning-custom',mensagemAlertaCampoEmBranco)
        mudarBotaoAcaoRealizada("#btn_validar_arp",false,"btn-success", "btn-info")
        mudarBotaoAcaoRealizada("#icone_btn_validar_arp",false,"fas fa-check","fas fa-search")

        return
    }

    // Chama a função para adicionar o zero à esquerda no campo do número da ata
    let numeroAta = adicionarZeroEsquerda($("input[name=numero]").val());

    // Chama a função para validar o número da ata
    let resultadoValidacao = validarNumeroAta(numeroAta);

    // Verifica o resultado da validação
    if (resultadoValidacao !== true) {
        exibirAlertaNoty('warning-custom', resultadoValidacao);
        return;
    }


    let unidadeGerenciadora = $("#unidade_origem_id").val()

    // Se todos os campos estiverem preenchidos, envia para o ajax
    if (!campoEmBranco(anoAta) && !campoEmBranco(numeroAta)) {
        let urlValidarNumeroAta = $("#url_buscar_arp").val()
        $.post(
            urlValidarNumeroAta,
            {numeroAta:numeroAta , anoAta: anoAta, unidadeGerenciadora: unidadeGerenciadora}
        ).done(function( response ) {
            // Se a resposta for sucesso, exibe a seleção do fornecedor
            if(response) {
                exibirAreaAtaCompra(true)

                return ;
            }

            mensagemAlertaCampoEmBranco = 'Existe ata com o mesmo número e ano registrada para essa unidade. Para prosseguir, corrija as informações digitadas'
            exibirAlertaNoty('warning-custom',mensagemAlertaCampoEmBranco)
            exibirAreaAtaCompra(false)
        })
        return
    }

    if (campoEmBranco(anoAta) ) {
        mensagemAlertaCampoEmBranco = 'O Campo do Ano da Ata está em branco'
    }

    if (campoEmBranco(numeroAta) ) {
        mensagemAlertaCampoEmBranco = 'O Campo do Número da Ata está em branco'
    }

    exibirAlertaNoty('warning-custom',mensagemAlertaCampoEmBranco)
    exibirAreaAtaCompra(false)

    mudarBotaoAcaoRealizada("#btn_validar_arp",true,"btn-info","btn-success")
    mudarBotaoAcaoRealizada("#icone_btn_validar_arp",true,"fas fa-search","fas fa-check")
});

// Função para bloquear o envio da requisição para buscar a compra
function bloquearEnvioCamposCompra() {
    let unidade_origem_id = $("input[name=unidade_origem_id]").val();
    let numero_ano = $("input[name=numero_ano]").val();
    let retorno = []

    let unidadeOrigemCompra = $("#unidade_origem_compra_id").val()
    let modalidadeId = $('select[name="modalidade_id"]').val()

    if(
        campoEmBranco(unidade_origem_id) ||
        campoEmBranco(numero_ano) ||
        campoEmBranco(unidadeOrigemCompra) ||
        modalidadeId == 0
        ) {

        retorno['type'] = 'error-custom'
        if(campoEmBranco(unidade_origem_id)) {
            retorno['message'] = 'Unidade de Origem está em branco, favor preencher.'
        }

        if(campoEmBranco(numero_ano)) {
            retorno['message'] = 'Número da compra está em branco, favor preencher.'
        }

        if(campoEmBranco(unidadeOrigemCompra)) {
            retorno['message'] = 'Unidade origem da compra está em branco, favor preencher.'
        }

        if(modalidadeId == 0) {
            retorno['message'] = 'Modalidade da compra está em branco, favor preencher.'
            // Retira todas as marcações que o usuário realizou
            // devido faltar um campo obrigatório
            $("#areaItemFornecedor").hide()
            $("#areaItemSelecionado").hide()
            $("#areaFornecedor").hide()
            $("#areaDetalheMaiorDesconto").hide()
        }

        return retorno
    }

    return null
}

function areaCamposValidacaoCompra(sucesso) {
    $("#areaFornecedor").hide()
    if(sucesso) {
        $("#areaFornecedor").show()
    }
    $("#areaItemFornecedor").hide()
    $("#areaItemSelecionado").hide()
    $("#areaDadosAta").hide()
    $("#areaDetalheMaiorDesconto").hide()
    $.unblockUI();
}

// Função para descer a página quando clicar no botão
function descerPagina() {
    $("html, body").animate({
        scrollTop: $(
          'html, body').get(0).scrollHeight
    }, 2000);
}

inserirLinhaFornecedor()

// Função responsável em exibir o item do fornecedor ao clicar no botão
function exibirItemFornecedor(idFornecedor, nomeFornecedor, unidadeOrigem) {
    $("#nomeFornecedor").text(nomeFornecedor)
    carregarTabelaItemFornecedor(idFornecedor,false, unidadeOrigem)

    $("#areaItemFornecedor").show()
    $("#areaDetalheMaiorDesconto").hide()
    descerPagina()
}

// Função responsável em recuperar a coluna da tabela do item selecionado
function recuperarColunaTabelaItemSelecionado(posicaoColuna) {
    return document.body.querySelectorAll(`#table_item_compra_selecionado_arp td:nth-child(${posicaoColuna})`)
}

// Função responsável em incluir o item do fornecedor ao clicar no botão de adicionar item
function inserirLinhaItemSelecionado(item) {
    //recupera a coluna numero e se for igual, nao adiciona a linha
    let colunaNumeroItem = recuperarColunaTabelaItemSelecionado(4)
    // document.body.querySelectorAll("#table_item_compra_selecionado_arp td:nth-child(4)")
    let colunaCnpj = recuperarColunaTabelaItemSelecionado(2)
    document.body.querySelectorAll("#table_item_compra_selecionado_arp td:nth-child(2)")

    for(let x=0; x<colunaNumeroItem.length; x++){
        numeroItemColuna = colunaNumeroItem[x].textContent.trim()
        cpnjColuna = colunaCnpj[x].textContent.trim()
        if (numeroItemColuna == item.numero && cpnjColuna == item.cpf_cnpj_idgener) {
            return
        }
    }

    // Substitui o valor informado no banco para texto
    item.permite_carona = item.permite_carona * 1
    let textoPermiteCarona = 'Não'

    if (item.permite_carona == 1) {
        textoPermiteCarona = 'Sim'
    }

    // Monta a linha para incluir na tabela
    let idLinha = `linha_${item.compraItemFornecedorId}`
    let linha = `   <tr id="${idLinha}">
                        <td>
                            <a class="br-button circle btn-danger large mr-3 br-button-row-table" href='javascript:void(0)'
                                onclick="removerLinhaItemSelecionado('${idLinha}', '${item.compraItemFornecedorId}')"  >
                                <i class='nav-icon la la-trash'></i>
                            </a>
                        </td>
                        <td>${item.cpf_cnpj_idgener}</td>
                        <td>${item.nomefornecedor}<br>(${item.classificacao})</td>
                        <td>${item.numero}</td>
                        <td>${item.descricao}</td>
                        <td>${item.codigo_siasg}</td>
                        <td>${item.descricaosimplificada}</td>
                        <td>${item.quantidadehomologadavencedor}</td>
                        <td>${item.valor_unitario}</td>
                        <td style="text-align: right;">${item.valor_negociado}</td>
                        <td style="text-align: right;">${item.maximo_adesao}</td>
                        <td style="text-align: right;">${item.maximo_adesao_informado_compra}</td>
                        <td style="text-align: right;">${textoPermiteCarona}
                            <input type="hidden" name="id_item_fornecedor[]" value="${item.compraItemFornecedorId}"/>
                        </td>
                    </tr>`

    // Adiciona a linha na tabela
    $("#table_item_compra_selecionado_arp #tbody_item_compra_selecionado_arp").append(linha)
    exibirEsconderBotao(true)
    descerPagina()
}

function calcularValorTotalAta(item) {
    valorItem = item.replaceAll(".", "")
    valorItem = valorItem.replaceAll(",", ".")
    return (valorItem * 1.0)
}

// Função em responsável de remover a linha da tabela do item selecionado
function removerLinhaItemSelecionado (idLinha, numero) {
    $(`#${idLinha}`).remove()
    removerItemSelecionadoItemCompra(numero)
    checkTodos(`#${numero}` , false)
    calcularAta(false)
}


function recolherAreaDetalheInformacao(id = '#areaDetalheMaiorDesconto') {
    $(id).hide()
}


function checkTodos(idCampo , state) {
    $(idCampo).prop('checked', state)
}

// Função responsável em retirar do array dos itens selecionados
function removerItemSelecionado(id) {
    if(selected.includes(id)) {
        index = selected.indexOf(id)
        selected.splice(index, 1)
    }

    let rowCount = $("#tbody_item_compra_selecionado_arp tr").length;
    if(rowCount == 0) {
        exibirEsconderBotao()
    }
    $(`#linha_${id}`).remove()
}

// Função responsável quando o botão de incluir item for clicado
function itemSelecionado (compraItemFornecedorId) {

    // Se o item estiver marcado, ele inclui na lista
    if($(`#${compraItemFornecedorId}`).is(":checked")) {
        removerItemSelecionado(compraItemFornecedorId)
        selected.push(compraItemFornecedorId)
        return
    }

    removerItemSelecionado(compraItemFornecedorId)
    checkTodos(".selectAll",false)
}

function contarDadosSelecionado() {
    let count = 0

    $.each(selected, function( index, value ) {
        count = count + 1
    });

    $("#qtdItemSelecionado").text(count)
}

// Função responsável em marcar os itens na tabela
function recuperarItemMarcado() {
    $.each(selected, function( index, value ) {
        checkTodos(`#${value}`,true)
    });
}

// Função responsável em marcar todos os itens quando clicar no check de todos
function marcartodos() {
    state = $(".selectAll").is(":checked")
    cols = datableModalItem.column(0).nodes(), state

    // percorre todas as marcações e inclui ou retira as marcações
    for (i = 0; i < cols.length; i += 1) {
        cols[i].querySelector("input[type='checkbox']").checked = state
        idFornecedor = cols[i].querySelector("input[type='checkbox']").id
        itemSelecionado(idFornecedor)
    }

    // Recupera todas as linhas da tabela de itens para incluir ou remover da lista
    let data = datableModalItem.rows().data();

    $.each(data, function( index, value ) {
        let idItem = value.numero
        if(state) {
            selectedData[value.compraItemFornecedorId] = value
        } else {
            $(`#linha_${value.compraItemFornecedorId}`).remove()
            removerItemSelecionadoItemCompra(idItem)
        }
    });

    contarDadosSelecionado()
    calcularAta(false)
}

$(".selectAll").on( "click", function(e) { marcartodos()  });

// Click do botão inserir item
$("#btnInserirItemFornecedor").click(function(){
    $("#areaItemSelecionado").show()
    $("#areaDadosAta").show()

    // Percore todos os itens incluidos no array e insere na tabela
    $.each(selected, function( index, valueSelected ) {
        if(selectedData[valueSelected] != undefined) {
            inserirLinhaItemSelecionado(selectedData[valueSelected])
        }

    });

    $('.arquivo-hidden').show()
    $("button[type=submit]").show()
    $(".areaItemSelecionado").show()
    calcularAta()
})

// Função responsável em calcular o valor total da ata pelos itens incluidos
function calcularAta(somarItem = true, item = null) {
    let valor = 0
    let tabela = recuperarColunaTabelaItemSelecionado(10)
    //document.body.querySelectorAll("#table_item_compra_selecionado_arp td:nth-child(10)")

    // Percorre toda a coluna e soma com os valores incluidos
    for(let x=0; x<tabela.length; x++){
        valor +=  calcularValorTotalAta( tabela[x].textContent.trim() )
    }

    // Responsável em exibir o valor formatado para o usuário
    let valorFormatado = valor

    if (valorFormatado > 0) {
        valorFormatado = valorFormatado.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})
    }

    // Inclui o valor formatado para o usuário e inclui no hidden o valor no formato do banco
    $("input[name='valor_total_fake']").val(valorFormatado)
    // $("input[name='valor_total']").val(valor)
}

// Método responsável em remover o item do array para envio
function removerItemSelecionadoItemCompra(id) {
    if(selectedData[id]) {
        selectedData.splice(id, 1)
        removerItemSelecionado(id)
    }
}

// Função responsável em incluir o item selecionado no array para ser percorrido e incluido na tabela
// de itens selecionados
$('#table_selecionar_item_compra_arp tbody').on( 'click', 'tr', function () {
    let data = datableModalItem.row( this ).data()
    // Insere o id da tabela compra_item_fornecedor devido ser único
    let idArray = data.compraItemFornecedorId
    selectedData[idArray] = data

    contarDadosSelecionado()
} );

// Método responsável em exibir o loading para o usuário
function montarLoadingCarregar(response) {
    return `<div class="br-loading" data-progress="${response.percentual}" id="loadingProgress" style="margin-left: 45%;">
    <div class="br-loading-mask full">
      <div class="br-loading-fill"></div>
    </div>
  </div>
  `
}

// Função responsável em validar as dastas para envio do form
function validarDatas(data_assinatura, vigencia_inicial, vigencia_final) {
    data_assinatura = formatarDataComparacao(data_assinatura)
    vigencia_inicial = formatarDataComparacao(vigencia_inicial)
    vigencia_final = formatarDataComparacao(vigencia_final)

    let timeElapsed = Date.now();
    let today = new Date(timeElapsed);
    let hoje = today.toLocaleDateString()
    hoje = formatarDataComparacao(hoje)

    if (data_assinatura > vigencia_inicial) {
        exibirAlertaNoty('error-custom', 'A data de vigência inicial não pode ser anterior à data de assinatura da ata')
        return false
    }

    if (vigencia_inicial > vigencia_final) {
        exibirAlertaNoty('error-custom', 'A data de vigência final não pode ser anterior à data de início da vigência da ata')
        return false
    }

    return true

}
function validarNumeroProcesso() {
    const numeroProcessoInput = document.querySelector('input[name="numero_processo"]');
    const valorInput = numeroProcessoInput.value;
    const unidadeSISG = true; // Verifique se a unidade está marcada como SISG

    // Verifica se a máscara do processo está configurada
    const mascaraProcesso = numeroProcessoInput.getAttribute('data-mask');

    if (!mascaraProcesso) {
        if (unidadeSISG) {
            // Permitir preenchimento sem máscara e sem valor mínimo de digitação
            return;
        } else {
            // Permitir preenchimento sem máscara, mas aplicar valor mínimo de digitação
            const valorSemMascara = valorInput.replace(/[^0-9A-Za-z]/g, ''); // Remover caracteres não numéricos e não alfabéticos
            if (valorSemMascara.length === 0) {
                const mensagemErro = 'Por favor, insira um valor válido para o número do processo.';
                exibirAlertaNoty('error-custom', mensagemErro);
                numeroProcessoInput.value = ''; // Limpar o campo
                numeroProcessoInput.blur(); // Remover o foco do campo
                return;
            }
        }
    }

    const valorSemMascara = valorInput.replace(/[^0-9A-Za-z]/g, ''); // Remover caracteres não numéricos e não alfabéticos

    const tamanhoMinimo = mascaraProcesso.replace(/[^0-9A-Za-z]/g, '').length; // Obter tamanho da máscara sem caracteres não numéricos e não alfabéticos

    if (valorSemMascara.length < tamanhoMinimo) {
        const mensagemErro = `Por favor, no campo Número do Processo informe no mínimo ${tamanhoMinimo} números.`;
        exibirAlertaNoty('error-custom', mensagemErro);
        numeroProcessoInput.value = ''; // Limpar o campo
        numeroProcessoInput.blur(); // Remover o foco do campo
        return;
    }
}

document.addEventListener('DOMContentLoaded', function() {
    const numeroProcessoInput = document.querySelector('input[name="numero_processo"]');

    numeroProcessoInput.addEventListener('blur', function() {
        validarNumeroProcesso();
    });
});
function formatarDataComparacao(data) {
    let arrayData = data.split('/')
    let dataFormatada = `${arrayData[2]}/${arrayData[1]}/${arrayData[0]}`
    return new Date(dataFormatada).getTime()
}

// Variável responsável em bloquear o envio do form
var bloquearSubmit = false
//Sobreescreveu o metodo para fazer a validacao nas datas
$("#rascunho").click(function(e) {

    // Verifica se a compra inserida está no banco de dados
    let podeValidarCompra =  validarCompraFinalizacaoCadastro()

    // Se não existir na base, exibe o erro para o usuário
    if (!podeValidarCompra) {
        exibirAlertaNoty('error-custom', 'Compra não encontrada. Para prosseguir, corrija as informações digitadas ')
        bloquearSubmit = true
        return
    }

    var rowCount = $("#tbody_item_compra_selecionado_arp tr").length;

    if(rowCount == 0) {
        exibirAlertaNoty('error-custom', 'Selecione ao menos um item da ata')
        bloquearSubmit = true
        return
    }

    // Responsável em verificar os campos da compra para o rascunho
    let validacao = bloquearEnvioCamposCompra()

    if( validacao != null) {
        bloquearSubmit = true
        exibirAlertaNoty(validacao.type,validacao.message)
        return
    }

    let id = $(this).attr("id")
    let nameHidden =  $(`#${id} span`).data("namecustom")
    let valueHidden =  $(`#${id} span`).data("valuecustom")

    let data_assinatura = $("input[name='data_assinatura']").val()
    let vigencia_inicial = $("input[name='vigencia_inicial']").val()
    let vigencia_final = $("input[name='vigencia_final']").val()

    $("input[name='data_assinatura']").prop('required',false);
    $("input[name='vigencia_inicial']").prop('required',false);
    $("input[name='vigencia_final']").prop('required',false);

    if (valueHidden == 0) {

        if (data_assinatura == "") {
            exibirAlertaNoty('error-custom', 'A data de assinatura está em branco')
            bloquearSubmit = true
            return
        }

        if (vigencia_inicial == "") {
            exibirAlertaNoty('error-custom', 'A data de vigência incial está em branco')
            bloquearSubmit = true
            return
        }

        if (vigencia_final == "") {
            exibirAlertaNoty('error-custom', 'A data de vigência final está em branco')
            bloquearSubmit = true
            return
        }

        data_assinatura = formatarDataComparacao(data_assinatura)
        vigencia_inicial = formatarDataComparacao(vigencia_inicial)
        vigencia_final = formatarDataComparacao(vigencia_final)

        let timeElapsed = Date.now();
        let today = new Date(timeElapsed);
        let hoje = today.toLocaleDateString()
        hoje = formatarDataComparacao(hoje)

        if (data_assinatura >= vigencia_inicial) {
            exibirAlertaNoty('error-custom', 'A data de vigência inicial não pode ser anterior ou igual à data de assinatura da ata')
            bloquearSubmit = true
            return
        }

        if (vigencia_inicial > vigencia_final) {
            exibirAlertaNoty('error-custom', 'A data de vigência final não pode ser anterior à data de início da vigência da ata')
            bloquearSubmit = true
            return
        }


    }

    // Chama a função para adicionar o zero à esquerda no campo do número da ata
    let numeroAta = adicionarZeroEsquerda();

    // Chama a função para validar o número da ata
    let resultadoValidacao = validarNumeroAta(numeroAta);

    // Verifica o resultado da validação
    if (resultadoValidacao !== true) {
        exibirAlertaNoty('error-custom', resultadoValidacao);
        bloquearSubmit = true;
        return;
    }


    // Se todos os campos estiverem válidos, envia o form
    if($(this).closest('form')[0].checkValidity()) {
        e.preventDefault()
        $('#hidden_custom').attr('name',nameHidden);
        $('#hidden_custom').attr('value',valueHidden);

        var saveActions = $('#saveActions'),
            crudForm        = saveActions.parents('form'),
            saveActionField = $('[name="_save_action"]');

        crudForm.submit(function (event) {
            window.removeEventListener('beforeunload', preventUnload);
            $("button[type=submit]").prop('disabled', true);
        });

        crudForm.submit();
    }

});

$("#adicionar").click(function(e) {

    let podeValidarCompra = true

    // Se for na tela de criação, realiza a validação
    if(!currentLocation.pathname.includes("edit")) {
        // Verifica se a compra inserida está no banco de dados
        podeValidarCompra =  validarCompraFinalizacaoCadastro()
    }

    // Se não existir na base, exibe o erro para o usuário
    if (!podeValidarCompra) {
        exibirAlertaNoty('error-custom', 'Compra não encontrada. Para prosseguir, corrija as informações digitadas ')
        bloquearSubmit = true
        return
    }
    // Recupera as informações para validar o que o usuário digitou após a validação da compra
    let unidadeGerenciadora = $("#unidade_origem_id").val()
    let numeroAta = $("input[name=numero]").val()
    let anoAta = $("input[name=ano]").val()
    let urlValidarNumeroAta = $("#url_buscar_arp").val()

    bloquearSubmit = false
    // Responsável em buscar as informações se a ata pode ser inserida
    $.ajax({
        url: urlValidarNumeroAta,
        type: "POST",
        async: false,
        data: {numeroAta:numeroAta , anoAta: anoAta, unidadeGerenciadora: unidadeGerenciadora},
        success: function (response) {
          if (!response) {
            exibirAlertaNoty(
                'error-custom',
                `Existe ata com o mesmo número e ano registrada para essa unidade.
                 Para prosseguir, corrija as informações digitadas`
            )
            bloquearSubmit = true
            return
          }
        }
    });

    let temArquivo = parseInt($("input[name=temArquivo]").val())
    let arquivoInput = $("input[name=arquivo]")

    if(arquivoInput.get(0).files.length === 0 && ((temArquivo && temArquivo === 0) || !temArquivo)) {
        exibirAlertaNoty('error-custom', 'O campo do arquivo é obrigatório')
        bloquearSubmit = true
        return
    }

    var rowCount = $("#tbody_item_compra_selecionado_arp tr").length;

    if(rowCount == 0) {
        exibirAlertaNoty('error-custom', 'Selecione ao menos um item da ata')
        bloquearSubmit = true
        return
    }

    let id = $(this).attr("id")
    let nameHidden =  $(`#${id} span`).data("namecustom")
    let valueHidden =  $(`#${id} span`).data("valuecustom")

    let data_assinatura = $("input[name='data_assinatura']").val()
    let vigencia_inicial = $("input[name='vigencia_inicial']").val()
    let vigencia_final = $("input[name='vigencia_final']").val()

    $("input[name='data_assinatura']").prop('required',false);
    $("input[name='vigencia_inicial']").prop('required',false);
    $("input[name='vigencia_final']").prop('required',false);

    if (valueHidden == 0) {

        if (data_assinatura == "") {
            exibirAlertaNoty('error-custom', 'A data de assinatura está em branco')
            bloquearSubmit = true
            return
        }

        if (vigencia_inicial == "") {
            exibirAlertaNoty('error-custom', 'A data de vigência incial está em branco')
            bloquearSubmit = true
            return
        }

        if (vigencia_final == "") {
            exibirAlertaNoty('error-custom', 'A data de vigência final está em branco')
            bloquearSubmit = true
            return
        }

        data_assinatura = formatarDataComparacao(data_assinatura)
        vigencia_inicial = formatarDataComparacao(vigencia_inicial)
        vigencia_final = formatarDataComparacao(vigencia_final)

        let timeElapsed = Date.now();
        let today = new Date(timeElapsed);
        let hoje = today.toLocaleDateString()
        hoje = formatarDataComparacao(hoje)

        if (data_assinatura >= vigencia_inicial) {
            exibirAlertaNoty(
                'error-custom',
                'A data de vigência inicial não pode ser anterior ou igual à data de assinatura da ata'
            )
            bloquearSubmit = true
            return
        }

        if (vigencia_inicial > vigencia_final) {
            exibirAlertaNoty(
                'error-custom',
                'A data de vigência final não pode ser anterior à data de início da vigência da ata'
            )
            bloquearSubmit = true
            return
        }

        if (vigencia_inicial == vigencia_final) {
            exibirAlertaNoty(
                'error-custom',
                'A data de vigência final não pode ser igual à data de início da vigência da ata'
            )
            bloquearSubmit = true
            return
        }

        let validacao = bloquearEnvioCamposCompra()

        if( validacao != null) {
            bloquearSubmit = true
            return
        }

    }

});

function preventUnload(event) {
if (initData !== getFormData()) {
    // Cancel the event as stated by the standard.
    event.preventDefault();
    // Older browsers supported custom message
    event.returnValue = '';
}
}

// Click do botão para recuperar a compra para o usuário
$("#btn_recuperar_compra_arp").click(function(e){
    selected = null
    selectedData = null

    $(".areaItemSelecionado").hide()
    e.preventDefault()
    let validacao = bloquearEnvioCamposCompra()

    if( validacao != null) {
        $.unblockUI();

        exibirAlertaNoty(validacao.type,validacao.message)
        mudarBotaoAcaoRealizada("#btn_recuperar_compra_arp",false,"btn-success", "btn-info")
        mudarBotaoAcaoRealizada("#icone_btn_recuperar_compra_arp",false,"fas fa-check","fas fa-search")

        return
    }

    $.blockUI({ message: $('#loadingContratos') });

    let form = $("form").serialize()
    form = form.replaceAll("&_method=PUT", "")

    let urlBuscaCompra = $('[name="url_buscar_compra"]').val()

    let dadosCompra = montarInformacoesCompra()

    try {
        $.post( urlBuscaCompra, dadosCompra).done(function( response ) {

            let data = JSON.parse(JSON.stringify(response));
            exibirAlertaNoty(data.type +'-custom',data.message)

            if(data.code == 202 || data.code == 203) {
                mudarBotaoAcaoRealizada("#btn_recuperar_compra_arp",false,"btn-success", "btn-info")
                mudarBotaoAcaoRealizada("#icone_btn_recuperar_compra_arp",false,"fas fa-check","fas fa-search")
                areaCamposValidacaoCompra(false)
                return
            }

            $("#table_fornecedor_compra_arp #tbody_fornecedor_compra_arp").empty()
            if (!currentLocation.pathname.includes("edit")) {
                $("#table_item_compra_selecionado_arp #tbody_item_compra_selecionado_arp").empty();
            }
            let fornecedores = data.data
            inserirLinhaFornecedor(fornecedores, false)

            if(datableModalItem != null) {
                datableModalItem.clear();
                datableModalItem.destroy();
            }

            carregarTabelaItemFornecedor()
            // limpar a tabela e esconder os campos
            areaCamposValidacaoCompra(true)

            selected = []
            selectedData = []
            mudarBotaoAcaoRealizada("#btn_recuperar_compra_arp",true, "btn-info", "btn-success")
            mudarBotaoAcaoRealizada("#icone_btn_recuperar_compra_arp",true,"fas fa-search", "fas fa-check")

            descerPagina()
        }) .catch((error) => {
            areaCamposValidacaoCompra(false)
            console.error('Error during service worker registration:', error.responseJSON);
            $.unblockUI();

            if (error.responseJSON.message.includes('Contratação com local de entrega inválido')) {
                exibirAlertaNoty('error-custom', error.responseJSON.message)
                return
            }

            if (error.responseJSON.code != undefined && error.responseJSON.code != 200) {
                exibirAlertaNoty(`${error.responseJSON.type}-custom`, error.responseJSON.message)
                return
            }

            exibirAlertaNoty('error-custom', 'Erro ao carregar a compra')
        });
    } catch (error) {
        alert(error)
    }

})

// Método responsável em exibir o alerta para alteração do campo de origem da compra
$('#unidade_origem_compra_fake').on('select2:select', function (e) {
    let data = e.params.data;
    let unidadeSubrrogada = $("input[name=unidade_origem_id_fake]").val()
    let mensagem = `Ao alterar a unidade origem da compra,
                    a busca da compra será realizada a partir das seguintes informações:<br>
                    Unidade origem da compra: ${data.text} <br>
                    Unidade sub-rogada: ${unidadeSubrrogada}

                    `
    exibirAlertaNoty('warning-custom',mensagem)
    $("#unidade_origem_compra_id").val(data.id)
});

/**
 * Método responsável em verificar se os dados da compra informado estão corretas
 * para poder finalizar o cadastro da ata
 */
function validarCompraFinalizacaoCadastro()
{
    let bloquearCadastroFinalizacao = false
    let urlValidarCompraFinalizacaoCadastro = $("#url_validar_compra_cadastro").val()

    $.ajax({
        url: urlValidarCompraFinalizacaoCadastro,
        type: "POST",
        async: false,
        data: montarInformacoesCompra(),
        success: function (response) {
            bloquearCadastroFinalizacao = response
        }
    });

    return bloquearCadastroFinalizacao
}

/**
 * Método responsável em montar as informações que serão responsáveis
 * em consultar a compra
 */
function montarInformacoesCompra()
{
    let modalidade_id = $("#modalidade_id").val()

    // Se não encontrar o campo hidden, pega pela seleção do usuário
    if (modalidade_id == "") {
        modalidade_id = $('select[name="modalidade_id"]').val()
    }

    let unidade_origem_id = $("#unidade_origem_id").val()
    let unidade_origem_compra_id= $("#unidade_origem_compra_id").val()
    let numero_ano = $("input[name=numero_ano]").val()

    return {
        modalidade_id: modalidade_id,
        unidade_origem_id: unidade_origem_id,
        unidade_origem_compra_id: unidade_origem_compra_id,
        numero_ano: numero_ano
    }

}

$(document).ready(function () {
    $(document).on('keydown', function (event) {
        if (event.keyCode === 27) {
            event.preventDefault(); // Impede o comportamento padrão da tecla "ESC"
        }
    });
});