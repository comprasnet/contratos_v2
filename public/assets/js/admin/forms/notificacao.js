function alterarSituacaoNotificacao(id, situacaoAtual = undefined) {
    if (situacaoAtual == undefined) {
        situacaoAtual = 0;
    }

    $.ajax({
        url: `notificacao/alterarsituacao/${id}`,
        data: {situacaoAtual, id},
        type: 'PUT',
        success: function(response) {
            exibirAlertaNoty('success-custom', response.message)
            crud.table.ajax.reload();
        },
        error: function(xhr, status, error) {
            // Handle errors here
            console.error(error);
            exibirAlertaNoty('error-custom', 'Erro ao alterar a situação da notificação')
        }
    });
}

function alterarSituacaoNotificacaoAcessoRapido(id, situacaoAtual, exibirAlerta = false) {
    $.ajax({
        url: `/notificacao/alterarsituacao/${id}`,
        data: {situacaoAtual, id},
        type: 'PUT',
        success: function(response) {
            if (exibirAlerta) {
                exibirAlertaNoty('success-custom', response.message)
            }
        },
        error: function(xhr, status, error) {
            // Handle errors here
            console.error(error);
            exibirAlertaNoty('error-custom', 'Erro ao alterar a situação da notificação')
        }
    });
}