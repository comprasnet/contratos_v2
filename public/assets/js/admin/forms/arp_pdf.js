$(document).ready(function () {
    $('.gerar-pdf').click(function (e) {
        e.preventDefault();

        if ($(this).data('load') === 1) {
            return false;
        }

        var that = $(this);
        that.data('load', 1);

        var arpId = $(this).data('arp-id');

        var $gerarPdfButton = $(this);

        // Função para executar o reCAPTCHA
        function executeRecaptcha() {
            grecaptcha.ready(function () {
                grecaptcha.execute('6LdUa3YoAAAAAGPRF2IyPNt1DNQ8z0zrnnXZCh2n', {action: 'submit'}).then(function (token) {
                    // Preencha o campo g-recaptcha-response
                    $('#g-recaptcha-response').val(token);
                    that.data('load', 1);
                    enviarRequisicaoPDF();
                });
            });
        }


        // Função para enviar a requisição do PDF
        function enviarRequisicaoPDF() {
            $.ajax({
                type: 'POST',
                url: '/transparencia/disparar-job-pdf/' + arpId,
                data: $('#recaptcha-form').serialize(),
                dataType: 'json',
                success: function (response) {
                    var score = response.score;
                    if (score > 0.5) {
                        gerarPDFComRecaptcha(response);
                    } else if(usarRecaptcha === false){
                        gerarPDFComRecaptcha(response);
                    }else {
                        handleErroRecaptcha();
                    }
                },
                error: function () {
                    handleErroRequisicaoPDF();
                }
            });
        }

        // Função para lidar com a geração do PDF com reCAPTCHA
        function gerarPDFComRecaptcha(response) {
            var alertTimer;

            if ($gerarPdfButton.hasClass('pdf-gerado')) {
                window.location.href = $gerarPdfButton.data('pdf-url');
            } else if (!$gerarPdfButton.hasClass('pdf-em-processo-de-geracao')) {
                $gerarPdfButton.addClass('pdf-em-processo-de-geracao').prop('disabled', true);
                exibirAlertaNoty('info-custom', 'O PDF está sendo gerado e ficará disponível quando for concluído.<br>O PDF é gerado apenas uma vez por dia, eventuais atualizações realizadas após o horário de geração do PDF estarão disponíveis no próximo dia.');

                $gerarPdfButton.find('i').removeClass('fa-file-pdf').addClass('fa-spinner fa-spin');

                alertTimer = setTimeout(function () {
                    exibirAlertaNoty('warning-custom', 'A geração do PDF está demorando mais que o esperado. Por favor, volte mais tarde.');
                }, 15000);

                if (response.success) {
                    var intervalId = setInterval(function () {
                        $.ajax({
                            type: 'GET',
                            url: '/transparencia/check-status-pdf/' + arpId,
                            dataType: 'json',
                            success: function (statusResponse) {
                                if (statusResponse.gerado) {
                                    clearInterval(intervalId);
                                    clearTimeout(alertTimer);
                                    $gerarPdfButton.removeClass('pdf-em-processo-de-geracao pdf-spinner').addClass('pdf-gerado');
                                    exibirAlertaNoty('success-custom', 'PDF gerado com sucesso!');
                                    $gerarPdfButton.find('i').removeClass('fa-spinner fa-spin').addClass('fas fa-download');
                                    that.data('load', 0);
                                    window.location.href = response.pdfUrl;
                                    var $visualizarPdfButton = $('<div class="ml-0 mt-5"><a href="/transparencia/visualizar-pdf/' + arpId + '/visualizar" title="Visualizar PDF" target="_blank"><i class="fas fa-eye" style="font-size: 22px !important;"></i></a></div>');
                                    $gerarPdfButton.data('pdf-url', '/transparencia/visualizar-pdf/' + arpId + '/download');
                                    $gerarPdfButton.after($visualizarPdfButton);
                                } else if (statusResponse.erro) {
                                    clearInterval(intervalId);
                                    $gerarPdfButton.removeClass('pdf-em-processo-de-geracao pdf-spinner').prop('disabled', false);
                                    exibirAlertaNoty('error-custom', 'Ocorreu um erro ao gerar o PDF.');
                                    $gerarPdfButton.find('i').removeClass('fa-spinner fa-spin').addClass('fa-file-pdf');
                                }
                            },
                            error: function () {
                                that.data('load', 0);
                                alert('Ocorreu um erro ao iniciar a geração do PDF.');
                            }
                        });
                    }, 5000);
                } else {
                    $gerarPdfButton.removeClass('pdf-em-processo-de-geracao pdf-spinner').prop('disabled', false);
                    that.data('load', 0);
                    clearTimeout(alertTimer);
                    alert('Ocorreu um erro ao iniciar a geração do PDF.');
                }
            }
        }

        // Função para lidar com erro no reCAPTCHA
        function handleErroRecaptcha() {
            $gerarPdfButton.removeClass('pdf-em-processo-de-geracao pdf-spinner').prop('disabled', false);
            that.data('load', 0);
            exibirAlertaNoty('error-custom', 'A pontuação do reCAPTCHA é muito baixa. Não é permitido gerar o PDF.');
        }

        // Função para lidar com erro na requisição do PDF
        function handleErroRequisicaoPDF() {
            $gerarPdfButton.removeClass('pdf-em-processo-de-geracao pdf-spinner').prop('disabled', false);
            alert('Ocorreu um erro ao iniciar a geração do PDF.');
        }

        if (usarRecaptcha) {
            executeRecaptcha();
        } else {
            enviarRequisicaoPDF();
        }
    });
});
