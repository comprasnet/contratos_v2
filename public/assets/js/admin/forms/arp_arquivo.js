/**
 * Ao Criar arquivo verifia se é um arquivo publico e caso sim exibe loading de enviando para o Pncp
 */

$('button[type="submit"]').on('click', function () {
    $('form').submit();

    /*verifica se é um arquivo publico*/
    if (!$('#input_restrito').is(":checked")) {
        /*verifica se o arquivo é do tipo tipo Arp*/
        if (crud.field('tipo_id').input.value === '1') {
            exibirAlertaEnvioCustomizado('Salvando Dados e Atualizando PNCP');
        }
    }
})

function showArquivoAlert(val) {
    if(val == 'Ata de Registro de Preços') {
        $('#arp_arquivo_alert').show();
    } else {
        $('#arp_arquivo_alert').hide();
    }
}

$(function(){
    $("#vigencia_inicial,#vigencia_final").each(function(){
        if($(this).attr('readonly')){
            $(this).next().remove();
        }
    });
});

$('#tipo_id').on('select2:select', function(e) {
    let data = e.params.data;
    let disabled = false

    if ( data.text === 'Ata de Registro de Preços') {
        disabled = true
    }

    $("#input_restrito").prop('disabled', disabled)
});