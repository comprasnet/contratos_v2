let adesaoIdCancelamento = null
let urlAdesaoCancelamento = null

function cancelarAdesao (adesaoId, numeroAdesao, urlCancelamento) {

    adesaoIdCancelamento = adesaoId
    urlAdesaoCancelamento = urlCancelamento

    $("#modalcancelaradesao").addClass( "active" );
    $("#modalcancelaradesao .br-modal-header").text( `Cancelar adesão ${numeroAdesao}` );
}

$("#btn_fechar_modal_cancelamento").click(function() {
    $("#modalcancelaradesao").removeClass( "active" );
    $("#justificativa_cancelamento").val('')
})

$("#btn_realizar_cancelamento").click(function () {
    let justificativaCancelamento = $("#justificativa_cancelamento").val()

    if (justificativaCancelamento === '') {
        exibirAlertaNoty('warning-custom', 'Informe uma justificativa para realizar o cancelamento');
        return
    }

    $.post( urlAdesaoCancelamento,{adesaoIdCancelamento, justificativaCancelamento })
        .done(function( response ) {
            exibirAlertaNoty(`${response.type}-custom`, response.message);

            if (response.type == 'success') {
                $('#crudTable').DataTable().ajax.reload();
                $("#modalcancelaradesao").removeClass( "active" );
            }
        })
        .catch((error) => {
            let objetoErro = error.responseJSON.errors
            Object.keys(objetoErro).forEach(function(chave) {
                exibirAlertaNoty('error-custom', objetoErro[chave]);
            });
        });
})