let entregaIdCancelamento = null
let urlEntregaCancelamento = null

function cancelarEntrega (adesaoId, numeroEntrega, urlCancelamento) {

    entregaIdCancelamento = adesaoId
    urlEntregaCancelamento = urlCancelamento

    $("#modalcancelarentrega").addClass( "active" );
    $("#modalcancelarentrega .br-modal-header").text( `Cancelar entrega ${numeroEntrega}` );
}

$("#btn_fechar_modal_cancelamento").click(function() {
    $("#modalcancelarentrega").removeClass( "active" );
    $("#justificativa_cancelamento").val('')
})

$("#btn_realizar_cancelamento").click(function () {
    let justificativaCancelamento = $("#justificativa_cancelamento").val()

    if (justificativaCancelamento === '') {
        exibirAlertaNoty('warning-custom', 'Informe uma justificativa para realizar o cancelamento');
        return
    }

    $.post( `entrega/${entregaIdCancelamento}/${urlEntregaCancelamento}`,{entregaIdCancelamento, justificativa_motivo_analise: justificativaCancelamento })
        .done(function( response ) {
            exibirAlertaNoty(`${response.type}-custom`, response.message);

            if (response.type == 'success') {
                $('#crudTable').DataTable().ajax.reload();
                $("#modalcancelarentrega").removeClass( "active" );
            }
        })
        .catch((error) => {
            let objetoErro = error.responseJSON.errors
            Object.keys(objetoErro).forEach(function(chave) {
                exibirAlertaNoty('error-custom', objetoErro[chave]);
            });
        });
})