function acaoEsconderExibir(marcado, idArea) {
    if (marcado) {
        $(idArea).show()
        return
    }

    $(idArea).hide()
}

function esconderTodaAreas() {
    acaoEsconderExibir(false, ".areaVigencia")
    acaoEsconderExibir(false, ".areaValoresRegistrados")
    acaoEsconderExibir(false, ".areaInformativo")
    acaoEsconderExibir(false, ".justificativa_motivo_cancelamento")
    acaoEsconderExibir(false, ".quantitativo_renovado_vigencia")
    acaoEsconderExibir(false, ".amparo_legal_renovacao_quantitativo")
    acaoEsconderExibir(false, ".areaAlteracaoFornecedor")
}
let exibirColunaValorRegistrado = false
let exibirColunaItemCancelado = false
let itemCarregado = false

function exibirCampoFormulario(campo) {
    let tipoSelecionado = campo.dataset.textOption
    let idArp = campo.dataset.idarp
    let url = campo.dataset.url

    if (tipoSelecionado == 'Vigência') {
        acaoEsconderExibir(campo.checked, ".areaVigencia");
        acaoEsconderExibir(campo.checked, ".quantitativo_renovado_vigencia");
        analisarCampoQuantitativo(".quantitativo_renovado_vigencia input[type='checkbox']");

    }

    if (tipoSelecionado == 'Valor(es) registrado(s)') {
        exibirItem(idArp, url+'/itemalteracao', campo.checked, tipoSelecionado)
    }

    if (tipoSelecionado == 'Cancelamento de item(ns)') {
        exibirItem(idArp, url+'/itemalteracao', campo.checked, tipoSelecionado)
    }

    if (tipoSelecionado == 'Informativo') {
        acaoEsconderExibir(campo.checked, ".areaInformativo")
    }

    if (tipoSelecionado == 'Fornecedor') {
        exibirItemFornecedorAlteracao(idArp, url+'/fornecedoralteracao', campo.checked)
    }
}

function exibirEsconderColunaValorRegistradoItemCancelado(tipoSelecionado, executarAcao) {
    if (tipoSelecionado == 'Valor(es) registrado(s)') {
        exibirColunaValorRegistrado = executarAcao
    }

    if (tipoSelecionado == 'Cancelamento de item(ns)') {
        exibirColunaItemCancelado = executarAcao
    }

    acaoEsconderExibir(exibirColunaValorRegistrado, ".novovalor")
    acaoEsconderExibir(exibirColunaValorRegistrado, ".novovalordesconto")
    acaoEsconderExibir(exibirColunaValorRegistrado, ".percentual-antigo")
    acaoEsconderExibir(exibirColunaItemCancelado, ".itemcancelado")
    acaoEsconderExibir(exibirColunaItemCancelado, ".justificativa_motivo_cancelamento")
    //acaoEsconderExibir(exibirColunaItemCancelado, ".quantitativo_renovado_vigencia")
}

function analisarCampoQuantitativo(campo) {
    // Verifica se a opção selecionada é "Sim"
    if ($(campo).val() == 1) {
        acaoEsconderExibir(true, ".amparo_legal_renovacao_quantitativo");
        exibirAlertaNoty('warning-custom', 'Essa opção só pode ser utilizada por unidades Não-Sisg que possuam' +
            'regulamentação permitindo a renovação de saldo.<br>' +
            'O Decreto Federal nº 11.462/2023 não permite a renovação de quantitativo para atas da Administração ' +
            'Pública federal direta, autárquica e fundacional.')
    } else {
        acaoEsconderExibir(false, ".amparo_legal_renovacao_quantitativo");
    }
}

/**
 * Método responsável em exibir a mensagem se o item estiver sido selecionado
 * ou esteja contido em uma minuta de empenho
 * @param {*} url
 * @param {*} arpItemId
 * @returns
 */
function exibirMensagemMinutaEmpenho(url, campoSelecionado)
{
    if (!campoSelecionado) {
        return
    }

    // Quebra a url para pegar a o id do item
    let parts = url.split('/');
    // Recupera o id do item
    let arpItemId = parts.pop() || parts.pop();
    let bloquearAcao = false
    $.ajax({
        url: url,
        async: false,
        type:'GET',
        data: {arpItemId : arpItemId},
        success: function (response) {
            if (response != "") {
                exibirAlertaNoty('warning-custom', response, '', false)
                bloquearAcao = true
            }
        }
    });

    return bloquearAcao
}

// Se for um item cancelado, bloqueia o lançamento do valor
function bloquearNovoValor(campoNovoValor, campo) {
    
    let id = `input[name='${campoNovoValor}']`
    $(id).prop('readonly', campo.checked);

    // Se o campo for marcado, seta o valor da quantidade com o zero
    if(campo.checked) {

        // Não verifica mais se o item tem empenho e/ou contratos vinculados
        //exibirMensagemMinutaEmpenho(url, campo.checked)

        // $.get( url, {arpItemId: lastPart}).done(function( response ) {
        //     exibirAlertaNoty('warning-custom', response)
        // })

        $(id).val(0)
        return
    }

    $(id).val()
}

// Exibe a listagem dos itens
function exibirItem(idArp, url, executarAcao, tipoSelecionado) {
    //Verifica se a página está no modo de edição
    var modoEdicao = currentLocation.pathname.includes("edit");
    acaoEsconderExibir(true, ".areaValoresRegistrados")

    // $(document).on('input', '.inputValorDesconto', function() {
    //     var $campoNovoValorUnitario = $(this).closest('tr').find('.inputValorRegistrado');
    //     var novoValorDesconto = $(this).val();
    //     var criterioJulgamento = $(this).closest('tr').find('.inputValorRegistrado').data('criterio-julgamento');
    //
    //     if (novoValorDesconto !== "") {
    //         // Se 'novo_valor_desconto' foi preenchido, desabilita 'novo_valor_unitario'
    //         $campoNovoValorUnitario.prop('disabled', true);
    //     } else {
    //         // Se 'novo_valor_desconto' foi esvaziado e 'criterio_julgamento' for vazio ou null,
    //         // habilita 'novo_valor_unitario' e limpa o campo
    //         if (!criterioJulgamento) {
    //             $campoNovoValorUnitario.prop('disabled', false);
    //             $(this).closest('tr').find('.inputValorRegistrado').val('');
    //         }
    //     }
    // });
    //
    // $(document).on('input', '.inputValorRegistrado', function() {
    //     var $campoNovoValorDesconto = $(this).closest('tr').find('.inputValorDesconto');
    //     var novoValorUnitario = $(this).val();
    //     var criterioJulgamento = $(this).data('criterio-julgamento');
    //
    //     if (novoValorUnitario !== "") {
    //         // Se 'novo_valor_unitario' foi preenchido, desabilita 'novo_valor_desconto'
    //         $campoNovoValorDesconto.prop('disabled', true);
    //     } else {
    //         // Se 'novo_valor_unitario' foi esvaziado e 'criterio_julgamento' for vazio ou null,
    //         // habilita 'novo_valor_desconto'
    //         if (!criterioJulgamento) {
    //             $campoNovoValorDesconto.prop('disabled', false);
    //         }
    //     }
    // });



    if (executarAcao) {
        if (!itemCarregado) {
            $.ajax({
                url: url,
                async: false,
                data: {idArp : idArp},
                type:'POST',
            }).done(function (response) { // "data" é o que retorna do Ajax
                $("#tabelaValoresRegistrados tbody").empty()

                $.each(response, function( index, dados ) {
                    let urlPesquisarEmpenho = window.location.origin + `/arp/alteracaoarp/${dados.idata}/pesquisarempenho/` + dados.id
                    let nameInputValorUnitario = `novo_valor_unitario[${dados.id}]`
                    let nameInputValorDesconto = `novo_valor_desconto[${dados.id}]`
                    let classInputValorDesconto = `novo_valor_desconto_${dados.id}`
                    let classInputValorUnitario = `novo_valor_unitario_${dados.id}`
                    let idValorUnitarioColuna = `valor_unitario_coluna_${dados.id}`

                    let idCheckBoxItemCancelado = `chk_${dados.id}`
                    let linha = `<tr>
                        <td style="vertical-align:middle !important">${dados.nomefornecedor}</td>
                        <td style="vertical-align:middle !important">${dados.numero}</td>
                        <td style="vertical-align:middle !important">${dados.descricaodetalhada}</td>
                        <td id="${idValorUnitarioColuna}"  style="vertical-align:middle !important">${dados.valor_unitario}</td>
                        <td class="percentual-antigo" style="vertical-align:middle !important">
                            ${dados.percentual_maior_desconto !== null ? dados.percentual_maior_desconto : ''}
                        </td>
                        
                        <td class="novovalordesconto" style="vertical-align:middle !important">
                            <div class="br-input">
                                <input type="text"                      
                                    id="${classInputValorDesconto}"
                                    data-id="${dados.id}"
                                    class="inputValorDesconto"
                                    name="${nameInputValorDesconto}"                     
                                    ${modoEdicao ? `value="${dados.percentual_desconto !== null ? dados.percentual_desconto : ''}"` : ''}
                                    onblur="calcularValores(this)"  
                            
                               ${(!dados.criterio_julgamento || dados.criterio_julgamento === "" || 
                                dados.criterio_julgamento === 'V' || dados.criterio_julgamento !== 'D') ? 'disabled' : ''} 
                 
   
                            </div>
                        </td>                       
                        <td class="novovalor" style="vertical-align:middle !important">
                            <div class="br-input">
                                <input type="text"
                                    id="${classInputValorUnitario}"
                                    class="inputValorRegistrado"
                                    data-criterio-julgamento="${dados.criterio_julgamento}" 

                                    name="${nameInputValorUnitario}"
                                    ${modoEdicao ? `value="${dados.valor_rascunho !== null ? dados.valor_rascunho : ''}"` : ''}
                                    
                                    onblur="exibirMensagemMinutaEmpenho('${urlPesquisarEmpenho}', true)" 
                                      ${dados.criterio_julgamento === 'D' ? 'disabled' : ''} />                                        
                            </div>
                        </td>            
                        
                        <td class="itemcancelado" style="vertical-align:middle !important">`
                    if (!dados.item_alterado_fornecedor) {
                        linha +=`
                        <div class="br-checkbox">
                                <input id="${idCheckBoxItemCancelado}" class="itemcanceladoclass" type="checkbox"
                                        name="item_cancelado[${dados.id}]"
                                        onclick="bloquearNovoValor('${nameInputValorUnitario}', this)"
                                        value="1"/>
                                <label for="${idCheckBoxItemCancelado}"></label>
                            </div>`
                    }

                    if (dados.item_alterado_fornecedor) {
                        linha +=`Houve uma alteração de fornecedor para o item, não pode ser cancelado na ata.`
                    }
                    linha +=`
                        </td>
                    </tr>`
                    $("#tabelaValoresRegistrados tbody").append(linha)
                })
                itemCarregado = true
                $('.inputValorRegistrado').mask('000.000.000.000.000,0000', {reverse: true});
            });
        }
        exibirEsconderColunaValorRegistradoItemCancelado(tipoSelecionado, executarAcao)
        return
    }

    exibirEsconderColunaValorRegistradoItemCancelado(tipoSelecionado, executarAcao)
    itemCarregado = false

    if (!exibirColunaValorRegistrado && !exibirColunaItemCancelado) {
        acaoEsconderExibir(false, ".areaValoresRegistrados")
        $("#tabelaValoresRegistrados tbody").empty()
    }
}


function calcularValores(dado) {
    let valorInput = dado.value.replace(',', '.');
    // Realiza o cálculo com o valor ajustado
    let novoValorUnitarioPorcentagem = 1 - (parseFloat(valorInput) / 100);
    let valorAntigo = parseFloat($('#valor_unitario_coluna_' + dado.dataset.id).text());
    let resultado = isNaN(novoValorUnitarioPorcentagem) ? "" : (valorAntigo * novoValorUnitarioPorcentagem).toFixed(4);
    $('#novo_valor_unitario_' + dado.dataset.id).val(resultado);
}

let currentLocation = window.location;



if (currentLocation.pathname.includes("edit")) {

    exibirEsconderBotaoEditar();
    itemCarregado = false;

    $(document).ready(function() {
        $('input[type=checkbox]').each(function () {
            if (this.checked) {
                let dataset = this.dataset
                let tipoSelecionado = dataset.textOption
                let idArp = dataset.idarp
                let url = dataset.url

                if (tipoSelecionado == 'Fornecedor') {
                    exibirItemFornecedorAlteracao(idArp, url+'/fornecedoralteracao', this.checked)
                }
            }
        });

        // Calcula o valor imediatamente
        $('input.inputValorDesconto').each(function() {
            var input = this;
            var criterioJulgamento = $('input.inputValorRegistrado').data('criterio-julgamento');
            if (criterioJulgamento === 'D') {
                var valorAtual = input.value; // Armazena o valor atual
                calcularValores(input);
                // Atribui o valor de volta após o cálculo
                input.value = valorAtual;
            }
        });


        $('input.inputValorRegistrado').each(function() {
            var input = this;
            var criterioJulgamento = $('input.inputValorRegistrado').data('criterio-julgamento');

            if (criterioJulgamento === 'V') {
                var valorAtual = input.value; // Armazena o valor atual
                calcularValores(input); // Calcula o novo valor diretamente no campo
                // Atribui o valor de volta após o cálculo
                input.value = valorAtual;
            }

        });

        $(".quantitativo_renovado_vigencia input[type='radio']").each(function () {
            analisarCampoQuantitativoEdit(this);
        });

        $(".quantitativo_renovado_vigencia input[type='radio']").change(function () {
            analisarCampoQuantitativoEdit(this);
        });

    });
} else {
    esconderTodaAreas();
}

function recuperarMarcacoesUsuario() {
    atLeastOneIsChecked = $('input[name="edit_tipo_id[]"]').val();
    atLeastOneIsChecked = JSON.parse(atLeastOneIsChecked)

    return atLeastOneIsChecked;
}

function analisarCampoQuantitativoEdit(campo) {
    // Verifica se o radio button marcado tem valor igual a 1
    if ($(campo).is(':checked') && $(campo).val() == 1) {
        acaoEsconderExibir(true, ".amparo_legal_renovacao_quantitativo");
       // exibirAlertaNoty('warning-custom', 'Quantitativo renovado na prorrogação de vigência')
    } else {
        acaoEsconderExibir(false, ".amparo_legal_renovacao_quantitativo");
    }
}

// Responsável para exibir as informações digitadas pelo usuário
function exibirEsconderBotaoEditar() {
    esconderTodaAreas()
    atLeastOneIsChecked = recuperarMarcacoesUsuario()
    let valorRegistradoMarcado = false
    // Recupera todas as informações marcadas pelo usuário
    $.each(atLeastOneIsChecked, function(index, value) {
        let idCheck = `#chk_${value}`
        let itemMarcado = $(idCheck).prop('checked')

        // Se o item marcado for igual ao selecionado, entra no condicional
        if (itemMarcado) {
            let data = $(idCheck).data()

            // Se for marcador a vigência
            if (data.textOption == 'Vigência') {

                acaoEsconderExibir(itemMarcado, ".areaVigencia")
                //acaoEsconderExibir(itemMarcado, ".quantitativo_renovado_vigencia");
                analisarCampoQuantitativoEdit(".quantitativo_renovado_vigencia input[type='radio']");
            }

            // Se for marcador os valores registrados
            if (data.textOption == 'Valor(es) registrado(s)') {
                valorRegistradoMarcado = itemMarcado
                acaoEsconderExibir(itemMarcado, ".areaValoresRegistrados")

                // Recupera os valores lançados e converte para array
                let itemValorAlterado =  $('input[name="item_valor_alterado"]').val();
                itemValorAlterado = JSON.parse(itemValorAlterado)

                $.each(itemValorAlterado, function(index, value) {
                    $(`input[name="novo_valor_unitario[${index}]"]`).val(value)
                })

                $('.inputValorRegistrado').mask('000.000.000.000.000,0000', {reverse: true});
                acaoEsconderExibir(itemMarcado, ".novovalor")
                acaoEsconderExibir(itemMarcado, ".novovalordesconto")
                acaoEsconderExibir(itemMarcado, ".percentual-antigo")
                acaoEsconderExibir(false, ".itemcancelado")
                itemCarregado = true
                exibirColunaValorRegistrado = true
            }


            // Se for marcador os itens cancelado
            if (data.textOption == 'Cancelamento de item(ns)') {
                acaoEsconderExibir(itemMarcado, ".areaValoresRegistrados")

                // Recupera os valores lançados e converte para array
                let itemValorAlterado =  $('input[name="item_cancelado"]').val();
                // itemValorAlterado = JSON.parse(itemValorAlterado)

                // $.each(itemValorAlterado, function(index, value) {
                //     $(`input[name="item_cancelado[${index}]"]`).prop('checked', 'checked');
                // })
                acaoEsconderExibir(itemMarcado, ".itemcancelado")
                if (!valorRegistradoMarcado) {
                    acaoEsconderExibir(false, ".novovalor")
                    acaoEsconderExibir(false, ".novovalordesconto")
                    acaoEsconderExibir(false, ".percentual-antigo")
                }

                acaoEsconderExibir(true, ".justificativa_motivo_cancelamento")
         //       acaoEsconderExibir(false, ".quantitativoRenovadoVigencia")

                itemCarregado = true
                exibirColunaItemCancelado = true
            }

            // Se for marcador o informativo
            if (data.textOption == 'Informativo') {
                acaoEsconderExibir(itemMarcado, ".areaInformativo")
            }
        }
    });
}







var bloquearSubmit = true

//Sobreescreveu o metodo para fazer a validacao nas datas
$(".button-custom").click(function(e) {
    e.preventDefault()
    bloquearSubmit = false

    let id = $(this).attr("id")
    let nameHidden =  $(`#${id} span`).data("namecustom")
    let valueHidden =  $(`#${id} span`).data("valuecustom")

    let campoObrigatorioObjetoAlteracao = false
    let campoObrigatoriojustificativaAlteracao = false
    let campoObrigatorioAmparoAlteracao = false
    // retira todos os campos obrigatórios
    $("#arquivo_alteracao").prop('required',false);
    $("#descricao_anexo").prop('required',false);
    $("#objeto_alteracao").prop('required',false);
    $("#justificativa_motivo_cancelamento").prop('required',false);
    $("#quantitativo_renovado_vigencia").prop('required',false);
    $("#amparo_legal_renovacao_quantitativo").prop('required',false);
    $("#data_assinatura_alteracao_vigencia").prop('required',false);
    $("#justificativa_alteracao_vigencia").prop('required',false);

    // recupera o que tiver marcado pelo usuário
    atLeastOneIsChecked = recuperarMarcacoesUsuario()

    if (atLeastOneIsChecked.length == 0) {
        exibirAlertaNoty('error-custom', 'Selecione ao menos um tipo de alteração')
        bloquearSubmit = true
        return
    }

    // Recupera todas as informações marcadas pelo usuário
    $.each(atLeastOneIsChecked, function(index, value) {
        let idCheck = `#chk_${value}`
        let itemMarcado = $(idCheck).prop('checked')

        // Se o item marcado for igual ao selecionado, entra no condicional
        if (itemMarcado) {
            let data = $(idCheck).data()
            if (data.textOption == 'Vigência') {
                let opcaoSelecionada = $("input[name='quantitativo_renovado_vigencia']:checked").val();
               // campoObrigatorioAmparoAlteracao = true;
                let dataAlteracaoFim = $("#data_assinatura_alteracao_fim_vigencia").val();
                if (opcaoSelecionada == 1 || opcaoSelecionada == undefined) {
                    campoObrigatorioAmparoAlteracao = true;
                }

                if (dataAlteracaoFim == "") {
                    exibirAlertaNoty('error-custom', 'Preencha o campo Data fim da vigência')
                    bloquearSubmit = true
                    return
                }
                $("#justificativa_alteracao_vigencia").prop('required',true);
            }

            if (data.textOption == 'Valor(es) registrado(s)') {
                let $j_object = $(".inputValorRegistrado");
                let contadorPreenchido = false;
                let contadorNaoPreenchido = false;
                let criterioJulgamento = $('input.inputValorRegistrado').data('criterio-julgamento');

                $j_object.each( function(i){
                    if( $j_object.eq(i).val() == "")  {
                        contadorNaoPreenchido = true
                    }
                    if( $j_object.eq(i).val() != "")  {
                        contadorPreenchido = true
                    }
                });

                let $desconto = $(".inputValorDesconto");
                let $valorUnit = $(".inputValorRegistrado");
                let contadorPreenchidoDesconto = false;
                let contadorNaoPreenchidoDesconto = false;
                let contadorPreenchidoUnitario = false;
                $desconto.each( function(i){
                    if( $desconto.eq(i).val() == "")  {
                        contadorNaoPreenchidoDesconto = true
                    }
                    if( $j_object.eq(i).val() != "")  {
                        contadorPreenchidoDesconto = true
                    }
                });
                $valorUnit.each( function(i){
                    if( $valorUnit.eq(i).val() == "")  {
                        contadorNaoPreenchidoUnitario = true
                    }
                    if( $j_object.eq(i).val() != "")  {
                        contadorPreenchidoUnitario = true
                    }
                });


                if ((contadorNaoPreenchido && !contadorPreenchido) &&
                    (contadorNaoPreenchidoUnitario && !contadorPreenchidoUnitario)
                    && (!criterioJulgamento || criterioJulgamento === "" || criterioJulgamento !== 'D')) {
                    exibirAlertaNoty('error-custom', 'Preencha o campo Novo valor unitário');
                    bloquearSubmit = true;
                    return
                }
                if ((contadorNaoPreenchido && !contadorPreenchido)&&(contadorNaoPreenchidoDesconto && !contadorPreenchidoDesconto)) {
                    exibirAlertaNoty('error-custom', 'Preencha o campo Novo valor unitário ou percentual de desconto')
                    bloquearSubmit = true
                    return
                }

            }




            if (data.textOption == 'Cancelamento de item(ns)') {
                let $j_object = $(".itemcanceladoclass");
                // $("#justificativa_motivo_cancelamento").prop('required',true);

                let contadorPreenchido = false ;
                let contadorNaoPreenchido = false ;
                campoObrigatoriojustificativaAlteracao = true
                $('input:checkbox.itemcanceladoclass').each(function () {
                    if( !this.checked )  {
                        contadorNaoPreenchido = true
                    }

                    if( this.checked)  {
                        contadorPreenchido = true
                    }
                });


                if (contadorNaoPreenchido && !contadorPreenchido ) {
                    exibirAlertaNoty('error-custom', 'Marque ao menos um item cancelado')
                    bloquearSubmit = true
                    return
                }
            }

            if (data.textOption == 'Informativo') {
                // $("#objeto_alteracao").prop('required',true);
                campoObrigatorioObjetoAlteracao = true
            }

            if (data.textOption == 'Fornecedor') {
                let $j_object = $(".inputcnpjfornecedornovo");

                let contadorPreenchidoCnpjFornecedor = false;
                let contadorNaoPreenchidoCnpjFornecedor = false;
                $j_object.each( function(i){
                    if( $j_object.eq(i).val() == "")  {
                        contadorNaoPreenchidoCnpjFornecedor = true
                    }
                    if( $j_object.eq(i).val() != "")  {
                        contadorPreenchidoCnpjFornecedor = true
                    }
                });

                if ($j_object.length === 0) {
                    exibirAlertaNoty('error-custom', 'Não existe informação do fornecedor para alteração')
                    bloquearSubmit = true
                    return
                }

                $j_object = $(".inputnomefornecedornovo");
                contadorPreenchido = false;
                contadorNaoPreenchido = false;

                $j_object.each( function(i){
                    if( $j_object.eq(i).val() == "")  {
                        contadorNaoPreenchido = true
                    }
                    if( $j_object.eq(i).val() != "")  {
                        contadorPreenchido = true
                    }
                });

                if (contadorNaoPreenchidoCnpjFornecedor && !contadorPreenchidoCnpjFornecedor) {
                    exibirAlertaNoty('error-custom', 'Informação do fornecedor para alteração incompletas')
                    bloquearSubmit = true
                    return
                }
            }
        }
    })

    // Recupera a data informada pelo usuário
    let dataAssinaturaAlteracao = $("#data_assinatura_alteracao_vigencia").val()


    // Se for criar direto, sem ser rascunho
    if (valueHidden == 0) {
        $("#data_assinatura_alteracao_vigencia").prop('required',true);

        $("#descricao_anexo").prop('required',true);

        numberOfChecked = $('input:checkbox:checked').length;

        if (numberOfChecked == 0) {
            exibirAlertaNoty('error-custom', 'Selecione um tipo de alteração e preencha os demais campos')
            bloquearSubmit = true
            return;
        }

        let anexoJustificativa =  document.getElementById("arquivo_alteracao").files.length

        let arquivoSalvo = $("#arquivoSalvo").val()

        if( anexoJustificativa== 0 && (arquivoSalvo == "false" || arquivoSalvo == "")){
            $("#arquivo_alteracao").prop('required',true);
            exibirAlertaNoty('error-custom', `Selecione um anexo da alteração`)
            bloquearSubmit = true
            return ;
        }

        // Se não tiver sido selecionada a data de assinatura da alteração
        if (dataAssinaturaAlteracao == "") {
            exibirAlertaNoty('error-custom', `Preencha a data de assinatura`)
            bloquearSubmit = true
            return ;
        }

        // Se o Informativo estiver marcado e for Criar a Alteração, então insere o campo como obrigatório
        if (campoObrigatorioObjetoAlteracao) {
            $("#objeto_alteracao").prop('required',campoObrigatorioObjetoAlteracao);
        }
        if (campoObrigatorioAmparoAlteracao) {
            $("#amparo_legal_renovacao_quantitativo").prop('required',campoObrigatorioAmparoAlteracao);
        }


        if (campoObrigatoriojustificativaAlteracao) {
            $("#justificativa_motivo_cancelamento").prop('required',campoObrigatoriojustificativaAlteracao);
        }
    }

    // Formata para o padrão de comparação
    dataAssinaturaAlteracao = formatarDataComparacao(dataAssinaturaAlteracao)

    // Recupera a data limite de seleção
    let dataLimite = $("#data_limite_vigencia").val()
    dataLimiteComparacao = formatarDataComparacao(dataLimite)

    // Se a data de assinatura selecionada for maior, não pode deixar cadastrar
    if (dataAssinaturaAlteracao > dataLimiteComparacao ) {
        dataLimite = dataLimite.split('-').reverse().join('/');
        exibirAlertaNoty('error-custom', `A data de assinatura deve ser anterior a ${dataLimite}`)
        bloquearSubmit = true
        return ;
    }

    // Recupera a data inicial da ata
    let dataLimiteInicial = $("#data_limite_vigencia_inicial").val()
    dataLimiteInicialComparacao = formatarDataComparacao(dataLimiteInicial)

    // Se a data de assinatura for menor que a vigência inicial, não pode deixar passar
    if (dataAssinaturaAlteracao < dataLimiteInicialComparacao ) {
        dataLimiteInicial = dataLimiteInicial.split('-').reverse().join('/');
        exibirAlertaNoty('error-custom', `A data de assinatura deve ser posterior a ${dataLimiteInicial}`)
        bloquearSubmit = true
        return ;
    }

    let dataAlteracaoFimVigencia = $("#data_assinatura_alteracao_fim_vigencia").val()
    dataAlteracaoFimVigenciaComparacao = formatarDataComparacao(dataAlteracaoFimVigencia)

    // Se a data de assinatura for menor que a vigência inicial, não pode deixar passar
    if (dataAlteracaoFimVigenciaComparacao < dataLimiteInicialComparacao ) {
        dataLimiteInicial = dataLimiteInicial.split('-').reverse().join('/');
        exibirAlertaNoty('error-custom', `A data de vigência final deve ser posterior a ${dataLimiteInicial}`)
        bloquearSubmit = true
        return ;
    }


    // Se a data de assinatura for menor que a vigência inicial, não pode deixar passar
    if (dataAlteracaoFimVigenciaComparacao > dataLimiteComparacao ) {
        dataLimite = dataLimite.split('-').reverse().join('/');
        exibirAlertaNoty('error-custom', `A data de vigência final deve ser anterior a ${dataLimite}`)
        bloquearSubmit = true
        return ;
    }

    let formValidation = $(this).closest('form')[0]

    if(formValidation.checkValidity() && !bloquearSubmit) {
        bloquearSubmit = false
        $('#hidden_custom').attr('name',nameHidden);
        $('#hidden_custom').attr('value',valueHidden);

        var saveActions = $('#saveActions'),
            crudForm        = saveActions.parents('form'),
            saveActionField = $('[name="_save_action"]');

        crudForm.submit(function (event) {
            window.removeEventListener('beforeunload', preventUnload);
            $("button[type=submit]").prop('disabled', true);
        });

        crudForm.submit();
    }

    formValidation.forEach(function(item) {
        if (item.validationMessage != "") {
            if (item.name == 'descricao_anexo') {
                exibirAlertaNoty('error-custom', 'Preencha o campo Descrição do Anexo')
            }
            if (item.name == 'objeto_alteracao') {
                exibirAlertaNoty('error-custom', 'Preencha o campo Objeto da alteração')
            }
            if (item.name == 'amparo_legal_renovacao_quantitativo') {
                exibirAlertaNoty('error-custom', 'Preencha o campo Amparo legal da renovação de quantitativo')
            }
            if (item.name == 'justificativa_motivo_cancelamento') {
                exibirAlertaNoty('error-custom', 'Preencha o campo Justificativa/Motivo de cancelamento')
            }

            if (item.className == 'inputnomefornecedornovo') {
                exibirAlertaNoty('error-custom', 'Preencha o nome do fornecedor')
            }

            if (item.name == 'justificativa_alteracao_vigencia') {
                exibirAlertaNoty('error-custom', 'Preencha a Justificativa/Motivo de alteração de vigência')
            }
        }
    });


});

function preventUnload(event) {
    if (initData !== getFormData()) {
        // Cancel the event as stated by the standard.
        event.preventDefault();
        // Older browsers supported custom message
        event.returnValue = '';
    }
}

// Método responsável em marcar/desmarcar todos os itens para cancelar
$(".selectAll").on( "click", function(e) {
    let state = $(this).prop('checked')
    $(".itemcanceladoclass").each(function() {
        this.checked=state;
    });
});

$("#data_assinatura_alteracao_fim_vigencia").change(function(){

    // Recupera a data limite de seleção
    let dataLimite = $("#data_limite_vigencia").val()
    dataLimiteComparacao = formatarDataComparacao(dataLimite)

    let dataCampo = formatarDataComparacao($(this).val())

    if (dataCampo > dataLimiteComparacao) {
        let arrayData = dataLimite.split('-')
        let dataBr = `${arrayData[2]}/${arrayData[1]}/${arrayData[0]}`
        exibirAlertaNoty('error-custom', `A nova data fim da vigência não pode ser superior a ${dataBr}.`)

        let dataFormatada = `${arrayData[2]}/${arrayData[1]}/${arrayData[0]}`

        $("#data_assinatura_alteracao_fim_vigencia").val(dataFormatada).trigger()
    }
})

function exibirItemFornecedorAlteracao(idArp, url, executarAcao) {
    //Verifica se a página está no modo de edição
    var modoEdicao = currentLocation.pathname.includes("edit");
    acaoEsconderExibir(executarAcao, ".areaAlteracaoFornecedor")

    let idArpAlteracao = 0
    if (modoEdicao) {
        var urlEdicao = window.location.href;

        var idPattern = /\/(\d+)\/[^/]*$/;
        var match = urlEdicao.match(idPattern);
        idArpAlteracao = match[1]
    }

    if (executarAcao) {
        $.ajax({
            url: url,
            async: false,
            data: {idArp, idArpAlteracao},
            type:'POST',
        }).done(function (response) { // "data" é o que retorna do Ajax
            $("#tabelaAlteracaoFornecedor tbody").empty()

            $.each(response, function( index, dados ) {
                let nameInputNovoCnpjFornecedor = `novo_cnpj_fornecedor[${dados.id}]`
                let nameIdNovoCnpjFornecedor = `novo_cnpj_cnpj_fornecedor_${dados.id}`

                let nameInputNovoNomeFornecedor = `novo_nome_cnpj_fornecedor[${dados.id}]`
                let nameIdNovoNomeFornecedor = `novo_nome_cnpj_fornecedor_${dados.id}`


                let linha = `<tr>
                    <td style="vertical-align:middle !important">${dados.nomefornecedor}</td>
                    <td style="vertical-align:middle !important">${dados.numero}</td>
                    <td style="vertical-align:middle !important">${dados.descricaodetalhada}</td>
                    <td class="cnpjfornecedornovo" style="vertical-align:middle !important">`

                if (!dados.item_alterado_fornecedor && !dados.item_utilizado_fornecedor) {
                    linha +=`
                        <div class="br-input">
                            <input type="text"                      
                                id="${nameIdNovoCnpjFornecedor}"
                                data-id="${dados.id}"
                                class="inputcnpjfornecedornovo"
                                name="${nameInputNovoCnpjFornecedor}"
                                placeholder="Insira o CPF/CNPJ do fornecedor"
                                data-url = "${dados.urlpesquisarcnpj}"
                                data-compra-item-id = "${dados.compra_item_id}"
                                onblur="pesquisarCnpjFornecedorAlteracao(this)"
                                value="${dados.novo_cnpj ?? ''}"/>
                        </div>`
                }

                if (dados.item_alterado_fornecedor) {
                    linha +=` Somente uma alteração de fornecedor por item pode ser realizada`
                }

                if (dados.item_utilizado_fornecedor) {
                    if (dados.item_adesao_remanejamento_pendente) {
                        linha +=` Existe(m) remanejamento ou solicitação de adesão pendente(s) de análise para esta ata, 
                        não permitindo alteração de fornecedor`
                    } else {
                        linha +=` Existe(m) minuta, empenho, contrato, remanejamento ou 
                        solicitação de adesão para esta ata, não permitindo alteração de fornecedor`
                    }
                }

                linha +=`
                    </td>         
                    <td class="nomefornecedornovo" style="vertical-align:middle !important">`

                if (!dados.item_alterado_fornecedor && !dados.item_utilizado_fornecedor) {
                    let nomeFornecedor = dados.novo_nome ?? ''
                    let disabled = `disabled`

                    if (dados.fornecedor_novo == true) {
                        disabled = ''
                    }

                    linha += `
                             <div class="br-textarea">
                                <textarea
                                    name="${nameInputNovoNomeFornecedor}"
                                    id="${nameIdNovoNomeFornecedor}"
                                    class="inputnomefornecedornovo"
                                    placeholder="Insira o nome do fornecedor"
                                    style="height: 83px;"
                                    data-id="${dados.id}"
                                    ${disabled}>${nomeFornecedor}</textarea>
                            </div>`
                }

                if (dados.item_alterado_fornecedor) {
                    linha +=` Somente uma alteração de fornecedor por item pode ser realizada`
                }

                linha +=`
                    </td>
                </tr>`

                $("#tabelaAlteracaoFornecedor tbody").append(linha)
                mascaraCnpjAlteracaoFornecedor(nameIdNovoCnpjFornecedor, dados.novo_cnpj)
            })
        });

        return
    }

    if (!exibirColunaValorRegistrado && !exibirColunaItemCancelado) {
        acaoEsconderExibir(false, ".areaValoresRegistrados")
        $("#tabelaValoresRegistrados tbody").empty()
    }
}

$( document ).ready(function($) {
    mascaraCnpjAlteracaoFornecedor()
});

function mascaraCnpjAlteracaoFornecedor(idCampo = null, cpfCnpj = null) {
    const cpfCnpjOptions = {
        onKeyPress: function(val, e, field, options) {
            var typed = val.replace(/\D/g, '');
            if (typed.length <= 11) {
                field.mask("000.000.000-000", options);
            }

            if (typed.length > 11) {
                field.mask("00.000.000/0000-00", options);
            }
        }
    };

    let cpfCnpjSomenteNumero = 0

    if(cpfCnpj != null) {
        cpfCnpjSomenteNumero = cpfCnpj.replace(/\D/g, '');
    }

    if (cpfCnpjSomenteNumero.length <= 11) {
        $(`#${idCampo}`).mask("000.000.000-000", cpfCnpjOptions);
        return ;
    }

    $(`#${idCampo}`).mask("00.000.000/0000-00", cpfCnpjOptions);
}

function pesquisarCnpjFornecedorAlteracao(campo) {
    var typed = campo.value.replace(/\D/g, '');

    if (typed.length < 11) {
        exibirAlertaNoty('error-custom', 'Para buscar o CPF, digitar no mínimo 11 dígitos')
        campo.value = ''
        $(`#novo_nome_cnpj_fornecedor_${campo.dataset.id}`).val('')
        return
    }

    if (typed.length > 11 && typed.length < 14) {
        exibirAlertaNoty('error-custom', 'Para buscar o CNPJ, digitar no mínimo 15 dígitos')
        campo.value = ''
        $(`#novo_nome_cnpj_fornecedor_${campo.dataset.id}`).val('')
        return
    }

    $.ajax({
        url: campo.dataset.url,
        async: false,
        data: {cnpj : campo.value, compraItemId: campo.dataset.compraItemId},
        type:'POST',
    }).done(function (response) {
        console.log(response)
        let bloquearCampoAlteracao = false
        let campoObrigatorio = false


        if (!response.sucesso) {
            exibirAlertaNoty('error-custom',response.mensagem)
           $(`#novo_cnpj_cnpj_fornecedor_${campo.dataset.id}`).val('')
            return
        }

        if (response.mensagem == '') {
            exibirAlertaNoty(
                'info-custom',
                'A informação não existe na base de dados, ao finalizar a alteração o fornecedor será cadastrado.'
            )
            campoObrigatorio = true
        }

        if (response.mensagem != '') {
            bloquearCampoAlteracao = true
            exibirAlertaNoty(
                'info-custom',
                `O fornecedor desse item será alterado para o ${response.mensagem}`
            )
            $(`#novo_nome_cnpj_fornecedor_${campo.dataset.id}`).val(response.mensagem)
        }

        $(`#novo_nome_cnpj_fornecedor_${campo.dataset.id}`).attr('disabled', bloquearCampoAlteracao)
        $(`#novo_nome_cnpj_fornecedor_${campo.dataset.id}`).attr('required', campoObrigatorio)
    })
}



