
$(document).ready(function() {
    $('#modalidades').select2({
        multiple: true
    });

    $(`#unidades_gerenciadoras`).select2({
        ajax: {
            url: '/arp/ajax-unidade-options',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        },
        multiple: true
    });

    $(`#orgao_gerenciador`).select2({

        ajax: {
            url: '/arp/ajax-orgaos-options',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

    $(`#unidades_participante`).select2({

        ajax: {
            url: '/arp/ajax-unidade-options',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }

    });

    $(`#orgao_participante`).select2({

        ajax: {
            url: '/arp/ajax-orgaos-options',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

    $(`#unidades_federacao`).select2({

        ajax: {
            url: '/arp/ajax-estados-options',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });
    $(`#fornecedor_ata`).select2({
        ajax: {
            url: '/arp/ajax-fornecedor-options',
            dataType: 'json',
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        },
        multiple: true
    });
});