function alterarEstadoJustificativa(status, numero, id) {
    $(`input[name="quantidade_aprovada[${id}]"]`).attr('readonly', true);
    if(status == 'Aceitar Parcialmente') {
        $(`#textarea-${id}`).val('');
        $(`input[name="quantidade_aprovada[${id}]"]`).attr('readonly', false);
    }

    if(status == 'Negar') {
        $(`#textarea-${id}`).val('');
        $(`#textarea-${id}`).prop('disabled', false);
        $(`#textarea-${id}`).prop('required',true);
        $(`input[name="quantidade_aprovada[${id}]"]`).prop('required',false);
        $(`input[name="quantidade_aprovada[${id}]"]`).val(null);
        return
    }

    if(status != 'Aceitar') {
        $(`#textarea-${id}`).val('');
        $(`#textarea-${id}`).prop('disabled', false);
        $(`#textarea-${id}`).prop('required',true);
        $(`input[name="quantidade_aprovada[${id}]"]`).prop('required',true);
        return
    }
    $(`#textarea-${id}`).val('');

    $(`#textarea-${id}`).prop('required',false);
    $(`#textarea-${id}`).prop('disabled', true);
    $(`input[name="quantidade_aprovada[${id}]"]`).prop('required',false);
    $(`input[name="quantidade_aprovada[${id}]"]`).val(null);
}

$("#modalexecucaodescentralizadaprogramaprojetofedal").addClass("active");