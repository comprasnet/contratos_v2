document.addEventListener("DOMContentLoaded", function () {

    const listArpElement = document.querySelector('.list-arp-dashboard');
    const unidade = listArpElement.dataset.unidade;
    const pagination = new LaravelPaginate('#pagination-arp-dashboard');
    const model = document.getElementById('item-model');
    let search = '';
    let curPage = 1;

    pagination.onChangePage = (currentPage) => {
        getPaginationArp(currentPage);
        curPage = currentPage;
    }

    const debounce = (func, wait) => {
        let timeout;

        return function executedFunction(...args) {
            const later = () => {
                clearTimeout(timeout);
                func(...args);
            };

            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
        };
    };

    const onSearch = () => {
        const executeSearch = (e) => {
            let value = e.target.value;
            search = value;
            getPaginationArp(1);
        }
        const returnedFunction = debounce(function (e) {
            executeSearch(e);
        }, 1000);
        let elementSearch = document.getElementById('search-ata');
        elementSearch.addEventListener("keyup", returnedFunction);
        elementSearch.addEventListener('search', (e) => {
            executeSearch(e);
        })
    }
    const loadingShow = () => {
        document.querySelector('.loading-table').removeAttribute('hidden');
        document.querySelector('.list-arp-dashboard').classList.add('opacity');
    }

    const loadingHide = () => {
        document.querySelector('.loading-table').setAttribute('hidden', 'true');
        document.querySelector('.list-arp-dashboard').classList.remove('opacity')
    }

    const getPaginationArp = (page = 1) => {
        loadingShow();
        if (search !== "") {
            search = "&filter-ata=" + search;
        }
        let searchLocation = location.search.substring(1);
        if (searchLocation !== "") {
            search = search + "&" + searchLocation;
        }

        // É necessário selecionar a rota (transparência ou não)
        // para considerar ou não a autenticação e consequentemente, os filtros (unidade, etc.)
        let rota = window.location.href.includes('transparencia') ? 'transparencia/' : '';        
        
        // Busca a relação de atas de registro de preço
        fetch(rota + "dashboard/list-arp?page=" + page + search).then((response) => {
            response.json().then((json) => {
                pagination.setPaginator(json).draw();
                mount(json);
                loadingHide();
            });
        });
    }

    const mount = (arpJson) => {
        const dataJson = arpJson.data;
        listArpElement.innerHTML = '';
        for (let jsonItem of dataJson) {
            listArpElement.append(mountItem(jsonItem));
        }
    }

    const mountItem = (jsonItem) => {
        let divListItem = document.createElement('div');
        divListItem.classList.add('br-item');
        divListItem.setAttribute('role', 'listitem');

        divListItem.innerHTML = model.innerHTML;

        const aElement = divListItem.querySelector('a');
        const pElement = aElement.querySelector('p');
        const spanElement = aElement.querySelector('span');

        const statusElement = divListItem.querySelector('.arp-status');

        let linkRouting = '/transparencia/arpshow/' + jsonItem.id + '/show';
        let target = false;
        if (unidade === jsonItem.unidade_origem_id.toString()) {
            linkRouting = '/arp/' + jsonItem.id + '/show';
            target = '_blank'
        }

        aElement.setAttribute('href', linkRouting);
        aElement.setAttribute('target', target);

        pElement.innerText = "Ata de Registro de Preços nº " + jsonItem.numero + "/" + jsonItem.ano;
        spanElement.innerText = "Unidade Gerenciadora " + jsonItem.unidade;

        let status = "Não Vigente";
        let vigenciaInicial = new Date(jsonItem.vigencia_inicial);
        let vigenciaFinal = new Date(jsonItem.vigencia_final);
        let today = new Date();

        if (today >= vigenciaInicial  &&  today <= vigenciaFinal) {
            status = 'Vigente';
        }

        if (jsonItem.situacao_ata != 'Ata de Registro de Preços') {
            status = 'Não Vigente';
        }

        statusElement.innerText = status;
        return divListItem;
    }

    getPaginationArp();
    onSearch();

})
5

18

30