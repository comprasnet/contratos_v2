$.fn.decimal = function () {
    $(this).on("keyup", function (e) {
        let charItem = String.fromCharCode(e.which);
        let val = $(this).val()

        val = $.decimalBrFormater(val, 17);
        $(this).val(val);
    })
}
$.fn.integer = function () {
    $(this).on("change keyup keydown", function () {
        let val = $(this).val();
        val = $.integerFormater(val);
        $(this).val(val);
    })
}
$.decimalBrFormater = function (number, max = 2) {
    let onlyNumber = number.replace(/[^0-9,]/g, '');
    let splited = onlyNumber.split(',');
    let integerNumber = parseInt(splited[0]);

    let newNumber = integerNumber.toLocaleString("pt-BR");
    if(isNaN(integerNumber)){
        newNumber = '';
    }

    if (splited[1] !== undefined) {
        newNumber += ',' + $.integerFormater(splited[1]).slice(0, max);
    }

    return newNumber;
}
$.integerFormater = function (numero) {
    return numero.replace(/[^0-9]/g, '');
}

$.floatRound = function (numero, maxRoundDigits = 3) {
    if (typeof numero === 'string' || numero instanceof String) {
        numero = $.ptBRToFloat(numero)
    }
    return +(Math.floor(numero + ('e+' + maxRoundDigits)) + ('e-' + maxRoundDigits));

}
$.floatBrRound = function (numero, digitos = 2, maxRoundDigits = 3) {
    return $.floatRound(numero,maxRoundDigits).toLocaleString("pt-BR", {maximumFractionDigits: digitos, minimumFractionDigits: digitos});
}

$.ptBRToFloat = function (valorFormatado) {
    if(valorFormatado === "" || valorFormatado === undefined) {
        return 0;
    }
    return parseFloat(valorFormatado.replace(/\./g, '').replace(',', '.'));
}