document.addEventListener("DOMContentLoaded", function () {

    let nameDashboard = 'ata-fornecedor';
    let urlDashboard = 'preposto/atas';
    const listArpElement = document.querySelector(`.list-${nameDashboard}-dashboard`);
    const pagination = new LaravelPaginate(`#pagination-${nameDashboard}-dashboard`);
    const model = document.getElementById('item-model');
    let search = '';
    let curPage = 1;

    pagination.onChangePage = (currentPage) => {
        getPagination(currentPage);
        curPage = currentPage;
    }

    const debounce = (func, wait) => {
        let timeout;

        return function executedFunction(...args) {
            const later = () => {
                clearTimeout(timeout);
                func(...args);
            };

            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
        };
    };

    const onSearch = () => {
        const executeSearch = (e) => {
            let value = e.target.value;
            search = value;
            getPagination(1);
        }
        const returnedFunction = debounce(function (e) {
            executeSearch(e);
        }, 1000);
        let elementSearch = document.getElementById(`search-${nameDashboard}`);
        elementSearch.addEventListener("keyup", returnedFunction);
        elementSearch.addEventListener('search', (e) => {
            executeSearch(e);
        })
    }
    const loadingShow = () => {
        document.querySelector(`.loading-table-${nameDashboard}`).removeAttribute('hidden');
        document.querySelector(`.list-${nameDashboard}-dashboard`).classList.add('opacity');
    }

    const loadingHide = () => {
        document.querySelector(`.loading-table-${nameDashboard}`).setAttribute('hidden', 'true');
        document.querySelector(`.list-${nameDashboard}-dashboard`).classList.remove('opacity');
    }

    const getPagination = (page = 1) => {
        loadingShow();
        if (search !== "") {
            search = "&filter-dashboard=" + search;
        }
        let searchLocation = location.search.substring(1);
        if (searchLocation !== "") {
            search = search + "&" + searchLocation;
        }

        // Busca a relação de atas de registro de preço
        fetch(  `${urlDashboard}?page=` + page + search).then((response) => {
            response.json().then((json) => {
                pagination.setPaginator(json).draw();
                mount(json);
                loadingHide();
            });
        });
    }

    const mount = (arpJson) => {
        const dataJson = arpJson.data;
        console.log(dataJson);
        listArpElement.innerHTML = '';
        for (let jsonItem of dataJson) {
            listArpElement.append(mountItem(jsonItem));
        }
    }

    const formataDataBr = (dateString) => {
        const date = new Date(dateString);
        const day = String(date.getDate()).padStart(2, '0');
        const month = String(date.getMonth() + 1).padStart(2, '0'); // Mês de 0-11 para 1-12
        const year = date.getFullYear();
        return `${day}-${month}-${year}`;
    };


    const mountItem = (jsonItem) => {
        let divListItem = document.createElement('div');
        divListItem.classList.add('br-item');
        divListItem.setAttribute('role', 'listitem');

        divListItem.innerHTML = model.innerHTML;

        const aElement = divListItem.querySelector('a');
        const pElement = aElement.querySelector('p');
        const spanElement = aElement.querySelector('span');

        linkRouting = '/fornecedor/arp/' + jsonItem.id + '/show'; //erro 333

        aElement.setAttribute('href', linkRouting);

        spanElement.innerText = "Ata nº " + jsonItem.numero_ano_ata;
        pElement.innerText = "Unidade " + jsonItem.unidade_gerenciadora_da_ata;


        // Cria um elemento expansível para o campo objeto
        let objetoElement = document.createElement('div');

        objetoElement.classList.add('expandido');

        let textoCompleto = jsonItem.objeto
        let textoCurto = textoCompleto.substring(0, 70) + (textoCompleto.length > 70 ? '...' : '');

        objetoElement.innerHTML = `<span class="expansivel-texto"></span>`;

        const textoElement = objetoElement.querySelector('.expansivel-texto');
        textoElement.innerHTML = `<i class="fas fa-angle-down mr-2"></i>${textoCurto}`;

        objetoElement.addEventListener('click', function(event) {
            event.preventDefault(); // Impede que o link seja ativado
            event.stopPropagation(); // Previne a propagação do evento para o pai
            const iconElement = this.querySelector('i');

            if (iconElement) {
                if (iconElement.classList[1] == 'fa-angle-up') {
                    textoElement.innerHTML = `<i class="fas fa-angle-down mr-2"></i>${textoCurto}`;
                } else {
                    textoElement.innerHTML = `<i class="fas fa-angle-up mr-2"></i>${textoCompleto}`;
                }
            }
        });

        // Cria um novo elemento para vigencia_fim
        let vigenciaFimElement = document.createElement('div');
        const vigenciaFinalBr = formataDataBr(jsonItem.vigencia_final);
        vigenciaFimElement.innerText = "Vencimento: " + vigenciaFinalBr;
        vigenciaFimElement.classList.add('vigencia-final'); // Adicione uma classe CSS para estilização se necessário
       /* vigenciaFimElement.classList.add('br-item');
        vigenciaFimElement.setAttribute('role', 'listitem');*/

        // Adiciona os elementos ao divListItem
        aElement.appendChild(objetoElement);
        divListItem.appendChild(aElement);
        divListItem.appendChild(vigenciaFimElement);

        return divListItem;
    }

    const css = `
.br-item {
  display: flex;
  justify-content: flex-start;
}
.br-item > * {
    flex: 1;
    min-width: 150px;
}

.expansivel {
    cursor: pointer;
    margin-top: 10px;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
}

.expansivel-texto {
    display: none;
    margin-top: 25px;
    padding-left: 20px;
    font-size: 0.9em;
    color: #666;
}

.expandido .expansivel-texto {
    display: block;
}

.expansivel-toggle {
    margin-left: 5px;
    position: absolute;
    top: 0;
}

.vigencia-final {
  text-align: right;
}
`;

// Criar um elemento <style> e adicionar ao <head>
    const styleElement = document.createElement('style');
    styleElement.textContent = css;
    document.head.appendChild(styleElement);

    getPagination();
    onSearch();

})
