function exibirPrepostos(contratoId, url, page = 1) {
    // Ativar a modal
    $("#modalprepostos").addClass("active");

    // Adicionar o ID do contrato ao botão
    console.log(contratoId);
    $("#btn_indicar_preposto").attr("data-contrato-id", contratoId);

    // Definir o título da modal
    $("#modalprepostos .br-modal-header").text("Prepostos");

    limparCampos();

    $.ajax({
        type: 'GET',
        url: `${url}/${contratoId}?page=${page}`,
        success: function (response) {
            if (response.status === 'no_prepostos') {
                // Exibir mensagem de que não há prepostos
                let mensagem = `
                    <tr>
                        <td colspan="4" class="fontepequena text-center">
                            ${response.message}
                        </td>
                    </tr>
                `;
                $("#prepostos_tbody").html(mensagem);
            } else if (response.status === 'success') {
                // Preencher a tabela com os prepostos
                let prepostosHtml = `
                    <thead>
                        <tr>
                            <th class="text-bold">Nome</th>
                            <th class="text-bold">CPF</th>
                            <th class="text-bold">Situação</th>
                            <th class="text-bold">Período</th>
                        </tr>
                    </thead>
                    <tbody>`;
                response.data.forEach(function (preposto) {
                    prepostosHtml += `
                        <tr>
                            <td class="fontepequena">${preposto.nome}</td>
                            <td class="fontepequena">${preposto.cpf}</td>
                            <td class="fontepequena">${preposto.situacao}</td>
                            <td class="fontepequena">${preposto.periodo}</td>
                        </tr>
                    `;
                });
                prepostosHtml += `</tbody>`;
                $("#prepostos_tbody").html(prepostosHtml);
                renderizarPaginacao(response.pagination, contratoId, url);
            }
        },
        error: function () {
            exibirAlertaNoty('error-custom', 'Erro ao carregar as informações');
        }
    });
}

// Botão para indicar preposto
$("#btn_indicar_preposto").click(function () {
    const contratoId = $(this).data("contrato-id");
    const indicarPrepostoRoute = `/fornecedor/contrato/${contratoId}/prepostos/create`;
    window.location.href = indicarPrepostoRoute;
});

// Fechar a modal ao clicar no botão "Voltar"
$("#btn_fechar_modal_preposto").click(function() {
    $("#modalprepostos").removeClass("active");
});

// Função para limpar os campos antes de carregar novos dados
function limparCampos() {
    let carregando = `<tr><td colspan="4" class="text-center">Carregando...</td></tr>`;
    $("#prepostos_tbody").html(carregando);
    $("#pagination").html("");
}

// Função para renderizar a paginação
function renderizarPaginacao(pagination, contratoId, url) {
    const { current_page, last_page } = pagination;
    //console.log(typeof pagination.total);
    let paginationHtml = `<div class="pagination text-center mt-3">`;

    if (current_page > 1) {
        paginationHtml += `
            <button class="prev-page btn btn-primary btn-sm mx-1" onclick="exibirPrepostos(${contratoId}, '${url}', ${current_page - 1})">
                Anterior
            </button>`;
    }

    if (current_page < last_page) {
        paginationHtml += `
            <button class="next-page btn btn-primary btn-sm mx-1" onclick="exibirPrepostos(${contratoId}, '${url}', ${current_page + 1})">
                Próximo
            </button>`;
    }

    paginationHtml += `</div>`;
    if(pagination.total > 5) {
        $("#pagination").html(paginationHtml);
    }
}
