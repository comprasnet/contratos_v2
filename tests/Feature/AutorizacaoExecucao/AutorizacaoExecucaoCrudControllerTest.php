<?php

namespace AutorizacaoExecucao;

use App\Http\Traits\BuscaCodigoItens;
use App\Models\BackpackUser;
use App\Models\Contratohistorico;
use App\Models\Contratoitem;
use App\Models\Contratoresponsavel;
use App\Models\Saldohistoricoitem;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class AutorizacaoExecucaoCrudControllerTest extends TestCase
{
    use BuscaCodigoItens;

    protected function setUp(): void
    {
        parent::setUp();

        DB::beginTransaction();

        // Administrador
        $this->usuario = BackpackUser::find(User::factory()->create()->id);
        $this->usuario->roles()->attach(1);

        $this->actingAs($this->usuario);
    }

    protected function tearDown(): void
    {
        DB::rollBack();

        parent::tearDown();
    }

    public function testErroValidacaoSaldoHistoricoItemPorPeriodo()
    {
        $contratoResponsavel = Contratoresponsavel::factory()->create([
            'user_id' => $this->usuario->id
        ]);

        $response = $this->get(
            route(
                'autorizacaoexecucao.saldoHistoricoItemPorPeriodo',
                [
                    'contrato_id' => $contratoResponsavel->contrato_id,
                    'id' => 0,
                    'vigencia_inicio' => '2021-01-01',
                    'vigencia_fim' => '2021-12-31',
                ]
            )
        );

        $response->assertSessionHasErrors(['id'])
            ->assertStatus(302);
    }

    public function testGetSaldoHistoricoItemPorPeriodo()
    {
        $contratoResponsavel = Contratoresponsavel::factory()->create([
            'user_id' => $this->usuario->id
        ]);
        $contratoItem = Contratoitem::create([
            'contrato_id' => $contratoResponsavel->contrato_id,
            'tipo_id' => 1,
            'grupo_id' => 1,
            'catmatseritem_id' => 1,
            'quantidade' => 20,
            'valorunitario' => 1000,
            'valortotal' => 20000,
        ]);

        // primeiro periodo
        $contratoHistorico[] = Contratohistorico::create([
            'numero' => '00001/2021',
            'contrato_id' => $contratoResponsavel->contrato_id,
            'vigencia_inicio' => '2021-01-10',
            'vigencia_fim' => '2022-01-09',
        ]);
        $saldoHistoricoItens[] = Saldohistoricoitem::create([
            'saldoable_type' => Contratohistorico::class,
            'saldoable_id' => $contratoHistorico[0]->id,
            'contratoitem_id' => $contratoItem->id,
            'tiposaldo_id' => 1,
            'quantidade' => 10,
            'valorunitario' => 1000,
            'valortotal' => 10000,
            'numero_item_compra' => '00001'
        ]);
        // segundo periodo
        $contratoHistorico[] = Contratohistorico::create([
            'numero' => '00001/2022',
            'contrato_id' => $contratoResponsavel->contrato_id,
            'vigencia_inicio' => '2022-01-10',
            'vigencia_fim' => '2023-01-09',
        ]);
        $saldoHistoricoItens[] = Saldohistoricoitem::create([
            'saldoable_type' => Contratohistorico::class,
            'saldoable_id' => $contratoHistorico[1]->id,
            'contratoitem_id' => $contratoItem->id,
            'tiposaldo_id' => 1,
            'quantidade' => 10,
            'valorunitario' => 1000,
            'valortotal' => 10000,
        ]);
        // terceiro periodo
        $contratoHistorico[] = Contratohistorico::create([
            'numero' => '00001/2023',
            'contrato_id' => $contratoResponsavel->contrato_id,
            'vigencia_inicio' => '2023-01-10',
            'vigencia_fim' => '2024-01-09',
        ]);
        $saldoHistoricoItens[] = Saldohistoricoitem::create([
            'saldoable_type' => Contratohistorico::class,
            'saldoable_id' => $contratoHistorico[2]->id,
            'contratoitem_id' => $contratoItem->id,
            'tiposaldo_id' => 1,
            'quantidade' => 10,
            'valorunitario' => 1000,
            'valortotal' => 10000,
        ]);
        // quarto periodo
        $contratoHistorico[] = Contratohistorico::create([
            'numero' => '00001/2024',
            'contrato_id' => $contratoResponsavel->contrato_id,
            'vigencia_inicio' => '2024-01-10',
            'vigencia_fim' => '2025-01-09',
        ]);
        $saldoHistoricoItens[] = Saldohistoricoitem::create([
            'saldoable_type' => Contratohistorico::class,
            'saldoable_id' => $contratoHistorico[3]->id,
            'contratoitem_id' => $contratoItem->id,
            'tiposaldo_id' => 1,
            'quantidade' => 10,
            'valorunitario' => 1000,
            'valortotal' => 10000,
        ]);

        $response = $this->get(
            route(
                'autorizacaoexecucao.saldoHistoricoItemPorPeriodo',
                [
                    'contrato_id' => $contratoResponsavel->contrato_id,
                    'id' => $contratoItem->id,
                    'vigencia_inicio' => '2021-01-10',
                    'vigencia_fim' => '2024-12-31',
                ]
            )
        );

        $response->assertStatus(200);

        $response->assertJsonCount(4); // número de periodos

        // primeiro periodo
        $response->assertJsonFragment([
            "periodo" => "De: 10/01/2021 Até: 09/01/2022",
            "periodo_vigencia_inicio" => '2021-01-10',
            "periodo_vigencia_fim" => '2022-01-09',
            "contratohistorico_id" => $contratoHistorico[0]->id,
            "saldohistoricoitens_id" => $saldoHistoricoItens[0]->id,
        ]);
        // segundo periodo
        $response->assertJsonFragment([
            "periodo" => "De: 10/01/2022 Até: 09/01/2023",
            "periodo_vigencia_inicio" => '2022-01-10',
            "periodo_vigencia_fim" => '2023-01-09',
            "contratohistorico_id" => $contratoHistorico[1]->id,
            "saldohistoricoitens_id" => $saldoHistoricoItens[1]->id,
        ]);
        // terceiro periodo
        $response->assertJsonFragment([
            "periodo" => "De: 10/01/2023 Até: 09/01/2024",
            "periodo_vigencia_inicio" => '2023-01-10',
            "periodo_vigencia_fim" => '2024-01-09',
            "contratohistorico_id" => $contratoHistorico[2]->id,
            "saldohistoricoitens_id" => $saldoHistoricoItens[2]->id,
        ]);
        // quarto periodo
        $response->assertJsonFragment([
            "periodo" => "De: 10/01/2024 Até: 09/01/2025",
            "periodo_vigencia_inicio" => '2024-01-10',
            "periodo_vigencia_fim" => '2025-01-09',
            "contratohistorico_id" => $contratoHistorico[3]->id,
            "saldohistoricoitens_id" => $saldoHistoricoItens[3]->id,
        ]);
    }

    public function testUploadAnexoComArquivo()
    {
        $contratoResponsavel = Contratoresponsavel::factory()->create([
            'user_id' => $this->usuario->id
        ]);

        $response = $this->post(
            route(
                'autorizacaoexecucao.anexo',
                ['contrato_id' => $contratoResponsavel->contrato_id],
            ),
            ['upload_arquivo_anexo' => UploadedFile::fake()->create('anexo.pdf')]
        )->assertOk();

        $response->assertJson([
            'nome_arquivo_anexo' => 'anexo.pdf'
        ]);
    }

    public function testUploadAnexoSemArquivo()
    {
        $contratoResponsavel = Contratoresponsavel::factory()->create([
            'user_id' => $this->usuario->id
        ]);

        $this->post(
            route(
                'autorizacaoexecucao.anexo',
                ['contrato_id' => $contratoResponsavel->contrato_id],
            ),
            ['upload_arquivo_anexo' => null]
        )->assertStatus(302);
    }
}
