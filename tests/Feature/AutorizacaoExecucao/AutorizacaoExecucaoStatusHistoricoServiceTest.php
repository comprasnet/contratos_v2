<?php

namespace Tests\Feature\AutorizacaoExecucao;

use App\Models\AutorizacaoExecucao;
use App\Models\BackpackUser;
use App\Models\CodigoItem;
use App\Models\User;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoStatusHistoricoService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class AutorizacaoExecucaoStatusHistoricoServiceTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        DB::beginTransaction();

        $this->usuario = BackpackUser::find(User::factory()->create()->id);
        $this->actingAs($this->usuario);

        $this->autorizacaoexecucao = AutorizacaoExecucao::factory()->create();
        $this->aeStatusHistoricoService = new AutorizacaoExecucaoStatusHistoricoService();
    }

    protected function tearDown(): void
    {
        DB::rollBack();

        parent::tearDown();
    }

    public function testAddStatusInformarNoHistorico()
    {
        $statusInformar = CodigoItem::where('descres', 'ae_st_historico_1')->first();

        AutorizacaoExecucaoStatusHistoricoService::addStatusInformar($this->autorizacaoexecucao->id);

        $this->assertDatabaseHas(
            'autorizacaoexecucao_status_historico',
            [
                'autorizacaoexecucao_id' => $this->autorizacaoexecucao->id,
                'status_id' => $statusInformar->id,
                'usuario_id' => $this->usuario->id,
                'observacao' => null,
            ]
        );
    }

    public function testAddStatusEnviarAssinaturaNoHistorico()
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_2')->first();

        AutorizacaoExecucaoStatusHistoricoService::addStatusEnviarAssinatura($this->autorizacaoexecucao->id);

        $this->assertDatabaseHas(
            'autorizacaoexecucao_status_historico',
            [
                'autorizacaoexecucao_id' => $this->autorizacaoexecucao->id,
                'status_id' => $status->id,
                'usuario_id' => $this->usuario->id,
                'observacao' => null,
            ]
        );
    }

    public function testAddStatusAssinarNoHistorico()
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_3')->first();

        AutorizacaoExecucaoStatusHistoricoService::addStatusAssinar($this->autorizacaoexecucao->id);

        $this->assertDatabaseHas(
            'autorizacaoexecucao_status_historico',
            [
                'autorizacaoexecucao_id' => $this->autorizacaoexecucao->id,
                'status_id' => $status->id,
                'usuario_id' => $this->usuario->id,
                'observacao' => null,
            ]
        );
    }

    public function testAddStatusRecusarAssinaturaNoHistorico()
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_4')->first();

        AutorizacaoExecucaoStatusHistoricoService::addStatusRecusarAssinatura(
            $this->autorizacaoexecucao->id,
            'Motivo de recusa'
        );

        $this->assertDatabaseHas(
            'autorizacaoexecucao_status_historico',
            [
                'autorizacaoexecucao_id' => $this->autorizacaoexecucao->id,
                'status_id' => $status->id,
                'usuario_id' => $this->usuario->id,
                'observacao' => 'Motivo de recusa',
            ]
        );
    }

    public function testAddStatusEnviarAlteracaoAssinaturaNoHistorico()
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_8')->first();

        AutorizacaoExecucaoStatusHistoricoService::addStatusEnviarAlteracaoAssinatura(
            $this->autorizacaoexecucao->id,
            'Motivo de alteração'
        );

        $this->assertDatabaseHas(
            'autorizacaoexecucao_status_historico',
            [
                'autorizacaoexecucao_id' => $this->autorizacaoexecucao->id,
                'status_id' => $status->id,
                'usuario_id' => $this->usuario->id,
                'observacao' => 'Motivo de alteração',
            ]
        );
    }

    public function testAddStatusAssinarAlteracaoNoHistorico()
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_9')->first();

        AutorizacaoExecucaoStatusHistoricoService::addStatusAssinarAlteracao($this->autorizacaoexecucao->id);

        $this->assertDatabaseHas(
            'autorizacaoexecucao_status_historico',
            [
                'autorizacaoexecucao_id' => $this->autorizacaoexecucao->id,
                'status_id' => $status->id,
                'usuario_id' => $this->usuario->id,
                'observacao' => null,
            ]
        );
    }

    public function testAddStatusRecusarAssinaturaAlteracaoNoHistorico()
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_10')->first();

        AutorizacaoExecucaoStatusHistoricoService::addStatusRecusarAssinaturaAlteracao(
            $this->autorizacaoexecucao->id,
            'Motivo de recusa da assinatura da alteração'
        );

        $this->assertDatabaseHas(
            'autorizacaoexecucao_status_historico',
            [
                'autorizacaoexecucao_id' => $this->autorizacaoexecucao->id,
                'status_id' => $status->id,
                'usuario_id' => $this->usuario->id,
                'observacao' => 'Motivo de recusa da assinatura da alteração',
            ]
        );
    }

    public function testAddStatusAlteracaoNoHistorico()
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_5')->first();

        AutorizacaoExecucaoStatusHistoricoService::addStatusAlterar($this->autorizacaoexecucao->id);

        $this->assertDatabaseHas(
            'autorizacaoexecucao_status_historico',
            [
                'autorizacaoexecucao_id' => $this->autorizacaoexecucao->id,
                'status_id' => $status->id,
                'usuario_id' => $this->usuario->id,
            ]
        );
    }

    public function testAddStatusExtinguirNoHistorico()
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_6')->first();

        AutorizacaoExecucaoStatusHistoricoService::addStatusExtinguir($this->autorizacaoexecucao->id);

        $this->assertDatabaseHas(
            'autorizacaoexecucao_status_historico',
            [
                'autorizacaoexecucao_id' => $this->autorizacaoexecucao->id,
                'status_id' => $status->id,
                'usuario_id' => $this->usuario->id,
            ]
        );
    }

    public function testAddStatusRetificarNoHistorico()
    {
        $status = CodigoItem::where('descres', 'ae_st_historico_7')->first();

        AutorizacaoExecucaoStatusHistoricoService::addStatusRetificar(
            $this->autorizacaoexecucao->id,
            'Motivo de retificação'
        );

        $this->assertDatabaseHas(
            'autorizacaoexecucao_status_historico',
            [
                'autorizacaoexecucao_id' => $this->autorizacaoexecucao->id,
                'status_id' => $status->id,
                'usuario_id' => $this->usuario->id,
                'observacao' => 'Motivo de retificação',
            ]
        );
    }
}
