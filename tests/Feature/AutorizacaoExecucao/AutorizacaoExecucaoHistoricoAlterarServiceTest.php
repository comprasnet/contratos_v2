<?php

namespace Tests\Feature\AutorizacaoExecucao;

use App\Models\ArquivoGenerico;
use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoexecucaoHistorico;
use App\Models\AutorizacaoexecucaoItens;
use App\Models\AutorizacaoexecucaoItensHistorico;
use App\Models\CodigoItem;
use App\Models\Contrato;
use App\Models\Contratopreposto;
use App\Models\ModelSignatario;
use App\Models\Saldohistoricoitem;
use App\Models\User;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoHistoricoAlterarService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class AutorizacaoExecucaoHistoricoAlterarServiceTest extends TestCase
{
    /**
     * @var AutorizacaoExecucaoHistoricoAlterarService
     */
    private $aehService;

    /**
     * @return array
     */

    protected function setUp(): void
    {
        parent::setUp();

        DB::beginTransaction();

        $this->aehService = new AutorizacaoExecucaoHistoricoAlterarService(new AutorizacaoExecucao());

        $this->contrato1 = Contrato::factory()->create();

        // Cria outro contrato com alterações aguardando assinatura para certificar que somente o saldo do contrato 1
        // será validado pelo método getSaldoAlteradoDoSaldoHistoricoItensIds
        $contrato2 = Contrato::factory()->create();
        $this->gerarDadosSaldoHistoricoAlteracao(
            $contrato2->id,
            CodigoItem::where('descres', 'ae_status_5')->first()->id
        );
    }

    protected function tearDown(): void
    {
        DB::rollBack();

        parent::tearDown();
    }


    public function testIgnorarSaldoAlteradoParaAlteracoesEmElaboracao()
    {
        $this->gerarDadosSaldoHistoricoAlteracao(
            $this->contrato1->id,
            CodigoItem::where('descres', 'ae_status_1')->first()->id // Em Elaboração
        );

        $itensSaldoHistoricoAlterados = $this->aehService
            ->getSaldoAlteradoDoSaldoHistoricoItensIds($this->contrato1->id);

        $this->assertEmpty($itensSaldoHistoricoAlterados);
    }

    public function testRetornaSaldoAlteradoParaAlteracoesAguardandoAssinatura()
    {
        $saldoHistoricoItemIds = $this->gerarDadosSaldoHistoricoAlteracao(
            $this->contrato1->id,
            CodigoItem::where('descres', 'ae_status_5')->first()->id
        );

        $itensSaldoHistoricoAlterados = $this->aehService
            ->getSaldoAlteradoDoSaldoHistoricoItensIds($this->contrato1->id);

        $this->assertEquals(
            [
                ['saldohistoricoitens_id' => $saldoHistoricoItemIds[0], 'quantidade_total' => 10.0],
                ['saldohistoricoitens_id' => $saldoHistoricoItemIds[1], 'quantidade_total' => -20.0],
                ['saldohistoricoitens_id' => $saldoHistoricoItemIds[2], 'quantidade_total' => 40.0],
                ['saldohistoricoitens_id' => $saldoHistoricoItemIds[3], 'quantidade_total' => 30.0],
            ],
            $itensSaldoHistoricoAlterados
        );
    }

    public function testIgnorarSaldoAlteradoParaAlteracoesAssinaturaRecusada()
    {
        $this->gerarDadosSaldoHistoricoAlteracao(
            $this->contrato1->id,
            CodigoItem::where('descres', 'ae_status_6')->first()->id // Assinatura Recusada
        );

        $itensSaldoHistoricoAlterados = $this->aehService
            ->getSaldoAlteradoDoSaldoHistoricoItensIds($this->contrato1->id);

        $this->assertEmpty($itensSaldoHistoricoAlterados);
    }

    public function testGetSaldoAlteradoDoSaldoHistoricoIdsComAlteracoesAssinada()
    {
        $saldoHistoricoItemIds = $this->gerarDadosSaldoHistoricoAlteracao(
            $this->contrato1->id,
            CodigoItem::where('descres', 'ae_status_7')->first()->id // Assinada
        );

        $itensSaldoHistoricoAlterados = $this->aehService
            ->getSaldoAlteradoDoSaldoHistoricoItensIds($this->contrato1->id);

        $this->assertEquals(
            [
                ['saldohistoricoitens_id' => $saldoHistoricoItemIds[0], 'quantidade_total' => 10.0],
                ['saldohistoricoitens_id' => $saldoHistoricoItemIds[1], 'quantidade_total' => -20.0],
                ['saldohistoricoitens_id' => $saldoHistoricoItemIds[2], 'quantidade_total' => 40.0],
                ['saldohistoricoitens_id' => $saldoHistoricoItemIds[3], 'quantidade_total' => 30.0],
            ],
            $itensSaldoHistoricoAlterados
        );
    }

    public function testGetSaldoAlteradoDoSaldoHistoricoIdsComAlteracoesEmExecucao()
    {
        $this->gerarDadosSaldoHistoricoAlteracao(
            $this->contrato1->id,
            CodigoItem::where('descres', 'ae_status_2')->first()->id // Em execução
        );

        $itensSaldoHistoricoAlterados = $this->aehService
            ->getSaldoAlteradoDoSaldoHistoricoItensIds($this->contrato1->id);

        $this->assertEmpty($itensSaldoHistoricoAlterados);
    }

    public function testAlteraSituacaoDeAssinadoParaEmExecucaoEAplicaAlteracaoVigencia()
    {
        $autorizacoes = AutorizacaoExecucao::factory(2)->create([
            'data_vigencia_inicio' => Carbon::now()->subYear(),
            'data_vigencia_fim' => Carbon::now(),
        ]);

        $aeHistoricos = [];
        $autorizacoes->each(function ($autorizacao) use (&$aeHistoricos) {
            $aeHistoricos[] = AutorizacaoExecucaoHistorico::factory()->create([
                'autorizacaoexecucoes_id' => $autorizacao->id,
                'situacao_id' => CodigoItem::where('descres', 'ae_status_7')->first()->id,
                'data_inicio_alteracao' => Carbon::now()->subDay(),
                'tipo_alteracao' => 'vigencia',
                'data_vigencia_fim_antes' => $autorizacao->data_vigencia_fim,
                'data_vigencia_fim_depois' => Carbon::now()->addYear(),
                'usuario_responsavel_id' => User::factory()->create()->id
            ]);
        });


        $aeHistoricoAlterarService = new AutorizacaoExecucaoHistoricoAlterarService(new AutorizacaoExecucao());
        $aeHistoricoAlterarService->alteraSituacaoSeAtingirInicioAlteracao();

        $autorizacoes->each(function ($autorizacao) {
            $this->assertDatabaseHas(
                'autorizacaoexecucoes',
                ['id' => $autorizacao->id, 'data_vigencia_fim' => Carbon::now()->addYear()->format('Y-m-d')]
            );
        });

        foreach ($aeHistoricos as $aeHistorico) {
            $this->assertDatabaseHas(
                'autorizacaoexecucao_historico',
                ['id' => $aeHistorico->id, 'situacao_id' => CodigoItem::where('descres', 'ae_status_2')->first()->id]
            );

            $this->assertDatabaseHas(
                'autorizacaoexecucao_status_historico',
                [
                    'autorizacaoexecucao_id' => $aeHistorico->autorizacaoexecucoes_id,
                    'status_id' => CodigoItem::where('descres', 'ae_st_historico_5')->first()->id,
                    'usuario_id' => $aeHistorico->usuario_responsavel_id,
                    'observacao' => null,
                ]
            );
        }
    }

    public function testAlterarSituacaoDeAssinadoParaEmExecucaoEAplicaAlteracaoExtinguir()
    {
        $autorizacoes = AutorizacaoExecucao::factory(2)->create([
            'data_vigencia_inicio' => Carbon::now()->subYear(),
            'data_vigencia_fim' => Carbon::now(),
        ]);

        $aeHistoricos = [];
        $autorizacoes->each(function ($autorizacao) use (&$aeHistoricos) {
            $aeHistoricos[] = AutorizacaoExecucaoHistorico::factory()->create([
                'autorizacaoexecucoes_id' => $autorizacao->id,
                'situacao_id' => CodigoItem::where('descres', 'ae_status_7')->first()->id,
                'data_inicio_alteracao' => Carbon::now()->subDay(),
                'tipo_alteracao' => 'extingir',
                'data_extincao' => Carbon::now(),
                'usuario_responsavel_id' => User::factory()->create()->id
            ]);
        });


        $aeHistoricoAlterarService = new AutorizacaoExecucaoHistoricoAlterarService(new AutorizacaoExecucao());
        $aeHistoricoAlterarService->alteraSituacaoSeAtingirInicioAlteracao();

        $autorizacoes->each(function ($autorizacao) {
            $this->assertDatabaseHas(
                'autorizacaoexecucoes',
                [
                    'id' => $autorizacao->id,
                    'situacao_id' => CodigoItem::where('descres', 'ae_status_3')->first()->id,
                ]
            );
        });

        foreach ($aeHistoricos as $aeHistorico) {
            $this->assertDatabaseHas(
                'autorizacaoexecucao_historico',
                ['id' => $aeHistorico->id, 'situacao_id' => CodigoItem::where('descres', 'ae_status_2')->first()->id]
            );

            $this->assertDatabaseHas(
                'autorizacaoexecucao_status_historico',
                [
                    'autorizacaoexecucao_id' => $aeHistorico->autorizacaoexecucoes_id,
                    'status_id' => CodigoItem::where('descres', 'ae_st_historico_6')->first()->id,
                    'usuario_id' => $aeHistorico->usuario_responsavel_id,
                    'observacao' => null,
                ]
            );
        }
    }


    public function testAlterarSituacaoDeAssinadoParaEmExecucaoEAplicaAlteracaoItens()
    {
        $aeHistoricos = AutorizacaoExecucaoHistorico::factory(2)->create([
            'situacao_id' => CodigoItem::where('descres', 'ae_status_7')->first()->id,
            'data_inicio_alteracao' => Carbon::now()->subDay(),
            'tipo_alteracao' => 'itens',
            'usuario_responsavel_id' => User::factory()->create()->id
        ]);

        $aeItens = [];
        $aeItensHistorico = [];
        $aeHistoricos->each(function ($aeHistorico) use (&$aeItens, &$aeItensHistorico) {
            $aeItens[] = AutorizacaoexecucaoItens::factory()->create([
                'autorizacaoexecucoes_id' => $aeHistorico->autorizacaoexecucoes_id,
            ]);

            $aeItensHistorico[] = AutorizacaoexecucaoItensHistorico::factory()->create([
                'autorizacaoexecucao_historico_id' => $aeHistorico->id,
                'tipo_historico' => 'depois',
                'quantidade' => 2,
                'horario' => '00:00:00',
                'horario_fim' => '04:00:00',
                'subcontratacao' => false,
                'valor_unitario' => 100,
                'parcela' => 1,
            ]);
        });

        $aeHistoricoAlterarService = new AutorizacaoExecucaoHistoricoAlterarService(new AutorizacaoExecucao());
        $aeHistoricoAlterarService->alteraSituacaoSeAtingirInicioAlteracao();

        foreach ($aeItens as $aeItem) {
            $this->assertDatabaseMissing('autorizacaoexecucao_itens', ['id' => $aeItem->id]);
        }

        foreach ($aeHistoricos as $key => $aeHistorico) {
            $this->assertDatabaseHas(
                'autorizacaoexecucao_itens',
                [
                    'autorizacaoexecucoes_id' => $aeHistorico->autorizacaoexecucoes_id,
                    'saldohistoricoitens_id' => $aeItensHistorico[$key]->saldohistoricoitens_id,
                    'quantidade' => 2,
                    'horario' => '00:00:00',
                    'horario_fim' => '04:00:00',
                    'subcontratacao' => false,
                    'valor_unitario' => 100,
                    'parcela' => 1,
                ]
            );

            $this->assertDatabaseHas(
                'autorizacaoexecucao_status_historico',
                [
                    'autorizacaoexecucao_id' => $aeHistorico->autorizacaoexecucoes_id,
                    'status_id' => CodigoItem::where('descres', 'ae_st_historico_5')->first()->id,
                    'usuario_id' => $aeHistorico->usuario_responsavel_id,
                    'observacao' => null,
                ]
            );
        }
    }

    public function testAlterarSituacaoDeAguardandoAssinaturaParaEmElaboracaoSeNinguemAssinou()
    {
        $autorizacoes = AutorizacaoExecucao::factory(2)->create([
            'data_vigencia_inicio' => Carbon::now()->subYear(),
            'data_vigencia_fim' => Carbon::now(),
        ]);

        $aeHistoricos = [];
        $autorizacoes->each(function ($autorizacao) use (&$aeHistoricos) {
            $aeHistoricos[] = AutorizacaoExecucaoHistorico::factory()->create([
                'autorizacaoexecucoes_id' => $autorizacao->id,
                'situacao_id' => CodigoItem::where('descres', 'ae_status_5')->first()->id,
                'data_inicio_alteracao' => Carbon::now()->subDay(),
                'tipo_alteracao' => 'extingir',
                'data_extincao' => Carbon::now(),
                'usuario_responsavel_id' => User::factory()->create()->id
            ]);
        });

        $aeHistoricoAlterarService = new AutorizacaoExecucaoHistoricoAlterarService(new AutorizacaoExecucao());
        $aeHistoricoAlterarService->alteraSituacaoSeAtingirInicioAlteracao();

        foreach ($aeHistoricos as $aeHistorico) {
            $this->assertDatabaseHas(
                'autorizacaoexecucao_historico',
                ['id' => $aeHistorico->id, 'situacao_id' => CodigoItem::where('descres', 'ae_status_1')->first()->id]
            );
        }
    }

    public function testAlterarSituacaoDeAguardandoAssinaturaParaAssinaturaRecusadaSeSomentePrepostoOuFiscalAssinaram()
    {
        $autorizacoes = AutorizacaoExecucao::factory(2)->create([
            'data_vigencia_inicio' => Carbon::now()->subYear(),
            'data_vigencia_fim' => Carbon::now(),
        ]);

        $aeHistoricos = [];
        $autorizacoes->each(function ($autorizacao) use (&$aeHistoricos) {
            $aeHistoricoCreated = AutorizacaoExecucaoHistorico::factory()->create([
                'autorizacaoexecucoes_id' => $autorizacao->id,
                'situacao_id' => CodigoItem::where('descres', 'ae_status_5')->first()->id,
                'data_inicio_alteracao' => Carbon::now()->subDay(),
                'tipo_alteracao' => 'extingir',
                'data_extincao' => Carbon::now(),
                'usuario_responsavel_id' => User::factory()->create()->id
            ]);

            ModelSignatario::create([
                'model_autorizacaoexecucao_historico_id' => $aeHistoricoCreated->id,
                'signatario_contratopreposto_id' => Contratopreposto::factory()->create()->id,
                'status_assinatura_id' => CodigoItem::where('descricao', 'Assinado')->first()->id,
                'arquivo_generico_id' => ArquivoGenerico::first()->id,
                'posicao_x_assinatura' => 0,
                'posicao_y_assinatura' => 0,
                'pagina_assinatura' => 1,
                'data_operacao' => Carbon::now()
            ]);

            $aeHistoricos[] = $aeHistoricoCreated;
        });

        $aeHistoricoAlterarService = new AutorizacaoExecucaoHistoricoAlterarService(new AutorizacaoExecucao());
        $aeHistoricoAlterarService->alteraSituacaoSeAtingirInicioAlteracao();

        foreach ($aeHistoricos as $aeHistorico) {
            $this->assertDatabaseHas(
                'autorizacaoexecucao_historico',
                ['id' => $aeHistorico->id, 'situacao_id' => CodigoItem::where('descres', 'ae_status_6')->first()->id]
            );
        }
    }


    private function gerarDadosSaldoHistoricoAlteracao($contratoId, $situacaoAlteracaoId): array
    {
        $autorizacoes = AutorizacaoExecucao::factory(3)->create([
            'contrato_id' => $contratoId,
            'situacao_id' => CodigoItem::where('descres', 'ae_status_2')->first()->id
        ]);

        $aeAlteracoes = collect();
        $autorizacoes->each(function ($autorizacao) use (&$aeAlteracoes, $situacaoAlteracaoId) {
            $aeAlteracoes->push(AutorizacaoexecucaoHistorico::factory()->create([
                'autorizacaoexecucoes_id' => $autorizacao->id,
                'tipo_alteracao' => 'itens',
                'situacao_id' => $situacaoAlteracaoId
            ]));
        });

        $saldoHistoricoItemIds = [];
        $aeAlteracoes->each(function ($aeAlteracao, $key) use (&$saldoHistoricoItemIds) {
            $saldoHistoricoItemId = Saldohistoricoitem::factory()->create()->id;
            $saldoHistoricoItemIds[] = $saldoHistoricoItemId;
            AutorizacaoexecucaoItensHistorico::factory()->create([
                'autorizacaoexecucao_historico_id' => $aeAlteracao->id,
                'saldohistoricoitens_id' => $saldoHistoricoItemId,
                'quantidade' => 10,
                'parcela' => $key + 1,
                'tipo_historico' => 'antes',
            ]);
            // $key === 1 simula alteração de um item por outro
            if ($key === 1) {
                $saldoHistoricoItemId = Saldohistoricoitem::factory()->create()->id;
                $saldoHistoricoItemIds[] = $saldoHistoricoItemId;
            }
            AutorizacaoexecucaoItensHistorico::factory()->create([
                'autorizacaoexecucao_historico_id' => $aeAlteracao->id,
                'saldohistoricoitens_id' => $saldoHistoricoItemId,
                'quantidade' => 20,
                'parcela' => $key + 1,
                'tipo_historico' => 'depois',
            ]);
        });
        return $saldoHistoricoItemIds;
    }
}
