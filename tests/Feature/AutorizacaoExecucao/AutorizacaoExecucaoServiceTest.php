<?php

namespace Tests\Feature\AutorizacaoExecucao;

use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoexecucaoSignatario;
use App\Models\CodigoItem;
use App\Models\Contrato;
use App\Models\Contratopreposto;
use App\Models\Contratoresponsavel;
use App\Models\ModelSignatario;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoService;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class AutorizacaoExecucaoServiceTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        DB::beginTransaction();

        $this->aeService = new AutorizacaoExecucaoService();
    }


    protected function tearDown(): void
    {
        DB::rollBack();

        parent::tearDown();
    }

    public function testConvertePrepostosResponsaveisDoContratoParaAssinantes()
    {
        $contrato = Contrato::factory()->create();
        $responsaveis = Contratoresponsavel::factory(3)->create(['contrato_id' => $contrato->id]);
        $preposto = Contratopreposto::factory(2)->create(['contrato_id' => $contrato->id]);
        $this->aeService->setContrato($contrato);

        $tiposWithIds = $responsaveis->map(function ($responsavel) {
            return "responsaveis-{$responsavel->id}";
        })->toArray();

        $preposto->each(function ($preposto) use (&$tiposWithIds) {
            $tiposWithIds[] = "preposto-{$preposto->id}";
        });

        $assinantes = $this->aeService->convertPrepostosResponsaveisToAssinantes($tiposWithIds);

        $this->assertEquals($assinantes[0]->id, $responsaveis[0]->id);
        $this->assertEquals($assinantes[1]->id, $responsaveis[1]->id);
        $this->assertEquals($assinantes[2]->id, $responsaveis[2]->id);
        $this->assertEquals($assinantes[3]->id, $preposto[0]->id);
        $this->assertEquals($assinantes[4]->id, $preposto[1]->id);
    }

    public function testNaoConvertePrepostosResponsaveisDoContratoParaAssinantesComParametroVazio()
    {
        $contrato = Contrato::factory()->create();
        $responsaveis = Contratoresponsavel::factory(3)->create(['contrato_id' => $contrato->id]);
        $this->aeService->setContrato($contrato);

        $this->assertEmpty($this->aeService->convertPrepostosResponsaveisToAssinantes([]));
    }
    public function testNaoConvertePrepostosResponsaveisDoContratoParaAssinantesComParametroErrado()
    {
        $contrato = Contrato::factory()->create();
        $responsaveis = Contratoresponsavel::factory(3)->create(['contrato_id' => $contrato->id]);
        $this->aeService->setContrato($contrato);

        $tiposWithIds = ['213', '321', '21'];

        try {
            $this->aeService->convertPrepostosResponsaveisToAssinantes($tiposWithIds);
        } catch (\Exception $exception) {
            $this->assertEquals($exception->getMessage(), 'Parâmetro idsWithTipos inválido');
        }
    }


    public function testAlteraSituacaoDeAguardandoAssinaturaParaEmElaboracaoSeNaoPossuirAssinaturas()
    {
        $status = CodigoItem::whereIn('descres', ['ae_status_1', 'ae_status_5'])->orderBy('descres')->get();
        $statusIdEmElaboracao = $status[0]->id;
        $statusIdAguardandoAssinatura = $status[1]->id;

        $autorizacoesExecucao = AutorizacaoExecucao::factory(2)->create([
            'situacao_id' => $statusIdAguardandoAssinatura,
            'data_vigencia_inicio' => Carbon::now()->subDay(),
        ]);

        ModelSignatario::factory()->create([
            'model_autorizacaoexecucao_id' => $autorizacoesExecucao[0]->id,
            'signatario_contratoresponsavel_id' => Contratoresponsavel::factory()->create()->id,
            'status_assinatura_id' => CodigoItem::where('descres', 'status_assinatura')
                ->where('descricao', 'Aguardando')
                ->first()->id,
            'data_operacao' => Carbon::now()->subDays(5),
        ]);

        ModelSignatario::factory()->create([
            'model_autorizacaoexecucao_id' => $autorizacoesExecucao[1]->id,
            'signatario_contratoresponsavel_id' => Contratoresponsavel::factory()->create()->id,
            'status_assinatura_id' => CodigoItem::where('descres', 'status_assinatura')
                ->where('descricao', 'Recusado')
                ->first()->id,
            'data_operacao' => Carbon::now()->subDays(5),
        ]);

        $this->aeService->alteraSituacaoSeAtingirInicioVigencia();

        $autorizacoesExecucao->each(function ($autorizacaoExecucao) use ($statusIdEmElaboracao) {
            $this->assertDatabaseHas(
                'autorizacaoexecucoes',
                [
                    'id' => $autorizacaoExecucao->id,
                    'situacao_id' => $statusIdEmElaboracao,
                ]
            );
        });
    }

    public function testAlteraSituacaoDeAguardandoAssinaturaParaAssinaturaRecusadaQuantoPossuirAssinaturas()
    {
        $status = CodigoItem::whereIn('descres', ['ae_status_5', 'ae_status_6'])->orderBy('descres')->get();
        $statusIdAguardandoAssinatura = $status[0]->id;
        $statusIdAssinaturaRecusada = $status[1]->id;
        $autorizacoesExecucao = AutorizacaoExecucao::factory(2)->create([
            'situacao_id' => $statusIdAguardandoAssinatura, // Aguardando assinatura
            'data_vigencia_inicio' => Carbon::now()->subDay(),
        ]);

        $autorizacoesExecucao->each(function ($autorizacaoExecucao) {
            ModelSignatario::factory(2)->create([
                'model_autorizacaoexecucao_id' => $autorizacaoExecucao->id,
                'signatario_contratoresponsavel_id' => Contratoresponsavel::factory()->create()->id,
                'status_assinatura_id' => CodigoItem::where('descres', 'status_assinatura')
                        ->where('descricao', 'Assinado')
                        ->first()->id,
                'data_operacao' => Carbon::now()->subDays(5),
            ]);
        });


        $this->aeService->alteraSituacaoSeAtingirInicioVigencia();

        $autorizacoesExecucao->each(function ($autorizacaoExecucao) use ($statusIdAssinaturaRecusada) {
            $this->assertDatabaseHas(
                'autorizacaoexecucoes',
                [
                    'id' => $autorizacaoExecucao->id,
                    'situacao_id' => $statusIdAssinaturaRecusada,
                ]
            );
        });
    }

    public function testNaoAlteraSituacaoSeDataVigenciaNaoForAtingida()
    {
        $status = CodigoItem::where('descres', 'ae_status_5')->first();
        $statusIdAguardandoAssinatura = $status->id;

        $autorizacaoExecucao = AutorizacaoExecucao::factory()->create([
            'situacao_id' => $statusIdAguardandoAssinatura,
            'data_vigencia_inicio' => Carbon::now()->addDay(),
        ]);

        ModelSignatario::factory(1)->create([
            'model_autorizacaoexecucao_id' => $autorizacaoExecucao->id,
            'signatario_contratoresponsavel_id' => Contratoresponsavel::factory()->create()->id,
            'status_assinatura_id' => CodigoItem::where('descres', 'status_assinatura')
                ->where('descricao', 'Assinado')
                ->first()->id,
            'data_operacao' => Carbon::now()->subDays(5),
        ]);


        $this->aeService->alteraSituacaoSeAtingirInicioVigencia();

        $this->assertDatabaseHas(
            'autorizacaoexecucoes',
            [
                'id' => $autorizacaoExecucao->id,
                'situacao_id' => $statusIdAguardandoAssinatura,
            ]
        );
    }

    public function testAlteraSituacaoDeEmExecucaoParaConcluidoSeDataVigenciaFimForAtingida()
    {
        $status = CodigoItem::where('descres', 'ae_status_2')->first();
        $statusIdEmExecucao = $status->id;

        $autorizacaoExecucao = AutorizacaoExecucao::factory()->create([
            'situacao_id' => $statusIdEmExecucao,
            'data_vigencia_inicio' => Carbon::now()->subMonths(3),
            'data_vigencia_fim' => Carbon::now()
        ]);

        $this->aeService->alteraSituacaoSeAtingirVigenciaFim();

        $status = CodigoItem::where('descres', 'ae_status_4')->first();
        $statusConcluido = $status->id;

        $this->assertDatabaseHas(
            'autorizacaoexecucoes',
            [
                'id' => $autorizacaoExecucao->id,
                'situacao_id' => $statusConcluido,
            ]
        );
    }

    public function testNaoAlteraSituacaoSeDataVigenciaFimNaoForAtingida()
    {
        $status = CodigoItem::where('descres', 'ae_status_2')->first();
        $statusIdEmExecucao = $status->id;

        $autorizacaoExecucao = AutorizacaoExecucao::factory()->create([
            'situacao_id' => $statusIdEmExecucao,
            'data_vigencia_inicio' => Carbon::now()->subMonths(3),
            'data_vigencia_fim' => Carbon::now()->addDay(),
        ]);

        $this->aeService->alteraSituacaoSeAtingirVigenciaFim();

        $this->assertDatabaseHas(
            'autorizacaoexecucoes',
            [
                'id' => $autorizacaoExecucao->id,
                'situacao_id' => $statusIdEmExecucao,
            ]
        );
    }

    public function testNaoAlterarSituacaoSeAtingirDataVienciaFimESituacaoNaoForEmExecucao()
    {
        $status = CodigoItem::where('descres', '<>', 'ae_status_2')->pluck('id');
        $situacaoId = $status[rand(0, count($status) - 1)];

        $autorizacaoExecucao = AutorizacaoExecucao::factory()->create([
            'situacao_id' => $situacaoId,
            'data_vigencia_inicio' => Carbon::now()->subMonths(3),
            'data_vigencia_fim' => Carbon::now(),
        ]);

        $this->aeService->alteraSituacaoSeAtingirVigenciaFim();

        $this->assertDatabaseHas(
            'autorizacaoexecucoes',
            [
                'id' => $autorizacaoExecucao->id,
                'situacao_id' => $situacaoId,
            ]
        );
    }
}
