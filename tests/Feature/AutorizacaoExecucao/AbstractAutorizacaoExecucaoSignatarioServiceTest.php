<?php

namespace Tests\Feature\AutorizacaoExecucao;

use App\Models\AutorizacaoExecucao;
use App\Models\AutorizacaoexecucaoHistorico;
use App\Models\ModelSignatario;
use App\Models\BackpackUser;
use App\Models\CodigoItem;
use App\Models\Contratopreposto;
use App\Models\Contratoresponsavel;
use App\Models\User;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoHistoricoAlterarSignatarioService;
use App\Services\AutorizacaoExecucao\AutorizacaoExecucaoSignatarioService;
use App\Services\ModelSignatario\ModelDTO;
use App\Services\ModelSignatario\SignatarioDTO;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class AbstractAutorizacaoExecucaoSignatarioServiceTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        DB::beginTransaction();

        $autorizacaoExecucao = AutorizacaoExecucao::factory()->create([
            'situacao_id' => CodigoItem::where('descres', 'ae_status_5')->first()->id,
        ]);

        $aehistorico = AutorizacaoexecucaoHistorico::factory()->create([
            'autorizacaoexecucoes_id' => $autorizacaoExecucao->id,
            'situacao_id' => CodigoItem::where('descres', 'ae_status_5')->first()->id,
        ]);

        $classes = [$autorizacaoExecucao, $aehistorico];
        $keyRandom = array_rand($classes);
        $this->modelDTO = new ModelDTO($classes[$keyRandom]);

        $this->aeSignatarioService = new AutorizacaoExecucaoHistoricoAlterarSignatarioService($aehistorico);
        if ($this->modelDTO->fkColumn === 'model_autorizacaoexecucao_id') {
            $this->aeSignatarioService = new AutorizacaoExecucaoSignatarioService($autorizacaoExecucao);
        }
    }

    protected function tearDown(): void
    {
        DB::rollBack();

        parent::tearDown();
    }

    public function testVerificaPeloRelacionamentoSeUsuarioPrepostoPodeAssinar()
    {
        $usuario = BackpackUser::find(User::factory()->create()->id);
        $this->actingAs($usuario);

        ModelSignatario::factory()->create([
            $this->modelDTO->fkColumn => $this->modelDTO->model->id,
            'signatario_contratopreposto_id' => Contratopreposto::factory()->create(['user_id' => $usuario->id])->id,
        ]);


        $this->assertTrue($this->aeSignatarioService->verifyUsuarioCanAssinar());
    }

    public function testVerificaPeloCpfSeUsuarioPrepostoPodeAssinar()
    {
        $usuario = BackpackUser::find(User::factory()->create()->id);
        $this->actingAs($usuario);

        ModelSignatario::factory()->create([
            $this->modelDTO->fkColumn => $this->modelDTO->model->id,
            'signatario_contratopreposto_id' => Contratopreposto::factory()->create([
                'user_id' => null,
                'cpf' => $usuario->cpf
            ])->id,
        ]);

        $this->assertTrue($this->aeSignatarioService->verifyUsuarioCanAssinar());
    }

    public function testVerificaSeUsuarioResponsavelPodeAssinar()
    {
        $usuario = BackpackUser::find(User::factory()->create()->id);
        $this->actingAs($usuario);

        ModelSignatario::factory()->create([
            $this->modelDTO->fkColumn => $this->modelDTO->model->id,
            'signatario_contratoresponsavel_id' => Contratoresponsavel::factory()->create([
                'user_id' => $usuario->id
            ])->id,
        ]);

        $this->assertTrue($this->aeSignatarioService->verifyUsuarioCanAssinar());
    }

    public function testVerificaSeUsuarioNaoPodeAssinar()
    {
        $usuario = BackpackUser::find(User::factory()->create()->id);
        $this->actingAs($usuario);

        ModelSignatario::factory()->create([
            $this->modelDTO->fkColumn => $this->modelDTO->model->id,
        ]);

        $this->assertFalse($this->aeSignatarioService->verifyUsuarioCanAssinar());
    }

    public function testVerificaSeUsuarioSignatarioQueJaAssinouNaoPodeAssinar()
    {
        $usuario = BackpackUser::find(User::factory()->create()->id);
        $this->actingAs($usuario);

        ModelSignatario::factory()->create([
            $this->modelDTO->fkColumn => $this->modelDTO->model->id,
            'signatario_contratopreposto_id' => Contratopreposto::factory()->create(['user_id' => $usuario->id])->id,
            'data_operacao' => Carbon::now(),
        ]);

        $this->assertFalse($this->aeSignatarioService->verifyUsuarioCanAssinar());
    }

    public function testVerificaSeUsuarioSignatarioPodeRecusarSeSituacaoIgualAguardandoAssinatura()
    {
        $usuario = BackpackUser::find(User::factory()->create()->id);
        $this->actingAs($usuario);

        ModelSignatario::factory()->create([
            $this->modelDTO->fkColumn => $this->modelDTO->model->id,
            'signatario_contratoresponsavel_id' => Contratoresponsavel::factory()->create([
                'user_id' => $usuario->id
            ])->id,
        ]);

        $this->assertTrue($this->aeSignatarioService->verifyUsuarioResponsavelCanRecusar());
    }

    public function testGetIdAutorizacaoExecucaoSignatarioPrepostoByUsuario()
    {
        $usuario = BackpackUser::find(User::factory()->create()->id);
        $this->actingAs($usuario);

        $aeSignatario = ModelSignatario::factory()->create([
            $this->modelDTO->fkColumn => $this->modelDTO->model->id,
            'signatario_contratopreposto_id' => Contratopreposto::factory()->create([
                'user_id' => null,
                'cpf' => $usuario->cpf
            ])->id,
        ]);

        $signtarioDTO = new SignatarioDTO($this->aeSignatarioService->getSignatarioByUsuario());

        $this->assertEquals(
            $aeSignatario->signatario_contratopreposto_id,
            $signtarioDTO->signatario->pivot->signatario_contratopreposto_id
        );

        $this->assertEquals(
            $aeSignatario->{$this->modelDTO->fkColumn},
            $signtarioDTO->signatario->pivot->{$this->modelDTO->fkColumn}
        );
    }

    public function testGetIdAutorizacaoExecucaoSignatarioResponsavelByUsuario()
    {
        $usuario = BackpackUser::find(User::factory()->create()->id);
        $this->actingAs($usuario);

        $aeSignatario = ModelSignatario::factory()->create([
            $this->modelDTO->fkColumn => $this->modelDTO->model->id,
            'signatario_contratoresponsavel_id' => Contratoresponsavel::factory()->create([
                'user_id' => $usuario->id
            ])->id,
        ]);

        $signtarioDTO = new SignatarioDTO($this->aeSignatarioService->getSignatarioByUsuario());

        $this->assertEquals(
            $aeSignatario->signatario_contratopreposto_id,
            $signtarioDTO->signatario->pivot->signatario_contratopreposto_id
        );

        $this->assertEquals(
            $aeSignatario->{$this->modelDTO->fkColumn},
            $signtarioDTO->signatario->pivot->{$this->modelDTO->fkColumn}
        );
    }

    public function testAlterarSituacaoAssinandoComoGestor()
    {
        $usuario = BackpackUser::find(User::factory()->create()->id);
        $this->actingAs($usuario);

        $this->aeSignatarioService->assinar(
            Contratoresponsavel::factory()->create([
                'funcao_id' => CodigoItem::where('descres', 'GESTOR')->first()->id,
            ])
        );

        $status = CodigoItem::where('descres', 'ae_status_7')->first(); // para autorizacaoexecucao_historico
        $data = ['id' => $this->modelDTO->model->id, 'situacao_id' => $status->id];
        if ($this->modelDTO->fkColumn === 'model_autorizacaoexecucao_id') {
            $status = CodigoItem::where('descres', 'ae_status_2')->first(); // para autorizacaoexecucao
            $data = [
                'id' => $this->modelDTO->model->id,
                'situacao_id' => $status->id,
                'data_assinatura' => Carbon::now()
            ];
        }

        $this->assertDatabaseHas($this->modelDTO->model->getTable(), $data);
    }


    public function testAlterarSituacaoAssinandoGestorSubstituto()
    {
        $usuario = BackpackUser::find(User::factory()->create()->id);
        $this->actingAs($usuario);

        $this->aeSignatarioService->assinar(
            Contratoresponsavel::factory()->create([
                'funcao_id' => CodigoItem::where('descres', 'GESTORSUB')->first()->id,
            ])
        );

        $status = CodigoItem::where('descres', 'ae_status_7')->first(); // para autorizacaoexecucao_historico
        $data = ['id' => $this->modelDTO->model->id, 'situacao_id' => $status->id];
        if ($this->modelDTO->fkColumn === 'model_autorizacaoexecucao_id') {
            $status = CodigoItem::where('descres', 'ae_status_2')->first(); // para autorizacaoexecucao
            $data = [
                'id' => $this->modelDTO->model->id,
                'situacao_id' => $status->id,
                'data_assinatura' => Carbon::now()
            ];
        }

        $this->assertDatabaseHas($this->modelDTO->model->getTable(), $data);
    }


    public function testAlterarSituacaoRecusandoComoQualquerFuncao()
    {
        $usuario = BackpackUser::find(User::factory()->create()->id);
        $this->actingAs($usuario);

        $this->aeSignatarioService->recusarAssinatura(
            Contratoresponsavel::factory()->create(),
            'motivo da recusa'
        );

        $status = CodigoItem::where('descres', 'ae_status_6')->first();
        $data = ['id' => $this->modelDTO->model->id, 'situacao_id' => $status->id];

        $this->assertDatabaseHas($this->modelDTO->model->getTable(), $data);
    }


    public function testNaoDeveAtualizarSituacaoAssinandoComoFiscalOuPreposto()
    {
        $usuario = BackpackUser::find(User::factory()->create()->id);
        $this->actingAs($usuario);

        $this->aeSignatarioService->assinar(
            Contratoresponsavel::factory()->create([
                'funcao_id' => CodigoItem::where('descres', 'FSCTEC')->first()->id,
            ])
        );
        $this->aeSignatarioService->assinar(Contratopreposto::factory()->create());

        $status = CodigoItem::where('descres', 'ae_status_5')->first();
        $data = ['id' => $this->modelDTO->model->id, 'situacao_id' => $status->id];

        $this->assertDatabaseHas($this->modelDTO->model->getTable(), $data);
    }
}
