<?php

namespace Tests\Feature\AutorizacaoExecucao;

use App\Models\ArquivoGenerico;
use App\Models\AutorizacaoExecucao;
use App\Repositories\AutorizacaoExecucao\AutorizacaoExecucaoRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AutorizacaoExecucaoRepositoryTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        DB::beginTransaction();

        $this->aeRepository = new AutorizacaoExecucaoRepository();

        Storage::fake('public');
    }

    protected function tearDown(): void
    {
        DB::rollBack();

        parent::tearDown();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSalvarAnexo()
    {
        $autorizacaoExecucao = AutorizacaoExecucao::factory()->create();
        Storage::disk('public')->put('/temporario/arquivo.pdf', 'arquivo');
        Storage::disk('public')->assertExists('/temporario/arquivo.pdf');

        $anexoData = [
            'nome_arquivo_anexo' => 'arquivo.pdf',
            'descricao_arquivo_anexo' => 'descricao',
            'url_arquivo_anexo' => '/temporario/arquivo.pdf',
            'remove_file' => false,
        ];

        $this->aeRepository->salvarAnexo($autorizacaoExecucao, $anexoData);

        $date = Carbon::now();
        $urlDestino = 'autorizacaoexecucao/' .
            $date->format('Y') . '/' .
            $date->format('m') . '/' .
            $anexoData['nome_arquivo_anexo'];

        Storage::disk('public')->assertExists($urlDestino);

        $this->assertDatabaseHas(
            'arquivo_generico',
            [
                'arquivoable_type' => AutorizacaoExecucao::class,
                'arquivoable_id' => $autorizacaoExecucao->id,
                'nome' => $anexoData['nome_arquivo_anexo'],
                'descricao' => $anexoData['descricao_arquivo_anexo'],
                'url' => $urlDestino,
                'restrito' => false,
            ]
        );
    }

    public function testExcluirAnexoTemporario()
    {
        Storage::disk('public')->put('/temporario/arquivo.pdf', 'arquivo');

        $anexoData = [
            'nome_arquivo_anexo' => 'arquivo.pdf',
            'descricao_arquivo_anexo' => 'descricao',
            'url_arquivo_anexo' => '/temporario/arquivo.pdf',
            'remove_file' => true,
        ];

        $this->aeRepository->excluiAnexo($anexoData);

        Storage::disk('public')->assertMissing('/temporario/arquivo.pdf');
    }

    public function testExcluirAnexoPersistido()
    {
        $url = 'autorizacaoexecucao/' .
            Carbon::now()->format('Y') . '/' .
            Carbon::now()->format('m') . '/arquivo.pdf';
        Storage::fake('public')->put($url, 'arquivo');
        $arquivoGenerico = ArquivoGenerico::factory()->create([
            'arquivoable_type' => AutorizacaoExecucao::class,
            'arquivoable_id' => AutorizacaoExecucao::factory()->create()->id,
            'url' => $url,
        ]);

        $anexoData = [
            'id' => $arquivoGenerico->id,
            'nome_arquivo_anexo' => $arquivoGenerico->nome,
            'descricao_arquivo_anexo' => $arquivoGenerico->descricao,
            'url_arquivo_anexo' => $url,
            'remove_file' => true,
        ];

        Storage::disk('public')->assertExists($url);

        $this->aeRepository->excluiAnexo($anexoData);

        $this->assertDatabaseMissing('arquivo_generico', ['id' => $arquivoGenerico->id]);
        Storage::disk('public')->assertMissing($url);
    }
}
