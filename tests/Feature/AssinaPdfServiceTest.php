<?php

namespace Tests\Feature;

use App\Models\BackpackUser;
use App\Models\User;
use App\Services\AssinaPdfService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Smalot\PdfParser\Parser;
use Tests\TestCase;

class AssinaPdfServiceTest extends TestCase
{
    private $assinaPdfService;

    protected function setUp(): void
    {
        parent::setUp();
        DB::beginTransaction();

        $this->usuario = BackpackUser::find(User::factory()->create()->id);
        $this->actingAs($this->usuario);

        Storage::fake('public');
        $this->pdf = new \TCPDF();
        $this->pdf->addPage();
        Storage::disk('public')->put('temporario/arquivo.pdf', $this->pdf->getPDFData());

        $this->assinaPdfService = new AssinaPdfService(
            'temporario/arquivo.pdf'
        );
    }

    protected function tearDown(): void
    {
        DB::rollBack();

        parent::tearDown();
    }

    public function testGetConteudoByteRange()
    {
        try {
            $byteRange = $this->assinaPdfService->getConteudoByteRange();
        } catch (\Exception $e) {
            $this->assertTrue(false);
            return;
        }

        $this->assertTrue(!strstr($byteRange, '/Contents<000000'));
    }

    public function testGetConteudoByteRangeDaSegundaAssinatura()
    {
        $this->assinaPdfService->getConteudoByteRange();
        $pdfAssinado = $this->assinaPdfService->addAssinaturaDigital('AssinaturaFake');

        Storage::disk('public')->put('temporario/arquivo_assinado.pdf', $pdfAssinado);
        $assinaPdfService = new AssinaPdfService('temporario/arquivo_assinado.pdf');
        $byteRange = $assinaPdfService->getConteudoByteRange();

        $this->assertEquals(2, substr_count($byteRange, '/ByteRange'));
        $this->assertTrue(!strstr($byteRange, '/Contents<000000'));
    }

    public function testAddAssinaturaDigital()
    {
        $conteudoByteRange = $this->assinaPdfService->getConteudoByteRange();
        $assinaturaBase64 = 'assinatura';
        $conteudoComAssinatura = $this->assinaPdfService->addAssinaturaDigital($assinaturaBase64);
        $signature = bin2hex($assinaturaBase64);
        $signature = str_pad($signature, 11742, '0');
        $this->assertNotEquals($conteudoComAssinatura, $conteudoByteRange);
        $this->assertTrue(!!strstr($conteudoComAssinatura, $signature));
    }

    public function testAddSegundaAssinaturaDigital()
    {
        $this->assinaPdfService->getConteudoByteRange();
        $assinatura_1_base64 = 'assinatura1';
        $pdfAssinado = $this->assinaPdfService->addAssinaturaDigital($assinatura_1_base64);

        Storage::disk('public')->put('temporario/arquivo_assinado.pdf', $pdfAssinado);
        $assinaPdfService = new AssinaPdfService('temporario/arquivo_assinado.pdf');
        $assinaPdfService->getConteudoByteRange();

        $assinatura_2_base64 = 'assinatura2';
        $conteudoComAssinatura = $assinaPdfService->addAssinaturaDigital($assinatura_2_base64);


        $signature_1 = bin2hex($assinatura_1_base64);
        $signature_1 = str_pad($signature_1, 11742, '0');

        $signature_2 = bin2hex($assinatura_2_base64);
        $signature_2 = str_pad($signature_2, 11742, '0');

        $this->assertTrue(!!strstr($conteudoComAssinatura, $signature_1));
        $this->assertTrue(!!strstr($conteudoComAssinatura, $signature_2));
    }


    public function testAddTerceiraAssinaturaDigital()
    {
        $this->assinaPdfService->getConteudoByteRange();
        $assinatura_1_base64 = 'assinatura1';
        $pdfAssinado = $this->assinaPdfService->addAssinaturaDigital($assinatura_1_base64);

        Storage::disk('public')->put('temporario/arquivo_assinado.pdf', $pdfAssinado);
        $assinaPdfService = new AssinaPdfService('temporario/arquivo_assinado.pdf');
        $assinaPdfService->getConteudoByteRange();

        $assinatura_2_base64 = 'assinatura2';
        $conteudoComAssinatura2 = $assinaPdfService->addAssinaturaDigital($assinatura_2_base64, true);
        Storage::disk('public')->put('temporario/arquivo_assinado.pdf', $conteudoComAssinatura2);

        $assinaPdfService3 = new AssinaPdfService('temporario/arquivo_assinado.pdf', true);
        $assinaPdfService3->getConteudoByteRange();

        $assinatura_3_base64 = 'assinatura3';
        $conteudoComAssinatura3 = $assinaPdfService3->addAssinaturaDigital($assinatura_3_base64);
        Storage::disk('public')->put('temporario/arquivo_assinado.pdf', $conteudoComAssinatura3);


        $signature_1 = bin2hex($assinatura_1_base64);
        $signature_1 = str_pad($signature_1, 11742, '0');

        $signature_2 = bin2hex($assinatura_2_base64);
        $signature_2 = str_pad($signature_2, 11742, '0');

        $signature_3 = bin2hex($assinatura_3_base64);
        $signature_3 = str_pad($signature_3, 11742, '0');

        $this->assertTrue(!!strstr($conteudoComAssinatura3, $signature_1));
        $this->assertTrue(!!strstr($conteudoComAssinatura3, $signature_2));
        $this->assertTrue(!!strstr($conteudoComAssinatura3, $signature_3));
    }

    public function testErroByteRangeEmptyEmAddAssinaturaDigital()
    {
        try {
            $this->assinaPdfService->addAssinaturaDigital('assinatura');
        } catch (\Exception $e) {
            $this->assertTrue(true);
            $this->assertEquals(
                'ByteRange não encontrado, por favor formate o arquivo PDF antes de adicionar a assinatura digital',
                $e->getMessage()
            );
            return;
        }

        $this->assertFalse(true);
    }
}
