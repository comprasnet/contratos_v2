<?php

namespace Tests\Feature\ModelSignatario;

use App\Models\ArquivoGenerico;
use App\Models\AutorizacaoExecucao;
use App\Models\CodigoItem;
use App\Models\Contratopreposto;
use App\Models\Contratoresponsavel;
use App\Models\ModelSignatario;
use App\Services\ModelSignatario\ModelDTO;
use App\Services\ModelSignatario\ModelSignatarioService;
use App\Services\ModelSignatario\SignatarioDTO;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class ModelSignatarioServiceTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        DB::beginTransaction();

        $this->autorizacaoExecucao = AutorizacaoExecucao::factory()->create([
            'situacao_id' => CodigoItem::where('descres', 'ae_status_5')->first()->id,
        ]);

        $this->arquivoGenerico = ArquivoGenerico::factory()->create([
            'arquivoable_type' => 'App\Models\AutorizacaoExecucao',
            'arquivoable_id' => $this->autorizacaoExecucao->id,
            'url' => 'arquivo_fake.pdf',
        ]);

        $this->arquivoData = [
            'arquivo_generico_id' => $this->arquivoGenerico->id,
            'posicao_x_assinatura' => 0,
            'posicao_y_assinatura' => 0,
            'pagina_assinatura' => 1,
        ];
    }

    protected function tearDown(): void
    {
        DB::rollBack();

        parent::tearDown();
    }

    public function testExceptionModelInvalida()
    {
        try {
            $dto = new ModelDTO(new CodigoItem());
            $modelSignatario = new ModelSignatarioService($dto);
        } catch (\Exception $e) {
            $this->assertEquals('Model inválida', $e->getMessage());
            $this->assertTrue(true);
        }
    }

    /**
     * @throws \Exception
     */
    public function testAddSignatario()
    {
        $modelSignatario = new ModelSignatarioService(new ModelDTO($this->autorizacaoExecucao));

        $signatarioContratoResponsavel = Contratoresponsavel::factory()->create();

        $statusAguardandoAssinatura = CodigoItem::where('descres', 'status_assinatura')
            ->where('descricao', 'Aguardando')
            ->first();

        $modelSignatario->addSignatario(new SignatarioDTO($signatarioContratoResponsavel), $this->arquivoData);

        $this->assertDatabaseHas(
            'models_signatarios',
            [
                'model_autorizacaoexecucao_id' => $this->autorizacaoExecucao->id,
                'signatario_contratoresponsavel_id' => $signatarioContratoResponsavel->id,
                'status_assinatura_id' => $statusAguardandoAssinatura->id,
                'arquivo_generico_id' => $this->arquivoGenerico->id,
                'posicao_x_assinatura' => $this->arquivoData['posicao_x_assinatura'],
                'posicao_y_assinatura' => $this->arquivoData['posicao_y_assinatura'],
                'pagina_assinatura' => $this->arquivoData['pagina_assinatura'],
                'data_operacao' => null,
            ]
        );
    }

    public function testAssinar()
    {
        $statusAssinado = CodigoItem::where('descres', 'status_assinatura')
            ->where('descricao', 'Assinado')
            ->first();

        $modelSignatario = new ModelSignatarioService(new ModelDTO($this->autorizacaoExecucao));

        $signatarioContratoPreposto = Contratopreposto::factory()->create();

        $modelSignatario->addSignatario(new SignatarioDTO($signatarioContratoPreposto), $this->arquivoData);
        $modelSignatario->assinar(new SignatarioDTO($signatarioContratoPreposto));

        $this->assertDatabaseHas(
            'models_signatarios',
            [
                'model_autorizacaoexecucao_id' => $this->autorizacaoExecucao->id,
                'signatario_contratopreposto_id' => $signatarioContratoPreposto->id,
                'status_assinatura_id' => $statusAssinado->id,
                'data_operacao' => Carbon::now(),
            ]
        );
    }

    public function testRecusarAssinatura()
    {
        $statusRecusado = CodigoItem::where('descres', 'status_assinatura')
            ->where('descricao', 'Recusado')
            ->first();

        $modelSignatario = new ModelSignatarioService(new ModelDTO($this->autorizacaoExecucao));

        $signatarioContratoPreposto = Contratopreposto::factory()->create();

        $modelSignatario->addSignatario(new SignatarioDTO($signatarioContratoPreposto), $this->arquivoData);
        $modelSignatario->recusar(new SignatarioDTO($signatarioContratoPreposto));

        $this->assertDatabaseHas(
            'models_signatarios',
            [
                'model_autorizacaoexecucao_id' => $this->autorizacaoExecucao->id,
                'signatario_contratopreposto_id' => $signatarioContratoPreposto->id,
                'status_assinatura_id' => $statusRecusado->id,
                'data_operacao' => Carbon::now(),
            ]
        );
    }

    public function testRemoverTodosSignatarios()
    {
        $modelSignatario = new ModelSignatarioService(new ModelDTO($this->autorizacaoExecucao));

        $signatarioContratoPreposto = Contratopreposto::factory()->create();
        $modelSignatario->addSignatario(new SignatarioDTO($signatarioContratoPreposto), $this->arquivoData);

        $signatarioContratoResponsavel = Contratoresponsavel::factory()->create();
        $modelSignatario->addSignatario(new SignatarioDTO($signatarioContratoResponsavel), $this->arquivoData);

        $modelSignatario->removerTodosSignatarios();

        $this->assertDatabaseMissing(
            'models_signatarios',
            [
                'model_autorizacaoexecucao_id' => $this->autorizacaoExecucao->id,
                'signatario_contratoresponsavel_id' => $signatarioContratoResponsavel->id,
            ]
        );

        $this->assertDatabaseMissing(
            'models_signatarios',
            [
                'model_autorizacaoexecucao_id' => $this->autorizacaoExecucao->id,
                'signatario_contratopreposto_id' => $signatarioContratoPreposto->id,
            ]
        );
    }

    public function testRemoverSignatario()
    {
        $modelSignatario = new ModelSignatarioService(new ModelDTO($this->autorizacaoExecucao));


        $signatarioContratoResponsavel = Contratoresponsavel::factory()->create();
        $modelSignatario->addSignatario(new SignatarioDTO($signatarioContratoResponsavel), $this->arquivoData);

        $signatarioContratoPreposto = Contratopreposto::factory()->create();
        $signtarioPrepostoDTO = new SignatarioDTO($signatarioContratoPreposto);
        $modelSignatario->addSignatario($signtarioPrepostoDTO, $this->arquivoData);

        $modelSignatario->removerSignatario($signtarioPrepostoDTO);

        $this->assertDatabaseHas(
            'models_signatarios',
            [
                'model_autorizacaoexecucao_id' => $this->autorizacaoExecucao->id,
                'signatario_contratoresponsavel_id' => $signatarioContratoResponsavel->id,
            ]
        );

        $this->assertDatabaseMissing(
            'models_signatarios',
            [
                'model_autorizacaoexecucao_id' => $this->autorizacaoExecucao->id,
                'signatario_contratopreposto_id' => $signatarioContratoPreposto->id,
            ]
        );
    }
}
